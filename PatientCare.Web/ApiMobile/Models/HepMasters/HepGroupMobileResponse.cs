﻿using PatientCare.Web.Api.Models.HepGroups;
using PatientCare.Web.ApiMobile.Models.HepDetails;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.HepMasters
{
    [DataContract]
    public class HepGroupMobileResponse
    {
        [DataMember]
        public long? GroupId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int? ExerciseOrder { get; set; }

        //[DataMember]
        //public HepExerciseMobileResponse[] HepDetails { get; set; }

    }
}