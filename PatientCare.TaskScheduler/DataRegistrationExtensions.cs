﻿using System;
using Autofac;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Core.Services.Dashboards;
using PatientCare.Core.Services.ResourceScoreCalculation;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework.DataContext;
using PatientCare.Data.EntityFramework.Repositories;
using PatientCare.Services;
using PatientCare.Services.ResourceScoreCalculation;
using PatientCare.Services.Dashboards;
using PatientCare.Core.Services.Mailing;
using PatientCare.Services.Mailing;
using PatientCare.TaskScheduler.Mailing;

namespace PatientCare.TaskScheduler.IoC
{
    public static class DataRegistrationExtensions
    {
        public static ContainerBuilder RegisterRepositories(this ContainerBuilder containerBuilder)
        {
            if (containerBuilder == null) { throw new ArgumentNullException("containerBuilder"); }

            containerBuilder.RegisterType<DataContextFactory>().As<IDataContextFactory>().SingleInstance();
            containerBuilder.RegisterType<DataContextScopeFactory>().As<IDataContextScopeFactory>().SingleInstance();
            containerBuilder.RegisterType<AmbientDataContextLocator>().As<IAmbientDataContextLocator>().SingleInstance();

            #region PicklistRepository Register
            containerBuilder.RegisterType<PicklistRepository<JobTitle>>().As<IPicklistRepository<JobTitle>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Language>>().As<IPicklistRepository<Language>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Skill>>().As<IPicklistRepository<Skill>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<ResourceCompany>>().As<IPicklistRepository<ResourceCompany>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<SurveyCategory>>().As<IPicklistRepository<SurveyCategory>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Certificate>>().As<IPicklistRepository<Certificate>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Division>>().As<IPicklistRepository<Division>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Department>>().As<IPicklistRepository<Department>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Country>>().As<IPicklistRepository<Country>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<City>>().As<IPicklistRepository<City>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<State>>().As<IPicklistRepository<State>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<ResourceScoreFilter>>().As<IPicklistRepository<ResourceScoreFilter>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<DashboardSetup>>().As<IPicklistRepository<DashboardSetup>>().SingleInstance();
            #endregion

            #region Entity Repository Register
            containerBuilder.RegisterType<SurveyQuestionRepository>().As<ISurveyQuestionRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceRepository>().As<IResourceRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceTravelRepository>().As<IResourceTravelRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceCriminalRecordRepository>().As<IResourceCriminalRecordRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceEducationRepository>().As<IResourceEducationRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceSpokenLanguageRepository>().As<IResourceSpokenLanguageRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceTeamRepository>().As<IResourceTeamRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourcePreviousEmploymentRepository>().As<IResourcePreviousEmploymentRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceSurveyResultRepository>().As<IResourceSurveyResultRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceCertificateRepository>().As<IResourceCertificateRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceSkillRepository>().As<IResourceSkillRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceBehaviouralDetailRepository>().As<IResourceBehaviouralDetailRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceIndustryRepository>().As<IResourceIndustryRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourcePracticeAreaRepository>().As<IResourcePracticeAreaRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceRecognitionRepository>().As<IResourceRecognitionRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceInsuranceRepository>().As<IResourceInsuranceRepository>().SingleInstance();
            containerBuilder.RegisterType<CustomFieldRepository>().As<ICustomFieldRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceCustomFieldValueRepository>().As<IResourceCustomFieldValueRepository>().SingleInstance();
            containerBuilder.RegisterType<SentEmailRepository>().As<ISentEmailRepository>().SingleInstance();
            containerBuilder.RegisterType<RoleConfigureLayoutRepository>().As<IRoleConfigureLayoutRepository>().SingleInstance();
            containerBuilder.RegisterType<HolidayRepository>().As<IHolidayRepository>().SingleInstance();
            containerBuilder.RegisterType<LeaveTypeRepository>().As<ILeaveTypeRepository>().SingleInstance();
            containerBuilder.RegisterType<EntryTypeRepository>().As<IEntryTypeRepository>().SingleInstance();
            containerBuilder.RegisterType<LeaveRequestRepository>().As<ILeaveRequestRepository>().SingleInstance();
            containerBuilder.RegisterType<UserLoginHistoryRepository>().As<IUserLoginHistoryRepository>().SingleInstance();
            containerBuilder.RegisterType<DataContextScopeFactory>().As<IDataContextScopeFactory>().SingleInstance();
            containerBuilder.RegisterType<DropDownConfigRepository>().As<IDropDownConfigRepository>().SingleInstance();
            containerBuilder.RegisterType<UiConfigurationRepository>().As<IUiConfigurationRepository>().SingleInstance();
            containerBuilder.RegisterType<MailMessageFactory>().As<IMailMessageFactory>().SingleInstance();
            #endregion

            #region Service Register
            containerBuilder.RegisterType<AppUtilityService>().As<IAppUtilityService>().SingleInstance();
            containerBuilder.RegisterType<DropDownConfigService>().As<IDropDownConfigService>().SingleInstance();
            containerBuilder.RegisterType<UiConfigurationService>().As<IUiConfigurationServices>().SingleInstance();
            containerBuilder.RegisterType<TimeManagementService>().As<ITimeManagementService>().SingleInstance();
            containerBuilder.RegisterType<ResourceService>().As<IResourceService>().SingleInstance();
            containerBuilder.RegisterType<ResourceScoreService>().As<IResourceScoreService>().SingleInstance();
            containerBuilder.RegisterType<CustomFieldService>().As<ICustomFieldService>().SingleInstance();            
            containerBuilder.RegisterType<DashboardService>().As<IDashboardService>().SingleInstance();
            containerBuilder.RegisterType<EmailService>().As<IEmailService>().SingleInstance();
            containerBuilder.RegisterType<ClinicService>().As<IClinicService>().SingleInstance();
            #endregion

            containerBuilder.RegisterType<EmailSender>().As<IEmailSender>().SingleInstance();

            return containerBuilder;
        }
    }
}