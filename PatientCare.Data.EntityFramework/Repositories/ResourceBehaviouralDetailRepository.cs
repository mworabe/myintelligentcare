﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceBehaviouralDetailRepository : RepositoryBase<ResourceBehaviouralDetail>, IResourceBehaviouralDetailRepository
    {
        public ResourceBehaviouralDetailRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
