﻿using System;
using System.Collections.Generic;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Patient Address.
    /// </summary>
    public class PatientAddress : Entity
    {
        #region Patient Address Division

        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public string Address { get; set; }

        public bool? AddressIsPrimary { get; set; }

        public long? CityId { get; set; }
        public virtual City City { get; set; }

        public long? StateId { get; set; }
        public virtual State State { get; set; }

        public long? CountryId { get; set; }
        public virtual Country Country { get; set; }

        public string ZipCode { get; set; }

        public int? Order { get; set; }
        
        #endregion
    }
}
