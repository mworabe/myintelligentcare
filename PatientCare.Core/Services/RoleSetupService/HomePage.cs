﻿namespace PatientCare.Core.Services.RoleSetupService
{
    public enum HomePage
    {
        HumanResourceHome = 0,
        ResourceHome = 1,
        Calendar = 2,
        Dashboard = 3,
        Projects = 4,
        Resources = 5,
    }
}
