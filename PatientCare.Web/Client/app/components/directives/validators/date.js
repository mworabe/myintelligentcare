// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.
//
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngFormatDate", [
        function () {
            return {
                restrict: "A",
                require: "ngModel",
                link: function ($scope, $elem, $attrs, ngCtrl) {
                    $elem.bind('blur', function () {

                        var reGoodDate = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
                        

                        if (!$elem.val().match(reGoodDate)) {
                            $elem.addClass('element-validation-error');
                            $elem.parent('form').addClass('ng-invalid');
                            ngCtrl.$valid = false;
                            ngCtrl.$pristine = false;
                            ngCtrl.$setValidity("pattern", false);
                        } else {
                            $elem.removeClass('element-validation-error');
                            $elem.parent('form').removeClass('ng-invalid');
                            ngCtrl.$valid = true;
                            ngCtrl.$setValidity("pattern", true);
                        }

                    });

                    //$scope.$watch($attrs.ngModel, function (value) {
                    //    if(!value) {
                    //        ngCtrl.$setViewValue(undefined);
                    //        ngCtrl.$render();
                    //        ngCtrl.$setPristine();
                    //        ngCtrl.$$parentForm.$setPristine();
                    //    }
                    //});
                }
            };
        }
    ]);