﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.ResourceSpokenLanguages
{
    [DataContract]
    public class ResourceSpokenLanguageResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public PicklistResponse Language { get; set; }

        [DataMember]
        public int YearsOfExpirience { get; set; }

        [DataMember]
        public int SpeakingProficiency { get; set; }

        [DataMember]
        public int WritingProficiency { get; set; }
        [DataMember]
        public long ResourceId { get; set; }

        [DataMember]
        public FileRequest[] Files { get; set; }

    }
}