"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("CarePlanAddEditCntrl",
    ["$scope", 'HepService', '$state', 'toaster', '$stateParams', 'dataObject', '$timeout', "ConstantFacory",
        function ($scope, HepService, $state, toaster, $stateParams, dataObject, $timeout, ConstantFacory) {
            self = this;

            var modelId = $stateParams.Id || undefined;

            if (modelId && dataObject) {
                self.model = dataObject[0];
                self.title = "Edit Care Plan"
            }

            var modelId = $stateParams.id || undefined;
            self.model = {
                patientId: $stateParams.patientId,
                patientCaseId: $stateParams.caseId,
                hepDetails: [],
                hepGroups: []
            }
            self.title = "Create Care Plan";

            self.isNewCarePlan = function () {
                return (modelId && modelId > 0);
            }

            self.getDefaultItem = function (item) {
                var freq;
                if (_.isEqual(item.frequency, "1x Day")) {
                    freq = [
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 1, day: 0 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 1, day: 1 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 1, day: 2 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 1, day: 3 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 1, day: 4 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 1, day: 5 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 1, day: 6 },
                    ];
                }
                else if (_.isEqual(item.frequency, '2x Day')) {
                    freq = [
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 2, day: 0 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 2, day: 1 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 2, day: 2 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 2, day: 3 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 2, day: 4 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 2, day: 5 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 2, day: 6 },
                    ];
                }
                else if (_.isEqual(item.frequency, '3x Day')) {
                    freq = [
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 3, day: 0 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 3, day: 1 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 3, day: 2 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 3, day: 3 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 3, day: 4 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 3, day: 5 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 3, day: 6 },
                    ];
                }
                else if (_.isEqual(item.frequency, '4x Day')) {
                    freq = [
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 4, day: 0 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 4, day: 1 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 4, day: 2 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 4, day: 3 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 4, day: 4 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 4, day: 5 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 4, day: 6 },
                    ];
                }
                else if (_.isEqual(item.frequency, '5x Day')) {
                    freq = [
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 5, day: 0 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 5, day: 1 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 5, day: 2 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 5, day: 3 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 5, day: 4 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 5, day: 5 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 5, day: 6 },
                    ];
                }
                else {
                    freq = [
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 0, day: 0 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 0, day: 1 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 0, day: 2 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 0, day: 3 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 0, day: 4 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 0, day: 5 },
                        { id: 0, holdsUnit: 0, ExerciseFrequency: 0, day: 6 },
                    ];
                }
                return {
                    exerciseId: item.id,
                    id: 0,
                    sets: item.set,
                    reps: item.reps,
                    time: item.time,
                    timeUnit: item.timeUnit,
                    holds: item.holds,
                    holdsUnit: item.holdUnit,
                    weight: item.weight,
                    weightUnit: item.weightUnit,
                    isActiveFrequency: false,
                    resistance: item.resistance,
                    frequency: item.frequency,
                    frequencies: freq,
                    comments: item.comments,
                    name: item.name
                };
            }

            self.isItemInList = function (exerciseItem) {

                if (self.model.hepDetails) {
                    for (var i = 0; i < self.model.hepDetails.length; i++) {
                        var obj = self.model.hepDetails[i];
                        if (obj.exerciseId == exerciseItem.id) {
                            return obj;
                        }
                    }
                }
                return null;
            }

            self.onChooseBackClick = function () {
                $state.go("patientCaseEdit", { patientId: self.model.patientId, caseId: self.model.patientCaseId, currentTab: "hep" });
            }
            self.additional;
            self.myTemplate;
            self.count = 0;
            self.aa = [];
            self.k = {};
            self.l = {};
            self.bb = [];
            
            self.onChooseSaveClick = function (selectedItems) {
                if (selectedItems && selectedItems.length > 0) {
                    var abc = selectedItems.length;
                    self.l.exerciseId = 0;
                    self.l.template = null;
                    selectedItems.push(self.l);     
                    for (var i = 0; i < selectedItems.length-1; i++) {
                        var item = selectedItems[i];
                        var itemss = selectedItems[i + 1];
                        if (!self.isItemInList(item)) {
                            if (item.template) {
                                if (self.myTemplate == null && itemss.template == item.template) {
                                    self.k.name = item.template;
                                    self.myTemplate = self.k.name;
                                    self.aa[0] = self.getDefaultItem(item);
                                    self.count++;
                                }
                                else if (self.myTemplate == null && itemss.template == null) {
                                    self.k.name = item.template;
                                    self.myTemplate = self.k.name;
                                    self.aa[0] = self.getDefaultItem(item);
                                    self.k.hepDetails = self.aa;
                                    self.model.hepGroups.push(self.k);
                                    self.count = 0;
                                    self.aa = [];
                                    self.k = {};
                                    self.myTemplate = null;
                                }
                                else if (self.myTemplate == item.template && itemss.template == self.myTemplate) {
                                    self.aa[self.count] = self.getDefaultItem(item);
                                    self.count++;
                                }
                                else if (self.myTemplate == item.template && itemss.template != self.myTemplate && itemss.template != null) {
                                    self.aa[self.count] = self.getDefaultItem(item);
                                    self.k.hepDetails = self.aa;
                                    self.model.hepGroups.push(self.k);
                                    self.count = 0;
                                    self.aa = [];
                                    self.k = {};
                                    self.myTemplate = null;
                                }
                                else if (self.myTemplate == item.template && itemss.template == null) {
                                    self.aa[self.count] = self.getDefaultItem(item);
                                    self.k.hepDetails = self.aa;
                                    self.model.hepGroups.push(self.k);
                                    self.count = 0;
                                    self.aa = [];
                                    self.k = {};
                                    self.myTemplate = null;
                                }
                                else if (self.myTemplate != item.template && itemss.template == null) {
                                    self.k.hepDetails = self.aa;
                                    self.model.hepGroups.push(self.k);
                                    self.count = 0;
                                    self.aa = [];
                                    self.k = {};
                                    self.myTemplate = null;
                                }                               
                            }
                            else {
                                var obj = self.getDefaultItem(item);
                                obj.exerciseOrder = (i + 1);
                                self.model.hepDetails.push(obj);
                            }
                        }
                    }
                    self.onSave();
                } else {
                    toaster.pop("error", "", "Please select at least one exercise item.");
                }
            }

            self.chooseScreenConfig = {
                onBackBtn: self.onChooseBackClick,
                onSaveBtn: self.onChooseSaveClick,
                showCopyFromMaster: true,
                showCopyFromFavorite: true
            };


            if (modelId && dataObject) {
                self.model = dataObject[0];
                self.title = "Add Exercise"
            }



            function getRequest() {
                if (self.model.patient) {
                    self.model.patient = null;
                }

                if (self.model.patientCase) {
                    self.model.patientCase = null;
                }



                if (self.model.hepDetails && self.model.hepDetails.length > 0) {
                    angular.forEach(self.model.hepDetails, function (item) {
                        item.exercise = null;
                    })
                }

                if (self.model.hepGroups && self.model.hepGroups.length > 0) {
                    angular.forEach(self.model.hepGroups, function (groupItem) {
                        angular.forEach(groupItem.hepDetails, function (item) {
                            item.exercise = null;
                        })
                    })
                }
                self.model.isSaveAsDraft = true;

                return self.model;
            }
            function validate(form) {
                if (!(self.model.hepDetails && self.model.hepDetails.length > 0)) {
                    toaster.pop("error", "", "Please select at least one exercise.");
                    return false;
                }

                if (!self.model.startDate) {
                    toaster.pop("error", "", "Please select start date.");
                    return false;
                }

                if (!self.model.endDate) {
                    toaster.pop("error", "", "Please select end date.");
                    return false;
                }

                if (self.model.startDate > self.model.endDate) {
                    toaster.pop("error", "", "Start date must be less than end date. ");
                    return false;
                }

                if (self.model.endDate < self.model.startDate) {
                    toaster.pop("error", "", "End date must be greater than start date. ");
                    return false;
                }

                return true;
            }

            self.onSave = function (form) {

                if (validate()) {
                    self.isLoading = HepService.saveHep(getRequest(), onSuccess, onError);
                }
            }
            function onSuccess(response) {
                if (response && response.id) {
                    toaster.pop("success", "", "Exercise Template Saved Successfully.");
                    self.onChooseBackClick();
                }
            }
            function onError(Error) {

            }


        }
    ]);