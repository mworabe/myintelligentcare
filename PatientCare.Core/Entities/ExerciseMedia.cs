﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Exercise Media.
    /// </summary>
    public class ExerciseMedia : NamedEntity
    {
        public long? ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        public string OriginalName { get; set; }

        public MediaType? MediaType { get; set; }

        public string ThumbImageName { get; set; }
    }
}
