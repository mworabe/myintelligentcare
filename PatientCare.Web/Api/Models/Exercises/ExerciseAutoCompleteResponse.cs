﻿using PatientCare.Web.Api.Models.DropDownConfigs;
using PatientCare.Web.Api.Models.ExerciseActivities;
using PatientCare.Web.Api.Models.ExerciseMedias;
using PatientCare.Web.Api.Models.ExercisePositions;
using PatientCare.Web.Api.Models.InjuryRegions;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Exercises
{
    [DataContract]
    public class ExerciseAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? RegionId { get; set; }

        [DataMember]
        public InjuryRegionAutoCompleteResponse Region { get; set; }

        [DataMember]
        public long? PositionId { get; set; }

        [DataMember]
        public ExercisePositionAutoCompleteResponse Position { get; set; }

        [DataMember]
        public long? ActivityId { get; set; }

        [DataMember]
        public ExerciseActivityAutoCompleteResponse Activity { get; set; }

        [DataMember]
        public long? ExerciseRegionId { get; set; }

        [DataMember]
        public DropDownConfigResponse ExerciseRegion { get; set; }

        [DataMember]
        public string Keywords { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public ExerciseMediaResponse[] Medias { get; set; }

        [DataMember]
        public long? set { get; set; }

        [DataMember]
        public long? reps { get; set; }

        [DataMember]
        public long? holds { get; set; }

        [DataMember]
        public long? weight { get; set; }

        [DataMember]
        public string weightUnit { get; set; }

        [DataMember]
        public string holdUnit { get; set; }

        [DataMember]
        public string frequency { get; set; }

    }
}