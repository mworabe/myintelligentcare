﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("ClinicianService", ["$resource", function ($resource) {
    var resource = $resource("api/Clinicians/:controller/:id",
        null, //parameters default
        {
            'query': { method: "GET", params: {}, isArray: false },
            'update': { method: "PUT", params: {}, isArray: false },
            'all': { method: "GET", params: {}, isArray: true, url: "api/Clinicians/autocomplete?search=&type=all" },
            'alll': { method: "GET", params: {}, isArray: true, url: "api/Clinicians/autocompletee" },
        });
    return resource;
}]);
