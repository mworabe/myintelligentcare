﻿using System;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.ResourceInsurances;
using PatientCare.Core.Entities;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceInsuranceMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceInsuranceRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceInsurance, ResourceInsuranceResponse>
                    (new ExtendedMapConfig<ResourceInsurance,ResourceInsuranceResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceInsurance[],ResourceInsuranceResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceInsuranceResponse, ResourceInsurance>
                    (new ExtendedMapConfig<ResourceInsuranceResponse, ResourceInsurance>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceInsuranceRequest,ResourceInsurance>
                    (new ExtendedMapConfig<ResourceInsuranceRequest,ResourceInsurance>()));

            return mapperProvider;
        }
    }
}