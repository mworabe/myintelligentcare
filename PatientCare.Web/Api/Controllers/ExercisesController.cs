﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Exercises;
using System.Linq.Expressions;
using System.Web.Http.Description;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Web.Services.FileStorage;
using System.Collections.Generic;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using PatientCare.Data.EntityFramework;
using PatientCare.Data.AbstractDataContext;
using System.Net.Http;
using System.Net;
using System.IO;
using PatientCare.Core.Enums;
using System.Diagnostics;
using PatientCare.Web.Api.Models.Exercise_injury;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Exercise operations.
    /// </summary>
    [Authorize]
    public sealed class ExercisesController : ApiBaseController
    {
        /// <summary>
        /// Description : Exercise Field IExerciseService.
        /// </summary>
        private readonly IExerciseService _exerciseService;
        /// <summary>
        /// Description : Exercise Field IExerciseRepository.
        /// </summary>
        private readonly IExerciseRepository _exerciseRepository;
        /// <summary>
        /// Description : Exercise Field IExerciseMediaRepository.
        /// </summary>
        private readonly IExerciseMediaRepository _exerciseMediaRepository;
        /// <summary>
        /// Description : Exercise Field IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        private readonly IExercise_ExerciseActivityRepository _exerciseActivityRepository;

        private readonly IExercise_injuryRepository _exerciseDropdownConfigRepository;

        ReadOptions<Exercise> _readOptions = new ReadOptions<Exercise>();

        public object ctx { get; private set; }

        /// <summary>
        /// Description : Exercise Class Constructor.      
        /// </summary>
        /// <param name="exerciseService">IExercise Service</param>
        /// <param name="exerciseRepository">IExercise Repository</param>
        /// <param name="exerciseMediaRepository">IExercise Media Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        ///  /// <param name="exerciseDropdown">IExercise Media Repository</param>
        /// <param name="exerciseActivity">IData Context Scope Factory</param>
        public ExercisesController
            (
                IExerciseService exerciseService,
                IExerciseRepository exerciseRepository,
                IExerciseMediaRepository exerciseMediaRepository,
                IDataContextScopeFactory dataContextScopeFactory,
                IExercise_injuryRepository exerciseDropdown,
                IExercise_ExerciseActivityRepository exerciseActivity
            )
        {
            _exerciseService = exerciseService;
            _exerciseRepository = exerciseRepository;
            _exerciseMediaRepository = exerciseMediaRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
            _exerciseActivityRepository = exerciseActivity;
            _exerciseDropdownConfigRepository = exerciseDropdown;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Exercises/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Exercise.
        /// </summary>
        /// <param name="request">Exercise List Request Model</param>
        /// <returns>Exercise Response Model</returns>
        [HttpGet]
        [Route("api/Exercises")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ExerciseListResponse>))]
        public IHttpActionResult GetAllExercise([FromUri]ExerciseListRequest request)
        {
            var query = PagingExtensions<Exercise>.CreatePagedQuery
                      (request,
                          (string.IsNullOrWhiteSpace(request.SearchField)
                              && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                  ?
                             GetSearchColumnExpressions(request.SearchPhrase)
                            : GetAutocompleteSearchExpressions(request.SearchPhrase)
                           ));

            // here we not take deleted exercise in list screen
            query.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);

            var readOptions = new ReadOptions<Exercise>().AsReadOnly();

            var page = _exerciseRepository.PagedList(query, readOptions);
            var data = page.Entities.ToArray();
            var newPages = new List<Exercise>();

            if (data != null && data.Count() > 0)
                newPages = data.ToList();

            var dtos = Mapper.Map<Exercise[], ExerciseResponse[]>(newPages.ToArray());
            var response = new ListResponse<ExerciseResponse>(dtos, page.TotalCount);        
            foreach (var exercise in response.Data)
            {
                foreach (var file in exercise.Medias)
                {
                    file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(exercise.Id));
                    file.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(file.Name);

                    List<FileRequest> fileAll = new List<FileRequest>();
                    FileRequest tempFile = new FileRequest();

                    if (StorageService.UseAmazonStorage == true)
                        tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(file.Name, GetExerciseMediaS3Path(file.ExerciseId.Value));
                    else
                        tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + file.Id;

                    tempFile.S3BucketName = file.Name;
                    tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                    #region

                    if (file.ThumbImageName != null)
                    {
                        file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(exercise.Id));
                        //file.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                        file.ThumbImageName = file.ThumbImageName;

                        if (StorageService.UseAmazonStorage == true)
                            tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(file.ThumbImageName, GetExerciseMediaS3Path(file.ExerciseId.Value));
                        else
                            tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                        tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                    }
                    #endregion

                    fileAll.Add(tempFile);
                    file.Files = fileAll.ToArray();
                }
            }

            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Exercise.   
        /// </summary>
        /// <param name="exerciseRequest">Exercise Request Model</param>
        /// <returns>Exercise Response Model</returns>
        [HttpPost]
        [Route("api/Exercises")]
        [SwaggerResponse(201, "Created", typeof(ExerciseResponse))]
        public async Task<IHttpActionResult> CreateExercise([FromBody]ExerciseRequest exerciseRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var userId = User.Identity.GetUserId();
                if (userId == null)
                {
                    ModelState.AddModelError("Exercise", "your session is expired. please login again");
                    return BadRequest(ModelState);
                }

                //var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
                //if (user == null && user.Resource == null)
                //{
                //    ModelState.AddModelError("Exercise", "Logged in user is not associated with Resource, contact admin to assign resource");
                //    return BadRequest(ModelState);
                //}

                var exercise = Mapper.Map<ExerciseRequest, Exercise>(exerciseRequest);
                var injuries = exercise.injuries;
                exercise.injuries = null;
                await _exerciseService.CreateExerciseAsync(exercise);
                foreach (var injury in injuries)
                {
                    injury.exercise_Id = exercise.Id;
                    await _exerciseDropdownConfigRepository.CreateAsync(injury);
                }
                await ExerciseMediaSave(exercise.Id, exercise, true);
                return Created(GetEntityLocation(exercise.Id), Mapper.Map<Exercise, ExerciseResponse>(exercise));
            }
            catch(Exception ex)
            {
                return Ok("");
            }
        }

        /// <summary>
        /// Method Description : Using for Update Exercise.   
        /// </summary>
        /// <param name="Id">Exercise Request Model</param>
        /// <param name="exerciseRequest">Exercise Request Model</param>
        /// <returns>Exercise Response Model</returns>
        [HttpPut]
        [Route("api/Exercises/{Id}")]
        public async Task<IHttpActionResult> EditExercise(long Id, [FromBody] ExerciseRequest exerciseRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                ModelState.AddModelError("Exercise", "your session is expired. please login again");
                return BadRequest(ModelState);
            }

            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            //if (user == null && user.Resource == null)
            //{
            //    ModelState.AddModelError("Exercise", "Logged in user is not associated with Resource, contact admin to assign resource");
            //    return BadRequest(ModelState);
            //}

            var exercise = Mapper.Map<ExerciseRequest, Exercise>(exerciseRequest);
            exercise.Id = Id;
            var injuries = exercise.injuries;
            exercise.injuries = null;
            await _exerciseService.UpdateExerciseAsync(exercise);
            foreach (var inj in injuries)
            {
                inj.exercise_Id = exercise.Id;
                await _exerciseDropdownConfigRepository.CreateAsync(inj);
            }
            

            await ExerciseMediaSave(exercise.Id, exercise);

            return Ok(Mapper.Map<Exercise, ExerciseResponse>(exercise));
        }

        /// <summary>
        /// Method Description : Using for Delete Exercise  by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Exercise Response Model</returns>
        [HttpDelete]
        [Route("api/Exercises/{Id}")]
        public async Task<IHttpActionResult> DeleteExerciseItem(long id)
        {
            var exercise = _exerciseRepository.Read(id);
            if (exercise == null)
                return NotFound();
            await _exerciseService.DeleteExerciseAsync(exercise);
            return Ok(new ExerciseResponse { Id = exercise.Id });
        }

        /// <summary>
        /// Method Description : Using for Get Exercise by Id.    
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>Exercise Response Model</returns>
        [HttpGet]
        [Route("api/Exercises/{Id}")]
        public IHttpActionResult GetExercise(long Id)
        {
            var viewModel = GetExerciseById(Id);
            return Ok(viewModel);
        }

        /// <summary>
        /// Method Description : Using for Get Exercise by Id.    
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>Exercise Response Model</returns>
        private ExerciseResponse GetExerciseById(long Id)
        {
            var viewModel = new ExerciseResponse();
            var exercise = _exerciseRepository.Read(Id);
            if (exercise == null)
                return new ExerciseResponse();

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                List<Exercise_injuryResponse> exerciseinjury = ctx.injury.AsNoTracking()
                                .Where(x => x.exercise_Id == Id)
                                .Select(y => new Exercise_injuryResponse { Id = y.Id, exercise_Id = y.exercise_Id, injury_Id = y.injury_Id }).ToList();
                //exercise.injuries = exerciseinjury;
                // }

                viewModel = Mapper.Map<Exercise, ExerciseResponse>(exercise);
                foreach (var file in viewModel.Medias)
                {
                    file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(exercise.Id));
                    file.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(file.Name);

                    List<FileRequest> fileAll = new List<FileRequest>();
                    FileRequest tempFile = new FileRequest();

                    if (StorageService.UseAmazonStorage == true)
                        tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(file.Name, GetExerciseMediaS3Path(file.ExerciseId.Value));
                    else
                        tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + file.Id;

                    tempFile.S3BucketName = file.OriginalName;
                    tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                    #region
                    if (file.ThumbImageName != null)
                    {
                        file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(exercise.Id));
                        //file.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                        file.ThumbImageName = file.ThumbImageName;

                        if (StorageService.UseAmazonStorage == true)
                            tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(file.ThumbImageName, GetExerciseMediaS3Path(file.ExerciseId.Value));
                        else
                            tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                        tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                    }
                    #endregion


                    fileAll.Add(tempFile);
                    file.Files = fileAll.ToArray();
                }
                viewModel.injuries = exerciseinjury.ToArray();
                return viewModel;
            }
        }

        private static Expression<Func<Exercise, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return exercise => exercise.Name.Contains(searchPhrase)
                   || exercise.Keywords.Contains(searchPhrase);
        }

        private static Expression<Func<Exercise, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
                return null;

            //return exercise => exercise.Name.Contains(searchPhrase);

            return exercise => exercise.Name.Contains(searchPhrase)
                   || exercise.Keywords.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Exercise.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Exercise Position Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/Exercises/autocomplete")]
        [ResponseType(typeof(ExerciseAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<Exercise>();
            if (!string.IsNullOrWhiteSpace(search))
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));

            filter.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);
            var autocompleteResult = await _exerciseRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<Exercise[], ExerciseAutoCompleteResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }

        private async Task ExerciseMediaSave(long id, Exercise Exercise, bool isNew = false)
        {
            var mediaList = new List<Task>();

            var mediaFiles = (await _exerciseMediaRepository.ListAsync(
                    new FilterQuery<ExerciseMedia>().AddFilter(x => x.ExerciseId == id))).ToList();

            var s3Files = isNew ? new string[0] : FileUploadHelpers.GetFileNames(
                string.Format(FileUploadHelpers.ExerciseMediaFilesFolderTemplate, id));

            foreach (var exerciseMedia in mediaFiles)
            {
                if (string.IsNullOrWhiteSpace(exerciseMedia.Name))
                    continue;

                if (s3Files.Contains(exerciseMedia.Name))
                    continue;

                mediaList.Add(FileUploadHelpers.CopyAsync(exerciseMedia.Name, "temp",
                    exerciseMedia.Name,
                    GetExerciseMediaS3Path(id)));

                if (exerciseMedia.MediaType == MediaType.Video)
                {
                    if (string.IsNullOrWhiteSpace(exerciseMedia.ThumbImageName))
                        continue;

                    if (s3Files.Contains(exerciseMedia.ThumbImageName))
                        continue;

                    mediaList.Add(FileUploadHelpers.CopyAsync(exerciseMedia.ThumbImageName, "temp",
                        exerciseMedia.ThumbImageName,
                        GetExerciseMediaS3Path(id)));
                }
            }

            await Task.WhenAll(mediaList.ToArray());
        }

        /// <summary>
        /// Method Description : Using for Get All Exercise.
        /// </summary>
        /// <param name="request">Exercise List Request Model</param>
        /// <returns>Exercise Response Model</returns>
        [HttpGet]
        [Route("api/GetAllExercises")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ExerciseFilterListResponse>))]
        public async Task<IHttpActionResult> GetAllExercises([FromUri]ExerciseListRequest request)
        {
            request.PageIndex = request.PageIndex - 1;
            if (request.RegionId != null)
                request.RegionId = ',' + request.RegionId + ',';
            if (request.ActivityId != null)
                request.ActivityId = ',' + request.ActivityId + ',';
            List<ExerciseResponse> exerciseList = new List<ExerciseResponse>();
            var response = new ListResponse<ExerciseResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                //For Data
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC SP_EXXX_News @responseType='Data',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@regionId = '" + request.RegionId + "',@activityId = '" + request.ActivityId + "',@keywords = '" + request.Keywords + "'";
                var exerciseIdList = await ctx.Database.SqlQuery<long>(query).ToListAsync();

                foreach (var exercise in exerciseIdList.ToList())
                {
                    var tempExercise = GetExerciseById(exercise);
                    exerciseList.Add(tempExercise);
                }
                response.Data = exerciseList.ToArray();             
                string itemCountQuery = "EXEC SP_EXXX_News @responseType='DataCount',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@regionId = '" + request.RegionId + "',@activityId = '" + request.ActivityId + "',@keywords = '" + request.Keywords + "'";
                response.ItemsCount = await ctx.Database.SqlQuery<long>(itemCountQuery).FirstOrDefaultAsync();

                return Ok(response);
            }
        }

        [HttpGet]
        [Route("api/GetAllExercisess")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ExerciseFilterListResponse>))]
        public async Task<IHttpActionResult> GetAllExercisess([FromUri]ExerciseListRequest request)
        {
            request.PageIndex = request.PageIndex - 1;
            request.PageIndex = request.PageIndex - 1;
            if (request.RegionId != null)
                request.RegionId = ',' + request.RegionId + ',';
            if (request.ActivityId != null)
                request.ActivityId = ',' + request.ActivityId + ',';
            List<ExerciseResponse> exerciseList = new List<ExerciseResponse>();
            var response = new ListResponse<ExerciseResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                //For Data
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC SP_EXXX_New @responseType='Data',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@regionId = '" + request.RegionId + "',@activityId = '" + request.ActivityId + "',@keywords = '" + request.Keywords + "'";//,@regionId = '" + request.RegionId + "',@positionId = '" + request.PositionId + "',@activityId = '" + request.ActivityId + "'";
                var exerciseIdList = await ctx.Database.SqlQuery<long>(query).ToListAsync();

                foreach (var exercise in exerciseIdList.ToList())
                {
                    var tempExercise = GetExerciseById(exercise);
                    exerciseList.Add(tempExercise);
                }
                response.Data = exerciseList.ToArray();
                string itemCountQuery = "EXEC SP_EXXX_New @responseType='DataCount',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@regionId = '" + request.RegionId + "',@activityId = '" + request.ActivityId + "',@keywords = '" + request.Keywords + "'";//,@regionId = '" + request.RegionId + "',@positionId = '" + request.PositionId + "',@activityId = '" + request.ActivityId + "'";
                response.ItemsCount = await ctx.Database.SqlQuery<long>(itemCountQuery).FirstOrDefaultAsync();

                return Ok(response);
            }
        }

        /// <summary>
        /// Method Description : Using for Upload File. 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Exercises/UploadFile")]
        public async Task<IHttpActionResult> UploadFile()
        {

            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            var result = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());
            var originalFileName = result.Files.First().Headers.ContentDisposition.FileName;
            originalFileName = originalFileName.Substring(1, originalFileName.Length - 2);
            var newfileName = string.Format(FileUploadHelpers.FileNameTemplate, Guid.NewGuid(), originalFileName).ToLower();

            var file = result.Files[0];
            var fileStream = await file.ReadAsStreamAsync();

            string ctype = result.Files[0].Headers.ContentType.MediaType.ToString();

            var wasS3UploadSuccessful = await FileUploadHelpers.S3UploadAsync(newfileName, fileStream, "temp");

            if (wasS3UploadSuccessful)
            {
                #region Generate video thumb
                string thumbFileName = string.Empty;

                if (ctype == "image/jpg" || ctype == "image/jpeg" || ctype == "image/png")
                {
                    return Ok(new
                    {
                        FileName = newfileName,
                        ThumbFileName = thumbFileName
                    });
                }
                else
                {
                    string MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(newfileName, "temp");
                    var ffMpeg = new NReco.VideoConverter.FFMpegConverter();

                    Stream outputS = new MemoryStream();

                    ffMpeg.GetVideoThumbnail(MediaURL, outputS, ((float)StorageService.FrameStartInSecond));
                    thumbFileName = string.Format(FileUploadHelpers.FileNameTemplate, Guid.NewGuid(), "thumb_image.jpg").ToLower();

                    var wasS3UploadThumbSuccessful = await FileUploadHelpers.S3UploadAsync(thumbFileName, outputS, "temp");
                    string thumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(thumbFileName, "temp");

                    return Ok(new
                    {
                        FileName = newfileName,
                        ThumbFileName = thumbFileName
                    });
                }
                #endregion
            }
            return BadRequest();
        }

    }
}
