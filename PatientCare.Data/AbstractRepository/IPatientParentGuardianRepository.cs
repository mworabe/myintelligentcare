﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IPatientParentGuardianRepository : IRepository<PatientParentGuardian>
    {
    }
}
