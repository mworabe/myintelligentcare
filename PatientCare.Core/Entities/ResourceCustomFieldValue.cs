﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Custom Field Value.
    /// </summary>
    public class ResourceCustomFieldValue : Entity
    {
        public long CustomFieldId { get; set; }
        public virtual CustomField CustomField { get; set; }
        public long ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        public string Value { get; set; }
    }
}