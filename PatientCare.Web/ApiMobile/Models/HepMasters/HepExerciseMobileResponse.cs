﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.HepFrequencies;
using PatientCare.Web.ApiMobile.Models.ExerciseMedias;
using PatientCare.Web.ApiMobile.Models.HepFrequencies;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.HepMasters
{
    [DataContract]
    public class HepExerciseMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ExerciseId { get; set; }

        [DataMember]
        public string ExerciseName { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public ExerciseMediaMobileResponse[] Medias { get; set; }

        [DataMember]
        public long? GroupId { get; set; }

        [DataMember]
        public long? HepId { get; set; }

        [DataMember]
        public int? Sets { get; set; }

        [DataMember]
        public int? Reps { get; set; }

        [DataMember]
        public int? Time { get; set; }

        [DataMember]
        [JsonConverter(typeof(StringEnumConverter))]
        public TimeEnum? TimeUnit { get; set; }

        [DataMember]
        public decimal? Weight { get; set; }

        [DataMember]
        public string WeightUnit { get; set; }

        [DataMember]
        public int? Holds { get; set; }

        [DataMember]
        [JsonConverter(typeof(StringEnumConverter))]
        public TimeEnum? HoldsUnit { get; set; }

        [DataMember]
        public int? ExerciseOrder { get; set; }

        [DataMember]
        public HepFrequencyMobileResponse[] Frequencies { get; set; }
        
        [DataMember]
        public bool? IsActiveFrequency { get; set; }

    }
}