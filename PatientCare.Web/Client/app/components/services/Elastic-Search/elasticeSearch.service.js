﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
.service("ElasticSearch", ["$resource", function ($resource) {
    var resource = $resource("api/search",
        null, {
            'getFilterData': { method: "GET", params: { filter: "" }, isArray: false }
        });
    return resource;
}
]);