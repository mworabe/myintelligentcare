﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IEntryTypeRepository : IRepository<EntryType>
    {
    }
}
