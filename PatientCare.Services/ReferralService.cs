﻿using System;
using System.Data;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle Referral operations And implementing all methods which is initialize in IReferralService.
    /// </summary>
    public class ReferralService : IReferralService
    {
        /// <summary>
        ///Description : Referral Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Referral Service Field IReferralPhysicianRepository
        /// </summary>
        private readonly IReferralPhysicianRepository _referralPhysicianRepository;

        /// <summary>
        ///Description : Referral Service Field IPatientRepository
        /// </summary>
        private readonly IPatientRepository _patientRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Referral class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="referralPhysicianRepository">Referral Physician Repository</param>
        /// <param name="patientRepository">Patient Repository</param>
        public ReferralService
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IReferralPhysicianRepository referralPhysicianRepository,
                IPatientRepository patientRepository
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _referralPhysicianRepository = referralPhysicianRepository;
            _patientRepository = patientRepository;
        }

        #region Referral Physician Methods

        /// <summary>
        /// Method Description : Using for Update Referral Physician.
        /// </summary>
        /// <param name="referralPhysician">Referral Physician Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateReferralPhysicianAsync(ReferralPhysician referralPhysician)
        {
            if (referralPhysician == null) throw new ArgumentNullException("referralPhysicians");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originreferralPhysician = _referralPhysicianRepository.Read(referralPhysician.Id, new ReadOptions<ReferralPhysician>().AsReadOnly());

                await _referralPhysicianRepository.UpdateAsync(referralPhysician, UpdateOptions.Create<ReferralPhysician>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new Referral Physician.
        /// </summary>
        /// <param name="referralPhysician">Referral Physician Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateReferralPhysicianAsync(ReferralPhysician referralPhysician)
        {
            if (referralPhysician == null) throw new ArgumentNullException("referralPhysicians");

            await _referralPhysicianRepository.CreateAsync(referralPhysician, UpdateOptions.Create<ReferralPhysician>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Referral Physician by Id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Referral Physician Entity</returns>
        public async Task<ReferralPhysician> ReadReferralPhysicianAsync(long id)
        {
            var readOptions = new ReadOptions<ReferralPhysician>().AsReadOnly();

            var referralPhysician = await _referralPhysicianRepository.ReadAsync(id, readOptions);

            return referralPhysician;

        }

        /// <summary>
        /// Method Description : Using for delete Referral Physician. 
        /// </summary>
        /// <param name="physician">Referral Physician Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<string> DeleteReferralPhysicianAsync(ReferralPhysician physician)
        {
            var allowDeleting = true;

            var sb = new StringBuilder(string.Format("You can't delete {0} because :", physician.Name));

            #region patient
            var patients = (await _patientRepository.ListAsync(
                    new FilterQuery<Patient>().AddFilter(x => x.ReferralPhysicianId == physician.Id)))
                    .ToList();

            if (patients.Any())
            {
                allowDeleting = false;
                string s = Environment.NewLine + "&nbsp&nbsp&nbsp▪ " + physician.Name + " is referring this patient(s) : " + string.Join(", ", patients.Select(x => x.Name).ToList());
                sb.AppendLine(s);
            }
            #endregion

            if (allowDeleting)
            {
                await _referralPhysicianRepository.DeleteAsync(physician);
                return "";
            }

            sb.Replace(Environment.NewLine, "<br/>");

            return sb.ToString();
        }

        #endregion
    }
}
