﻿/*
 * Creted by: Miguel Arteaga Orozco 
 * March 22, 2016 
 */

"use strict";

angular.module("EDZoutstaffingPortalApp")

.filter('maskSsn', function () {
    return function (value) {
        if (value && value.length > 5) {
            var returnValue = "*****";
            returnValue += value.substring(5, value.length);
            return returnValue;
        }
        return value;

    }

}).directive('maskedHidePartialText',
  function ($window, $filter) {
      return {

          restrict: 'EAC',
          require: '?ngModel',
          scope: {
              relatedModule: '=',
              fieldId: '='
          },
          link: function ($scope, element, attrs, modelCtrl) {

              var maskSsn = $filter('maskSsn');

              element.on('focus', function () {
                  element.val($scope.relatedModule[attrs.name]);
              });

              element.on('blur', function () {
                  //element.$validate();
                  if (element.val().length > 0 && element.val().length != 9) {
                      $("#SSN_ERROR_MSG").html('Invalid SSN Number');
                      element.addClass('element-validation-error');
                  } else {
                      $("#SSN_ERROR_MSG").html('');
                      element.removeClass('element-validation-error');
                  }
                  $scope.relatedModule[attrs.name] = element.val();
                  element.val(maskSsn(element.val()));
              });
          }
      };
  }
);