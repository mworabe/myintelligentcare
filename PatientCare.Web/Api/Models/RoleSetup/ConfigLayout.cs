﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientCare.Web.Api.Models.RoleSetup
{
    public class ConfigLayout
    {
        public ConfigTab[] Tabs { get; set; }
        public ConfigTabContainer[] Containers { get; set; }
    }
}