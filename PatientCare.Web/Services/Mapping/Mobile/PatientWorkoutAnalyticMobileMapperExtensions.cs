﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.ApiMobile.Models.PatientWorkoutAnalytics;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class PatientWorkoutAnalyticMobileMapperExtensions
    {
        public static EmitMapperMapperProvider PatientWorkoutAnalyticMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientWorkoutAnalytic, PatientWorkoutAnalyticResponse>
                    (new ExtendedMapConfig<PatientWorkoutAnalytic, PatientWorkoutAnalyticResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientWorkoutAnalytic[], PatientWorkoutAnalyticResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientWorkoutAnalyticRequest, PatientWorkoutAnalytic>
                    (new ExtendedMapConfig<PatientWorkoutAnalyticRequest, PatientWorkoutAnalytic>()));

            return mapperProvider;
        }
    }
}