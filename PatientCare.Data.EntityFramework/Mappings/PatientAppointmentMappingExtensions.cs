﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PatientAppointmentMappingExtensions
    {
        public static DbModelBuilder MapPatientAppointment(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<PatientAppointment>().ToTable("patient_appointments")
                .HasKey(i => i.Id);

            modelBuilder.Entity<PatientAppointment>()
                 .Property(p => p.PatientId)
                 .HasColumnName("patient_id")
                 .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
                .HasOptional<Patient>(p => p.PatientName)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<PatientAppointment>()
                .Property(p => p.CaseId)
                .HasColumnName("case_id")
                .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
                .HasOptional<PatientCase>(p => p.Case)
                .WithMany()
                .HasForeignKey(p => p.CaseId);

            modelBuilder.Entity<PatientAppointment>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<PatientAppointment>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<PatientAppointment>()
                .Property(p => p.StartDate)
                .HasColumnName("start_date")
                .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
                 .Property(p => p.ClinicLocationId)
                 .HasColumnName("clinic_location_id")
                 .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
                .HasOptional<ClinicLocation>(p => p.ClinicLocation)
                .WithMany()
                .HasForeignKey(p => p.ClinicLocationId);

            modelBuilder.Entity<PatientAppointment>()
              .Property(p => p.AppointmentTypeId)
              .HasColumnName("appointment_type_id")
              .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
                 .HasOptional<AppointmentType>(p => p.AppointmentType)
                 .WithMany()
                 .HasForeignKey(p => p.AppointmentTypeId);

            modelBuilder.Entity<PatientAppointment>()
              .Property(p => p.ClinicianId)
              .HasColumnName("clinician_id")
              .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
                 .HasOptional<Resource>(p => p.Clinician)
                 .WithMany()
                 .HasForeignKey(p => p.ClinicianId);

            modelBuilder.Entity<PatientAppointment>()
               .Property(p => p.FacilityId)
               .HasColumnName("facility_id")
               .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
                .HasOptional<Clinic>(p => p.Facility)
                .WithMany()
                .HasForeignKey(p => p.FacilityId);

            modelBuilder.Entity<PatientAppointment>()
               .Property(p => p.AdditionalDetail)
               .HasColumnName("additional_detail")
               .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
               .Property(p => p.AppointmentStatus)
               .HasColumnName("appointment_status")
               .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
               .Property(p => p.IsMobile)
               .HasColumnName("is_mobile")
               .IsOptional();

            modelBuilder.Entity<PatientAppointment>()
               .Property(p => p.IsReschedule)
               .HasColumnName("is_reschedule")
               .IsOptional();

            return modelBuilder;
        }
    }
}
