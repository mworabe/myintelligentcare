﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using EmitMapper.MappingConfiguration;
using System.Linq;
using PatientCare.Web.ApiMobile.Models.HepMasters;
using PatientCare.Web.ApiMobile.Models.HepFrequencies;
using PatientCare.Web.ApiMobile.Models.HepDetails;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class HepDetailMobileMapperExtensions
    {
        public static EmitMapperMapperProvider HepDetailMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepDetail, HepDetailMobileResponse>
                (new ExtendedMapConfig<HepDetail, HepDetailMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepDetail[], HepDetailMobileResponse[]>());
            
            var hepFrequencyMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<HepFrequencyMobileRequest, HepFrequency>(
                  new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                 ObjectMapperManager.DefaultInstance.GetMapper<HepDetailsMobileRequest, HepDetail>
                    (new ExtendedMapConfig<HepDetailsMobileRequest, HepDetail>()
                        .ForMember(x => x.Frequencies, y => y.Frequencies != null ? y.Frequencies.Select(i => hepFrequencyMapper.Map(i)).ToList() : null)
                    ));

            return mapperProvider;
        }
    }
}