﻿using System;
using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Core.Enums;
using PatientCare.Core.Entities;

namespace PatientCare.Web.Api.Models.ResourceProfessionalOrganizations
{
    [DataContract]
    public class ResourceProfessionalOrganizationResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public string Organization { get; set; }

        [DataMember]
        public long? CommitteeMembershipId { get; set; }

        [DataMember]        
        public PicklistResponse CommitteeMembership { get; set; }
        //public virtual DropDownConfig CommitteeMembership { get; set; }

        [DataMember]
        public long? CommitteeTitleId { get; set; }

        [DataMember]
        public PicklistResponse  CommitteeTitle { get; set; }

    }
}