﻿/// <reference path="cell-view.html" />
"use strict";
var app = angular.module("EDZoutstaffingPortalApp");
app.directive("timeEntryCell", ["uuid2", "$uibModal", "Modal", "toaster", "$state",
    function (uuid2, $uibModal, modal, toaster, $state) {
        return {
            templateUrl: "Client/app/components/directives/TimeSheetEntryCellDirective/cell-view.html",
            restrict: "E",
            replace: false,
            scope: {
                dateList: "=ngDateList",
                date: "=ngDate",
                task: "=ngTask",
            },
            link: function (scope) {

                scope.currentTimeSpentSum = getTimeEntrySumByDate(scope.task, scope.date);


                Array.prototype.unique = function () {
                    var a = this.concat();
                    for (var i = 0; i < a.length; ++i) {
                        for (var j = i + 1; j < a.length; ++j) {
                            if (a[i] === a[j])
                                a.splice(j--, 1);
                        }
                    }
                    return a;
                }

                function getTimeEntrySumByDate(task, date) {
                    var sum = 0;
                    var originalDate = moment(date.originalDate).format("LL");

                    _.forEach(task.timeEntries, function (_timeEntry) {
                        var loggedDate = moment(_timeEntry.loggedDate).format("LL");
                        if (originalDate == loggedDate) {
                            sum += parseFloat(_timeEntry.timeSpent);
                        }
                    });
                    task["sum_" + moment(originalDate).format("YYMD")] = (typeof sum === "number") ? sum.toFixed(2) : 0;

                    return (sum > 0) ? sum.toFixed(2) : "&nbsp;";
                }
                function showListPopup(rowEntity, colDef) {

                    var modalInstance = $uibModal.open({
                        backdrop: 'static',
                        controller: "TimeEntryListModalCtrl",
                        controllerAs: "TimeEntryListModalCtrl",
                        templateUrl: "Client/app/controllers/time-management/add-time-entry/time-entry-list-modal-template.html",
                        size: "lg",
                        resolve: {
                            timeEntryData: function loadlogWorks() {
                                return {
                                    task: rowEntity,
                                    loggedDate: colDef.originalDate,
                                };
                            }
                        }
                    });
                    modalInstance.result.then(function (data) {
                        if (data) {
                            scope.currentTimeSpentSum = getTimeEntrySumByDate(scope.task, scope.date);
                        }
                    }, function () {
                        scope.currentTimeSpentSum = getTimeEntrySumByDate(scope.task, scope.date);
                        console.info('Modal dismissed at: ' + new Date());
                    });
                }
                function showLogWorkPopup(rowEntity, colDef) {
                    var modalInstance = $uibModal.open({
                        backdrop: 'static',
                        controller: "LogWorkModalController",
                        controllerAs: "LogWorkModalController",
                        templateUrl: "Client/app/controllers/projectTask/templates/time-entry-modal-template.html",
                        size: "md",
                        resolve: {
                            logworkData: function loadlogWorks() {
                                return {
                                    projectTaskId: rowEntity.id,
                                    logDate: colDef.originalDate,
                                };
                            }
                        }
                    });
                    modalInstance.result.then(function (data) {
                        if (data) {
                            if (!rowEntity.timeEntries) {
                                scope.task.timeEntries = [];
                            }
                            scope.task.timeEntries.push(data);
                            scope.currentTimeSpentSum = getTimeEntrySumByDate(scope.task, scope.date);
                            // $state.go($state.current, { startDate: new Date(document.getElementById("startDate").value), endDate: new Date(document.getElementById("endDate").value) }, { reload: true });
                        }
                    }, function () {
                        console.info('Modal dismissed at: ' + new Date());
                    });

                    /*
                     = new Date(document.getElementById("startDate").value); 
                     new Date(document.getElementById("endDate").value);
                    */
                }


                function onTableCellClick(rowEntity, colDef) {
                    var isToShowListPopup = false;

                    if (rowEntity.timeEntries && rowEntity.timeEntries.length > 0) {
                        var loggedDate = new Date(colDef.originalDate);
                        _.forEach(rowEntity.timeEntries, function (timeEntry) {
                            var rowDate = new Date(timeEntry.loggedDate);
                            if (rowDate.getDate() == loggedDate.getDate()) {
                                isToShowListPopup = true;
                            }
                        })
                    }

                    if (isToShowListPopup) {
                        showListPopup(rowEntity, colDef);
                    } else {
                        showLogWorkPopup(rowEntity, colDef);
                    }
                }

                scope.getTimeEntrySumByDate = getTimeEntrySumByDate;
                scope.onTableCellClick = onTableCellClick;

                scope.task.getTimeEntrySumByDate = getTimeEntrySumByDate;
            }
        };
    }]);