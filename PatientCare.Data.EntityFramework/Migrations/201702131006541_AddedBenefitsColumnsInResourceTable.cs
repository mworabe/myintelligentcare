namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBenefitsColumnsInResourceTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.resources", "rb_participating", c => c.Boolean());
            AddColumn("dbo.resources", "rb_eligible_wages", c => c.String());
            AddColumn("dbo.resources", "rb_employee_contribution", c => c.String());
            AddColumn("dbo.resources", "rb_employer_matching", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.resources", "rb_employer_matching");
            DropColumn("dbo.resources", "rb_employee_contribution");
            DropColumn("dbo.resources", "rb_eligible_wages");
            DropColumn("dbo.resources", "rb_participating");
        }
    }
}
