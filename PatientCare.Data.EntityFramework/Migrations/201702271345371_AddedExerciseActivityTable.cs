namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedExerciseActivityTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.exercise_activitys",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),

                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropTable("dbo.exercise_activitys");
        }
    }
}
