﻿using System.Collections.Generic;

namespace PatientCare.Core.Services.Dashboards
{
    public class ProjectsByPriorityViewModel
    {
        public IList<ProjectCountByDivision> Low { get; set; }
        public IList<ProjectCountByDivision> Medium { get; set; }
        public IList<ProjectCountByDivision> High { get; set; }
    }

    public struct ProjectCountByDivision
    {
        public ProjectCountByDivision(string name, int count) : this()
        {
            Name = name;
            Count = count;
        }

        public string Name { get; private set; }
        public int Count { get; private set; }
    }
}
