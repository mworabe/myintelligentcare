﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDZoutstaffingportal.EmailNotificationSender
{
    public interface ISender
    {
        void Send();
    }
}
