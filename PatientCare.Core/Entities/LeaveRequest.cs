﻿using PatientCare.Core.Entities.Abstract;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Leave Request.
    /// </summary>
    public class LeaveRequest : Entity
    {
        public long? LeaveTypeId { get; set; }
        public virtual LeaveType LeaveType { get; set; }

        public long? SupervisorId { get; set; }
        public virtual Resource Supervisor { get; set; }

        public long? ResourceId { get; set; }
        public virtual Resource Resource { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DateTime? StatusModifiedDate { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

        public string CommentFromSupervisor { get; set; }        

        public long? TotalTimeOffDays { get; set; }
    }
}
