﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("UIConfigService", ["$resource", function ($resource) {
    //http://localhost:51121/api/uiconfiguration/(id)2
    var resource = $resource("api/uiconfiguration/:id/:controller",
        null, //parameters default
        {
            'update': { method: "PUT" },
            'query': { method: "GET", params: {}, isArray: false },
            'saveSystemConfigurations': { method: "POST", params: { controller: "saveSystemConfigurations" }, isArray: false },
            'getSystemConfigurationisByKey': { method: "GET", params: {}, isArray: false },
        });

    resource.getTableOption = {
        pageName: "Skills",
        cols: function () {
            return [
                { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "name", title: "Name", sortable: "name", show: true, dataType: "text", required: true },
                { field: "action", title: ".", dataType: "command" },
            ];
        }
    };
    return resource;
}
]);