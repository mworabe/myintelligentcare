namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedPatientLIstSP : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
           "dbo.sp_patient_detail_list",
           p => new
           {
               pageIndex = p.Int(defaultValueSql: null),
               pageSize = p.Int(defaultValueSql: null),
               responseType = p.String(defaultValueSql: null),
               sortField = p.String(defaultValueSql: null),
               sortOrder = p.String(defaultValueSql: null),
               searchPhrase = p.String(defaultValueSql: null)
           },
           body:
             @" IF(@responseType = 'DataCount')
                BEGIN
                select CAST(Count(distinct pat.Id) AS  BIGINT) AS ItemCount 
		                from patients as pat where pat.is_deleted IS NULL	      
                END
                IF(@responseType = 'Data')
                BEGIN
                select maintable.Id,maintable.LastName,maintable.FirstName,maintable.Name,maintable.PatientNumber,maintable.BirthDate,maintable.photo_file_name,
                maintable.Status,maintable.PreExercisesPainRating,maintable.CaseId,maintable.CaseNo,maintable.LastVisitDate,
                maintable.InjuryRegion,maintable.UserId,hepmas.Id as HepId from (
                select  pat.Id,pat.last_name as LastName,pat.first_name as FirstName,(pat.last_name +' '+ pat.first_name) as Name,pat.patient_number as PatientNumber,pat.birth_date as BirthDate,
                pat.photo_file_name,pat.status as Status,
                (select Top 1 pre_exercises_pain_rating as PreExercisesPainRating from patient_workout_analytics where patient_id = pat.Id order by start_date desc) as PreExercisesPainRating,
                (select Top 1 patcase.Id as CaseId from patient_cases as patcase left join injury_regions as injregion on patcase.injury_region_id = injregion.id	 where patcase.patient_id = pat.Id) as CaseId,
                (select Top 1 patcase.case_no as CaseNo	 from patient_cases as patcase left join injury_regions as injregion on patcase.injury_region_id = injregion.id	 where patcase.patient_id = pat.Id) as CaseNo,
                (select Top 1 patcase.return_to as LastVisitDate	from patient_cases as patcase left join injury_regions as injregion on patcase.injury_region_id = injregion.id	 where patcase.patient_id = pat.Id) as LastVisitDate,
                (select Top 1 injregion.name as InjuryRegion from patient_cases as patcase left join injury_regions as injregion on patcase.injury_region_id = injregion.id	 where patcase.patient_id = pat.Id) as InjuryRegion,
                (select TOP 1 Id  as UserId from AspNetUsers where PatientId = pat.Id) as UserId
                from patients as pat where pat.is_deleted IS NULL) as maintable 
                left join hep_master as hepmas on maintable.Id = hepmas.patient_id and maintable.CaseId = hepmas.patient_case_id
                where (( maintable.Name like ('%'+@searchPhrase+'%')) 
                or (maintable.PatientNumber like ('%'+@searchPhrase+'%')) 
                or (maintable.CaseNo like ('%'+@searchPhrase+'%'))
                or (maintable.InjuryRegion like ('%'+@searchPhrase+'%'))
                or (convert(varchar(50),maintable.LastVisitDate,126) like ('%'+@searchPhrase+'%')) 
                --or (convert(datetime,maintable.LastVisitDate,103) like ('%'+@search+'%'))   --convert(nvarchar(50), OrderDate,126) LIKE '1996-07- %'
                )
                order by
                case
                when @sortOrder <> 'Ascending' then 0
                when @sortField = 'Id' then maintable.Id
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'Name' then maintable.Name --(maintable.last_name +' '+ maintable.first_name)
                end ASC,
                case
                when @sortOrder <> 'Ascending' then cast(null as date)
                when @sortField = 'DOB' then  maintable.BirthDate --maintable.birth_date
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'CaseNo' then maintable.CaseNo
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'InjuryRegion' then maintable.InjuryRegion
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'LastVisitDate' then maintable.LastVisitDate
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'PreExercisesPainRating' then maintable.PreExercisesPainRating
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'Status' then maintable.Status
                end ASC,

                case
                when @sortOrder <> 'Descending' then 0
                when @sortField = 'Id' then maintable.Id
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'Name' then  maintable.Name  --(maintable.last_name +' '+ pat.first_name)
                end DESC,
                case
                when @sortOrder <> 'Descending' then cast(null as date)
                when @sortField = 'DOB' then maintable.BirthDate --maintable.birth_date
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'CaseNo' then maintable.CaseNo
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'InjuryRegion' then maintable.InjuryRegion
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'LastVisitDate' then maintable.LastVisitDate
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'PreExercisesPainRating' then maintable.PreExercisesPainRating
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'Status' then maintable.Status
                end DESC
                OFFSET(@pageIndex * @pageSize) ROWS FETCH NEXT @pageSize ROWS ONLY

                select CAST(Count(distinct pat.Id) AS  BIGINT) AS ItemCount 
                from patients as pat where pat.is_deleted IS NULL
                END "
                    );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_patient_detail_list");
        }
    }
}
