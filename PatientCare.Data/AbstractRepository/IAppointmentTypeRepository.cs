﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IAppointmentTypeRepository : IRepository<AppointmentType>
    {
    }
}
