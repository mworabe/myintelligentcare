﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class HepFrequencyMappingExtensions
    {
        public static DbModelBuilder MapHepFrequency(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<HepFrequency>().ToTable("hep_exercise_frequency")
                .HasKey(i => i.Id);

            modelBuilder.Entity<HepFrequency>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<HepFrequency>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();
            
            modelBuilder.Entity<HepFrequency>()
                .Property(p => p.Day)
                .HasColumnName("day")
                .IsOptional();

            modelBuilder.Entity<HepFrequency>()
                .Property(p => p.ExerciseFrequency)
                .HasColumnName("exercise_frequency")
                .IsOptional();

            modelBuilder.Entity<HepFrequency>()
               .Property(p => p.HepId)
               .HasColumnName("hep_id")
               .IsOptional();

            modelBuilder.Entity<HepFrequency>()
                .HasOptional<HepMaster>(p => p.Hep)
                .WithMany()
                .HasForeignKey(p => p.HepId);

            modelBuilder.Entity<HepFrequency>()
                .Property(p => p.HepDetailId)
                .HasColumnName("hep_detail_id")
                .IsOptional();

            modelBuilder.Entity<HepFrequency>()
                .HasOptional<HepDetail>(p => p.HepDetail)
                .WithMany()
                .HasForeignKey(p => p.HepDetailId);

            modelBuilder.Entity<HepFrequency>()
                .Property(p => p.IsActive)
                .HasColumnName("is_active")
                .IsOptional();
            return modelBuilder;
        }
    }
}
