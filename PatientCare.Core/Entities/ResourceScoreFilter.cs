﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Score Filter.
    /// </summary>
    public class ResourceScoreFilter : NamedEntity
    {
        public string JsonFilter { get; set; }

        public string UserId { get; set; }

        public string FilterType { get; set; }
    }
}
