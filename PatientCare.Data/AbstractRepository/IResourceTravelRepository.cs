﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceTravelRepository : IRepository<ResourceTravel>
    {
    }
}
