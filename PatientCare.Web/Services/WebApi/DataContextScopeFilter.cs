﻿using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Autofac.Integration.WebApi;
using PatientCare.Data.AbstractDataContext;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Practices.ServiceLocation;

namespace PatientCare.Web.Services.WebApi
{
    internal sealed class DataContextScopeFilter : IAutofacActionFilter
    {
        private IDataContextScope _dataContextScope;

        public void OnActionExecuting(HttpActionContext actionContext)
        {
            var owinDataContextScope = actionContext.Request.GetOwinContext().Get<IDataContextScope>();
            var ambientContextLocator = ServiceLocator.Current.GetInstance<IAmbientDataContextLocator>();
            _dataContextScope = ambientContextLocator.Get<IDataContextScope>();
            if (_dataContextScope == null)
            {
                ambientContextLocator.Set(owinDataContextScope);
                _dataContextScope = owinDataContextScope;
            }
        }

        public void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            _dataContextScope.SaveChanges();
            _dataContextScope.Dispose();
        }
    }
}
