﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class AppointmentTypeMappingExtensions
    {
        public static DbModelBuilder MapAppointmentType(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<AppointmentType>().ToTable("appointment_types").HasKey(i => i.Id);

            modelBuilder.Entity<AppointmentType>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<AppointmentType>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<AppointmentType>()
              .Property(p => p.Color)
              .HasColumnName("color")
              .HasMaxLength(20)
              .IsOptional();

            modelBuilder.Entity<AppointmentType>()
                .Property(p => p.Name)
                .HasColumnName("Name")
                .IsOptional();

            return modelBuilder;
        }
    }
}
