// <auto-generated />
namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AlterSchedulerStoreProcedureFilterByClinic : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AlterSchedulerStoreProcedureFilterByClinic));
        
        string IMigrationMetadata.Id
        {
            get { return "201703241314000_AlterSchedulerStoreProcedureFilterByClinic"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
