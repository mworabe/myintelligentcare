﻿using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.ResourceTravels
{
    public class ResourceTravelRequest
    {
        public long Id { get; set; }

        public long ResourceId { get; set; }

        public VisaOrPermitEnum VisaOrPermit { get; set; }

        public long CountryId { get; set; }

        public int TimeRemainingInDays { get; set; }

        public string SpecialConsiderations { get; set; }
    }
}