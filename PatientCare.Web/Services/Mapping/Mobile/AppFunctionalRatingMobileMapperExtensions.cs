﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.Ratings;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class AppFunctionalRatingMobileMapperExtensions
    {
        public static EmitMapperMapperProvider AppFunctionalRatingMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<AppFunctionalRating, AppFunctionalRatingMobileResponse>
                    (new ExtendedMapConfig<AppFunctionalRating, AppFunctionalRatingMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<AppFunctionalRating[], AppFunctionalRatingMobileResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<AppFunctionalRatingMobileRequest, AppFunctionalRating>(
                        new ExtendedMapConfig<AppFunctionalRatingMobileRequest, AppFunctionalRating>()
                    ));

            return mapperProvider;
        }
    }
}