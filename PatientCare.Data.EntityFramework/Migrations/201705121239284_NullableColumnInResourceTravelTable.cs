namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableColumnInResourceTravelTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.resource_travels", "country_id", "dbo.countries");
            DropIndex("dbo.resource_travels", new[] { "country_id" });
            AlterColumn("dbo.resource_travels", "visa_or_permit", c => c.Int());
            AlterColumn("dbo.resource_travels", "country_id", c => c.Long());
            AlterColumn("dbo.resource_travels", "time_remaining_in_days", c => c.Int());
            AlterColumn("dbo.resource_travels", "special_considerations", c => c.String());
            CreateIndex("dbo.resource_travels", "country_id");
            AddForeignKey("dbo.resource_travels", "country_id", "dbo.countries", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.resource_travels", "country_id", "dbo.countries");
            DropIndex("dbo.resource_travels", new[] { "country_id" });
            AlterColumn("dbo.resource_travels", "special_considerations", c => c.String(maxLength: 1023));
            AlterColumn("dbo.resource_travels", "time_remaining_in_days", c => c.Int(nullable: false));
            AlterColumn("dbo.resource_travels", "country_id", c => c.Long(nullable: false));
            AlterColumn("dbo.resource_travels", "visa_or_permit", c => c.Int(nullable: false));
            CreateIndex("dbo.resource_travels", "country_id");
            AddForeignKey("dbo.resource_travels", "country_id", "dbo.countries", "Id", cascadeDelete: true);
        }
    }
}
