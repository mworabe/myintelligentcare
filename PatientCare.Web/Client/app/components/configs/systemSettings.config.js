﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config([
        "$stateProvider", "$urlRouterProvider", "$locationProvider", "dashboardProvider", "$httpProvider", "cfpLoadingBarProvider",
        function ($stateProvider, $urlRouterProvider, $locationProvider, dashboardProvider, $httpProvider, cfpLoadingBarProvider) {

            cfpLoadingBarProvider.includeSpinner = false;
            dashboardProvider.widgetsPath("Client/app/components/dashboardWidgets");
            $locationProvider.html5Mode(true);
            $httpProvider.interceptors.push("interceptor");
            $urlRouterProvider.otherwise("/");
            moment.fn.toJSON = function () {
                return  this.add(this.utcOffset(), "minutes").format();
            }
            Date.prototype.toJSON = function() {
                var currMomentDate = moment(this);
                return currMomentDate.add(currMomentDate.utcOffset(), "minutes").format();
            }
        }
    ]);