﻿using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using PatientCare.Data.EntityFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle Helth care plan operations And implementing all methods which is initialize in IHepService.
    /// </summary>
    public class HepService : IHepService
    {
        /// <summary>
        ///Description : Hep Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Hep Service Field IHepGroupRepository
        /// </summary>
        private readonly IHepGroupRepository _hepGroupRepository;

        /// <summary>
        ///Description : Hep Service Field IHepMasterRepository
        /// </summary>
        private readonly IHepMasterRepository _hepMasterRepository;

        /// <summary>
        ///Description : Hep Service Field IHepDetailRepository
        /// </summary>
        private readonly IHepDetailRepository _hepDetailRepository;

        /// <summary>
        ///Description : Hep Service Field IHepFrequencyRepository
        /// </summary>
        private readonly IHepFrequencyRepository _hepFrequencyRepository;

        /// <summary>
        ///Description : Hep Service Field IHepPatientPlanRepository
        /// </summary>
        private readonly IHepPatientPlanRepository _hepPatientPlanRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Hep class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="hepGroupRepository">Hep Group Repository</param>
        /// <param name="hepMasterRepository">Hep Master Repository</param>
        /// <param name="hepDetailRepository">Hep Detail Repository</param>
        /// <param name="hepFrequencyRepository">Hep Frequency Repository</param>
        /// <param name="hepPatientPlanRepository">Hep Patient Plan Repository</param>
        public HepService
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IHepGroupRepository hepGroupRepository,
                IHepMasterRepository hepMasterRepository,
                IHepDetailRepository hepDetailRepository,
                IHepFrequencyRepository hepFrequencyRepository,
                IHepPatientPlanRepository hepPatientPlanRepository

            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _hepGroupRepository = hepGroupRepository;
            _hepMasterRepository = hepMasterRepository;
            _hepDetailRepository = hepDetailRepository;
            _hepFrequencyRepository = hepFrequencyRepository;
            _hepPatientPlanRepository = hepPatientPlanRepository;
        }

        /// <summary>
        /// Method Description : Using for Create new Hep data.
        /// </summary>
        /// <param name="hepmaster">HepMaster Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateAsync(HepMaster hepmaster)
        {
            if (hepmaster == null) throw new ArgumentNullException("HepMaster");

            await _hepMasterRepository.CreateAsync(hepmaster, UpdateOptions.Create<HepMaster>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Hep by Id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>HepMaster Entity</returns>
        public async Task<HepMaster> ReadAsync(long id)
        {
            var readOptions = new ReadOptions<HepMaster>().AsReadOnly();

            var hepMaster = await _hepMasterRepository.ReadAsync(id, readOptions);

            return hepMaster;

        }

        /// <summary>
        /// Method Description : Using for get Hep by case Id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>HepMaster Entity</returns>
        public async Task<HepMaster> ReadByCaseAsync(long id)
        {
            var readOptions = new ReadOptions<HepMaster>().AsReadOnly();

            var exercisemastertemplateitem = (await _hepMasterRepository.ListAsync(
                   new FilterQuery<HepMaster>().AddFilter(x => x.PatientCaseId == id),
                   new ReadOptions<HepMaster>().AsReadOnly()))
                   .ToList();

            var hepMaster = await _hepMasterRepository.ReadAsync(id, readOptions);

            return hepMaster;

        }

        /// <summary>
        /// Method Description : Using for Update Hep data.
        /// </summary>
        /// <param name="hepmaster">HepMaster Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateAsync(HepMaster hepmaster)
        {
            if (hepmaster == null) throw new ArgumentNullException("resource");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                HepChidlrenProcessing(ctx, hepmaster);

                //var originHep = _hepMasterRepository.Read(hepmaster.Id, new ReadOptions<HepMaster>().AsReadOnly());

                var hepDetailSer = ctx.Set<HepDetail>();
                if (hepmaster.HepDetails.Count > 0)
                {
                    foreach (HepDetail hepDetail in hepmaster.HepDetails)
                    {
                        hepDetail.HepId = hepmaster.Id;
                        hepDetail.Exercise = null;
                        hepDetail.Frequencies = null;
                        hepDetailSer.Attach(hepDetail);
                        ctx.Entry(hepDetail).State = hepDetail.Id > 0 ? EntityState.Modified : EntityState.Added;
                    }
                }



                await _hepMasterRepository.UpdateAsync(hepmaster, UpdateOptions.Create<HepMaster>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Delete Hep data.
        /// </summary>
        /// <param name="hepmaster">HepMaster Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<string> DeleteAsync(HepMaster hepmaster)
        {
            var allowDeleting = true;
            var resourceId = hepmaster.Id;
            var sb = new StringBuilder(string.Format("You can't delete {0} because :", hepmaster.Patient));

            var hepDetailItem = await _hepDetailRepository.ListAsync(new FilterQuery<HepDetail>().AddFilter(x => x.HepId == hepmaster.Id));
            DeletetestChildItem(hepDetailItem);

            if (allowDeleting)
            {
                await _hepMasterRepository.DeleteAsync(hepmaster);
                return "";
            }

            sb.Replace(Environment.NewLine, "<br/>");

            return sb.ToString();
        }

        /// <summary>
        /// Method Description : Using for Delete Hep Detail.
        /// </summary>
        /// <param name="HepDetailItem">Hep Detail Entity</param>
        /// <returns>Returns True/False</returns>
        private bool DeletetestChildItem(IEnumerable<HepDetail> HepDetailItem)
        {
            foreach (var HepDetailsItem in HepDetailItem)
            {
                _hepDetailRepository.Delete(HepDetailsItem);
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Update Hep Detail.
        /// </summary>
        /// <param name="ctx">DbContext</param>
        /// <param name="hepmaster">Hep Master Entity</param>
        private static void HepChidlrenProcessing(DbContext ctx, HepMaster hepmaster)
        {
            var hepDetails = hepmaster.HepDetails;
            ctx.ChildrenProcessingHepFrequency(hepDetails, x => x.HepId == hepmaster.Id);

            var hepGroups = hepmaster.HepGroups;
            ctx.ChildrenProcessingHepFrequency(hepGroups, x => x.HepId == hepmaster.Id);
        }

        /// <summary>
        /// Method Description : Using for Create / Update Hep.
        /// </summary>
        /// <param name="hepmaster">Hep Master Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> InsertUpdateHepAsync(HepMaster hepmaster)
        {
            if (hepmaster == null) throw new ArgumentNullException("HepMaster");

            if (hepmaster.Id == 0)
            {
                await _hepMasterRepository.CreateAsync(hepmaster, UpdateOptions.Create<HepMaster>()
                       .WithNotModified(p => p.Created)
                       .WithNotModified(p => p.Modified));
            }
            else
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                    var originHep = _hepMasterRepository.Read(hepmaster.Id, new ReadOptions<HepMaster>().AsReadOnly());

                    #region All Id List

                    var hepDetailList = new List<long>();
                    var hepGroupList = new List<long>();
                    var hepGroupDetailList = new List<long>();

                    if (hepmaster.HepDetails != null)
                    {
                        foreach (var hepDetail in hepmaster.HepDetails)
                        {
                            if (hepDetail.Id != 0)
                            {
                                hepDetailList.Add(hepDetail.Id);
                            }
                        }
                    }

                    if (hepmaster.HepGroups != null)
                    {
                        foreach (var hepGroup in hepmaster.HepGroups)
                        {
                            if (hepGroup.Id != 0)
                            {
                                hepGroupList.Add(hepGroup.Id);
                            }
                        }

                        foreach (var hepGroups in hepmaster.HepGroups)
                        {
                            foreach (HepDetail hepDetail in hepGroups.HepDetails)
                            {
                                if (hepDetail.Id != 0)
                                {
                                    hepGroupDetailList.Add(hepDetail.Id);
                                }
                            }
                        }
                    }
                    #endregion

                    #region Delete Code

                    foreach (var hepDetail in originHep.HepDetails.Where(x => x.GroupId == null))
                    {
                        if (hepDetail.Id != 0)
                        {
                            //bool isInList = hepDetailList.IndexOf(hepDetail.Id) != -1;
                            var existsInHepDetail = hepDetailList.Contains(hepDetail.Id);
                            var existsInGroupHepDetail = hepGroupDetailList.Contains(hepDetail.Id);
                            if (existsInHepDetail == false && existsInGroupHepDetail == false)
                            {
                                var item = _hepDetailRepository.Read(hepDetail.Id);
                                var errorMessage = await DeleteDetailAsync(item);
                            }
                        }
                    }

                    foreach (var hepGroups in originHep.HepGroups)
                    {
                        if (hepGroups.Id != 0)
                        {
                            var exists = hepGroupList.Contains(hepGroups.Id);
                            if (exists == false)
                            {
                                var item = _hepGroupRepository.Read(hepGroups.Id);

                                //foreach (var i in item.HepDetails.ToList())
                                //{
                                //    var hepDetailId = i.Id;
                                //    using (var dbContextScopehepGroup = _dataContextScopeFactory.Create())
                                //    {
                                //        var ctx2 = dbContextScopehepGroup.DataContexts.Get<PatientCareDbContext>();
                                //        //var hepDetailUpdateData = ctx2.HepDetails.FirstOrDefault(x => x.Id == hepDetailId);
                                //        var hepDetailUpdateData = ctx2.HepDetails.Find(i.Id);
                                //        if (hepDetailUpdateData == null) return false;
                                //        //string query = "declare @returnData as int;EXEC @returnData= sp_Hep_Detail_Updated @GroupId = " + null + ", @HepId = " + i.HepId + ", @Id = " + hepDetailId + "; select @returnData;";
                                //        string query = "declare @returnData as int;EXEC @returnData= sp_Hep_Detail_Updated @HepId = " + i.HepId + ", @Id = " + hepDetailId + "; select @returnData;";
                                //        var result = await ctx.Database.SqlQuery<int>(query).FirstOrDefaultAsync();

                                //        //hepDetailUpdateData.HepId = i.HepId;
                                //        //hepDetailUpdateData.GroupId = null;
                                //        ////hepDetailUpdateData.Modified = DateTime.UtcNow;
                                //        //ctx2.SaveChanges();
                                //        item.HepDetails.Remove(i);
                                //    }
                                //}
                                //var errorMessage = await DeletehepGroupAsync(item.Id);

                                var errorMessage = await DeleteGroupAsync(item);
                            }
                        }
                    }

                    foreach (var hepGroup in originHep.HepGroups)
                    {
                        foreach (var hepDetails in hepGroup.HepDetails)
                        {
                            if (hepDetails.Id != 0)
                            {
                                //bool isInList = hepGroupDetailList.IndexOf(hepDetail.Id) != -1;
                                var existsInHepDetail = hepDetailList.Contains(hepDetails.Id);
                                var existsInGroupHepDetail = hepGroupDetailList.Contains(hepDetails.Id);
                                if (existsInHepDetail == false && existsInGroupHepDetail == false)
                                {
                                    var item = _hepDetailRepository.Read(hepDetails.Id);
                                    var errorMessage = await DeleteDetailAsync(item);
                                }
                            }
                        }
                    }

                    var originhepMaster = _hepMasterRepository.Read(hepmaster.Id, new ReadOptions<HepMaster>().AsReadOnly());
                    if (originhepMaster.HepDetails.Count == 0 && originhepMaster.HepGroups.Count == 0 && hepmaster.HepDetails.Count == 0 && hepmaster.HepGroups.Count == 0)
                    {
                        using (var dbContextScope2 = _dataContextScopeFactory.Create())
                        {
                            var ctx2 = dbContextScope2.DataContexts.Get<PatientCareDbContext>();
                            var DBset = ctx2.Set<HepMaster>();
                            var hepMasterDataDelete = ctx2.HepMasters.FirstOrDefault(x => x.Id == originhepMaster.Id);
                            DBset.Remove(hepMasterDataDelete);
                            ctx2.SaveChanges();
                        }
                        //var errorMessage = await DeleteAsync(originhepMaster);
                    }

                    #endregion

                    #region Update Code

                    if (hepmaster.HepDetails != null)
                    {
                        foreach (HepDetail hepDetail in hepmaster.HepDetails.ToList())
                        {
                            if (hepDetail != null)
                            {
                                if (hepDetail.Id == 0)
                                {
                                    //Create New Hep Detail
                                    hepDetail.HepId = hepmaster.Id;
                                    //var dbSet = ctx.Set<>();

                                    await _hepDetailRepository.CreateAsync(hepDetail, UpdateOptions.Create<HepDetail>()
                                            .WithNotModified(p => p.Created)
                                            .WithNotModified(p => p.Modified));
                                    hepmaster.HepDetails.Remove(hepDetail);
                                }
                                else
                                {
                                    foreach (HepFrequency hepFrequency in hepDetail.Frequencies.ToList())
                                    {
                                        if (hepFrequency != null)
                                        {
                                            if (hepFrequency.Id == 0)
                                            {
                                                //Create New Frequency
                                                hepFrequency.HepId = hepmaster.Id;
                                                hepFrequency.HepDetailId = hepDetail.Id;
                                                await _hepFrequencyRepository.CreateAsync(hepFrequency, UpdateOptions.Create<HepFrequency>()
                                                       .WithNotModified(p => p.Created)
                                                       .WithNotModified(p => p.Modified));
                                                hepDetail.Frequencies.Remove(hepFrequency);
                                            }
                                            else
                                            {
                                                //Update New Frequency
                                                //var hepDetailUpdate = ctx.HepDetails.FirstOrDefault(x => x.Id == hepDetail.Id);
                                                //ctx.SaveChanges();
                                                hepFrequency.HepId = hepmaster.Id;
                                                hepFrequency.HepDetailId = hepDetail.Id;
                                                await _hepFrequencyRepository.UpdateAsync(hepFrequency, UpdateOptions.Create<HepFrequency>()
                                                      .WithNotModified(p => p.Created));
                                                hepDetail.Frequencies.Remove(hepFrequency);
                                            }
                                        }
                                    }
                                    //Update Hep Detail
                                    hepDetail.HepId = hepmaster.Id;
                                    hepDetail.GroupId = null;
                                    //await _hepDetailRepository.UpdateAsync(hepDetail, UpdateOptions.Create<HepDetail>()
                                    //       .WithNotModified(p => p.Created));
                                    _hepDetailRepository.Update(hepDetail, UpdateOptions.Create<HepDetail>()
                                           .WithNotModified(p => p.Created));
                                    hepmaster.HepDetails.Remove(hepDetail);
                                }
                            }
                        }
                    }

                    if (hepmaster.HepGroups != null)
                    {
                        foreach (HepGroup hepGroup in hepmaster.HepGroups.ToList())
                        {
                            if (hepGroup != null)
                            {
                                if (hepGroup.Id == 0)
                                {
                                    //Create New Group
                                    hepGroup.HepId = hepmaster.Id;
                                    foreach (var hepDetail in hepGroup.HepDetails)
                                    {
                                        if (hepDetail.Id > 0)
                                            using (var dbContext = _dataContextScopeFactory.Create())
                                            {
                                                var ctxx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                                                ctxx.HepDetails.Attach(hepDetail);
                                            }
                                    }

                                    await _hepGroupRepository.CreateAsync(hepGroup, UpdateOptions.Create<HepGroup>()
                                            .WithNotModified(p => p.Created)
                                            .WithNotModified(p => p.Modified));
                                    hepmaster.HepGroups.Remove(hepGroup);
                                }
                                else
                                {
                                    foreach (HepDetail hepDetail in hepGroup.HepDetails.ToList())
                                    {
                                        if (hepDetail != null)
                                        {
                                            if (hepDetail.Id == 0)
                                            {
                                                //Create New Hep Detail
                                                //hepDetail.HepId = hepmaster.Id;
                                                hepDetail.GroupId = hepGroup.Id;
                                                await _hepDetailRepository.CreateAsync(hepDetail, UpdateOptions.Create<HepDetail>()
                                                        .WithNotModified(p => p.Created)
                                                        .WithNotModified(p => p.Modified));
                                                hepGroup.HepDetails.Remove(hepDetail);
                                            }
                                            else
                                            {
                                                foreach (HepFrequency hepFrequency in hepDetail.Frequencies.ToList())
                                                {
                                                    if (hepFrequency != null)
                                                    {
                                                        if (hepFrequency.Id == 0)
                                                        {
                                                            //Create New Frequency
                                                            hepFrequency.HepId = hepmaster.Id;
                                                            hepFrequency.HepDetailId = hepDetail.Id;
                                                            await _hepFrequencyRepository.CreateAsync(hepFrequency, UpdateOptions.Create<HepFrequency>()
                                                                   .WithNotModified(p => p.Created)
                                                                   .WithNotModified(p => p.Modified));
                                                            hepDetail.Frequencies.Remove(hepFrequency);
                                                        }
                                                        else
                                                        {
                                                            //Update Frequency
                                                            hepFrequency.HepId = hepmaster.Id;
                                                            hepFrequency.HepDetailId = hepDetail.Id;
                                                            await _hepFrequencyRepository.UpdateAsync(hepFrequency, UpdateOptions.Create<HepFrequency>()
                                                                 .WithNotModified(p => p.Created));
                                                            hepDetail.Frequencies.Remove(hepFrequency);
                                                        }
                                                    }
                                                }
                                                //Update Hep Detail
                                                //hepDetail.HepId = hepmaster.Id;
                                                hepDetail.GroupId = hepGroup.Id;
                                                await _hepDetailRepository.UpdateAsync(hepDetail, UpdateOptions.Create<HepDetail>()
                                                     .WithNotModified(p => p.Created));
                                                hepGroup.HepDetails.Remove(hepDetail);
                                            }
                                        }
                                    }
                                    //Update Group
                                    hepGroup.HepId = hepmaster.Id;
                                    await _hepGroupRepository.UpdateAsync(hepGroup, UpdateOptions.Create<HepGroup>()
                                         .WithNotModified(p => p.Created));
                                    hepmaster.HepGroups.Remove(hepGroup);
                                }
                            }
                        }
                    }

                    HepMaster hepMasterUpdate = new HepMaster();
                    hepMasterUpdate.Id = hepmaster.Id;
                    hepMasterUpdate.PatientId = (hepmaster.PatientId != null) ? hepmaster.PatientId : 0;
                    hepMasterUpdate.OwnerId = (hepmaster.OwnerId != null) ? hepmaster.OwnerId : 0;
                    hepMasterUpdate.PatientCaseId = (hepmaster.PatientCaseId != null) ? hepmaster.PatientCaseId : 0;
                    hepMasterUpdate.StartDate = (hepmaster.StartDate != null) ? hepmaster.StartDate : null;
                    hepMasterUpdate.EndDate = (hepmaster.EndDate != null) ? hepmaster.EndDate : null;
                    hepMasterUpdate.Modified = DateTime.UtcNow;
                    hepMasterUpdate.IsSaveAsDraft = hepmaster.IsSaveAsDraft;

                    //Update Hep Msater

                    using (var dbContextScope2 = _dataContextScopeFactory.Create())
                    {
                        var ctx2 = dbContextScope2.DataContexts.Get<PatientCareDbContext>();
                        var hepMasterUpdateData = ctx2.HepMasters.FirstOrDefault(x => x.Id == hepMasterUpdate.Id);
                        if (hepMasterUpdateData == null) return false;

                        hepMasterUpdateData.StartDate = hepMasterUpdate.StartDate;
                        hepMasterUpdateData.EndDate = hepMasterUpdate.EndDate;
                        hepMasterUpdateData.Modified = hepMasterUpdate.Modified;
                        hepMasterUpdateData.IsSaveAsDraft = hepMasterUpdate.IsSaveAsDraft;
                        ctx2.SaveChanges();
                    }

                    #endregion
                }
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for delete Hep group.
        /// </summary>
        /// <param name="hepgroup">Hep Group Entity</param>
        /// <returns></returns>
        public async Task<string> DeleteGroupAsync(HepGroup hepgroup)
        {
            var allowDeleting = true;
            var resourceId = hepgroup.Id;
            var sb = new StringBuilder(string.Format("You can't delete {0} because :", hepgroup.Patient));

            foreach (HepDetail hepDetail in hepgroup.HepDetails.ToList())
            {
                var hepFrequencyItem = await _hepFrequencyRepository.ListAsync(new FilterQuery<HepFrequency>().AddFilter(x => x.HepDetailId == hepDetail.Id));
                DeleteDetailChildItem(hepFrequencyItem);
                await _hepDetailRepository.DeleteAsync(hepDetail);
                hepgroup.HepDetails.Remove(hepDetail);
            }

            if (allowDeleting)
            {
                await _hepGroupRepository.DeleteAsync(hepgroup);
                return "";
            }

            sb.Replace(Environment.NewLine, "<br/>");

            return sb.ToString();
        }

        /// <summary>
        /// Method Description : Using for delete Hep Detail.
        /// </summary>
        /// <param name="hepdetail">Hep Detail Entity</param>
        /// <returns></returns>
        public async Task<string> DeleteDetailAsync(HepDetail hepdetail)
        {
            var allowDeleting = true;
            var resourceId = hepdetail.Id;
            var sb = new StringBuilder(string.Format("You can't delete {0} because :", hepdetail));

            var hepFrequencyItem = await _hepFrequencyRepository.ListAsync(new FilterQuery<HepFrequency>().AddFilter(x => x.HepDetailId == hepdetail.Id));
            DeleteDetailChildItem(hepFrequencyItem);

            if (allowDeleting)
            {
                await _hepDetailRepository.DeleteAsync(hepdetail);
                return "";
            }

            sb.Replace(Environment.NewLine, "<br/>");

            return sb.ToString();
        }

        /// <summary>
        /// Method Description : Using for delete Hep Frequency.
        /// </summary>
        /// <param name="hepFrequencyItem">HepFrequency Entity</param>
        /// <returns>Returns True/False</returns>
        private bool DeleteDetailChildItem(IEnumerable<HepFrequency> hepFrequencyItem)
        {
            foreach (var hepFrequencies in hepFrequencyItem)
            {
                _hepFrequencyRepository.Delete(hepFrequencies);
            }
            return true;
        }

        #region Hep Plan Patient

        /// <summary>
        /// Method Description : Using for Create / Update Hep Patient Plan.
        /// </summary>
        /// <param name="hepPatientsPlan">Hep Patients Plan Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> InsertUpdateHepPatientPlanAsync(HepPatientsPlan hepPatientsPlan)
        {
            if (hepPatientsPlan == null) throw new ArgumentNullException("HepMaster");

            if (hepPatientsPlan.Id == 0)
            {
                await _hepPatientPlanRepository.CreateAsync(hepPatientsPlan, UpdateOptions.Create<HepPatientsPlan>()
                       .WithNotModified(p => p.Created)
                       .WithNotModified(p => p.Modified));
            }
            else
            {
                await _hepPatientPlanRepository.UpdateAsync(hepPatientsPlan, UpdateOptions.Create<HepPatientsPlan>()
                        .WithNotModified(p => p.Created));
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for get Patient Hep Plan by Id. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Hep Patients Plan Entity</returns>
        public async Task<HepPatientsPlan> ReadHepPatientAsync(long id)
        {
            var readOptions = new ReadOptions<HepPatientsPlan>().AsReadOnly();

            var patientHepMaster = await _hepPatientPlanRepository.ReadAsync(id, readOptions);

            return patientHepMaster;

        }

        #endregion

        /// <summary>
        /// Method Description : Using for Delete Patient Hep Group by Id. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Reurns True/False</returns>
        public async Task<bool> DeletehepGroupAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var group = _hepGroupRepository.Read(id);

                if (group != null)
                {
                    _hepGroupRepository.Delete(group);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }
    }
}
