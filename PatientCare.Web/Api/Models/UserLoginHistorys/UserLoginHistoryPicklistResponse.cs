﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;
using System;

namespace PatientCare.Web.Api.Models.UserLoginHistorys
{
    [DataContract]
    public class UserLoginHistoryPicklistResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public DateTime? LogDateTime { get; set; }

        [DataMember]
        public string IPAddress { get; set; }

        [DataMember]
        public string SessionKey { get; set; }

        [DataMember]
        public string Username { get; set; }
    }
}