﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.HepGroups;
using PatientCare.Web.Api.Models.HepDetails;
using EmitMapper.MappingConfiguration;
using System.Linq;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Services.Mapping
{
    public static class HepGroupMapperExtensions
    {
        public static EmitMapperMapperProvider HepGroupRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepGroup, HepGroupResponse>
                    (new ExtendedMapConfig<HepGroup, HepGroupResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepGroup[], HepGroupResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepGroup, PicklistDefaultResponse>
                    (new ExtendedMapConfig<HepGroup, PicklistDefaultResponse>()));

            var hepDetailMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<HepDetailRequest, HepDetail>
                    (new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                 ObjectMapperManager.DefaultInstance.GetMapper<HepGroupRequest, HepGroup>
                    (new ExtendedMapConfig<HepGroupRequest, HepGroup>()
                        .ForMember(x => x.HepDetails, y => y.HepDetails != null ? y.HepDetails.Select(i => hepDetailMapper.Map(i)).ToList() : null)
                    ));

            return mapperProvider;
        }
    }
}