﻿using System;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Core.Entities;
using PatientCare.Web.Api.Models.ResourceTimeoffBenefits;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceTimeoffBenefitMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceTimeoffBenefitRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTimeoffBenefit, ResourceTimeoffBenefitResponse>
                    (new ExtendedMapConfig<ResourceTimeoffBenefit, ResourceTimeoffBenefitResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTimeoffBenefit[], ResourceTimeoffBenefitResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTimeoffBenefitResponse, ResourceTimeoffBenefit>
                    (new ExtendedMapConfig<ResourceTimeoffBenefitResponse, ResourceTimeoffBenefit>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTimeoffBenefitRequest, ResourceTimeoffBenefit>
                    (new ExtendedMapConfig<ResourceTimeoffBenefitRequest, ResourceTimeoffBenefit>()));

            return mapperProvider;
        }
    }
}