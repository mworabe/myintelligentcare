using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Hep Group.
    /// </summary>
    public class HepGroup : NamedEntity
    {
        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public long? PatientCaseId { get; set; }
        public virtual PatientCase PatientCase { get; set; }

        public long? HepId { get; set; }
        public virtual HepMaster Hep { get; set; }

        public int? ExerciseOrder { get; set; }

        public virtual ICollection<HepDetail> HepDetails { get; set; }
    }
}
