﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using PatientCare.Core.Entities;
using PatientCare.Core.Enums;
using PatientCare.Core.Services.ResourceScoreCalculation;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Services.ResourceScoreCalculation
{
    public class ResourceScoreService : IResourceScoreService
    {
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        public ResourceScoreService
            (
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        public IList<Tuple<Resource, double, long>> Calculate(ScoreCalculationFilter filter, int pageIndex, int pageSize)
        {
            if (filter == null) throw new ArgumentNullException("filter");

            List<long> resultIds = FilterResourcesPrepareData(filter, pageIndex, pageSize);

            long resourceIDCount = resultIds.Count();

            var resourceData = FilterResourcesData(resultIds, pageIndex, pageSize);

            // var resources = FilterResources(filter, pageIndex, pageSize);

            return CalculateScore(resourceData, filter, resourceIDCount);
        }
        private List<long> FilterResourcesPrepareData(ScoreCalculationFilter filter, int pageIndex, int pageSize)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var resources = ctx.Resources.AsNoTracking().ApplyIncludes()
                    .Select(x => new ResourceProjection
                    {
                        Id = x.Id,
                        TimeZoneInfo = x.TimeZoneInfo,
                        WorksWellAlone = x.WorksWellAlone,
                        WorksWellInTeam = x.WorksWellInTeam,
                        Productivity = x.Productivity,
                        Rating = x.Rating,
                        SpokenLanguages = x.SpokenLanguages,
                        Skills = x.Skills,
                        Certificates = x.Certificates,
                        Educations = x.Educations,
                        CountryId = x.CountryId,
                        JobTitle = x.JobTitle.Name,
                        DivisionId = x.DivisionId,
                        SupervisorId = x.SupervisorId,
                        DepartmentId = x.DepartmentId,                        
                        RangeType = x.Range,
                        HourlyRate = x.HourlyRate,
                        GradeLevel = x.GradeLevel,
                        Location = x.Location
                    });

                var result = resources.Select(x => x.Id) // just a starting point of filtering
                    .ApplyFilter(resources, filter.JobTitle, r => r.JobTitle == filter.JobTitle.Item)
                    .ApplyFilter(resources, filter.Company, r => r.CompanyId == filter.Company.Item)
                    .ApplyFilter(resources, filter.Division, r => r.DivisionId == filter.Division.Item)
                    .ApplyFilter(resources, filter.Department, r => r.DepartmentId == filter.Department.Item)
                    .ApplyFilter(resources, filter.RangeType, r => r.RangeType == filter.RangeType.Item)
                    .ApplyFilter(resources, filter.ResourceType, r => r.ResourceType == filter.ResourceType.Item)
                    .ApplyFilter(resources, filter.WorksWellInTeam, r => r.WorksWellInTeam == filter.WorksWellInTeam.Item)
                    .ApplyFilter(resources, filter.WorksWellAlone, r => r.WorksWellAlone == filter.WorksWellAlone.Item)
                    .ApplyFilter(resources, filter.Location, r => r.Location == filter.Location.Item)
                    .ApplyRangeFilter(resources, filter.GradeLevel,
                     () =>
                     {
                         var startRange = filter.GradeLevel != null ? filter.GradeLevel.StartRange : 0;
                         var endRange = filter.GradeLevel != null ? filter.GradeLevel.EndRange : 0;
                         if (startRange == null && endRange == null) return r => r.GradeLevel == filter.GradeLevel.Item;
                         if (startRange <= 0 && endRange <= 0) return r => true;
                         if (startRange > 0 && endRange > 0)
                         {
                             return r => r.GradeLevel >= startRange && r.GradeLevel <= endRange;
                         }
                         return r => r.GradeLevel >= startRange || r.GradeLevel <= endRange;
                     })
                    .ApplyRangeFilter(resources, filter.HourlyRate,
                     () =>
                     {
                         //var startRange = filter.HourlyRate != null ? filter.HourlyRate.StartRange : 0;
                         //var endRange = filter.HourlyRate != null ? filter.HourlyRate.EndRange : 0;
                         //if (startRange == null && endRange == null) return r => Math.Abs(r.HourlyRate ?? 0 - filter.HourlyRate.Item ?? 0) < ((decimal)0.001);
                         //if (startRange <= 0 && endRange <= 0) return r => true;
                         //if (startRange > 0 && endRange > 0)
                         //{
                         //    return r => r.HourlyRate >= startRange && r.HourlyRate <= endRange;
                         //}
                         //return r => r.HourlyRate >= startRange || r.HourlyRate <= endRange;

                         //return r => Math.Abs((r.HourlyRate ?? 0) - (filter.HourlyRate.Item ?? 0)) < ((decimal)0.001);

                         return r => r.HourlyRate == filter.HourlyRate.Item;
                     })
                    .ApplyRangeFilter(resources, filter.Productivity,
                     () =>
                     {
                         var startRange = filter.Productivity != null ? filter.Productivity.StartRange : 0;
                         var endRange = filter.Productivity != null ? filter.Productivity.EndRange : 0;
                         if (!(startRange > 0) && !(endRange > 0)) return r => true;
                         if (startRange > 0 && endRange > 0)
                         {
                             return r => r.Productivity >= startRange && r.Productivity <= endRange;
                         }
                         return r => r.Productivity >= startRange || r.Productivity <= endRange;
                     })
                    .ApplyRangeFilter(resources, filter.Rating,
                    () =>
                    {
                        var startRange = filter.Rating != null ? filter.Rating.StartRange : 0;
                        var endRange = filter.Rating != null ? filter.Rating.EndRange : 0;
                        if (!(startRange > 0) && !(endRange > 0)) return r => true;
                        if (startRange > 0 && endRange > 0)
                        {
                            return r => r.Rating >= startRange && r.Rating <= endRange;
                        }
                        return r => r.Rating >= startRange || r.Rating <= endRange;
                    })
                    .ApplyFilter(resources, filter.Education, r => r.Educations.Any(x => x.NameOfSchool == filter.Education.Item || x.Subject == filter.Education.Item))
                    .ApplyFilter(resources, filter.Country, r => r.CountryId == filter.Country.Item);
                
                result = FilterByTimeZoneInfo(filter, result, resources);
                result = FilterBySpokenLanguages(filter, resources, result);
                result = FilterByWrittenLanguages(filter, resources, result);
                result = FilterBySkills(filter, resources, result);
                result = FilterByCertificates(filter, resources, result);
               
                var resultIds = result.ToList();
                return resultIds;
            }
        }

        private IList<Resource> FilterResourcesData(List<long> resultIds, int pageIndex, int pageSize)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var finalResourceData = resultIds.Skip((pageIndex - 1) * pageSize).Take(pageSize);

                return ctx.Resources.ApplyIncludes().Where(x => finalResourceData.Contains(x.Id)).ToList();
            }
        }

        private IList<Resource> FilterResources(ScoreCalculationFilter filter, int pageIndex, int pageSize)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var resources = ctx.Resources.AsNoTracking().ApplyIncludes()
                    .Select(x => new ResourceProjection
                    {
                        Id = x.Id,
                        TimeZoneInfo = x.TimeZoneInfo,
                        WorksWellAlone = x.WorksWellAlone,
                        WorksWellInTeam = x.WorksWellInTeam,
                        Productivity = x.Productivity,
                        Rating = x.Rating,
                        SpokenLanguages = x.SpokenLanguages,
                        Skills = x.Skills,
                        Certificates = x.Certificates,
                        Educations = x.Educations,
                        CountryId = x.CountryId,
                        JobTitle = x.JobTitle.Name,
                        DivisionId = x.DivisionId,
                        SupervisorId = x.SupervisorId,
                        DepartmentId = x.DepartmentId,
                        RangeType = x.Range,
                        HourlyRate = x.HourlyRate,
                        GradeLevel = x.GradeLevel,
                        Location = x.Location
                    });

                var result = resources.Select(x => x.Id) // just a starting point of filtering
                    .ApplyFilter(resources, filter.JobTitle, r => r.JobTitle == filter.JobTitle.Item)
                    .ApplyFilter(resources, filter.Company, r => r.CompanyId == filter.Company.Item)
                    .ApplyFilter(resources, filter.Division, r => r.DivisionId == filter.Division.Item)
                    .ApplyFilter(resources, filter.Department, r => r.DepartmentId == filter.Department.Item)
                    .ApplyFilter(resources, filter.RangeType, r => r.RangeType == filter.RangeType.Item)
                    .ApplyFilter(resources, filter.ResourceType, r => r.ResourceType == filter.ResourceType.Item)
                    .ApplyFilter(resources, filter.WorksWellInTeam, r => r.WorksWellInTeam == filter.WorksWellInTeam.Item)
                    .ApplyFilter(resources, filter.WorksWellAlone, r => r.WorksWellAlone == filter.WorksWellAlone.Item)
                    .ApplyFilter(resources, filter.Location, r => r.Location == filter.Location.Item)
                    .ApplyRangeFilter(resources, filter.GradeLevel,
                     () =>
                     {
                         var startRange = filter.GradeLevel != null ? filter.GradeLevel.StartRange : 0;
                         var endRange = filter.GradeLevel != null ? filter.GradeLevel.EndRange : 0;
                         if (startRange == null && endRange == null) return r => r.GradeLevel == filter.GradeLevel.Item;
                         if (startRange <= 0 && endRange <= 0) return r => true;
                         if (startRange > 0 && endRange > 0)
                         {
                             return r => r.GradeLevel >= startRange && r.GradeLevel <= endRange;
                         }
                         return r => r.GradeLevel >= startRange || r.GradeLevel <= endRange;
                     })
                    .ApplyRangeFilter(resources, filter.HourlyRate,
                     () =>
                     {
                         //var startRange = filter.HourlyRate != null ? filter.HourlyRate.StartRange : 0;
                         //var endRange = filter.HourlyRate != null ? filter.HourlyRate.EndRange : 0;
                         //if (startRange == null && endRange == null) return r => Math.Abs(r.HourlyRate ?? 0 - filter.HourlyRate.Item ?? 0) < ((decimal)0.001);
                         //if (startRange <= 0 && endRange <= 0) return r => true;
                         //if (startRange > 0 && endRange > 0)
                         //{
                         //    return r => r.HourlyRate >= startRange && r.HourlyRate <= endRange;
                         //}
                         //return r => r.HourlyRate >= startRange || r.HourlyRate <= endRange;

                         //return r => Math.Abs((r.HourlyRate ?? 0) - (filter.HourlyRate.Item ?? 0)) < ((decimal)0.001);

                         return r => r.HourlyRate == filter.HourlyRate.Item;
                     })
                    .ApplyRangeFilter(resources, filter.Productivity,
                     () =>
                     {
                         var startRange = filter.Productivity != null ? filter.Productivity.StartRange : 0;
                         var endRange = filter.Productivity != null ? filter.Productivity.EndRange : 0;
                         if (!(startRange > 0) && !(endRange > 0)) return r => true;
                         if (startRange > 0 && endRange > 0)
                         {
                             return r => r.Productivity >= startRange && r.Productivity <= endRange;
                         }
                         return r => r.Productivity >= startRange || r.Productivity <= endRange;
                     })
                    .ApplyRangeFilter(resources, filter.Rating,
                    () =>
                    {
                        var startRange = filter.Rating != null ? filter.Rating.StartRange : 0;
                        var endRange = filter.Rating != null ? filter.Rating.EndRange : 0;
                        if (!(startRange > 0) && !(endRange > 0)) return r => true;
                        if (startRange > 0 && endRange > 0)
                        {
                            return r => r.Rating >= startRange && r.Rating <= endRange;
                        }
                        return r => r.Rating >= startRange || r.Rating <= endRange;
                    })
                    .ApplyFilter(resources, filter.Education, r => r.Educations.Any(x => x.NameOfSchool == filter.Education.Item || x.Subject == filter.Education.Item))
                    .ApplyFilter(resources, filter.Country, r => r.CountryId == filter.Country.Item);
                /*.ApplyRangeFilter(resources, filter.AvailabilityVsDemand,
                () =>
                {
                    return null;
                });*/

                result = FilterByTimeZoneInfo(filter, result, resources);
                result = FilterBySpokenLanguages(filter, resources, result);
                result = FilterByWrittenLanguages(filter, resources, result);
                result = FilterBySkills(filter, resources, result);
                result = FilterByCertificates(filter, resources, result);                

                var resultIds = result.ToList();
                var finalResourceData = resultIds.Skip((pageIndex - 1) * pageSize).Take(pageSize);

                return ctx.Resources.ApplyIncludes().Where(x => finalResourceData.Contains(x.Id)).ToList();
            }
        }

        private static IQueryable<long> FilterByCertificates(ScoreCalculationFilter filter, IQueryable<ResourceProjection> resources, IQueryable<long> result)
        {
            if (filter.Certificates != null && filter.Certificates.Any(x => x.Mandatory))
            {
                var certificatesFilterIds = filter.Certificates.Where(x => x.Mandatory).Select(x => x.Item).ToList();
                var certificatesResources =
                    resources.SelectMany(r => r.Certificates)
                        .Where(certificate => certificatesFilterIds.Any(x => x == certificate.CertificateId))
                        .Select(x => x.ResourceId);
                return result.Join(certificatesResources, x => x, y => y, (x, y) => x);
            }
            return result;
        }

        private static IQueryable<long> FilterBySkills(ScoreCalculationFilter filter, IQueryable<ResourceProjection> resources, IQueryable<long> result)
        {
            if (filter.Skills != null && filter.Skills.Any(x => x.Mandatory))
            {
                var skillsFilterIds = filter.Skills.Where(x => x.Mandatory).Select(x => x.Item.SkillId).ToList();
                var skillsResources =
                    resources.SelectMany(r => r.Skills)
                        .Where(skill => skillsFilterIds.Any(x => x == skill.SkillId))
                        .ToList()
                        .Where(i => filter.Skills.Any(x =>
                        {
                            var startRange = x.StartRange != null ? x.StartRange.YearsOfExpirience : 0;
                            var endRange = x.EndRange != null ? x.EndRange.YearsOfExpirience : 0;
                            if (startRange <= 0 && endRange <= 0) return true;
                            if (startRange > 0 && endRange > 0)
                            {
                                return x.Item.SkillId == i.SkillId &&
                                       i.YearsOfExpirience >= startRange &&
                                       i.YearsOfExpirience <= endRange;
                            }
                            return x.Item.SkillId == i.SkillId &&
                                   (i.YearsOfExpirience >= startRange ||
                                    i.YearsOfExpirience <= endRange);
                        }))
                        .Select(x => x.ResourceId);
                return result.Join(skillsResources, x => x, y => y, (x, y) => x);
            }
            return result;
        }

        private static IQueryable<long> FilterByWrittenLanguages(ScoreCalculationFilter filter, IQueryable<ResourceProjection> resources,
            IQueryable<long> result)
        {
            if (filter.WrittenLanguages != null && filter.WrittenLanguages.Any(x => x.Mandatory))
            {
                var writtenLanguageFilterIds = filter.WrittenLanguages.Where(x => x.Mandatory).Select(x => x.Item.LanguageId).ToList();
                var writtenLanguagesResources =
                    resources.SelectMany(r => r.SpokenLanguages)
                        .Where(language => writtenLanguageFilterIds.Any(x => x == language.LanguageId))
                        .ToList()
                        .Where(i => filter.WrittenLanguages.Any(
                            x =>
                            {
                                var startRange = x.StartRange != null ? x.StartRange.WritingProficiency : 0;
                                var endRange = x.EndRange != null ? x.EndRange.WritingProficiency : 0;
                                if (!(startRange > 0) && !(endRange > 0)) return true;
                                if (startRange > 0 && endRange > 0)
                                {
                                    return x.Item.LanguageId == i.LanguageId &&
                                           i.WritingProficiency >= startRange &&
                                           i.WritingProficiency <= endRange;
                                }
                                return x.Item.LanguageId == i.LanguageId &&
                                       (i.WritingProficiency >= startRange ||
                                        i.WritingProficiency <= endRange);
                            }))
                        .Select(x => x.ResourceId);
                return result.Join(writtenLanguagesResources, x => x, y => y, (x, y) => x);
            }
            return result;
        }

        private static IQueryable<long> FilterBySpokenLanguages(ScoreCalculationFilter filter, IQueryable<ResourceProjection> resources, IQueryable<long> result)
        {
            if (filter.SpokenLanguages != null && filter.SpokenLanguages.Any(x => x.Mandatory))
            {
                var spokenLanguagesFilterIds = filter.SpokenLanguages.Select(x => x.Item.LanguageId).ToList();
                var spokenLanguagesResources =
                    resources.SelectMany(r => r.SpokenLanguages)
                        .Where(language => spokenLanguagesFilterIds.Any(x => x == language.LanguageId))
                        .ToList()
                        .Where(i => filter.SpokenLanguages.Any(x =>
                        {
                            var startRange = x.StartRange != null ? x.StartRange.SpeakingProficiency : 0;
                            var endRange = x.EndRange != null ? x.EndRange.SpeakingProficiency : 0;
                            if (!(startRange > 0) && !(endRange > 0)) return true;
                            if (startRange > 0 && endRange > 0)
                            {
                                return x.Item.LanguageId == i.LanguageId &&
                                       i.SpeakingProficiency >= startRange &&
                                       i.SpeakingProficiency <= endRange;
                            }
                            return x.Item.LanguageId == i.LanguageId &&
                                   (i.SpeakingProficiency >= startRange ||
                                    i.SpeakingProficiency <= endRange);
                        }))
                        .Select(x => x.ResourceId);
                return result.Join(spokenLanguagesResources, x => x, y => y, (x, y) => x);
            }
            return result;
        }

        private static IQueryable<long> FilterByTimeZoneInfo(ScoreCalculationFilter filter, IQueryable<long> result, IQueryable<ResourceProjection> resources)
        {
            if (filter.TimeZoneInfo != null && filter.TimeZoneInfo.Mandatory)
            {
                var timeZoneInfoStart = filter.TimeZoneInfo.StartRange != null
                    ? GetHours(filter.TimeZoneInfo.StartRange)
                    : -13;
                var timeZoneInfoEnd = filter.TimeZoneInfo.EndRange != null ? GetHours(filter.TimeZoneInfo.EndRange) : 13;
                return result.Join(
                        resources.ToList().Where(r => r.TimeZoneInfo == filter.TimeZoneInfo.Item)
                            .Select(x => x.Id), x => x, y => y, (x, y) => x).AsQueryable();

                //return result.Join(
                //       resources.ToList().Where(
                //           r =>
                //               GetHours(r.TimeZoneInfo) >= timeZoneInfoStart && GetHours(r.TimeZoneInfo) <= timeZoneInfoEnd)
                //           .Select(x => x.Id), x => x, y => y, (x, y) => x).AsQueryable();
            }
            return result;
        }

        private static IList<Tuple<Resource, double, long>> CalculateScore(IList<Resource> resources, ScoreCalculationFilter filter, long resourceIDCount)
        {
            var filterScore = CalculateFilterScore(filter);
            if (filterScore > 0)
            {
                var result = resources.Select(x => new ResourceCalcResult { Id = x.Id, Score = 0.0, Resource = x })
                    // just a starting point of evaluation
                    .ApplyCalculation(resources, filter.TimeZoneInfo,
                        x => TimeZoneScoreCalc(x.TimeZoneInfo, filter.TimeZoneInfo))
                    .ApplyCalculation(resources, filter.WorksWellInTeam, x => 1.0 * filter.WorksWellInTeam.Importance,
                        where: r => r.WorksWellInTeam == filter.WorksWellInTeam.Item)
                    .ApplyCalculation(resources, filter.WorksWellAlone, x => 1.0 * filter.WorksWellAlone.Importance,
                        where: r => r.WorksWellAlone == filter.WorksWellAlone.Item)
                    .ApplyCalculation(resources, filter.Productivity, x => (x.Productivity ?? 0) * filter.Productivity.Importance)
                    .ApplyCalculation(resources, filter.Rating, x => (x.Rating ?? 0) * filter.Rating.Importance)
                    .ApplyCalculation(resources, filter.Education, x => 1.0 * filter.Education.Importance,
                        where: r => r.Educations.Any(x => x.NameOfSchool == filter.Education.Item || x.Subject == filter.Education.Item))
                    .ApplyCalculation(resources, filter.ResourceType, x => 1.0 * filter.ResourceType.Importance)
                    .ApplyCalculation(resources, filter.JobTitle, x => 1.0 * filter.JobTitle.Importance,
                        where: r => r.JobTitle.Name.Contains(filter.JobTitle.Item))
                    .ApplyCalculation(resources, filter.Location, x => 1.0 * filter.Location.Importance,
                        where: r => r.Location == filter.Location.Item);


                if (filter.SpokenLanguages != null && filter.SpokenLanguages.Any())
                {
                    var spokenLanguagesFilterIds =
                        filter.SpokenLanguages.Select(x => new { x.Item.LanguageId, x.Importance }).ToList();
                    var spokenLanguagesResources =
                        resources.SelectMany(r => r.SpokenLanguages)
                            .Join(spokenLanguagesFilterIds,
                                language => language.LanguageId,
                                f => f.LanguageId,
                                (l, f) =>
                                    new ResourceCalcResult
                                    {
                                        Id = l.ResourceId,
                                        Score = f.Importance * l.SpeakingProficiency,
                                        Resource = l.Resource
                                    })
                            .ToList();

                    result = result.Concat(spokenLanguagesResources);
                }

                if (filter.WrittenLanguages != null && filter.WrittenLanguages.Any())
                {
                    var writtenLanguageFilterIds =
                        filter.WrittenLanguages.Select(x => new { x.Item.LanguageId, x.Importance }).ToList();
                    var writtenLanguagesResources =
                        resources.SelectMany(r => r.SpokenLanguages)
                            .Join(writtenLanguageFilterIds,
                                language => language.LanguageId,
                                f => f.LanguageId,
                                (l, f) =>
                                    new ResourceCalcResult
                                    {
                                        Id = l.ResourceId,
                                        Score = f.Importance * l.WritingProficiency,
                                        Resource = l.Resource
                                    })
                            .ToList();

                    result = result.Concat(writtenLanguagesResources);
                }

                if (filter.Skills != null && filter.Skills.Any())
                {
                    var skillsFilterIds = filter.Skills.Select(x => new { x.Item.SkillId, x.Importance }).ToList();
                    var skillsResources =
                        resources.SelectMany(r => r.Skills)
                            .Join(skillsFilterIds,
                                skill => skill.SkillId,
                                f => f.SkillId,
                                (s, f) =>
                                    new ResourceCalcResult
                                    {
                                        Id = s.ResourceId,
                                        Score = 1.0 * f.Importance * s.YearsOfExpirience,
                                        Resource = s.Resource
                                    })
                            .ToList();
                    result = result.Concat(skillsResources);
                }

                if (filter.Certificates != null && filter.Certificates.Any())
                {
                    var certificatesFilterIds = filter.Certificates.ToList();
                    var certificatesResources =
                        resources.SelectMany(r => r.Certificates)
                            .Join(certificatesFilterIds,
                                skill => skill.CertificateId,
                                f => f.Item,
                                (s, f) =>
                                    new ResourceCalcResult
                                    {
                                        Id = s.ResourceId,
                                        Score = 1.0 * f.Importance,
                                        Resource = s.Resource
                                    })
                            .ToList();
                    result = result.Union(certificatesResources.Select(x => x));
                }
                return
                    result.GroupBy(t => t.Resource)
                        .Select(x => Tuple.Create(x.Key, ((x.Sum(y => y.Score)) / filterScore * 100), resourceIDCount))
                        .ToList();
            }
            return resources.Select(x => Tuple.Create(x, 0.0, resourceIDCount)).ToList();
        }

        private static double CalculateFilterScore(ScoreCalculationFilter filter)
        {
            var result = 0.0;

            if (filter.JobTitle != null)
            {
                result += 1.0 * filter.JobTitle.Importance;
            }

            if (filter.TimeZoneInfo != null)
            {
                result += 1.0 * filter.TimeZoneInfo.Importance;
            }

            if (filter.Location != null)
            {
                result += 1.0 * filter.Location.Importance;
            }

            if (filter.WorksWellAlone != null)
            {
                result += 1.0 * filter.WorksWellAlone.Importance;
            }

            if (filter.WorksWellInTeam != null)
            {
                result += 1.0 * filter.WorksWellInTeam.Importance;
            }

            if (filter.Education != null)
            {
                result += 1.0 * filter.Education.Importance;
            }

            if (filter.ResourceType != null)
            {
                result += 1.0 * filter.ResourceType.Importance;
            }

            if (filter.Productivity != null)
            {
                result += filter.Productivity.Item * filter.Productivity.Importance;
            }
            if (filter.Rating != null)
            {
                result += filter.Rating.Item * filter.Rating.Importance;
            }

            if (filter.Skills != null)
            {
                foreach (var skill in filter.Skills)
                {
                    result += skill.Item.YearsOfExpirience * skill.Importance;
                }
            }

            if (filter.Certificates != null)
            {
                foreach (var cert in filter.Certificates)
                {
                    result += 1.0 * cert.Importance;
                }
            }

            if (filter.SpokenLanguages != null)
            {
                foreach (var language in filter.SpokenLanguages)
                {
                    result += language.Item.SpeakingProficiency * language.Importance;
                }
            }

            if (filter.WrittenLanguages != null)
            {
                foreach (var language in filter.WrittenLanguages)
                {
                    result += language.Item.WritingProficiency * language.Importance;
                }
            }

            return result;
        }

        private static double TimeZoneScoreCalc(string timeZoneInfo, ScoreFilterItem<string> scoreFilterItem)
        {
            try
            {
                var resourceTimeZoneOffsetInt = GetHours(timeZoneInfo);
                var filterTimeZoneOffsetInt = GetHours(scoreFilterItem.Item);
                return resourceTimeZoneOffsetInt != 0 ? filterTimeZoneOffsetInt / resourceTimeZoneOffsetInt : 0;
            }
            catch (TimeZoneNotFoundException)
            {
                return 0;
            }
        }

        private static int GetHours(string timeZoneInfo)
        {
            if (string.IsNullOrEmpty(timeZoneInfo))
            {
                return 0;
            }
            try
            {
                return TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo).BaseUtcOffset.Hours;
            }
            catch (TimeZoneNotFoundException)
            {
                return 0;
            }
        }
    }

    internal class ResourceProjection
    {
        public long Id;
        public string TimeZoneInfo;
        public bool? WorksWellAlone;
        public bool? WorksWellInTeam;
        public double? Productivity;
        public double? Rating;
        public string JobTitle;
        public long? CompanyId;
        public long? DepartmentId;
        public long? DivisionId;
        public long? SupervisorId;
        public RangeTypeEnum? RangeType;
        public ResourceTypeEnum? ResourceType;
        public decimal? HourlyRate;
        public ICollection<ResourceSpokenLanguage> SpokenLanguages;
        public ICollection<ResourceSkill> Skills;
        public ICollection<ResourceCertificate> Certificates;
        public ICollection<ResourceEducation> Educations;
        public int? GradeLevel;
        public long? CountryId;
        public string Location;
    }

    internal class ResourceCalcResult
    {
        public long Id;
        public double Score;
        public Resource Resource;
    }

    internal static class ApplyFilterExtensions
    {
        public static IQueryable<Resource> ApplyIncludes(this IQueryable<Resource> resources)
        {
            return resources.Include(item => item.Certificates)
                .Include(item => item.JobTitle)
                .Include(item => item.Certificates)
                .Include(item => item.Certificates.Select(c => c.Certificate))
                .Include(item => item.Educations)
                .Include(item => item.Division)
                .Include(item => item.Supervisor)
                .Include(item => item.Department)
                .Include(item => item.Skills)
                .Include(item => item.Skills.Select(c => c.Skill))
                .Include(item => item.Travels)
                .Include(item => item.SpokenLanguages)
                .Include(item => item.SpokenLanguages.Select(c => c.Language));
        }

        public static IQueryable<long> ApplyFilter<T>(this IQueryable<long> result, IQueryable<ResourceProjection> resources, ScoreFilterItem<T> filterItem, Expression<Func<ResourceProjection, bool>> where)
        {
            if (filterItem != null && filterItem.Mandatory)
            {
                return result.Join(resources.Where(where), x => x, y => y.Id, (x, y) => x);
            }
            return result;
        }

        public static IQueryable<long> ApplyRangeFilter<T>(this IQueryable<long> result, IQueryable<ResourceProjection> resources, ScoreFilterItem<T> filterItem,
            Func<Expression<Func<ResourceProjection, bool>>> getWhere)
        {

            if (filterItem != null && filterItem.Mandatory)
            {
                return result.Join(resources.Where(getWhere()), x => x, y => y.Id, (x, y) => x);
            }
            return result;
        }

        public static IEnumerable<ResourceCalcResult> ApplyCalculation<T>(this IEnumerable<ResourceCalcResult> result, IList<Resource> resources, ScoreFilterItem<T> filterItem, Func<Resource, double> scoreCalc, Func<Resource, bool> where = null)
        {
            if (filterItem != null)
            {
                if (where != null)
                {
                    result = result.Concat(resources.Where(where).Select(x => new ResourceCalcResult { Id = x.Id, Score = scoreCalc(x), Resource = x }));
                }
                else
                {
                    result = result.Concat(resources.Select(x => new ResourceCalcResult { Id = x.Id, Score = scoreCalc(x), Resource = x }));
                }

            }
            return result;
        }
    }
}
