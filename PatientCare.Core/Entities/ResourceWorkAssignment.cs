﻿using System;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Work Assignement.
    /// </summary>
    public class ResourceWorkAssignment : Entity
    {
        public string AssignmentDescription { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long ResourceWorkExperienceId { get; set; }
        public virtual ResourceWorkExperience ResourceWorkExperience { get; set; }

        public long? AssignmentIndustryId { get; set; }
        public virtual DropDownConfig AssignmentIndustry { get; set; }

        public bool? OfNote { get; set; }
        public bool? JudicialClerkShip { get; set; }
    }
}
