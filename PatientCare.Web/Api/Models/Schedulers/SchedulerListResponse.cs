﻿using PatientCare.Core.Enums;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Schedulers
{
    [DataContract]
    public class SchedulerListResponse 
    {
        [DataMember]
        public long? AppointmentId { get; set; }

        [DataMember]
        public long? ClinicianId { get; set; }

        [DataMember]
        public string ClinicianName { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public string PatientName { get; set; }

        [DataMember]
        public string PatientNumber { get; set; }

        [DataMember]
        public string PhotoFileName { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public string CellNumber { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public long? CaseId { get; set; }

        [DataMember]
        public string CaseNo { get; set; }

        [DataMember]
        public CaseStatusEnum? CaseStatus { get; set; }

        [DataMember]
        public long? InjuryRegionId { get; set; }

        [DataMember]
        public string InjuryRegionName { get; set; }

        [DataMember]
        public long? PrimayDiagnosisId { get; set; }

        [DataMember]
        public string PrimayDiagnosisName { get; set; }

        [DataMember]
        public DateTime? ReturnTo { get; set; }

        [DataMember]
        public long? ReferingPhysicianId { get; set; }

        [DataMember]
        public string ReferingPhysicianName { get; set; }

        [DataMember]
        public long? AppointmentTypeId { get; set; }

        [DataMember]
        public string AppointmentTypeName { get; set; }

        [DataMember]
        public string AppointmentTypeColor { get; set; }

        [DataMember]
        public long? FacilityId { get; set; }

        [DataMember]
        public long? ClinicLocationId { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public AppointmentStatusEnum? AppointmentStatus { get; set; }
        
        [DataMember]
        public bool? IsMobile { get; set; }

        [DataMember]
        public bool? IsReschedule { get; set; }

        [DataMember]
        public long PatientWorkOutId { get; set; }

        [DataMember]
        public long? PatientWorkOutCaseId { get; set; }

        [DataMember]
        public DateTime? WorkOutStartDate { get; set; }

        [DataMember]
        public DateTime? WorkOutEndDate { get; set; }

        [DataMember]
        public int? PreExercisesPainRating { get; set; }

        [DataMember]
        public int? WorkoutFeelResult { get; set; }

        [DataMember]
        public string WorkoutComment { get; set; }

        [DataMember]
        public string AppointmentComment { get; set; }
    }
}