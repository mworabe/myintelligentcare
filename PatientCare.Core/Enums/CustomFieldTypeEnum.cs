﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title :Custom Field Type Enum.
    /// </summary>
    public enum CustomFieldTypeEnum
    {
        Alpha = 1,
        Alphanumeric = 2,
        Numeric = 3,
        Select = 4,
        Date = 5,
        Checkbox = 6,
        Bigtext = 7,
        File = 8,
        Time = 9,
        WeekTime = 10
    }
}