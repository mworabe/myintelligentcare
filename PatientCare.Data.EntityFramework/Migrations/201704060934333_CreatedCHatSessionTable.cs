namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedCHatSessionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.chat_Sessions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        channel_id = c.String(),
                        channel_unique_name = c.String(),
                        channel_name = c.String(),
                        from_user_id = c.String(),
                        to_user_id = c.String(),
                        created_by_user_id = c.String(),
                        last_message = c.String(),
                        type = c.Int(),
                        created = c.DateTime(nullable: false),
                        modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.chat_Sessions");
        }
    }
}
