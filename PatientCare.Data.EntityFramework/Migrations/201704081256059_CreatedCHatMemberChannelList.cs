namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedCHatMemberChannelList : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
        "dbo.sp_Chat_Member_channel",
        p => new
        {
            from_user_id = p.String(defaultValueSql: null),
            to_user_id = p.String(defaultValueSql: null)
        },
        body:
             @"select chatm.Id as memberId,chats.Id ,chats.channel_id as ChannelsId from chat_members chatm
		            left join chat_Sessions chats on chatm.chat_session_id = chats.id
	            where chatm.user_id in (@from_user_id,@to_user_id) "
            );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_Chat_Member_channel");
        }
    }
}
