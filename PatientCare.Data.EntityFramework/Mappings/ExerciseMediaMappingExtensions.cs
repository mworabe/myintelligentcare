﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ExerciseMediaMappingExtensions
    {
        public static DbModelBuilder MapExerciseMedia(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ExerciseMedia>().ToTable("exercise_medias")
                .HasKey(i => i.Id);

            modelBuilder.Entity<ExerciseMedia>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ExerciseMedia>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();
            
           modelBuilder.Entity<ExerciseMedia>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            modelBuilder.Entity<ExerciseMedia>()
                .Property(p => p.ExerciseId)
                .HasColumnName("exercise_id")
                .IsOptional();

            modelBuilder.Entity<ExerciseMedia>()
                .HasOptional<Exercise>(p => p.Exercise)
                .WithMany()
                .HasForeignKey(p => p.ExerciseId);

            modelBuilder.Entity<ExerciseMedia>()
                .Property(p => p.ThumbImageName)
                .HasColumnName("thumb_image_name")
                .IsOptional();

            modelBuilder.Entity<ExerciseMedia>()
                .Property(p => p.OriginalName)
                .HasColumnName("original_name")
                .IsOptional();

            modelBuilder.Entity<ExerciseMedia>()
                .Property(p => p.MediaType)
                .HasColumnName("media_type")
                .IsOptional();
            
            return modelBuilder;
        }
    }
}
