﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.AutoCompletes
{
    public class AutoCompleteRequest
    {
        [DataMember(Name = "search")]
        public string Search { get; set; }
        
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "clinicId")]
        public long? ClinicId { get; set; }

    }
}