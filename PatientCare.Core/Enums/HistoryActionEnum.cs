﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : History Action Enum.
    /// </summary>
    public enum HistoryActionEnum
    {
        Create = 0,
        Delete = 1,
        Update = 2
    }
}
