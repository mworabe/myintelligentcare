﻿using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceInsurances
{
    [DataContract]
    public class ResourceInsuranceResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public PicklistResponse Resource { get; set; }

        [DataMember]
        public long? InsuranceTypeId { get; set; }

        [DataMember]
        public PicklistResponse InsuranceType { get; set; }

        [DataMember]
        public string Participating { get; set; }

        [DataMember]
        public string EmployeeCost { get; set; }

        [DataMember]
        public string EmployerContribution { get; set; }
    }
}