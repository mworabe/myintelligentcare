﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Priority Order Enum.
    /// </summary>
    public enum PriorityOrderEnum
    {
        High = 0,
        Medium = 1,
        Low = 2
    }
}
