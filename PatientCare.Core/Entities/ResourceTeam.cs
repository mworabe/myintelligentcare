﻿using System.Collections.Generic;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Team.
    /// </summary>
    public class ResourceTeam: NamedEntity
    {
        public ResourceTeam()
        {
            Resources = new List<Resource>();
        }
        public long ResourceCompanyId { get; set; }
        public virtual ResourceCompany ResourceCompany { get; set; }
        public virtual IList<Resource> Resources { get; set; }
    }
}
