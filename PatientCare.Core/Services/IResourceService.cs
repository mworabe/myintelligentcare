﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;

namespace PatientCare.Core.Services
{
    public interface IResourceService
    {

        Task<bool> UpdateAsync(Resource resource, IList<RestrictAccessTo> restrictAccess);
        Task<bool> CreateAsync(Resource resource, IList<RestrictAccessTo> restrictAccess);
        Task<Resource> ReadAsync(long id, IList<RestrictAccessTo> restrictAccess);
        Task<IEnumerable<Resource>> ReadAsync(IList<RestrictAccessTo> restrictAccess);



        Task<bool> AdditionResourceCustomFields(Resource resource);
        Task<bool> ImportAsync(List<Resource> resources);
        Task<string> DeleteResourceAsync(Resource resource);
        Task MergeBulkUpdate(long[] id, Resource resource);

    }
}
