﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Enums;
using PatientCare.Core.Services;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle Resource operations And implementing all methods which is initialize in IResourceService.
    /// </summary>
    public class ResourceService : IResourceService
    {
        /// <summary>
        ///Description : Resource Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Resource Service Field IResourceRepository
        /// </summary>
        private readonly IResourceRepository _resourceRepository;

        /// <summary>
        ///Description : Resource Service Field ICustomFieldRepository
        /// </summary>
        private readonly ICustomFieldRepository _customFieldRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Resource class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="resourceRepository">Resource Repository</param>
        /// <param name="customFieldRepository">Custom Field Repository</param>
        public ResourceService
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IResourceRepository resourceRepository,
                ICustomFieldRepository customFieldRepository
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _resourceRepository = resourceRepository;
            _customFieldRepository = customFieldRepository;
        }

        /// <summary>
        /// Method Description : Using for Create new Resource.
        /// </summary>
        /// <param name="resource">Resource Entity</param>
        /// <param name="restrictAccess">Restrict AccessTo Enum</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateAsync(Resource resource, IList<RestrictAccessTo> restrictAccess)
        {
            if (resource == null) throw new ArgumentNullException("resource");

            ApplyRestrictions(restrictAccess, resource, new Resource());

            await _resourceRepository.CreateAsync(resource, UpdateOptions.Create<Resource>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Resource by id. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="restrictAccess">Restrict AccessTo Enum</param>
        /// <returns>Resource Entity</returns>
        public async Task<Resource> ReadAsync(long id, IList<RestrictAccessTo> restrictAccess)
        {
            var readOptions = new ReadOptions<Resource>().AsReadOnly()
                .WithIncludePredicate(item => item.JobTitle)
                .WithIncludePredicate(item => item.Country)
                .WithIncludePredicate(item => item.State)
                .WithIncludePredicate(item => item.City)
                .WithIncludePredicate(item => item.Division)
                .WithIncludePredicate(item => item.Supervisor)
                .WithIncludePredicate(item => item.Department)
                .WithIncludePredicate(item => item.Certificates)
                .WithIncludePredicate(item => item.Certificates.Select(c => c.Certificate))
                .WithIncludePredicate(item => item.Educations)
                .WithIncludePredicate(item => item.ResourceWorkExperiences)
                .WithIncludePredicate(item => item.ResourceWorkExperiences.Select(c => c.ResourceWorkAssignments))
                .WithIncludePredicate(item => item.ResourceProfessionalOrganizations)
                .WithIncludePredicate(item => item.SurveyResults)
                .WithIncludePredicate(item => item.ResourceTeam)
                .WithIncludePredicate(item => item.Skills)
                .WithIncludePredicate(item => item.Skills.Select(c => c.Skill))
                .WithIncludePredicate(item => item.CriminalRecords)
                .WithIncludePredicate(item => item.Travels)
                .WithIncludePredicate(item => item.ResourceBehaviouralDetails)
                .WithIncludePredicate(item => item.Industry)
                .WithIncludePredicate(item => item.ResourcePracticeAreas)
                .WithIncludePredicate(item => item.ResourceRecognitions)
                .WithIncludePredicate(item => item.Travels.Select(t => t.Country))
                .WithIncludePredicate(item => item.PreviousEmployments)
                .WithIncludePredicate(item => item.SpokenLanguages)
                .WithIncludePredicate(item => item.SpokenLanguages.Select(c => c.Language))
                .WithIncludePredicate(item => item.Travels.Select(x => x.Country))
                .WithIncludePredicate(item => item.CustomFieldValues)
                .WithIncludePredicate(item => item.CustomFieldValues.Select(z => z.CustomField))
                .WithIncludePredicate(item => item.CustomFieldValues.Select(z => z.CustomField.CustomFieldProperties));

            var resource = await _resourceRepository.ReadAsync(id, readOptions);
            ApplyRestrictions(restrictAccess, resource, new Resource());

            return resource;
        }

        /// <summary>
        /// Method Description : Using for get all Resource list. 
        /// </summary>
        /// <param name="restrictAccess">Restrict AccessTo Enum</param>
        /// <returns>Resource Entity</returns>
        public async Task<IEnumerable<Resource>> ReadAsync(IList<RestrictAccessTo> restrictAccess)
        {
            var readOptions = new ReadOptions<Resource>().AsReadOnly()
                .WithIncludePredicate(item => item.JobTitle)
                .WithIncludePredicate(item => item.Country)
                .WithIncludePredicate(item => item.State)
                .WithIncludePredicate(item => item.City)
                .WithIncludePredicate(item => item.Division)
                .WithIncludePredicate(item => item.Supervisor)
                .WithIncludePredicate(item => item.Department)
                .WithIncludePredicate(item => item.Certificates)
                .WithIncludePredicate(item => item.Certificates.Select(c => c.Certificate))
                .WithIncludePredicate(item => item.Educations)
                .WithIncludePredicate(item => item.SurveyResults)
                .WithIncludePredicate(item => item.ResourceTeam)
                .WithIncludePredicate(item => item.Skills)
                .WithIncludePredicate(item => item.Skills.Select(c => c.Skill))
                .WithIncludePredicate(item => item.ResourceBehaviouralDetails)
                .WithIncludePredicate(item => item.ResourceRecognitions)
                .WithIncludePredicate(item => item.Industry)
                .WithIncludePredicate(item => item.ResourcePracticeAreas)
                .WithIncludePredicate(item => item.CriminalRecords)
                .WithIncludePredicate(item => item.Travels)
                .WithIncludePredicate(item => item.Travels.Select(t => t.Country))
                .WithIncludePredicate(item => item.PreviousEmployments)
                .WithIncludePredicate(item => item.SpokenLanguages)
                .WithIncludePredicate(item => item.SpokenLanguages.Select(c => c.Language))
                .WithIncludePredicate(item => item.Travels.Select(x => x.Country))
                .WithIncludePredicate(item => item.CustomFieldValues)
                .WithIncludePredicate(item => item.CustomFieldValues.Select(z => z.CustomField))
                .WithIncludePredicate(item => item.CustomFieldValues.Select(z => z.CustomField.CustomFieldProperties));

            var resources = await _resourceRepository.ListAsync(filter: null, readOptions: readOptions);
            foreach (var resource in resources)
            {
                ApplyRestrictions(restrictAccess, resource, new Resource());
            }
            return resources;

        }

        /// <summary>
        /// Method Description : Using for add resource custom field.  
        /// </summary>
        /// <param name="resource">Resource Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> AdditionResourceCustomFields(Resource resource)
        {
            if (resource == null) throw new ArgumentNullException("resource");
            var customFieldsResources =
                await _customFieldRepository.ListAsync(
                        new FilterQuery<CustomField>()
                            .AddFilter(x =>
                                   x.ResourceAvailable &&
                                   x.ResourceCustomFieldValues.All(v => v.ResourceId != resource.Id)), new ReadOptions<CustomField>().WithIncludePredicate(z => z.CustomFieldProperties));
            foreach (var customFieldsResource in customFieldsResources)
            {
                resource.CustomFieldValues.Add(new ResourceCustomFieldValue
                {
                    CustomFieldId = customFieldsResource.Id,
                    CustomField = customFieldsResource,
                    ResourceId = resource.Id,
                });
            }
            return true;
        }

        /// <summary>
        /// Method Description : Create / Update Resources.   
        /// </summary>
        /// <param name="resources">List of Resource Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> ImportAsync(List<Resource> resources)
        {
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                try
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    foreach (var resource in resources)
                    {
                        ctx.Resources.Attach(resource);
                        ctx.Entry(resource).State = resource.Id > 0 ? EntityState.Modified : EntityState.Added;
                    }
                    await ctx.SaveChangesAsync();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Method Description : Using for delete Resource. 
        /// </summary>
        /// <param name="resource">Resource Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<string> DeleteResourceAsync(Resource resource)
        {
            var allowDeleting = true;
            var resourceId = resource.Id;
            var gender = (resource.Gender ?? GenderEnum.Male) == GenderEnum.Male ? "He" : "She";
            var sb = new StringBuilder(string.Format("You can't delete {0} because :", resource.Name));

            if (allowDeleting)
            {
                await _resourceRepository.DeleteAsync(resource);
                return "";
            }

            sb.Replace(Environment.NewLine, "<br/>");

            return sb.ToString();
        }

        /// <summary>
        /// Method Description : Using for Update Resource.
        /// </summary>
        /// <param name="resource">Resource Entity</param>
        /// <param name="restrictAccess">List of Restrict AccessTo Enum</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateAsync(Resource resource, IList<RestrictAccessTo> restrictAccess)
        {
            if (resource == null) throw new ArgumentNullException("resource");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                ResourceChidlrenProcessing(ctx, resource);
                var originResource = _resourceRepository.Read(resource.Id, new ReadOptions<Resource>().AsReadOnly());

                ApplyRestrictions(restrictAccess, resource, originResource);

                await _resourceRepository.UpdateAsync(resource, UpdateOptions.Create<Resource>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for bulk Update Resource. 
        /// </summary>
        /// <param name="ids">Array of Id</param>
        /// <param name="resource">Resource Entity</param>
        /// <returns></returns>
        public async Task MergeBulkUpdate(long[] ids, Resource resource)
        {
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                foreach (var id in ids)
                {
                    BulkUpdate(ctx, id, resource);
                }

                await dbContextScope.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Method Description : Using for Update Resource by id.  
        /// </summary>
        /// <param name="ctx">PatientCare DbContext</param>
        /// <param name="id">Id</param>
        /// <param name="resource">Resource Entity</param>
        private void BulkUpdate(PatientCareDbContext ctx, long id, Resource resource)
        {
            var resourceToSave = ctx.Resources.FirstOrDefault(x => x.Id == id);

            if (resourceToSave == null) return;

            resourceToSave.Gender = resource.Gender != null ? resource.Gender : resourceToSave.Gender;
            resourceToSave.MartialStatus = resource.MartialStatus != null ? resource.MartialStatus : resourceToSave.MartialStatus;
            resourceToSave.Ethnicity = resource.Ethnicity != null ? resource.Ethnicity : resourceToSave.Ethnicity;
            resourceToSave.JobTitle = resource.JobTitle != null ? resource.JobTitle : resourceToSave.JobTitle;
            resourceToSave.Location = resource.Location != null ? resource.Location : resourceToSave.Location;
            resourceToSave.TimeZoneInfo = resource.TimeZoneInfo != null ? resource.TimeZoneInfo : resourceToSave.TimeZoneInfo;
            resourceToSave.Division = resource.Division != null ? resource.Division : resourceToSave.Division;
            resourceToSave.Department = resource.Department != null ? resource.Department : resourceToSave.Department;
            resourceToSave.Supervisor = resource.Supervisor != null ? resource.Supervisor : resourceToSave.Supervisor;
            resourceToSave.Address = resource.Address != null ? resource.Address : resourceToSave.Address;
            resourceToSave.Address2 = resource.Address2 != null ? resource.Address2 : resourceToSave.Address2;
            resourceToSave.City = resource.City != null ? resource.City : resourceToSave.City;
            resourceToSave.State = resource.State != null ? resource.State : resourceToSave.State;
            resourceToSave.ZipCode = resource.ZipCode != null ? resource.ZipCode : resourceToSave.ZipCode;
            resourceToSave.Country = resource.Country != null ? resource.Country : resourceToSave.Country;
            resourceToSave.StartDate = resource.StartDate != null ? resource.StartDate : resourceToSave.StartDate;
            resourceToSave.EndDate = resource.EndDate != null ? resource.EndDate : resourceToSave.EndDate;
            resourceToSave.EmploymentType = resource.EmploymentType != null ? resource.EmploymentType : resourceToSave.EmploymentType;
            resourceToSave.HourlyRate = resource.HourlyRate != 0 ? resource.HourlyRate : resourceToSave.HourlyRate;
            resourceToSave.BonusOrOtherPay = resource.BonusOrOtherPay != null ? resource.BonusOrOtherPay : resourceToSave.BonusOrOtherPay;

            ctx.SaveChanges();
        }

        /// <summary>
        /// Method Description : Using for Update Resource's child.   
        /// </summary>
        /// <param name="ctx">DbContext</param>
        /// <param name="resource">Resource</param>
        private static void ResourceChidlrenProcessing(DbContext ctx, Resource resource)
        {
            var spokenLanguages = resource.SpokenLanguages;
            ctx.ChildrenProcessing(spokenLanguages, x => x.ResourceId == resource.Id);

            var educations = resource.Educations;
            ctx.ChildrenProcessing(educations, x => x.ResourceId == resource.Id);

            var resourcePracticeArea = resource.ResourcePracticeAreas;
            ctx.ChildrenProcessing(resourcePracticeArea, x => x.ResourceId == resource.Id);

            var travels = resource.Travels;
            ctx.ChildrenProcessing(travels, x => x.ResourceId == resource.Id);

            var behaviouralDetails = resource.ResourceBehaviouralDetails;
            ctx.ChildrenProcessing(behaviouralDetails, x => x.ResourceId == resource.Id);

            var indutryDetails = resource.Industry;
            ctx.ChildrenProcessing(indutryDetails, x => x.ResourceId == resource.Id);

            var resourceRecognitions = resource.ResourceRecognitions;
            ctx.ChildrenProcessing(resourceRecognitions, x => x.ResourceId == resource.Id);

            var certificates = resource.Certificates;
            ctx.ChildrenProcessing(certificates, x => x.ResourceId == resource.Id);

            var skills = resource.Skills;
            ctx.ChildrenProcessing(skills, x => x.ResourceId == resource.Id);

            var previousEmployments = resource.PreviousEmployments;
            ctx.ChildrenProcessing(previousEmployments, x => x.ResourceId == resource.Id);

            var criminalRecords = resource.CriminalRecords;
            ctx.ChildrenProcessing(criminalRecords, x => x.ResourceId == resource.Id);

            var professionalOraganizationRecords = resource.ResourceProfessionalOrganizations;
            ctx.ChildrenProcessing(professionalOraganizationRecords, x => x.ResourceId == resource.Id);

            var workExperienceRecords = resource.ResourceWorkExperiences;
            ctx.ChildrenProcessingExperience(workExperienceRecords, x => x.ResourceId == resource.Id);

            SaveAssignment(ctx, resource.ResourceWorkExperiences);

            var customFieldValues = resource.CustomFieldValues;
            ctx.ChildrenProcessing(customFieldValues, x => x.ResourceId == resource.Id);

            var resourceInsurances = resource.ResourceInsurances;
            ctx.ChildrenProcessing(resourceRecognitions, x => x.ResourceId == resource.Id);
        }

        /// <summary>
        /// Method Description : Using for save Resource Work Assignments.    
        /// </summary>
        /// <param name="ctx">DbContext</param>
        /// <param name="workexperienceList">Collection type Resource Work Experience Entity</param>
        private static void SaveAssignment(DbContext ctx, ICollection<ResourceWorkExperience> workexperienceList)
        {
            if (workexperienceList == null) return;

            var dbSet = ctx.Set<ResourceWorkAssignment>();

            foreach (var item in workexperienceList)
            {
                if (item.ResourceWorkAssignments != null && item.ResourceWorkAssignments.Count > 0)
                {
                    foreach (var assignment in item.ResourceWorkAssignments)
                    {
                        assignment.ResourceWorkExperienceId = item.Id;

                        dbSet.Attach(assignment);
                        ctx.Entry(assignment).State = assignment.Id > 0 ? EntityState.Modified : EntityState.Added;
                    }

                    List<ResourceWorkAssignment> notDeletedAssignementlist = new List<ResourceWorkAssignment>(item.ResourceWorkAssignments);

                    List<ResourceWorkAssignment> allAssignmentList = new List<ResourceWorkAssignment>(dbSet.AsQueryable().Where(o => o.ResourceWorkExperienceId == item.Id));

                    var peopleDifference = from asl in allAssignmentList
                                           where !(from nasl in notDeletedAssignementlist
                                                   select nasl.Id
                                                  ).Contains(asl.Id)
                                           select asl;

                    foreach (var deleteAssignment in peopleDifference)
                    {
                        ctx.Entry(deleteAssignment).State = EntityState.Deleted;
                    }
                }
            }
        }

        private void ApplyRestrictions(IEnumerable<RestrictAccessTo> restrictAccess, Resource currentResource, Resource originResource)
        {
            foreach (var restrictAccessTo in restrictAccess)
            {
                var restrictFunc = _restrictAccessFuncMap[restrictAccessTo];
                if (restrictFunc != null)
                {
                    restrictFunc(currentResource, originResource);
                }
            }
        }

        private readonly Dictionary<RestrictAccessTo, Action<Resource, Resource>> _restrictAccessFuncMap = new Dictionary<RestrictAccessTo, Action<Resource, Resource>>
        {
            {
                RestrictAccessTo.CriminalRecords,
                (current, origin) =>
                {
                    current.CriminalRecords = origin.CriminalRecords;
                }
            },
            {
                RestrictAccessTo.PreviousEmployments,
                (current, origin) =>
                {
                    current.PreviousEmployments = origin.PreviousEmployments;
                }
            },
            {
                RestrictAccessTo.Salary,
                (current, origin) =>
                {
                    current.HourlyRate = origin.HourlyRate;
                    current.Salary = origin.Salary;
                    current.BonusOrOtherPay = origin.BonusOrOtherPay;
                }
            },
            {
                RestrictAccessTo.PersonalInfo,
                (current, origin) =>
                {
                    current.MartialStatus = origin.MartialStatus;
                    current.Ssn = origin.Ssn;
                    current.LGBT = null;
                    current.Ethnicity = null;
                    current.Gender = null;
                    current.EmergencyContactName = origin.EmergencyContactName;
                    current.EmergencyContactPhoneNumber = origin.EmergencyContactPhoneNumber;
                }
            }
            ,
            {
                RestrictAccessTo.BackgroundChecks,
                (current, origin) =>
                {
                    current.ArmedForces = origin.ArmedForces;
                    current.ArmedForcesBranch = origin.ArmedForcesBranch;
                    current.ArmedForcesCountryId = origin.ArmedForcesCountryId;
                    current.DrugTest = origin.DrugTest;
                    current.CreditReportOk = origin.CreditReportOk;
                    current.MotorVehicleReport = origin.MotorVehicleReport;
                    current.MotorVehicleReportOther = origin.MotorVehicleReportOther;
                    current.ProfessionalReferenceChecks = origin.ProfessionalReferenceChecks;
                    current.ProfessionalReferenceChecksOther = origin.ProfessionalReferenceChecksOther;
                    current.CredentialVerifications = origin.CredentialVerifications;
                    current.CredentialVerificationsOther = origin.CredentialVerificationsOther;
                    current.EmploymentEligibilityVerification = origin.EmploymentEligibilityVerification;
                    current.EmploymentEligibilityVerificationOther = origin.EmploymentEligibilityVerificationOther;
                    current.FormI9 = origin.FormI9;
                    current.FormI9Other = origin.FormI9Other;
                    current.FormEVerify = origin.FormEVerify;
                    current.FormEVerifyOther = origin.FormEVerifyOther;
                    current.InternationalWorkHistory = origin.InternationalWorkHistory;
                    current.InternationalWorkHistoryOther = origin.InternationalWorkHistoryOther;
                    current.FingerPrinting = origin.FingerPrinting;
                    current.FingerPrintingOther = origin.FingerPrintingOther;
                    current.DrugScreening = origin.DrugScreening;
                    current.DrugScreeningOther = origin.DrugScreeningOther;
                    current.CreditCheck = origin.CreditCheck;
                    current.CreditCheckOther = origin.CreditCheckOther;
                }
            }
        };
    }
}
