﻿using System;
using System.Data;
using System.Data.Sql;
using System.Linq;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using System.Collections.Generic;
using System.Text;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle time managment operations And implementing all methods which is initialize in ITimeManagementService.
    /// </summary>
    public class TimeManagementService : ITimeManagementService
    {
        /// <summary>
        ///Description : Time Management Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ///// <summary>
        /////Description : Time Management Service Field IHolidayRepository
        ///// </summary>
        //private readonly IHolidayRepository _holidayRepository;

        ///// <summary>
        /////Description : Time Management Service Field ILeaveTypeRepository
        ///// </summary>
        //private readonly ILeaveTypeRepository _leaveTypeRepository;

        ///// <summary>
        /////Description : Time Management Service Field ILeaveRequestRepository
        ///// </summary>
        //private readonly ILeaveRequestRepository _leaveRequestRepository;

        /// <summary>
        ///Description : Time Management Service Field IUserLoginHistoryRepository
        /// </summary>
        private readonly IUserLoginHistoryRepository _userLoginHistoryRepository;

        ///// <summary>
        /////Description : Time Management Service Field IEntryTypeRepository
        ///// </summary>
        //private readonly IEntryTypeRepository _entryTypeRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Time Management class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="holidayRepository">Holiday Repository</param>
        /// <param name="leaveTypeRepository">Leave Type Repository</param>
        /// <param name="leaveRequestRepository">Leave Request Repository</param>
        /// <param name="userLoginHistoryRepository">User Login History Repository</param>
        /// <param name="entryTypeRepository">Entry Type Repository</param>
        public TimeManagementService(
                IDataContextScopeFactory dataContextScopeFactory,
                //IHolidayRepository holidayRepository,
                //ILeaveTypeRepository leaveTypeRepository,
                //ILeaveRequestRepository leaveRequestRepository,
                IUserLoginHistoryRepository userLoginHistoryRepository
                //IEntryTypeRepository entryTypeRepository
                )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            //_holidayRepository = holidayRepository;
            //_leaveTypeRepository = leaveTypeRepository;
            //_leaveRequestRepository = leaveRequestRepository;
            _userLoginHistoryRepository = userLoginHistoryRepository;
            //_entryTypeRepository = entryTypeRepository;
        }

        //#region Holiday Methods

        //public async Task<bool> UpdateHolidayAsync(Holiday holiday)
        //{
        //    if (holiday == null) throw new ArgumentNullException("holidays");

        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        var originHoliday = _holidayRepository.Read(holiday.Id, new ReadOptions<Holiday>().AsReadOnly());

        //        await _holidayRepository.UpdateAsync(holiday, UpdateOptions.Create<Holiday>()
        //        .WithNotModified(p => p.Created));

        //    }
        //    return true;
        //}

        //public async Task<bool> CreateHolidayAsync(Holiday holiday)
        //{
        //    if (holiday == null) throw new ArgumentNullException("holidays");

        //    await _holidayRepository.CreateAsync(holiday, UpdateOptions.Create<Holiday>()
        //        .WithNotModified(p => p.Created)
        //        .WithNotModified(p => p.Modified));

        //    return true;
        //}

        //public async Task<Holiday> ReadHolidayAsync(long id)
        //{
        //    var readOptions = new ReadOptions<Holiday>().AsReadOnly();

        //    var holiday = await _holidayRepository.ReadAsync(id, readOptions);

        //    return holiday;

        //}

        //public List<Holiday> ReadHolidayMonthlyAsync(long month, long year)
        //{
        //    var holiday = new List<Holiday>();
        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
        //        var dbSet = ctx.Set<Holiday>();
        //        holiday = dbSet.Where(s => s.HolidayDate.Value.Month == month
        //                                       && s.HolidayDate.Value.Year == year).ToList();

        //    }

        //    return holiday;
        //}

        //public List<Holiday> ReadAllHolidayAsync()
        //{
        //    var holidayList = new List<Holiday>();
        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
        //        var dbSet = ctx.Set<Holiday>();
        //        holidayList = dbSet.ToList();

        //    }
        //    return holidayList;
        //}

        //public async Task<bool> DeleteHolidayAsync(long id)
        //{
        //    using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
        //    {
        //        var holidayItem = _holidayRepository.Read(id);

        //        if (holidayItem != null)
        //        {
        //            _holidayRepository.Delete(holidayItem);

        //            await dbContextScope.SaveChangesAsync();
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //#endregion

        //#region LeaveType Methods
        //public async Task<bool> UpdateLeaveTypeAsync(LeaveType leaveType)
        //{
        //    if (leaveType == null) throw new ArgumentNullException("leaveTypes");

        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        var originLeaveType = _leaveTypeRepository.Read(leaveType.Id, new ReadOptions<LeaveType>().AsReadOnly());

        //        await _leaveTypeRepository.UpdateAsync(leaveType, UpdateOptions.Create<LeaveType>()
        //        .WithNotModified(p => p.Created));

        //    }
        //    return true;
        //}
        //public async Task<bool> CreateLeaveTypeAsync(LeaveType leaveType)
        //{
        //    if (leaveType == null) throw new ArgumentNullException("leaveTypes");

        //    await _leaveTypeRepository.CreateAsync(leaveType, UpdateOptions.Create<LeaveType>()
        //        .WithNotModified(p => p.Created)
        //        .WithNotModified(p => p.Modified));

        //    return true;
        //}
        //public async Task<LeaveType> ReadLeaveTypeAsync(long id)
        //{
        //    var readOptions = new ReadOptions<LeaveType>().AsReadOnly();

        //    var leaveType = await _leaveTypeRepository.ReadAsync(id, readOptions);

        //    return leaveType;

        //}
        //public List<LeaveType> SearcLeaveTypeAsync(string search)
        //{
        //    var leaveTypeList = new List<LeaveType>();
        //    if (search != null || search != "")
        //    {
        //        using (var dbContextScope = _dataContextScopeFactory.Create())
        //        {
        //            var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
        //            var dbSet = ctx.Set<LeaveType>();
        //            leaveTypeList = dbSet.Where(x => x.Name.StartsWith(search) || search == null).ToList();
        //        }
        //    }
        //    return leaveTypeList;
        //}
        //public async Task<bool> DeleteLeaveTypeAsync(long id)
        //{
        //    using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
        //    {
        //        var leaveType = _leaveTypeRepository.Read(id);

        //        if (leaveType != null)
        //        {
        //            _leaveTypeRepository.Delete(leaveType);

        //            await dbContextScope.SaveChangesAsync();
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //#endregion

        //#region Leave Request Methods

        //public async Task<bool> SendLeaveRequestAsync(LeaveRequest leaveRequest)
        //{
        //    if (leaveRequest == null) throw new ArgumentNullException("LeaveRequests");

        //    await _leaveRequestRepository.CreateAsync(leaveRequest, UpdateOptions.Create<LeaveRequest>()
        //        .WithNotModified(p => p.Created)
        //        .WithNotModified(p => p.Modified));

        //    return true;
        //}

        //public async Task<LeaveRequest> ReadLeaveRequestByIdAsync(long id)
        //{
        //    var readOptions = new ReadOptions<LeaveRequest>().AsReadOnly();

        //    var leaveRequest = await _leaveRequestRepository.ReadAsync(id, readOptions);

        //    return leaveRequest;

        //}

        //public async Task<bool> ApproveLeaveRequestAsync(LeaveRequest approveLeaveRequest)
        //{
        //    if (approveLeaveRequest == null) throw new ArgumentNullException("LeaveRequests");

        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        var originHoliday = _leaveRequestRepository.Read(approveLeaveRequest.Id, new ReadOptions<LeaveRequest>().AsReadOnly());

        //        await _leaveRequestRepository.UpdateAsync(approveLeaveRequest, UpdateOptions.Create<LeaveRequest>()
        //            .WithNotModified(p => p.Created));

        //    }
        //    return true;
        //}

        //public string GetStringForLeaveRequestList(long resourceid, bool isSupervisor)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("select lr.Id, lr.created,");
        //    sb.Append("\r\n");
        //    sb.Append("	lr.leave_type_id as leaveTypeId,lt.total_leaves as totalLeaves,lt.name as leavetitle,");
        //    sb.Append("\r\n");
        //    sb.Append("	isnull((select sum(datediff(day,lrinner.start_date, lrinner.end_date))  ");
        //    sb.Append("\r\n");
        //    sb.Append("			from leave_requests lrinner  ");
        //    sb.Append("\r\n");
        //    sb.Append("			where lrinner.leave_type_id = lr.leave_type_id ");
        //    sb.Append("\r\n");
        //    sb.Append("				and lrinner.resource_id = lr.resource_id ");
        //    sb.Append("\r\n");
        //    sb.Append("				and status = 'Approved'),0) as takenleaves,");
        //    sb.Append("\r\n");
        //    sb.Append("	(isnull(lt.total_leaves,0)");
        //    sb.Append("\r\n");
        //    sb.Append("		-isnull((select sum(datediff(day,lrinner.start_date, lrinner.end_date))  ");
        //    sb.Append("\r\n");
        //    sb.Append("				from leave_requests lrinner  ");
        //    sb.Append("\r\n");
        //    sb.Append("				where lrinner.leave_type_id = lr.leave_type_id ");
        //    sb.Append("\r\n");
        //    sb.Append("					and lrinner.resource_id = lr.resource_id ");
        //    sb.Append("\r\n");
        //    sb.Append("					and status = 'Approved'");
        //    sb.Append("\r\n");
        //    sb.Append("				),0");
        //    sb.Append("\r\n");
        //    sb.Append("			)");
        //    sb.Append("\r\n");
        //    sb.Append("	) as remainingleave,");
        //    sb.Append("\r\n");
        //    sb.Append("	case when lr.status = 'Pending' then 0 when lr.status='Approved' then 1 when lr.status = 'Rejected' then 2 end as statusNumber,");
        //    sb.Append("\r\n");
        //    sb.Append("	lr.status as Status,	");
        //    sb.Append("\r\n");
        //    sb.Append("	lr.start_date as startdate, lr.end_date as enddate, lt.is_paid as ispaidtype, 	");
        //    sb.Append("\r\n");
        //    sb.Append("	lr.supervisor_id as supervisorid, (super.last_name +' '+ super.first_name) as supervisorname,");
        //    sb.Append("\r\n");
        //    sb.Append("	lr.resource_id as resourceid, (res.last_name +' '+ res.first_name) as resourcename,	");
        //    sb.Append("\r\n");
        //    sb.Append("	lr.status_modified_date as statusmodifieddate, ");
        //    sb.Append("\r\n");
        //    sb.Append("	lr.reason as reason,");
        //    sb.Append("\r\n");
        //    sb.Append("	lr.comment_from_supervisor as commentfromsupervisor, ");
        //    sb.Append("\r\n");
        //    sb.Append("	lr.canceled ");
        //    sb.Append("\r\n");
        //    sb.Append("from leave_requests lr ");
        //    sb.Append("\r\n");
        //    sb.Append("	LEFT JOIN leave_types lt ON lr.leave_type_id = lt.Id ");
        //    sb.Append("\r\n");
        //    sb.Append("	LEFT JOIN resources super ON lr.supervisor_id = super.Id ");
        //    sb.Append("\r\n");
        //    sb.Append("	LEFT JOIN resources res ON lr.resource_id = res.Id  ");
        //    sb.Append("\r\n");
        //    if (isSupervisor == true)
        //    {
        //        sb.AppendFormat("where lr.supervisor_id = {0}", resourceid);
        //    }
        //    else
        //    {
        //        sb.AppendFormat("where lr.resource_id = {0}", resourceid);
        //    }
        //    sb.Append("\r\n");
        //    sb.Append("order by statusNumber,lr.created desc");
        //    sb.Append("\r\n");

        //    return sb.ToString();
        //}

        //public async Task<bool> UpdateLeaveRequestAsync(LeaveRequest leaveRequest)
        //{
        //    if (leaveRequest == null) throw new ArgumentNullException("leaveRequests");

        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        var originLeaveType = _leaveRequestRepository.Read(leaveRequest.Id, new ReadOptions<LeaveRequest>().AsReadOnly());

        //        await _leaveRequestRepository.UpdateAsync(leaveRequest, UpdateOptions.Create<LeaveRequest>()
        //        .WithNotModified(p => p.Created));

        //    }
        //    return true;
        //}
        //#endregion

        #region UserLoginHistory Method

        //public async Task<bool> UpdateUserLoginHistoryAsync(UserLoginHistory userLoginHistory)
        //{
        //    if (userLoginHistory == null) throw new ArgumentNullException("UserLoginHistorys");

        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        var originuserLoginHistory = _userLoginHistoryRepository.Read(userLoginHistory.Id, new ReadOptions<UserLoginHistory>().AsReadOnly());

        //        await _userLoginHistoryRepository.UpdateAsync(userLoginHistory, UpdateOptions.Create<UserLoginHistory>()
        //        .WithNotModified(p => p.Created));

        //    }
        //    return true;
        //}

        /// <summary>
        /// Method Description : Using for Create user login history. 
        /// </summary>
        /// <param name="userLoginHistory">User Login History Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateuserLoginHistoryAsync(UserLoginHistory userLoginHistory)
        {
            if (userLoginHistory == null) throw new ArgumentNullException("userLoginHistorys");

            await _userLoginHistoryRepository.CreateAsync(userLoginHistory, UpdateOptions.Create<UserLoginHistory>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        #endregion

        //#region Entry Type Methods
        //public async Task<bool> UpdateEntryTypeAsync(EntryType entryType)
        //{
        //    if (entryType == null) throw new ArgumentNullException("entrytypes");

        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        var originLeaveType = _entryTypeRepository.Read(entryType.Id, new ReadOptions<EntryType>().AsReadOnly());

        //        await _entryTypeRepository.UpdateAsync(entryType, UpdateOptions.Create<EntryType>()
        //        .WithNotModified(p => p.Created));

        //    }
        //    return true;
        //}
        //public async Task<bool> CreateEntryTypeAsync(EntryType entryType)
        //{
        //    if (entryType == null) throw new ArgumentNullException("entryTypes");

        //    await _entryTypeRepository.CreateAsync(entryType, UpdateOptions.Create<EntryType>()
        //        .WithNotModified(p => p.Created)
        //        .WithNotModified(p => p.Modified));

        //    return true;
        //}
        //public async Task<EntryType> ReadEntryTypeAsync(long id)
        //{
        //    var readOptions = new ReadOptions<EntryType>().AsReadOnly();

        //    var entryType = await _entryTypeRepository.ReadAsync(id, readOptions);

        //    return entryType;

        //}
        //public List<EntryType> SearcEntryTypeAsync(string search)
        //{
        //    var entryTypeList = new List<EntryType>();
        //    if (search != null || search != "")
        //    {
        //        using (var dbContextScope = _dataContextScopeFactory.Create())
        //        {
        //            var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
        //            var dbSet = ctx.Set<EntryType>();
        //            entryTypeList = dbSet.Where(x => x.Name.StartsWith(search) || search == null).ToList();
        //        }
        //    }
        //    return entryTypeList;
        //}
        //public async Task<bool> DeleteEntryTypeAsync(long id)
        //{
        //    using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
        //    {
        //        var entryType = _entryTypeRepository.Read(id);

        //        if (entryType != null)
        //        {
        //            _entryTypeRepository.Delete(entryType);

        //            await dbContextScope.SaveChangesAsync();
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //#endregion                
    }
}
