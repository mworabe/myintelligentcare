﻿using System.Collections.Generic;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Custom Field.
    /// </summary>
    public class CustomField : NamedEntity
    {
        public bool ResourceAvailable { get; set; }

        public CustomFieldTypeEnum Type { get; set; }
        public bool Require { get; set; }
        public string DefaultValue { get; set; }
        public string Description { get; set; }
        public bool IsSearchable { get; set; }

        public virtual ICollection<ResourceCustomFieldValue> ResourceCustomFieldValues { get; set; }
        public virtual ICollection<CustomFieldProperty> CustomFieldProperties { get; set; }
    }
}