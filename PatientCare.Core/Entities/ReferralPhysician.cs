﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Referral Physician.
    /// </summary>
    public class ReferralPhysician : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Speciality { get; set; }
        public MedicalDesignationEnum? MedicalDesignation { get; set; }
        public string MedicalDesignationOther { get; set; }
        public string PhoneNumber { get; set; }
        public string ExtensionNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string FacilityName { get; set; }

        public string PostalCode { get; set; }

        public long? CityId { get; set; }
        public virtual City City { get; set; }

        public long? StateId { get; set; }
        public virtual State State { get; set; }

        public long? CountryId { get; set; }
        public virtual Country Country { get; set; }

        public string Name
        {
            get { return string.Concat(LastName ?? string.Empty, " ", FirstName ?? string.Empty); }
        }

        //public string Location
        //{
        //    get { return string.Concat(City.Name ?? string.Empty, ",", State.Name ?? string.Empty); }
        //}
    }
}
