﻿using FluentValidation;
using PatientCare.Web.Api.Models.InjuryRegions;

namespace PatientCare.Web.Api.Models.DropDownConfigs
{
    public class InjuryRegionRequestValidator : AbstractValidator<InjuryRegionRequest>
    {
        public InjuryRegionRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);            
        }
    }
}