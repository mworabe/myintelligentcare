namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedColumnInPatinetTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "patient_status", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.patients", "patient_status");
        }
    }
}
