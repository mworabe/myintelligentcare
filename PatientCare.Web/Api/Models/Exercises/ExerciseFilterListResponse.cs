﻿using PatientCare.Core.Enums;
using PatientCare.Web.Services.FileStorage;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Exercises
{
    [DataContract]
    public class ExerciseFilterListResponse
    {
        [DataMember]
        public long? ExerciseId { get; set; }
                
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Keywords { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public long? ExerciseMediaId { get; set; }

        [DataMember]
        public string OriginalName { get; set; }

        [DataMember]
        public string MediasName { get; set; }

        [DataMember]
        public MediaType? MediaType { get; set; }

        [DataMember]
        public string MediaURL { get; set; }

        [DataMember]
        public bool IsLocal
        {
            get { return (StorageService.UseAmazonStorage); }
            set { IsLocal = value; }
        }
        [DataMember]
        public string MediaOriginalName { get; set; }

        [DataMember]
        public FileRequest[] Files { get; set; }

        [DataMember]
        public long? set { get; set; }

        [DataMember]
        public long? reps { get; set; }

        [DataMember]
        public long? holds { get; set; }

        [DataMember]
        public long? weight { get; set; }

        [DataMember]
        public string weightUnit { get; set; }

        [DataMember]
        public string holdUnit { get; set; }

        [DataMember]
        public string frequency { get; set; }
    }
}