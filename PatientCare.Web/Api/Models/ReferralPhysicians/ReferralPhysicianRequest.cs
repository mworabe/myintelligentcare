﻿using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.ReferralPhysicians
{
    public class ReferralPhysicianRequest
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Speciality { get; set; }

        public MedicalDesignationEnum? MedicalDesignation { get; set; }

        public string MedicalDesignationOther { get; set; }

        public string PhoneNumber { get; set; }

        public string ExtensionNumber { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string FacilityName { get; set; }

        public string PostalCode { get; set; }

        public string PostalCodeTemp { get; set; }

        public long? CountryId { get; set; }

        public long? StateId { get; set; }

        public long? CityId { get; set; }

    }   
}