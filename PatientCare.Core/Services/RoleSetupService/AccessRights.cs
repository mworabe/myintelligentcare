﻿using System.Collections.Generic;

namespace PatientCare.Core.Services.RoleSetupService
{
    public class AccessRights
    {
        public AccessRights()
        {
            Sections = new List<AccessItem<Section>>(10);
            ConfigurationSections = new List<AccessItem<ConfigurationSection>>(5);
            Picklists = new List<Picklist>(15);
            RestrictAccess = new List<RestrictAccessTo>(5);
        }
        public IList<AccessItem<Section>> Sections { get; set; }

        public IList<AccessItem<ConfigurationSection>> ConfigurationSections { get; set; }

        public IList<RestrictAccessTo> RestrictAccess { get; set; }

        public IList<Picklist> Picklists { get; set; }

        public HomePageDynamic MainHomePage { get; set; }

        public bool? OnlyForDivision { get; set; }

        public bool IsForDivisionOnly { get { return OnlyForDivision ?? false; } }

        public bool? OnlyForDepartment { get; set; }

        public bool IsForDepartmentOnly { get { return OnlyForDepartment ?? false; } }
    }

    public class HomePageDynamic
    {
        public string Value { get; set; }
        public string name { get; set; }
        public StateParams StateParams { get; set; }
    }

    public class StateParams { public long Id { get; set; } }
}
