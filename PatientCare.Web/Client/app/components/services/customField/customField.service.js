﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("CustomField", [
        "$resource", "$state", "edzCustomFieldEditHelper", "CUSTOM_FIELD", function ($resource, $state, edzCustomFieldEditHelper, CUSTOM_FIELD) {
            var resource = $resource("api/CustomFields/:id/:controller",
                null, //parameters default
                {
                    'update': { method: "PUT" },
                    'query': { method: "GET", params: {}, isArray: false },

                    'customFieldsForResource': { method: "GET", params: { controller: "custom-fields-for-resource" }, isArray: true },
                    'customFieldsForProject': { method: "GET", params: { controller: "custom-fields-for-project" }, isArray: true },
                    'customFieldsForProjectTask': { method: "GET", params: { controller: "custom-fields-for-project-task" }, isArray: true },
                    'customFieldForIntake': { method: "GET", params: { controller: "custom-fields-for-intake" }, isArray: true },

                    'customFieldValuesByResource': { method: "GET", params: { controller: "custom-field-values-for-resource" }, isArray: true },
                    'customFieldValuesByProject': { method: "GET", params: { controller: "custom-field-values-by-project" }, isArray: true },
                    'customFieldValuesByProjectTask': { method: "GET", params: { controller: "custom-field-values-by-project-task" }, isArray: true },
                    'customFieldValuesByIntake': { method: "GET", params: { controller: "custom-field-values-by-intake" }, isArray: true },

                    //'customFieldValuesByAssignment': { method: "GET", params: { controller: "custom-field-values-for-assignment" }, isArray: true },

                });

            resource.getTableOption = {
                pageName: "Custom Fields",
                hideBulkEdit: true,
                cols: function () {
                    return [
                        { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                        { field: "name", title: "Name", sortable: "name", show: true, dataType: "text", required: true },
                        { field: "description", title: "Desicription", sortable: "description", show: true, dataType: "text", required: true },
                        { field: "type", title: "Type", sortable: "type", show: true, dataType: "text", required: true },
                        {
                            field: "require", title: "Required", sortable: "require", show: true, dataType: "text", required: true,
                            customViewText: function (row) {
                                return row.require ? "Yes" : "No";
                            }
                        }, {
                            field: "", title: "Forms where used", sortable: false, show: true, dataType: "text", required: true,
                            customViewText: function (row) {
                                var whereUsed = [];
                                if (row.projectAvailable)
                                    whereUsed.push("Project");
                                if (row.resourceAvailable)
                                    whereUsed.push("Resource");
                                if (row.taskAvailable)
                                    whereUsed.push("Task");
                                if (row.intakeAvailable)
                                    whereUsed.push("Intake");
                                return whereUsed.join(", ");

                            }
                        }, {
                            field: "", title: "Length", sortable: false, show: true, dataType: "text", required: true,
                            customViewText: function (row) {
                                var property = edzCustomFieldEditHelper.getPropertyByName(row.customFieldProperties, CUSTOM_FIELD.PROPERTY_NAME.GENERAL.LENGTH);
                                if (!property)
                                    return "";
                                return property.value;

                                //CUSTOM_FIELD.PROPERTY_NAME.NUMERIC.NUMBER_OF_DECIMALS
                            }
                        }, {
                            field: "", title: "Decimals", sortable: false, show: true, dataType: "text", required: true,
                            customViewText: function (row) {
                                var property = edzCustomFieldEditHelper.getPropertyByName(row.customFieldProperties, CUSTOM_FIELD.PROPERTY_NAME.NUMERIC.NUMBER_OF_DECIMALS);
                                if (!property)
                                    return "";

                                return property.value;

                            }
                        },
                        {
                            field: "isSearchable", title: "Searchable", sortable: "isSearchable", show: true, dataType: "text", required: true,
                            customViewText: function (row) {
                                return row.isSearchable ? "Yes" : "No";
                            }
                        },
                        { field: "action", title: ".", dataType: "command" },
                    ];
                },
                onAdd_Click: function () {
                    $state.go("customFieldEdit");
                },
                onEdit_Click: function (row) {
                    $state.go("customFieldEdit", { id: row.id });
                },
                headerButtons: [
                    {
                        text: "Default Layout",
                        click: function () {
                            $state.go("configLayout");
                        },
                        iconClass: "layout"
                    }
                ]
            };
            return resource;
        }
    ]);