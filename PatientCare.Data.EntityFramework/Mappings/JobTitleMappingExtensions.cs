﻿using System;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class JobTitleMappingExtensions
    {
        public static DbModelBuilder MapJobTitle(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.MapPicklistInfo<JobTitle>("job_titles", "ix_job_title_name_unique");

            modelBuilder.Entity<JobTitle>()
                .Property(p => p.Rate)
                .HasColumnName("rate")
                .IsOptional();

            modelBuilder.Entity<JobTitle>()
                .Property(p => p.PriorityOrder)
                .HasColumnName("priority_order")
                .IsOptional();

            modelBuilder.Entity<JobTitle>()
                .Property(p => p.HolidayEligible)
                .HasColumnName("holiday_eligible")                
                .IsOptional();

            modelBuilder.Entity<JobTitle>()
                .Property(p => p.HoursEdit)
                .HasColumnName("hours_edit")
                .IsOptional();

            modelBuilder.Entity<JobTitle>()
               .Property(p => p.EntryTypeId)
               .HasColumnName("entry_type_id")
               .IsOptional();

            modelBuilder.Entity<JobTitle>()
                .HasOptional<EntryType>(p => p.EntryType)
                .WithMany()
                .HasForeignKey(p => p.EntryTypeId);

            return modelBuilder;
        }
    }
}
