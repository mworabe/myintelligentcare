﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("edzDynamicFormControllerCompiler", [
        "ConfigLayout", "$templateRequest", "$compile", "edzDynamicFormControllerTemplateFactory", 'DYNAMIC_FORM_CONSTROLLER',
        function (configLayoutFactory, $templateRequest, $compile, edzDynamicFormControllerTemplateFactory, DYNAMIC_FORM_CONSTROLLER) {
            return {
                restrict: "E",
                scope: false,
                require: "^form",
                link: function (scope, el, attrs, form) {
                    scope.__controllerName = attrs.edzControllerName;
                    scope.appForm = form;
                    var fieldId = attrs.edzFieldId;
                    var formName = attrs.form;
                    var generalPropertyList = scope[scope.__controllerName].generalPoropertyList;

                    scope.field = _.find(generalPropertyList, { id: fieldId });


                    if (scope.field && scope.field.type == DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME) {
                        scope.onDateChange = function ($event) {
                            var fieldValue = scope[scope.__controllerName]['model'][scope.field.id];
                            var textValue = $event.target.value;
                            var isValid = moment(textValue, "MM/DD/YYYY", true).isValid();
                            var control = scope.field.id;
                            var obj = scope.appForm[control];
                            if (textValue && !isValid) {
                                obj.$setValidity("invalid",false)
                                //obj.$valid = false;
                                //obj.invalid = true;
                                //obj.$error.invalid = true;
                            } else if (textValue && isValid) {
                                obj.$setValidity("invalid", true);
                            } else if (!textValue && !scope.field.required) {
                                obj.$setValidity("invalid", true);
                            }

                        }
                    }

                    if (scope.field && scope.field.type == DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT) {

                        var cntrName = scope.__controllerName;
                        var enumId = fieldId + "_Enum";
                        var enumFields = scope[cntrName][enumId];

                        var enumFieldValue = angular.copy(enumFields);

                        var enumFields = [];
                        if (enumFieldValue && enumFieldValue.length > 0) {
                            var firstObject = enumFieldValue[0];
                            if (typeof firstObject == 'string') {
                                for (var i = 0; i < enumFieldValue.length; i++) {
                                    var obj = {
                                        "title": enumFieldValue[i],
                                        "value": enumFieldValue[i]
                                    }
                                    enumFields.push(obj);
                                }

                                scope.field.specialName = "title";
                                scope.field.specialValue = "value";

                                scope[cntrName][enumId] = enumFields;
                            }

                        }

                        scope.onUISelect = function (object) {
                            if (object && object.originalObject) {
                                scope[scope.__controllerName]['model'][scope.field.id] = (object.originalObject.value ? object.originalObject.value : object.originalObject.id);
                            }
                        }

                        scope.getDropdownDisplayValue = function (value) {

                            var cntrName = scope.__controllerName;
                            var enumId = fieldId + "_Enum";
                            var enumFields = scope[cntrName][enumId];
                            var enumFieldValue = angular.copy(enumFields);
                            var fieldValueObject = _.find(enumFields, { 'value': value });

                            if (!scope.field.specialName || !enumFieldValue || !fieldValueObject) {
                                return value;// for server dropdown
                            } else {
                                return fieldValueObject[scope.field.specialName];
                            }
                        }
                    }
                    el.bind('keydown', function (e) {
                        var code = e.keyCode || e.which;
                        if (code === 13) {
                            var currentElem = el.querySelectorAll('.tabable')[0];
                            if (currentElem.type && currentElem.type === "button") {
                                return;
                            }
                            e.preventDefault();
                            var domForm = el.closest('form');

                            var fieldsList = domForm.querySelectorAll('.tabable');

                            _.forEach(fieldsList, function (field, key) {
                                if (field.name && field.name === currentElem.name) {
                                    var nextField = fieldsList[(key + 1)];
                                    if (nextField)
                                        nextField.focus();
                                    return;
                                }
                            });
                        }
                    });


                    if (scope.field) {
                        $templateRequest(edzDynamicFormControllerTemplateFactory(scope.field), true).then(function (html) {
                            el.html(html);
                            $compile(el.contents())(scope);
                        });
                    } else {
                        var customFields = scope[scope.__controllerName].model.customFieldValues;
                        scope.field = _.find(customFields, { customFieldId: parseInt(fieldId) });

                        if (scope.field) {
                            $templateRequest("Client/app/components/dynamicFormController/temlates/customField.html", true).then(function (html) {
                                el.html(html);
                                $compile(el.contents())(scope);
                            });

                        }
                    }
                }
            };
        }
    ]);