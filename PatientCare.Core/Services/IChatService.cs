﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Services
{
    public interface IChatService
    {
        #region Chat Session

        Task<ChatSession> ReadChatAsync(long id);

        Task<bool> DeleteChatAsync(long id);

        Task<bool> InsertUpdateChatAsync(ChatSession chatsession);

        #endregion

        #region Chat Key

        Task<bool> InsertUpdateChatKeyAsync(ChatKey chatKey);

        Task<ChatKey> ReadChatKeyAsync(long id);

        #endregion
    }
}
