﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Resources
{
    [DataContract]
    public class ResourceListWithSupervisorResponse
    {
        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public string ResourceName { get; set; }

        [DataMember]
        public long? SupervisorId { get; set; }

        [DataMember]
        public string SupervisorName { get; set; }
    }
}