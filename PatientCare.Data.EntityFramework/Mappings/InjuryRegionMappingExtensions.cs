﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class InjuryRegionMappingExtensions
    {
        public static DbModelBuilder MapInjuryRegion(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<InjuryRegion>().ToTable("injury_regions").HasKey(i => i.Id);

            modelBuilder.Entity<InjuryRegion>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<InjuryRegion>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<InjuryRegion>()
                 .Property(p => p.Name)
                 .HasColumnName("name")
                 .IsOptional();
            return modelBuilder;
        }
    }
}
