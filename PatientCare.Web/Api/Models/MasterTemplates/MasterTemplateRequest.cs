﻿using PatientCare.Web.Api.Models.MasterTemplateExercises;

namespace PatientCare.Web.Api.Models.MasterTemplates
{
    public class MasterTemplateRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long? OwnerId { get; set; }

        public bool? IsMaster { get; set; }

        public bool? IsFavorite { get; set; }

        public bool? IsPublic { get; set; }

        public bool? IsDeleted { get; set; }

        public MasterTemplateExerciseRequest[] TemplateExercises { get; set; }
    }   
}