﻿using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceSkills
{
    [DataContract]
    public class PicklistResponseWithSkills
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public PicklistResponse Skill { get; set; }

        [DataMember]
        public long SkillId { get; set; }

        [DataMember]
        public string name
        {
            get { return (Skill != null ? Skill.Name : ""); }
            set { name = value; }
        }
        
        [DataMember]
        public int YearsOfExpirience { get; set; }

        [DataMember]
        public double Proficiency { get; set; }

        [DataMember]
        public long ResourceId { get; set; }
    }
}