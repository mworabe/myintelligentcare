namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveNameFieldFromPatientCaseTable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.patients_cases", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.patients_cases", "Name", c => c.String());
        }
    }
}
