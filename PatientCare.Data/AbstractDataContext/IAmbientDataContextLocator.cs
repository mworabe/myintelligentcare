﻿namespace PatientCare.Data.AbstractDataContext
{
    public interface IAmbientDataContextLocator
    {
        T Get<T>() where T : class;
        void Set(IDataContextScope dataContextScope);
    }
}
