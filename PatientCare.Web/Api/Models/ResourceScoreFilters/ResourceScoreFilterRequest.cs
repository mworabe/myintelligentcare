﻿namespace PatientCare.Web.Api.Models.ResourceScoreFilters
{
    public class ResourceScoreFilterRequest
    {
        public string Name { get; set; }

        public string JsonFilter { get; set; }

        public string UserId { get; set; }

        public string FilterType { get; set; }
    }
}