﻿using System;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using System.Web.Http.Description;
using Swashbuckle.Swagger.Annotations;
using PatientCare.Core.Mapping;
using PatientCare.Utilities;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Web.Api.Models.Picklist;
using System.Threading.Tasks;
using PatientCare.Web.Api.Models;
using System.Linq;
using PatientCare.Web.Api.Models.Countries;
using System.Collections.Generic;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// his Controller is used to handle Countries operations.
    /// </summary>
    [Authorize]
    //[RoutePrefix("api/Countries")]
    public sealed class CountriesController : EdzPicklistApiController<Country>
    {
        private PatientCareDbContext db = new PatientCareDbContext();

        /// <summary>
        /// </summary>
        /// <param name="picklistRepository">Country Entity</param>
        public CountriesController(IPicklistRepository<Country> picklistRepository)
            : base(picklistRepository)
        {
            GetPicklistStatusCodeErrorFunc = () => GetPicklistStatusCodeError(Picklist.Countries);
        }

        protected override Uri GetPicklistLocation(long id)
        {
            return new Uri("/Countries/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Country by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Country Response Model</returns>
        [HttpGet]
        [Route("api/Countries/{id}")]
        [ResponseType(typeof(CountryResponse))]
        public IHttpActionResult GetCountryItem(long id)
        {
            var item = PicklistRepository.Read(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Country, CountryResponse>(item));
        }

        /// <summary>
        /// Method Description : Using for Update Country.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="itemVm">Country Request Model</param>
        /// <returns>Country Response Model</returns>
        [HttpPut]
        [ResponseType(typeof(CountryResponse))]
        [Route("api/Countries/{id}")]
        public IHttpActionResult EditItem(long id, [FromBody]CountryRequest itemVm)
        {
            var statusCodeResult = GetPicklistStatusCodeErrorFunc();
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (PicklistRepository.Exists(FilterQuery.Create<Country>().AddFilter(p => p.Name == itemVm.Name && p.Id != id)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<Country>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var item = PicklistRepository.Read(id);
            item.Name = itemVm.Name;

            PicklistRepository.Update(item, new UpdateOptions<Country>().WithNotModified(p => p.Created));

            return Ok(Mapper.Map<Country, CountryResponse>(item));
        }

        /// <summary>
        /// Method Description : Using for Create Country.    
        /// </summary>
        /// <param name="itemVm">Country Request Model</param>
        /// <returns>Country Response Model</returns>
        [HttpPost]
        [Route("api/Countries")]
        [ResponseType(typeof(CountryResponse))]
        [SwaggerResponse(201, "Created", typeof(PicklistResponse))]
        public IHttpActionResult CreateItem([FromBody]CountryRequest itemVm)
        {
            var statusCodeResult = GetPicklistStatusCodeErrorFunc();
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (PicklistRepository.Exists(FilterQuery.Create<Country>().AddFilter(p => p.Name == itemVm.Name)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<Country>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var item = Mapper.Map<CountryRequest, Country>(itemVm);

            PicklistRepository.Create(item, UpdateOptions.Create<Country>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            var reponse = PicklistRepository.Read(item.Id);

            return Created(GetPicklistLocation(item.Id), Mapper.Map<Country, CountryResponse>(reponse));
        }

        /// <summary>
        /// Method Description : Using for Delete Country by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Country Response Model</returns>
        [HttpDelete]
        [Route("api/Countries/{id}")]
        [ResponseType(typeof(CountryResponse))]
        public IHttpActionResult DeleteCountryItem(long id)
        {
            var statusCodeResult = GetPicklistStatusCodeErrorFunc();
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            var item = PicklistRepository.Read(id);
            if (item == null)
            {
                return NotFound();
            }
            try
            {
                PicklistRepository.Delete(item);
            }
            catch (AggregateException ex)
            {
                ModelState.AddModelError("", string.Join(", ", ex.InnerExceptions.Select(x => x.Message)));
            }

            return Ok(item);
        }

        /// <summary>
        /// Method Description : Using for Get Country List.    
        /// </summary>
        /// <param name="request">List Request</param>
        /// <param name="dropdowntype">Dropdown Type Field</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Countries")]
        [ResponseType(typeof(CountryResponse))]
        public IHttpActionResult CountryList([FromUri]ListRequest request, string dropdowntype = null)
        {
            request.SortField = "name";
            request.SearchField = "name";
            var query = PagingExtensions<Country>.CreatePagedQuery(request);
            var page = PicklistRepository.PagedList(query);
            var dtos = Mapper.Map<Country[], CountryResponse[]>(page.Entities.ToArray());
            var response = new ListResponse<CountryResponse>(dtos, page.TotalCount);
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Country Type.      
        /// </summary>
        /// <param name="search">Search</param>
        /// <param name="cityId">City Id</param>
        /// <param name="stateId">State Id</param>
        /// <returns>Country Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/Countries/autocomplete")]
        [ResponseType(typeof(CountryResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search, long? cityId = null, long? stateId = null)
        {
            var filter = new FilterQuery<Country>();
            if (!string.IsNullOrWhiteSpace(search))
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));

            long? countryId = null;
            if (cityId != null && stateId != null)
                countryId = db.Cities.AsNoTracking().Where(x => x.Id == cityId && x.StateId == stateId).Select(x => x.CountryId).FirstOrDefault();
            if (cityId == null && stateId != null)
                countryId = db.States.AsNoTracking().Where(x => x.Id == stateId).Select(x => x.CountryId).FirstOrDefault();
            if (cityId != null && stateId == null)
                countryId = db.Cities.AsNoTracking().Where(x => x.Id == cityId).Select(x => x.CountryId).FirstOrDefault();
            if (countryId != null)
                filter.AddFilter(x => x.Id == countryId);

            var autocompleteResult = await PicklistRepository.ListAsync(filter);

            var result = Mapper.Map<Country[], CountryResponse[]>(autocompleteResult.ToArray());



            List<CountryResponse> listData = result.ToList();
            var item = listData.Where(x => x.Name.StartsWith("United States")).FirstOrDefault();
            listData.OrderBy(x => x.Name);
            if (item != null)
            {
                var oldIndex = listData.IndexOf(item);
                listData.RemoveAt(oldIndex);
                listData.Insert(0, item);

                if (listData != null)
                    listData = listData.Take(20).ToList();

                return Ok(listData);
            }

            if (result != null)
                result = result.Take(20).ToArray();

            return Ok(result);
        }
    }
}