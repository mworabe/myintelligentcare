﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Clinic Location.
    /// </summary>
    public class ClinicLocation : NamedEntity
    {
        public long? ClinicId { get; set; }
        public virtual Clinic Clinic { get; set; }

        public string ContactPerson { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string LocationPrefix { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
