﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourcePracticeAreaRepository : RepositoryBase<ResourcePracticeArea>, IResourcePracticeAreaRepository
    {
        public ResourcePracticeAreaRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
