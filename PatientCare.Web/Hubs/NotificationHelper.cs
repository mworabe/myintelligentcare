﻿using Microsoft.AspNet.SignalR;
using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Api.Models.PatientAppointment;
using PatientCare.Web.Api.Models.Schedulers;
using System.Threading.Tasks;

namespace PatientCare.Web.Hubs
{
    public class NotificationHelper
    {
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        public NotificationHelper(IDataContextScopeFactory dataContextScopeFactory)
        {
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        public interface IUserIdProvider
        {
            string GetUserId(IRequest request);
        }

        public async Task<bool> pushAppointmentUpdate(string Title, long AppointmentId)
        {
            
            SchedulerListResponse appointments = new SchedulerListResponse();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_get_single_appointment_signalR @appointmentId = '" + AppointmentId + "'";
                appointments = await ctx.Database.SqlQuery<SchedulerListResponse>(query).SingleOrDefaultAsync();
            }

            NotificationResponse Response = new NotificationResponse();
            Response.Title = Title;
            Response.DataObject = appointments;
            Response.NotifType = "Appointment";
            var hubs = GlobalHost.ConnectionManager.GetHubContext<AppointmentBroadCasterHub>();
            hubs.Clients.All.broadcastAppointmentUpdate(Response);
            return true;
        }

        public async Task<bool> sendPainRatingNotif(string Title, Patient dataObj)
        {

            NotificationResponse Response = new NotificationResponse();
            Response.Title = Title;
            Response.DataObject = dataObj.Id;
            Response.NotifType = "PainRatings";
            var hubs = GlobalHost.ConnectionManager.GetHubContext<AppointmentBroadCasterHub>();
            hubs.Clients.All.broadCastPainRating(Response);
            return true;
        }


    }
}