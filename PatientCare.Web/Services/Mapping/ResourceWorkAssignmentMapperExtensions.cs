﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.ResourceWorkAssignment;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceWorkAssignmentMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceWorkAssignementRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkAssignment, ResourceWorkAssignmentResponse>
                (new ExtendedMapConfig<ResourceWorkAssignment, ResourceWorkAssignmentResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkAssignment[], ResourceWorkAssignmentResponse[]>());
            
            mapperProvider.RegisterMapper(() =>
               ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkAssignmentRequest, ResourceWorkAssignment>
                    (new ExtendedMapConfig<ResourceWorkAssignmentRequest, ResourceWorkAssignment>()));

            
            return mapperProvider;
        }
    }
}