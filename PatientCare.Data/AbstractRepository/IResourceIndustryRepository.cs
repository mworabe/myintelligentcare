﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceIndustryRepository : IRepository<ResourceIndustry>
    {
    }
}
