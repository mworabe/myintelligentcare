﻿using FluentValidation;

namespace PatientCare.Web.Api.Models.DashboardSetups
{
    public class DashboardSetupRequestValidator : AbstractValidator<DashboardSetupRequest>
    {
        public DashboardSetupRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);

            //RuleFor(model => model.Id)
            //    .NotEmpty()
            //    .WithMessage("Dashboard is not assigned. contact your administrator.");
        }
    }
}