﻿using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;


namespace PatientCare.Core.Entities
{
    public class Exercise_ExerciseActivity : Entity
    {
        public long? exercise_Id { get; set; }
        public virtual Exercise exercise { get; set; }

        public long? activity_Id { get; set; }
        public virtual ExerciseActivity activity { get; set; }
    }
}
