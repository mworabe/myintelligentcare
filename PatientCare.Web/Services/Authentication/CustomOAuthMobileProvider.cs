﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using PatientCare.Data.EntityFramework.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using PatientCare.Web.Api.Controllers;
using PatientCare.Core.Entities;
using System;
using PatientCare.Web.ApiMobile.Models.Patients;
using PatientCare.Core.Enums;
using System.Collections.Generic;

namespace PatientCare.Web.Services.Authentication
{
    public class CustomOAuthMobileProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

            if (allowedOrigin == null) allowedOrigin = "*";
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            //var allowedOrigin = "*";
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            var roleManager = context.OwinContext.GetUserManager<ApplicationRoleManager>();

            var user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            if (user.Patient != null && (user.Patient.IsDeleted == true))
            {
                context.SetError("invalid_grant", "Patient is inactive, contact to administrator.");
                return;
            }

            if (user.Patient != null && (user.Patient.Status != null) && (user.Patient.Status == PatientStatusEnum.InActive))
            {
                context.SetError("invalid_grant", "Patient is inactive, contact to administrator.");
                return;
            }

            if (user != null)
            {
                LoginHistory temp = new LoginHistory();
                bool isExpiry = false;
                isExpiry = temp.GetUserExpiryDate();

                if (isExpiry == true)
                {
                    context.SetError("invalid_grant", "The user trial period has expired.");
                    return;
                }
            }

            var oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, "JWT");

            var props = new AuthenticationProperties();

            LoginHistory controller = new LoginHistory();
            UserLoginHistory userLoginHistoryRequest = new UserLoginHistory();
            userLoginHistoryRequest.UserId = user.Id;
            userLoginHistoryRequest.Action = "Login";
            userLoginHistoryRequest.LogDateTime = DateTime.Now;
            userLoginHistoryRequest.Username = user.UserName;
            controller.CreateUserLoginHistory(userLoginHistoryRequest);

            var roleLink = user.Roles.FirstOrDefault();
            if (roleLink != null)
            {
                var role = await roleManager.FindByIdAsync(roleLink.RoleId);
                if (role != null)
                {
                    if (role.Name == ApplicationUser.PatientRole)
                    {
                        if (user.PatientId.HasValue)
                        {
                            long? PatientId = user.PatientId;
                            /*string PatientId = user.PatientId.ToString()*/
                            ;
                            string UserId = user.Id;

                            PatientMobileProfileMobileResponse profile = controller.GetPatientProfileData(user.PatientId);

                            string PatientFullName = (profile != null) ? profile.PatientFullName : " ";
                            string PatientPhone = (profile != null) ? ((profile.PatientPhone != null) ? profile.PatientPhone : " ") : " ";
                            string PatientEmail = (profile != null) ? ((profile.PatientEmail != null) ? profile.PatientEmail : " ") : " ";
                            string PatientPhoto = (profile != null) ? ((profile.PatientPhoto != null) ? profile.PatientPhoto : " ") : " ";
                            string PatientPhotoNameUrl = (profile != null) ? ((profile.PatientPhotoNameUrl != null) ? profile.PatientPhotoNameUrl : " ") : " ";

                            string PatientLanguage = (profile != null) ? ((profile.PatientLanguage != null) ? profile.PatientLanguage : " ") : " ";
                            string PatientCountry = (profile != null) ? ((profile.PatientCountry != null) ? profile.PatientCountry : " ") : " ";
                            string PatientState = (profile != null) ? ((profile.PatientState != null) ? profile.PatientState : " ") : " ";
                            string PatientCity = (profile != null) ? ((profile.PatientCity != null) ? profile.PatientCity : " ") : " ";

                            string PatientNumber = (profile != null) ? ((profile.PatientNumber != null) ? profile.PatientNumber : " ") : " ";
                            //string PatientCaseId = (profile != null) ? ((profile.PatientCaseId != null) ? profile.PatientCaseId.ToString() : " ") : " ";
                            long? PatientCaseId = (profile != null) ? ((profile.PatientCaseId != null) ? profile.PatientCaseId : 0) : 0;
                            string PatientCaseNo = (profile != null) ? ((profile.PatientCaseNo != null) ? profile.PatientCaseNo : " ") : " ";
                            string PatientInjuryRegionId = (profile != null) ? ((profile.PatientInjuryRegionId != null) ? profile.PatientInjuryRegionId.ToString() : " ") : " ";
                            string PatientInjuryRegion = (profile != null) ? ((profile.PatientInjuryRegion != null) ? profile.PatientInjuryRegion : " ") : " ";

                            bool? IsMobileAccess = (user.Patient.HasMobileAccess == null) ? user.Patient.HasMobileAccess : false;
                            if (!IsMobileAccess.HasValue)
                                controller.UpdatePatientMobileAccess(user.PatientId, true);

                            props.Dictionary.Add("sessionExpirationTimeInMinit", LoginAttemptService.MobileApplicationSessionExpiryTime.ToString());
                            props.Dictionary.Add("refreshTokenExpirationTimeInMinit", LoginAttemptService.MobileApplicationRefreshTokenExpiryTime.ToString());

                            if (PatientId != null)
                                oAuthIdentity.AddClaim(new Claim("PatientId", PatientId.ToString()));
                            if (PatientFullName != null)
                                oAuthIdentity.AddClaim(new Claim("PatientFullName", PatientFullName));
                            if (PatientPhone != null)
                                oAuthIdentity.AddClaim(new Claim("PatientPhone", PatientPhone));
                            if (PatientEmail != null)
                                oAuthIdentity.AddClaim(new Claim("PatientEmail", PatientEmail));
                            if (PatientPhoto != null)
                                oAuthIdentity.AddClaim(new Claim("PatientPhoto", PatientPhoto));
                            if (PatientPhotoNameUrl != null)
                                oAuthIdentity.AddClaim(new Claim("PatientPhotoNameUrl", PatientPhotoNameUrl));

                            if (PatientLanguage != null)
                                oAuthIdentity.AddClaim(new Claim("PatientLanguage", PatientLanguage));
                            if (PatientCountry != null)
                                oAuthIdentity.AddClaim(new Claim("PatientCountry", PatientCountry));
                            if (PatientState != null)
                                oAuthIdentity.AddClaim(new Claim("PatientState", PatientState));
                            if (PatientCity != null)
                                oAuthIdentity.AddClaim(new Claim("PatientCity", PatientCity));
                            if (UserId != null)
                                oAuthIdentity.AddClaim(new Claim("UserId", UserId));
                            if (PatientNumber != null)
                                oAuthIdentity.AddClaim(new Claim("PatientNumber", PatientNumber));
                            if (PatientCaseId != null)
                                oAuthIdentity.AddClaim(new Claim("PatientCaseId", PatientCaseId.ToString()));
                            if (PatientCaseNo != null)
                                oAuthIdentity.AddClaim(new Claim("PatientCaseNo", PatientCaseNo));
                            if (PatientInjuryRegionId != null)
                                oAuthIdentity.AddClaim(new Claim("PatientInjuryRegionId", PatientInjuryRegionId));
                            if (PatientInjuryRegion != null)
                                oAuthIdentity.AddClaim(new Claim("PatientInjuryRegion", PatientInjuryRegion));

                            if (PatientId != null)
                                props.Dictionary.Add("PatientId", PatientId.ToString());
                            if (PatientFullName != null)
                                props.Dictionary.Add("PatientFullName", PatientFullName);
                            if (PatientPhone != null)
                                props.Dictionary.Add("PatientPhone", PatientPhone);
                            if (PatientEmail != null)
                                props.Dictionary.Add("PatientEmail", PatientEmail);
                            if (PatientPhoto != null)
                                props.Dictionary.Add("PatientPhoto", PatientPhoto);
                            if (PatientPhotoNameUrl != null)
                                props.Dictionary.Add("PatientPhotoNameUrl", PatientPhotoNameUrl);

                            if (PatientLanguage != null)
                                props.Dictionary.Add("PatientLanguage", PatientLanguage);
                            if (PatientCountry != null)
                                props.Dictionary.Add("PatientCountry", PatientCountry);
                            if (PatientState != null)
                                props.Dictionary.Add("PatientState", PatientState);
                            if (PatientCity != null)
                                props.Dictionary.Add("PatientCity", PatientCity);
                            if (UserId != null)
                                props.Dictionary.Add("UserId", UserId);
                            if (PatientNumber != null)
                                props.Dictionary.Add("PatientNumber", PatientNumber);
                            if (PatientCaseId != null)
                                props.Dictionary.Add("PatientCaseId", PatientCaseId.ToString());
                            if (PatientCaseNo != null)
                                props.Dictionary.Add("PatientCaseNo", PatientCaseNo);
                            if (PatientInjuryRegionId != null)
                                props.Dictionary.Add("PatientInjuryRegionId", PatientInjuryRegionId);
                            if (PatientInjuryRegion != null)
                                props.Dictionary.Add("PatientInjuryRegion", PatientInjuryRegion);
                        }
                    }
                    else
                    {
                        context.SetError("invalid_grant", "The user name or password is incorrect.");
                        return;
                    }
                }
            }

            var ticket = new AuthenticationTicket(oAuthIdentity, props);
            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            //if (context.Properties.Dictionary.ContainsKey("PatientId"))
            //    context.AdditionalResponseParameters.Add("PatientId", context.Properties.Dictionary["PatientId"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientFullName"))
            //    context.AdditionalResponseParameters.Add("PatientFullName", context.Properties.Dictionary["PatientFullName"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientPhone"))
            //    context.AdditionalResponseParameters.Add("PatientPhone", context.Properties.Dictionary["PatientPhone"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientEmail"))
            //    context.AdditionalResponseParameters.Add("PatientEmail", context.Properties.Dictionary["PatientEmail"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientPhoto"))
            //    context.AdditionalResponseParameters.Add("PatientPhoto", context.Properties.Dictionary["PatientPhoto"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientPhotoNameUrl"))
            //    context.AdditionalResponseParameters.Add("PatientPhotoNameUrl", context.Properties.Dictionary["PatientPhotoNameUrl"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientLanguage"))
            //    context.AdditionalResponseParameters.Add("PatientLanguage", context.Properties.Dictionary["PatientLanguage"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientCountry"))
            //    context.AdditionalResponseParameters.Add("PatientCountry", context.Properties.Dictionary["PatientCountry"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientState"))
            //    context.AdditionalResponseParameters.Add("PatientState", context.Properties.Dictionary["PatientState"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientCity"))
            //    context.AdditionalResponseParameters.Add("PatientCity", context.Properties.Dictionary["PatientCity"]);

            //if (context.Properties.Dictionary.ContainsKey("UserId"))
            //    context.AdditionalResponseParameters.Add("UserId", context.Properties.Dictionary["UserId"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientNumber"))
            //    context.AdditionalResponseParameters.Add("PatientNumber", context.Properties.Dictionary["PatientNumber"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientCaseId"))
            //    context.AdditionalResponseParameters.Add("PatientCaseId", context.Properties.Dictionary["PatientCaseId"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientCaseNo"))
            //    context.AdditionalResponseParameters.Add("PatientCaseNo", context.Properties.Dictionary["PatientCaseNo"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientInjuryRegionId"))
            //    context.AdditionalResponseParameters.Add("PatientInjuryRegionId", context.Properties.Dictionary["PatientInjuryRegionId"]);

            //if (context.Properties.Dictionary.ContainsKey("PatientInjuryRegion"))
            //    context.AdditionalResponseParameters.Add("PatientInjuryRegion", context.Properties.Dictionary["PatientInjuryRegion"]);

            return Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

            if (allowedOrigin == null) allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            //validate your client
            //var currentClient = context.ClientId;

            //if (Client does not match)
            //{
            //    context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
            //    return Task.FromResult<object>(null);
            //}

            // Change authentication ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }
    }
}