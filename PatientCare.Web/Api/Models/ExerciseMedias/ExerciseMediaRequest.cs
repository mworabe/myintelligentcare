﻿using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.ExerciseMedias
{
    public class ExerciseMediaRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }
        
        public long? ExerciseId { get; set; }
        
        public string OriginalName { get; set; }
        
        public MediaType? MediaType { get; set; }

        public string ThumbImageName { get; set; }

    }   
}