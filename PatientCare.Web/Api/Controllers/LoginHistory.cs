﻿using System;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Data.EntityFramework;
using System.Collections.Generic;
using PatientCare.Web.Api.Models.Resources;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Web.Services.Convertation;
using System.Data.Entity;
using System.Linq;
using PatientCare.Web.Api.Models.PatientCases;
using PatientCare.Core.Enums;
using PatientCare.Web.ApiMobile.Models.Patients;
using PatientCare.Core.Mapping;
using PatientCare.Web.Api.Models.Picklist;
using Newtonsoft.Json;
using PatientCare.Web.Services.FileStorage;
using PatientCare.Web.ApiMobile.Models.PatientGoals;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Login History operations.
    /// </summary>
    public class LoginHistory
    {
        private PatientCareDbContext db = new PatientCareDbContext();

        /// <summary>
        /// Method Description : Using for Create New Login History. 
        /// </summary>
        /// <param name="userLoginHistoryRequest">User Login History Entity</param>
        public void CreateUserLoginHistory([FromBody]UserLoginHistory userLoginHistoryRequest)
        {
            using (db = new PatientCareDbContext())
            {
                db.UserLoginHistorys.Add(userLoginHistoryRequest);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Method Description : Using for Get Resource. 
        /// </summary>
        public void GetDummyResourceCall()
        {
            using (db = new PatientCareDbContext())
            {
                IEnumerable<ResourceMainPicklistResponse> result = new List<ResourceMainPicklistResponse>();
            }
        }

        /// <summary>
        /// Method Description : Using for Get User Expiry Date.  
        /// </summary>
        /// <returns></returns>
        public bool GetUserExpiryDate()
        {
            bool isExpiry = false;
            using (db = new PatientCareDbContext())
            {
                var query = db.TrialPeriods.SqlQuery("SELECT Id,trial_period_date as TrialPeriodDate,created ,modified FROM trial_period");
                foreach (var item in query)
                {
                    if (item.TrialPeriodDate != null)
                    {
                        DateTime loginTime = TimeZone.CurrentTimeZone.ToLocalTime(DateTime.Now);
                        DateTime trialPeriodDate = TimeZone.CurrentTimeZone.ToLocalTime(item.TrialPeriodDate.Value);
                        if (loginTime > trialPeriodDate)
                        {
                            isExpiry = true;
                        }
                    }
                }
            }
            return isExpiry;
        }

        /// <summary>
        /// Method Description : Using for Get User Ui Configuration.  
        /// </summary>
        /// <param name="resourceid">Resource Id</param>
        /// <returns></returns>
        public string GetUserUiConfiguration(long? resourceid)
        {
            string result = "[";
            using (db = new PatientCareDbContext())
            {
                string sqlquery = "SELECT Id, resource_id as ResourceId ,[key], value, created, modified FROM ui_configurations where resource_id =" + resourceid + " order by created desc";
                var query = db.UiConfigurations.SqlQuery(sqlquery);

                foreach (var item in query)
                {
                    result += "{\"id\":" + item.Id + ",\"key\": " + "\"" + item.Key + "\"" + " , \"value\":" + item.Value + ", \"resourceId\":" + item.ResourceId + "},";
                }
                if (result.Length > 1)
                    result = result.Substring(0, (result.Length - 1));
            }
            result += "]";
            return result;
        }

        /// <summary>
        /// Method Description : Using for Modification In Application Role.   
        /// </summary>
        /// <param name="role">Role</param>
        /// <returns>Json</returns>
        public string ModificationInApplicationRoleOfActivity(ApplicationRole role)
        {
            AccessRights accessRights = new AccessRights();
            string json = string.Empty;

            if (role != null && role.JsonSetup != null)
                accessRights = JsonConvert.DeserializeObject<AccessRights>(role.JsonSetup);

            accessRights.MainHomePage = (accessRights.MainHomePage == null) ? new HomePageDynamic() : accessRights.MainHomePage;
            json = JsonConverterHelper.SerializeObject(accessRights);
            return json;
        }

        /// <summary>
        /// Method Description : Using for Modification In Application Role.   
        /// </summary>
        /// <param name="patientId">Patient Id</param>
        /// <param name="isMobileAccess">Is Mobile Access</param>
        public void UpdatePatientMobileAccess(long? patientId, bool isMobileAccess)
        {
            using (db = new PatientCareDbContext())
            {
                var patient = db.Patients.AsNoTracking().Where(x => x.Id == patientId.Value).FirstOrDefault();
                patient.HasMobileAccess = isMobileAccess;

                db.Patients.Attach(patient);
                db.Entry(patient).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Method Description : Using for Get Patient Current Case Data by Patient Id.    
        /// </summary>
        /// <param name="patientId">Patient Id</param>
        /// <returns>Patient Current Case Data Model</returns>
        public PatientCurrentCaseData GetpatientCurrentCaseData(long? patientId)
        {
            PatientCurrentCaseData detail = new PatientCurrentCaseData();
            using (db = new PatientCareDbContext())
            {
                var patientCase = db.PatientCases.AsNoTracking().Where(x => x.PatientId == patientId.Value && x.CaseStatus == CaseStatusEnum.Inprogress && (x.IsDeleted == false || x.IsDeleted == null)).FirstOrDefault();
                if (patientCase != null)
                {
                    detail.PatientId = patientId;
                    detail.CaseId = patientCase.Id;
                    detail.CaseNo = patientCase.CaseNo;
                    detail.PatientNumber = (patientCase.Patient != null) ? patientCase.Patient.PatientNumber : string.Empty;
                    detail.InjuryRegion = (patientCase.InjuryRegion != null) ? patientCase.InjuryRegion.Name : string.Empty;
                    detail.InjuryRegionId = patientCase.InjuryRegionId;
                    if (patientCase.AssignedPhysicianId != null)
                    {
                        detail.AssignedPhysician = (patientCase.AssignedPhysician != null) ? new PicklistDefaultResponse { Id = patientCase.AssignedPhysician.Id, Name = patientCase.AssignedPhysician.Name } : new PicklistDefaultResponse();
                        detail.AssignedPhysicianId = patientCase.AssignedPhysicianId;
                    }
                }
                else
                {
                    var patientData = db.Patients.AsNoTracking().Where(x => x.Id == patientId && (x.IsDeleted == false || x.IsDeleted == null)).FirstOrDefault();
                    if (patientData != null)
                    {
                        if (patientData.Clinician != null)
                        {
                            detail.AssignedPhysician = (patientData.Clinician != null) ? new PicklistDefaultResponse { Id = patientData.Clinician.Id, Name = patientData.Clinician.Name } : new PicklistDefaultResponse();
                            detail.AssignedPhysicianId = patientData.ClinicianId;
                        }
                    }
                }
                return detail;
            }
        }

        /// <summary>
        /// Method Description : Using for Get Patient Profile Data by Patient Id.     
        /// </summary>
        /// <param name="patientId">Patient Id</param>
        /// <returns>Patient Mobile Profile Mobile Response Model</returns>
        public PatientMobileProfileMobileResponse GetPatientProfileData(long? patientId)
        {
            PatientMobileProfileMobileResponse detail = new PatientMobileProfileMobileResponse();
            using (db = new PatientCareDbContext())
            {
                var profile = db.PatientMobileProfiles.AsNoTracking().Where(x => x.PatientId == patientId).FirstOrDefault();
                var userLoginDetail = db.Users.AsNoTracking().Where(x => x.PatientId == patientId).FirstOrDefault();

                if (profile != null)
                {
                    detail.Id = profile.Id;
                    detail.PatientId = patientId;
                    detail.UserId = (userLoginDetail != null) ? userLoginDetail.Id : null;
                    detail.PatientFullName = profile.FullName;
                    detail.PatientLanguage = profile.Language;
                    detail.PatientCity = profile.City;
                    detail.PatientState = profile.State;
                    detail.PatientCountry = profile.Country;
                    detail.PatientEmail = profile.Email;
                    detail.PatientPhone = profile.Phone;
                    detail.PatientPhoto = profile.Photo;
                    detail.PatientPhotoNameUrl = (profile.Photo != null) ? FileUploadHelpers.GetStorageUrlPhoto(profile.Photo, FileUploadHelpers.PatientFolderTemplate) : null;
                    profile.Goals = new PatientGoal[0];

                    PatientCurrentCaseData currentCaseData = new PatientCurrentCaseData();
                    currentCaseData = GetpatientCurrentCaseData(patientId);

                    if (currentCaseData != null)
                    {
                        detail.PatientNumber = currentCaseData.PatientNumber;
                        detail.PatientCaseId = currentCaseData.CaseId;
                        detail.PatientCaseNo = currentCaseData.CaseNo;
                        detail.PatientInjuryRegion = currentCaseData.InjuryRegion;
                        detail.PatientInjuryRegionId = currentCaseData.InjuryRegionId;

                        List<PatientClinicianMobileResponse> clinicianData = new List<PatientClinicianMobileResponse>();
                        if (currentCaseData.AssignedPhysician != null)
                        {
                            if (currentCaseData.AssignedPhysician.Id > 0)
                            {
                                clinicianData.Add(new PatientClinicianMobileResponse { Id = currentCaseData.AssignedPhysician.Id, Name = currentCaseData.AssignedPhysician.Name });
                                detail.Clinicians = (clinicianData.Count > 0) ? clinicianData.ToArray() : new PatientClinicianMobileResponse[0];
                            }
                        }
                    }
                }
            }
            using (db = new PatientCareDbContext())
            {
                if (detail.PatientId != null && detail.PatientCaseId != null)
                {
                    var goalList = db.PatientGoals.AsNoTracking().Where(x => x.PatientId == detail.PatientId && x.PatientCaseId == detail.PatientCaseId).ToList();
                    if (goalList != null && goalList.Count > 0)
                    {
                        detail.Goals = Mapper.Map<PatientGoal[], PatientGoalMobileResponse[]>(goalList.ToArray());
                    }
                }
            }
            return detail;
        }
    }
}
