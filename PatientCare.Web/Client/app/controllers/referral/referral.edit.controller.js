﻿(function ResourceEditCtrlIIFE() {
    "use strict";
    angular.module("EDZoutstaffingPortalApp")
        .controller("ReferralPhysicianEditController",
            ["$scope", "$stateParams", "patientService", "$state", "$q", "intialData",
                "$uibModalInstance", "DYNAMIC_FORM_CONSTROLLER", "ReferralPhysicianService", "toaster",
                "StateService", "CityService", "ClinicService", "CreatorSearchFunction", "ZipCodeService", "Resource", "$timeout",
                ReferralEditControllerFunc]
        );
    function ReferralEditControllerFunc($scope, $stateParams, patientService, $state, $q, intialData,
        $uibModalInstance, DYNAMIC_FORM_CONSTROLLER, referralPhysicianService, toaster,
        StateService, CityService, clinicService, creatorSearchFunction, ZipCodeService, resource, $timeout) {

        var initialValues = intialData;
        var self = this;
        self.autocompleteMinLength = 0;
        self.pageTitle = intialData.name || "New Physician";
        self.isPatientLoadedCorrectly = !(initialValues[0] && initialValues[0].errorCode === 404);
        self.model = intialData || {};
        self.model.medicalDesignation = self.model.medicalDesignation || {};

        self.generalPoropertyList = self.formFields = [
            {
                id: "firstName",
                name: "First Name",
                container: "referralDetails",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                containerCssClass: "col-xs-12",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
            }, {
                id: "lastName",
                name: "Last Name",
                container: "referralDetails",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                containerCssClass: "col-xs-12",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
            }, {
                id: "speciality",
                name: "Speciality",
                container: "referralDetails",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                containerCssClass: "col-xs-12",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
            }, {
                id: "medicalDesignation",
                name: "Medical Designation",
                container: "referralDetails",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                containerCssClass: "col-xs-12",
                specialName: "title",
                specialValue: "value",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
            }
            //, {
            //    id: "medicalDesignationOther",
            //    name: "Other",
            //    container: "referralDetails",
            //    position: "1",
            //    pattern: ".{0,255}",
            //    required: false,
            //    visible: true,
            //    containerCssClass: "col-xs-12",
            //    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXTAREA,
            //}
            , {
                id: "facilityName",
                name: "Facility Name",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
            }, {
                id: "phoneNumber",
                name: "Phone Number",
                container: "referralDetails",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                containerCssClass: "col-xs-12",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
            }, {
                id: "fax",
                name: "FAX",
                container: "referralDetails",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                containerCssClass: "col-xs-12",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
            }, {
                id: "email",
                name: "Email",
                container: "referralDetails",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                containerCssClass: "col-xs-12",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
            }, {
                id: "city",
                name: "City",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: false,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                descriptionField: "stateName",
            }, {
                id: "state",
                name: "State",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: false,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
            }, {
                id: "postalCode",
                name: "Postal Code",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: false,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
            },
        ];

        self.medicalDesignation_Enum = [
            { title: "MD", value: "MD" },
            { title: "GP", value: "GP" },
            { title: "OT", value: "OT" },
            { title: "Psych", value: "Psych" },
            { title: "ENT", value: "ENT" },
            { title: "Gyn", value: "Gyn" },
            { title: "OB/GYN", value: "OBGYN" },
            { title: "Physio", value: "Physio" },
            { title: "Other", value: "Other" },
        ];

        function getValueInAnguAutocomplete(selected) {
            return (selected && selected.originalObject) ? selected.originalObject.id : null;
        }


        creatorSearchFunction(self, "state_Search", StateService);
        creatorSearchFunction(self, "city_Search", CityService);
        creatorSearchFunction(self, "postalCode_Search", ZipCodeService);
        

        function onStateSelected(selected) {
            self.model.stateId = getValueInAnguAutocomplete(selected);
        }
        function onCitySelected(selected) {
            self.model.cityId = getValueInAnguAutocomplete(selected);
        }

        self.state_Selected = onStateSelected;
        self.city_Selected = onCitySelected;



        self.postalCode_InputChanged = function (str) {
            self.model.zipCode = $("#postalCode_value").val();
            updateGeoNamesForZipCode();
        }
        self.postalCode_Selected = function (selected) {
            self.model.postalCode = (selected && selected.originalObject) ? selected.originalObject.name : null;
            updateGeoNamesForZipCode();
        }


        function updateGeoNamesForZipCode() {
            
            if (self.model.postalCode && self.prevZipCode != self.model.postalCode) {
                self.prevZipCode = angular.copy(self.model.postalCode);
                resource.getGeoNamesForZipCode({ zipCode: self.model.postalCode }, function (success) {
                    
                    if (success && success.city && success.state && success.country) {
                        $timeout(function updateAutoCompleteValue() {
                            self.model.city = success.city;
                            self.model.state = success.state;
                            self.model.country = success.country;

                            $scope.$broadcast('angucomplete-alt:changeInput', 'city', success.city);
                            $scope.$broadcast('angucomplete-alt:changeInput', 'state', success.state);
                            $scope.$broadcast('angucomplete-alt:changeInput', 'country', success.country);

                        }, 100);
                    } else {
                        console.info("Zip Cleared from GeoNames");
                        $scope.$broadcast('angucomplete-alt:clearInput', 'postalCode');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'city');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'state');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'country');
                        toaster.pop("error", "", "Entered ZIP code is not valid, Re-Enter it or Choose one from list.");
                    }
                }, function (error) { });
            }
        }


        self.onCloseButtonClick = function onCloseButtonClick(value) {
            $uibModalInstance.close(null);
        }
        self.onSaveButtonClick = function onSaveButtonClick(form) {

            var keys = Object.keys(form.$error);

            //if (form.$invalid) {
            if (keys && keys.length > 0) {
                toaster.pop("error", "", "Invalid Data, Please Try again later.");
                return;
            }
            if (self.model.id && self.model.id > 0)
                referralPhysicianService.update({ id: self.model.id }, self.model, function (success) {
                    toaster.pop("success", "", "Referral Physician Updated successfully.");
                    $uibModalInstance.close(success);
                }, function (error) {
                    toaster.pop("error", "", "Unable to Save Referral Physician, Please try again later.");
                });
            else
                referralPhysicianService.save(self.model, function (success) {
                    toaster.pop("success", "", "Referral Physician Saved successfully.");
                    $uibModalInstance.close(success);
                }, function (error) {
                    toaster.pop("error", "", "Unable to Save Referral Physician, Please try again later.");
                });
        }
    }

})();

