﻿using System.Runtime.Serialization;
using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.CustomFieldProperties;

namespace PatientCare.Web.Api.Models.CustomFields
{
    [DataContract]
    public class CustomFieldResponse
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool ResourceAvailable { get; set; }
        [DataMember]
        public bool ProjectAvailable { get; set; }
        [DataMember]
        public bool TaskAvailable { get; set; }
        [DataMember]
        public bool IntakeAvailable { get; set; }

        [DataMember]
        public CustomFieldTypeEnum Type { get; set; }
        [DataMember]
        public bool Require { get; set; }
        [DataMember]
        public string DefaultValue { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public CustomFieldPropertyResponse[] CustomFieldProperties { get; set; }
        [DataMember]
        public bool IsSearchable { get; set; }
    }
}