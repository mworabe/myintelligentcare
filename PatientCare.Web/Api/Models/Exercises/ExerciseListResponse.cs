﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.DropDownConfigs;
using PatientCare.Web.Api.Models.Exercise_injury;
//using PatientCare.Web.Api.Models.Exercise_InjuryRegion;
using PatientCare.Web.Api.Models.ExerciseActivities;
using PatientCare.Web.Api.Models.ExerciseMedias;
using PatientCare.Web.Api.Models.ExercisePositions;
using PatientCare.Web.Api.Models.Exewrcise_ExerciseActivities;
using PatientCare.Web.Api.Models.InjuryRegions;
using PatientCare.Web.Services.FileStorage;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Exercises
{
    [DataContract]
    public class ExerciseListResponse
    {
        [DataMember]
        public long? Id { get; set; }
                
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Exercise_ExerciseActivityResponse[] activities { get; set; }

        [DataMember]
        public Exercise_injuryResponse[] injuries { get; set; }

        [DataMember]
        public string Keywords { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public long? ExerciseMediaId { get; set; }

        [DataMember]
        public string OriginalName { get; set; }

        [DataMember]
        public string MediasName { get; set; }

        [DataMember]
        public MediaType? MediaType { get; set; }

        [DataMember]
        public string MediaURL { get; set; }

        [DataMember]
        public bool IsLocal
        {
            get { return (StorageService.UseAmazonStorage); }
            set { IsLocal = value; }
        }
        [DataMember]
        public string MediaOriginalName { get; set; }

        [DataMember]
        public FileRequest[] Files { get; set; }

        [DataMember]
        public ExerciseMediaResponse[] Medias { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public long? set { get; set; }

        [DataMember]
        public long? reps { get; set; }

        [DataMember]
        public long? holds { get; set; }

        [DataMember]
        public long? weight { get; set; }

        [DataMember]
        public string weightUnit { get; set; }

        [DataMember]
        public string holdUnit { get; set; }

        [DataMember]
        public string frequency { get; set; }
    }
}