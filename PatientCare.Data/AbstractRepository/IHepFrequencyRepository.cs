﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IHepFrequencyRepository : IRepository<HepFrequency>
    {
    }
}
