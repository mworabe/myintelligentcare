﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class EntryTypeMappingExtensions
    {
        public static DbModelBuilder MapEntryType(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<EntryType>().ToTable("entry_types").HasKey(i => i.Id);

            modelBuilder.Entity<EntryType>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<EntryType>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<EntryType>()
              .Property(p => p.EntryPeriod)
              .HasColumnName("entry_period")
              .IsOptional();
            
            modelBuilder.Entity<EntryType>()
              .Property(p => p.Name)
              .HasColumnName("name")
              .IsOptional();

            modelBuilder.Entity<EntryType>()
              .Property(p => p.Days)
              .HasColumnName("days")
              .IsOptional();

            modelBuilder.Entity<EntryType>()
              .Property(p => p.StartDate)
              .HasColumnName("start_date")
              .IsOptional();

            modelBuilder.Entity<EntryType>()
              .Property(p => p.EndDate)
              .HasColumnName("end_date")
              .IsOptional();

            modelBuilder.Entity<EntryType>()
              .Property(p => p.AdvancedCreatePeriod)
              .HasColumnName("advanced_create_period")
              .IsOptional();

            modelBuilder.Entity<EntryType>()
              .Property(p => p.LastEntryDateCreated)
              .HasColumnName("last_entry_date_created")
              .IsOptional();

            return modelBuilder;
        }
    }
}
