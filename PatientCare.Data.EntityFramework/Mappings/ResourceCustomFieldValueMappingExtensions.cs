﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceCustomFieldValueMappingExtensions
    {
        public static DbModelBuilder MapResourceCustomFieldValue(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceCustomFieldValue>().ToTable("resource_custom_field_values").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceCustomFieldValue>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceCustomFieldValue>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceCustomFieldValue>()
                .Property(p => p.Value)
                .HasColumnName("value")
                .IsUnicode()
                .IsOptional();

            modelBuilder.Entity<ResourceCustomFieldValue>()
                .Property(p => p.CustomFieldId)
                .HasColumnName("custom_field_id")
                .IsRequired();

            modelBuilder.Entity<ResourceCustomFieldValue>()
                .HasRequired(p => p.CustomField)
                .WithMany()
                .HasForeignKey(item => item.CustomFieldId);
            
            modelBuilder.Entity<ResourceCustomFieldValue>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsRequired();

            modelBuilder.Entity<ResourceCustomFieldValue>()
                .HasRequired(p => p.Resource)
                .WithMany()
                .HasForeignKey(item => item.ResourceId);
            
            return modelBuilder;
        }
    }
}