﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.CustomFields;

namespace PatientCare.Web.Api.Models.Resources
{
    [DataContract]
    public class ResourceCustomFieldValueResponse
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long CustomFieldId { get; set; }
        [DataMember]
        public virtual CustomFieldResponse CustomField { get; set; }
        [DataMember]
        public long ResourceId { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public string AdditionalInfo { get; set; }
    }
}