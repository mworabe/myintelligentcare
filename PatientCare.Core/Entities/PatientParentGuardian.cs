﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Patient Parent Guardian.
    /// </summary>
    public class PatientParentGuardian : Entity
    {
        #region Patient Parents/Gaurdian Division

        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public RelationshipEnum? RelationshiptoPatient { get; set; }

        public string Address { get; set; }

        public long? CityId { get; set; }
        public virtual City City { get; set; }

        public long? StateId { get; set; }
        public virtual State State { get; set; }

        public long? CountryId { get; set; }
        public virtual Country Country { get; set; }

        public string ZipCode { get; set; }

        public string CellNumber { get; set; }

        public bool? CellNumberIsPrimary { get; set; }

        public string HomeNumber { get; set; }

        public bool? HomeNumberIsPrimary { get; set; }

        #endregion
    }
}
