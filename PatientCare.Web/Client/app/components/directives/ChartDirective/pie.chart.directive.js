﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.directive("pieChart", ["$timeout", function ($timeout) {
    return {
        templateUrl: "Client/app/components/directives/ChartDirective/pieChart.html",
        restrict: "E",
        replace: false,
        scope: {
            chartData: "=",
            chartWidth: "=",
            chartHeight: "=",
            isSmall: "="
        },
        link: function ($scope) {
            $scope._chartWidth = 100; // ($scope.chartWidth ? $scope.chartWidth : 85);
            $scope._chartHeight = 100; //($scope.chartHeight ? $scope.chartHeight : 85);
            $timeout(function () {
            }, 1000);

            $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
            $scope.data = [300, 500, 100];
        }
    };
}]);