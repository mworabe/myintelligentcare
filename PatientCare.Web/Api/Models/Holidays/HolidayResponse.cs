﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Holidays
{
    [DataContract]
    public class HolidayResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long Location { get; set; }

        [DataMember]
        public DateTime? HolidayDate { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}