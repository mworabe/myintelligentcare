﻿
'use strict';

angular.module('EDZoutstaffingPortalApp')
    .config(function (dashboardProvider) {
        dashboardProvider
            .structure('6-6_6-6', {
                rows: [{
                    columns: [{
                        styleClass: 'col-md-6'
                    }, {
                        styleClass: 'col-md-6'
                    }]
                }, {
                    columns: [{
                        styleClass: 'col-md-6'
                    }, {
                        styleClass: 'col-md-6'
                    }]
                }]
            }).structure('6-6_3-3-3-3', {
                rows: [
                    {
                        columns: [{
                            styleClass: 'col-md-6'
                        }, {
                            styleClass: 'col-md-6'
                        }]
                    },
                    {
                        columns: [{
                            styleClass: 'col-md-3'
                        }, {
                            styleClass: 'col-md-3'
                        }, {
                            styleClass: 'col-md-3'
                        }, {
                            styleClass: 'col-md-3'
                        }]
                    }
                ]
            }).structure('3-9', {
                rows: [
                    {
                        columns: [{
                            styleClass: 'col-md-3'
                        }, {
                            styleClass: 'col-md-9'
                        }]
                    }
                ]
            }).structure('4-8', {
                rows: [
                    {
                        columns: [{
                            styleClass: 'col-md-4'
                        }, {
                            styleClass: 'col-md-8'
                        }]
                    }
                ]
            }).structure('12-4-4-4', {
                rows: [
                    {
                        columns: [{
                            styleClass: 'col-md-12'
                        }]
                    }, {
                        columns: [{
                            styleClass: 'col-md-4'
                        }, {
                            styleClass: 'col-md-4'
                        }, {
                            styleClass: 'col-md-4'
                        }]
                    }
                ]
            })

        

        /*
        dashboardProvider
            .structure('<img src="Client/assets/images/dashboard/6-6_6-6.png" />', {
                rows: [{
                    columns: [{
                        styleClass: 'col-md-6'
                    }, {
                        styleClass: 'col-md-6'
                    }]
                }, {
                    columns: [{
                        styleClass: 'col-md-6'
                    }, {
                        styleClass: 'col-md-6'
                    }]
                }]
            })
            .structure('<img src="Client/assets/images/dashboard/6-6_3-3-3-3.png" />', {
                rows: [{
                    columns: [{
                        styleClass: 'col-md-6'
                    }, {
                        styleClass: 'col-md-6'
                    }]
                }, {
                    columns: [{
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    },{
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }]
                }]
            })
            .structure('<img src="Client/assets/images/dashboard/6-6_3-9.png" />', {
                rows: [{
                    columns: [{
                        styleClass: 'col-md-6'
                    }, {
                        styleClass: 'col-md-6'
                    }]
                }, {
                    columns: [{
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-9'
                    }]
                }]
            })
            .structure('<img src="Client/assets/images/dashboard/6-6_4-4-4.png" />', {
                rows: [{
                    columns: [{
                        styleClass: 'col-md-6'
                    }, {
                        styleClass: 'col-md-6'
                    }]
                }, {
                    columns: [{
                        styleClass: 'col-md-4'
                    }, {
                        styleClass: 'col-md-4'
                    }, {
                        styleClass: 'col-md-4'
                    }]
                }]
            })
            .structure('<img src="Client/assets/images/dashboard/4-4-4_3-3-3-3.png" />', {
                rows: [{
                    columns: [{
                        styleClass: 'col-md-4'
                    }, {
                        styleClass: 'col-md-4'
                    }, {
                        styleClass: 'col-md-4'
                    }]
                }, {
                    columns: [{
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }]
                }]
            })
            .structure('<img src="Client/assets/images/dashboard/3-3-3-3_3-3-3-3.png" />', {
                rows: [{
                    columns: [{
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }]
                }, {
                    columns: [{
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }]
                }]
            })
            .structure('<img src="Client/assets/images/dashboard/3-9_3-3-3-3.png" />', {
                rows: [{
                    columns: [{
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-9'
                    }]
                }, {
                    columns: [{
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }, {
                        styleClass: 'col-md-3'
                    }]
                }]
            })
            .structure('<img src="Client/assets/images/dashboard/3_6_2-2.png" />', {
                rows: [{
                    columns: [{
                        styleClass: 'col-md-4'
                    }, {
                        styleClass: 'col-md-8'
                    }]
                }, {
                    columns: [{
                        styleClass: 'col-md-4 col-md-offset-4'
                    }, {
                        styleClass: 'col-md-4'
                    }]
                }]
            })
            */
    });
