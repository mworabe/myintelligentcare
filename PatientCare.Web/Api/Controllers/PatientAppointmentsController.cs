﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.PatientAppointment;
using PatientCare.Core.Enums;
using PatientCare.Data.AbstractDataContext;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Collections.Generic;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Api.Models.Schedulers;
using Microsoft.AspNet.SignalR;
using PatientCare.Web.Hubs;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Patient Appointment operations.
    /// </summary>
    [System.Web.Http.Authorize]
    public sealed class PatientAppointmentsController : ApiBaseController
    {
        /// <summary>
        /// Description : Patient Appointment IPatientService.
        /// </summary>
        private readonly IPatientService _patientService;
        /// <summary>
        /// Description : Patient Appointment IPatientAppointmentRepository.
        /// </summary>
        private readonly IPatientAppointmentRepository _patientAppointmentRepository;
        /// <summary>
        /// Description : Patient Appointment IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : Patient Appointment IPatientWorkoutAnalyticRepository.
        /// </summary>
        private readonly IPatientWorkoutAnalyticRepository _patientWorkoutAnalyticRepository;

        ReadOptions<PatientAppointment> _readOptions = new ReadOptions<PatientAppointment>();

        private IHubContext _hubs;

        /// <summary>
        /// Description : Initializes a new instance of the Patient Appointment Class.  
        /// </summary>
        /// <param name="patientService">IPatient Service</param>
        /// <param name="patientAppointmentRepository">IPatient Appointment Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        /// <param name="patientWorkoutAnalyticRepository">IPatient Workout Analytic Repository</param>
        public PatientAppointmentsController
            (
                IPatientService patientService,
                IPatientAppointmentRepository patientAppointmentRepository,
                IDataContextScopeFactory dataContextScopeFactory,
                IPatientWorkoutAnalyticRepository patientWorkoutAnalyticRepository
            )
        {
            _hubs = GlobalHost.ConnectionManager.GetHubContext<AppointmentBroadCasterHub>();
            _patientService = patientService;
            _patientAppointmentRepository = patientAppointmentRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
            _patientWorkoutAnalyticRepository = patientWorkoutAnalyticRepository;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("patientappointments/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Patient Appointment.
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="request">Patient Appointmen tList Request Model</param>
        /// <returns>Patient Appointment List Response Model</returns>
        [HttpGet]
        [Route("api/patientAppointments/{id}/appointment")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<PatientAppointmentListResponse>))]
        public IHttpActionResult GetAllPatientAppointment(long Id, [FromUri]PatientAppointmentListRequest request)
        {
            if (Id == 0)
                return NotFound();

            var query = PagingExtensions<PatientAppointment>.CreatePagedQuery(request);

            query.AddFilter(x => x.PatientId == Id);

            var readOptions = new ReadOptions<PatientAppointment>().AsReadOnly();
            var page = _patientAppointmentRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<PatientAppointment[], PatientAppointmentResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<PatientAppointmentResponse>(dtos, page.TotalCount);
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Patient Appointment.   
        /// </summary>
        /// <param name="patientAppointmentRequest">Patient Appointment Request Model</param>
        /// <returns>Patient Appointment Response Model</returns>
        [HttpPost]
        [Route("api/patientAppointments")]
        [SwaggerResponse(201, "Created", typeof(PatientAppointmentResponse))]
        public async Task<IHttpActionResult> CreatePatientAppointment([FromBody]PatientAppointmentRequest patientAppointmentRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var patientAppointment = Mapper.Map<PatientAppointmentRequest, PatientAppointment>(patientAppointmentRequest);
            patientAppointment.IsMobile = false;
            patientAppointment.IsReschedule = false;
            await _patientService.CreatePatientApointmentAsync(patientAppointment);

            var notification = new NotificationHelper(_dataContextScopeFactory);
            await notification.pushAppointmentUpdate("New Appointment Created.", patientAppointment.Id);

            var newAppointment = await _patientService.ReadPatientApointmentAsync(patientAppointment.Id);
            var viewmodel = Mapper.Map<PatientAppointment, PatientAppointmentResponse>(newAppointment);
            return Created(GetEntityLocation(patientAppointment.Id), viewmodel);
        }

        /// <summary>
        /// Method Description : Using for Update Patient Appointment.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="patientAppointmentRequest">Patient Appointment Request Model</param>
        /// <returns>Patient Appointment Response Model</returns>
        [HttpPut]
        [Route("api/patientAppointments/{id}")]
        public async Task<IHttpActionResult> EditPatientsAppointment(long id, [FromBody] PatientAppointmentRequest patientAppointmentRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var patientAppointment = Mapper.Map<PatientAppointmentRequest, PatientAppointment>(patientAppointmentRequest);
            patientAppointment.Id = id;
            await _patientService.UpdatePatientApointmentAsync(patientAppointment);

            var notification = new NotificationHelper(_dataContextScopeFactory);
            await notification.pushAppointmentUpdate("Appointment Modified.", patientAppointment.Id);

            var newAppointment = await _patientService.ReadPatientApointmentAsync(patientAppointment.Id);
            var viewmodel = Mapper.Map<PatientAppointment, PatientAppointmentResponse>(newAppointment);

            return Ok(viewmodel);
        }

        /// <summary>
        /// Method Description : Using for Update  Patient Appointment Status.  
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="status">Appointment Status Enum</param>
        /// <returns>Patient Appointment Response Model</returns>
        [HttpPut]
        [Route("api/patientAppointments/{id}/{status}")]
        public async Task<IHttpActionResult> UpdateAppointmentStatus(long id, [FromUri] AppointmentStatusEnum? status = null)
        {
            if (status == null)
                return BadRequest(ModelState);

            var patientAppointment = await _patientService.ReadPatientApointmentAsync(id);
            if (patientAppointment == null)

                return NotFound();

            patientAppointment.Id = id;
            patientAppointment.AppointmentStatus = status;

            await _patientService.UpdatePatientApointmentAsync(patientAppointment);

            var response = Mapper.Map<PatientAppointment, PatientAppointmentResponse>(patientAppointment);

            var notification = new NotificationHelper(_dataContextScopeFactory);
            await notification.pushAppointmentUpdate("Appointment Modified.", patientAppointment.Id);

            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Delete Patient Appointment by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patient Appointment Response Model</returns>
        [HttpDelete]
        [Route("api/patientAppointments/{id}")]
        public async Task<IHttpActionResult> DeletePatientsAppointmentItem(long id)
        {
            var success = await _patientService.DeletePatientApointmentAsync(id);
            if (!success)
                return NotFound();

            var notification = new NotificationHelper(_dataContextScopeFactory);
            await notification.pushAppointmentUpdate("Appointment Deleted.", id);

            return Ok(new PatientAppointment { Id = id });
        }

        /// <summary>
        /// Method Description : Using for Get Patients Appointment by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patients Appointmente Response Model</returns>
        [HttpGet]
        [Route("api/patientAppointments/{id}")]
        public async Task<IHttpActionResult> GetPatientsAppointment(long id)
        {
            var patientAppointment = await _patientService.ReadPatientApointmentAsync(id);
            if (patientAppointment == null)
                return NotFound();

            var viewModel = Mapper.Map<PatientAppointment, PatientAppointmentResponse>(patientAppointment);
            return Ok(viewModel);
        }

        /// <summary>
        /// Method Description : Using for Get User Appointment.    
        /// </summary>
        /// <param name="request">Patient Appointment List Request</param>
        /// <returns>Scheduler List Response Model</returns>
        [HttpGet]
        [Route("api/patientAppointments/user")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<SchedulerListResponse>))]
        public async Task<IHttpActionResult> GetUsersAppointment([FromUri]PatientAppointmentListRequest request)
        {
            var roleInfo = GetRoleAccessRights();
            if (roleInfo == null)
                return NotFound();

            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                ModelState.AddModelError("User Type Appointment", "your session is expired. please login again");
                return BadRequest(ModelState);
            }
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user == null && user.Resource == null)
            {
                ModelState.AddModelError("User Type Appointment", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }

            request.PageIndex = request.PageIndex - 1;

            ListResponse<SchedulerListResponse> response = new ListResponse<SchedulerListResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {

                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_get_appointment_for_user @responseType = 'Data',@UserType = '" + roleInfo.RoleName + "',@resourceId = '" + user.ResourceId + "',@pageSize = '" + request.PageSize + "',@pageIndex = '" + request.PageIndex + "',@startDate = '" + request.From.Value.ToString("MM/dd/yyyy") + "',@endDate = '" + request.To.Value.ToString("MM/dd/yyyy") + "'";
                response.Data = await ctx.Database.SqlQuery<SchedulerListResponse>(query).ToArrayAsync();

            }
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_get_appointment_for_user @responseType = 'DataCount',@UserType = '" + roleInfo.RoleName + "',@resourceId = '" + user.ResourceId + "',@pageSize = '" + request.PageSize + "',@pageIndex = '" + request.PageIndex + "',@startDate = '" + request.From.Value.ToString("MM/dd/yyyy") + "',@endDate = '" + request.To.Value.ToString("MM/dd/yyyy") + "'";
                response.ItemsCount = await ctx.Database.SqlQuery<long>(query).FirstOrDefaultAsync();
            }
            foreach (var item in response.Data)
            {
                var lastWorkOut = _patientWorkoutAnalyticRepository.List(new FilterQuery<PatientWorkoutAnalytic>().AddFilter(x => x.PatientId == item.PatientId)).OrderByDescending(x => x.StartDate).FirstOrDefault();
                if (lastWorkOut != null)
                {
                    item.PatientWorkOutId = lastWorkOut.Id;
                    item.PatientWorkOutCaseId = (lastWorkOut.CaseId != null) ? lastWorkOut.CaseId : 0;
                    item.WorkOutStartDate = (lastWorkOut.StartDate != null) ? lastWorkOut.StartDate : null;
                    item.WorkOutEndDate = (lastWorkOut.EndDate != null) ? lastWorkOut.EndDate : null;
                    item.PreExercisesPainRating = (lastWorkOut.PreExercisesPainRating != null) ? lastWorkOut.PreExercisesPainRating : 0;
                    item.WorkoutFeelResult = (lastWorkOut.WorkoutFeelResult != null) ? lastWorkOut.WorkoutFeelResult : 0;
                    item.WorkoutComment = (lastWorkOut.WorkoutComment != null) ? lastWorkOut.WorkoutComment : string.Empty;
                }
            }

            response = new ListResponse<SchedulerListResponse>(response.Data, response.ItemsCount);

            return Ok(response);
        }

    }
}
