﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Core.Services.ResourceScoreCalculation
{
    public class AvailabilityVsDemandFilter
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public double MinimalAvailability { get; set; }
    }
}
