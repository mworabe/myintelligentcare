﻿using System;
using PatientCare.Data.EntityFramework.Identity;
using FluentValidation;

namespace PatientCare.Web.Api.Models.RoleSetup
{
    public class RoleSetupRequestValidator : AbstractValidator<RoleSetupRequest>
    {
        public RoleSetupRequestValidator()
        {
            //RuleFor(model => model.Name)
            //    .NotEmpty()
            //    .Must(m => !string.Equals(m, ApplicationUser.AdminRole, StringComparison.InvariantCultureIgnoreCase));

            RuleFor(model => model.Description)
                .Length(0, 255);
        }
    }
}