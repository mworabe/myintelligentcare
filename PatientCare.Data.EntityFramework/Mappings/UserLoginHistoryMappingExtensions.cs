﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class UserLoginHistoryMappingExtensions
    {
        public static DbModelBuilder MapUserLoginHistory(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<UserLoginHistory>().ToTable("user_login_history").HasKey(i => i.Id);

            modelBuilder.Entity<UserLoginHistory>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<UserLoginHistory>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<UserLoginHistory>()
                .Property(p => p.UserId)
                .HasColumnName("user_id")
                .IsOptional();

            modelBuilder.Entity<UserLoginHistory>()
                .Property(p => p.Action)
                .HasColumnName("action")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<UserLoginHistory>()
                .Property(p => p.LogDateTime)
                .HasColumnName("log_date_time")
                .IsOptional()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<UserLoginHistory>()
                .Property(p => p.IPAddress)
                .HasColumnName("ip_address")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<UserLoginHistory>()
                .Property(p => p.SessionKey)
                .HasColumnName("session_key")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<UserLoginHistory>()
              .Property(p => p.Username)
              .HasColumnName("user_name")
              .IsOptional()
              .HasMaxLength(1024);
            
            return modelBuilder;
        }
    }
}
