﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class AppFunctionalRatingMappingExtensions
    {
        public static DbModelBuilder MapAppFunctionalRating(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<AppFunctionalRating>().ToTable("app_functional_ratings").HasKey(i => i.Id);

            modelBuilder.Entity<AppFunctionalRating>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<AppFunctionalRating>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();
            
            modelBuilder.Entity<AppFunctionalRating>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<AppFunctionalRating>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<AppFunctionalRating>()
                .Property(p => p.PatientCaseId)
                .HasColumnName("patient_case_id")
                .IsOptional();

            modelBuilder.Entity<AppFunctionalRating>()
                .HasOptional<PatientCase>(p => p.PatientCase)
                .WithMany()
                .HasForeignKey(p => p.PatientCaseId);

            modelBuilder.Entity<AppFunctionalRating>()
                .Property(p => p.Comment)
                .HasColumnName("comment")
                .IsOptional();
            
            modelBuilder.Entity<AppFunctionalRating>()
                .Property(p => p.FunctionalRating)
                .HasColumnName("functional_rating")
                .IsOptional();
            
            return modelBuilder;
        }
    }
}
