namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedPatientAddressTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.patient_address",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    patient_id = c.Long(),
                    address = c.String(maxLength: 255),
                    address_is_primary = c.Boolean(),
                    city_id = c.Long(),
                    state_id = c.Long(),
                    country_id = c.Long(),
                    zipcode = c.String(maxLength: 10),
                    Order = c.Int(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.city_id)
                .ForeignKey("dbo.countries", t => t.country_id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.States", t => t.state_id)
                .Index(t => t.patient_id)
                .Index(t => t.city_id)
                .Index(t => t.state_id)
                .Index(t => t.country_id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.patient_address", "state_id", "dbo.States");
            DropForeignKey("dbo.patient_address", "patient_id", "dbo.patients");
            DropForeignKey("dbo.patient_address", "country_id", "dbo.countries");
            DropForeignKey("dbo.patient_address", "city_id", "dbo.Cities");
            DropIndex("dbo.patient_address", new[] { "country_id" });
            DropIndex("dbo.patient_address", new[] { "state_id" });
            DropIndex("dbo.patient_address", new[] { "city_id" });
            DropIndex("dbo.patient_address", new[] { "patient_id" });
            DropTable("dbo.patient_address");
        }
    }
}
