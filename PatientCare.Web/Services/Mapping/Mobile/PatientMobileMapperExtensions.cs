﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.Patients;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class PatientMobileMapperExtensions
    {
        public static EmitMapperMapperProvider PatientMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient, PatientMobileResponse>
                    (new ExtendedMapConfig<Patient, PatientMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient[], PatientMobileResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientMobileRequest, Patient>(
                        new ExtendedMapConfig<PatientMobileRequest, Patient>()
                    ));

            return mapperProvider;
        }
    }
}