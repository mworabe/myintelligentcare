﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PatientCare.Core.Mapping;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Account;
using PatientCare.Web.Api.Models.Resources;
using PatientCare.Web.Models;
using PatientCare.Web.Services.WebApi;
using Microsoft.AspNet.Identity;
using Swashbuckle.Swagger.Annotations;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using PatientCare.Core.Services;
using PatientCare.Web.ApiMobile.Models.ChatSessions;
using PatientCare.Core.Entities;

namespace PatientCare.Web.Controllers
{
    //[Authorize(Roles = (ApplicationUser.AdminRole + "," + ApplicationUser.HumanResourceRole))]
    [ExceptionHandling]
    public class AccountsController : AuthBaseApiController
    {
        private readonly IChatService _chatService;

        public AccountsController(
            IChatService chatService
            )
        {
            _chatService = chatService;
        }

        private readonly static string chatUrl = ConfigurationManager.AppSettings["urlChat"];

        [HttpGet]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ResourceResponse>))]
        public async Task<IHttpActionResult> GetUsers([FromUri]ListRequest request)
        {
            // TODO fix this sorting by role
            Expression<Func<ApplicationUser, object>> customOrderby = null;
            if (string.IsNullOrEmpty(request.SortField))
            {
                request.SortField = "userName";
            }
            if (request.SortField == "role")
            {
                customOrderby = user => user.Roles.FirstOrDefault().RoleId;
            }
            var query = PagingExtensions<ApplicationUser>.CreatePagedQuery(request, string.IsNullOrWhiteSpace(request.SearchPhrase) ? null : GetSearchColumnExpressions(request.SearchPhrase), customOrderby);

            var readOptions = new ReadOptions<ApplicationUser>()
                .AsReadOnly()
                .WithIncludePredicate(x => x.Resource);

            var page = AppUserManager.PagedList(query, readOptions);
            var dtos = Mapper.Map<ApplicationUser[], AccountResponse[]>(page.Entities.ToArray());

            var roles = AppRoleManager.Roles.Where(x => x.IsActive == true).Select(x => x.Name).ToArray();

            foreach (var dto in dtos)
            {
                var role = await AppUserManager.GetRolesAsync(dto.Id);
                if (role.Any())
                    dto.Role = role.FirstOrDefault();
            }

            List<AccountResponse> newDtos = dtos.Where(x => x.Role != ApplicationUser.PatientRole).ToList();

            var response = new AccountListResponse(newDtos.ToArray(), page.TotalCount, roles);
            return Ok(response);
        }

        [HttpPost]
        [ResponseType(typeof(AccountRequest))]
        [SwaggerResponse(201, "Created", typeof(AccountRequest))]
        public async Task<IHttpActionResult> Create(AccountRequest model)
        {
            object result = string.Empty;
            object resultToken = string.Empty;

            if (model != null && (model.Password == null || model.Password == ""))
            {
                ModelState.AddModelError("password", "Password is required.");
                return BadRequest(ModelState);
            }

            IdentityResult passwordErrors = new IdentityResult();
            var passwordValid = true;
            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                passwordErrors = await AppUserManager.PasswordValidator.ValidateAsync(model.Password);
                passwordValid = passwordErrors.Succeeded;
            }
            if (!ModelState.IsValid || !passwordValid)
            {
                ModelState.AddModelError("password", passwordErrors.Errors.FirstOrDefault());
                return BadRequest(ModelState);
            }
            model.Id = Guid.NewGuid().ToString();
            var item = Mapper.Map<AccountRequest, ApplicationUser>(model);
            await AppUserManager.CreateAsync(item);
            await AppUserManager.AddToRoleAsync(item.Id, model.Role);
            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                await AppUserManager.RemovePasswordAsync(item.Id);
                await AppUserManager.AddPasswordAsync(item.Id, model.Password);
            }
            model.Password = Guid.NewGuid().ToString();

            if (item.Id != null)
            {

                using (var clientToken = new HttpClient())
                {
                    //clientToken.BaseAddress = new Uri(" http://localhost:1008/");
                    clientToken.BaseAddress = new Uri(chatUrl);
                    clientToken.DefaultRequestHeaders.Accept.Clear();
                    clientToken.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await clientToken.GetAsync("api/mobile/v1/auth/twilio-token?userId=" + item.Id + "&deviceType=");
                    if (response.IsSuccessStatusCode)
                    {
                        resultToken = await response.Content.ReadAsAsync<object>();
                    }
                }

                using (var client = new HttpClient())
                {
                    //client.BaseAddress = new Uri(" http://localhost:1008/");
                    client.BaseAddress = new Uri(chatUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.GetAsync("api/mobile/v1/auth/virgil-card?userId=" + item.Id + "");
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<object>();
                    }
                }

                if (result == null)
                {
                    ModelState.AddModelError("PrivateKey", "Can not Create Chat User please save again.");
                    return BadRequest(ModelState);
                }

                ChatKeyRequest key = new ChatKeyRequest();
                key.UserId = item.Id;
                key.PrivateKey = result.ToString();

                //await ChatKey(key);

                //if (!ModelState.IsValid)
                //    return BadRequest(ModelState);

              

                var chat = Mapper.Map<ChatKeyRequest, ChatKey>(key);
                await _chatService.InsertUpdateChatKeyAsync(chat);

                var chatkeys = await _chatService.ReadChatKeyAsync(chat.Id);

                //return Ok(chatkeys);

            }

            Response res = new Response();
            res.Id = model.Id;
            res.isUpdate = model.isUpdate;
            res.Password = model.Password;
            res.PatientId = model.PatientId;
            res.PrivateKey = result.ToString();
            res.ResourceId = model.ResourceId;
            res.Role = model.Role;
            res.UserId = model.Id;
            res.UserName = model.UserName;
            //return Ok(model);
            //return Ok(result + " " + item.Id);
            return Ok(res);
        }

        public class Response
        {
            public string Id { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
            public long? ResourceId { get; set; }
            public long? PatientId { get; set; }
            public string Role { get; set; }
            public bool isUpdate { get; set; }
            public string UserId { get; set; }
            public string PrivateKey { get; set; }
        }

        //public async Task<IHttpActionResult> ChatKey([FromBody] ChatKeyRequest chatKey)
        //{
        //    bool isUpdate = chatKey.Id > 0 ? true : false;

        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    var chat = Mapper.Map<ChatKeyRequest, ChatKey>(chatKey);
        //    await _chatService.InsertUpdateChatKeyAsync(chat);

        //    var chatkeys = await _chatService.ReadChatKeyAsync(chat.Id);

        //    //return Ok(chatkeys);

        //    return Ok(Mapper.Map<ChatKey, ChatKeyResponse>(chatkeys));

        //}

        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> EditItem(string id, [FromBody] AccountRequest itemVm)
        {
            var olduserDetails = await AppUserManager.FindByIdAsync(id);
            var olduser = Mapper.Map<ApplicationUser, AccountResponse>(olduserDetails);
            var roles = AppRoleManager.Roles.Select(x => x.Name).ToArray();
            var role = await AppUserManager.GetRolesAsync(olduser.Id);
            olduser.Role = role.FirstOrDefault();

            if (olduser.UserName == ApplicationUser.SystemAdminUser && olduser.Role == ApplicationUser.AdminRole)
            {
                if (itemVm.UserName != ApplicationUser.SystemAdminUser || itemVm.Role != ApplicationUser.AdminRole)
                {
                    return BadRequest("Can not modify username or role for Admin user.");
                }
            }

            IdentityResult passwordErrors = new IdentityResult();
            var passwordValid = true;
            if (!string.IsNullOrWhiteSpace(itemVm.Password))
            {
                passwordErrors = await AppUserManager.PasswordValidator.ValidateAsync(itemVm.Password);
                passwordValid = passwordErrors.Succeeded;
            }
            if (!ModelState.IsValid || !passwordValid)
            {
                ModelState.AddModelError("password", passwordErrors.Errors.FirstOrDefault());
                return BadRequest(ModelState);
            }

            var user = await AppUserManager.FindByIdAsync(id);

            TheModelFactory.SetApplicationUserModel(user, itemVm);
            user.Roles.Clear();
            await AppUserManager.UpdateAsync(user);
            await AppUserManager.AddToRoleAsync(id, itemVm.Role);

            if (!string.IsNullOrWhiteSpace(itemVm.Password))
            {
                await AppUserManager.RemovePasswordAsync(itemVm.Id);
                await AppUserManager.AddPasswordAsync(itemVm.Id, itemVm.Password);
            }

            return Ok(Mapper.Map<ApplicationUser, AccountResponse>(user));
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteUser(string id)
        {

            //Only SuperAdmin or Admin can delete users (Later when implement roles)

            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser != null)
            {
                IdentityResult result = await AppUserManager.DeleteAsync(appUser);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                return Ok();

            }
            return NotFound();
        }

        [Authorize(Roles = ApplicationUser.AdminRole)]
        [Route("user/{id:guid}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(string id)
        {
            //Only SuperAdmin or Admin can delete users (Later when implement roles)
            var user = await AppUserManager.FindByIdAsync(id);

            if (user != null)
            {
                return Ok(TheModelFactory.Create(user));
            }
            return NotFound();
        }

        [Route("api/Accounts/user-rules")]
        public async Task<IHttpActionResult> GetUserRules()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return StatusCode(HttpStatusCode.Unauthorized);
            }
            var roles = await AppUserManager.GetRolesAsync(User.Identity.GetUserId());

            var roleString = roles.FirstOrDefault();
            if (roleString == null)
            {
                return StatusCode(HttpStatusCode.Unauthorized);
            }

            var role = await AppRoleManager.FindByNameAsync(roleString);
            if (role == null)
            {
                return NotFound();
            }

            return Ok(role.JsonSetup);

        }

        [Authorize(Roles = ApplicationUser.AdminRole)]
        [Route("user/{username}")]
        public async Task<IHttpActionResult> GetUserByName(string username)
        {
            //Only SuperAdmin or Admin can delete users (Later when implement roles)
            var user = await AppUserManager.FindByNameAsync(username);

            if (user != null)
            {
                return Ok(TheModelFactory.Create(user));
            }

            return NotFound();
        }

        [AllowAnonymous]
        [Route("create")]
        public async Task<IHttpActionResult> CreateUser(CreateUserBindingModel createUserModel)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser()
            {
                UserName = createUserModel.Username
                //Level = 3,
                //JoinDate = DateTime.Now.Date,
            };

            if (!(await AppRoleManager.RoleExistsAsync(createUserModel.RoleName)))
            {
                return BadRequest(string.Format("Role {0} does not exist.", createUserModel.RoleName));
            }

            if (createUserModel.RoleName == ApplicationUser.FrontOfficeRole)
            {
                user.ResourceId = createUserModel.ResourceId;
            }

            var addUserResult = await AppUserManager.CreateAsync(user, createUserModel.Password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }
            var addRoleResult = await AppUserManager.AddToRoleAsync(user.Id, createUserModel.RoleName);

            if (!addRoleResult.Succeeded)
            {
                return GetErrorResult(addRoleResult);
            }

            var code = await this.AppUserManager.GenerateEmailConfirmationTokenAsync(user.Id);

            var callbackUrl = new Uri(Url.Link("ConfirmEmailRoute", new { userId = user.Id, code }));

            await this.AppUserManager.SendEmailAsync(user.Id,
                                                    "Confirm your account",
                                                    "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

            var locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));

            return Created(locationHeader, TheModelFactory.Create(user));

        }

        [AllowAnonymous]
        [HttpGet]
        [Route("ConfirmEmail", Name = "ConfirmEmailRoute")]
        public async Task<IHttpActionResult> ConfirmEmail(string userId = "", string code = "")
        {
            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(code))
            {
                ModelState.AddModelError("", "User Id and Code are required");
                return BadRequest(ModelState);
            }

            IdentityResult result = await this.AppUserManager.ConfirmEmailAsync(userId, code);

            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return GetErrorResult(result);
            }
        }

        [HttpPost]
        [Route("api/Accounts/change-password")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await this.AppUserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        [Authorize(Roles = ApplicationUser.AdminRole)]
        [Route("user/{id:guid}/roles")]
        [HttpPut]
        public async Task<IHttpActionResult> AssignRolesToUser([FromUri] string id, [FromBody] string[] rolesToAssign)
        {

            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser == null)
            {
                return NotFound();
            }

            var currentRoles = await AppUserManager.GetRolesAsync(appUser.Id);

            var rolesNotExists = rolesToAssign.Except(this.AppRoleManager.Roles.Select(x => x.Name)).ToArray();

            if (rolesNotExists.Any())
            {

                ModelState.AddModelError("", string.Format("Roles '{0}' does not exixts in the system", string.Join(",", rolesNotExists)));
                return BadRequest(ModelState);
            }

            var removeResult = await AppUserManager.RemoveFromRolesAsync(appUser.Id, currentRoles.ToArray());

            if (!removeResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to remove user roles");
                return BadRequest(ModelState);
            }

            var addResult = await AppUserManager.AddToRolesAsync(appUser.Id, rolesToAssign);

            if (!addResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to add user roles");
                return BadRequest(ModelState);
            }

            return Ok();

        }

        [Authorize(Roles = ApplicationUser.AdminRole)]
        [Route("user/{id:guid}/assignclaims")]
        [HttpPut]
        public async Task<IHttpActionResult> AssignClaimsToUser([FromUri] string id, [FromBody] List<ClaimBindingModel> claimsToAssign)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser == null)
            {
                return NotFound();
            }

            foreach (var claimModel in claimsToAssign)
            {
                if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                {

                    await this.AppUserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
                }

                await this.AppUserManager.AddClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
            }

            return Ok();
        }

        [Authorize(Roles = ApplicationUser.AdminRole)]
        [Route("user/{id:guid}/removeclaims")]
        [HttpPut]
        public async Task<IHttpActionResult> RemoveClaimsFromUser([FromUri] string id, [FromBody] List<ClaimBindingModel> claimsToRemove)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var appUser = await this.AppUserManager.FindByIdAsync(id);

            if (appUser == null)
            {
                return NotFound();
            }

            foreach (ClaimBindingModel claimModel in claimsToRemove)
            {
                if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                {
                    await this.AppUserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
                }
            }

            return Ok();
        }

        private static Expression<Func<ApplicationUser, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return user => user.UserName.Contains(searchPhrase)
                || string.Concat(user.Resource.FirstName, " ", user.Resource.LastName).Contains(searchPhrase);
        }

    }
}