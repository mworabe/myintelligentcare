﻿using System;
using System.Configuration;
using PatientCare.Data.EntityFramework;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.Services.Authentication;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace PatientCare.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        private void ConfigureOAuthTokenGeneration(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(PatientCareDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManagerFactory.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManagerFactory.Create);

            #region For web Token Generate
            var oAuthServerOptions = new OAuthAuthorizationServerOptions();

            //For Dev enviroment only (on production should be AllowInsecureHttp = false)
            oAuthServerOptions.AllowInsecureHttp = true;

            oAuthServerOptions.TokenEndpointPath = new PathString("/oauth/token");
            //oAuthServerOptions.AccessTokenExpireTimeSpan = TimeSpan.FromDays(1);

            int? applicationSeesionExpiryTime = LoginAttemptService.ApplicationSessionExpiryTime;
            oAuthServerOptions.AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(applicationSeesionExpiryTime.Value);

            oAuthServerOptions.Provider = new CustomOAuthProvider();
            oAuthServerOptions.RefreshTokenProvider = new RefreshTokenProvider();

            oAuthServerOptions.AccessTokenFormat = new CustomJwtFormat(ConfigurationManager.AppSettings["jwt:Issuer"]);

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            #endregion

            #region For Mobile Token Generate
            var oAuthServerMobileOptions = new OAuthAuthorizationServerOptions();

            //For Dev enviroment only (on production should be AllowInsecureHttp = false)
            oAuthServerMobileOptions.AllowInsecureHttp = true;

            oAuthServerMobileOptions.TokenEndpointPath = new PathString("/oauth/tokenMobile");
            //oAuthServerOptions.AccessTokenExpireTimeSpan = TimeSpan.FromDays(1);

            int? mobileApplicationSeesionExpiryTime = LoginAttemptService.MobileApplicationSessionExpiryTime;
            oAuthServerMobileOptions.AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(mobileApplicationSeesionExpiryTime.Value);

            oAuthServerMobileOptions.Provider = new CustomOAuthMobileProvider();
            oAuthServerMobileOptions.RefreshTokenProvider = new RefreshMobileTokenProvider();

            oAuthServerMobileOptions.AccessTokenFormat = new CustomJwtFormat(ConfigurationManager.AppSettings["jwt:Issuer"]);

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerMobileOptions); 
            #endregion
        }

        private void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {
            var issuer = ConfigurationManager.AppSettings["jwt:Issuer"];
            var audienceId = ConfigurationManager.AppSettings["jwt:AudienceId"];
            var audienceSecret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["jwt:AudienceSecret"]);

            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { audienceId },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, audienceSecret)
                    }
                });
        }
    }
}