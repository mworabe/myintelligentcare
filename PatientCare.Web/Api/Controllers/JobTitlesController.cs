﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.JobTitles;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Job Titles operations.
    /// </summary>
    [Authorize]
    public sealed class JobTitlesController : EdzBaseApiController<JobTitle, JobTitleRequest, JobTitleResponse>
    {
        /// <summary>
        /// Description : In this class we pass Job Titles IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="picklistRepository">Certificate Entity</param>
        public JobTitlesController(IPicklistRepository<JobTitle> picklistRepository)
            : base(picklistRepository)
        {
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return crud == Crud.Read ? null : GetPicklistStatusCodeError(Picklist.JobTitles);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Job Titles.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Job Titles Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/JobTitles/autocomplete")]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<JobTitle>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
            }
            var autocompleteResult =
                await
                    EntityRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<JobTitle[], JobTitlePickListResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }

        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/JobTitles/" + id, UriKind.Relative);
        }
    }
}