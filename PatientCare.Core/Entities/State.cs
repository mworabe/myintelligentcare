﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for State.
    /// </summary>
    public class State : NamedEntity
    {
        public string StateCode { get; set; }

        public long? CountryId { get; set; }

    }
}
