﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.PatientCases;
using PatientCare.Core.Enums;
using PatientCare.Utilities;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using System.Collections.Generic;
using PatientCare.Web.Api.Models.PatientAppointment;
using System.Runtime.Serialization;
using System.Web.Http.Description;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Patient Cases operations.
    /// </summary>
    [Authorize]
    public sealed class PatientCasesController : ApiBaseController
    {
        /// <summary>
        /// Description : Patient Cases IPatientService.
        /// </summary>
        private readonly IPatientService _patientService;
        /// <summary>
        /// Description : Patient Cases IPatientCaseRepository.
        /// </summary>
        private readonly IPatientCaseRepository _patientCaseRepository;
        /// <summary>
        /// Description : Patient Cases IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<PatientCase> _readOptions = new ReadOptions<PatientCase>();

        /// <summary>
        /// Description : Initializes a new instance of the Patient Cases Class.   
        /// </summary>
        /// <param name="patientService">IPatient Service</param>
        /// <param name="patientCaseRepository">IPatient Case Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public PatientCasesController
            (
                IPatientService patientService,
                IPatientCaseRepository patientCaseRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _patientService = patientService;
            _patientCaseRepository = patientCaseRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/patientcases/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Description : Case Response Class.     
        /// </summary>
        public class CaseResponse
        {

            [DataMember]
            public string Status { get; set; }

            [DataMember]
            public object DataObject { get; set; }

        }

        /// <summary>
        /// Method Description : Using for Get All Patient Cases.
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="request">Patient Case List Request Model</param>
        /// <returns>Patient Case List Response Model</returns>
        [HttpGet]
        [Route("api/patientcases/{id}/cases")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<PatientCaseListResponse>))]
        public IHttpActionResult GetAllPatientCase(long Id, [FromUri]PatientCaseListRequest request)
        {
            if (Id == 0)
                return NotFound();

            var query = PagingExtensions<PatientCase>.CreatePagedQuery(request);

            query.AddFilter(x => x.PatientId == Id);

            // here we not take deleted patient in list screen
            query.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);

            var readOptions = new ReadOptions<PatientCase>().AsReadOnly();
            var page = _patientCaseRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<PatientCase[], PatientCaseResponse[]>(page.Entities.OrderByDescending(x => x.Id).ToArray());

            var response = new ListResponse<PatientCaseResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Patient Case.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="patientCaseRequest">Patient Case Request Model</param>
        /// <returns>Patient Case Response Model</returns>
        [HttpPost]
        [Route("api/patientcases/{id}/cases")]
        [SwaggerResponse(201, "Created", typeof(PatientCaseResponse))]
        public async Task<IHttpActionResult> CreatePatientCase(long id, [FromBody]PatientCaseRequest patientCaseRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (_patientCaseRepository.Exists(FilterQuery.Create<PatientCase>().AddFilter(p => p.PatientId == id && (p.CaseStatus == CaseStatusEnum.Inprogress && (p.IsDeleted == false || p.IsDeleted == null)))))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<PatientCase>(p => p.CaseNo), DisplayResources.Error_CaseExists);
                return BadRequest(ModelState);
            }

            patientCaseRequest.PatientId = id;
            var patientDetail = await _patientService.ReadPatientAsync(patientCaseRequest.PatientId.Value);
            patientCaseRequest.PatientNumber = patientDetail.PatientNumber;
            patientCaseRequest.BirthDate = (patientCaseRequest.BirthDate != null) ? patientCaseRequest.BirthDate : patientDetail.BirthDate;

            var patientCase = Mapper.Map<PatientCaseRequest, PatientCase>(patientCaseRequest);
            patientCase.CaseStatus = CaseStatusEnum.Inprogress;

            await _patientService.CreatePatientCaseAsync(patientCase);

            await _patientService.UpdatePatientCaseNoAsync(patientCase.PatientId, patientCase.Id);

            return Created(GetEntityLocation(patientCase.Id), Mapper.Map<PatientCase, PatientCaseResponse>(patientCase));
        }

        /// <summary>
        /// Method Description : Using for Update Patient Case.   
        /// </summary>
        /// <param name="patientId">Patient Id</param>
        /// <param name="Id">Id</param>
        /// <param name="patientCaseRequest">Patient Case Request Model</param>
        /// <returns>Patient Case Response Model</returns>
        [HttpPut]
        [Route("api/patientcases/{patientId}/cases/{id}")]
        public async Task<IHttpActionResult> EditPatientCase(long patientId, long Id, [FromBody] PatientCaseRequest patientCaseRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //var hepDetailList = new List<String>();

            //CaseResponse Response = new CaseResponse();

            //using (var dbContextScope = _dataContextScopeFactory.Create())
            //{
            //    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
            //    var patient = ctx.PatientCases.AsNoTracking().Where(x => x.PatientId == patientId && x.Id != Id).ToList();
            //    foreach (var patientCaseStatus in patient.ToList())
            //    {
            //        hepDetailList.Add(patientCaseStatus.CaseStatus.ToString());
            //    }

            //    if (patientCaseRequest.CaseStatus == CaseStatusEnum.Inprogress)
            //    {
            //        var existsCaseStatus = hepDetailList.Contains(patientCaseRequest.CaseStatus.ToString());
            //        var patientCaseStatus = ctx.PatientCases.AsNoTracking().Where(x => x.PatientId == patientId && x.CaseStatus == CaseStatusEnum.Inprogress).FirstOrDefault();
            //        if (existsCaseStatus == true)
            //            hepDetailList.Clear();
            //        string message = "Cannot make this case in Inprogress due to Case no : " + patientCaseStatus.CaseNo + " is already in Inprogress" ;


            //        Response.Status = "False";
            //        Response.DataObject = message;

            //        return Ok(Response);
            //    }
            //}

            patientCaseRequest.PatientId = patientId;
            var patientDetail = await _patientService.ReadPatientAsync(patientCaseRequest.PatientId.Value);

            var patientCaseDetial = await _patientService.ReadPatientCaseAsync(Id);

            patientCaseRequest.PatientNumber = patientDetail.PatientNumber;
            patientCaseRequest.BirthDate = (patientCaseRequest.BirthDate != null) ? patientCaseRequest.BirthDate : patientDetail.BirthDate;

            var patientCase = Mapper.Map<PatientCaseRequest, PatientCase>(patientCaseRequest);
            patientCase.Id = Id;

            if (patientCaseDetial.CaseStatus == CaseStatusEnum.Discharged)
            {
                return Ok("You can not change Discharged case");
            }

            await _patientService.UpdatePatientCaseAsync(patientCase);

            //var res = (Mapper.Map<PatientCase, PatientCaseResponse>(patientCase));

            //Response.Status = "True";
            //Response.DataObject = res;

            return Ok((Mapper.Map<PatientCase, PatientCaseResponse>(patientCase)));
        }

        /// <summary>
        /// Method Description : Using for Delete Patient Case by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patient Case Response Model</returns>
        [HttpDelete]
        [Route("api/patientcases/cases/{id}")]
        public async Task<IHttpActionResult> DeletePatientCaseItem(long id)
        {
            var success = await _patientService.DeletePatientCaseAsync(id);
            if (!success)
                return NotFound();

            return Ok(new PatientCase { Id = id });
        }

        /// <summary>
        /// Method Description : Using for Get Patients Case by Id and Patient Id.    
        /// </summary>
        /// <param name="patientId">Patient Id</param>
        /// <param name="Id">Id</param>
        /// <returns>Patients Case Response Model</returns>
        [HttpGet]
        [Route("api/patientcases/{patientId}/cases/{id}")]
        public async Task<IHttpActionResult> GetPatientCase(long patientId, long Id)
        {
            var patientCase = await _patientService.ReadPatientCaseAsync(Id);
            if (patientCase == null)
                return NotFound();

            if (patientCase.IsDeleted == true)
                return NotFound();

            var viewModel = Mapper.Map<PatientCase, PatientCaseResponse>(patientCase);
            return Ok(viewModel);
        }

        [HttpGet]
        [Route("api/PatientsCases/autocompletee")]
        [ResponseType(typeof(List<PatientCaseResponse>))]
        public async Task<IHttpActionResult> Autocompletee([FromUri] long search)
        {
            List<PatientCaseResponse> dd = new List<PatientCaseResponse>();
            List<PatientCaseResponse> respons = new List<PatientCaseResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC SP_PatientCases @Id =" + search;
                respons = await ctx.Database.SqlQuery<PatientCaseResponse>(query).ToListAsync();
            }
            return Ok(respons);
        }

        [HttpGet]
        [Route("api/PatientsCases/autocomplete")]
        [ResponseType(typeof(PatientCaseAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            try
            {
                var filter = new FilterQuery<PatientCase>();

                filter.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);
                var autocompleteResult = await _patientCaseRepository.AutocompleteAsync(filter);

                var result = Mapper.Map<PatientCase[], PatientCaseAutoCompleteResponse[]>(autocompleteResult.ToArray());

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
