﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Pass Fail Enum.
    /// </summary>
    public enum PassFailEnum
    {
        Pass = 0,
        Fail = 1
    }
}
