﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.HepMasters;
using PatientCare.Web.Services.FileStorage;
using System.Collections.Generic;
using PatientCare.Core.Enums;
using PatientCare.Web.ApiMobile.Models.HepMasters;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.ApiMobile.Models.HepPatientPlan;
using PatientCare.Data.EntityFramework;
using PatientCare.Data.AbstractDataContext;
using System.Data.Entity;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Hep Master operations.
    /// </summary>
    //[Authorize(Roles = ApplicationUser.PatientRole)]
    public sealed class HepMastersMobileController : ApiBaseController
    {
        /// <summary>
        /// Description : Hep Master IHepService.
        /// </summary>
        private readonly IHepService _hepService;
        /// <summary>
        /// Description : Hep Master IHepGroupRepository.
        /// </summary>
        private readonly IHepGroupRepository _hepGroupRepository;
        /// <summary>
        /// Description : Hep Master IHepMasterRepository.
        /// </summary>
        private readonly IHepMasterRepository _hepMasterRepository;
        /// <summary>
        /// Description : Hep Master IHepDetailRepository.
        /// </summary>
        private readonly IHepDetailRepository _hepDetailRepository;
        /// <summary>
        /// Description : Hep Master IHepFrequencyRepository.
        /// </summary>
        private readonly IHepFrequencyRepository _hepFrequencyRepository;
        /// <summary>
        /// Description : Hep Master IExerciseRepository.
        /// </summary>
        private readonly IExerciseRepository _exerciseRepository;
        /// <summary>
        /// Description : Hep Master IPatientCaseRepository.
        /// </summary>
        private readonly IPatientCaseRepository _patientCaseRepository;
        /// <summary>
        /// Description : Hep Master IPatientRepository.
        /// </summary>
        private readonly IPatientRepository _patientRepository;
        /// <summary>
        /// Description : Hep Master IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;


        ReadOptions<HepMaster> _readOptions = new ReadOptions<HepMaster>();

        /// <summary>
        /// Description : Initializes a new instance of the Hep Master Class.
        /// </summary>
        /// <param name="hepService">IHep Service</param>
        /// <param name="hepGroupRepository">IHepGroup Repository</param>
        /// <param name="hepMasterRepository">IHepMaster Repository</param>
        /// <param name="hepDetailRepository">IHepDetail Repository</param>
        /// <param name="hepFrequencyRepository">IHepFrequency Repository</param>
        /// <param name="exerciseRepository">IExercise Repository</param>
        /// <param name="patientCaseRepository">IPatientCase Repository</param>
        /// <param name="patientRepository">IPatient Repository</param>
        /// <param name="dataContextScopeFactory">IDataContext Scope Factory</param>
        public HepMastersMobileController
            (
                IHepService hepService,
                IHepGroupRepository hepGroupRepository,
                IHepMasterRepository hepMasterRepository,
                IHepDetailRepository hepDetailRepository,
                IHepFrequencyRepository hepFrequencyRepository,
                IExerciseRepository exerciseRepository,
                IPatientCaseRepository patientCaseRepository,
                IPatientRepository patientRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _hepService = hepService;
            _hepGroupRepository = hepGroupRepository;
            _hepMasterRepository = hepMasterRepository;
            _hepDetailRepository = hepDetailRepository;
            _hepFrequencyRepository = hepFrequencyRepository;
            _exerciseRepository = exerciseRepository;
            _patientCaseRepository = patientCaseRepository;
            _patientRepository = patientRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/HepMasters/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Hep Master by Id.    
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>Hep Master Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/HepMasters/{Id}")]
        public async Task<IHttpActionResult> GetHepMaster(long Id)
        {
            //var currentCaseDetail = _patientCaseRepository.List(new FilterQuery<PatientCase>().AddFilter(x => x.PatientId == Id && x.CaseStatus == CaseStatusEnum.Inprogress)).FirstOrDefault();
            var HepMaster = await _hepService.ReadAsync(Id);
            if (HepMaster == null)
                return NotFound();

            var viewModel = Mapper.Map<HepMaster, HepMasterResponse>(HepMaster);

            foreach (var file in viewModel.HepDetails)
            {
                foreach (var exercise in file.Exercise.Medias)
                {
                    exercise.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.Name, GetExerciseMediaS3Path(exercise.ExerciseId.Value));
                    exercise.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                    List<FileRequest> fileAll = new List<FileRequest>();
                    FileRequest tempFile = new FileRequest();

                    if (StorageService.UseAmazonStorage == true)
                        tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(exercise.Name, GetExerciseMediaS3Path(exercise.ExerciseId.Value));
                    else
                        tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + exercise.Id;

                    tempFile.S3BucketName = exercise.Name;
                    tempFile.Name = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                    #region

                    if (exercise.ThumbImageName != null)
                    {
                        exercise.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.ThumbImageName, GetExerciseMediaS3Path(exercise.ExerciseId.Value));
                        exercise.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);

                        if (StorageService.UseAmazonStorage == true)
                            tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(exercise.ThumbImageName, GetExerciseMediaS3Path(exercise.ExerciseId.Value));
                        else
                            tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                        tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);
                    }
                    #endregion

                    fileAll.Add(tempFile);
                    exercise.Files = fileAll.ToArray();
                }
            }

            return Ok(viewModel);
        }

        private string GetExerciseMediaS3Path(long exerciseId)
        {
            return string.Format(FileUploadHelpers.ExerciseMediaFilesTemplate, exerciseId);
        }

        /// <summary>
        /// Method Description : Using for Get Hep Master by Case Id. 
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="request">Hep Master List Request Model</param>
        /// <returns>Hep Master Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/HepMasters/{Id}/cases")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<HepMasterMobileListResponse>))]
        public async Task<IHttpActionResult> GetHepMasterByCases(long Id, [FromUri]HepMasterListRequest request)
        {
            request.PageIndex = 1;
            request.PageSize = 999;
            var query = PagingExtensions<HepMaster>.CreatePagedQuery(request);
            query.AddFilter(x => x.PatientCaseId == Id);

            var readOptions = new ReadOptions<HepMaster>().AsReadOnly();
            var page = _hepMasterRepository.PagedList(query, readOptions);

            var response = new HepMasterResponse();
            if (page.Entities.Length > 0)
            {
                var dtos = Mapper.Map<HepMaster, HepMasterResponse>(page.Entities[0]);
                response = dtos;

                foreach (var exercise in response.HepDetails)
                {
                    foreach (var file in exercise.Exercise.Medias)
                    {
                        file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(exercise.Id));
                        file.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(file.Name);

                        List<FileRequest> fileAll = new List<FileRequest>();
                        FileRequest tempFile = new FileRequest();

                        if (StorageService.UseAmazonStorage == true)
                            tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(file.Name, GetExerciseMediaS3Path(file.ExerciseId.Value));
                        else
                            tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + file.Id;

                        tempFile.S3BucketName = file.Name;
                        tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                        #region

                        if (file.ThumbImageName != null)
                        {
                            file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(exercise.Id));
                            file.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);

                            if (StorageService.UseAmazonStorage == true)
                                tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(file.ThumbImageName, GetExerciseMediaS3Path(file.ExerciseId.Value));
                            else
                                tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                            tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                        }
                        #endregion


                        fileAll.Add(tempFile);
                        file.Files = fileAll.ToArray();
                    }
                }
            }

            return Ok(response);

        }

        /// <summary>
        /// Method Description : Using for Get Hep Master by Patient Id. 
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>Hep Get Mobile Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/HepMasters/{Id}/group")]  //Where Id is PatientId 
        public IHttpActionResult GetHepMasterByCase(long Id)
        {
            List<HepDetailMobileResponse> listHepDetail = new List<HepDetailMobileResponse>();
            List<HepGroupMobileResponse> listHepGroup = new List<HepGroupMobileResponse>();
            List<HepExerciseMobileResponse> listHepExercise = new List<HepExerciseMobileResponse>();
            HepDetailMobileResponse hepDetailMobile = new HepDetailMobileResponse();

            var currentCaseDetail = _patientCaseRepository.List
                                  (new FilterQuery<PatientCase>().AddFilter
                                  (x => x.PatientId == Id && x.CaseStatus == CaseStatusEnum.Inprogress && (x.IsDeleted == false || x.IsDeleted == null))).FirstOrDefault();

            if (currentCaseDetail != null)
            {
                var CaseId = currentCaseDetail.Id;

                var readOptions = new ReadOptions<HepMaster>().AsReadOnly();
                var page = _hepMasterRepository.List(new FilterQuery<HepMaster>().AddFilter(x => x.PatientCaseId == CaseId), readOptions).FirstOrDefault();
                //var dtos = Mapper.Map<HepMaster, HepMasterResponse>(page);
                var dtos = Mapper.Map<HepMaster, HepMasterMobileResponse>(page);

                hepDetailMobile.StartDate = dtos.StartDate;
                hepDetailMobile.EndDate = dtos.EndDate;
                hepDetailMobile.updatedDate = dtos.Modified;

                if (page != null)
                {
                    foreach (var hepexercise in dtos.HepDetails)
                    {
                        if (hepexercise.Group != null)
                        {
                            HepGroupMobileResponse dataHepGroupMobile = new HepGroupMobileResponse();
                            dataHepGroupMobile.GroupId = (hepexercise.GroupId != null) ? hepexercise.GroupId : 0;
                            dataHepGroupMobile.Name = (hepexercise.Group != null) ? hepexercise.Group.Name : string.Empty;
                            listHepGroup.Add(dataHepGroupMobile);
                        }
                        HepExerciseMobileResponse hepExerciseMobile = new HepExerciseMobileResponse();
                        hepExerciseMobile.Id = hepexercise.Id;
                        hepExerciseMobile.ExerciseId = hepexercise.ExerciseId;
                        hepExerciseMobile.ExerciseName = hepexercise.Exercise.Name;
                        hepExerciseMobile.Comments = hepexercise.Exercise.Comments;
                        hepExerciseMobile.GroupId = hepexercise.GroupId;
                        hepExerciseMobile.HepId = hepexercise.HepId;
                        hepExerciseMobile.Sets = hepexercise.Sets;
                        hepExerciseMobile.Reps = hepexercise.Reps;
                        hepExerciseMobile.Time = hepexercise.Time;
                        hepExerciseMobile.TimeUnit = hepexercise.TimeUnit;
                        hepExerciseMobile.Weight = hepexercise.Weight;
                        hepExerciseMobile.Holds = hepexercise.Holds;
                        hepExerciseMobile.HoldsUnit = hepexercise.HoldsUnit;
                        hepExerciseMobile.ExerciseOrder = hepexercise.ExerciseOrder;
                        hepExerciseMobile.Frequencies = hepexercise.Frequencies;
                        hepExerciseMobile.Medias = hepexercise.Exercise.Medias;

                        foreach (var file in hepexercise.Exercise.Medias)
                        {
                            file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(hepexercise.Exercise.Id));
                            //file.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(file.Name);

                            List<FileRequest> fileAll = new List<FileRequest>();
                            FileRequest tempFile = new FileRequest();

                            //if (StorageService.UseAmazonStorage == true)
                            //    tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(file.Name, GetExerciseMediaS3Path(file.ExerciseId.Value));
                            //else
                            //    tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + file.Id;

                            tempFile.S3BucketName = file.Name;
                            tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                            #region

                            if (file.ThumbImageName != null)
                            {
                                file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(hepexercise.Exercise.Id));
                                file.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);

                                if (StorageService.UseAmazonStorage == true)
                                    tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(file.ThumbImageName, GetExerciseMediaS3Path(hepexercise.Exercise.Id));
                                else
                                    tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                                tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                            }
                            #endregion

                            fileAll.Add(tempFile);
                            //file.Files = fileAll.ToArray();
                        }
                        listHepExercise.Add(hepExerciseMobile);
                    }
                }
            }

            HepGetMobileResponse newhepGetMobile = new HepGetMobileResponse();
            newhepGetMobile.HepDetails = hepDetailMobile;
            newhepGetMobile.HepGroups = listHepGroup.ToArray();
            newhepGetMobile.HepExercises = listHepExercise.ToArray();

            return Ok(newhepGetMobile);
        }

        /// <summary>
        /// Method Description : Using for Get Hep Master Group by Patient Id. 
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>Hep GetMobile Response Model</returns>
        [HttpGet]
        //[Route("api/mobile/v1/HepMasters/{Id}/case")]  //Where Id is PatientId
        [Route("api/mobile/v1/PatientHepMasters/{Id}")]
        public IHttpActionResult GetHepMasterGroupByCase(long Id)
        {
            List<HepDetailMobileResponse> listHepDetail = new List<HepDetailMobileResponse>();
            List<HepGroupMobileResponse> listHepGroup = new List<HepGroupMobileResponse>();
            List<HepExerciseMobileResponse> listHepExercise = new List<HepExerciseMobileResponse>();
            List<HepExerciseMobileResponse> listHepGroupExercise = new List<HepExerciseMobileResponse>();
            HepDetailMobileResponse hepDetailMobile = new HepDetailMobileResponse();

            var currentCaseDetail = _patientCaseRepository.List(new FilterQuery<PatientCase>()
                                .AddFilter(x => x.PatientId == Id && x.CaseStatus == CaseStatusEnum.Inprogress && (x.IsDeleted == false || x.IsDeleted == null))).FirstOrDefault();

            if (currentCaseDetail != null)
            {
                var CaseId = currentCaseDetail.Id;

                var readOptions = new ReadOptions<HepMaster>().AsReadOnly();
                var page = _hepMasterRepository.List(new FilterQuery<HepMaster>().AddFilter(x => x.PatientCaseId == CaseId), readOptions).FirstOrDefault();

                var dtos = Mapper.Map<HepMaster, HepMasterGroupMobileResponse>(page);
                if (dtos != null)
                {
                    hepDetailMobile.StartDate = dtos.StartDate;
                    hepDetailMobile.EndDate = dtos.EndDate;
                    hepDetailMobile.updatedDate = dtos.Modified;

                    foreach (var hepexercise in dtos.HepDetails.Where(x => x.GroupId == null))
                    {
                        HepExerciseMobileResponse hepExerciseMobile = new HepExerciseMobileResponse();
                        hepExerciseMobile.Id = hepexercise.Id;
                        hepExerciseMobile.ExerciseId = hepexercise.ExerciseId;
                        hepExerciseMobile.ExerciseName = hepexercise.Exercise.Name;
                        //hepExerciseMobile.Comments = hepexercise.Exercise.Comments;
                        hepExerciseMobile.Comments = hepexercise.Comments;
                        hepExerciseMobile.GroupId = hepexercise.GroupId;
                        hepExerciseMobile.HepId = hepexercise.HepId;
                        hepExerciseMobile.Sets = hepexercise.Sets;
                        hepExerciseMobile.Reps = hepexercise.Reps;
                        hepExerciseMobile.Time = hepexercise.Time;
                        hepExerciseMobile.TimeUnit = hepexercise.TimeUnit;
                        hepExerciseMobile.Weight = hepexercise.Weight;
                        hepExerciseMobile.WeightUnit = hepexercise.WeightUnit;
                        hepExerciseMobile.Holds = hepexercise.Holds;
                        hepExerciseMobile.HoldsUnit = hepexercise.HoldsUnit;
                        hepExerciseMobile.ExerciseOrder = hepexercise.ExerciseOrder;
                        hepExerciseMobile.Frequencies = hepexercise.Frequencies;
                        hepExerciseMobile.Medias = hepexercise.Exercise.Medias;

                        foreach (var file in hepexercise.Exercise.Medias)
                        {
                            file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(hepexercise.Exercise.Id));
                            //file.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(file.Name);

                            List<FileRequest> fileAll = new List<FileRequest>();
                            FileRequest tempFile = new FileRequest();

                            //if (StorageService.UseAmazonStorage == true)
                            //    tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(file.Name, GetExerciseMediaS3Path(file.ExerciseId.Value));
                            //else
                            //    tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + file.Id;

                            tempFile.S3BucketName = file.Name;
                            tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                            #region

                            if (file.ThumbImageName != null)
                            {
                                file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(hepexercise.Exercise.Id));
                                //file.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);

                                //if (StorageService.UseAmazonStorage == true)
                                //    tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(file.ThumbImageName, GetExerciseMediaS3Path(hepexercise.Exercise.Id));
                                //else
                                //    tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                                tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                            }
                            #endregion


                            fileAll.Add(tempFile);
                            //file.Files = fileAll.ToArray();
                        }
                        listHepExercise.Add(hepExerciseMobile);
                    }

                    foreach (var hepGroup in dtos.HepGroups)
                    {
                        HepGroupMobileResponse dataHepGroupMobile = new HepGroupMobileResponse();
                        dataHepGroupMobile.GroupId = hepGroup.Id;
                        dataHepGroupMobile.Name = (hepGroup.Name != null) ? hepGroup.Name : string.Empty;
                        dataHepGroupMobile.ExerciseOrder = hepGroup.ExerciseOrder;

                        foreach (var hepexercise in hepGroup.HepDetails.Where(x => x.GroupId != null))
                        {
                            HepExerciseMobileResponse hepExerciseMobile = new HepExerciseMobileResponse();
                            hepExerciseMobile.Id = hepexercise.Id;
                            hepExerciseMobile.ExerciseId = hepexercise.ExerciseId;
                            hepExerciseMobile.ExerciseName = hepexercise.Exercise.Name;
                            //hepExerciseMobile.Comments = hepexercise.Exercise.Comments;
                            hepExerciseMobile.Comments = hepexercise.Comments;
                            hepExerciseMobile.GroupId = hepexercise.GroupId;
                            hepExerciseMobile.HepId = hepexercise.HepId;
                            hepExerciseMobile.Sets = hepexercise.Sets;
                            hepExerciseMobile.Reps = hepexercise.Reps;
                            hepExerciseMobile.Time = hepexercise.Time;
                            hepExerciseMobile.TimeUnit = hepexercise.TimeUnit;
                            hepExerciseMobile.Weight = hepexercise.Weight;
                            hepExerciseMobile.WeightUnit = hepexercise.WeightUnit;
                            hepExerciseMobile.Holds = hepexercise.Holds;
                            hepExerciseMobile.HoldsUnit = hepexercise.HoldsUnit;
                            hepExerciseMobile.ExerciseOrder = hepexercise.ExerciseOrder;
                            hepExerciseMobile.Frequencies = hepexercise.Frequencies;
                            hepExerciseMobile.Medias = hepexercise.Exercise.Medias;

                            foreach (var file in hepexercise.Exercise.Medias)
                            {
                                file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(hepexercise.Exercise.Id));
                                //file.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(file.Name);

                                List<FileRequest> fileAll = new List<FileRequest>();
                                FileRequest tempFile = new FileRequest();

                                //if (StorageService.UseAmazonStorage == true)
                                //    tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(file.Name, GetExerciseMediaS3Path(file.ExerciseId.Value));
                                //else
                                //    tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + file.Id;

                                tempFile.S3BucketName = file.Name;
                                tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                                #region

                                if (file.ThumbImageName != null)
                                {
                                    file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(hepexercise.Exercise.Id));
                                    //file.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);

                                    //if (StorageService.UseAmazonStorage == true)
                                    //    tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(file.ThumbImageName, GetExerciseMediaS3Path(hepexercise.Exercise.Id));
                                    //else
                                    //    tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                                    tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                                }
                                #endregion

                                fileAll.Add(tempFile);
                                //file.Files = fileAll.ToArray();
                            }
                            listHepGroupExercise.Add(hepExerciseMobile);
                            listHepExercise.Add(hepExerciseMobile);
                        }
                        HepGroupMobileResponse newhepGroupMobile = new HepGroupMobileResponse();
                        newhepGroupMobile.GroupId = dataHepGroupMobile.GroupId;
                        newhepGroupMobile.Name = dataHepGroupMobile.Name;
                        newhepGroupMobile.ExerciseOrder = dataHepGroupMobile.ExerciseOrder;
                        //newhepGroupMobile.HepDetails = listHepGroupExercise.ToArray();
                        listHepGroup.Add(newhepGroupMobile);
                        listHepGroupExercise.Clear();
                    }
                }
            }

            HepGetMobileResponse newhepGetMobile = new HepGetMobileResponse();
            newhepGetMobile.HepDetails = (hepDetailMobile != null) ? hepDetailMobile : new HepDetailMobileResponse();
            //newhepGetMobile.HepGroups = newhepGroupMobile;
            newhepGetMobile.HepGroups = (listHepGroup != null || listHepGroup.Count > 0) ? listHepGroup.ToArray() : new HepGroupMobileResponse[0];
            newhepGetMobile.HepExercises = (listHepExercise != null || listHepExercise.Count > 0) ? listHepExercise.ToArray() : new HepExerciseMobileResponse[0];

            return Ok(newhepGetMobile);
        }

        /// <summary>
        /// Method Description : Using for Get Patient Hep by Patient Id. 
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>Json</returns>
        [HttpGet]
        //[Route("api/mobile/v1/PatientHepMasters/{Id}")]
        [Route("api/mobile/v1/HepMasters/{Id}/case")]  //Where Id is PatientId
        public async Task<IHttpActionResult> GetPatientHepMaster(long Id)
        {
            var patientPlanData = new HepPatientsPlan();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                var dbSet = ctx.Set<HepPatientsPlan>();
                patientPlanData = await dbSet.AsNoTracking().Where(x => x.PatientId == Id).FirstOrDefaultAsync();
            }

            object obj = null;
            if (patientPlanData == null)
                return Ok(obj);

            //JavaScriptSerializer hepserialize = new JavaScriptSerializer();   //Using JavaScriptSerializer (Note: enum string not saved)
            //var resp = hepserialize.DeserializeObject(patientPlanData.HepPlan);

            //var resp = JsonConvert.DeserializeObject(patientPlanData.HepPlan);

            if (patientPlanData.HepPlan != null)
            {

                HepGetMobileResponse resp1 = JsonConvert.DeserializeObject<HepGetMobileResponse>(patientPlanData.HepPlan);
                foreach (var file1 in resp1.HepExercises)
                {
                    foreach (var file in file1.Medias)
                    {
                        file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(file1.ExerciseId.Value));

                        List<FileRequest> fileAll = new List<FileRequest>();
                        FileRequest tempFile = new FileRequest();

                        tempFile.S3BucketName = file.Name;
                        tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                        #region

                        if (file.ThumbImageName != null)
                        {
                            file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(file1.ExerciseId.Value));

                            tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                        }
                        #endregion

                        fileAll.Add(tempFile);
                    }
                }

                var serializerSettings = new JsonSerializerSettings();
                serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                var Hepjson = JsonConvert.SerializeObject(resp1, serializerSettings);
                var userHepPlan = Hepjson.ToString();

                var resp = JsonConvert.DeserializeObject(userHepPlan);
                return Ok(resp);
            }
            else
            {
                var resp = JsonConvert.DeserializeObject(patientPlanData.HepPlan);
                return Ok(resp);
            }

            //var resp = JsonConvert.DeserializeObject(patientPlanData.HepPlan);

            //return Ok(resp);
        }
    }
}
