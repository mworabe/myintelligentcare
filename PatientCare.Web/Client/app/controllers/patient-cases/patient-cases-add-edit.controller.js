﻿(function ResourceEditCtrlIIFE() {
    "use strict";
    angular.module("EDZoutstaffingPortalApp")
        .controller("PatientCasesEditController",
            ["$scope", "$stateParams", "patientService", "$state", "$q", "caseInitialData",
                "toaster", "Upload", "Country", "CreatorSearchFunction", "uuid2", "DateTimeHelper", "Modal",
                "CreatorLeavePage", "PermissionWorker", "$rootScope", "ConfigLayout", "StateService",
                "CityService", "DynamicDropDownConfigService", "DropdownConfigsFunction", "$timeout", "SystemFunctions",
                "$filter", "$translate", "$uibModal", "Language", "ReferralPhysicianService", "ClinicService", "patientCaseService", "Resource", "LocationService", "ClinicLocationService",
                "InjuryRegionService", "DiagnosisService", '$uibModalInstance', 'ClinicianService',
                PatientCasesEditControllerFunc]
        );
    function PatientCasesEditControllerFunc($scope, $stateParams, patientService, $state, $q, caseInitialData,
                              toaster, uploader, country, creatorSearchFunction, uuid2, dateTimeHelper, modal,
                              creatorLeavePage, permissionWorker, $rootScope, configLayoutFactory, StateService,
                              CityService, dynamicDropDownConfigService, dropdownConfigsFunction, $timeout, systemFunctionsService,
                              $filter, $translate, $uibModal, languageService, referralPhysicianService, clinicService, patientCaseService, resourceService, locationService, clinicLocationService,
                              injuryRegionService, diagnosisService, $uibModalInstance, ClinicianService) {
        console.info(caseInitialData);
        var initialValues = caseInitialData;
        var self = this;

        self.isPatientLoadedCorrectly = !(initialValues[2] && initialValues[2].errorCode === 404);
        self.patientBasicDetails = caseInitialData[0];

        if (!self.isPatientLoadedCorrectly) {
            toaster.pop("error", "", "We are unable to serve you at this moment, please try again later.");
            return;
        }

        self.hepTabs = [
                {
                    id: "hep",
                    title: "HEP",
                },
                    //, {
                //    id: "manual-treatment",
                //    title: "Manual Treatment"
                //}, 
                {
                    id: "performance-analytics",
                    title: "Performance Analytics"
                }
        ];
        self.selectedHepTab = self.hepTabs[0].id;
        self.isHepTabSelected = function (id) {
            if (id == self.selectedHepTab) return true;
            return false;
        }
        self.setHepSelectedTab = function (id) {
            self.selectedHepTab = id;
        }


        self.resourceConfig = initialValues[1] || {};
        self.resourceTabs = _.unionBy(initialValues[1].tabs, configLayoutFactory.getPatientCaseTab(), "id");

        var tempPatientModel = angular.copy(caseInitialData[0]);
        var tempCaseData = angular.copy(caseInitialData[1]);
        var tempResourceModel = angular.copy(initialValues[2]);
        var modelId = $stateParams.caseId || undefined;
        self.patientId = $stateParams.patientId || undefined;
        self.caseId = modelId;

        self.isNewPatient = (modelId && modelId > 0);
        var counterFilesPromise = 0;
        var audioService = {};
        var salaryCoeff = 2080;
        var params = {};
        var MIN_YEARS = 15;
        var MAX_YEARS = 100;
        var commonConfig = {
            onMediaCaptured: function (stream) {
                audioService.stream = stream;
                if (audioService.mediaCapturedCallback) {
                    audioService.mediaCapturedCallback();
                }

                audioService.disabled = false;
            },
            onMediaStopped: function () {
                if (!audioService.disableStateWaiting) {
                    audioService.disabled = false;
                }
            },
            onMediaCapturingFailed: function (error) {
                if (error.name === 'PermissionDeniedError' && !!navigator.mozGetUserMedia) {
                    intallFirefoxScreenCapturingExtension();
                }
                commonConfig.onMediaStopped();
            }
        };

        //tabs options
        self.other_Condition = isToShowOtherTab;
        self.summary_Condition = showSummaryTab;
        //self.backgroundCheck_Condition = permissionWorker.restrictBackgroundChecks;
        //self.criminalRecords_Condition = permissionWorker.restrictCriminalRecords;
        self.previousEmployments_Condition = permissionWorker.restrictPreviousEmployments;
        self.workingDetails_Click = calculateYearsOfExperience;
        self.resourceHref = $state.href("resource");
        self.leftMenuMaxHeight = (window.innerHeight - 30);


        self.model = {};
        self.model.range = "M";
        self.autocompleteMinLength = 0;
        self.availabilityArray = [];
        self.availableVsDemandData = [];
        self.availableVsDemandHeaders = [];
        self.availableList = [];
        self.projectId = modelId;
        self.model.active = true;
        self.currentTab = $stateParams.currentTab || "patientCaseDetails";
        self.availabilityModelShow = { timeRange: "Day" };
        self.callSubmit = false;
        self.companyName = "";
        self.organizationalChartData = [];
        self.availableVsDemandData = $stateParams.availabilityData || [];
        self.availableVsDemandHeaders = $stateParams.availabilityHeaders || [];
        $scope.resumeFieldList = {};


        //To select default referal Refering Physician On Add Mode
        if (!self.caseId &&
            self.patientBasicDetails &&
            self.patientBasicDetails.referralPhysicianId &&
            self.patientBasicDetails.referralPhysician)
        {
            self.model.referingPhysician = self.patientBasicDetails.referralPhysician;
            self.model.referingPhysicianId = self.patientBasicDetails.referralPhysicianId;
            
        }

        //To select default clinician On Add Mode
        if (!self.caseId &&
            self.patientBasicDetails &&
            self.patientBasicDetails.clinicId &&
            self.patientBasicDetails.clinic) {
            self.model.clinic = self.patientBasicDetails.clinic;
            self.model.clinicId = self.patientBasicDetails.clinicId;
        }

        //To select default clinic location On Add Mode
        if (!self.caseId &&
            self.patientBasicDetails &&
            self.patientBasicDetails.clinicLocationId &&
            self.patientBasicDetails.clinicLocation) {
            self.model.clinicLocation = self.patientBasicDetails.clinicLocation;
            self.model.clinicLocationId = self.patientBasicDetails.clinicLocationId;
        }



        function getValueInAnguAutocomplete(selected) {
            return (selected && selected.originalObject) ? selected.originalObject.id : null;
        }

        self.replaceSpecialChars = function (string) {
            if (!string || !string.length)
                return string;
            var replaceString = string.replace(/\n/g, "<br>");
            return replaceString;
        }
        self.getPageTitle = function getPageTitle() {
            if (!modelId)
                return "";

            if (caseInitialData[0] && caseInitialData[0].name && caseInitialData[1] && caseInitialData[1].injuryRegion) {
                var caseNo = caseInitialData[1].caseNo || 0;
                return caseInitialData[0].name + " / " + caseNo + " - " + caseInitialData[1].injuryRegion.name;
            } else {
                return "New Case";
            }
        }

        self.getPatientFullName = function getPatientFullName() {
            var title = "";
            if (self.patientBasicDetails) {
                if (self.patientBasicDetails.prefix)
                    title = self.patientBasicDetails.prefix;
                title += " " + self.patientBasicDetails.name;
            }
            return title;
        }



        function showSummaryTab() {
            return !(self.model.id && self.model.id > 0)
        }

        function isToShowOtherTab() {
            if (self.otherContainer) {
                if (self.otherContainer.container1 && self.otherContainer.container1.length > 0)
                    return false;
                else if (self.otherContainer.container2 && self.otherContainer.container2.length > 0)
                    return false;
                else if (self.otherContainer.container3 && self.otherContainer.container3.length > 0)
                    return false;
                else if (self.otherContainer.container4 && self.otherContainer.container4.length > 0)
                    return false;
                else if (self.otherContainer.container5 && self.otherContainer.container5.length > 0)
                    return false;
                else if (self.otherContainer.container6 && self.otherContainer.container6.length > 0)
                    return false;
                else
                    return true;
            }
            return true;
        }

        self.getPatientCaseList = patientCaseService.getPatientCases;
        self.getPatientCaseTableOptions = patientCaseService.getTableOption;
        self.getPatientCaseReloadCommand = patientCaseService.reloadCommand;
        self._onPatientCasesResponseTransform = function _onPatientCasesResponseTransform(data) {
            data.id = self.model.id;
        }
        self.onAddNewCaseButtonClick = function onAddNewCaseButtonClick() {
            $state.go('patientCaseEdit', { patientId: self.model.id });
        }

        self.birthDate_max = moment().subtract(MIN_YEARS, "year");
        self.birthDate_min = moment().subtract(MAX_YEARS, "year");

        self.dateFormat = function (date, formate) {
            if (!date)
                return "";
            if (!formate)
                formate = "MM/DD/YYYY"
            return moment(date).format(formate);
        }

        self.resumeSmallDateFormat = function (date) {
            if (!date)
                return "";
            return moment(date).format("MM/YYYY");
        }

        self.resumeCertificateDateFormat = function (date) {
            if (!date)
                return "";
            return moment(date).format("YYYY");
        }

        self.getPreviousEmploymentCompany_and_TitleValue = function getPreviousEmploymentCompany_and_TitleValue(companyName, titleOfPosition) {
            return (companyName) ? ((titleOfPosition) ? companyName + " / " + titleOfPosition : companyName) : "";
        }

        function getMonthsDiffrence(d1, d2) {
            var months;
            var d1Y = d1.getFullYear();
            var d2Y = d2.getFullYear();
            var d1M = d1.getMonth();
            var d2M = d2.getMonth();

            return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
        }

        self.getResourceFullName = function () {
            if (self.model) {
                if ((self.model.lastName && self.model.lastName.length > 0) && (self.model.firstName && self.model.firstName.length > 0)) {
                    return self.model.lastName + " " + self.model.firstName + " / ";
                }
                return "";
            }
        }

        self.getResourceFullNameForSummary = function () {
            if (self.model) {
                if ((self.model.lastName && self.model.lastName.length > 0) && (self.model.firstName && self.model.firstName.length > 0)) {
                    return self.model.lastName + ", " + self.model.firstName + " / ";
                }
                return "";
            }
        }

        function calculateYearsOfExperience() {
            if (self.model && self.model.startDate) {
                var totalMonths = getMonthsDiffrence(new Date(self.model.startDate), new Date());
                var years = parseInt(totalMonths / 12);
                var months = ((totalMonths / 12) - years) * 12;
                self.model.yearsEmployed = parseInt(years);
                self.model.monthsEmployed = parseInt(months);
            }
        }

        self.getEmployedFromString = function () {
            if (self.model && self.model.startDate) {
                var totalMonths = getMonthsDiffrence(new Date(self.model.startDate), new Date());
                var years = parseInt(totalMonths / 12);
                var months = ((totalMonths / 12) - years) * 12;
                return "Years Employed : " + ((years > 0) ? "<strong>" + parseInt(years) + "</strong> Years " : "") + ((months > 0) ? "<strong>" + parseInt(months) + "</strong> Months " : "");
            }
        }

        function resourceCompanySelected(selected) {
            self.model.resourceCompanyId = getValueInAnguAutocomplete(selected);
            self.model.resourceCompany = (selected && selected.originalObject) ? selected.originalObject.name : null;
        }

        function businessUnitSelected(selected) {
            self.model.businessUnitId = getValueInAnguAutocomplete(selected);
        }

        function IndustrySelected(selected) {
            self.model.industryId = getValueInAnguAutocomplete(selected);
        }

        function resourceJobTitleSelected(selected) {
            self.model.jobTitleId = getValueInAnguAutocomplete(selected);
        }

        function resourceCountrySelected(selected) {
            self.model.countryId = getValueInAnguAutocomplete(selected);
        }

        function resourceStateSelected(selected) {
            self.model.stateId = getValueInAnguAutocomplete(selected);
        }
        self.zipCode_Enum = [];
        function resourceCitySelected(selected) {
            self.model.cityId = getValueInAnguAutocomplete(selected);
            self.zipCode_Enum = (selected && selected.originalObject && selected.originalObject.postalCodeLists) ? selected.originalObject.postalCodeLists : [];

            if (selected && selected.title) { // manually Call city changed
                if (self.zipCode_Enum && self.zipCode_Enum.length > 0) {
                    //self.model.zipCode = self.zipCode_Enum[0].zipCode;
                    $timeout(function () {
                        $scope.$broadcast('angucomplete-alt:changeInput', 'zipCode', self.model.zipCode);
                    }, 1000);
                } else
                    self.model.zipCode = null;
            }
        }
        self.clinicLocation_Enum = [];

        function getAutoCompleteValueId(selected) {
            return (selected && selected.originalObject) ? selected.originalObject.id : null;
        }

        function referralPhysicianSelected(selected) {
            self.model.referingPhysicianId = getAutoCompleteValueId(selected);
            self.model.referingPhysician = (selected && selected.originalObject) ? selected.originalObject : undefined;
        }
        function assignedClinicianSelected(selected) {
            self.model.clinicianId = getAutoCompleteValueId(selected);
        }
        function clinicSelected(selected) {
            self.model.clinicId = getAutoCompleteValueId(selected);
            creatorSearchFunction(self, "clinicLocation_Search", clinicLocationService, "", { clinicId: self.model.clinicId });
        }
        function clinicLocationSelected(selected) {
            self.model.clinicLocationId = getAutoCompleteValueId(selected);
        }
        function injuryRegionSelected(selected) {
            self.model.injuryRegionId = getAutoCompleteValueId(selected);
        }
        function primaryDiagnosisSelected(selected) {
            self.model.primayDiagnosisId = getAutoCompleteValueId(selected);
        }
        function secondryDiagnosisSelected(selected) {
            self.model.secondaryDiagnosisId = getAutoCompleteValueId(selected);
        }
        function assignedPhysicianSelected(selected) {
            self.model.assignedPhysicianId = getAutoCompleteValueId(selected);
        }


        self.referingPhysician_Selected = referralPhysicianSelected;
        self.assignedClinician_Selected = assignedClinicianSelected;
        self.clinic_Selected = clinicSelected;
        self.clinicLocation_Selected = clinicLocationSelected;
        self.injuryRegion_Selected = injuryRegionSelected;
        self.primayDiagnosis_Selected = primaryDiagnosisSelected;
        self.secondaryDiagnosis_Selected = secondryDiagnosisSelected;
        self.assignedPhysician_Selected = assignedPhysicianSelected;


        self.assignedPhysician_top_button_click = function () {
          
            //templateUrl: "Client/app/controllers/patient-cases/avd-dialog/avd-dialog.html",
            var model = $uibModal.open({
                templateUrl: "Client/app/controllers/patient-cases/avd-dialog/avd-dialog.html",
                controller: "AvdDialogCntrl",
                size: "large-modal",
                keyboard: false,

            });


        };


        


        self.onCancel_Click = function () {
            $scope.ignoreRedirect = true;
            $state.go($state.current, { currentTab: self.currentTab, id: modelId }, { reload: true });
            $timeout(function () { toaster.pop("success", "", "Resource Restored Successful."); }, 2000);
        };

        self.onCancelButtonClick = function onCancelButtonClick() {
            systemFunctionsService.historyBackButtonClick($rootScope, $state, $scope);
            $scope.ignoreRedirect = true;
            window.history.back();
        }

        $scope.hideErrorTab = function () {
            $scope.formErrorList = null;
        }

        /**
            Form Validation and Header Error Mechanism Functions.
        */

        self.getAllResourceFields = function () {
            var allFieldsArray = [];
            if (configLayoutFactory.getPatientCaseFields().length > 0) {
                allFieldsArray = allFieldsArray.concat(configLayoutFactory.getPatientCaseFields())
            }
            return allFieldsArray;
        }

        function getTabName(tabId) {
            var tabList = configLayoutFactory.getPatientCaseTab();
            var tabName = "";
            _.forEach(tabList, function (tab) {
                if (tab.container == tabId) {
                    tabName = tab.name;
                }
            });
            return tabName;
        }

        function getFieldByName(fieldId, tabName) {

            var resourceAllFields = self.getAllResourceFields();
            if (!tabName) {
                for (var fieldIndex = 0; fieldIndex < resourceAllFields.length; fieldIndex++) {
                    var field = resourceAllFields[fieldIndex];

                    if (field.id == fieldId || field.fieldName == fieldId) {
                        return field;
                    }
                }
            } else {

                for (var fieldIndex = 0; fieldIndex < resourceAllFields.length; fieldIndex++) {
                    var field = resourceAllFields[fieldIndex];

                    if ((field.id == fieldId || field.fieldName == fieldId) && field.container == tabName) {
                        return field;
                    }
                }
            }
        }

        function prepareResourceFormErrorList(form) {
            var errorList = {};
            _.forIn(form.$error, function (arr) {
                var arry = arr;
                _.forEach(arry, function (item) {

                    var currentTabName = null;


                    // Patch for Skill, Education like tabs where field name is different than model name and added _uniq value.
                    if (item.$options.tabValidationName) {
                        currentTabName = item.$options.tabValidationName;
                    }
                    var fieldName = item.$name;
                    if (fieldName === "rating_date") {
                        errorList["Performance Reviews"] = ["Rating Date"];
                        return;
                    }

                    if (fieldName.indexOf("_") > 0 && fieldName != "rating_date")
                        fieldName = fieldName.substring(0, fieldName.indexOf("_"));



                    var actualField = getFieldByName(fieldName, currentTabName);




                    if (!actualField && item.$options && item.$options.tabValidationName) {
                        //"travel Details"
                        currentTabName = item.$options.tabValidationName;
                    }
                    if (!actualField || !actualField.container) {
                        //return;
                    }
                    if (actualField) {
                        if (item.$options.tabValidationName) {
                            currentTabName = getTabName(item.$options.tabValidationName);
                        } else {
                            currentTabName = getTabName(actualField.container);
                        }
                    }

                    if (actualField) {
                        var fieldArray = errorList[currentTabName];
                        var isFieldExist = _.find(fieldArray, function (field) { return (field === actualField.name || field === actualField.label) });

                        if (actualField.container !== "") {
                            if (!fieldArray) {
                                fieldArray = [];
                                errorList[currentTabName] = fieldArray;
                            }
                            if (!isFieldExist)
                                fieldArray.push((actualField.name !== '' && actualField.name !== "undefined" && actualField.name) ? actualField.name : actualField.label);
                        }
                    } else {
                        //_.find(self.otherContainer, )
                        var tab = _.find(configLayoutFactory.getPatientCaseTab(), { id: currentTabName });

                        if (tab && tab.name) {
                            var field = getFieldByName(fieldName);
                            if (!errorList[tab.name] && typeof errorList[tab.name] !== 'Array')
                                errorList[tab.name] = [];
                            if (field && field.name)
                                errorList[tab.name].push(field.name);
                        } else {

                        }
                    }
                });
            });
            $scope.formErrorList = errorList;
        }

        /**
            Form Validation and Header Error Mechanism Functions. ---------------------
        */

        self.saveHep = function (isToSaveAsDraft) {
            $scope.$broadcast('MIC:On-case-save-Update-HEP', isToSaveAsDraft);
        }
        self.emailHEP = function (isToSaveAsDraft) {
            $scope.$broadcast('MIC:ON-HEP-SEND-MAIL', isToSaveAsDraft);
        }
        $rootScope.$on('MIC:ON-IS-HEP-AVAILABLE', function (event, args) {
            self.isHepAvailable = args;
        });

        



        self.onSave_Click = function (form) {

            var newId = 0;
            var tempYearsEmployed;

            systemFunctionsService.scrollToTopZero();

            function successful() {
                $scope.ignoreRedirect = true;
                self.callSubmit = false;

                if (modelId && modelId > 0) {
                    toaster.pop("success", "", "Patient case updated successful");
                    modelId = self.model.id;
                    $state.go($state.current, {
                        currentTab: self.currentTab,
                        id: modelId,
                    }, { reload: true });
                } else {
                    toaster.pop("success", "", "Patient case Added successful.");
                    //self.onAddPatientCaseCancel();
                    //$state.go("communication.chat.chat-box", { sessionObject: item }, { reload: "communication.chat.chat-box" });
                    //$state.go("patientsEdit", { id: $stateParams.patientId }, { reload: "patientsEdit" });
                    $uibModalInstance.close({ patientId: $stateParams.patientId });
                }


            }

            function openTabInErrorModel() {
                _.forIn(form.$error, function (arr) {
                    _.forEach(arr, function (item) {
                        self[item.$options.accordionOpenFieldName] = true;
                    });
                });
            }


            $scope.formErrorList = null;


            if (form.$invalid) {
                openTabInErrorModel();
                prepareResourceFormErrorList(form);
                return;
            }

            //self.loadCorrect = true;
            self.callSubmit = true;
            if (counterFilesPromise !== 0) {
                toaster.pop("warning", "", "waiting file upload, please try again later.");
                return;
            }

            preSendChangeModel();
            var defer = $q.defer();

            if (modelId && modelId > 0) {
                self.loadCorrect = patientCaseService.updatePatientCase({ id: modelId, patientId: $stateParams.patientId }, self.model, function (responce) {
                    self.model = responce;
                    defer.resolve(responce);
                }, function (error) { reject(); });
            } else {

                self.loadCorrect = patientCaseService.savePatientCase({ patientId: $stateParams.patientId }, self.model, function (responce) {
                    self.model = responce;
                    defer.resolve(responce);
                }, function (error) { reject(); });
            }

            defer.promise.then(function (responce) {
                newId = responce.id;
                var deferUploadCsv = $q.defer();
                var deferUploadAvatar = $q.defer();

                if (self.uploadFileAvatar) {
                    patientService.uploadAvatar(uploader, self.uploadFileAvatar, responce.id, function () {
                        deferUploadAvatar.resolve();
                    }, reject);
                } else deferUploadAvatar.resolve();

                if (self.uploadFileCV) {
                    patientService.uploadCV(uploader, self.uploadFileCV, responce.id, function () {
                        deferUploadCsv.resolve();
                    }, reject);
                } else deferUploadCsv.resolve();

                $q.all([]).then(function () { successful(); });

            });
        }; // Save Func ****

        self.canUpdateOrInsertButtonDisabled = canUpdateOrInsertButtonDisabled;
        self.runRecord = function () {
            captureAudio(commonConfig);

            audioService.mediaCapturedCallback = function () {
                audioService.recordRTC = RecordRTC(audioService.stream, {
                    type: 'audio',
                    bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                    sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                    leftChannel: params.leftChannel || false,
                    disableLogs: params.disableLogs || false,
                    recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
                });

                audioService.recordingEndedCallback = function () {
                    if (!$scope.$$phase)
                        $scope.$apply(function () {
                            self.audioSrc = audioService.recordRTC.toURL();
                        });
                };

                audioService.recordRTC.startRecording();
            };
        };

        function setFormScope(form) {
            $scope.__form = form;
        }

        function getForm() {
            return $scope.__form.caseEditForm;
        }

        function hideFieldForNewPateint() {
            var hiddenFieldsForNewPateint = [{ id: "status" }, { id: "patientId" }, { id: "overview" }];
            _.forEach(self.generalPoropertyList, function (formField) {
                var field = _.find(hiddenFieldsForNewPateint, { id: formField.id });
                if (field && field.id) {
                    formField.visible = false;
                }
            });
        }

        function hideFieldForNewPateintV2(containers) {
            if (!containers)
                return;
            _.forEach([{ id: "caseStatus" }], function (fieldToHide) {
                var field = undefined;
                field = _.find(containers.container1, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container2, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container3, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container4, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container5, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container6, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }

            });
        }

        function parceLayoutModel() {
            self.tabs = _.unionBy(initialValues[1].tabs, configLayoutFactory.getPatientCaseTab(), "id");
            initialValues[1].containers = initialValues[1].containers || configLayoutFactory.getDefaultPatientCaseContainer();
            self.patientDetails = _.find(initialValues[1].containers, { id: "patientCaseDetails" });

            //configLayoutFactory.addRequiredCustomField(initialValues[1].containers, self.model.customFieldValues, self.otherContainer);
            self.generalPoropertyList = configLayoutFactory.getPatientCaseFields();


            // hide some fields if New Patient
            //hideFieldForNewPateint();
            if (!modelId)
                hideFieldForNewPateintV2(self.patientDetails);

            self.__ResourceEditCtrl = "PatientCasesEditController";
        }

        self.isTabVisible = function isTabVisible(tabId) {
            var tab = _.find(self.tabs, { id: tabId });
            return (tab && tab.visible)
        }

        self.getTabDisplayName = function (tabId) {
            if (self.tabs && self.tabs.length > 0) {
                var tab = _.find(self.tabs, { id: tabId });
                if (tab && tab.name)
                    return $filter('translate')(tab.name);
            }
        }

        if (modelId) {
            modelId = parseInt(modelId);
            self.model = caseInitialData[1];
            self.titleText = self.model.name;
            if (!tempResourceModel)
                tempResourceModel = angular.copy(self.model);

        } else {
            self.model.customFieldValues = initialValues[0];
            self.titleText = "New Resource";
        }
        parceLayoutModel();

        self.prevBirthDate = angular.copy(self.model.birthDate);

        $scope.$watch(function () { return self.model.birthDate }, function () {
            self.prevBirthDate = self.model.birthDate;
            if (self.model.birthDate) {
                self.model.age = moment().diff(self.model.birthDate, 'years');
            }
        })



        self.restrictPersonalInfo = permissionWorker.restrictPersonalInfo;
        self.restrictSalary = permissionWorker.restrictSalary;
        self.restrictBackgroundChecks = permissionWorker.restrictBackgroundChecks;
        self.restrictCriminalRecords = permissionWorker.restrictCriminalRecords;
        self.restrictPreviousEmployments = permissionWorker.restrictPreviousEmployments;


        creatorSearchFunction(self, "injuryRegion_Search", injuryRegionService);
        creatorSearchFunction(self, "primayDiagnosis_Search", diagnosisService);
        creatorSearchFunction(self, "secondaryDiagnosis_Search", diagnosisService);
        creatorSearchFunction(self, "assignedPhysician_Search", ClinicianService);
        creatorSearchFunction(self, "referingPhysician_Search", referralPhysicianService);
        creatorSearchFunction(self, "clinic_Search", clinicService);
        creatorSearchFunction(self, "clinicLocation_Search", clinicLocationService);


        self.relatedCause_Enum = [{
            value: "AutoAccidentAtFault",
            title: "Auto Accident At Fault",
        }, {
            value: "AutoAccidentNoFault",
            title: "Auto Accident No Fault",
        }, {
            value: "Fall",
            title: "Fall",
        }, {
            value: "Abuse",
            title: "Abuse",
        }, {
            value: "AnotherPartyResponsible",
            title: "Another Party Responsible",
        }, {
            value: "EmploymentInjury",
            title: "Employment Injury",
        }, {
            value: "SportsInjurySurgery",
            title: "Sports Injury Surgery",
        }, {
            value: "OtherAccident",
            title: "Other Accident",
        }, {
            value: "NoneOfTheAbove",
            title: "None Of The Above"
        }];


        // For Dynamic Dropdowns
        //dropdownConfigsFunction(self, "country_Search", country, "country");

        generateList();
        listModelInitialization();

        self.timeZoneInfo_Enum = initialValues[3];

        self.exportResource = function () {
            window.open("api/Resources/export?id=" + modelId, "_blank", "");
        };

        ////////////////////////////////////////////////////////////////////

        function canUpdateOrInsertButtonDisabled() {
            if (modelId > 0) {
                return permissionWorker.canPatientUpdate();
            }

            return permissionWorker.canPatientInsert();
        }

        function intallFirefoxScreenCapturingExtension() {
            InstallTrigger.install({
                'Foo': {
                    // URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                    URL: 'https://addons.mozilla.org/firefox/downloads/file/355418/enable_screen_capturing_in_firefox-1.0.006-fx.xpi?src=cb-dl-hotness',
                    toString: function () {
                        return this.URL;
                    }
                }
            });
        }

        function stopStream() {
            if (audioService.stream && audioService.stream.stop) {
                audioService.stream.getTracks()[0].stop();
                audioService.stream = null;
            }
        }

        function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
            navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
        }

        function captureAudio(config) {
            captureUserMedia({ audio: true }, function (audioStream) {

                config.onMediaCaptured(audioStream);

                audioStream.onended = function () {
                    config.onMediaStopped();
                };
            }, function (error) {
                config.onMediaCapturingFailed(error);
            });
        }

        function employmentTypeIsSalary() {
            return self.model.employmentType === "Salary";
        }

        function getBlobFileToRecord() {
            var blob = recordRTC.getBlob();
            blob.name = "record.wav";
        }

        function reject() {
            self.callSubmit = false;
        }

        function generateList() {
            self.prefix_Enum = ["Ms", "Miss", "Mrs", "Mr", "Dr", "Atty", "Prof", "Hon", ];
            self.gender_Enum = ["Male", "Female"];
            self.caseStatus_Enum = [
                {
                    title: "New",
                    value: "New"
                },
                {
                    title: "In Progress",
                    value: "Inprogress"
                },
                {
                    title: "Discharged",
                    value: "Discharged"
                },
                {
                    title: "Inactive",
                    value: "Inactive"
                },
                {
                    title: "Pending",
                    value: "Pending"
                }
            ]
            self.prefferedContactMethod_Enum = [{ title: "Email", value: "Email" }, { title: "Phone", value: "Phone" }];
            self.relationshipToPatient_Enum = self.relationshipToContact_Enum = [
                {
                    title: "Daughter",
                    value: "Daughter"
                }, {
                    title: "Friend",
                    value: "Friend"
                }, {
                    title: "Parent",
                    value: "Parent"
                }, {
                    title: "Partner",
                    value: "Partner"
                }, {
                    title: "Son",
                    value: "Son"
                }, {
                    title: "Sibling",
                    value: "Sibling"
                }, {
                    title: "Spouse",
                    value: "Spouse"
                }, {
                    title: "Legal Gardian",
                    value: "LegalGardian"
                }
            ];
            self.maritialStatus_Enum = ["Single", "Married"];
            self.status_Enum = ["Active", "Inactive"];

            self.range = ["L", "M", "H"];
            self.suffix_Enum = ["Sr.", "Jr.", "M.D.", "Ph.D."];
            self.visaOrWorkPermitEnum = ["Visa", "Permit"];
            self.salaryPer = ["Hour", "Week", "Month", "Year"];
            self.resourceType_Enum = ["Internal", "External", "Consultant"];
            self.employmentType_Enum = [
            {
                title: "Exempt",
                value: "Salary"
            },
            {
                title: "Non-Exempt",
                value: "Hourly"
            }];
            self.yesNoOtherEnum = ["NA", "Yes", "No", "Other"];
            self.passFailENUM = ["Pass", "Fail"];
            self.ethnicity_Enum = [
                "Native American or Alaska Native",
                "Caucasian",
                "Hispanic",
                "Black or African American",
                "Asian",
                "Native Hawaiian or Pacific Islander",
                "Other"
            ];
            self.availabilityTimeRange = dateTimeHelper.dateTypeEnum();
        }

        function setResourceIdInList(item) {
            if (modelId)
                item.resourceId = modelId;
        }

        self.getNA = function (fieldName, propertyName) {
            if (propertyName) {
                return (self.model[fieldName] && self.model[fieldName][propertyName]) || "-";
            }
            return self.model[fieldName] || "-";
        }

        self.getSummaryFieldValue = function (field) {
            var value = "";
            if (typeof self.model[field.id] === "object" && self.model[field.id] && self.model[field.id].hasOwnProperty("name")) {
                value = self.model[field.id].name;
            } else {
                value = self.model[field.id];
            }
            if (!value) {
                return value;
            }

            var actualField = _.find(self.generalPoropertyList, { id: field.id });
            if (actualField && actualField.type) {
                switch (actualField.type) {
                    case "DATE_TIME":
                        value = self.dateFormat(new Date(value));
                        break;
                    case "PHONE":
                        if (value && value.length > 10)
                            value = value.substring(0, 10);

                        if (value && value.length > 6)
                            value = ["(", value.slice(0, 3), ") ", value.slice(3, 6), "-", value.slice(6)].join('');
                        break;
                    case "CHECKBOX":
                        if (field.id == "active") {
                            if (value == true || value == "true") {
                                value = "Yes";
                            } else {
                                value = "No";
                            }
                        } else {
                            if (value == true || value == "true") {
                                value = "True";
                            } else {
                                value = "False";
                            }
                        }
                        break;
                    default:
                        value = value;
                        break;
                }
            }
            return value;
        }

        self.getSliderClass = function (fieldName) {
            var value = self.model[fieldName];
            if (!value)
                return "0%";
            return (value * 10) + "%";
        }


        function preSendChangeModel() {
            if (self.model.zipCode === '' && self.model.zipCode.length == 0) {
                self.model.zipCode = null;
            }
            calculateYearsOfExperience();
        }


        function base64ToBlob(base64Data, contentType) {
            contentType = contentType || '';
            var sliceSize = 1024;
            var byteCharacters = atob(base64Data.split(',')[1]);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            return new Blob(byteArrays, { type: contentType });
        }

        self.setFormScope = setFormScope;
        creatorLeavePage($scope, getForm);

        $rootScope.$on("custom-file-upload-start", function () {
            counterFilesPromise++;
        });
        $rootScope.$on("custom-file-upload-end", function () {
            counterFilesPromise--;
        });

        function listModelInitialization() {

            self.patientCasesViewModel = {
                fields: [
                {
                    fieldName: "caseId",
                    label: "Case ID",
                    dataType: "date-time",
                    inputClass: "from-date",
                    classContainer: "col-md-3"
                },
                {
                    fieldName: "maxAvailabilityPercentage",
                    label: "Availability %",
                    dataType: "number",
                    inputClass: "availability",
                    classContainer: "col-md-3"
                },
                {
                    fieldName: "created",
                    label: "Post Date",
                    dataType: "date-time",
                    inputClass: "created",
                    classContainer: "col-md-3"
                },
                {
                    fieldName: "changedBy",
                    label: "Changed By",
                    dataType: "text",
                    inputClass: "changed-by",
                    classContainer: "col-md-3"
                }
                ]
            };
        }


        self.onAddPatientCaseCancel = function () {
            self.onCancelButtonClick();
            $scope.close();
        }
        $scope.close = function () {
            $uibModalInstance.dismiss('');
        }

    }

})();
