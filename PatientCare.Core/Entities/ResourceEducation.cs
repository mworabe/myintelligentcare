﻿using System;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Education.
    /// </summary>
    public class ResourceEducation: Entity
    {
        public long ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        public string Subject { get; set; }
        //public string TypeOfDegreeOrCertObtained { get; set; }
        public string NameOfSchool { get; set; }
        public DateTime? DateObtained { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public long? ResourceDesignationId { get; set; }
        public virtual DropDownConfig ResourceDesignation { get; set; }
    }
}
