﻿angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("patients", {
                url: "/patients",
                templateUrl: "Client/app/controllers/patients/patients-list.html",
                controller: "PatientCtrl as PatientCtrl",
                params: {},
                data: {
                    security: {
                        authenticated: true
                    }
                }
            }).state("patients.patients-add", {
                url: "/add",
                params: {},
                data: {
                    security: {
                        authenticated: true
                    }
                },
                onEnter: ['$state', '$uibModal',
                    function ($state, $uibModal) {
                        $uibModal.open({
                            templateUrl: "Client/app/controllers/patients/patients-add-model.html",
                            controller: "PatientEditCtrl as PatientEditCtrl",
                            size: "large-modal",
                            resolve: {
                                customFieldsResource: [
                                    "$q", "$stateParams", "CustomField", "RoleSetup", "patientService", 'userTimeZoneFactory', "SystemFunctions",
                                    function loadResourceInitialData($q, $stateParams, customFieldService, roleSetupService, patientService, userTimeZoneFactory, systemFunctions) {
                                        function loadPatient() {
                                            var defer = $q.defer();
                                            if (!$stateParams.id) {
                                                defer.resolve();
                                            } else {
                                                defer.resolve();
                                            }
                                            return defer.promise;
                                        }

                                        function loadTimeZone() {
                                            var defer = $q.defer();
                                            userTimeZoneFactory.getTimeZone(defer);
                                            return defer.promise;
                                        }

                                        function loadCustomFields() {
                                            var defer = $q.defer();
                                            defer.resolve();
                                            /* if ($stateParams.id) {
                                                 defer.resolve();
                                             } else {
                                                 customFieldService.customFieldValuesByResource().$promise.then(function (response) {
                                                     defer.resolve(response);
                                                 });
                                             }
                                             */
                                            return defer.promise;
                                        }

                                        function loadConfigLayout() {
                                            var defer = $q.defer();
                                            userTimeZoneFactory.getResourceDefaultConfigs(defer);
                                            return defer.promise;
                                        }
                                        return $q.all([loadCustomFields(), {}, loadPatient(), loadTimeZone()])
                                            .then(function (resolutions) {
                                                systemFunctions.scrollToTopZero();
                                                return resolutions;
                                            });
                                    }
                                ]
                            }
                        }).result.then(function (result) {

                        }, function () {

                        }
                        )
                    }
                ],
                /*onExit: ["$state", function ($state) {
                    $state.go("patients", {}, { reload: true });
                }],*/
            }).state("patientsEdit", {
                url: "/patient/edit/:id/:currentTab",
                templateUrl: "Client/app/controllers/patients/patients-edit.html",
                controller: "PatientEditCtrl as PatientEditCtrl",
                //currentTab: "patientDetails" 
                params: {},
                resolve: {
                    $uibModalInstance: function () { return {} },
                    customFieldsResource: [
                        "$q", "$stateParams", "CustomField", "RoleSetup", "patientService", 'userTimeZoneFactory', "SystemFunctions",
                        function loadResourceInitialData($q, $stateParams, customFieldService, roleSetupService, patientService, userTimeZoneFactory, systemFunctions) {
                            function loadPatient() {
                                var defer = $q.defer();
                                if (!$stateParams.id) {
                                    defer.resolve();
                                } else {
                                    patientService.get({ id: $stateParams.id }).$promise.then(function (response) {

                                        if (response.patientAddress1) {
                                            response.add1 = response.patientAddress1.address;
                                            response.city1Id = response.patientAddress1.cityId;
                                            response.state1Id = response.patientAddress1.stateId;
                                            response.country1Id = response.patientAddress1.countryId;
                                            response.city1 = response.patientAddress1.city;
                                            response.state1 = response.patientAddress1.state;
                                            response.country1 = response.patientAddress1.country;
                                            response.zip1 = response.patientAddress1.zipCode;
                                            response.address1IsPrimary = response.patientAddress1.addressIsPrimary;
                                        }

                                        if (response.patientAddress2) {
                                            response.add2 = response.patientAddress2.address;
                                            response.city2Id = response.patientAddress2.cityId;
                                            response.state2Id = response.patientAddress2.stateId;
                                            response.country2Id = response.patientAddress2.countryId;
                                            response.city2 = response.patientAddress2.city;
                                            response.state2 = response.patientAddress2.state;
                                            response.country2 = response.patientAddress2.country;
                                            response.zip2 = response.patientAddress2.zipCode;
                                            response.address2IsPrimary = response.patientAddress2.addressIsPrimary;
                                        }

                                        defer.resolve(response);
                                    }, function (reason) {
                                        defer.resolve({
                                            errorCode: reason.status
                                        });
                                    });
                                }
                                return defer.promise;

                            }

                            function loadTimeZone() {
                                var defer = $q.defer();
                                userTimeZoneFactory.getTimeZone(defer);
                                return defer.promise;
                            }

                            function loadCustomFields() {
                                var defer = $q.defer();
                                defer.resolve();
                                //if ($stateParams.id) {
                                //    defer.resolve();
                                //} else {
                                //    customFieldService.customFieldValuesByResource().$promise.then(function (response) {
                                //        defer.resolve(response);
                                //    });
                                //}
                                return defer.promise;
                            }

                            function loadConfigLayout() {
                                var defer = $q.defer();
                                userTimeZoneFactory.getResourceDefaultConfigs(defer);
                                return defer.promise;
                            }
                            return $q.all([loadCustomFields(), {}, loadPatient(), loadTimeZone()])
                                .then(function (resolutions) {
                                    systemFunctions.scrollToTopZero();
                                    return resolutions;
                                });
                        }
                    ]
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });