﻿using FluentValidation;

namespace PatientCare.Web.Api.Models.EntryTypes
{
    public class EntryTypeRequestValidator : AbstractValidator<EntryTypeRequest>
    {
        public EntryTypeRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);
        }
    }
}