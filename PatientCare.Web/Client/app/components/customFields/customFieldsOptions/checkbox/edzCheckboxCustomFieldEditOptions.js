﻿"use strict";

(function edzCheckboxCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzCheckboxCustomFieldEditOptions", [
            edzCheckboxCustomFieldOptions
        ]);

    function edzCheckboxCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/checkbox/edzCheckboxCustomFieldEdit.html",
            restrict: "E",
            bindToController: {
                model: "=model"
            },
            controller: ["$scope", function edzAddCustomFieldCtrl($scope) {
            }],
            controllerAs: "edzCheckboxCustomFieldEditOptionsCtrl"
        };
    }
})();
