﻿namespace PatientCare.Core.Mapping
{
    public interface IMapper<TS,TD>
    {
        TD Map(TS source);
    }
}
