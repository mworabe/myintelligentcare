﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Services
{
    public interface ICustomFieldService
    {
        Task<bool> UpdateAsync(CustomField customField);
        Task<IList<ResourceCustomFieldValue>> CustomFieldValuesForResource();
    }
}
