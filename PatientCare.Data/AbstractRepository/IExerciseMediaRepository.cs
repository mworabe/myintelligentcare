﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IExerciseMediaRepository : IRepository<ExerciseMedia>
    {
    }
}
