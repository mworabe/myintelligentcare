﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("ResourceScore", [
        "$resource", "$state", "$filter", "$translate",
        function ($resource, $state, $filter, $translate) {
            var resource = $resource("api/ResourceScore/:id/:controller",
                null, //parameters default
                {
                    'update': { method: "PUT" },
                    'query': { method: "GET", params: {}, isArray: false },
                    'filter': { method: "POST", params: {}, isArray: false },
                    'advancedFilter': { method: "POST", params: { controller: 'advanced-filter' }, isArray: false },
                });


            resource.getTableOption = {
                hideTitle: true,
                hideActionButtons: true,
                pageName: "",
                showAddNewButton: false,
                searchField: "name",
                cols: function () {
                    return [
                        {
                            field: "name",
                            title: "Name",
                            sortable: "name",
                            image: function (row) {
                                return row.photoFileNameUrl || "Client/assets/images/userIco.png";
                            },
                            show: true,
                            dataTypeView: "href",
                            dataType: "text",
                            buildUrl: function (row) {
                                return $state.href("resourceEdit", { id: row.id });
                            },
                            customViewText: function (row) {
                                return row.name;
                            },
                        },
                        {
                            field: "jobTitle",
                            title: "Job Title",
                            sortable: "jobTitle",
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "department",
                            title: "Department",
                            sortable: "department",
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "division",
                            title: "Division",
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "company",
                            title: "Company",
                            sortable: "company",
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "productivity",
                            title: "Productivity",
                            sortable: "productivity",
                            show: true,
                            dataType: "text",
                            dataTypeView: "updatable"
                        },
                        {
                            field: "score",
                            title: "Rank",
                            sortable: "score",
                            show: true,
                            dataType: "text",
                            dataTypeView: "updatable"
                        }
                    ];
                }

            };

            return resource;
        }
    ]);