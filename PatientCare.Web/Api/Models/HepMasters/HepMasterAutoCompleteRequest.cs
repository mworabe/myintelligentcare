﻿using System;

namespace PatientCare.Web.Api.Models.HepMasters
{
    public class HepMasterAutoCompleteRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public long? OwnerId { get; set; }

        public long? PatientCaseId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

    }
}