﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for City.
    /// </summary>
    public class City : NamedEntity
    {
        public long? StateId { get; set; }

        public string StateCode { get; set; }

        public long? CountryId { get; set; }
    }
}
