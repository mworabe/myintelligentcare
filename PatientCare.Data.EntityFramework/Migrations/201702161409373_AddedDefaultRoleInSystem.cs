using PatientCare.Data.EntityFramework.Identity;

namespace PatientCare.Data.EntityFramework.Migrations
{
    
    using System.Data.Entity.Migrations;

    public partial class AddedDefaultRoleInSystem : DbMigration
    {
        public override void Up()
        {
            Sql(string.Format(
                                @"
                                INSERT INTO dbo.AspNetRoles(Id,Name,Discriminator)
                                VALUES('e8dcb07d-0997-4abc-8a47-11e951b53f26','{0}','ApplicationRole');
                                
                                INSERT INTO dbo.AspNetRoles(Id,Name,Discriminator)
                                VALUES('71dfeffe-24ea-4c86-a421-8d63781ebaab','{1}','ApplicationRole' );                                
                                ",
                                 ApplicationUser.FrontOfficeRole,
                                 ApplicationUser.TherapistRole
                                 ));
        }

        public override void Down()
        {
            Sql(@"
                    DELETE
                    FROM dbo.AspNetRoles
                    WHERE Id in('e8dcb07d-0997-4abc-8a47-11e951b53f26','71dfeffe-24ea-4c86-a421-8d63781ebaab')
                ");
        }
    }
}
