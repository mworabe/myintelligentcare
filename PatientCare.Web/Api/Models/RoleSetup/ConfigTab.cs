﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientCare.Web.Api.Models.RoleSetup
{
    public class ConfigTab
    {
        public string Name { get; set; }
        public string Id { get; set; }

        public bool Visible { get; set; }

    }
}