﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngListData", ["uuid2", "$uibModal", "Modal", "toaster", "$state",
        function (uuid2, $uibModal, modal, toaster, $state) {
            return {
                templateUrl: "Client/app/components/directives/listData/listDataV2.html",
                restrict: "E",
                replace: true,
                scope: {
                    model: "=ngModel",
                    option: "=ngOption",
                    viewMode: "=?ngViewMode",
                    angucomleteStorePrefix: "=?ngAngucomleteStorePrefix",
                    showSecondAdd: "=?showSecondAdd",
                    parentModel: "=?parentModel",
                    savePopupCallback: "=?savePopupCallback",
                    showAVDButton: "=?ngShowAvdButton"
                },
                link: function (scope) {
                    scope.angucomleteStorePrefix = scope.angucomleteStorePrefix || "___storeFieldSelectValue";
                    scope.option = scope.option || {};
                    scope.viewMode = (typeof scope.viewMode === "undefined" || scope.viewMode === null) ? false : scope.viewMode;

                    scope.option.transformModelPrePush = scope.option.transformModelPrePush || angular.noop;
                    scope.addNew = function () {
                        if (!scope.model)
                            scope.model = [];
                        var item = {};
                        scope.option.transformModelPrePush(item);
                        scope.model.push(item);
                    };

                    //if (scope.showAVDButton) {
                    scope.redirectOnAVDScreen = function (item) {

                        console.info(item);
                        scope.ignoreRedirect = true;
                        if (item.jobTitleId)
                            $state.go("rolesAvailabilityVsDemand", { jobTitleId: item.jobTitleId, jobTitleName: item.___storeFieldSelectValuejobTitleId.name });
                        else {
                            $state.go("rolesAvailabilityVsDemand");
                        }
                    }
                    //}
                    scope.addNewFromPopUp = function () {
                        var taskFilter = {
                            fromDate: this.$parent.parentModel.startDate,
                            toDate: this.$parent.parentModel.dueDate
                        };
                        scope.editTaskModal = $uibModal.open({
                            animation: true,
                            backdrop: "static",
                            size: "lg",
                            templateUrl: "Client/app/controllers/rolesAvailabilityVsDemand/rolesAvailabilityVsDemandPopUpV2.html",
                            controller: "RolesAvailabilityVsDemandPopUpCtrl as RolesAvailabilityVsDemandCtrl",
                            resolve: {
                                data: function () {
                                    return {
                                        taskFilter: taskFilter
                                    }
                                }
                            }
                        });
                        scope.editTaskModal.result.then(function (parameters) {

                            if (scope.savePopupCallback)
                                scope.savePopupCallback(parameters);
                        });
                    }
                    scope.dateFormat = function (value, format) {
                        if (!value)
                            return undefined;
                        if (angular.isString(value)) {
                            return moment(value).format(format || "MM/DD/YYYY");
                        }
                        return value.format(format || "MM/DD/YYYY");
                    }

                    scope.delete = function (field, item) {
                        var index = scope.model.indexOf(item);
                        if (index >= 0)
                            scope.model.splice(index, 1);
                    }

                    scope.angucompleteSelect = function (select) {
                        var field = this.$parent.$parent.field;
                        var fieldName = this.$parent.$parent.field.fieldName;
                        var item = this.$parent.$parent.$parent.item;
                        if (select && select.originalObject && select.originalObject.id > 0) {
                            item[fieldName] = select.originalObject.id;
                            item[scope.angucomleteStorePrefix + fieldName] = select.originalObject;
                            if (field.angucompleteSelector) {
                                field.angucompleteSelector(item, select.originalObject);
                            }
                        } else item[fieldName] = null;
                    }

                    /*
                        AnguComplete Override methods for custom changes, Client Needs Combination of Text and Selecteable input.
                        We are Storing string only, not refrence.
                    */
                    scope.angucompleteOnChanged = function (string) {
                        var field = this.$parent.$parent.field;
                        var fieldName = this.$parent.$parent.field.fieldName;
                        var item = this.$parent.$parent.$parent.item;

                        if (string && string.length > 0) {
                            item[fieldName] = string;
                        } else {
                            item[fieldName] = null;
                        }

                    }

                    scope.getAnguTextInitialValue = function (item, field) {
                        return item[field.fieldName];
                    }

                    scope.angucompleteTextSelect = function (selected) {
                        var field = this.$parent.$parent.field;
                        var fieldName = this.$parent.$parent.field.fieldName;
                        var item = this.$parent.$parent.$parent.item;

                        if (selected && selected.originalObject) {
                            item[fieldName] = selected.originalObject.name;
                            if (field.autoPopulateFieldList && field.autoPopulateFieldList.length > 0) {
                                _.forEach(field.autoPopulateFieldList, function (autoPopulateField) {
                                    item[autoPopulateField.fieldName] = selected.originalObject[autoPopulateField.dbName];
                                })
                            }
                        }
                    }

                    scope.deleteItem = function (item, arr) {
                        var languageItemIndex = arr.indexOf(item);
                        arr.splice(languageItemIndex, 1);
                    }
                    scope.generateId = uuid2.newguid;
                    scope.isFunction = angular.isFunction;
                }
            };
        }
    ]);