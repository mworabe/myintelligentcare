﻿using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PatientCare.Web.Api.Models.ResourceWorkAssignment
{
    [DataContract]
    public class ResourceWorkAssignmentResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string AssignmentDescription { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public long ResourceWorkExperienceId { get; set; }

        [DataMember]
        public long? AssignmentIndustryId { get; set; }
        [DataMember]
        public PicklistResponse AssignmentIndustry { get; set; }

        [DataMember]
        public bool? OfNote { get; set; }
        [DataMember]
        public bool? JudicialClerkShip { get; set; }
    }
}