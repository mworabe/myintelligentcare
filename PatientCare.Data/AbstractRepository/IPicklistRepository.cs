﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Data.AbstractRepository
{
    public interface IPicklistRepository<T> : IRepository<T> where T : Entity
    {
    }
}
