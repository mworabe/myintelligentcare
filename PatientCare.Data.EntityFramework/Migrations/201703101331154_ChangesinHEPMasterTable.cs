namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesinHEPMasterTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.hep_detail", "HepMaster_Id", "dbo.hep_master");
            DropIndex("dbo.hep_detail", new[] { "HepMaster_Id" });
            DropColumn("dbo.hep_detail", "HepMaster_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.hep_detail", "HepMaster_Id", c => c.Long());
            CreateIndex("dbo.hep_detail", "HepMaster_Id");
            AddForeignKey("dbo.hep_detail", "HepMaster_Id", "dbo.hep_master", "Id");
        }
    }
}
