﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Patient Goal Type Enum.
    /// </summary>
    public enum PatientGoalTypeEnum
    {
        PersonalGoal = 0,
        WellNessGoal = 1,
        OtherGoal = 2
    }
}
