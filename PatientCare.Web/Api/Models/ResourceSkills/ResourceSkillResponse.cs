﻿using System;
using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.ResourceSkills
{
    [DataContract]
    public class ResourceSkillResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public PicklistResponse Skill { get; set; }

        [DataMember]
        public int YearsOfExpirience { get; set; }

        [DataMember]
        public double Proficiency { get; set; }
        [DataMember]
        public long ResourceId { get; set; }
        [DataMember]
        public DateTime? LastUpdated { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}