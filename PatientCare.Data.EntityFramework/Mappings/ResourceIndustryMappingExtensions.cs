﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceIndustryMappingExtensions
    {
        public static DbModelBuilder MapResourceIndustry(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceIndustry>().ToTable("resource_industries").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceIndustry>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceIndustry>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceIndustry>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<ResourceIndustry>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceIndustry>()
                .Property(p => p.IndustryId)
                .HasColumnName("industry_id")
                .IsOptional();

            modelBuilder.Entity<ResourceIndustry>()
                .HasOptional<DropDownConfig>(p => p.Industry)
                .WithMany()
                .HasForeignKey(p => p.IndustryId)
                .WillCascadeOnDelete(false);

            return modelBuilder;
        }
    }
}
