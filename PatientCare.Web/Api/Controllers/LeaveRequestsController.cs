﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.LeaveRequests;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using PatientCare.Web.Api.Models;
using System.Linq.Expressions;
using System.Linq;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Web.Api.Controllers
{
    [Authorize]
    public class LeaveRequestsController : ApiBaseController
    {
        private readonly ITimeManagementService _timeManagementService;
        private readonly ILeaveRequestRepository _leaveRequestRepository;
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<LeaveRequest> _readOptions = new ReadOptions<LeaveRequest>();
        public LeaveRequestsController
            (
                ITimeManagementService timeManagementService,
                ILeaveRequestRepository leaveRequestRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _timeManagementService = timeManagementService;
            _leaveRequestRepository = leaveRequestRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/LeaveRequests/" + id, UriKind.Relative);
        }

        [HttpPost]
        [SwaggerResponse(201, "Created", typeof(LeaveRequestsResponse))]
        public async Task<IHttpActionResult> SendLeaveRequest([FromBody]LeaveRequestsRequest leaveRequestsRequest)
        {
            if (leaveRequestsRequest.ResourceId != 0)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbset = ctx.Set<Resource>();
                    var resourceNew = dbset.Where(x => x.Id == leaveRequestsRequest.ResourceId).Select(v => new { SupervisorId = v.SupervisorId }).SingleOrDefault();
                    if (resourceNew.SupervisorId == null)
                    {
                        ModelState.AddModelError("Time Off Request", "Logged in user is not associated with supervisor, contact admin to assign supervisor");
                        return BadRequest(ModelState);
                    }

                    #region validation for Already Applied leave request list new request exits or not 
                    var OldLeaveRequestLists = _leaveRequestRepository.List(new FilterQuery<LeaveRequest>().AddFilter(x => x.ResourceId == leaveRequestsRequest.ResourceId && (x.Status != "Rejected"))).ToList();
                    if (OldLeaveRequestLists.Count() > 0)
                    {
                        DateTime? DF = (leaveRequestsRequest.StartDate.Value.Date != null) ? (TimeZone.CurrentTimeZone.ToLocalTime(leaveRequestsRequest.StartDate.Value.Date)) : DateTime.UtcNow;
                        DateTime? DT = (leaveRequestsRequest.EndDate.Value.Date != null) ? (TimeZone.CurrentTimeZone.ToLocalTime(leaveRequestsRequest.EndDate.Value.Date)) : DateTime.UtcNow;

                        foreach (var OldLeaveRequestList in OldLeaveRequestLists)
                        {
                            if ((OldLeaveRequestList.StartDate.Value.Date >= DF && OldLeaveRequestList.StartDate.Value.Date <= DT)
                                || (OldLeaveRequestList.EndDate.Value.Date >= DF && OldLeaveRequestList.EndDate.Value.Date <= DT))
                            {
                                ModelState.AddModelError("Time Off Request", " On this Date Leave Already Applied.");
                                return BadRequest(ModelState);
                            }
                            else if ((OldLeaveRequestList.StartDate.Value.Date <= DF && OldLeaveRequestList.EndDate.Value.Date >= DT))
                            {
                                ModelState.AddModelError("Time Off Request", " On this Date Leave Already Applied.");
                                return BadRequest(ModelState);
                            }
                        }
                    }
                    #endregion

                    leaveRequestsRequest.SupervisorId = resourceNew.SupervisorId;
                    leaveRequestsRequest.Status = "Pending";
                    leaveRequestsRequest.StartDate = TimeZoneInfo.ConvertTimeToUtc(leaveRequestsRequest.StartDate.Value);
                    leaveRequestsRequest.EndDate = TimeZoneInfo.ConvertTimeToUtc(leaveRequestsRequest.EndDate.Value);

                    var leaveRequest = Mapper.Map<LeaveRequestsRequest, LeaveRequest>(leaveRequestsRequest);

                    await _timeManagementService.SendLeaveRequestAsync(leaveRequest);

                    return Created(GetEntityLocation(leaveRequest.Id), Mapper.Map<LeaveRequest, LeaveRequestsResponse>(leaveRequest));
                }
            }
            else
            {
                ModelState.AddModelError("Time Off Request", "Logged in user is not associated with Resource, contact admin to assign resource.");
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [SwaggerResponse(200, "Ok", typeof(LeaveRequestsResponse))]
        public async Task<IHttpActionResult> GetLeaveRequestById(long id)
        {
            var leaveRequest = await _timeManagementService.ReadLeaveRequestByIdAsync(id);
            if (leaveRequest == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<LeaveRequest, LeaveRequestsResponse>(leaveRequest);
            return Ok(viewModel);
        }

        [HttpGet]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<LeaveRequestsAdminApprovalListResponseView>))]
        public ListResponse<LeaveRequestsAdminApprovalListResponseView> GetAllLeaveRequestForResource([FromUri]LeaveRequestsListRequest request)
        {
            var lst = new ListResponse<LeaveRequestsAdminApprovalListResponseView>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                if (request.ResourceId != null)
                {

                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                    IEnumerable<LeaveRequestsAdminApprovalListResponseView> result = new List<LeaveRequestsAdminApprovalListResponseView>();
                    long id = Convert.ToInt64(request.ResourceId);
                    string s = _timeManagementService.GetStringForLeaveRequestList(id, false);
                    result = ctx.Database.SqlQuery<LeaveRequestsAdminApprovalListResponseView>(s).ToList();

                    lst.Data = result.ToArray();

                    lst = new ListResponse<LeaveRequestsAdminApprovalListResponseView>(lst.Data, lst.Data.Count());
                }
            }
            return lst;
        }

        private static Expression<Func<LeaveRequest, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return leaveRequest => leaveRequest.LeaveType.Name.Contains(searchPhrase);
        }

        private static Expression<Func<LeaveRequest, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return leaveRequest => leaveRequest.LeaveType.Name.Contains(searchPhrase);
        }

        [HttpGet]
        [Route("api/LeaveRequest/AdminApproval")]
        public ListResponse<LeaveRequestsAdminApprovalListResponseView> GetAllLeaveRequestForSupervisorApproval([FromUri]LeaveRequestsListRequest request)
        {
            var lst = new ListResponse<LeaveRequestsAdminApprovalListResponseView>();

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                if (request.ResourceId != 0)
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                    string s = _timeManagementService.GetStringForLeaveRequestList(Convert.ToInt32(request.ResourceId), true);

                    var query = ctx.Database.SqlQuery<LeaveRequestsAdminApprovalListResponseView>(s);
                    IEnumerable<LeaveRequestsAdminApprovalListResponseView> result = new List<LeaveRequestsAdminApprovalListResponseView>();

                    result = query.ToList();
                    lst.Data = result.ToArray();

                    lst = new ListResponse<LeaveRequestsAdminApprovalListResponseView>(lst.Data, lst.Data.Count());
                }
            }
            return lst;
        }

        [HttpPut]
        public async Task<IHttpActionResult> ApproveLeaveRequest(long id, string status, string comments = null)
        {
            var leaveRequest = await _timeManagementService.ReadLeaveRequestByIdAsync(id);

            leaveRequest.Status = status;
            leaveRequest.StatusModifiedDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);
            leaveRequest.CommentFromSupervisor = comments;

            #region Minus Weekend and Holidays from Applyed leave request 
            int totaldayApplyLeave = Convert.ToInt32((leaveRequest.EndDate.Value.Date - leaveRequest.StartDate.Value.Date).TotalDays) + 1;
            int countHolidayIntimeoff = 0;

            #region Calculation for minus weekend from Applyed Leave 
            DateTime stdt = new DateTime();
            stdt = leaveRequest.StartDate.Value.Date;

            int isWeekEndCount = 0;
            for (int i = 1; i <= totaldayApplyLeave; i++)
            {
                if (stdt.DayOfWeek == DayOfWeek.Saturday || stdt.DayOfWeek == DayOfWeek.Sunday)
                {
                    isWeekEndCount++;
                }
                stdt = stdt.AddDays(1);
            }
            #endregion

            #region Calculation For Minus Holiday Days from Leave Request dates 
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                var ctx = dbContextScope.DbContexts.Get<IPatientCareDbContext>();
                List<Holiday> allHolidayLists = ctx.Holidays.AsNoTracking().ToList();
                var holidaylists = allHolidayLists.Where(x => x.HolidayDate.Value.Date >= leaveRequest.StartDate.Value.Date && x.HolidayDate.Value.Date <= leaveRequest.EndDate.Value.Date).ToList();
                foreach (var allHolidayList in holidaylists)
                {
                    DateTime _startdate = new DateTime();
                    _startdate = leaveRequest.StartDate.Value.Date;
                    for (int j = 1; j <= totaldayApplyLeave; j++)
                    {
                        if (allHolidayList.HolidayDate.Value.Date == _startdate)
                        {
                            if ((leaveRequest.StartDate.Value.Date.DayOfWeek != DayOfWeek.Saturday && leaveRequest.StartDate.Value.Date.DayOfWeek != DayOfWeek.Sunday) || (leaveRequest.EndDate.Value.Date.DayOfWeek != DayOfWeek.Saturday && leaveRequest.EndDate.Value.Date.DayOfWeek != DayOfWeek.Sunday))
                            {
                                countHolidayIntimeoff++;
                            }
                        }
                        _startdate = _startdate.AddDays(1);
                    }
                }
            }
            #endregion

            leaveRequest.TotalTimeOffDays = totaldayApplyLeave - countHolidayIntimeoff - isWeekEndCount;
            #endregion

            //if (status == "Rejected")
            //{
            //    leaveRequest.Canceled = "Rejected";
            //}

            await _timeManagementService.ApproveLeaveRequestAsync(leaveRequest);
           
            var viewModel = Mapper.Map<LeaveRequest, LeaveRequestsResponse>(leaveRequest);
            return Ok(viewModel);
        }

        [HttpPut]
        public async Task<IHttpActionResult> EditLeaveRequest(long id, [FromBody] LeaveRequestsRequest leaveRequestsRequest)
        {
            if (leaveRequestsRequest.ResourceId != 0 && leaveRequestsRequest.Status == "Pending")
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbset = ctx.Set<Resource>();
                    var resourceNew = dbset.Where(x => x.Id == leaveRequestsRequest.ResourceId).Select(v => new { SupervisorId = v.SupervisorId }).SingleOrDefault();
                    if (resourceNew.SupervisorId == null)
                    {
                        ModelState.AddModelError("Time Off Request", "Logged in user is not associated with supervisor, contact admin to assign supervisor");
                        return BadRequest(ModelState);
                    }

                    leaveRequestsRequest.SupervisorId = resourceNew.SupervisorId;
                    leaveRequestsRequest.Status = "Pending";
                    leaveRequestsRequest.StartDate = TimeZoneInfo.ConvertTimeToUtc(leaveRequestsRequest.StartDate.Value);
                    leaveRequestsRequest.EndDate = TimeZoneInfo.ConvertTimeToUtc(leaveRequestsRequest.EndDate.Value);

                    var leaveRequest = Mapper.Map<LeaveRequestsRequest, LeaveRequest>(leaveRequestsRequest);

                    await _timeManagementService.UpdateLeaveRequestAsync(leaveRequest);

                    return Ok(Mapper.Map<LeaveRequest, LeaveRequestsResponse>(leaveRequest));
                }
            }
            else
            {
                ModelState.AddModelError("Time Off Request", "Logged in user is not associated with Resource, contact admin to assign resource.");
                return BadRequest(ModelState);
            }
        }
    }
}
