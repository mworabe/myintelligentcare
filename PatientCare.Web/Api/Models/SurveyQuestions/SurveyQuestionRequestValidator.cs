﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.SurveyQuestions
{
    public class SurveyQuestionRequestValidator : AbstractValidator<SurveyQuestionRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly ISurveyQuestionRepository _surveyQuestionRepository;
        private readonly IPicklistRepository<SurveyCategory> _categoryRepository;

        public SurveyQuestionRequestValidator(IDataContextScopeFactory dbContextScopeFactory, ISurveyQuestionRepository surveyQuestionRepository, IPicklistRepository<SurveyCategory> categoryRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _surveyQuestionRepository = surveyQuestionRepository;
            _categoryRepository = categoryRepository;

            RuleFor(model => model.Text)
                .NotEmpty()
                .Length(0, 255)
                .Must(s => ValidationHelpers.ItemBeUnique(_dbContextScopeFactory, _surveyQuestionRepository, x => x.Text == s))
                .WithMessage("Item with specified text exists");

            RuleFor(model => model.CategoryId)
                .NotEmpty()
                .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _categoryRepository, l))
                .WithMessage("The category with provided Id doesn't exists");
        }
    }
}