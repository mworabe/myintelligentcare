﻿using PatientCare.Core.Enums;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Clinics
{
    [DataContract]
    public class ClinicResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string ClinicPrefix { get; set; }

        [DataMember]
        public bool? IsDeleted { get; set; }


    }
}