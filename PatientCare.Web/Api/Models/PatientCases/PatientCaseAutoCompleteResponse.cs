﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.ClinicLocations;
using PatientCare.Web.Api.Models.Clinics;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.ReferralPhysicians;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PatientCases
{
    [DataContract]
    public class PatientCaseAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string CaseNo { get; set; }

        [DataMember]
        public CaseStatusEnum? CaseStatus { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }
    }
}