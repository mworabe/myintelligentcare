﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Exercises;
using PatientCare.Web.Api.Models.HepFrequencies;
using PatientCare.Web.Api.Models.HepGroups;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.ApiMobile.Models.Exercises;
using PatientCare.Web.ApiMobile.Models.HepFrequencies;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.HepDetails
{
    [DataContract]
    public class HepDetailsMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ExerciseId { get; set; }

        [DataMember]
        public ExerciseMobileResponse Exercise { get; set; }

        [DataMember]
        public long? GroupId { get; set; }

        [DataMember]
        public HepGroupResponse Group { get; set; }

        [DataMember]
        public long? HepId { get; set; }

        //[DataMember]
        //public PicklistDefaultResponse Hep { get; set; }

        [DataMember]
        public int? Sets { get; set; }

        [DataMember]
        public int? Reps { get; set; }

        [DataMember]
        public int? Time { get; set; }

        [DataMember]
        public TimeEnum? TimeUnit { get; set; }

        [DataMember]
        public decimal? Weight { get; set; }

        [DataMember]
        public string WeightUnit { get; set; }

        [DataMember]
        public int? Holds { get; set; }

        [DataMember]
        public TimeEnum? HoldsUnit { get; set; }
        
        [DataMember]
        public int? ExerciseOrder { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public bool? IsActiveFrequency { get; set; }

        [DataMember]
        public HepFrequencyMobileResponse[] Frequencies { get; set; }

    }
}