﻿using System;
using PatientCare.Core.Enums;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.CustomFields
{
    public class CustomFieldRequestValidator : AbstractValidator<CustomFieldRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly ICustomFieldRepository _customFieldRepository;
        public CustomFieldRequestValidator(IDataContextScopeFactory dbContextScopeFactory, ICustomFieldRepository customFieldRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _customFieldRepository = customFieldRepository;
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(1, 255)
                .Must((project, s) => ValidationHelpers.ItemNameBeUnique(_dbContextScopeFactory, _customFieldRepository, s, project.Id))
                .WithMessage("Custom Field with specified name already exists");


            RuleFor(model => model.Type)
                .NotNull()
                .Must(itemType => Enum.IsDefined(typeof(CustomFieldTypeEnum), itemType));

        }
    }
}