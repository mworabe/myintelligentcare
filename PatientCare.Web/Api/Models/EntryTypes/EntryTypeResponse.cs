﻿using PatientCare.Core.Enums;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.EntryTypes
{
    [DataContract]
    public class EntryTypeResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public EntryPeriodEnum? EntryPeriod { get; set; }

        [DataMember]
        public DayItem Days { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public long? AdvancedCreatePeriod { get; set; }
        
        [DataMember]
        public DateTime? LastEntryDateCreated { get; set; }
        [DataMember]
        public bool? IsTimesheetCreated { get; set; }
    }
}