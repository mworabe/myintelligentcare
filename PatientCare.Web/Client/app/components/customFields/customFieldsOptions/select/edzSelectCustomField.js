"use strict";
(function testCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzSelectCustomField", ["edzBaseCustomField", testCustomField]);

    ///////////////////////////////////////////////////////
    function testCustomField(baseCustomField) {
        function SelectCustomField() {}

        SelectCustomField.prototype = Object.create(baseCustomField);
        SelectCustomField.prototype.optionsTemplate = "<edz-select-custom-field-options></edz-select-custom-field-options>";
        SelectCustomField.prototype.optionsTemplateEdit = "<edz-select-custom-field-edit-options model='model'></edz-select-custom-field-edit-options>";
        return SelectCustomField;
    }
})();