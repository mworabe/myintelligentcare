﻿angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("patientsEdit.patient-case-add", {
                url: "/{patientId}/case",
                params: {},
                data: {
                    security: {
                        authenticated: true
                    }
                },
                onEnter: ['$state', '$uibModal',
                    function ($state, $uibModal) {
                        $uibModal.open({
                            templateUrl: "Client/app/controllers/patient-cases/patient-cases-add.html",
                            controller: "PatientCasesEditController as PatientCasesEditController",
                            size: "large-modal",
                            resolve: {
                                caseInitialData: ["$q", "$stateParams", "patientService", "SystemFunctions", "patientCaseService",'$state',
                                    function loadCaseInitialData($q, $stateParams, patientService, systemFunctions, patientCaseService, $state) {
                                        //NOTE $stateParams.patientId is not resolving due to bug in lib, we are using $stateParams.id as quick fix
                                        var patientId = $stateParams.id;//
                                        function loadPatientBasicData() {
                                            var defer = $q.defer();
                                            
                                            
                                            if (!patientId) {
                                                defer.resolve({});
                                            } else {
                                                patientService.get({ id: patientId}).$promise.then(function (success) {
                                                    defer.resolve(success);
                                                }, function (error) {
                                                    defer.resolve(error);
                                                });
                                            }
                                            return defer.promise;
                                        }
                                        function loadCaseDetails()
                                        {
                                            var defer = $q.defer();
                                            if (!$stateParams.caseId && !$stateParams.patientId) {
                                                defer.resolve({});
                                            } else {
                                                patientCaseService.getCase({ patientId: $stateParams.patientId, id: $stateParams.caseId }).$promise.then(function (success) {
                                                    defer.resolve(success);
                                                }, function (error) {
                                                    defer.resolve(error);
                                                });
                                            }
                                            return defer.promise;
                                        }


                                        return $q.all([loadPatientBasicData(), loadCaseDetails()])
                                            .then(function (resolutions) {
                                                systemFunctions.scrollToTopZero();
                                                return resolutions;
                                            });

                                    }]
                            }

                        }).result.then(function (result) {
                            $state.go("patientsEdit", { id: result.patientId, currentTab: "patientCases" }, { reload: "patientsEdit" });
                        }, function () {
                            $state.go("^");
                        }
                        )
                    }
                ]


            }).state("patientCaseEdit", {
                url: "/patients/{patientId}/case/{caseId}/{currentTab}",
                templateUrl: "Client/app/controllers/patient-cases/patient-cases-edit.html",
                controller: "PatientCasesEditController as PatientCasesEditController",
                //                params: { currentTab: "patientCaseDetails" },
                params: {},
                data: {
                    security: {
                        authenticated: true
                    }
                },
                resolve: {
                    $uibModalInstance: function () { return {} },
                    caseInitialData: ["$q", "$stateParams", "patientService", "SystemFunctions", "patientCaseService",
                        function loadCaseInitialData($q, $stateParams, patientService, systemFunctions, patientCaseService) {
                            function loadPatientBasicData() {
                                var defer = $q.defer();
                                if (!$stateParams.patientId) {
                                    defer.resolve({});
                                } else {
                                    patientService.get({ id: $stateParams.patientId }).$promise.then(function (success) {
                                        defer.resolve(success);
                                    }, function (error) {
                                        defer.resolve(error);
                                    });
                                }
                                return defer.promise;
                            }
                            function loadCaseDetails() {
                                var defer = $q.defer();
                                if (!$stateParams.caseId && !$stateParams.patientId) {
                                    defer.resolve({});
                                } else {
                                    patientCaseService.getCase({ patientId: $stateParams.patientId, id: $stateParams.caseId }).$promise.then(function (success) {
                                        defer.resolve(success);
                                    }, function (error) {
                                        defer.resolve(error);
                                    });
                                }
                                return defer.promise;
                            }


                            return $q.all([loadPatientBasicData(), loadCaseDetails()])
                                .then(function (resolutions) {
                                    systemFunctions.scrollToTopZero();
                                    return resolutions;
                                });

                        }]
                }
            });
    });
