﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PatientAppointment
{
    public class PatientAppointmentListRequest : ListRequest
    {
        [DataMember(Name = "from")]
        public DateTime? From { get; set; }

        [DataMember(Name = "to")]
        public DateTime? To { get; set; }

    }
}