﻿using System;
using System.Linq;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.CustomFieldProperties;
using PatientCare.Web.Api.Models.CustomFields;
using EmitMapper;
using EmitMapper.MappingConfiguration;

namespace PatientCare.Web.Services.Mapping
{
    public static class CustomFieldMapperExtensions
    {
        public static EmitMapperMapperProvider CustomFieldRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<CustomField, CustomFieldResponse>
                    (new ExtendedMapConfig<CustomField, CustomFieldResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<CustomFieldResponse, CustomField>
                    (new ExtendedMapConfig<CustomFieldResponse, CustomField>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<CustomField[], CustomFieldResponse[]>());
            
            var customFieldPropertyMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<CustomFieldPropertyRequest, CustomFieldProperty>(
                    new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<CustomFieldRequest, CustomField>
                    (new ExtendedMapConfig<CustomFieldRequest, CustomField>()
                        .ForMember(x => x.CustomFieldProperties, y => y.CustomFieldProperties != null ? y.CustomFieldProperties.Select(i => customFieldPropertyMapper.Map(i)).ToList() : null)));

            return mapperProvider;
        }
    }
}