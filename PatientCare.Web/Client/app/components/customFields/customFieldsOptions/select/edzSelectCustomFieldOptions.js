"use strict";

(function edzSelectCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzSelectCustomFieldOptions", [
            "edzCustomFieldEditHelper",
            "CUSTOM_FIELD",
            edzSelectCustomFieldOptions
        ]);

    function edzSelectCustomFieldOptions(edzCustomFieldEditHelper, CUSTOM_FIELD) {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/select/edzSelectCustomField.html",
            restrict: "E",
            scope: false,
            controller: ["$scope", function ($scope) {
                var option = $scope.option;

                $scope.validation = {};
                $scope.validation.require = $scope.require;
                var data = _.filter(option, function (item) {
                    return item.name === CUSTOM_FIELD.PROPERTY_NAME.SELECT.SELECT;
                });
                $scope.list = _.map(data, "value");
                function loadData() {

                }
                $scope.on_Select = function ($item) {
                    $scope.value = $item;
                }

                function activate() {
                    loadData();
                }

                activate();
            }]
        };
    }
})();
