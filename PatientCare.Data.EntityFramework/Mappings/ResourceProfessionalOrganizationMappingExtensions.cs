﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceProfessionalOrganizationMappingExtensions
    {
        public static DbModelBuilder MapResourceProfessionalOrganization(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceProfessionalOrganization>().ToTable("resource_professional_organizations").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceProfessionalOrganization>()
                .Property(p => p.Organization)
                .HasColumnName("organization")
                .IsOptional();

            modelBuilder.Entity<ResourceProfessionalOrganization>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceProfessionalOrganization>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceProfessionalOrganization>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<ResourceProfessionalOrganization>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ResourceProfessionalOrganization>()
                .Property(p => p.CommitteeMembershipId)
                .HasColumnName("committee_membership_id")
                .IsOptional();

            modelBuilder.Entity<ResourceProfessionalOrganization>()
                .HasOptional<DropDownConfig>(p => p.CommitteeMembership)
                .WithMany()
                .HasForeignKey(p => p.CommitteeMembershipId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceProfessionalOrganization>()
                .Property(p => p.CommitteeTitleId)
                .HasColumnName("committee_title_id")
                .IsOptional();

            modelBuilder.Entity<ResourceProfessionalOrganization>()
                .HasOptional<DropDownConfig>(p => p.CommitteeTitle)
                .WithMany()
                .HasForeignKey(p => p.CommitteeTitleId)
                .WillCascadeOnDelete(false);

            return modelBuilder;
        }
    }
}
