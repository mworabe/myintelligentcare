﻿using PatientCare.Web.ApiMobile.Models.ChatMembers;
using System;

namespace PatientCare.Web.ApiMobile.Models.ChatSessions
{
    public class ChatKeyRequest
    {
        public long Id { get; set; }

        public string UserId { get; set; }

        public string PrivateKey { get; set; }

    }
}