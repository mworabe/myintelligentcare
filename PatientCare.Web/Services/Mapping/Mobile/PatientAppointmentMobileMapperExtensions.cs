﻿using System;
using EmitMapper;
using PatientCare.Services.Mapping;
using PatientCare.Core.Entities;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.ApiMobile.Models.PatientAppointment;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class PatientAppointmentMobileMapperExtensions
    {
        public static EmitMapperMapperProvider PatientAppointmentMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment, PatientAppointmentMobileResponse>
                    (new ExtendedMapConfig<PatientAppointment, PatientAppointmentMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment[], PatientAppointmentMobileResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment, PatientAppointmentMobileListResponse>
                    (new ExtendedMapConfig<PatientAppointment, PatientAppointmentMobileListResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment[], PatientAppointmentMobileListResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment, PicklistResponse>
                    (new ExtendedMapConfig<PatientAppointment, PicklistResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment, PicklistDefaultResponse>
                    (new ExtendedMapConfig<PatientAppointment, PicklistDefaultResponse>()));

            mapperProvider.RegisterMapper(() =>
              ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointmentMobileRequest, PatientAppointment>(
                  new ExtendedMapConfig<PatientAppointmentMobileRequest, PatientAppointment>()));

            return mapperProvider;
        }
    }
}