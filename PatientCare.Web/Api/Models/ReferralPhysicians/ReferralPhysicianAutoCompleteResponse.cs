﻿using PatientCare.Core.Enums;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ReferralPhysicians
{
    [DataContract]
    public class ReferralPhysicianAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Speciality { get; set; }

        [DataMember]
        public MedicalDesignationEnum? MedicalDesignation { get; set; }

        [DataMember]
        public string MedicalDesignationOther { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string ExtensionNumber { get; set; }
        
        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string PostalCode { get; set; }
    }
}