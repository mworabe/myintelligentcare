﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IInjuryRegionRepository : IRepository<InjuryRegion>
    {
    }
}
