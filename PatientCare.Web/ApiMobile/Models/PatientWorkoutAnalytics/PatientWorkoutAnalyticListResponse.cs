﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.PatientWorkoutAnalytics
{
    [DataContract]
    public class PatientWorkoutAnalyticListResponse
    {
        [DataMember]
        public decimal? TotalTime { get; set; }

        [DataMember]
        public DateTime? Dates { get; set; }

        [DataMember]
        public decimal? PainRating { get; set; }

        [DataMember]
        public decimal? AvgRating { get; set; }
       
    }
}