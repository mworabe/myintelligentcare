namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ColumnDataTypeChangesInResource : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.resources", "job_title_id", "dbo.job_titles");
            DropForeignKey("dbo.resources", "resource_company_id", "dbo.resource_companies");
            DropIndex("dbo.resources", new[] { "job_title_id" });
            DropIndex("dbo.resources", new[] { "resource_company_id" });
            AlterColumn("dbo.resources", "employee_id", c => c.String(maxLength: 255));
            AlterColumn("dbo.resources", "job_title_id", c => c.Long());
            AlterColumn("dbo.resources", "resource_company_id", c => c.Long());
            AlterColumn("dbo.resources", "range", c => c.Int());
            CreateIndex("dbo.resources", "job_title_id");
            CreateIndex("dbo.resources", "resource_company_id");
            AddForeignKey("dbo.resources", "job_title_id", "dbo.job_titles", "Id");
            AddForeignKey("dbo.resources", "resource_company_id", "dbo.resource_companies", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.resources", "resource_company_id", "dbo.resource_companies");
            DropForeignKey("dbo.resources", "job_title_id", "dbo.job_titles");
            DropIndex("dbo.resources", new[] { "resource_company_id" });
            DropIndex("dbo.resources", new[] { "job_title_id" });
            AlterColumn("dbo.resources", "range", c => c.Int(nullable: false));
            AlterColumn("dbo.resources", "resource_company_id", c => c.Long(nullable: false));
            AlterColumn("dbo.resources", "job_title_id", c => c.Long(nullable: false));
            AlterColumn("dbo.resources", "employee_id", c => c.String(nullable: false, maxLength: 255));
            CreateIndex("dbo.resources", "resource_company_id");
            CreateIndex("dbo.resources", "job_title_id");
            AddForeignKey("dbo.resources", "resource_company_id", "dbo.resource_companies", "Id", cascadeDelete: true);
            AddForeignKey("dbo.resources", "job_title_id", "dbo.job_titles", "Id", cascadeDelete: true);
        }
    }
}
