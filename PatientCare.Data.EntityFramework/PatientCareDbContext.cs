using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using PatientCare.Core.Entities;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Data.EntityFramework.Mappings;
using PatientCare.Data.EntityFramework.Migrations;
using Microsoft.AspNet.Identity.EntityFramework;


namespace PatientCare.Data.EntityFramework
{
    public class PatientCareDbContext : IdentityDbContext<ApplicationUser>, IPatientCareDbContext
    {
        public PatientCareDbContext()
            : base("name=PatientCareDbConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<PatientCareDbContext, Configuration>());
            Database.Log = s => Debug.Write(s);
        }

        public static PatientCareDbContext Create()
        {
            return new PatientCareDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo")
                .MapPicklistInfo<Language>("languages", "ix_languages_name_unique")
                .MapPicklistInfo<Skill>("skills", "ix_skills_name_unique")
                .MapPicklistInfo<ResourceCompany>("resource_companies", "ix_resource_companies_name_unique")
                .MapPicklistInfo<SurveyCategory>("survey_categories", "ix_survey_categories_name_unique")
                .MapSurveyQuestion()
                .MapResource()
                .MapResourceSpokenLanguage()
                .MapResourceCriminalRecord()
                .MapResourceEducation()
                .MapResourcePracticeArea()
                .MapResourceTravel()
                .MapResourceTeam()
                .MapResourceBehaviouralDetail()
                .MapResourceRecognition()
                .MapResourceIndustry()
                .MapResourcePreviousEmployment()
                .MapResourceSurveyResult()
                .MapPicklistInfo<Certificate>("certificates", "ix_certificates_name_unique")
                .MapResourceCertificate()
                .MapResourceSkill()
                .MapResourceProfessionalOrganization()
                .MapResourceWorkExperience()
                .MapResourceWorkAssignment()
                .MapPicklistInfo<Division>("divisions", "ix_divisions_name_unique")
                .MapPicklistInfo<Department>("departments", "ix_departments_name_unique")
                .MapJobTitle()
                //.MapPicklistInfo<Country>("countries", "ix_countries_name_unique")
                .MapExercise_ExcerciseActivity()
                .MapExercise_injury()
                .MapCountry("ix_countries_name_unique")
                .MapCity()
                .MapState()
                .MapHoliday()
                .MapDropDownConfig()
                .MapLeaveType()
                .MapEntryType()
                .MapLeaveRequest()
                .MapResourceScoreFilter()
                .MapCustomField()
                .MapResourceCustomFieldValue()
                .MapApplicationRole()
                .MapCustomFieldProperty()
                .MapRoleConfigureLayout()
                .MapDashboardSetup()
                .MapSentEmail()
                .MapUserLoginHistory()
                .MapTrialPeriod()
                .MapUiConfiguration()
                .MapPostalCode()
                .MapResourceInsurance()
                .MapResourceTimeoffBenefit()
                .MapPatient()
                .MapPatientAddress()
                .MapPatientParentGaurdian()
                .MapClinic()
                .MapClinicLocation()
                .MapReferralPhysician()
                .MapInjuryRegion()
                .MapDiagnosis()
                .MapPatientCase()
                .MapAppointmentType()
                .MapPatientAppointment()
                .MapExercisePosition()
                .MapExerciseActivity()
                .MapExercise()
                .MapExerciseMedia()
                .MapMasterTemplate()
                .MapMasterTemplateExercise()
                .MapHepGroup()
                .MapHepMaster()
                .MapHepDetail()
                .MapHepFrequency()
                .MapPatientWorkoutAnalytics()
                .MapExerciseComment()
                .MapExerciseRating()
                .MapAppFunctionalRating()
                .MapPatientMobileProfile()
                .MapPatientGoal()
                .MapChatSession()
                .MapChatMember()
                .MapChatKey()
                .MapHepPatientsPlan();

            /***PLEASE READ THIS***/
            /* 
             * While creating migration for new Entity<TId> table, please, 
             * add <defaultValueSql: "GETUTCDATE()"> to fields <created> and <modified> 
             */
            /***PLEASE READ THIS***/
            ;

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    switch (number)
                    {
                        case 547:
                            throw new Exception("This operation failed because another data entry uses this entry.");
                        case 2601:
                            throw new Exception("One of the properties is marked as Unique index and there is already an entry with that value.");
                        default:
                            throw;
                    }
                }
                throw;
            }
        }

        public DbSet<JobTitle> Positions { get; set; }

        public DbSet<Language> Languages { get; set; }

        public DbSet<Skill> Skills { get; set; }

        public DbSet<ResourceCompany> ResourceCompanies { get; set; }

        public DbSet<SurveyCategory> SurveyCategories { get; set; }

        public DbSet<SurveyQuestion> SurveyQuestions { get; set; }

        public DbSet<Resource> Resources { get; set; }

        public DbSet<ResourceSpokenLanguage> ResourceSpokenLanguages { get; set; }

        public DbSet<ResourceCriminalRecord> ResourceCriminalRecords { get; set; }

        public DbSet<ResourceEducation> ResourceEducations { get; set; }

        public DbSet<ResourcePracticeArea> ResourcePracticeAreas { get; set; }

        public DbSet<ResourceTravel> ResourceTravels { get; set; }

        public DbSet<ResourceTeam> ResourceTeams { get; set; }

        public DbSet<ResourcePreviousEmployment> ResourcePreviousEmployments { get; set; }

        public DbSet<ResourceBehaviouralDetail> ResourceBehaviouralDetails { get; set; }

        public DbSet<ResourceRecognition> ResourceRecognitions { get; set; }

        public DbSet<ResourceIndustry> Industry { get; set; }

        public DbSet<ResourceSurveyResult> ResourceSurveyResults { get; set; }

        public DbSet<Certificate> Certificates { get; set; }

        public DbSet<ResourceCertificate> ResourceCertificates { get; set; }

        public DbSet<ResourceSkill> ResourceSkills { get; set; }

        public DbSet<Division> Divisions { get; set; }

        public DbSet<Department> Departments { get; set; }

        public DbSet<ResourceScoreFilter> ResourceScoreFilters { get; set; }

        public DbSet<CustomField> CustomFields { get; set; }

        public DbSet<CustomFieldProperty> CustomFieldProperties { get; set; }

        public DbSet<ResourceCustomFieldValue> ResourceCustomFieldValues { get; set; }

        public DbSet<RoleConfigureLayout> RoleConfigureLayouts { get; set; }

        public DbSet<ResourceWorkAssignment> ResourceWorkAssignments { get; set; }

        public DbSet<ResourceWorkExperience> ResourceWorkExperiences { get; set; }

        public DbSet<ResourceProfessionalOrganization> ResourceProfessionalOrganizations { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<State> States { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<PostalCode> PostalCodes { get; set; }

        public DbSet<Holiday> Holidays { get; set; }

        public DbSet<LeaveType> LeaveTypes { get; set; }

        public DbSet<EntryType> EntryTypes { get; set; }

        public DbSet<DropDownConfig> DropDownConfigs { get; set; }

        public DbSet<LeaveRequest> LeaveRequests { get; set; }

        public DbSet<UserLoginHistory> UserLoginHistorys { get; set; }

        public DbSet<DashboardSetup> DashboardSetups { get; set; }

        public DbSet<TrialPeriod> TrialPeriods { get; set; }

        public DbSet<UiConfiguration> UiConfigurations { get; set; }

        public DbSet<ResourceInsurance> ResourceInsurances { get; set; }

        public DbSet<ResourceTimeoffBenefit> ResourceTimeoffBenefits { get; set; }

        public DbSet<Patient> Patients { get; set; }

        public DbSet<PatientAddress> PatientAddresss { get; set; }

        public DbSet<PatientParentGuardian> PatientParentGuardians { get; set; }

        public DbSet<Clinic> Clinics { get; set; }

        public DbSet<ClinicLocation> ClinicLocations { get; set; }

        public DbSet<ReferralPhysician> ReferralPhysicians { get; set; }

        public DbSet<InjuryRegion> InjuryRegions { get; set; }

        public DbSet<Diagnosis> Diagnosiss { get; set; }

        public DbSet<PatientCase> PatientCases { get; set; }

        public DbSet<AppointmentType> AppointmentTypes { get; set; }

        public DbSet<PatientAppointment> PatientAppointments { get; set; }

        public DbSet<ExercisePosition> ExercisePositions { get; set; }

        public DbSet<ExerciseActivity> ExerciseActivities { get; set; }

        public DbSet<Exercise> Exercises { get; set; }

        public DbSet<Exercise_injury> injury { get; set; }

        public DbSet<Exercise_ExerciseActivity> exerciseActivity { get; set; }

        public DbSet<ExerciseMedia> ExerciseMedias { get; set; }

        public DbSet<MasterTemplate> MasterTemplates { get; set; }

        public DbSet<MasterTemplateExercise> MasterTemplateExercises { get; set; }

        public DbSet<HepGroup> HepGroups { get; set; }

        public DbSet<HepMaster> HepMasters { get; set; }

        public DbSet<HepDetail> HepDetails { get; set; }

        public DbSet<HepFrequency> HepFrequencys { get; set; }

        public DbSet<PatientWorkoutAnalytic> PatientWorkoutAnalytics { get; set; }

        public DbSet<ExerciseComment> ExerciseComments { get; set; }

        public DbSet<ExerciseRating> ExerciseRatings { get; set; }

        public DbSet<PatientMobileProfile> PatientMobileProfiles { get; set; }

        public DbSet<AppFunctionalRating> AppFunctionalRatings { get; set; }

        public DbSet<PatientGoal> PatientGoals { get; set; }

        public DbSet<ChatSession> ChatSessions { get; set; }

        public DbSet<ChatMember> ChatMembers { get; set; }

        public DbSet<ChatKey> ChatKeys { get; set; }

        public DbSet<HepPatientsPlan> HepPatientsPlans { get; set; }
    }
}