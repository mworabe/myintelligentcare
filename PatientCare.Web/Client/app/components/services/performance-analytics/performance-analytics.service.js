﻿"use strict";
var EOPApp = angular.module("EDZoutstaffingPortalApp")
EOPApp.service("PerformanceAnylyticService", ["$resource",  function ($resource) {
    
    var resource = $resource("api/PerformanceAnalytics/:id/:controller", null, {
        
        "getData": { method: "GET", params: {}, isArray: true },
    });
    
    return resource;
}]);
