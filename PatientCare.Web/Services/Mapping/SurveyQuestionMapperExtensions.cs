﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.SurveyQuestions;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class SurveyQuestionMapperExtensions
    {
        public static EmitMapperMapperProvider SurveyQuestionRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");
            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<SurveyQuestion, SurveyQuestionResponse>(new ExtendedMapConfig<SurveyQuestion, SurveyQuestionResponse>()));

            mapperProvider.RegisterMapper(() => 
                ObjectMapperManager.DefaultInstance.GetMapper<SurveyQuestion[], SurveyQuestionResponse[]>());

            mapperProvider.RegisterMapper(() => 
                ObjectMapperManager.DefaultInstance.GetMapper<SurveyQuestionRequest, SurveyQuestion>(new ExtendedMapConfig<SurveyQuestionRequest, SurveyQuestion>()));

            mapperProvider.RegisterMapper(() =>
                    ObjectMapperManager.DefaultInstance.GetMapper<JobTitle[], PicklistResponse[]>());
            return mapperProvider;
        }
    }
}