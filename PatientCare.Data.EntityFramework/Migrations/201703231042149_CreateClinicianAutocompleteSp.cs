namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateClinicianAutocompleteSp : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
           "dbo.sp_clinician_autocomplete",
           p => new
           {
               Operation = p.Int(defaultValueSql: null),
               DataCount = p.Int(defaultValueSql: null),
               Search = p.String(defaultValueSql: null)
           },
           body:
               @"if @Operation = 1
	            begin
		            select Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email from resources 
		            where Id in (
		            select Distinct  users.ResourceId from AspNetUsers users
		            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
		            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist')
	            end
	            else if @Operation = 2
	            begin
		            select TOP (@DataCount) Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email from resources 
		            where Id in (
		            select Distinct  users.ResourceId from AspNetUsers users
		            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
		            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist')
		            and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
	            end"
         );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_clinician_autocomplete");
        }
    }
}
