﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("AssignmentEditController",
   ["$scope", "$state", "$stateParams", "ConfigLayout", "assignmentResource", "DropdownConfigsFunction", "DynamicDropDownConfigService",
       "AssignmentService", "toaster", "CreatorSearchFunction", "Resource", AssignmentEditControllerFunc]);

function AssignmentEditControllerFunc($scope, $state, $stateParams, configLayoutFactory, assignmentResource, dropdownConfigsFunction, dynamicDropDownConfigService,
          assignmentService, toaster, creatorSearchFunction, resource) {
    var self = this;
    self.currentTab = "basicInfo";
    self.modelId = $stateParams.id || 0;
    self.resourceId = $stateParams.resourceId;
    self.model = {};
    self.pageTitle = "New Assignment";
    self.autocompleteMinLength = 0;
    self.model = assignmentResource[0] || {};

    if (self.modelId) {
        self.pageTitle = self.model.searchAssignment = self.model.name;
        self.model.resourceAssign = self.model.resources[0].resource;
    }

    function getSelectedIdInAnguAutocomplete(selected) {
        return (selected && selected.originalObject) ? selected.originalObject.id : null;
    }

    //creatorSearchFunction(self, "resourceAssign_Search", resource);
    dropdownConfigsFunction(self, "searchAssignment_Search", dynamicDropDownConfigService, "assignmentConfigs");


    if (self.resourceId > 0) {
        if (self.model && self.model.resources) {
            self.model.resources[0].resourceId = self.resourceId;
        } else {
            self.model.resources = [];
            self.model.resources.push({ resourceId: self.resourceId });
        }
        self.model.assignMe = false;
    } else {
        self.model.assignMe = true;
    }

    /*
    self.resourceAssign_Selected = function (selectedValue) {

        if (selectedValue && selectedValue.originalObject) {
            if (self.model && self.model.resources) {
                self.model.resources[0].resourceId = selectedValue.originalObject.id;
            } else {
                self.model.resources = [];
                self.model.resources.push({ resourceId: selectedValue.originalObject.id });
            }
        }
    }
    */

    self.searchAssignment_Selected = function (selected) {
        self.model.assignmentId = getSelectedIdInAnguAutocomplete(selected);
        // setting assignment Name 
        self.model.name = (selected && selected.originalObject) ? selected.originalObject.name : null;
        self.assignmentId = (selected && selected.originalObject) ? selected.originalObject.name : null;
    }

    function parceLayoutModel() {
        var assignmentFields = configLayoutFactory.getAssignmentFields();
        self.tabs = configLayoutFactory.getAssignmentTab();
        var assignmentContainers = {};
        assignmentContainers.containers = configLayoutFactory.getDefaultAssignmentContainer();
        self.basicInfoContainer = _.find(assignmentContainers.containers, { id: "basicInfo" });
        self.generalPoropertyList = configLayoutFactory.getAssignmentFields();
        self.__AssignementEditCtrl = "AssignmentEditController";
    }


    self.onCancelButtonClick = function () {
        if (self.resourceId > 0)
            $state.go("resourceEdit", { id: self.resourceId, currentTab: "availability" });
        else
            $state.go("assignment");
    }

    self.onFormSubmit = function (form) {
        self.loadingData = true;
        if (form.$invalid) {
            toaster.pop("error", "", "Assignment is Invalid.");
            self.loadingData = false;
            return false;
        }
        function serverSuccessCall(response) {
            if (self.modelId && self.modelId > 0) {
                toaster.pop("success", "", "Assignment updated successfully.");
            } else {
                toaster.pop("success", "", "Assignment added successfully.");
            }
            self.loadingData = false;
            $state.go("assignmentEdit", { id: response.id });
        }
        function reject(response) {
            if (response && response.message) {
                toaster.pop("error", "", response.message);
            } else {
                toaster.pop("error", "", "Unable to " + (self.modelId > 0) ? "update " : "save " + "Assignement");
            }
            self.loadingData = false;
        }

        if (self.modelId && self.modelId > 0) {
            self.loadingData = assignmentService.update({ id: self.modelId }, self.model, serverSuccessCall, reject);
        } else {
            self.loadingData = assignmentService.save(self.model, serverSuccessCall, reject);
        }
    }

    parceLayoutModel();
}