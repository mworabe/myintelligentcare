﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PatientCare.Web.Api.Models.JobTitles
{
    public class JobTitlePickListResponse : PicklistResponse
    {
        [DataMember]
        public decimal? Rate { get; set; }

        [DataMember]
        public bool? HolidayEligible { get; set; }

        [DataMember]
        public bool? HoursEdit { get; set; }

        [DataMember]
        public EntryPeriodEnum? EntryPeriod { get; set; }
    }
}