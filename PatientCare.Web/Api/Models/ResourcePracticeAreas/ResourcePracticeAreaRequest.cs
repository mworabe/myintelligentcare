﻿namespace PatientCare.Web.Api.Models.ResourcePracticeAreas
{
    public class ResourcePracticeAreaRequest
    {
        public long Id { get; set; }

        public long? ResourceId { get; set; }

        public long? PracticeAreaId { get; set; }
    }
}