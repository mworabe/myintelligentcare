﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PatientCare.Core.Enums;
using PatientCare.Core.Services.ResourceScoreCalculation;

namespace PatientCare.Web.Api.Models.ResourceScore
{
    [DataContract]
    public class ScoreCalculationFilterRequest
    {
        [DataMember]
        public ScoreFilterItem<ResourceSkillFilterRequest>[] Skills { get; set; }

        [DataMember]
        public ScoreFilterItem<ResourceSpokenLanguageFilterRequest>[] SpokenLanguages { get; set; }

        [DataMember]
        public ScoreFilterItem<ResourceSpokenLanguageFilterRequest>[] WrittenLanguages { get; set; }

        [DataMember]
        public ScoreFilterItem<long>[] Certificates { get; set; }

        [DataMember]
        public ScoreFilterItem<string> TimeZoneInfo { get; set; }

        [DataMember]
        public ScoreFilterItem<long?> Country { get; set; }

        [DataMember]
        public ScoreFilterItem<bool> WorksWellInTeam { get; set; }

        [DataMember]
        public ScoreFilterItem<bool> WorksWellAlone { get; set; }

        [DataMember]
        public ScoreFilterItem<double> Rating { get; set; }

        [DataMember]
        public ScoreFilterItem<double> Productivity { get; set; }

        [DataMember]
        public ScoreFilterItem<string> Education { get; set; }

        [DataMember]
        public ScoreFilterItem<string> JobTitle { get; set; }

        [DataMember]
        public ScoreFilterItem<long?> Company { get; set; }

        [DataMember]
        public ScoreFilterItem<long?> Division { get; set; }

        [DataMember]
        public ScoreFilterItem<long?> Department { get; set; }

        [DataMember]
        public ScoreFilterItem<RangeTypeEnum?> RangeType { get; set; }

        [DataMember]
        public ScoreFilterItem<ResourceTypeEnum?> ResourceType { get; set; }

        [DataMember]
        public ScoreFilterItem<decimal?> HourlyRate { get; set; }

        [DataMember]
        public ScoreFilterItem<int?> GradeLevel { get; set; }

        [DataMember]
        public ScoreFilterItem<string> Location { get; set; }

        [DataMember]
        public ScoreFilterItem<AvailabilityVsDemandFilterRequest>[] AvailabilityVsDemand { get; set; }
    }
}