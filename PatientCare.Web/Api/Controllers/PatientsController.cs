﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PatientCare.Core.Entities;
using PatientCare.Core.Enums;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Core.Services.Mailing;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Account;
using PatientCare.Web.Api.Models.Cities;
using PatientCare.Web.Api.Models.PatientAddresses;
using PatientCare.Web.Api.Models.PatientCases;
using PatientCare.Web.Api.Models.PatientParentGuardians;
using PatientCare.Web.Api.Models.Patients;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.PostalCodes;
using PatientCare.Web.ApiMobile.Models.ChatSessions;
using PatientCare.Web.ApiMobile.Models.PatientWorkoutAnalytics;
using PatientCare.Web.Models;
using PatientCare.Web.Services.FileStorage;
using PatientCare.Web.Services.PDFUtils;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Security;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Patient operations.
    /// </summary>
    //[Authorize]
    public sealed class PatientsController : ApiBaseController
    {
        private readonly static string chatUrl = ConfigurationManager.AppSettings["urlChat"];

        /// <summary>
        /// Description : Patient IPatientService.
        /// </summary>
        private readonly IPatientService _patientService;
        /// <summary>
        /// Description : Patient IPatientRepository.
        /// </summary>
        private readonly IPatientRepository _patientRepository;
        /// <summary>
        /// Description : Patient IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : Patient IPatientCaseRepository.
        /// </summary>
        private readonly IPatientCaseRepository _patientCaseRepository;
        /// <summary>
        /// Description : Patient IPatientAppointmentRepository.
        /// </summary>
        private readonly IPatientAppointmentRepository _patientAppointmentRepository;
        /// <summary>
        /// Description : Patient IPatientWorkoutAnalyticRepository.
        /// </summary>
        private readonly IPatientWorkoutAnalyticRepository _patientWorkoutAnalyticRepository;
        /// <summary>
        /// Description : Patient IMobileService.
        /// </summary>
        private readonly IMobileService _mobileService;
        /// <summary>
        /// Description : Patient IEmailSender.
        /// </summary>
        private readonly IEmailSender _emailSender;
        /// <summary>
        /// Description : Patient IMailMessageFactory.
        /// </summary>
        private readonly IMailMessageFactory _messageFactory;
        /// <summary>
        /// Description : Patient IChatService.
        /// </summary>
        private readonly IChatService _chatService;

        ReadOptions<Patient> _readOptions = new ReadOptions<Patient>();

        /// <summary>
        /// Description : Initializes a new instance of the Patient Class.    
        /// </summary>
        /// <param name="patientService">IPatientService</param>
        /// <param name="patientRepository">IPatientRepository</param>
        /// <param name="dataContextScopeFactory">IDataContextScopeFactory</param>
        /// <param name="patientCaseRepository">IPatientCaseRepository</param>
        /// <param name="patientAppointmentRepository">IPatientAppointmentRepository</param>
        /// <param name="patientWorkoutAnalyticRepository">IPatientWorkoutAnalyticRepository</param>
        /// <param name="mobileService">IMobileService</param>
        /// <param name="emailSender">IEmailSender</param>
        /// <param name="messageFactory">IMessageFactory</param>
        /// <param name="chatService">IChatService</param>
        public PatientsController
            (
                IPatientService patientService,
                IPatientRepository patientRepository,
                IDataContextScopeFactory dataContextScopeFactory,
                IPatientCaseRepository patientCaseRepository,
                IPatientAppointmentRepository patientAppointmentRepository,
                IPatientWorkoutAnalyticRepository patientWorkoutAnalyticRepository,
                IMobileService mobileService,
                IEmailSender emailSender,
                IMailMessageFactory messageFactory,
                IChatService chatService
            )
        {
            _patientService = patientService;
            _patientRepository = patientRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
            _patientCaseRepository = patientCaseRepository;
            _patientAppointmentRepository = patientAppointmentRepository;
            _patientWorkoutAnalyticRepository = patientWorkoutAnalyticRepository;
            _mobileService = mobileService;
            _emailSender = emailSender;
            _messageFactory = messageFactory;
            _chatService = chatService;
        }

        /// <summary>
        /// 
        /// </summary>
        public PatientsController()
        {
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/patients/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Patient.
        /// </summary>
        /// <param name="request">Patient List Request Model</param>
        /// <returns>Patient List Response Model</returns>
        [HttpGet]
        //[Route("api/Patients")]
        [Route("api/PatientsList")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<PatientListResponse>))]
        public IHttpActionResult GetAllPatient([FromUri]PatientListRequest request)
        {
            //source.orderBy(???).Skip(PageIndex * PageSize).Take(PageSize));

            var query = PagingExtensions<Patient>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            // here we not take deleted patient in list screen
            query.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);

            var readOptions = new ReadOptions<Patient>().AsReadOnly();
            var page = _patientRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<Patient[], PatientListResponse[]>(page.Entities.ToArray());

            foreach (var item in dtos.ToList())
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                    var casePatient = ctx.PatientCases.AsNoTracking()
                        .Where(x => x.PatientId == item.Id && x.CaseStatus == CaseStatusEnum.Inprogress && (x.IsDeleted == false || x.IsDeleted == null))
                        .FirstOrDefault();

                    if (casePatient != null)
                    {
                        var patientCurrentCase = Mapper.Map<PatientCase, PatientCaseResponse>(casePatient);
                        item.CaseId = patientCurrentCase.Id;
                        item.CaseNo = patientCurrentCase.CaseNo;
                        item.CaseStatus = patientCurrentCase.CaseStatus;
                        item.RelatedCause = patientCurrentCase.RelatedCause;
                        item.InjuryRegionId = patientCurrentCase.InjuryRegionId;
                        item.InjuryRegion = patientCurrentCase.InjuryRegion;

                        #region Set last Appointment Date
                        var lastAppointment = ctx.PatientAppointments.AsNoTracking()
                                             .Where(x => x.CaseId == item.CaseId)
                                             .OrderByDescending(x => x.StartDate)
                                             .FirstOrDefault();

                        item.LastVisitDate = (lastAppointment != null) ? lastAppointment.StartDate : null;
                        #endregion

                        #region set PatientWorkoutAnalytic
                        var lastWorkOut = ctx.PatientWorkoutAnalytics.AsNoTracking()
                                           .Where(x => x.PatientId == item.Id)
                                           .OrderByDescending(x => x.StartDate)
                                           .FirstOrDefault();

                        if (lastWorkOut != null)
                        {
                            item.PatientWorkAnalytic = Mapper.Map<PatientWorkoutAnalytic, PatientWorkoutAnalyticResponse>(lastWorkOut);
                        }
                        #endregion

                        #region Set Hep Item
                        var hepDetail = ctx.HepMasters.Where(x => x.PatientId == item.Id && x.PatientCaseId == casePatient.Id).FirstOrDefault();
                        if (hepDetail != null)
                        {
                            item.HasHep = true;
                            item.HepId = hepDetail.Id;
                        }
                        #endregion
                    }
                }
            }

            var response = new ListResponse<PatientListResponse>(dtos, page.TotalCount);
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Patient.   
        /// </summary>
        /// <param name="patientRequest">Patient Request Model</param>
        /// <returns>Patient Response Model</returns>
        [HttpPost]
        [Route("api/Patients")]
        [SwaggerResponse(201, "Created", typeof(PatientResponse))]
        public async Task<IHttpActionResult> CreatePatient([FromBody]PatientRequest patientRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #region Verify Country State City Zip Code Combination 

            if (patientRequest.PatientAddress1.CountryId != null && patientRequest.PatientAddress1.StateId != null
                       && patientRequest.PatientAddress1.CityId != null && patientRequest.PatientAddress1.ZipCode != null && patientRequest.PatientAddress1.ZipCode != "")
            {
                PostalCodeResponse code = await GetPostalCodeDetail(patientRequest.PatientAddress1.ZipCode);
                if (code == null)
                {
                    ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    return BadRequest(ModelState);
                }
                else
                {
                    if (code.Country != null)
                    {
                        if (patientRequest.PatientAddress1.CountryId != code.Country.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.State != null)
                    {
                        if (patientRequest.PatientAddress1.StateId != code.State.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.City != null)
                    {
                        if (patientRequest.PatientAddress1.CityId != code.City.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }

            if (patientRequest.PatientAddress2.CountryId != null && patientRequest.PatientAddress2.StateId != null
                       && patientRequest.PatientAddress2.CityId != null && patientRequest.PatientAddress2.ZipCode != null && patientRequest.PatientAddress2.ZipCode != "")
            {
                PostalCodeResponse code = await GetPostalCodeDetail(patientRequest.PatientAddress2.ZipCode);
                if (code == null)
                {
                    ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    return BadRequest(ModelState);
                }
                else
                {
                    if (code.Country != null)
                    {
                        if (patientRequest.PatientAddress2.CountryId != code.Country.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.State != null)
                    {
                        if (patientRequest.PatientAddress2.StateId != code.State.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.City != null)
                    {
                        if (patientRequest.PatientAddress2.CityId != code.City.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }

            if (patientRequest.ParentGaurdian != null)
            {
                if (patientRequest.ParentGaurdian.CountryId != null && patientRequest.ParentGaurdian.StateId != null
                       && patientRequest.ParentGaurdian.CityId != null && patientRequest.ParentGaurdian.ZipCode != null && patientRequest.ParentGaurdian.ZipCode != "")
                {
                    PostalCodeResponse code = await GetPostalCodeDetail(patientRequest.ParentGaurdian.ZipCode);
                    if (code == null)
                    {
                        ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                        return BadRequest(ModelState);
                    }
                    else
                    {
                        if (code.Country != null)
                        {
                            if (patientRequest.ParentGaurdian.CountryId != code.Country.Id)
                            {
                                ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                                return BadRequest(ModelState);
                            }
                        }
                        if (code.State != null)
                        {
                            if (patientRequest.ParentGaurdian.StateId != code.State.Id)
                            {
                                ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                                return BadRequest(ModelState);
                            }
                        }
                        if (code.City != null)
                        {
                            if (patientRequest.ParentGaurdian.CityId != code.City.Id)
                            {
                                ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                                return BadRequest(ModelState);
                            }
                        }
                    }
                }
            }

            #endregion

            #region Patient Address Mapping
            List<PatientAddressRequest> addressList = new List<PatientAddressRequest>();
            if (patientRequest.PatientAddress1 != null)
                addressList.Add(patientRequest.PatientAddress1);
            if (patientRequest.PatientAddress2 != null)
                addressList.Add(patientRequest.PatientAddress2);

            patientRequest.Addresses = addressList.ToArray();
            #endregion

            #region Patient Gaurdians Mapping
            List<PatientParentGuardianRequest> parentGaurdiansList = new List<PatientParentGuardianRequest>();
            if (patientRequest.ParentGaurdian != null)
                parentGaurdiansList.Add(patientRequest.ParentGaurdian);

            patientRequest.ParentGaurdians = parentGaurdiansList.ToArray();
            #endregion

            var patient = Mapper.Map<PatientRequest, Patient>(patientRequest);
            patient.Status = PatientStatusEnum.Active;

            await _patientService.CreatePatientAsync(patient);

            await _patientService.UpdatePatientNoAsync(patient.Id);

            var tempPatient = await _patientService.ReadPatientAsync(patient.Id);

            var viewModel = Mapper.Map<Patient, PatientResponse>(tempPatient);
            if (patient.Addresses.Count > 0)
            {
                viewModel.PatientAddress1 = (tempPatient.Addresses.Count >= 1) ? (Mapper.Map<PatientAddress, PatientAddressResponse>(tempPatient.Addresses.ToArray()[0])) : null;
                viewModel.PatientAddress2 = (tempPatient.Addresses.Count >= 2) ? (Mapper.Map<PatientAddress, PatientAddressResponse>(tempPatient.Addresses.ToArray()[1])) : null;
            }

            #region we are added default data in patient mobile profile entity

            PatientMobileProfile mobileProfile = new PatientMobileProfile();
            mobileProfile.Id = 0;
            mobileProfile.FullName = viewModel.FirstName + " " + viewModel.LastName;
            mobileProfile.PatientId = viewModel.Id;
            mobileProfile.Email = viewModel.Email;
            mobileProfile.Phone = viewModel.CellNumber;
            mobileProfile.Language = (viewModel.Language != null) ? viewModel.Language.Name : null;
            mobileProfile.Country = (viewModel.PatientAddress1 != null) ? ((viewModel.PatientAddress1.Country != null) ? viewModel.PatientAddress1.Country.Name : null) : null;
            mobileProfile.State = (viewModel.PatientAddress1 != null) ? ((viewModel.PatientAddress1.State != null) ? viewModel.PatientAddress1.State.Name : null) : null;
            mobileProfile.City = (viewModel.PatientAddress1 != null) ? ((viewModel.PatientAddress1.City != null) ? viewModel.PatientAddress1.City.Name : null) : null;
            await _mobileService.InsertUpdatePatientMobileProfileAsync(mobileProfile);

            #endregion

            return Created(GetEntityLocation(patient.Id), Mapper.Map<Patient, PatientResponse>(patient));
        }

        private async Task<PostalCodeResponse> GetPostalCodeDetail(string zipCode)
        {
            PostalCodeResponse postalCode = new PostalCodeResponse();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var query = await (from code in ctx.PostalCodes.AsNoTracking()
                                   join db_city in ctx.Cities.AsNoTracking() on code.CityId equals db_city.Id into city
                                   join db_state in ctx.States.AsNoTracking() on code.StateId equals db_state.Id into state
                                   join db_country in ctx.Countries.AsNoTracking() on code.CountryId equals db_country.Id into country

                                   from db_city in city.DefaultIfEmpty()
                                   from db_state in state.DefaultIfEmpty()
                                   from db_country in country.DefaultIfEmpty()

                                   where code.ZipCode == zipCode
                                   select new PostalCodeResponse
                                   {
                                       Id = code.Id,
                                       ZipCode = code.ZipCode,
                                       City = city.Select(c => new CodeResponse { Id = db_city.Id, Name = (db_city.Name + " - " + db_city.StateCode), StateCode = db_city.StateCode }).FirstOrDefault(),
                                       State = state.Select(c => new CodeResponse { Id = db_state.Id, Name = db_state.Name, StateCode = db_state.StateCode }).FirstOrDefault(),
                                       Country = country.Select(c => new CodeResponse { Id = db_country.Id, Name = db_country.Name, StateCode = null }).FirstOrDefault(),
                                   }).ToListAsync();

                postalCode = query.FirstOrDefault();
                return postalCode;
            }
        }

        /// <summary>
        /// Method Description : Using for Create / Update Patient Mobile Login.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="model">Account Request Model</param>
        /// <returns>Id</returns>
        public async Task<IHttpActionResult> InsertUpdateUser(long? id, AccountRequest model)
        {
            if (model != null && (model.Password == null || model.Password == ""))
            {
                ModelState.AddModelError("password", "Password is required.");
                return BadRequest(ModelState);
            }

            IdentityResult passwordErrors = new IdentityResult();
            var passwordValid = true;
            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                passwordErrors = await AppUserManager.PasswordValidator.ValidateAsync(model.Password);
                passwordValid = passwordErrors.Succeeded;
            }
            if (!ModelState.IsValid || !passwordValid)
            {
                ModelState.AddModelError("password", passwordErrors.Errors.FirstOrDefault());
                return BadRequest(ModelState);
            }

            var item = Mapper.Map<AccountRequest, ApplicationUser>(model);
            if (item.Id == null && item.Id == "")
            {
                item.Id = Guid.NewGuid().ToString(); ;
                await AppUserManager.CreateAsync(item);
                await AppUserManager.AddToRoleAsync(item.Id, model.Role);
            }
            else
            {
                var userUpdate = await AppUserManager.FindByIdAsync(item.Id);
                userUpdate.UserName = item.UserName;
                userUpdate.PasswordHash = item.PasswordHash;
                await AppUserManager.UpdateAsync(userUpdate);
                await AppUserManager.AddPasswordAsync(userUpdate.Id, model.Password);
            }
            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                await AppUserManager.RemovePasswordAsync(item.Id);
                await AppUserManager.AddPasswordAsync(item.Id, model.Password);
            }
            model.Password = Guid.NewGuid().ToString();

            return Ok(id);
        }

        /// <summary>
        /// Method Description : Using for Create / Update Patient Mobile Login.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="request">Account Request Model</param>
        /// <returns>Result</returns>
        [HttpPut]
        [Route("api/Patients/{id}/mobileAccess")]
        public async Task<IHttpActionResult> InsertUpdatePatientUser(long? id, AccountRequest request)
        {
            object result = string.Empty;
            object resultToken = string.Empty;

            AccountRequest model = new AccountRequest();
            model.Id = (!string.IsNullOrWhiteSpace(request.Id)) ? request.Id : Guid.NewGuid().ToString();
            model.isUpdate = (!string.IsNullOrWhiteSpace(request.Id)) ? true : false;
            model.UserName = request.UserName;
            model.Role = ApplicationUser.PatientRole;
            model.PatientId = id;
            model.Password = request.Password;

            if (model != null && (model.Password == null || model.Password == ""))
            {
                ModelState.AddModelError("password", "Password is required.");
                return BadRequest(ModelState);
            }

            IdentityResult passwordErrors = new IdentityResult();
            var passwordValid = true;
            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                passwordErrors = await AppUserManager.PasswordValidator.ValidateAsync(model.Password);
                passwordValid = passwordErrors.Succeeded;
            }
            if (!ModelState.IsValid || !passwordValid)
            {
                ModelState.AddModelError("password", passwordErrors.Errors.FirstOrDefault());
                return BadRequest(ModelState);
            }

            var item = Mapper.Map<AccountRequest, ApplicationUser>(model);
            string userId = request.Id;
            if (model.isUpdate == false)
            {
                await AppUserManager.CreateAsync(item);
                userId = item.Id;

                using (var clientToken = new HttpClient())
                {
                    //clientToken.BaseAddress = new Uri(" http://localhost:1008/");
                    clientToken.BaseAddress = new Uri(chatUrl);
                    clientToken.DefaultRequestHeaders.Accept.Clear();
                    clientToken.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await clientToken.GetAsync("api/mobile/v1/auth/twilio-token?userId=" + userId + "&deviceType=");
                    if (response.IsSuccessStatusCode)
                    {
                        resultToken = await response.Content.ReadAsAsync<object>();
                    }
                }

                using (var client = new HttpClient())
                {
                    //client.BaseAddress = new Uri(" http://localhost:1008/");
                    client.BaseAddress = new Uri(chatUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //object result = string.Empty;
                    HttpResponseMessage response = await client.GetAsync("api/mobile/v1/auth/virgil-card?userId=" + userId + "");
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<object>();
                    }
                }

                if (result == null)
                {
                    ModelState.AddModelError("PrivateKey", "Can not Create Chat User please save again.");
                    return BadRequest(ModelState);
                }

                ChatKeyRequest key = new ChatKeyRequest();
                key.UserId = userId;
                key.PrivateKey = result.ToString();

                await ChatKey(key);
            }
            else
            {
                var user = await AppUserManager.FindByIdAsync(request.Id);
                SetApplicationUserModel(user, model);
                user.Roles.Clear();
                await AppUserManager.UpdateAsync(user);
            }

            await AppUserManager.AddToRoleAsync(item.Id, model.Role);

            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                await AppUserManager.RemovePasswordAsync(item.Id);
                await AppUserManager.AddPasswordAsync(item.Id, model.Password);
            }

            #region Send Email 

            string email = string.Empty;
            string patientName = string.Empty;
            var patient = _patientRepository.Read(Convert.ToInt64(id), ReadOptions.Create<Patient>().AsReadOnly());
            patientName = patient.LastName + ' ' + patient.FirstName;
            if (patient.Email != null && patient.Email.Length > 0)
            {
                email = patient.Email;
                sendEmail(email, patientName, model.UserName, model.Password);
            }

            #endregion

            model.Password = Guid.NewGuid().ToString();

            return Ok(result);
        }

        public async Task<IHttpActionResult> ChatKey([FromBody] ChatKeyRequest chatKey)
        {
            bool isUpdate = chatKey.Id > 0 ? true : false;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var chat = Mapper.Map<ChatKeyRequest, ChatKey>(chatKey);
            await _chatService.InsertUpdateChatKeyAsync(chat);

            var chatkeys = await _chatService.ReadChatKeyAsync(chat.Id);

            //return Ok(chatkeys);

            return Ok(Mapper.Map<ChatKey, ChatKeyResponse>(chatkeys));

        }

        public void sendEmail(String email, String patientName, String userName, String password)
        {
            try
            {
                var message = _messageFactory.MobileAccessMailMessage(email, patientName, userName, password);
                _emailSender.Send(message);

            }
            catch (Exception ex)
            {

            }
        }

        public void SetApplicationUserModel(ApplicationUser model, AccountRequest request)
        {
            model.Id = request.Id;
            model.UserName = request.UserName;
            model.ResourceId = request.ResourceId;
        }

        /// <summary>
        /// Method Description : Using for Update Patient.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="patientRequest">Patient Request Model</param>
        /// <returns>Patient Response Model</returns>
        [HttpPut]
        [Route("api/Patients/{id}")]
        public async Task<IHttpActionResult> EditPatient(long id, [FromBody] PatientRequest patientRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #region Verify Country State City Zip Code Combination 

            if (patientRequest.PatientAddress1.CountryId != null && patientRequest.PatientAddress1.StateId != null
                       && patientRequest.PatientAddress1.CityId != null && patientRequest.PatientAddress1.ZipCode != null && patientRequest.PatientAddress1.ZipCode != "")
            {
                PostalCodeResponse code = await GetPostalCodeDetail(patientRequest.PatientAddress1.ZipCode);
                if (code == null)
                {
                    ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    return BadRequest(ModelState);
                }
                else
                {
                    if (code.Country != null)
                    {
                        if (patientRequest.PatientAddress1.CountryId != code.Country.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.State != null)
                    {
                        if (patientRequest.PatientAddress1.StateId != code.State.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.City != null)
                    {
                        if (patientRequest.PatientAddress1.CityId != code.City.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }

            if (patientRequest.PatientAddress2.CountryId != null && patientRequest.PatientAddress2.StateId != null
                       && patientRequest.PatientAddress2.CityId != null && patientRequest.PatientAddress2.ZipCode != null && patientRequest.PatientAddress2.ZipCode != "")
            {
                PostalCodeResponse code = await GetPostalCodeDetail(patientRequest.PatientAddress2.ZipCode);
                if (code == null)
                {
                    ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    return BadRequest(ModelState);
                }
                else
                {
                    if (code.Country != null)
                    {
                        if (patientRequest.PatientAddress2.CountryId != code.Country.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.State != null)
                    {
                        if (patientRequest.PatientAddress2.StateId != code.State.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.City != null)
                    {
                        if (patientRequest.PatientAddress2.CityId != code.City.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }

            if (patientRequest.ParentGaurdian != null)
            {
                if (patientRequest.ParentGaurdian.CountryId != null && patientRequest.ParentGaurdian.StateId != null
                           && patientRequest.ParentGaurdian.CityId != null && patientRequest.ParentGaurdian.ZipCode != null && patientRequest.ParentGaurdian.ZipCode != "")
                {
                    PostalCodeResponse code = await GetPostalCodeDetail(patientRequest.ParentGaurdian.ZipCode);
                    if (code == null)
                    {
                        ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                        return BadRequest(ModelState);
                    }
                    else
                    {
                        if (code.Country != null)
                        {
                            if (patientRequest.ParentGaurdian.CountryId != code.Country.Id)
                            {
                                ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                                return BadRequest(ModelState);
                            }
                        }
                        if (code.State != null)
                        {
                            if (patientRequest.ParentGaurdian.StateId != code.State.Id)
                            {
                                ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                                return BadRequest(ModelState);
                            }
                        }
                        if (code.City != null)
                        {
                            if (patientRequest.ParentGaurdian.CityId != code.City.Id)
                            {
                                ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                                return BadRequest(ModelState);
                            }
                        }
                    }
                }
            }

            #endregion

            #region Patient Address Mapping
            List<PatientAddressRequest> addressList = new List<PatientAddressRequest>();
            if (patientRequest.PatientAddress1 != null)
                addressList.Add(patientRequest.PatientAddress1);
            if (patientRequest.PatientAddress2 != null)
                addressList.Add(patientRequest.PatientAddress2);

            patientRequest.Addresses = addressList.ToArray();
            #endregion

            #region Patient Gaurdians Mapping
            List<PatientParentGuardianRequest> parentGaurdiansList = new List<PatientParentGuardianRequest>();
            if (patientRequest.ParentGaurdian != null)
                parentGaurdiansList.Add(patientRequest.ParentGaurdian);

            patientRequest.ParentGaurdians = parentGaurdiansList.ToArray();
            #endregion

            var patient = Mapper.Map<PatientRequest, Patient>(patientRequest);
            patient.Id = id;

            await _patientService.UpdatePatientAsync(patient);

            #region Mobile Access
            /*AccountRequest patientUserRequest = new AccountRequest();
            patientUserRequest.PatientId = patient.Id;
            patientUserRequest.Id = patientRequest.MobileAccessUserId;
            patientUserRequest.UserName = patientRequest.MobileAccessUserName;
            patientUserRequest.Password = patientRequest.MobileAccessPassword;
            patientUserRequest.Role = ApplicationUser.PatientRole;
            patientUserRequest.ResourceId = null;

            if (patientUserRequest != null && patientRequest.MobileAccessUserName != null && patientRequest.MobileAccessPassword != null)
                await InsertUpdateUser(patient.Id, patientUserRequest);
            */
            #endregion

            var tempPatient = await _patientService.ReadPatientAsync(patient.Id);

            var viewModel = Mapper.Map<Patient, PatientResponse>(tempPatient);
            if (patient.Addresses.Count > 0)
            {
                viewModel.PatientAddress1 = (tempPatient.Addresses.Count >= 1) ? (Mapper.Map<PatientAddress, PatientAddressResponse>(tempPatient.Addresses.ToArray()[0])) : null;
                viewModel.PatientAddress2 = (tempPatient.Addresses.Count >= 2) ? (Mapper.Map<PatientAddress, PatientAddressResponse>(tempPatient.Addresses.ToArray()[1])) : null;
            }

            #region we are added default data in patient mobile profile entity

            var mobileProfileData = new PatientMobileProfile();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                var dbSet = ctx.Set<PatientMobileProfile>();
                mobileProfileData = await dbSet.AsNoTracking().Where(x => x.PatientId == id).FirstOrDefaultAsync();
            }
            if (mobileProfileData != null)
            {
                PatientMobileProfile mobileProfile = new PatientMobileProfile();
                mobileProfile.Id = mobileProfileData.Id;
                mobileProfile.FullName = mobileProfileData.FullName;
                mobileProfile.PatientId = id;
                mobileProfile.Email = viewModel.Email;
                mobileProfile.Phone = viewModel.CellNumber;
                mobileProfile.Language = (viewModel.Language != null) ? viewModel.Language.Name : null;
                mobileProfile.Country = (viewModel.PatientAddress1 != null) ? ((viewModel.PatientAddress1.Country != null) ? viewModel.PatientAddress1.Country.Name : null) : null;
                mobileProfile.State = (viewModel.PatientAddress1 != null) ? ((viewModel.PatientAddress1.State != null) ? viewModel.PatientAddress1.State.Name : null) : null;
                mobileProfile.City = (viewModel.PatientAddress1 != null) ? ((viewModel.PatientAddress1.City != null) ? viewModel.PatientAddress1.City.Name : null) : null;
                await _mobileService.InsertUpdatePatientMobileProfileAsync(mobileProfile);
            }
            #endregion

            return Ok(Mapper.Map<Patient, PatientResponse>(patient));
        }

        /// <summary>
        /// Method Description : Using for Delete Patient by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patient Response Model</returns>
        [HttpDelete]
        [Route("api/Patients/{id}")]
        public async Task<IHttpActionResult> DeletePatientItem(long id)
        {
            var patient = _patientRepository.Read(id);
            if (patient == null)
                return NotFound();

            await _patientService.DeletePatientAsync(patient);

            return Ok(new PatientResponse { Id = patient.Id });
        }

        /// <summary>
        /// Method Description : Using for Get Patients by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patients Response Model</returns>
        [HttpGet]
        [Route("api/Patients/{id}")]
        public async Task<IHttpActionResult> GetPatient(long id)
        {
            var patient = await _patientService.ReadPatientAsync(id);

            var viewModel = Mapper.Map<Patient, PatientResponse>(patient);
            if (patient.Addresses.Count > 0)
            {
                viewModel.PatientAddress1 = (patient.Addresses.Count >= 1) ? (Mapper.Map<PatientAddress, PatientAddressResponse>(patient.Addresses.ToArray()[0])) : null;
                viewModel.PatientAddress2 = (patient.Addresses.Count >= 2) ? (Mapper.Map<PatientAddress, PatientAddressResponse>(patient.Addresses.ToArray()[1])) : null;

                #region set detail data of city state postal

                viewModel.PatientAddress1 = (viewModel.PatientAddress1 != null) ? ModifyAddressData(viewModel.PatientAddress1) : null;
                viewModel.PatientAddress2 = (viewModel.PatientAddress2 != null) ? ModifyAddressData(viewModel.PatientAddress2) : null;

                #endregion
            }
            if (patient.ParentGaurdians.Count > 0)
            {
                viewModel.ParentGaurdian = (patient.ParentGaurdians.Count >= 1) ? (Mapper.Map<PatientParentGuardian, PatientParentGuardianResponse>(patient.ParentGaurdians.ToArray()[0])) : null;
                viewModel.ParentGaurdian = (viewModel.ParentGaurdian != null) ? ModifyPatientParentGuardianAddressData(viewModel.ParentGaurdian) : null;
            }

            var casePatient = _patientCaseRepository.List(new FilterQuery<PatientCase>()
                      .AddFilter(x => x.PatientId == id && x.CaseStatus == CaseStatusEnum.Inprogress && (x.IsDeleted == false || x.IsDeleted == null))).FirstOrDefault();
            if (casePatient != null)
            {
                var patientCurrentCase = Mapper.Map<PatientCase, PatientCaseResponse>(casePatient);
                viewModel.CaseId = patientCurrentCase.Id;
            }

            #region Get Patient Asp net User Detail for the Mobile Access
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                var patientUser = ctx.Users.Where(x => x.PatientId == viewModel.Id).FirstOrDefault();

                if (patientUser != null)
                {
                    AccountPatientResponse patientPrfoile = new AccountPatientResponse();
                    patientPrfoile.Id = patientUser.Id;
                    patientPrfoile.UserName = patientUser.UserName;
                    patientPrfoile.Role = ApplicationUser.PatientRole;
                    patientPrfoile.Password = string.Empty;//patientUser.PasswordHash; //-- here default we set null value in password.
                    patientPrfoile.PatientId = patientUser.PatientId;

                    viewModel.PatientUserInfo = patientPrfoile;
                }
            }
            #endregion

            return Ok(viewModel);
        }

        private PatientAddressResponse ModifyAddressData(PatientAddressResponse res)
        {
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                #region City Postal Code
                if (res.CityId != null)
                {
                    var cityQuery = (from city in ctx.Cities.AsNoTracking()
                                     where city.Id == res.CityId
                                     select new CityResponse
                                     {
                                         Id = city.Id,
                                         Name = city.Name,
                                         StateId = city.StateId,
                                         StateCode = city.StateCode
                                     }).FirstOrDefault();

                    if (cityQuery != null)
                    {
                        cityQuery.State = (cityQuery.StateId != null) ? (ctx.States.AsNoTracking().Where(x => x.Id == cityQuery.StateId).Select(y => new CodeResponse { Id = y.Id, Name = y.Name }).FirstOrDefault()) : null;
                        cityQuery.StateName = (cityQuery.State != null) ? cityQuery.State.Name : cityQuery.StateCode;

                        //List<CodeResponse> postalcodes = ctx.PostalCodes.AsNoTracking()
                        //                    .Where(x => x.CityId == cityQuery.Id)
                        //                    .Select(y => new CodeResponse { Id = y.Id, Name = y.ZipCode }).ToList();

                        //cityQuery.PostalCodeLists = (postalcodes != null) ? postalcodes.ToArray() : null;
                        res.City = cityQuery;
                    }
                }
                #endregion

                #region zipCode
                string tempZipcode = ctx.PatientAddresss.AsNoTracking().Where(x => x.Id == res.Id).Select(x => x.ZipCode).FirstOrDefault();

                if (tempZipcode != null)
                {
                    var zipCodeQuery = (from code in ctx.PostalCodes.AsNoTracking()
                                        where code.ZipCode == tempZipcode
                                        select new PicklistResponse
                                        {
                                            Id = code.Id,
                                            Name = code.ZipCode
                                        }).FirstOrDefault();
                    res.ZipCode = zipCodeQuery;
                    res.ZipCodeTemp = tempZipcode;
                }
                #endregion
            }
            return res;
        }

        private PatientParentGuardianResponse ModifyPatientParentGuardianAddressData(PatientParentGuardianResponse res)
        {
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                #region City Postal Code
                if (res.CityId != null)
                {
                    var cityQuery = (from city in ctx.Cities.AsNoTracking()
                                     where city.Id == res.CityId
                                     select new CityResponse
                                     {
                                         Id = city.Id,
                                         Name = city.Name,
                                         StateId = city.StateId,
                                         StateCode = city.StateCode
                                     }).FirstOrDefault();

                    if (cityQuery != null)
                    {
                        cityQuery.State = (cityQuery.StateId != null) ? (ctx.States.AsNoTracking().Where(x => x.Id == cityQuery.StateId).Select(y => new CodeResponse { Id = y.Id, Name = y.Name }).FirstOrDefault()) : null;
                        cityQuery.StateName = (cityQuery.State != null) ? cityQuery.State.Name : cityQuery.StateCode;
                        res.City = cityQuery;
                    }
                }
                #endregion

                #region zipCode
                string tempZipcode = ctx.PatientParentGuardians.AsNoTracking().Where(x => x.Id == res.Id).Select(x => x.ZipCode).FirstOrDefault();

                if (tempZipcode != null)
                {
                    var zipCodeQuery = (from code in ctx.PostalCodes.AsNoTracking()
                                        where code.ZipCode == tempZipcode
                                        select new PicklistResponse
                                        {
                                            Id = code.Id,
                                            Name = code.ZipCode
                                        }).FirstOrDefault();
                    res.ZipCode = zipCodeQuery;
                    res.ZipCodeTemp = tempZipcode;
                }
                #endregion
            }
            return res;
        }


        private static Expression<Func<Patient, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            //return clininc => clininc.Name.Contains(searchPhrase);
            return patient => string.Concat(patient.LastName, " ", patient.FirstName).Contains(searchPhrase)
            || patient.PatientNumber.Contains(searchPhrase);
            //|| patient.PatientStatus.Value(searchPhrase);
            //|| resource.Department.Name.Contains(searchPhrase)
            //|| resource.Location.Contains(searchPhrase)
            //|| resource.CustomFieldValues.Where(c => c.CustomField.IsSearchable).Select(x => x.Value).Any(y => y.Contains(searchPhrase));
        }

        private static Expression<Func<Patient, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
                return null;

            //return Patient => Patient.Name.Contains(searchPhrase);
            return patient => string.Concat(patient.LastName, " ", patient.FirstName).Contains(searchPhrase)
            || patient.PatientNumber.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Patient.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Patient Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/Patients/autocomplete")]
        [ResponseType(typeof(PatientAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<Patient>();
            if (!string.IsNullOrWhiteSpace(search))
                //filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
                filter.AddFilter(x => (x.LastName + " " + x.FirstName).Contains(search.ToLower()));

            filter.AddFilter(x => x.Status != 0);
            var autocompleteResult = await _patientRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<Patient[], PatientAutoCompleteResponse[]>(autocompleteResult.ToArray()).Where(x => (x.IsDeleted == false || x.IsDeleted == null));
            //var result = Mapper.Map<Patient[], PatientAutoCompleteResponse[]>(autocompleteResult.ToArray());

            foreach (var item in result.ToList())
            {
                var currentCaseDetail = _patientCaseRepository.List(new FilterQuery<PatientCase>().AddFilter(x => x.PatientId == item.Id && x.CaseStatus == CaseStatusEnum.Inprogress && (x.IsDeleted == false || x.IsDeleted == null))).FirstOrDefault();
                if (currentCaseDetail != null)
                {
                    item.CaseId = currentCaseDetail.Id;
                    item.CaseNo = currentCaseDetail.CaseNo;
                }
            }
            return Ok(result);
        }

        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer = new byte[0];
            if (File.Exists(filePath))
            {
                FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                try
                {
                    int length = (int)fileStream.Length;  // get file length
                    buffer = new byte[length];            // create buffer
                    int count;                            // actual number of bytes read
                    int sum = 0;                          // total number of bytes read

                    // read until Read method returns 0 (end of the stream has been reached)
                    while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                        sum += count;  // sum is a buffer offset for next reading
                }
                finally
                {
                    fileStream.Close();
                }
            }
            return buffer;
        }

        /// <summary>
        /// Method Description : Using for Upload Photo.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Patients/UploadPhoto")]
        public async Task<IHttpActionResult> UploadPhoto(long id)
        {
            var statusCodeResult = GetStatusCodeError(Crud.Update, Section.Patients);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            if (!_patientRepository.Exists(FilterQuery.Create<Patient>().AddFilter(p => p.Id == id)))
            {
                ModelState.AddModelError("id", "The patient with provided Id doesn't exists");
                return BadRequest(ModelState);
            }

            var result = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());

            var originalFileName = result.Files.First().Headers.ContentDisposition.FileName;
            var ext = Path.GetExtension(JsonConvert.DeserializeObject(originalFileName).ToString());
            var newfileName = string.Format(FileUploadHelpers.PatientPhotoFileNameTemplate, id, ext).ToLower();

            var file = result.Files[0];
            var fileStream = await file.ReadAsStreamAsync();

            var wasS3UploadSuccessful = await FileUploadHelpers.S3PatientPhotoUploadAsync(newfileName, fileStream);

            if (!wasS3UploadSuccessful)
                return BadRequest();
            else
            {
                #region Default Upload Photo from mobile
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbset = ctx.Set<PatientMobileProfile>();
                    var patientProfileDetail = ctx.PatientMobileProfiles.Where(x => x.PatientId == id).FirstOrDefault();
                    if (patientProfileDetail != null)
                    {
                        if (patientProfileDetail.Photo == "" || patientProfileDetail.Photo == null)
                        {
                            var extMobile = Path.GetExtension(JsonConvert.DeserializeObject(originalFileName).ToString());
                            var newfileNameMobile = string.Format(FileUploadHelpers.PatientMobilePhotoFileNameTemplate, patientProfileDetail.PatientId, ext).ToLower();

                            var wasS3UploadmMobileSuccessful = await FileUploadHelpers.S3PatientMobileDefaultPhotoUploadFromWebAsync(newfileName, FileUploadHelpers.PatientFolderTemplate, newfileNameMobile, FileUploadHelpers.PatientFolderTemplate);
                            if (wasS3UploadmMobileSuccessful)
                            {
                                patientProfileDetail.Photo = newfileNameMobile;
                                string updatePhotoQuery = "update patient_mobile_profiles set photo = N'" + newfileNameMobile + "' where patient_Id = " + patientProfileDetail.PatientId;
                                ctx.Database.ExecuteSqlCommand(updatePhotoQuery);
                            }
                        }
                    }
                }
                #endregion
            }
            var resource = _patientRepository.Read(id);
            resource.PhotoFileName = newfileName;
            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Upload File.   
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Patients/UploadFile")]
        public async Task<IHttpActionResult> UploadFile()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            var result = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());
            var originalFileName = result.Files.First().Headers.ContentDisposition.FileName;
            originalFileName = originalFileName.Substring(1, originalFileName.Length - 2);
            var newfileName = string.Format(FileUploadHelpers.FileNameTemplate, Guid.NewGuid(), originalFileName).ToLower();

            var file = result.Files[0];
            var fileStream = await file.ReadAsStreamAsync();

            var wasS3UploadSuccessful = await FileUploadHelpers.S3UploadAsync(newfileName, fileStream, "temp");

            if (wasS3UploadSuccessful)
            {
                return Ok(new
                {
                    FileName = newfileName
                });
            }
            return BadRequest();
        }

        /// <summary>
        /// Method Description : Using for Get All Patient List.      
        /// </summary>
        /// <param name="request">Patient List Request Model</param>
        /// <returns>Patient Detail List Response Model</returns>
        [HttpGet]
        [Route("api/Patients")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<PatientDetailListResponse>))]
        public async Task<IHttpActionResult> GetAllPatientList([FromUri]PatientListRequest request)
        {
            request.PageIndex = request.PageIndex - 1;

            List<PatientDetailListResponse> patientList = new List<PatientDetailListResponse>();
            var response = new ListResponse<PatientDetailListResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                //For Data
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_patient_detail_list @responseType='Data',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@sortField = '" + request.SortField + "',@sortOrder = '" + request.SortOrder + "',@searchPhrase = '" + request.SearchPhrase + "'";
                var patientDataList = await ctx.Database.SqlQuery<PatientDetailListResponse>(query).ToListAsync();

                foreach (var pat in patientDataList.ToList())
                {
                    if (pat.UserId != null)
                    {
                        pat.HasMobileAccess = true;
                    }
                    else
                    {
                        pat.HasMobileAccess = false;
                    }

                    if (pat.HepId != null)
                    {
                        pat.HasHep = true;
                    }
                    else
                    {
                        pat.HasHep = false;
                    }

                    var photo = pat.PhotoFileNameUrl;
                    pat.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(photo, FileUploadHelpers.ResourceFolderNameTemplate);
                }

                response.Data = patientDataList.ToArray();

                //For Item Count                
                string itemCountQuery = "EXEC sp_patient_detail_list @responseType='DataCount',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@sortField = '" + request.SortField + "',@sortOrder = '" + request.SortOrder + "',@searchPhrase = '" + request.SearchPhrase + "'";
                response.ItemsCount = await ctx.Database.SqlQuery<long>(itemCountQuery).FirstOrDefaultAsync();

                return Ok(response);
            }
        }


        private byte[] getPDFForHEP()
        {

            StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Templates/PdfTemplates/HtmlPage1.html"));
            string readFile = reader.ReadToEnd();
            string StrContent = "";
            StrContent = readFile;
            //        StringBuilder sb = new StringBuilder();
            StrContent = StrContent.Replace("[patientname]", "Mulugeta");
            StrContent = StrContent.Replace("[casenumber]", "123");
            StrContent = StrContent.Replace("[startdate]", "2/2/2021");
            StrContent = StrContent.Replace("[enddate]", "2/2/2021");
            reader.Close();
            return PdfUtils.genratePDF(StrContent, iTextSharp.text.PageSize.A4);
        }


        [HttpGet]
        [Route("api/patients/exportToPdf")]
        public HttpResponseMessage ExportHEPDetail() // here id = caseId
        {
            byte[] pdfByteArray = null;

            var message = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(getPDFForHEP())
            };
            message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

            string pdfFileName = "Mulugeta_HEP_.pdf";

            message.Content.Headers.ContentDisposition.FileName = pdfFileName;

            return message;
        }
    }
}
