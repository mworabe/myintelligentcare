﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ExerciseActivities
{
    [DataContract]
    public class ExerciseActivityResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}