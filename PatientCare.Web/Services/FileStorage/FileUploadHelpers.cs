﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;

namespace PatientCare.Web.Services.FileStorage
{
    public static class FileUploadHelpers
    {
        public const string CvFileNameTemplate = "{0}_cv{1}";
        public const string PhotoFileNameTemplate = "{0}_photo{1}";
        public const string LogoFileNameTemplate = "{0}_{1}_logo{2}";
        public const string FileNameTemplate = "{0}{1}";

        public const string ResourceFolderNameTemplate = "resources/";
        public const string ResourcePhotoFileNameTemplate = "{0}_photo{1}";

        public const string ResourceFolderTemplate = "resources\\{0}";

        public const string ResourceLanguageTemplate = "resources/{0}/languages/{1}";
        public const string ResourceLanguageFolderTemplate = "resources\\{0}\\languages";

        public const string ResourceCustomFieldFilesTemplate = "resources/{0}/customFields";
        public const string ResourceCustomFieldFilesFolderTemplate = "resources\\{0}\\customFields";

        public const string ExerciseMediaFilesTemplate = "exercise/{0}/medias";
        public const string ExerciseMediaFilesFolderTemplate = "exercise\\{0}\\medias";

        public const string PatientFolderTemplate = "patients/";
        public const string PatientPhotoFileNameTemplate = "{0}_photo{1}";

        public const string PatientMobilePhotoFileNameTemplate = "{0}_mobile_photo{1}";


        public static async Task<bool> S3PatientMobileDefaultPhotoUploadFromWebAsync(string fileName, string folderPath, string newProfileNameMobile,string newfolderPath)
        {
            bool wasUploadSuccessful;
            var storageService = new StorageService();
            try
            {
                await storageService.CopyFileAsync(fileName, folderPath, newProfileNameMobile, newfolderPath);              
                wasUploadSuccessful = true;
            }
            catch (Exception ex)
            {
                // TODO Remake this shit eventually
                wasUploadSuccessful = false;
            }

            return wasUploadSuccessful;
        }

        public static async Task<bool> S3PatientPhotoUploadAsync(string fileName, Stream stream)
        {
            return await S3PatientPhotoUploadAsync(fileName, stream, PatientFolderTemplate);
        }

        public static async Task<bool> S3PatientPhotoUploadAsync(string fileName, Stream stream, string folderPath)
        {
            bool wasUploadSuccessful;
            var storageService = new StorageService();
            try
            {
                await storageService.UploadFileAsync(fileName, folderPath, stream);
                stream.Close();
                wasUploadSuccessful = true;
            }
            catch (Exception ex)
            {
                // TODO Remake this shit eventually
                wasUploadSuccessful = false;
            }

            return wasUploadSuccessful;
        }

        public static async Task<bool> S3ResourcePhotoUploadAsync(string fileName, Stream stream)
        {
            return await S3ResourcePhotoUploadAsync(fileName, stream, ResourceFolderNameTemplate);
        }

        public static async Task<bool> S3ResourcePhotoUploadAsync(string fileName, Stream stream, string folderPath)
        {
            bool wasUploadSuccessful;
            var storageService = new StorageService();
            try
            {
                await storageService.UploadFileAsync(fileName, folderPath, stream);
                stream.Close();
                wasUploadSuccessful = true;
            }
            catch
            {
                // TODO Remake this shit eventually
                wasUploadSuccessful = false;
            }

            return wasUploadSuccessful;
        }

        public static async Task<bool> S3UploadAsync(string fileName, Stream stream)
        {
            return await S3UploadAsync(fileName, stream, string.Empty);
        }

        public static async Task<bool> S3UploadAsync(string fileName, Stream stream, string folderPath)
        {
            bool wasUploadSuccessful;
            var storageService = new StorageService();
            try
            {
                await storageService.UploadFileAsync(fileName, folderPath, stream);
                stream.Close();
                wasUploadSuccessful = true;
            }
            catch
            {
                // TODO Remake this shit eventually
                wasUploadSuccessful = false;
            }

            return wasUploadSuccessful;
        }

        public static async Task<bool> CopyAsync(string fileName, string folderPath, string destFileName, string destFolderPath)
        {            // Do S3 upload stuff
            bool wasS3UploadSuccessful;
            var storageService = new StorageService();
            try
            {
                await storageService.CopyFileAsync(fileName, folderPath, destFileName, destFolderPath);
                wasS3UploadSuccessful = true;
            }
            catch (Exception)
            {
                // TODO Remake this shit eventually
                wasS3UploadSuccessful = false;
            }


            return wasS3UploadSuccessful;

        }

        public static async Task<bool> S3DeleteFilesAsync(string[] fileNames)
        {            // Do S3 upload stuff
            bool wasS3UploadSuccessful;
            var storageService = new StorageService();
            try
            {
                await storageService.DeleteObjects(fileNames);
                wasS3UploadSuccessful = true;
            }
            catch (Exception)
            {
                // TODO Remake this shit eventually
                wasS3UploadSuccessful = false;
            }


            return wasS3UploadSuccessful;

        }

        public static IEnumerable<S3DirectoryInfo> GetS3DirectoryInfos(string path)
        {
            var storageService = new StorageService();
            return storageService.GetS3DirectoryInfos(path);
        }

        public static IEnumerable<string> GetDirectoryInfos(string path)
        {
            var storageService = new StorageService();
            return storageService.GetDirectoryInfos(path);
        }

        public static void DeleteFolder(string path)
        {
            var storageService = new StorageService();
            storageService.DeleteFolder(path);
        }

        public static string GetStorageUrl(string fileName, string directoryPath)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }

            var storageService = new StorageService();
            var result = storageService.GetStorageUrl(fileName, directoryPath);
            return result;
        }

        public static string GetStorageUrlPhoto(string fileName, string directoryPath)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }

            var storageService = new StorageService();
            var result = storageService.GetStorageUrlPhoto(fileName, directoryPath);
            return result;
        }

        public static string GetStorageMediaUrlPhoto(string fileName, string directoryPath)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }

            var storageService = new StorageService();
            var result = storageService.GetStorageMediaUrlPhoto(fileName, directoryPath);
            return result;
        }

        public static IList<string> GetFileNames(string directoryPath)
        {
            var storageService = new StorageService();
            return storageService.GetFileNames(directoryPath);
        }
    }
}