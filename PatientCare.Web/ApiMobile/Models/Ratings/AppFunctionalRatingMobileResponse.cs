﻿using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.Ratings
{
    [DataContract]
    public class AppFunctionalRatingMobileResponse
    {
        [DataMember]
        public long Id { get; set; }
        
        [DataMember]
        public long? PatientId { get; set; }
        [DataMember]
        public PicklistDefaultResponse Patient { get; set; }

        [DataMember]
        public long? PatientCaseId { get; set; }
        [DataMember]
        public PicklistDefaultResponse PatientCase { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public int? FunctionalRating { get; set; }
    }
}