﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Clinic.
    /// </summary>
    public class Clinic : NamedEntity
    {
        public string ContactPerson { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ClinicPrefix { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
