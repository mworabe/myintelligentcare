﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ChatSessionMappingExtensions
    {
        public static DbModelBuilder MapChatSession(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ChatSession>().ToTable("chat_Sessions").HasKey(i => i.Id);

            modelBuilder.Entity<ChatSession>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ChatSession>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ChatSession>()
                .Property(p => p.ChannelsId)
                .HasColumnName("channel_id")
                .IsOptional();

            modelBuilder.Entity<ChatSession>()
                .Property(p => p.ChannelUniqueName)
                .HasColumnName("channel_unique_name")
                .IsOptional();

            modelBuilder.Entity<ChatSession>()
                .Property(p => p.ChannelName)
                .HasColumnName("channel_name")
               .IsOptional();

            modelBuilder.Entity<ChatSession>()
                .Property(p => p.FromUserId)
                .HasColumnName("from_user_id")
                .IsOptional();

            modelBuilder.Entity<ChatSession>()
                .Property(p => p.ToUserId)
                .HasColumnName("to_user_id")
                .IsOptional();

            modelBuilder.Entity<ChatSession>()
                .Property(p => p.CreatedByUserId)
                .HasColumnName("created_by_user_id")
                .IsOptional();

            modelBuilder.Entity<ChatSession>()
                .Property(p => p.LastMessage)
                .HasColumnName("last_message")
                .IsOptional();


            modelBuilder.Entity<ChatSession>()
                .Property(p => p.Type)
                .HasColumnName("type")
                .IsOptional();

            modelBuilder.Entity<ChatSession>()
                .HasMany<ChatMember>(p => p.ChatMembers)
                .WithOptional()
                .HasForeignKey(item => item.ChatSessionId);

            //modelBuilder.Entity<ChatSession>()
            //    .Property(p => p.CreatedDates)
            //    .HasColumnName("created_dates")
            //    .IsOptional();

            return modelBuilder;
        }
    }
}
