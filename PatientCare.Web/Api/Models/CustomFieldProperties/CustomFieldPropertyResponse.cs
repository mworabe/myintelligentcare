﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.CustomFieldProperties
{
    [DataContract]
    public class CustomFieldPropertyResponse
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public long CustomFieldId { get; set; }
        [DataMember]
        public string Value { get; set; }
    }
}