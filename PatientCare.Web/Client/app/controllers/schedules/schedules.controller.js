
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ScheduleCtrl", ['$rootScope', "$scope", "AppointmentFactory", "$stateParams", "initialData", 'uiCalendarConfig',
        '$q', '$timeout', '$compile', '$uibModal', 'ScheduleService', 'AppointmentService',
        'Modal', 'ReferralDialogFactory', "$state", "AppointmentTypeService",
        function ($rootScope, $scope, AppointmentFactory, $stateParams, initialData, uiCalendarConfig,
            $q, $timeout, $compile, $uibModal, ScheduleService, AppointmentService,
            Modal, referralDialogFactory, $state, appointmentTypeService) {
            var self = this;

            function calculateHeight() {
                var top = $("#scheduler_here").offset().top;
                var windowHeight = $(window).height();
                self.calendarHeight = windowHeight - top - 50;
                $("#scheduler_here").height(self.calendarHeight);
            }
            calculateHeight();
            $(window).resize(function () {
                $timeout(function () {
                    calculateHeight();
                    scheduler.setCurrentView();
                }, 100);
            });

            //var dayStartHour = 8;
            //var dayEndHour = 20;
            self.currentDate = new Date();
            self.loadPage = undefined;
            self.clinicList = initialData[0];
            self.clinicianList = initialData[1];
            self.myLocations = initialData[2];
            self.selectedClinics = [];
            self.selectedStatus = [];
            self.sections = [];                                                           // are units that displayed the lists of clinicians selected
            self.addedEvents = [];
            self.dataList = [];                                                           //data to be bound to the schedular
            self.statusList = AppointmentFactory.getAllStatus();
            self.selectedStatus = angular.copy(AppointmentFactory.getAllStatus());
            self.selectedClinics = angular.copy(self.clinicList);
            self.clinicLocs = [];
            self.mySelection;
            self.clinicianListToDisplay = [];
            self.myClinicLocations;
            self.statusConfigs = { smartButtonMaxItems: 1, buttonClasses: 'btn btn-default btn-blue' };

            $rootScope.$broadcast('onClearNotif', "Appointment");
                        if (self.clinicianList) {
                angular.forEach(self.clinicianList, function (item) {
                    item.isSelected = true;
                });
            }

            self.onClinicSelection = function () {
                self.clinicLocs = [];
                _.forEach(self.myLocations, function (item) {
                    if (_.isEqual(item.clinicId, self.mySelection.id)) {
                        item.isSelected = false;
                        self.clinicLocs.push(item);
                    }
                });
                self.sections = [];
                self.loadData(true);
            }

            self.populateClinician = function () {
                self.clinicianListToDisplay = [{ key: 0, name: "Select All" }];
                _.forEach(self.clinicianList, function (item) {
                    if (_.isEqual(item.clinicLocationId, self.myClinicLocations.id)) {
                        item.isSelected = false;
                        self.clinicianListToDisplay.push(item);
                    }
                });
                self.sections = [];
                self.loadData(true);
            }

            self.onStatusSelected = function (item) {
                self.loadData(true);
            }

            var units = scheduler.serverList("units");                                  //returns lists by name units 
            if (!units) {
                scheduler.serverList("units", self.sections);                           //returns lists with value of sections
            } else {
                self.sections = units;
            }
            self.lastDragEventId = null;
            self.lastDragMode = null;
            self.lastDragNativeEvent = null;

            $scope.$on('onNewAppointment', function (event, item) {
                if (item) {
                }
                self.loadData(true);
            });
            self.addNewRef = function () {
                referralDialogFactory.showPhysicianModal({}, function (data) { });
            }
            self.addNewPatientButtonClick = function addNewPatientButtonClick() {
                $state.go("schedules.patients-add");
            }
            self.addNewAppointment = function (date, selectedClinician) {
                AppointmentFactory.newAppointment({ scheduleDate: date, clinician: selectedClinician }, function (result) {
                    if (result) {
                        refreshGrid();
                    }
                });
            }
            self.onAllClinicianSelected = function onAllClinicianSelected() {
                _.forEach(self.clinicianList, function (clinician) {
                    clinician.isSelected = self.clinicianList.isSelected;
                });
               
                self.loadData(true);
            }
            self.onClinicianSelected = function (clinician) {
                if (_.isEqual(clinician.key, 0)) {
                    if (clinician.isSelected) {
                        _.forEach(self.clinicianListToDisplay, function (item) {
                            item.isSelected = true;
                        });
                    }
                    else {
                        {
                            _.forEach(self.clinicianListToDisplay, function (item) {
                                item.isSelected = false;
                            });
                        }
                    }
                }
                else if (!clinician.isSelected) {
                    _.forEach(self.clinicianListToDisplay, function (item) {
                        if (_.isEqual(item.key, 0)) {
                            if (item.isSelected) {
                                item.isSelected = false;
                            }
                        }
                    });
                }
                else if (clinician.isSelected)
                {
                    var i = 0;
                    _.forEach(self.clinicianListToDisplay, function (item) {
                        if (!item.isSelected)
                        {
                            i++;
                        }
                    });
                    if (i == 1)
                    {
                        self.clinicianListToDisplay[0].isSelected = true;
                    }
                }

                self.isFilterApplied = true;
                self.loadData(true);
            }
            self.updateEvent = function (event) {
                self.pageLoading = AppointmentService.update({ id: event.appointmentId }, event, function (result) {
                    if (result) {
                    }
                });
            }
            self.deleteEvent = function (eventId) {
                Modal.confirm.delete(function () {
                    self.pageLoading = AppointmentService.delete({ id: eventId }, event, function (result) {
                        if (result) {
                            self.loadData(true);
                        }
                    });
                })("Warning", "Event");

            }

            function initSchedular() {
                scheduler.config.multi_day = true;
                scheduler.config.xml_date = "%Y-%m-%d %H:%i";
                scheduler.config.hour_date = "%h:%i %A";
                scheduler.locale.labels.unit_tab = "Clinicians Daily";
                scheduler.locale.labels.weekly_tab = "Clinicians Weekly";
                scheduler.locale.labels.section_custom = "Assigned to";
                scheduler.config.scroll_hour = 8;
                scheduler.config.time_step = 30;
                //scheduler.config.first_hour = 8;
                //scheduler.config.last_hour = 18;
                scheduler.config.drag_create = false;
                scheduler.config.details_on_create = false;
                scheduler.config.details_on_dblclick = false;
                scheduler.config.hour_size_px = 168;
                scheduler.templates.event_class = function (s, e, ev) { return ev.custom ? "custom" : ""; };
                scheduler.config.lightbox.sections = [
                    { name: "description", height: 130, map_to: "text", type: "textarea", focus: true },
                    { name: "custom", height: 23, type: "select", options: self.sections, map_to: "section_id" },
                    { name: "time", height: 72, type: "time", map_to: "auto" }
                ]


                scheduler.config.icons_select = ["icon_edit", "icon_delete"];
                scheduler._click.buttons.edit = function (id) {
                }

                scheduler._click.buttons.delete = function (id) {
                    self.deleteEvent(id);
                }

                scheduler.config.separate_short_events = true;

                self.addedEvents.push(scheduler.attachEvent("onTemplatesReady", function () {                            //attach handler to an inner event of dhtmlxScheduler
                    scheduler.templates.event_header = function (start, end, ev) {                                       //sets header for event
                        var tempalte = "<div class='event-header' style='padding-left:5px;text-align:left'>";
                        if (ev && ev.dataObject && ev.dataObject.appointmentStatus) {
                            var status = AppointmentFactory.getStatusFromText(ev.dataObject.appointmentStatus);
                            if (status) {
                                tempalte += "<i class=\"fa " + status.icon + "\" aria-hidden=\"true\"></i> ";
                            }
                        }
                        tempalte += "<span style='padding-left:5px'>" + scheduler.templates.event_date(start) + "-" +
                            scheduler.templates.event_date(end) + "</span> </div>";
                        return tempalte;
                    };

                    scheduler.templates.event_text = function (start, end, event) {
                        var status = AppointmentFactory.getTypeFromText(event.dataObject.appointmentTypeName);
                        var tempalte = "<div class='event-body'>";
                        tempalte += "<div style='margin-left:15px;' class='event-patient-name'>" + event.text + "</br>";
                        if (event.dataObject && event.dataObject.injuryRegionName)
                            tempalte += event.dataObject.injuryRegionName;
                        tempalte += "</div></div ></br>";
                        tempalte += "<div style='margin-left:30px;'><i class=\"fa " + status.icon + "\" aria-hidden=\"true\"></i></div>";
                        return tempalte;
                    }

                    var format = scheduler.date.date_to_str("%Y-%m-%d %H:%i");
                    scheduler.templates.tooltip_text = function (start, end, event) {
                        return "<div style='width:230px'><b> Appointment Type: </b>" + event.dataObject.appointmentTypeName + "</br><b> Clinician Name: </b>" + event.dataObject.clinicianName + "</br><b> Patient Name:</b> " + event.text + "</br><b> Injury Region :</b> " + event.dataObject.injuryRegionName +
                            "<br/><b> Payment Type: </b>" + " " + "</br><b> Start date:</b > " + format(start) + "<br/><b>End date:</b> " + format(end) + "</br></div>";
                    };

                }));
                scheduler.createUnitsView({
                    name: "unit",
                    property: "section_id",
                    list: self.sections,
                    skip_incorrect: true,
                    days: 1,
                    size: 20,
                    step: 1
                });
                scheduler.createUnitsView({
                    name: "weekly",
                    property: "section_id",
                    list: self.sections,
                    days: 7,
                    skip_incorrect: true,
                    size: 20,
                    step: 1
                });
                scheduler.init('scheduler_here', self.currentDate, "day");
                scheduler.parse(self.dataList, 'json');
                var calendar = scheduler.renderCalendar({
                    container: "cal_here",
                    date: scheduler._date,
                    navigation: true,
                    handler: function (date) {
                        processSections(date);
                        scheduler.setCurrentView(date);
                        //scheduler.destroyCalendar();
                        //displays the specified view and date
                    }
                });
                scheduler.linkCalendar(calendar);
                self.addedEvents.push(scheduler.attachEvent("onViewChange", function (mode, date) {
                    refreshMinMaxHour();
                    return true;
                }));
                self.addedEvents.push(scheduler.attachEvent("onBeforeDrag", function (id, mode, e) {
                    self.lastDragEventId = id;
                    self.lastDragMode = mode;
                    self.lastDragNativeEvent = e;
                    return true;
                }));
                self.addedEvents.push(scheduler.attachEvent("onDragEnd", function (id, mode, e) {

                    if (self.lastDragEventId && self.lastDragNativeEvent) {
                        var diffX = e.clientX - self.lastDragNativeEvent.clientX;
                        var diffY = e.clientY - self.lastDragNativeEvent.clientY;
                        var factor = 2;
                        if (Math.abs(diffX) > factor || Math.abs(diffY) > factor) {
                            var eventObject = scheduler.getEvent(self.lastDragEventId);
                            if (self.lastDragMode == 'resize' || self.lastDragMode == 'move') {
                                var dataObject = eventObject.dataObject;
                                dataObject.startDate = scheduler.date.convert_to_utc(eventObject.start_date);
                                dataObject.endDate = scheduler.date.convert_to_utc(eventObject.end_date);
                                dataObject.clinicianId = eventObject.section_id;
                                self.updateEvent(dataObject);
                            }
                        }
                    }
                    return true;
                }));
                self.addedEvents.push(scheduler.attachEvent("onClick", function (id, ev) {
                    var dataObject = scheduler.getEvent(id);
                    $scope.showDialog(dataObject, ev.id);
                    return true;
                }));
                self.addedEvents.push(scheduler.attachEvent("onEmptyClick", function (date, e) {
                    var actionData = scheduler.getActionData(e);
                    var selectedCLinician = _.find(self.sections, { key: actionData.section });
                    self.addNewAppointment(date, { id: selectedCLinician.key, name: selectedCLinician.label });
                }));
            }

            function refreshMinMaxHour() {
                var viewName = scheduler.getState().mode;
                var minHour = 24, maxHour = 0;
                var oldScrollHour = scheduler.config.scroll_hour;
                if (minHour < 24 && oldScrollHour != minHour) {
                    scheduler.config.scroll_hour = minHour;
                    if (viewName == "unit")
                    {
                    scheduler.init('scheduler_here', self.currentDate, "unit");
                    }
                    else if (viewName == "weekly") {
                        scheduler.init('scheduler_here', self.currentDate, "weekly");
                    }                   
                }
            }


            function refreshSection(date) {
                scheduler.updateCollection("units", self.sections);  //updates the specified collection with new options units the name of collection to update and the second variable is the new value
            }

            function refreshGrid() {
                scheduler.updateCollection("units", self.sections);   //updates the specified collection with new options
                scheduler.parse(self.dataList, 'json');               //loads data from a client-side resource
            }

            function getCalendarObject(item) {
                var mDate = moment.utc(item.startDate).local();
                var obj = {
                    id: item.appointmentId,
                    start_date: moment.utc(item.startDate).local().format('YYYY-MM-DD HH:mm'),
                    end_date: moment.utc(item.endDate).local().format('YYYY-MM-DD HH:mm'),
                    text: item.patientName,
                    section_id: item.clinicianId,
                    color: item.appointmentTypeColor,
                    dataObject: item
                };
                return obj;
            }

            function updateCalendarObject(item) {
                var eventObj = scheduler.getEvent(item.appointmentId);
                eventObj.text = item.patientName;
                eventObj.section_id = item.clinicianId;
                eventObj.color = item.appointmentTypeColor;
                eventObj.dataObject = item;
                scheduler.updateView();
            }

            function showCurrentViewSession() {
                if (self.clinicianListToDisplay) {
                    self.sections = [];
                    angular.forEach(self.clinicianListToDisplay, function (item) {
                        if (item.isSelected)
                            self.sections.push({ key: parseInt(item.id), label: item.name });
                    });
                }
            }

            function processSections(currentDate)
            {
                self.sections = [];
                angular.forEach(self.clinicianListToDisplay, function (item) {
                    if (isFilterApplied) {
                        if (item.isSelected) {
                            if (!_.isEqual(item.key, 0)) {
                                self.sections.push({ key: parseInt(item.id), label: item.name });
                            }
                        }
                    }
                    else {
                        if (!_.isEqual(item.key, 0)) {
                            self.sections.push({ key: parseInt(item.id), label: item.name });
                        }

                    }
                });

                refreshGrid(currentDate);

            }

            var isFilterApplied = false;
            self.processData = function (data, isFromFilter) {
                self.dataList = [];
                self.sections = [];
                if (data) {
                    angular.forEach(data, function (item) {
                        self.dataList.push(getCalendarObject(item));                               //id, start_date, end_date, text, section_id, color
                    });
                    isFilterApplied = isFromFilter;
                    var date = scheduler.getState().date;
                    processSections(date);
                }
            }
            function updateRequestObject(req) {
                if (self.selectedClinics && self.selectedClinics.length > 0) {
                    req.clinic = "";
                    _.forEach(self.selectedClinics, function (clinic) { req.clinic += clinic.id + ","; });
                    req.clinic = req.clinic.substring(0, (req.clinic.length - 1));
                }
                if (self.clinicianListToDisplay && self.clinicianListToDisplay.length > 0) {
                    req.clinician = "";
                    _.forEach(self.clinicianListToDisplay, function (clinician) { clinician.isSelected ? req.clinician += clinician.id + "," : "" });
                    req.clinician = req.clinician.substring(0, (req.clinician.length - 1));
                }
                if (self.selectedStatus && self.selectedStatus.length > 0) {
                    req.status = "";
                    _.forEach(self.selectedStatus, function (clinic) { req.status += clinic.id + ","; });
                    req.status = req.status.substring(0, (req.status.length - 1));
                }
            }

            self.loadData = function (isFromFilter) {
                var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                var startDate = new Date(y, m, 1);
                var endDate = new Date(y, m + 1, 0);

                var request = {
                    from: startDate,
                    to: endDate,
                };
                updateRequestObject(request);
                self.pageLoading = ScheduleService.loadScheduleData(request).$promise.then(function (result) { self.processData(result, isFromFilter); }, function (error) { });
            }

            initSchedular();
            self.loadData(false);
            var modalInstance = null;
            $scope.showDialog = function (dataObject, eventId) {

                if (modalInstance == null) {
                    console.info("showing dialog:" + eventId)
                    modalInstance = $uibModal.open({
                        animation: true,
                        backdrop: false,
                        templateUrl: 'Client/app/controllers/schedules/detail-modal.html',
                        controller: 'DetailModalCtrl',
                        size: 'lg',
                        keyboard: false,
                        resolve: { data: dataObject }
                    });
                    modalInstance.eventId = eventId;
                    modalInstance.result.then(function (result) {
                        if (result) {
                            updateCalendarObject(result);
                        }
                        modalInstance = null;
                    });
                    modalInstance.rendered.then(function (modal) {
                        //var modal = document.querySelector('.modal');
                        var modalDialog = document.querySelector('.modal-dialog');
                    });
                }
            }

            function loadAllAppointments() {
                self.pageLoading = appointmentTypeService.getAll({ pageIndex: 1, pageSize: 999 }, function (success) {
                    self.appointmentList = success.data;
                }, function () { self.appointmentList = []; });
            }
            loadAllAppointments();

            $scope.$on('$destroy', function () {
                if (self.addedEvents) {
                    angular.forEach(self.addedEvents, function (myEvent) {
                        scheduler.detachEvent(myEvent);
                    });
                }
            });
        }
    ]);

