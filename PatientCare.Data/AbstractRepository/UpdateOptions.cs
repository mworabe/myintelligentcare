﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PatientCare.Data.AbstractRepository
{
    public static class UpdateOptions
    {
        public static UpdateOptions<T> Create<T>()
        {
            return new UpdateOptions<T>();
        }
    }

    public class UpdateOptions<T>
    {
        protected readonly List<Expression<Func<T, object>>> NotModifiedPropertiesInternal = new List<Expression<Func<T, object>>>();

        public IEnumerable<Expression<Func<T, object>>> NotModifiedProperties { get { return NotModifiedPropertiesInternal; } }

        public UpdateOptions<T> WithNotModified(Expression<Func<T, object>> expression)
        {
            if (expression == null) { throw new ArgumentNullException("expression"); }
            NotModifiedPropertiesInternal.Add(expression);
            return this;
        }
    }
}
