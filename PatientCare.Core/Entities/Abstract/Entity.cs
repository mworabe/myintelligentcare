﻿using System;
using System.Diagnostics;

namespace PatientCare.Core.Entities.Abstract
{
    /// <summary>
    /// This class is use as Abstract class .
    /// </summary>
    [DebuggerDisplay("Id = {Id}")]
    public abstract class Entity
    {
        /// <summary>
        /// Description : Entity class Constructor.
        /// </summary>
        protected Entity()
        {
            Modified = DateTime.UtcNow;
        }
      
        public long Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
