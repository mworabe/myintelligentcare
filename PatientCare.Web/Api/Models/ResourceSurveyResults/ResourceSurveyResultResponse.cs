﻿using System;
using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.ResourceSurveyResults
{
    [DataContract]
    public class ResourceSurveyResultResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public PicklistResponse Resource { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Score { get; set; }

        [DataMember]
        public DateTime SurveyDate { get; set; }
    }
}