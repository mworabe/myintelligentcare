"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ClientCompanyCtrl", [
        "ClientCompany",
        function (clientCompanyService) {
            this.get = clientCompanyService.query;
            this.get2 = clientCompanyService.query2;
            this.option = clientCompanyService.getTableOption;
            this.delete = clientCompanyService.delete;
            this.update = clientCompanyService.update;
            this.save = clientCompanyService.save;

            var self = this;

            self.paymentTermsList = self.paymentTermsList || [];

            self.getPaymentterms = function () {

                clientCompanyService.paymentTerms({sortField:"name"}, {}, function (response) {

                    self.paymentTermsList.data

                }, null).$promise.then(function () {
                   
                });
            }

            self.init = function () {
                self.getPaymentterms();
            }

            self.init();
        }

    ]);