﻿using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.HepMasters
{
    [DataContract]
    public class HepGetMobileResponse
    {
        [DataMember]
        public HepDetailMobileResponse HepDetails { get; set; }

        [DataMember]
        public HepGroupMobileResponse[] HepGroups { get; set; }

        [DataMember]
        public HepExerciseMobileResponse[] HepExercises { get; set; }
    }
}