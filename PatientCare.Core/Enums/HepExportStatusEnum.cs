﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title :Hep Export Status Enum.
    /// </summary>
    public enum HepExportStatusEnum
    {
        Export = 0,
        Email = 1,
        Print = 2
    }
}
