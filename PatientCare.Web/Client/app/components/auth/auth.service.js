"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("Auth", [
        "$auth", "$q", "$state", "toaster", "Account", "config", "$http", "SystemConfigurationKeys", "ActivityService", "$rootScope", "SystemFunctions", "$window",
        function ($auth, $q, $state, toaster, account, config, $http, systemConfigurationKeys, activityService, $rootScope, systemFunctions, $window) {

            var self = this;

            var currentUser = $auth.getPayload() || {};
            var TOKEN_KEY = "irms_access_token";
            var rulesNameKey = "irms_user_rules";
            var activityRulesNameKey = "irms_user_rules_activity";
            var projectMenuKey = "PROJECT_MENU_ITEMS";
            var PRODUCTIVITY_CONFIG_KEY = "PRODUCTIVITY_CONFIGURATIONS";
            var currentRules = undefined;
            var currentActivityRules = undefined;
            var projectGanttConfigs = undefined;
            var taskGanttConfigs = undefined;
            var productivityConfigs = undefined;
            var projectMenuItem = undefined;
            var isNDA = false;

            self.getNDA = function getNDA() {
                var ndaValue = localStorage.getItem("IS_NDA")
                debugger;
                if (ndaValue && (ndaValue == 'True' || ndaValue == 'true' || ndaValue == true)) {
                    isNDA = true;
                }
                else
                {
                    isNDA = false;
                }
                
                return isNDA;
            }
            self.setNDA = function setNDA(_isNDA) {
                isNDA = _isNDA;
                localStorage.setItem("IS_NDA", isNDA);
            }


            /**
                Check out this Function is useful for MIC of not.
            */
            function getNavbarMenuForProject() {
                var defer = $q.defer();

                if (!self.isAuthenticated())
                    return defer.resolve();
                activityService.activityAccessibleList({ search: "" }).$promise.then(function (responce) {
                    projectMenuItem = responce;
                    localStorage.setItem(projectMenuKey, angular.toJson(responce));
                    $rootScope.$broadcast("edz:success-login", {});
                });
                return deferred.promise;
            }

            function loadLocaStorageAfterLogin(response) {
                var rules = response.data["user-rules"];
                //var activityRules = rules.activitySections;
                var projectGanttConf = {};
                var taskGanttConf = {};
                var projectGanttConfigs = {};
                var taskGanttConfigs = {};
                var serverConfig = angular.fromJson(response.data['ui-configuration']);

                projectGanttConf = _.find(serverConfig, { key: systemConfigurationKeys.systemConfigs.project.ganttColumnConfig });
                taskGanttConf = _.find(serverConfig, { key: systemConfigurationKeys.systemConfigs.task.ganttColumnConfig });
                projectGanttConfigs = _.find(serverConfig, { key: systemConfigurationKeys.systemConfigs.project.ganttScaleConfig });
                taskGanttConfigs = _.find(serverConfig, { key: systemConfigurationKeys.systemConfigs.task.ganttScaleConfig });
                productivityConfigs = _.find(serverConfig, { key: PRODUCTIVITY_CONFIG_KEY });

                localStorage.setItem(rulesNameKey, rules);
                //localStorage.setItem(activityRulesNameKey, activityRules);
                localStorage.setItem(systemConfigurationKeys.systemConfigs.project.ganttColumnConfig, angular.toJson(projectGanttConf || {}));
                localStorage.setItem(systemConfigurationKeys.systemConfigs.task.ganttColumnConfig, angular.toJson(taskGanttConf || {}));
                localStorage.setItem(systemConfigurationKeys.systemConfigs.project.ganttScaleConfig, angular.toJson(projectGanttConfigs || {}));
                localStorage.setItem(systemConfigurationKeys.systemConfigs.task.ganttScaleConfig, angular.toJson(taskGanttConfigs || {}));
                localStorage.setItem(PRODUCTIVITY_CONFIG_KEY, angular.toJson(productivityConfigs || {}));

                currentRules = angular.fromJson(rules);
                currentActivityRules = (currentRules && currentRules.activitySections) ? currentRules.activitySections : []; //angular.fromJson(activityRules);
                projectGanttConfigs = (projectGanttConf != undefined) ? projectGanttConf : {};
                taskGanttConfigs = (taskGanttConf != undefined) ? taskGanttConf : {};
            }
            function clearLocalStorageAfterLogout() {
                currentUser = {};
                localStorage.removeItem("IS_NDA");
                localStorage.removeItem(rulesNameKey);
                localStorage.removeItem(systemConfigurationKeys.systemConfigs.project.ganttColumnConfig);
                localStorage.removeItem(systemConfigurationKeys.systemConfigs.task.ganttColumnConfig);
                localStorage.removeItem(systemConfigurationKeys.systemConfigs.project.ganttScaleConfig);
                localStorage.removeItem(systemConfigurationKeys.systemConfigs.task.ganttScaleConfig);
                localStorage.removeItem(PRODUCTIVITY_CONFIG_KEY);
                localStorage.removeItem(projectMenuKey);
            }

            this.refreshUserToken = function (token) {
                var defer = $q.defer();
                $auth.login("grant_type=refresh_token" + "&refresh_token=" + token).then(function (res) {
                    loadLocaStorageAfterLogin(res, res.data["user-rules"]);
                    $rootScope.$broadcast('edz:tokenRefresh', res);
                    defer.resolve(res);
                }).catch(function (err) {
                    $rootScope.$broadcast('edz:destroyRefreshToken');
                    self.logout(false);
                    defer.resolve(err);
                });
                return defer.promise;
            }

            this.localLogin = function (login, password) {
                var deferred = $q.defer();
                $auth.login("userName=" + login + "&password=" + password + "&grant_type=password")
                    .then(function (response) {
                        angular.extend(currentUser, $auth.getPayload());
                        currentUser.clinicId = response.data.clinicId;
                        currentUser.clinicLocationId = response.data.clinicLocationId;
                        
                        self.setNDA(response.data.isNDA);
                        
                        loadLocaStorageAfterLogin(response);
                        //edz:success-login
                        $rootScope.$broadcast('edz:success-login');
                        deferred.resolve(response);
                    })
                    .catch(function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;

            };
            this.logout = function () {
                $http.post("api/UserLoginHistorys/Logout", { userid: currentUser.nameid, username: currentUser.unique_name })
                    .then(function (success) {
                        console.info(success);
                        toaster.pop("success", "", "Logoff  successful");
                        $auth.logout().then(function () {
                            clearLocalStorageAfterLogout();
                            $rootScope.$broadcast("edz:success-logout", {});
                            $state.go("login");

                        });
                    }, function (error) { });
                return;
            };

            this.isAuthenticated = function () {
                return $auth.isAuthenticated();
            };

            this.isAdmin = function () {
                return currentUser.role === config.userRoles.ADMIN;
            };

            this.isResource = function () {
                return currentUser.role === config.userRoles.RESOURCE;
            };

            this.getCurrentUser = function () {
                return currentUser;
            };


            this.getRules = function () {
                if (angular.isString(currentRules)) {
                    currentRules = angular.fromJson(localStorage.getItem(rulesNameKey));
                    return currentRules;
                }
                if (currentRules) {
                    return currentRules;
                }

                currentRules = angular.fromJson(localStorage.getItem(rulesNameKey));
                return currentRules;
            };

            this.geActivityRules = function geActivityRules() {
                if (angular.isString(currentRules)) {
                    currentRules = angular.fromJson(localStorage.getItem(rulesNameKey));
                    return currentRules.activitySections;
                }
                if (currentRules) {
                    return currentRules.activitySections;
                }

                currentRules = angular.fromJson(localStorage.getItem(rulesNameKey));
                return currentRules.activitySections;
            };


            this.getProjectGanttConfig = function () {
                if (angular.isString(projectGanttConfigs)) {
                    projectGanttConfigs = angular.fromJson(localStorage.getItem());
                    return projectGanttConfigs;
                }
                if (projectGanttConfigs) {
                    return projectGanttConfigs;
                }
                projectGanttConfigs = angular.fromJson(localStorage.getItem());
                return projectGanttConfigs;
            }
            return this;
        }
    ]);