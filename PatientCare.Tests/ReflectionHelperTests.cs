﻿using System.Runtime.Serialization;
using PatientCare.Utilities;
using NUnit.Framework;

namespace PatientCare.Tests
{
    [TestFixture]
    public static class ReflectionHelperTests
    {
        [Test, TestCaseSource("SimpleClassTestDataSource")]
        public static void Validate_DataContractAttributeResolution(SimpleClassTestData testData)
        {
            string dataContractName;
            var resolutionResult = ReflectionHelper.TryResolvePropertyNameFromDataMemeberAttribute<SimpleClass>(testData.PropertyName, out dataContractName);
            Assert.IsTrue(resolutionResult);
            Assert.AreEqual(dataContractName, testData.ExpectedDataContractName);
        }

        public class NestedClass
        {
            [DataMember(Name = "intprop1")]
            public int IntProperty1 { get; set; }
        }

        public class SimpleClass
        {
            [DataMember(Name = "prop1")]
            public int Property1 { get; set; }

            [DataMember(Name = "prop2")]
            public string Property2 { get; set; }

            [DataMember(Name = "prop3")]
            public SimpleClass Property3 { get; set; }

            [DataMember(Name = "nestedprop1")]
            public NestedClass NestedProperty1 { get; set; }
        }

        public class SimpleClassTestData
        {
            public string ExpectedDataContractName { get; set; }

            public string PropertyName { get; set; }
        }

        private static readonly SimpleClassTestData[] SimpleClassTestDataSource =
        {
            new SimpleClassTestData
            {
                PropertyName = "Property1",
                ExpectedDataContractName = "prop1"
            },
            new SimpleClassTestData
            {
                PropertyName = "Property2",
                ExpectedDataContractName = "prop2"
            },
            new SimpleClassTestData
            {
                PropertyName = "Property3",
                ExpectedDataContractName = "prop3"
            },
            new SimpleClassTestData
            {
                PropertyName = "NestedProperty1.IntProperty1",
                ExpectedDataContractName = "nestedprop1.intprop1"
            }
        };
    }
}
