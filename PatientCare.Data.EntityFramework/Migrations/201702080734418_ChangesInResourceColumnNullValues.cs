namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInResourceColumnNullValues : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.resources", new[] { "business_unit_id" });
            AlterColumn("dbo.resources", "resource_type", c => c.Byte());
            AlterColumn("dbo.resources", "motor_vehicle_report", c => c.Int());
            AlterColumn("dbo.resources", "employment_eligibility_verification", c => c.Int());
            AlterColumn("dbo.resources", "international_work_history", c => c.Int());
            AlterColumn("dbo.resources", "professional_reference_checks", c => c.Int());
            AlterColumn("dbo.resources", "form_i_9", c => c.Int());
            AlterColumn("dbo.resources", "form_e_verify", c => c.Int());
            AlterColumn("dbo.resources", "credential_verifications", c => c.Int());
            AlterColumn("dbo.resources", "business_unit_id", c => c.Long());
            CreateIndex("dbo.resources", "business_unit_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.resources", new[] { "business_unit_id" });
            AlterColumn("dbo.resources", "business_unit_id", c => c.Long(nullable: false));
            AlterColumn("dbo.resources", "credential_verifications", c => c.Int(nullable: false));
            AlterColumn("dbo.resources", "form_e_verify", c => c.Int(nullable: false));
            AlterColumn("dbo.resources", "form_i_9", c => c.Int(nullable: false));
            AlterColumn("dbo.resources", "professional_reference_checks", c => c.Int(nullable: false));
            AlterColumn("dbo.resources", "international_work_history", c => c.Int(nullable: false));
            AlterColumn("dbo.resources", "employment_eligibility_verification", c => c.Int(nullable: false));
            AlterColumn("dbo.resources", "motor_vehicle_report", c => c.Int(nullable: false));
            AlterColumn("dbo.resources", "resource_type", c => c.Byte(nullable: false));
            CreateIndex("dbo.resources", "business_unit_id");
        }
    }
}
