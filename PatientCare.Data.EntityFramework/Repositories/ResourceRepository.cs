﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceRepository : RepositoryBase<Resource>, IResourceRepository
    {
        public ResourceRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {}
    }
}
