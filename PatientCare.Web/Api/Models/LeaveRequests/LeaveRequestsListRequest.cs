﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PatientCare.Web.Api.Models.LeaveRequests
{
    public class LeaveRequestsListRequest : ListRequest
    {
        [DataMember(Name = "resourceId")]
        public long? ResourceId { get; set; }

        //[DataMember(Name = "resourceId")]
        //public long? ResourceId { get; set; }
    }
}