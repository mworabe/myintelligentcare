"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("CompanyConfigurationCtrl", ["$scope", "CompanyConfiguration", "Upload", "$q", "toaster",
        function ($scope, companyConfigurationService, uploader, $q, toaster) {
            var self = this;
            self.callSubmit = false;
            self.companyLogo;
            self.currentTab = "companySettings";

            self.companyConfigurationTabList = [
                {
                    id: "companySettings",
                    name: "Company Settings",
                    required: true,
                    visible: true,
                    tabIcon: "info"
                }, {
                    id: "systemMailSettings",
                    name: "System Mail Settings",
                    required: true,
                    visible: true,
                    tabIcon: "info"
                }
            ];

            self.companyConfigurationModel = {};

            self.saveCompanyConfiguration = function (companyConfigurationForm) {
                if (companyConfigurationForm.$invalid) {
                    return;
                }
                companyConfigurationService.save(self.companyConfigurationModel,
                    function (responce) {
                        // Success Call
                        toaster.pop("success", "", "Successfully Saved Company Configurations.");
                    },
                    function (responce) {
                        toaster.pop("error", "", responce.data.message);
                    }
                );
            }

            function reject() {
                self.callSubmit = false;
            }

            self.loadCompanyData = function () {
                companyConfigurationService.query();
            }

            self.uploadCompanyLogo = function (companyImageLogo) {                
                var deferCompanyLogo = $q.defer();
                companyConfigurationService.uploadFile(uploader, companyImageLogo, 1, function () {
                    deferCompanyLogo.resolve();
                }, reject);
            }

        }
    ]);