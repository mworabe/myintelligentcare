﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("Home", [
        "$resource", "$state", "$filter",
        function ($resource, $state, $filter) {
            var resource = $resource("api/Projects/:id/:controller",
                null, //parameters default
                {
                    'update': { method: "PUT" },
                    'query': { method: "GET", params: {}, isArray: false }
                });

            resource.getTableOption = {
                pageName: "My Projects",
                hideActionButtons: true,
                hideSearch: true,
                cols: function () {
                    return [
                        {
                            field: "name",
                            title: "Project Name",
                            sortable: "Name",
                            show: true,
                            getClasses: function (col) {
                                //isImportant priority_Important 
                                var result = "priority";
                                if (col)
                                    result += " priority_" + col.priority;

                                if (col.isImportant)
                                    result += " priority_Important";

                                return result;
                            }
                        },
                        {
                            field: "status",
                            title: "Status",
                            sortable: "Status",
                            show: true
                        },
                        {
                            field: "startDate",
                            title: "Start Date",
                            sortable: "startDate",
                            show: true,
                            customViewText: function(row) {
                                return $filter("date")(row.startDate, "MM/dd/yyyy");
                            }
                        },
                        {
                            field: "endDate",
                            title: "End Date",
                            sortable: "endDate",
                            show: true,
                            customViewText: function(row) {
                                return $filter("date")(row.endDate, "MM/dd/yyyy");
                            }
                        },
                        {
                            field: "priority",
                            title: "Priority",
                            sortable: "priority",
                            show: true
                        },
                        {
                            field: "description",
                            title: "Description",
                            sortable: "description",
                            show: true
                        },
                        {
                            field: "action",
                            title: "",
                            show: true,
                            dataType: "action",
                            content: "<a href='#'><i class='glyphicon glyphicon-triangle-right'></i></a>",
                            action: this.openProjectTask
                        }
                    ];
                },
                openProjectTask: function(project) {
                    $state.go("projectTask", { projectId: project.id, projectName: project.name });
                }
            };

            return resource;
        }
    ]);