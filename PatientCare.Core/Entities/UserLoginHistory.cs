﻿using PatientCare.Core.Entities.Abstract;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for User Login History.
    /// </summary>
    public class UserLoginHistory : Entity
    {
        public string UserId { get; set; }

        public string Action { get; set; }

        public DateTime? LogDateTime { get; set; }

        public string IPAddress { get; set; }

        public string SessionKey { get; set; }

        public string Username { get; set; }
    }
}
