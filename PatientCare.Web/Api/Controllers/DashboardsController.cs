﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Services.Dashboards;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Dashboards operations.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Dashboards")]
    public sealed class DashboardsController : ApiBaseController
    {
        /// <summary>
        /// Description : Dashboards Field IDashboardService.
        /// </summary>
        private readonly IDashboardService _dashboardService;

        /// <summary>
        /// Description : Initializes a new instance of the Dashboards Class.
        /// </summary>
        /// <param name="dashboardService">IDashboard Service</param>
        public DashboardsController
            (
                IDashboardService dashboardService
            )
        {
            _dashboardService = dashboardService;
        }
        
        #region Resource Charts

        //[HttpGet]
        //[Route("resource-country")]
        //public ProjectTypeViewModel[] ResourceByCountry()
        //{
        //    var result = _dashboardService.ResourceCountry();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-division")]
        //public ProjectTypeViewModel[] ResourceByDivision()
        //{
        //    var result = _dashboardService.ResourceByDivision();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-department")]
        //public ProjectTypeViewModel[] ResourceByDepartment()
        //{
        //    var result = _dashboardService.ResourceByDepartment();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-experience")]
        //public ProjectTypeViewModel[] ResourceByExperienceLevel()
        //{
        //    var result = _dashboardService.ResourceByExperienceLevel();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-job-title")]
        //public ProjectTypeViewModel[] ResourceByJobTitle()
        //{
        //    var result = _dashboardService.ResourceByJobTitle();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-language")]
        //public ProjectTypeViewModel[] ResourceByLanguage()
        //{
        //    var result = _dashboardService.ResourceByLanguage();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-gender")]
        //public ProjectTypeViewModel[] ResourceByGender()
        //{
        //    var result = _dashboardService.ResourceByGender();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-state")]
        //public ProjectTypeViewModel[] ResourceByState()
        //{
        //    var result = _dashboardService.ResourceByState();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-city")]
        //public ProjectTypeViewModel[] ResourceByCity()
        //{
        //    var result = _dashboardService.ResourceByCity();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-location")]
        //public ProjectTypeViewModel[] ResourceByLocation()
        //{
        //    var result = _dashboardService.ResourceByLocation();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-name-of-school")]
        //public ProjectTypeViewModel[] ResourceByEducationNameOfSchool()
        //{
        //    var result = _dashboardService.ResourceByEducationNameOfSchool();
        //    return result.ToArray();
        //}

        //[HttpGet]
        //[Route("resource-rating")]
        //public ProjectTypeViewModel[] ResourceByRating()
        //{
        //    var result = _dashboardService.ResourceByRating();
        //    return result.ToArray();
        //}

        #endregion

        private readonly int[] _defaultYears = { DateTime.UtcNow.Year };
    }
}