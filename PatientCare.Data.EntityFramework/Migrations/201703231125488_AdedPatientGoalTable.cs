namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AdedPatientGoalTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.patient_goals",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    patient_id = c.Long(),
                    patient_case_id = c.Long(),
                    goal_type = c.Int(),
                    goal_name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.patient_cases", t => t.patient_case_id)
                .Index(t => t.patient_id)
                .Index(t => t.patient_case_id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.patient_goals", "patient_case_id", "dbo.patient_cases");
            DropForeignKey("dbo.patient_goals", "patient_id", "dbo.patients");
            DropIndex("dbo.patient_goals", new[] { "patient_case_id" });
            DropIndex("dbo.patient_goals", new[] { "patient_id" });
            DropTable("dbo.patient_goals");
        }
    }
}
