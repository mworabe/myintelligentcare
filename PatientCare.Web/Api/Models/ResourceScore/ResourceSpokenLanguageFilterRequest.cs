﻿namespace PatientCare.Web.Api.Models.ResourceScore
{
    public class ResourceSpokenLanguageFilterRequest
    {
        public long LanguageId { get; set; }

        public double SpeakingProficiency { get; set; }

        public double WritingProficiency { get; set; }
    }
}