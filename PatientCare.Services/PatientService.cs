﻿using System;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using System.Text;
using System.Data.Entity;
using System.Data;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle patient operations And implementing all methods which is initialize in IPatientService.
    /// </summary>
    public class PatientService : IPatientService
    {
        /// <summary>
        ///Description : Patient Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Patient Service Field IPatientRepository
        /// </summary>
        private readonly IPatientRepository _patientRepository;

        /// <summary>
        ///Description : Patient Service Field IPatientAddressRepository
        /// </summary>
        private readonly IPatientAddressRepository _patientAddressRepository;

        /// <summary>
        ///Description : Patient Service Field IPatientParentGuardianRepository
        /// </summary>
        private readonly IPatientParentGuardianRepository _patientParentGuardianRepository;

        /// <summary>
        ///Description : Patient Service Field IPatientCaseRepository
        /// </summary>
        private readonly IPatientCaseRepository _PatientCaseRepository;

        /// <summary>
        ///Description : Patient Service Field IAppointmentTypeRepository
        /// </summary>
        private readonly IAppointmentTypeRepository _appointmentTypeRepository;

        /// <summary>
        ///Description : Patient Service Field IPatientAppointmentRepository
        /// </summary>
        private readonly IPatientAppointmentRepository _patientAppointmentRepository;

        /// <summary>
        ///Description : Patient Service Field IPatientWorkoutAnalyticRepository
        /// </summary>
        private readonly IPatientWorkoutAnalyticRepository _patientWorkoutAnalyticRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Patient class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="patientRepository">Patient Repository</param>
        /// <param name="patientAddressRepository">Patient Address Repository</param>
        /// <param name="patientParentGuardianRepository">Patient Parent Guardian Repository</param>
        /// <param name="PatientCaseRepository">Patient Case Repository</param>
        /// <param name="appointmentTypeRepository">Appointment Type Repository</param>
        /// <param name="patientAppointmentRepository">Patient Appointment Repository</param>
        /// <param name="patientWorkoutAnalyticRepository">Patient Workout Analytic Repository</param>
        public PatientService
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IPatientRepository patientRepository,
                IPatientAddressRepository patientAddressRepository,
                IPatientParentGuardianRepository patientParentGuardianRepository,
                IPatientCaseRepository PatientCaseRepository,
                IAppointmentTypeRepository appointmentTypeRepository,
                IPatientAppointmentRepository patientAppointmentRepository,
                IPatientWorkoutAnalyticRepository patientWorkoutAnalyticRepository
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _patientRepository = patientRepository;
            _patientAddressRepository = patientAddressRepository;
            _patientParentGuardianRepository = patientParentGuardianRepository;
            _PatientCaseRepository = PatientCaseRepository;
            _appointmentTypeRepository = appointmentTypeRepository;
            _patientAppointmentRepository = patientAppointmentRepository;
            _patientWorkoutAnalyticRepository = patientWorkoutAnalyticRepository;
        }

        #region Patients Methods

        /// <summary>
        /// Method Description : Using for Update patient data.
        /// </summary>
        /// <param name="patient">Patient Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdatePatientAsync(Patient patient)
        {
            if (patient == null) throw new ArgumentNullException("patients");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                PatientChidlrenProcessing(ctx, patient);

                var originPatient = _patientRepository.Read(patient.Id, new ReadOptions<Patient>().AsReadOnly());

                await _patientRepository.UpdateAsync(patient, UpdateOptions.Create<Patient>()
                .WithNotModified(p => p.Created));
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Update patient Addresses and Gaurdian.
        /// </summary>
        /// <param name="ctx">DbContext</param>
        /// <param name="patient">Patient Entity</param>
        private static void PatientChidlrenProcessing(DbContext ctx, Patient patient)
        {
            var spokenLanguages = patient.Addresses;
            ctx.ChildrenProcessing(spokenLanguages, x => x.PatientId == patient.Id);

            var educations = patient.ParentGaurdians;
            ctx.ChildrenProcessing(educations, x => x.PatientId == patient.Id);
        }

        /// <summary>
        /// Method Description : Using for Create new patient.
        /// </summary>
        /// <param name="patient">Patient Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreatePatientAsync(Patient patient)
        {
            if (patient == null) throw new ArgumentNullException("patients");

            await _patientRepository.CreateAsync(patient, UpdateOptions.Create<Patient>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Patient by Id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patient Entity</returns>
        public async Task<Patient> ReadPatientAsync(long id)
        {
            var readOptions = new ReadOptions<Patient>().AsReadOnly();
            var patient = await _patientRepository.ReadAsync(id, readOptions);
            return patient;
        }

        /// <summary>
        /// Method Description : Using for delete Patient. 
        /// </summary>
        /// <param name="patient">Patient Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeletePatientAsync(Patient patient)
        {
            // here we are not delete patient but we are keep entry is deleted.
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                patient.IsDeleted = true;

                var originPatient = _patientRepository.Read(patient.Id, new ReadOptions<Patient>().AsReadOnly());

                await _patientRepository.UpdateAsync(patient, UpdateOptions.Create<Patient>()
                    .WithNotModified(p => p.Created));

                return true;
            }
        }

        /// <summary>
        /// Method Description : Using for Update Patient Number/Id.  
        /// </summary>
        /// <param name="patientId">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdatePatientNoAsync(long patientId)
        {
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                int result;
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "declare @resturnData as int;EXEC @resturnData= sp_patient_no_generate @patientId = " + patientId + "; select @resturnData;";
                result = await ctx.Database.SqlQuery<int>(query).FirstOrDefaultAsync();

                if (result != 0)
                    return false;
            }
            return true;
        }
        #endregion

        #region Patients Cases Methods

        /// <summary>
        /// Method Description : Using for Update Patient Case data.
        /// </summary>
        /// <param name="PatientCase">Patient Case Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdatePatientCaseAsync(PatientCase PatientCase)
        {
            if (PatientCase == null) throw new ArgumentNullException("PatientCases");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originPatient = _PatientCaseRepository.Read(PatientCase.Id, new ReadOptions<PatientCase>().AsReadOnly());

                await _PatientCaseRepository.UpdateAsync(PatientCase, UpdateOptions.Create<PatientCase>()
                .WithNotModified(p => p.Created));
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new Patient Case.
        /// </summary>
        /// <param name="PatientCase">Patient Case Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreatePatientCaseAsync(PatientCase PatientCase)
        {
            if (PatientCase == null) throw new ArgumentNullException("PatientCases");

            await _PatientCaseRepository.CreateAsync(PatientCase, UpdateOptions.Create<PatientCase>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Patient Case by Id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patient Case Entity</returns>
        public async Task<PatientCase> ReadPatientCaseAsync(long id)
        {
            var readOptions = new ReadOptions<PatientCase>().AsReadOnly();

            var patientcase = await _PatientCaseRepository.ReadAsync(id, readOptions);

            return patientcase;

        }

        /// <summary>
        /// Method Description : Using for Delete Patient Case data. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeletePatientCaseAsync(long id)
        {
            var patientcase = _PatientCaseRepository.Read(id);

            // here we are not delete patient Case but we are keep entry is deleted.
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                patientcase.IsDeleted = true;

                var originPatient = _PatientCaseRepository.Read(patientcase.Id, new ReadOptions<PatientCase>().AsReadOnly());

                await _PatientCaseRepository.UpdateAsync(patientcase, UpdateOptions.Create<PatientCase>()
                    .WithNotModified(p => p.Created));

                return true;
            }
        }

        /// <summary>
        /// Method Description : Using for Update Patient Case number.  
        /// </summary>
        /// <param name="patientId">Patient Id</param>
        /// <param name="caseId">Case Id</param>
        /// <returns></returns>
        public async Task<bool> UpdatePatientCaseNoAsync(long? patientId, long? caseId)
        {
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                int result;
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "declare @resturnData as int;EXEC @resturnData= sp_patient_case_no_generate @patientId = " + patientId + ",@caseId= " + caseId + "; select @resturnData;";
                result = await ctx.Database.SqlQuery<int>(query).FirstOrDefaultAsync();

                if (result != 0)
                    return false;
            }
            return true;
        }
        #endregion

        #region Appointment Type

        /// <summary>
        /// Method Description : Using for Update Appointment Type data.
        /// </summary>
        /// <param name="appointmenttype">Appointment Type Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateApointmentTypeAsync(AppointmentType appointmenttype)
        {
            if (appointmenttype == null) throw new ArgumentNullException("appointmenttype");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originPatient = _PatientCaseRepository.Read(appointmenttype.Id, new ReadOptions<PatientCase>().AsReadOnly());

                await _appointmentTypeRepository.UpdateAsync(appointmenttype, UpdateOptions.Create<AppointmentType>()
                .WithNotModified(p => p.Created));
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Insert Appointment Type data.
        /// </summary>
        /// <param name="appointmenttype">Appointment Type Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateApointmentTypeAsync(AppointmentType appointmenttype)
        {
            if (appointmenttype == null) throw new ArgumentNullException("appointmenttype");

            await _appointmentTypeRepository.CreateAsync(appointmenttype, UpdateOptions.Create<AppointmentType>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for Read Appointment Type by id. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Appointment Type Entity</returns>
        public async Task<AppointmentType> ReadApointmentTypeAsync(long id)
        {
            var readOptions = new ReadOptions<AppointmentType>().AsReadOnly();

            var appointmenttype = await _appointmentTypeRepository.ReadAsync(id, readOptions);

            return appointmenttype;

        }


        /// <summary>
        /// Method Description : Using for Delete Appointment Type data. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteApointmentTypeAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var appointmenttype = _appointmentTypeRepository.Read(id);

                if (appointmenttype != null)
                {
                    _appointmentTypeRepository.Delete(appointmenttype);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Patient Appointment

        /// <summary>
        /// Method Description : Using for Update Appointment.
        /// </summary>
        /// <param name="patientappointment">Patient Appointment Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdatePatientApointmentAsync(PatientAppointment patientappointment)
        {
            if (patientappointment == null) throw new ArgumentNullException("patientappointment");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                var originPatient = _patientAppointmentRepository.Read(patientappointment.Id, new ReadOptions<PatientAppointment>().AsReadOnly());

                _patientAppointmentRepository.Update(patientappointment, UpdateOptions.Create<PatientAppointment>()
               .WithNotModified(p => p.Created));
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new Appointment.
        /// </summary>
        /// <param name="patientappointment">Patient Appointment Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreatePatientApointmentAsync(PatientAppointment patientappointment)
        {
            if (patientappointment == null) throw new ArgumentNullException("appointmenttype");

            await _patientAppointmentRepository.CreateAsync(patientappointment, UpdateOptions.Create<PatientAppointment>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for Read Appointment by id. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Appointment Entity</returns>
        public async Task<PatientAppointment> ReadPatientApointmentAsync(long id)
        {
            var readOptions = new ReadOptions<PatientAppointment>().AsReadOnly();

            var patientappointment = await _patientAppointmentRepository.ReadAsync(id, readOptions);

            return patientappointment;

        }

        /// <summary>
        /// Method Description : Using for Delete Appointment data. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeletePatientApointmentAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var patientappointment = _patientAppointmentRepository.Read(id);

                if (patientappointment != null)
                {
                    _patientAppointmentRepository.Delete(patientappointment);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }


        #endregion


        #region Patient Workout Analytic

        /// <summary>
        /// Method Description : Using for Create / Update Patient workout analytic.
        /// </summary>
        /// <param name="patientWorkoutAnalytic">Patient workout analytic Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> InsertUpdatePatientWorkoutAnalyticAsync(PatientWorkoutAnalytic patientWorkoutAnalytic)
        {
            if (patientWorkoutAnalytic == null) throw new ArgumentNullException("patientWorkoutAnalytics");
            if (patientWorkoutAnalytic.Id == 0)
            {
                await _patientWorkoutAnalyticRepository.CreateAsync(patientWorkoutAnalytic, UpdateOptions.Create<PatientWorkoutAnalytic>()
                    .WithNotModified(p => p.Created)
                    .WithNotModified(p => p.Modified));
            }
            else
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var originPatientWorkoutAnalytic = _patientWorkoutAnalyticRepository.Read(patientWorkoutAnalytic.Id, new ReadOptions<PatientWorkoutAnalytic>().AsReadOnly());
                    await _patientWorkoutAnalyticRepository.UpdateAsync(patientWorkoutAnalytic, UpdateOptions.Create<PatientWorkoutAnalytic>()
                        .WithNotModified(p => p.Created));
                }
            }

            return true;
        }

        /// <summary>
        /// Method Description : Using for Read Patient workout analytic by id. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patient Workout Analytic Entity</returns>
        public async Task<PatientWorkoutAnalytic> ReadPatientWorkoutAnalyticAsync(long id)
        {
            var readOptions = new ReadOptions<PatientWorkoutAnalytic>().AsReadOnly();

            var patientWorkout = await _patientWorkoutAnalyticRepository.ReadAsync(id, readOptions);

            return patientWorkout;

        }

        #endregion
    }
}
