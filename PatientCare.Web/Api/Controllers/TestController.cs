﻿using System;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    public class Test_V1Controller : ApiController
    {
        //[Route("api/v1/values/{id:int}")]
        [Route("api/mobile/v1/login/{id:int}")]
        public string Get(int id)
        {
            return string.Format("value {0} (V1)", id);
        }

        [Route("api/mobile/v2/login/{id:int}")]
        public string Get2(int id)
        {
            return string.Format("value {0} (V2)", id);
        }
    }

    public class Test_V2Controller : ApiController
    {
        //[Route("api/v2/values/{id:int}")]
        //[Route("api/mobile/v2/login/{id:int}")]
        //public string Get(int id)
        //{
        //    return string.Format("value {0} (V2)", id);
        //}
    }
}
