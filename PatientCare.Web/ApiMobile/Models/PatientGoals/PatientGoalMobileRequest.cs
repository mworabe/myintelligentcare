﻿using PatientCare.Core.Enums;

namespace PatientCare.Web.ApiMobile.Models.PatientGoals
{
    public class PatientGoalMobileRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public long? PatientCaseId { get; set; }

        public long? PatientMobileProfileId { get; set; }

        public PatientGoalTypeEnum? GoalType { get; set; }

        public string GoalName { get; set; }
    }   
}