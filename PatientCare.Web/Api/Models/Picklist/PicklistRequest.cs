﻿using System.Runtime.Serialization;
using FluentValidation.Attributes;

namespace PatientCare.Web.Api.Models.Picklist
{
    [DataContract]
    public class PicklistRequest
    {
        [DataMember]
        public string Name { get; set; }        
    }
}