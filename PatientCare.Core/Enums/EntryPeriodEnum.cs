﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Billing Period Type Enum.
    /// </summary>
    public enum EntryPeriodEnum
    {
        Daily = 0,
        Weekly = 1,
        BiMonthly = 2,
        Monthly = 3
    }
}
