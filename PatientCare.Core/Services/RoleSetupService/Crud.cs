﻿using System;

namespace PatientCare.Core.Services.RoleSetupService
{
    public enum Crud
    {
        None = 0,
        Create = 1,
        Read = 2,
        Update = 3,
        Delete = 4
    }
}
