﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider.state("adminConfigData", {
            url: "/admin-config-data",
            params: { currentTab: null },
            templateUrl: "Client/app/controllers/admin-configuration/admin-configuration-view.html",
            controller: "AdminConfigurationViewController as AdminConfigurationViewController",
            data: {
                security: {
                    authenticated: true
                }
            }
        });
    });