﻿using System;
using EmitMapper;
using PatientCare.Services.Mapping;
using PatientCare.Core.Entities;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.PatientAppointment;

namespace PatientCare.Web.Services.Mapping
{
    public static class PatientAppointmentMapperExtensions
    {
        public static EmitMapperMapperProvider PatientAppointmentRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment, PatientAppointmentResponse>
                    (new ExtendedMapConfig<PatientAppointment, PatientAppointmentResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment[], PatientAppointmentResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment, PatientAppointmentListResponse>
                    (new ExtendedMapConfig<PatientAppointment, PatientAppointmentListResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment[], PatientAppointmentListResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment, PicklistResponse>
                    (new ExtendedMapConfig<PatientAppointment, PicklistResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointment, PicklistDefaultResponse>
                    (new ExtendedMapConfig<PatientAppointment, PicklistDefaultResponse>()));

            mapperProvider.RegisterMapper(() =>
              ObjectMapperManager.DefaultInstance.GetMapper<PatientAppointmentRequest, PatientAppointment>(
                  new ExtendedMapConfig<PatientAppointmentRequest, PatientAppointment>()));

            return mapperProvider;
        }
    }
}