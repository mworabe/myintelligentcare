﻿using System.Data;
using PatientCare.Data.AbstractDataContext;

namespace PatientCare.Data.EntityFramework.DataContext
{
    public class DataContextReadOnlyScope : IDataContextReadOnlyScope
    {
        private readonly DataContextScope _internalScope;

        public IDataContextCollection DbContexts { get { return _internalScope.DataContexts; } }

        public DataContextReadOnlyScope(IDataContextFactory dbContextFactory = null)
            : this(creationOptions: DataContextCreationOptions.JoinExisting, isolationLevel: null, dbContextFactory: dbContextFactory)
        { }

        public DataContextReadOnlyScope(IsolationLevel isolationLevel, IDataContextFactory dbContextFactory = null)
            : this(creationOptions: DataContextCreationOptions.ForceCreateNew, isolationLevel: isolationLevel, dbContextFactory: dbContextFactory)
        { }

        public DataContextReadOnlyScope(DataContextCreationOptions creationOptions, IsolationLevel? isolationLevel, IDataContextFactory dbContextFactory = null)
        {
            _internalScope = new DataContextScope(creationOptions: creationOptions, readOnly: true, isolationLevel: isolationLevel, dataContextFactory: dbContextFactory);
        }

        public void Dispose()
        {
            _internalScope.Dispose();
        }
    }
}
