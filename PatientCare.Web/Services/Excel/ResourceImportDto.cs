﻿using System;
using PatientCare.Core.Enums;

namespace PatientCare.Web.Services.Excel
{
    public class ResourceImportDto
    {
        public bool NeedChangeExisting { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Address2 { get; set; }

        public string ZipCode { get; set; }

        public string StateProvince { get; set; }

        public int? YearsEmployed { get; set; }

        public string GradeLevel { get; set; }

        public int YearsOfCollege { get; set; }

        public bool CollegeDegree { get; set; }

        public string TimeZoneInfo { get; set; }

        public string Location { get; set; }

        public bool ValidPassport { get; set; }

        public bool AvailableToTravel { get; set; }

        public bool? WorksWellAlone { get; set; }

        public bool? WorksWellInTeam { get; set; }

        public bool EverBeenConvictedOfACrime { get; set; }

        public bool InformationCertifiedByEmployer { get; set; }

        public string Ethnicity { get; set; }

        public bool ArmedForces { get; set; }

        public string ArmedForcesBranch { get; set; }

        public string DrugTest { get; set; }

        public string CreditReportOk { get; set; }

        public string ProofOfCitizenship { get; set; }

        public decimal HourlyRate { get; set; }

        public decimal? Salary { get; set; }

        public decimal? BonusOrOtherPay { get; set; }

        public string Ssn { get; set; }

        public string EmployeeId { get; set; }

        public double? Productivity { get; set; }

        public double? Rating { get; set; }

        public RangeTypeEnum Range { get; set; }

        public ResourceTypeEnum ResourceType { get; set; }

        public EmploymentTypeEnum EmploymentType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? BirthDate { get; set; }

        public GenderEnum? Gender { get; set; }

        public RelationshipEnum? Relationship { get; set; }

        public MartialStatusEnum? MartialStatus { get; set; }

        public string PhoneNumber { get; set; }

        public string PhoneNumber2 { get; set; }

        public string EmergencyContactName { get; set; }

        public string EmergencyContactPhoneNumber { get; set; }
        
        public string PhotoFileName { get; set; }

        public string CvFileName { get; set; }

        public YesNoOtherEnum MotorVehicleReport { get; set; }

        public string MotorVehicleReportOther { get; set; }

        public YesNoOtherEnum ProfessionalReferenceChecks { get; set; }

        public string ProfessionalReferenceChecksOther { get; set; }

        public YesNoOtherEnum EmploymentEligibilityVerification { get; set; }

        public string EmploymentEligibilityVerificationOther { get; set; }

        public YesNoOtherEnum InternationalWorkHistory { get; set; }

        public string InternationalWorkHistoryOther { get; set; }

        public YesNoOtherEnum CredentialVerifications { get; set; }

        public string CredentialVerificationsOther { get; set; }

        public YesNoOtherEnum FormI9 { get; set; }

        public string FormI9Other { get; set; }

        public YesNoOtherEnum FormEVerify { get; set; }

        public string FormEVerifyOther { get; set; }

        public string Country { get; set; }

        public string ArmedForcesCountry { get; set; }

        public string JobTitle { get; set; }

        public string ResourceCompany { get; set; }

        public string ResourceTeam { get; set; }

        public string Division { get; set; }

        public string Department { get; set; }

        public string Supervisor { get; set; }

        public bool? Active { get; set; }
    }
}