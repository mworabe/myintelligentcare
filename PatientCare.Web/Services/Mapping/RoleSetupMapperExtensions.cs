﻿using System;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.RoleSetup;
using PatientCare.Web.Services.Convertation;
using EmitMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace PatientCare.Web.Services.Mapping
{
    public static class RoleSetupMapperExtensions
    {
        public static EmitMapperMapperProvider RoleSetupRegisterMappings(this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ApplicationRole, RoleSetupResponse>
                (new ExtendedMapConfig<ApplicationRole, RoleSetupResponse>()
                    .ForMember(x => x.IsActive, source => (source.IsActive == true) ? true : false)
                    .ForMember(x => x.IsActiveYesNo, y => (y.IsActive == true) ? "Yes" : "No")
                    .ForMember(member => member.AccessRights, source => string.IsNullOrEmpty(source.JsonSetup) ? null : JsonConvert.DeserializeObject<AccessRights>(source.JsonSetup))
                    ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ApplicationRole[], RoleSetupResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<RoleSetupRequest, ApplicationRole>(new ExtendedMapConfig<RoleSetupRequest, ApplicationRole>().
                ForMember(x => x.JsonSetup, source => source.AccessRights == null ? null : JsonConverterHelper.SerializeObject(source.AccessRights))));

            return mapperProvider;
        }
    }
}