﻿using System;

namespace PatientCare.Web.ApiMobile.Models.PatientWorkoutAnalytics
{
    public class PatientWorkoutAnalyticRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public long? CaseId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? PreExercisesPainRating { get; set; }

        public int? WorkoutFeelResult { get; set; }

        public string WorkoutComment { get; set; }
    }   
}