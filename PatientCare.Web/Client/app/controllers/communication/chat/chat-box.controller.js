﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("ChatBoxCntrl", [
        "$scope",
        'TwilioChatFactory',
        'toaster',
        '$stateParams',
        '$state',
        'SoundFactory',
        '$q',
        'VirgilFactory',
        'Auth',
        '$rootScope',
        'ChatService',
        function ($scope, TwilioChatFactory, toaster, $stateParams, $state, SoundFactory, $q, VirgilFactory, Auth, $rootScope, ChatService) {

            $scope.message = "";
            $scope.messageList = [];

            $scope.session = $stateParams.sessionObject;

            var client = TwilioChatFactory.getClient();
            var virgilClient = VirgilFactory.getClient();
            
            //var virgilClient = virgil.API("AT.a09fa5ebd76edcd2cdb496d28178a18cc0b2dce2a55bfff6c0bdd1cfd8a8d191");
            var currentUser = Auth.getCurrentUser();
            var currentChannel = null;

            var otherUserCard;
            var currentUserCard;
            
            if (client && $scope.session) {
                $scope.isChatLoading = ChatService.getUserKeys({ fromUserId: currentUser.nameid, toUserId: $scope.session.otherUser.userId }, function (result) {
                    if (result) {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].userId == currentUser.nameid) {
                                currentUser.privateKey = result[i].privateKey;
                            } else if (result[i].userId == $scope.session.otherUser.userId) {
                                $scope.session.otherUser.privateKey = result[i].privateKey;
                            }
                        }
                        loadCards();
                    }
                }, function (error) {
                })
            }


            function loadCards() {
                currentUserCard = VirgilFactory.getCurrentUserCard();

                var deferred = $q.defer();
                $scope.isChatLoading = deferred.promise;

                virgilClient.cards.find([$scope.session.otherUser.userId])
                  .then(function (userCard) {
                      deferred.resolve("");
                      otherUserCard = userCard[0];
                      initChat();
                  });
                
            }

            function initChat() {
                $scope.chatCurrentUser = client.userInfos;
                var deferred = $q.defer();
                $scope.isChatLoading = deferred.promise;

                

                client.getChannelBySid($scope.session.channelsId).then(function (channel) {
                    currentChannel = channel;
                    $rootScope.currentChannelId = currentChannel.sid;
                    deferred.resolve("");
                    setUpChannel();
                }, function (error) {
                    deferred.reject("");
                    toaster.pop("error", "", error.body.message);
                });
            }


            //function inviteOtherUser() {
            //    debugger;
            //     currentChannel.invite($scope.session.otherUser.userId).then(function () {
            //        console.log('Your friend has been invited!');

            //    }, function (error) {
            //        debugger;
            //    });
            //}

            function setUpChannel() {
                var deferred = $q.defer();
                $scope.isChatLoading = deferred.promise;

                if (currentChannel != null) {
                    if (currentChannel.status !== 'joined') {
                        currentChannel.join().then(function (channel) {
                            currentChannel = channel;
                            deferred.resolve("");
                            setUpChannel();
                        });
                        return;
                    }
                    //inviteOtherUser();
                    currentChannel.on("messageAdded", function (message) {
                        $scope.messageList.push(parseMessage(message));
                        $scope.$apply();
                        SoundFactory.playMsgReceivedSound();
                        scrollToBottom(800);
                    });
                    deferred.resolve("");
                    loadMessages();
                } else {
                    deferred.resolve("");
                }
            }

           

            function scrollToBottom(delay) {
                var mydiv = $(".chat-msg-list-container");
                var scrollPos = mydiv.prop("scrollHeight");
                mydiv.animate({
                    scrollTop: scrollPos
                }, delay);

            }

            function loadMessages() {
                var deferred = $q.defer();
                $scope.isChatLoading = deferred.promise;
                if (currentChannel) {
                    currentChannel.getMessages(50).then(function (page) {
                        $scope.messageList = [];
                        if (page.items) {
                            for (var i = 0; i < page.items.length; i++) {
                                $scope.messageList.push(parseMessage(page.items[i]));
                            }

                            deferred.resolve("");
                            $scope.$apply();
                            scrollToBottom(0);
                        }
                    }, function (error) {
                        deferred.reject(error);
                    })
                } else {
                    deferred.resolve("");
                }

            }

            function parseMessage(message) {
                if (message) {
                    try {
                        var decryptedMessage = "";
                        if (message.author == currentUser.nameid) {
                            decryptedMessage = VirgilFactory.decryptData($scope.session.otherUser.privateKey, message.body);
                        } else {
                            decryptedMessage = VirgilFactory.decryptData(currentUser.privateKey, message.body);
                        }
                        var obj = {
                            author: message.author,
                            body: decryptedMessage,
                            timestamp: message.timestamp,
                        }
                        return obj;
                    } catch (error) {
                        console.info(error)
                    }
                    
                }
            }

            $scope.sendMessage = function () {
                if (!$scope.message)
                    return;

                if (currentChannel != null) {
                    var cypherText = VirgilFactory.encryptData($scope.message, otherUserCard);
                    currentChannel.sendMessage(cypherText).then(function (data) {

                    });
                }
                $scope.message = "";
            }

            $scope.$on('$destroy', function iVeBeenDismissed() {
                $rootScope.currentChannelId = null;
            })





        }]);