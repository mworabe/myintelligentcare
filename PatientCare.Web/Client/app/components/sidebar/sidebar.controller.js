﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("SidebarCtrl", [
        "$scope", "$state", function($scope, $state) {
            $scope.menu = [
                {
                    'title': "Dashboard",
                    'state': "dashboard"
                },
                {
                    'title': "Client Company",
                    'state': "clientCompany"
                }, {
                    'title': "Client Project",
                    'state': "project"
                }, {
                    'title': "Language",
                    'state': "language"
                }, {
                    'title': "Position",
                    'state': "position"
                }, {
                    'title': "Programming Language",
                    'state': "skill"
                }, {
                    'title': "Project",
                    'state': "project"
                }, {
                    'title': "Project Assumption",
                    'state': "projectAssumption"
                }, {
                    'title': "Project Consulting Cost",
                    'state': "projectConsultingCost"
                }, {
                    'title': "Project Dependency",
                    'state': "projectDependency"
                }, {
                    'title': "Project Issue",
                    'state': "projectIssue"
                }, {
                    'title': "Project Risk",
                    'state': "projectRisk"
                }, {
                    'title': "Project Stakeholder",
                    'state': "projectStakeholder"
                }, {
                    'title': "Project StakeHolder Role",
                    'state': "projectStakeHolderRole"
                }, {
                    'title': "Project Type",
                    'state': "projectType"
                }, {
                    'title': "Resource Company",
                    'state': "resourceCompany"
                }, {
                    'title': "Survey Category",
                    'state': "surveyCategory"
                }, {
                    'title': "Survey Question",
                    'state': "surveyQuestion"
                }, {
                    'title': "Roles availability VS demand",
                    'state': "rolesAvailabilityVsDemand"
                }
            ];

            $scope.isCollapsed = true;

            $scope.isActive = function(state) {
                return state === $state.current.name;
            };
        }
    ]);