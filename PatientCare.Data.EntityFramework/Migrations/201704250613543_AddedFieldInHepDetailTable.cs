namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldInHepDetailTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hep_detail", "is_active_frequency", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.hep_detail", "is_active_frequency");
        }
    }
}
