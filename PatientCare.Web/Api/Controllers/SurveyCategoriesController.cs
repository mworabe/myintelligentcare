﻿using System;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Survey Categories operations.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/SurveyCategories")]
    public sealed class SurveyCategoriesController : EdzPicklistApiController<SurveyCategory>
    {
        /// <summary>
        /// Description : In this class we pass Survey Categories IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="picklistRepository">IPicklist Repository SurveyCategory</param>
        public SurveyCategoriesController(IPicklistRepository<SurveyCategory> picklistRepository)
            : base(picklistRepository)
        {
            GetPicklistStatusCodeErrorFunc = () => GetPicklistStatusCodeError(Picklist.SurveyCategories);
        }

        protected override Uri GetPicklistLocation(long id)
        {
            return new Uri("/SurveyCategories/" + id, UriKind.Relative);
        }
    }
}