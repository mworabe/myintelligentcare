﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ExerciseActivities
{
    [DataContract]
    public class ExerciseActivityAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}