﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("appMultiSelect",
        function () {
            return {
                template: "<div ng-dropdown-multiselect=\"\" " +
                    "options=\"items\" " +
                    "selected-model=\"model\" " +
                    "extra-settings=\"settings\"  " +
                    "events=\"eventConfigs\"  " +
                    "translation-texts=\"textSettings\" ></div>",
                restrict: "EA",


                scope: {
                    model: "=?data",
                    items: "=items",
                    config: "=config",
                    displayProp: "@",
                    placeHolder: "@",
                    onChange: "&",
                    onItemDeselect: "&",
                },
                controller: ['$scope', function ($scope) {

                    if (!$scope.model) {
                        $scope.model = [];
                    }
                    $scope.textSettings = { buttonDefaultText: "Select" };

                    if (!$scope.config) {
                        $scope.settings = { smartButtonMaxItems: 3, checkBoxes: true }
                    } else {
                        $scope.settings = $scope.config;
                    }


                    if ($scope.displayProp) {
                        $scope.settings.displayProp = $scope.displayProp;
                    }
                    if ($scope.placeHolder) {
                        $scope.textSettings.buttonDefaultText = $scope.placeHolder;
                    }

                    $scope.onSelectionChange = function (item) {
                        if ($scope.onChange) {
                            $scope.onChange({ item: item });
                        }
                    }

                    $scope.eventConfigs = {
                        onItemSelect: $scope.onSelectionChange,
                        onItemDeselect: $scope.onItemDeselect,
                    }

                }]

            };
        }
   );