﻿using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Chat Session.
    /// </summary>
    public class ChatSession : Entity
    {
        #region Chat Session

        public string ChannelsId { get; set; }

        public string ChannelUniqueName { get; set; }

        public string ChannelName { get; set; }

        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public string CreatedByUserId { get; set; }

        public string LastMessage { get; set; }

        public int? Type { get; set; }

        //public DateTime? CreatedDates { get; set; }

        public virtual ICollection<ChatMember> ChatMembers { get; set; }

        #endregion

    }
}
