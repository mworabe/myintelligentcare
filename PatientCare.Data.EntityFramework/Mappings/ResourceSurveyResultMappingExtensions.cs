﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceSurveyResultMappingExtensions
    {
        public static DbModelBuilder MapResourceSurveyResult(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceSurveyResult>().ToTable("resource_survey_results").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceSurveyResult>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsUnicode()
                .HasMaxLength(255)
                .IsRequired();

            modelBuilder.Entity<ResourceSurveyResult>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceSurveyResult>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceSurveyResult>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsRequired();

            modelBuilder.Entity<ResourceSurveyResult>()
                .HasRequired<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceSurveyResult>()
                .Property(p => p.Score)
                .HasColumnName("score")
                .IsRequired();

            modelBuilder.Entity<ResourceSurveyResult>()
                .Property(p => p.SurveyDate)
                .HasColumnName("survey_date")
                .IsRequired();

            return modelBuilder;
        }
    }
}
