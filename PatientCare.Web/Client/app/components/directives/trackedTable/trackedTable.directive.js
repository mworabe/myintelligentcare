﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("trackedTable", [function () {
            return {
                restrict: "A",
                priority: -1,
                require: "ngForm",
                controller: ['$scope', '$parse', '$attrs', '$element', function ($scope, $parse, $attrs, $element) {
                    var self = this;
                    //var tableForm = $element.controller("form");
                    var dirtyCellsByRow = [];
                    var invalidCellsByRow = [];

                    function init() {
                        var setter = $parse($attrs.trackedTable).assign;
                        setter($scope, self);
                        $scope.$on("$destroy", function() {
                            setter(null);
                        });
                    }

                    init();

                    ////////
                    function getCellsForRow(row, cellsByRow) {
                        return _.find(cellsByRow, function(entry) {
                            return entry.row === row;
                        });
                    }

                    function isCellDirty(row, cell) {
                        var rowCells = getCellsForRow(row, dirtyCellsByRow);
                        return rowCells && rowCells.cells.indexOf(cell) !== -1;
                    }

                    function setInvalid(isInvalid) {
                        self.$invalid = isInvalid;
                        self.$valid = !isInvalid;
                    }

                    function reset() {
                        dirtyCellsByRow = [];
                        invalidCellsByRow = [];
                        setInvalid(false);
                    }

                    function setCellStatus(row, cell, value, cellsByRow) {
                        var rowCells = getCellsForRow(row, cellsByRow);
                        if (!rowCells && !value) {
                            return;
                        }

                        if (value) {
                            if (!rowCells) {
                                rowCells = {
                                    row: row,
                                    cells: []
                                };
                                cellsByRow.push(rowCells);
                            }
                            if (rowCells.cells.indexOf(cell) === -1) {
                                rowCells.cells.push(cell);
                            }
                        } else {
                            _.remove(rowCells.cells, function(item) {
                                return cell === item;
                            });
                            if (rowCells.cells.length === 0) {
                                _.remove(cellsByRow, function(item) {
                                    return rowCells === item;
                                });
                            }
                        }
                    }

                    function setCellDirty(row, cell, isDirty) {
                        setCellStatus(row, cell, isDirty, dirtyCellsByRow);
                    }

                    function setCellInvalid(row, cell, isInvalid) {
                        setCellStatus(row, cell, isInvalid, invalidCellsByRow);
                        setInvalid(invalidCellsByRow.length > 0);
                    }

                    function untrack(row) {
                        _.remove(invalidCellsByRow, function(item) {
                            return item.row === row;
                        });
                        _.remove(dirtyCellsByRow, function(item) {
                            return item.row === row;
                        });
                        setInvalid(invalidCellsByRow.length > 0);
                    }

                    self.reset = reset;
                    self.isCellDirty = isCellDirty;
                    self.setCellDirty = setCellDirty;
                    self.setCellInvalid = setCellInvalid;
                    self.untrack = untrack;
                }]
            };
        }
    ]);