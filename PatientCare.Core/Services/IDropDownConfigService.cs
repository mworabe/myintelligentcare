﻿using System.Threading.Tasks;
using PatientCare.Core.Entities;
using System.Collections.Generic;

namespace PatientCare.Core.Services
{
    public interface IDropDownConfigService
    {
        Task<bool> UpdateDropDownConfigAsync(DropDownConfig dropDownConfig);

        Task<bool> CreateDropDownConfigAsync(DropDownConfig dropDownConfig);

        Task<bool> DeleteDropDownConfigItemAsync(long id);

        List<DropDownConfig> ReadAllDropDownConfigAsync();

        Task<DropDownConfig> ReadDropDownConfigAsync(long id);

        List<DropDownConfig> ReadDropDownTypeAsync(string dropDownType, string search, string searchField = null, string searchParameter = null, long? activityTypeId = null);
    }
}
