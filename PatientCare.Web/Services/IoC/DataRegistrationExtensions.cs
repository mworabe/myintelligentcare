﻿using System;
using Autofac;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Core.Services.Dashboards;
using PatientCare.Core.Services.ResourceScoreCalculation;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework.DataContext;
using PatientCare.Data.EntityFramework.Repositories;
using PatientCare.Services;
using PatientCare.Services.ResourceScoreCalculation;
using PatientCare.Services.Dashboards;
using PatientCare.Core.Services.Mailing;
using PatientCare.Web.Services.Mailing;
using PatientCare.Services.Mailing;

namespace PatientCare.Web.Services.IoC
{
    public static class DataRegistrationExtensions
    {
        public static ContainerBuilder RegisterRepositories(this ContainerBuilder containerBuilder)
        {
            if (containerBuilder == null) { throw new ArgumentNullException("containerBuilder"); }

            containerBuilder.RegisterType<DataContextFactory>().As<IDataContextFactory>().SingleInstance();
            containerBuilder.RegisterType<DataContextScopeFactory>().As<IDataContextScopeFactory>().SingleInstance();
            containerBuilder.RegisterType<AmbientDataContextLocator>().As<IAmbientDataContextLocator>().SingleInstance();

            #region PicklistRepository Register
            containerBuilder.RegisterType<PicklistRepository<JobTitle>>().As<IPicklistRepository<JobTitle>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Language>>().As<IPicklistRepository<Language>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Skill>>().As<IPicklistRepository<Skill>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<ResourceCompany>>().As<IPicklistRepository<ResourceCompany>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<SurveyCategory>>().As<IPicklistRepository<SurveyCategory>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Certificate>>().As<IPicklistRepository<Certificate>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Division>>().As<IPicklistRepository<Division>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Department>>().As<IPicklistRepository<Department>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<Country>>().As<IPicklistRepository<Country>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<City>>().As<IPicklistRepository<City>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<State>>().As<IPicklistRepository<State>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<ResourceScoreFilter>>().As<IPicklistRepository<ResourceScoreFilter>>().SingleInstance();
            containerBuilder.RegisterType<PicklistRepository<DashboardSetup>>().As<IPicklistRepository<DashboardSetup>>().SingleInstance();
            #endregion

            #region Entity Repository Register
            containerBuilder.RegisterType<SurveyQuestionRepository>().As<ISurveyQuestionRepository>().SingleInstance();            
            containerBuilder.RegisterType<ResourceRepository>().As<IResourceRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceTravelRepository>().As<IResourceTravelRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceCriminalRecordRepository>().As<IResourceCriminalRecordRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceEducationRepository>().As<IResourceEducationRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceSpokenLanguageRepository>().As<IResourceSpokenLanguageRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceTeamRepository>().As<IResourceTeamRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourcePreviousEmploymentRepository>().As<IResourcePreviousEmploymentRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceSurveyResultRepository>().As<IResourceSurveyResultRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceCertificateRepository>().As<IResourceCertificateRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceSkillRepository>().As<IResourceSkillRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceBehaviouralDetailRepository>().As<IResourceBehaviouralDetailRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceIndustryRepository>().As<IResourceIndustryRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourcePracticeAreaRepository>().As<IResourcePracticeAreaRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceRecognitionRepository>().As<IResourceRecognitionRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceInsuranceRepository>().As<IResourceInsuranceRepository>().SingleInstance();
            containerBuilder.RegisterType<CustomFieldRepository>().As<ICustomFieldRepository>().SingleInstance();
            containerBuilder.RegisterType<ResourceCustomFieldValueRepository>().As<IResourceCustomFieldValueRepository>().SingleInstance();
            containerBuilder.RegisterType<SentEmailRepository>().As<ISentEmailRepository>().SingleInstance();
            containerBuilder.RegisterType<RoleConfigureLayoutRepository>().As<IRoleConfigureLayoutRepository>().SingleInstance();
            containerBuilder.RegisterType<HolidayRepository>().As<IHolidayRepository>().SingleInstance();
            containerBuilder.RegisterType<LeaveTypeRepository>().As<ILeaveTypeRepository>().SingleInstance();
            containerBuilder.RegisterType<EntryTypeRepository>().As<IEntryTypeRepository>().SingleInstance();
            containerBuilder.RegisterType<LeaveRequestRepository>().As<ILeaveRequestRepository>().SingleInstance();
            containerBuilder.RegisterType<UserLoginHistoryRepository>().As<IUserLoginHistoryRepository>().SingleInstance();
            containerBuilder.RegisterType<DataContextScopeFactory>().As<IDataContextScopeFactory>().SingleInstance();
            containerBuilder.RegisterType<DropDownConfigRepository>().As<IDropDownConfigRepository>().SingleInstance();
            containerBuilder.RegisterType<UiConfigurationRepository>().As<IUiConfigurationRepository>().SingleInstance();
            containerBuilder.RegisterType<MailMessageFactory>().As<IMailMessageFactory>().SingleInstance();
            containerBuilder.RegisterType<ClinicRepository>().As<IClinicRepository>().SingleInstance();
            containerBuilder.RegisterType<ClinicLocationRepository>().As<IClinicLocationRepository>().SingleInstance();
            containerBuilder.RegisterType<ReferralPhysicianRepository>().As<IReferralPhysicianRepository>().SingleInstance();
            containerBuilder.RegisterType<PatientRepository>().As<IPatientRepository>().SingleInstance();
            containerBuilder.RegisterType<PatientAddressRepository>().As<IPatientAddressRepository>().SingleInstance();
            containerBuilder.RegisterType<PatientParentGuardianRepository>().As<IPatientParentGuardianRepository>().SingleInstance();
            containerBuilder.RegisterType<InjuryRegionRepository>().As<IInjuryRegionRepository>().SingleInstance();
            containerBuilder.RegisterType<DiagnosisRepository>().As<IDiagnosisRepository>().SingleInstance();
            containerBuilder.RegisterType<PatientCaseRepository>().As<IPatientCaseRepository>().SingleInstance();
            containerBuilder.RegisterType<AppointmentTypeRepository>().As<IAppointmentTypeRepository>().SingleInstance();
            containerBuilder.RegisterType<PatientAppointmentRepository>().As<IPatientAppointmentRepository>().SingleInstance();
            containerBuilder.RegisterType<ExercisePositionRepository>().As<IExercisePositionRepository>().SingleInstance();
            containerBuilder.RegisterType<ExerciseActivityRepository>().As<IExerciseActivityRepository>().SingleInstance();
            containerBuilder.RegisterType<ExerciseRepository>().As<IExerciseRepository>().SingleInstance();
            containerBuilder.RegisterType<Exercise_ExerciseActivityRepository>().As<IExercise_ExerciseActivityRepository>().SingleInstance();
            containerBuilder.RegisterType<Exercise_injuryRepository>().As<IExercise_injuryRepository>().SingleInstance();
            containerBuilder.RegisterType<MasterTemplateRepository>().As<IMasterTemplateRepository>().SingleInstance();
            containerBuilder.RegisterType<MasterTemplateExerciseRepository>().As<IMasterTemplateExerciseRepository>().SingleInstance();
            containerBuilder.RegisterType<ExerciseMediaRepository>().As<IExerciseMediaRepository>().SingleInstance();
            containerBuilder.RegisterType<HepGroupRepository>().As<IHepGroupRepository>().SingleInstance();
            containerBuilder.RegisterType<HepMasterRepository>().As<IHepMasterRepository>().SingleInstance();
            containerBuilder.RegisterType<HepDetailRepository>().As<IHepDetailRepository>().SingleInstance();
            containerBuilder.RegisterType<HepFrequencyRepository>().As<IHepFrequencyRepository>().SingleInstance();
            containerBuilder.RegisterType<PatientWorkoutAnalyticRepository>().As<IPatientWorkoutAnalyticRepository>().SingleInstance();
            containerBuilder.RegisterType<PatientGoalRepository>().As<IPatientGoalRepository>().SingleInstance();

            containerBuilder.RegisterType<ExerciseRatingRepository>().As<IExerciseRatingRepository>().SingleInstance();
            containerBuilder.RegisterType<ExerciseCommentRepository>().As<IExerciseCommentRepository>().SingleInstance();
            containerBuilder.RegisterType<AppFunctionalRatingRepository>().As<IAppFunctionalRatingRepository>().SingleInstance();
            containerBuilder.RegisterType<PatientMobileProfileRepository>().As<IPatientMobileProfileRepository>().SingleInstance();
            containerBuilder.RegisterType<ChatSessionRepository>().As<IChatSessionRepository>().SingleInstance();
            containerBuilder.RegisterType<ChatMemberRepository>().As<IChatMemberRepository>().SingleInstance();
            containerBuilder.RegisterType<ChatKeyRepository>().As<IChatKeyRepository>().SingleInstance();
            containerBuilder.RegisterType<HepPatientPlanRepository>().As<IHepPatientPlanRepository>().SingleInstance();
            containerBuilder.RegisterType<PostalCodeRepository>().As<IPostalCodeRepository>().SingleInstance();
            #endregion

            #region Service Register
            containerBuilder.RegisterType<AppUtilityService>().As<IAppUtilityService>().SingleInstance();
            containerBuilder.RegisterType<DropDownConfigService>().As<IDropDownConfigService>().SingleInstance();
            containerBuilder.RegisterType<UiConfigurationService>().As<IUiConfigurationServices>().SingleInstance();
            containerBuilder.RegisterType<TimeManagementService>().As<ITimeManagementService>().SingleInstance();            
            containerBuilder.RegisterType<ResourceService>().As<IResourceService>().SingleInstance();
            containerBuilder.RegisterType<ResourceScoreService>().As<IResourceScoreService>().SingleInstance();      
            containerBuilder.RegisterType<CustomFieldService>().As<ICustomFieldService>().SingleInstance();          
            containerBuilder.RegisterType<DashboardService>().As<IDashboardService>().SingleInstance();
            containerBuilder.RegisterType<EmailService>().As<IEmailService>().SingleInstance();
            containerBuilder.RegisterType<ClinicService>().As<IClinicService>().SingleInstance();
            containerBuilder.RegisterType<ReferralService>().As<IReferralService>().SingleInstance();
            containerBuilder.RegisterType<PatientService>().As<IPatientService>().SingleInstance();
            containerBuilder.RegisterType<ConfigurationDataService>().As<IConfigurationDataService>().SingleInstance();
            containerBuilder.RegisterType<ExerciseService>().As<IExerciseService>().SingleInstance();
            containerBuilder.RegisterType<HepService>().As<IHepService>().SingleInstance();
            containerBuilder.RegisterType<MobileService>().As<IMobileService>().SingleInstance();
            containerBuilder.RegisterType<ChatService>().As<IChatService>().SingleInstance();
           
            #endregion

            containerBuilder.RegisterType<EmailSender>().As<IEmailSender>().SingleInstance();

            return containerBuilder;
        }
    }
}