namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedCountryDefaultData : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                   -- setting to null resource countries fields
                    update r set country_id = null, armed_forces_country_id = null
                    from resources r
                    where country_id is not null or armed_forces_country_id is not null
                    -- deleting old countries values and reset index from one
                    delete from countries
                    -- adding new values to countries
                    SET IDENTITY_INSERT [dbo].[countries] ON 
                    INSERT [dbo].[countries] ([id], [name]) VALUES (1, N'Afghanistan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (2, N'Albania')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (3, N'Algeria')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (4, N'American Samoa')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (5, N'Andorra')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (6, N'Anla')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (7, N'Anguilla')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (8, N'Antarctica')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (9, N'Antigua And Barbuda')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (10, N'Argentina')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (11, N'Armenia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (12, N'Aruba')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (13, N'Australia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (14, N'Austria')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (15, N'Azerbaijan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (16, N'Bahamas The')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (17, N'Bahrain')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (18, N'Bangladesh')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (19, N'Barbados')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (20, N'Belarus')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (21, N'Belgium')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (22, N'Belize')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (23, N'Benin')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (24, N'Bermuda')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (25, N'Bhutan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (26, N'Bolivia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (27, N'Bosnia and Herzegovina')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (28, N'Botswana')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (29, N'Bouvet Island')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (30, N'Brazil')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (31, N'British Indian Ocean Territory')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (32, N'Brunei')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (33, N'Bulgaria')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (34, N'Burkina Faso')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (35, N'Burundi')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (36, N'Cambodia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (37, N'Cameroon')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (38, N'Canada')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (39, N'Cape Verde')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (40, N'Cayman Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (41, N'Central African Republic')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (42, N'Chad')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (43, N'Chile')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (44, N'China')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (45, N'Christmas Island')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (46, N'Cocos (Keeling) Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (47, N'Colombia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (48, N'Comoros')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (49, N'Congo')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (50, N'Congo The Democratic Republic Of The')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (51, N'Cook Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (52, N'Costa Rica')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (53, N'Cote D''Ivoire (Ivory Coast)')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (54, N'Croatia (Hrvatska)')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (55, N'Cuba')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (56, N'Cyprus')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (57, N'Czech Republic')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (58, N'Denmark')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (59, N'Djibouti')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (60, N'Dominica')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (61, N'Dominican Republic')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (62, N'East Timor')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (63, N'Ecuador')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (64, N'Egypt')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (65, N'El Salvador')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (66, N'Equatorial Guinea')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (67, N'Eritrea')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (68, N'Estonia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (69, N'Ethiopia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (70, N'External Territories of Australia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (71, N'Falkland Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (72, N'Faroe Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (73, N'Fiji Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (74, N'Finland')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (75, N'France')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (76, N'French Guiana')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (77, N'French Polynesia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (78, N'French Southern Territories')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (79, N'Gabon')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (80, N'Gambia The')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (81, N'Georgia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (82, N'Germany')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (83, N'Ghana')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (84, N'Gibraltar')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (85, N'Greece')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (86, N'Greenland')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (87, N'Grenada')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (88, N'Guadeloupe')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (89, N'Guam')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (90, N'Guatemala')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (91, N'Guernsey and Alderney')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (92, N'Guinea')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (93, N'Guinea-Bissau')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (94, N'Guyana')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (95, N'Haiti')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (96, N'Heard and McDonald Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (97, N'Honduras')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (98, N'Hong Kong S.A.R.')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (99, N'Hungary')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (100, N'Iceland')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (101, N'India')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (102, N'Indonesia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (103, N'Iran')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (104, N'Iraq')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (105, N'Ireland')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (106, N'Israel')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (107, N'Italy')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (108, N'Jamaica')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (109, N'Japan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (110, N'Jersey')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (111, N'Jordan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (112, N'Kazakhstan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (113, N'Kenya')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (114, N'Kiribati')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (115, N'Korea North')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (116, N'Korea South')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (117, N'Kuwait')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (118, N'Kyrgyzstan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (119, N'Laos')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (120, N'Latvia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (121, N'Lebanon')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (122, N'Lesotho')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (123, N'Liberia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (124, N'Libya')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (125, N'Liechtenstein')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (126, N'Lithuania')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (127, N'Luxembourg')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (128, N'Macau S.A.R.')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (129, N'Macedonia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (130, N'Madagascar')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (131, N'Malawi')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (132, N'Malaysia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (133, N'Maldives')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (134, N'Mali')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (135, N'Malta')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (136, N'Man (Isle of)')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (137, N'Marshall Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (138, N'Martinique')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (139, N'Mauritania')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (140, N'Mauritius')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (141, N'Mayotte')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (142, N'Mexico')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (143, N'Micronesia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (144, N'Moldova')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (145, N'Monaco')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (146, N'Mongolia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (147, N'Montserrat')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (148, N'Morocco')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (149, N'Mozambique')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (150, N'Myanmar')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (151, N'Namibia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (152, N'Nauru')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (153, N'Nepal')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (154, N'Netherlands Antilles')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (155, N'Netherlands The')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (156, N'New Caledonia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (157, N'New Zealand')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (158, N'Nicaragua')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (159, N'Niger')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (160, N'Nigeria')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (161, N'Niue')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (162, N'Norfolk Island')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (163, N'Northern Mariana Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (164, N'Norway')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (165, N'Oman')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (166, N'Pakistan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (167, N'Palau')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (168, N'Palestinian Territory Occupied')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (169, N'Panama')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (170, N'Papua new Guinea')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (171, N'Paraguay')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (172, N'Peru')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (173, N'Philippines')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (174, N'Pitcairn Island')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (175, N'Poland')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (176, N'Portugal')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (177, N'Puerto Rico')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (178, N'Qatar')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (179, N'Reunion')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (180, N'Romania')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (181, N'Russia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (182, N'Rwanda')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (183, N'Saint Helena')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (184, N'Saint Kitts And Nevis')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (185, N'Saint Lucia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (186, N'Saint Pierre and Miquelon')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (187, N'Saint Vincent And The Grenadines')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (188, N'Samoa')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (189, N'San Marino')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (190, N'Sao Tome and Principe')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (191, N'Saudi Arabia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (192, N'Senegal')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (193, N'Serbia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (194, N'Seychelles')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (195, N'Sierra Leone')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (196, N'Singapore')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (197, N'Slovakia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (198, N'Slovenia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (199, N'Smaller Territories of the UK')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (200, N'Solomon Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (201, N'Somalia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (202, N'South Africa')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (203, N'South Georgia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (204, N'South Sudan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (205, N'Spain')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (206, N'Sri Lanka')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (207, N'Sudan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (208, N'Suriname')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (209, N'Svalbard And Jan Mayen Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (210, N'Swaziland')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (211, N'Sweden')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (212, N'Switzerland')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (213, N'Syria')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (214, N'Taiwan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (215, N'Tajikistan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (216, N'Tanzania')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (217, N'Thailand')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (218, N'Togo')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (219, N'Tokelau')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (220, N'Tonga')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (221, N'Trinidad And Tobago')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (222, N'Tunisia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (223, N'Turkey')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (224, N'Turkmenistan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (225, N'Turks And Caicos Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (226, N'Tuvalu')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (227, N'Uganda')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (228, N'Ukraine')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (229, N'United Arab Emirates')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (230, N'United Kingdom')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (231, N'United States')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (232, N'United States Minor Outlying Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (233, N'Uruguay')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (234, N'Uzbekistan')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (235, N'Vanuatu')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (236, N'Vatican City State (Holy See)')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (237, N'Venezuela')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (238, N'Vietnam')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (239, N'Virgin Islands (British)')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (240, N'Virgin Islands (US)')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (241, N'Wallis And Futuna Islands')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (242, N'Western Sahara')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (243, N'Yemen')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (244, N'Yugoslavia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (245, N'Zambia')
                    INSERT [dbo].[countries] ([id], [name]) VALUES (246, N'Zimbabwe')
                    SET IDENTITY_INSERT [dbo].[countries] OFF
                ");
        }

        public override void Down()
        {
        }
    }
}
