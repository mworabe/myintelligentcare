﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Countries
{
    [DataContract]
    public class CountryRequest
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string IsoCode { get; set; }

        [DataMember]
        public string IsdCode { get; set; }
    }
}