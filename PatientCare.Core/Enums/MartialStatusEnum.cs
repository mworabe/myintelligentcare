﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Maritial Status Enum.
    /// </summary>
    public enum MartialStatusEnum
    {
        Single = 0,
        Married = 1
    }
}
