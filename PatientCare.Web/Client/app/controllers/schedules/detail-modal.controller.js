﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("DetailModalCtrl", [
         "$uibModal", "toaster", "$stateParams", "$scope", "$state", '$uibModalInstance', 'data', 'AppointmentFactory', 'AppointmentService',
        function ($uibModal, toaster, $stateParams, $scope, $state, $uibModalInstance, data, AppointmentFactory, AppointmentService) {


            var self = this;
            $scope.model = data.dataObject;

            $scope.statusItems = AppointmentFactory.getAllStatus();
            $scope.isModified = false;
            $scope.appointmentDetail = true;
            $scope.paymentType = false;
            $scope.func = function () {
                if (!$scope.appointmentDetail) {
                    $scope.appointmentDetail = true;
                }
                else {
                $scope.appointmentDetail = false;

                }
               
            }
 
            $scope.funct = function () {
                if (!$scope.paymentType) {
                    $scope.paymentType = true;
                }
                else {
                    $scope.paymentType = false;

                }

            }

            $scope.close = function () {
                $uibModalInstance.close($scope.model);
            }

            $scope.onViewClick = function () {
                $state.go("patientsEdit", { id: $scope.model.patientId });
                $uibModalInstance.close($scope.model);
            }
            $scope.onChatClick = function () {
                $state.go("communication.chat.chat-box");
                $uibModalInstance.close($scope.model);
            }

            $scope.onStatusChange = function (object) {
                $scope.loadCorrect = AppointmentService.changeStatus({
                    id: $scope.model.appointmentId,
                    appointmentStatus: $scope.model.appointmentStatus
                }, {}, function (result) {
                    self.model = result;
                    $scope.isModified = true;
                });
            }
            $scope.onApproveButtonClick = function onApproveButtonClick() {
                $scope.loadCorrect = AppointmentService.changeStatus({
                    id: $scope.model.appointmentId,
                    appointmentStatus: 'Confirmed',
                }, {}, function (result) {
                    $scope.model.appointmentStatus = 'Confirmed';
                    self.model = result;
                    $scope.isModified = true;
                    $uibModalInstance.close($scope.model);
                });
            }
            $scope.onAppointmentEditButtonClick = function onAppointmentEditButtonClick() {
                $scope.loadCorrect = AppointmentService.get({ id: $scope.model.appointmentId }, function (appontmientMode) {
                    $scope.close(null);
                    AppointmentFactory.newAppointment({ appointmentModel: appontmientMode }, function (result) {
                        if (result) {
                            //$scope.model = result;
                        }
                    });
                });
            }
         

        }
    ]);
