﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("DropDownConfigurationController", [
        "DynamicDropDownConfigService", "$scope",
        function (dynamicDropDownConfigService, $scope) {
            var self = this;

            self.loadDropDownData = function () {
                self.loadingPage = false;
                dynamicDropDownConfigService.query({ dropdowntype: self.cofigType },
                    function (success) {
                        self.dropDownList = success;
                        self.loadingPage = false;
                    },
                    function (error) {
                        toaster.pop("error", "", "Unable to process your request, please try again later.");
                        self.loadingPage = false;
                    }
                );
            }
            self.getTabList = function () {
                return [
                    {
                        id: "reasonCode",
                        container: 'personalInfo',
                        name: "Reason Code",
                        required: true,
                        visible: true,
                    },
                ];
            }
            self.assignTabClicks = function () {
                
                _.forEach(self.tabs, function (tab) {
                    var functionName = tab.id + "_Click";
                
                    self.functionName = new Function(functionName, "self.configType = tab.id; self.currentTab = tab.id;");
                
                });
            }
            self.getCurrentPanelTitle = function () {
                _.forEach(self.tabs, function (tab) {
                    if (self.currentTab && self.currentTab == tab.id)
                        return tab.name;
                });
                return "Project Configuration Data";
            }
            self.configType;
            self.currentTab;
            self.dropDownList = [];
            self.loadingPage = false;
            $scope.leftMenuMaxHeight = (window.innerHeight - 30);
            self.tabs = self.getTabList();
            self.assignTabClicks();
        }

    ]);