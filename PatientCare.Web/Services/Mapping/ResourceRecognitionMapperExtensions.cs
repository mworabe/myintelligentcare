﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.ResourceRecognitions;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceRecognitionMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceRecognitionRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceRecognition,ResourceRecognitionResponse>
                    (new ExtendedMapConfig<ResourceRecognition,ResourceRecognitionResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceRecognition[],ResourceRecognitionResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceRecognitionRequest,ResourceRecognition>
                    (new ExtendedMapConfig<ResourceRecognitionRequest,ResourceRecognition>()));

            return mapperProvider;
        }
    }
}