﻿using PatientCare.Core.Enums;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Patients
{
    [DataContract]
    public class PatientDetailListResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string PatientNumber { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public string PhotoFileNameUrl { get; set; }

        [DataMember]
        public long? CaseId { get; set; }

        [DataMember]
        public string CaseNo { get; set; }

        [DataMember]
        public string InjuryRegion { get; set; }

        [DataMember]
        public DateTime? LastVisitDate { get; set; }

        [DataMember]
        public PatientStatusEnum? Status { get; set; }

        [DataMember]
        public bool? HasMobileAccess { get; set; }

        [DataMember]
        public bool HasHep { get; set; }

        [DataMember]
        public long? HepId { get; set; }

        [DataMember]
        public int? PreExercisesPainRating { get; set; }

        [DataMember]
        public string UserId { get; set; }
        
    }
}