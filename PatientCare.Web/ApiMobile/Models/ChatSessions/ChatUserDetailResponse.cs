﻿using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.ChatSessions
{
    [DataContract]
    public class ChatUserDetailResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Role { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string PhotoFileName { get; set; }

        [DataMember]
        public string PhotoFileNameUrl { get; set; }

        [DataMember]
        public string PrivateKey { get; set; }
    }
}