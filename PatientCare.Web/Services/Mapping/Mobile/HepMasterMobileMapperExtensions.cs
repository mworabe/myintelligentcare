﻿using System;
using EmitMapper;
using PatientCare.Services.Mapping;
using System.Linq;
using PatientCare.Core.Entities;
using EmitMapper.MappingConfiguration;
using PatientCare.Web.Api.Models.HepDetails;
using PatientCare.Web.ApiMobile.Models.HepMasters;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class HepMasterMobileMapperExtensions
    {
        public static EmitMapperMapperProvider HepMasterMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMaster, HepMasterMobileResponse>
                    (new ExtendedMapConfig<HepMaster, HepMasterMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMaster[], HepMasterMobileResponse[]>());

            mapperProvider.RegisterMapper(() =>
                           ObjectMapperManager.DefaultInstance.GetMapper<HepMaster, HepMasterGroupMobileResponse>
                               (new ExtendedMapConfig<HepMaster, HepMasterGroupMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMaster[], HepMasterGroupMobileResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMaster, HepMasterMobileListResponse>
                    (new ExtendedMapConfig<HepMaster, HepMasterMobileListResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMaster[], HepMasterMobileListResponse[]>());

            var hepDetailMapper =
                 ObjectMapperManager.DefaultInstance.GetMapper<HepDetailRequest, HepDetail>(
                   new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMasterMobileRequest, HepMaster>(
                    new ExtendedMapConfig<HepMasterMobileRequest, HepMaster>()
                    .ForMember(x => x.HepDetails, y => y.HepDetails != null ? y.HepDetails.Select(i => hepDetailMapper.Map(i)).ToList() : null)
                  ));

            return mapperProvider;
        }
    }
}