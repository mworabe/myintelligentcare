﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.Patients
{
    [DataContract]
    public class PatientMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Prefix { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string NickName { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }
        
        [DataMember]
        public string SSN { get; set; }

        [DataMember]
        public GenderEnum? Gender { get; set; }

        [DataMember]
        public string Email { get; set; }
        
        [DataMember]
        public string CellNumber { get; set; }
        
        [DataMember]
        public long? LanguageId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Language { get; set; }

        [DataMember]
        public MartialStatusEnum? MaritialStatus { get; set; }

        [DataMember]
        public string EmergencyContact { get; set; }

        [DataMember]
        public RelationshipEnum? RelationshipToContact { get; set; }

        [DataMember]
        public string EmergencyPhone { get; set; }

        [DataMember]
        public bool? IdDeleted { get; set; }
    }
}