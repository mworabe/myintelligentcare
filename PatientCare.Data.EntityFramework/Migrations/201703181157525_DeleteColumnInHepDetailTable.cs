namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteColumnInHepDetailTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.hep_detail", "group_id", "dbo.hep_groups");
            DropIndex("dbo.hep_detail", new[] { "group_id" });
            DropColumn("dbo.hep_detail", "group_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.hep_detail", "group_id", c => c.Long());
            CreateIndex("dbo.hep_detail", "group_id");
            AddForeignKey("dbo.hep_detail", "group_id", "dbo.hep_groups", "Id");
        }
    }
}
