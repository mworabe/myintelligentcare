﻿using FluentValidation;

namespace PatientCare.Web.Api.Models.Clinics
{
    public class ClinicRequestValidator : AbstractValidator<ClinicRequest>
    {
        public ClinicRequestValidator()
        {
            RuleFor(model => model.ClinicPrefix)
                .NotEmpty()
                .WithMessage("Prefix is Required.");

            RuleFor(model => model.Name)
             .NotEmpty()
             .WithMessage("Clinic is Required.");
        }
    }
}