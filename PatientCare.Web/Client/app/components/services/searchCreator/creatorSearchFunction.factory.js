﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("CreatorSearchFunction", ["$q",
        function ($q) {

            return function (self, methodName, request, fieldName, additionalParameter) {
                self[methodName] = function (search) {
                    var defer = $q.defer();
                    additionalParameter = additionalParameter || {};
                    var query = {
                        search: search || ""
                    };
                    if (additionalParameter && typeof additionalParameter === "object") {
                        for (var obj in additionalParameter) {
                            query[obj] = additionalParameter[obj];
                        }
                    }
                    request.autocomplete(query).$promise.then(function (responce) {
                        defer.resolve(responce || []);
                    });
                    return defer.promise;
                }
            }

        }
    ])

    .factory("CreatorSearchParamFunction", ["$q",
        function ($q) {

            return function (self, methodName, request, fieldName, additionalParameter) {
                self[methodName] = function (search) {
                    var defer = $q.defer();
                    var query = {
                        search: search || ""
                    };

                    if (additionalParameter)
                        query["additionalParameter"] = additionalParameter;

                    request.autocomplete(query).$promise.then(function (responce) {
                        defer.resolve(responce || []);
                    });
                    return defer.promise;
                }
            }

        }
    ])

    .factory("DropdownConfigsFunction", ["$q",
        function ($q) {
            return function (self, methodName, request, dropDownType, additionalParameter) {
                self[methodName] = function (search) {
                    var defer = $q.defer();
                    additionalParameter = additionalParameter || {};
                    var query = {
                        dropdowntype: dropDownType || "",
                        search: search || ""
                    };
                    if (additionalParameter && typeof additionalParameter === "object") {
                        for (var obj in additionalParameter) {
                            query[obj] = additionalParameter[obj];
                        }
                    }
                    request.autocomplete(query).$promise.then(function (responce) {
                        defer.resolve(responce || []);
                    });
                    return defer.promise;
                }
            }
        }
    ]);