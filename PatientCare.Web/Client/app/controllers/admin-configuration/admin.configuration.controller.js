﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("AdminConfigurationViewController",
    ["$scope", "Auth", "$q", "$state", "$uibModal", "$stateParams",
        "CreatorSearchFunction", "Resource", "RoleSetup", "Account", "ClientCompany", "LoginHistory", "DashboardSetups",
        "UIConfigService", "DynamicDropDownConfigService", "toaster", "SystemFunctions",
        AdminConfigurationCtrl]);

function AdminConfigurationCtrl($scope, auth, $q, $state, $uibModal, $stateParams,
    creatorSearchFunction, resourceService, roleSetup, account, clientCompanyService, loginHistoryService, dashboardSetupService,
    UIConfigService, dynamicDropDownConfigService, toaster, systemFunctions) {

    var self = this;
    var CONFIG_KEY = "PRODUCTIVITY_CONFIGURATIONS";
    var serverData = {};
    var configs = angular.fromJson(localStorage.getItem(CONFIG_KEY));
    var errorList = [];
    self.isLoadingData = false;
    self.currentTab = $stateParams.currentTab || "roles";
    self.dataModel = {};
    self.roles = [];

    creatorSearchFunction(self, "searchResource", resourceService, "firstName", { showInactive: false });

    if (configs) {
        if (typeof configs === "object") {
            if (typeof configs.value === "string") {
                self.dataModel = angular.fromJson(configs.value);
            } else {
                self.dataModel = configs.value;
            }
        }
    }

    function getTaskComplexityData() {
        self.complxityDataList = dynamicDropDownConfigService.query({ dropdowntype: "taskComplexity", pageIndex: 1, pageSize: 50 });
    }

    function getDefectSeverityData() {
        self.severityDataList = dynamicDropDownConfigService.query({ dropdowntype: "defectSeverity", pageIndex: 1, pageSize: 50 });
    }
    function getServerDataForProductivityConfig() {
        self.isLoadingData = UIConfigService.getSystemConfigurationisByKey({ key: CONFIG_KEY, sysConfig: true }, function (serverData) {
            if (serverData && typeof serverData.value === "string") {
                var value = angular.fromJson(serverData.value);
                if (!self.dataModel) {
                    self.dataModel = {};
                    self.dataModel.jobTitle = value.jobTitle;
                    self.dataModel.range = value.range;
                    self.dataModel.EstimateHoursConfig = value.EstimateHoursConfig;
                    self.dataModel.EndDateConfig = value.EndDateConfig;

                } else {
                    self.dataModel.jobTitle = value.jobTitle;
                    self.dataModel.range = value.range;
                    self.dataModel.EstimateHoursConfig = value.EstimateHoursConfig;
                    self.dataModel.EndDateConfig = value.EndDateConfig;
                }
            }
        }, function (error) { });
    }
    self.productivityConfigurations_Click = function productivityConfigurations_Click() {
        getTaskComplexityData();
        getDefectSeverityData();
        getServerDataForProductivityConfig();
    }

    self.mainViewList = [
        {
            id: "roles",
            label: "Role",
            name: "Role",
            visible: true,
            service: {
                get: roleSetup.query,
                save: roleSetup.save,
                update: roleSetup.update,
                delete: roleSetup.delete,
            },
            tableConfigurations: {
                pageName: "Roles",
                hideBulkEdit: true,
                cols: function () {
                    return [
                        {
                            field: "name",
                            title: "Role Name",
                            sortable: "name",
                            show: true,
                            dataType: "text"
                        }, {
                            field: "description",
                            title: "Description",
                            sortable: "description",
                            show: true,
                            dataType: "text"
                        }, {
                            field: "isActiveYesNo",
                            title: "Active",
                            sortable: "isActiveYesNo",
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "action",
                            title: ".",
                            class: "blank-cell",
                            dataType: "command"
                        }
                    ];
                },
                onAdd_Click: function () {
                    $state.go("roleEdit");
                },
                onEdit_Click: function (row) {
                    $state.go("roleEdit", { id: row.id });
                },
                headerButtons: [
                    {
                        text: "Default Layout",
                        click: function () {
                            $state.go("configLayout");
                        },
                        iconClass: "layout"
                    }
                ]
            }
        }, {
            id: "users",
            label: "Users",
            name: "Users",
            visible: true,
            service: {
                get: account.query,
                save: account.save,
                update: account.update,
                delete: account.delete,
                onDataGet: function (response) {
                    account.roles = response.roles;
                    self.roles = response.roles;
                },
            },
            tableConfigurations: {
                pageName: "Users",
                hideBulkEdit: true,
                cols: function () {
                    return [
                        {
                            field: "userName",
                            title: "User Name",
                            sortable: "groupName",
                            show: true,
                            required: true,
                            dataType: "text"
                        }, {
                            field: "role",
                            title: "Role",
                            sortable: "role",
                            show: true,
                            dataType: "ui-select",
                            required: true,
                            list: function () { return self.roles; }
                        }, {
                            title: "Resource",
                            fieldInit: "resource",
                            field: "resourceId",
                            initValue: "resource",
                            dataType: "angucomplete-alt",
                            search: self.searchResource,
                            sortable: "resource.FirstName",
                            required: false,
                            customViewText: function (row) {
                                if (row.resource)
                                    return row.resource.name;
                                return "";
                            },
                            angucompleteSelect: function (select) {
                                var item = this.$parent.$parent.$parent.$parent.row;
                                if (select && select.originalObject && select.originalObject.id > 0) {
                                    item.resourceId = select.originalObject.id;
                                    item.resource = select.originalObject;
                                } else {
                                    item.resource = null;
                                    item.resourceId = null;
                                }

                            }
                        },
                        {
                            field: "password",
                            title: "Password",
                            sortable: false,
                            show: true,
                            dataType: "password",
                            required: true,
                            dataTypeView: "empty"
                        },
                        {
                            field: "action",
                            title: ".",
                            class: "blank-cell",
                            dataType: "command"
                        }
                    ];
                }
            }
        },
        {
            id: "loginHistory",
            label: "Login History",
            name: "Login History",
            visible: true,
            service: {
                get: loginHistoryService.query,
                save: loginHistoryService.save,
                update: loginHistoryService.update,
                delete: loginHistoryService.delete,
            },
            tableConfigurations: loginHistoryService.getTableOption
        }, {
            id: "dashboardList",
            label: "Dashboards",
            name: "Dashboard",
            visible: false,
            service: {
                get: dashboardSetupService.getDashboardList,
                save: dashboardSetupService.save,
                update: dashboardSetupService.update,
                delete: dashboardSetupService.delete,
            },
            tableConfigurations: dashboardSetupService.getTableOption
        }
        /*, {
            id: "productivityConfigurations",
            label: "Productivity Configurations",
            name: "Productivity Configurations",
            visible: true,
        },*/
    ];


    this.addNewDashboard = function addNewDashboard() {
        $uibModal.open({
            animation: true,
            templateUrl: 'Client/app/controllers/dashboard/add-dashboard-modal.html',
            controller: 'AddDashboardCtrl as AddDashboardCtrl',
            resolve: {}
        });
    }

    self.mainViewList[3].tableConfigurations.onAdd_Click = self.addNewDashboard;

    function validateModeBeforeSave() {
        if (self.dataModel && self.dataModel.EstimateHours) {
            var percentage = self.dataModel.EstimateHours.percentage || 0;
            if (percentage <= 0 && percentage > 100) {
                errorList.push({ type: "error", msg: "Estimate Hours Percentage is not Valid." });
                console.info("Estimate Hours Percentage is not Valid");
                return false;
            }
            var over = self.dataModel.EstimateHours.over;
            if (typeof over !== "number") {
                errorList.push({ type: "error", msg: "Estimate Hours Over value is not Valid." });
                console.info("Estimate Hours Over value is not Valid");
                return false;
            } else if (!over) {
                errorList.push({ type: "error", msg: "Estimate Hours Over value is Required." });
                console.info("Estimate Hours Over value is Required");
                return false;
            } else {
                return true;
            }
        } else {
            console.info("Estimate Hours fields are rewuired");
            return false;
        }
        return true;
    }

    self.saveButtonClick = function saveButtonClick() {
        systemFunctions.scrollToTopZero();
        var obj = {};
        obj.id = (configs) ? configs.id : 0;
        obj.key = CONFIG_KEY;
        obj.value = angular.toJson(self.dataModel);
        obj.complxityDataList = self.complxityDataList.data;
        obj.severityDataList = self.severityDataList.data;

        self.isLoadingData = UIConfigService.saveSystemConfigurations(obj, function (success) { toaster.pop("success", "", "Productivity Configurations Saved Successfully"); }, function (error) { toaster.pop("error", "", "Unable to save Productivity Configurations, please try again later."); });
    }

}