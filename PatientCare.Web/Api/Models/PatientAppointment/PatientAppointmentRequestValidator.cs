﻿using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using FluentValidation;
using PatientCare.Web.Api.Models.PatientAppointment;

namespace PatientCare.Web.Api.Models.ResourceSpokenLanguages
{
    public class PatientAppointmentRequestValidator : AbstractValidator<PatientAppointmentRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IPatientCaseRepository _patientCaseRepository;

        public PatientAppointmentRequestValidator
            (
                IDataContextScopeFactory dbContextScopeFactory, 
                IPatientCaseRepository patientCaseRepository
            )
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _patientCaseRepository = patientCaseRepository;

            RuleFor(model => model.PatientId)
                .NotEmpty()
                .WithMessage("Patient name is required.");

            RuleFor(model => model.CaseId)
                .NotEmpty()
                .WithMessage("This patient have no any case.");

        }
    }
}