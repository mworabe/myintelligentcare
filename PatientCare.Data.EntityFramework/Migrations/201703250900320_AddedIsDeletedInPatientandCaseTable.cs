namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsDeletedInPatientandCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "id_deleted", c => c.Boolean());
            AddColumn("dbo.patient_cases", "id_deleted", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.patient_cases", "id_deleted");
            DropColumn("dbo.patients", "id_deleted");
        }
    }
}
