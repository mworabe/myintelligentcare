﻿using PatientCare.Data.AbstractDataContext;

namespace PatientCare.Data.EntityFramework.DataContext
{
    public sealed class AmbientDataContextLocator : IAmbientDataContextLocator
    {
        public T Get<T>() where T : class
        {
            var ambientDbContextScope = DataContextScope.GetAmbientScope();
            return ambientDbContextScope == null ? null : ambientDbContextScope.DataContexts.Get<T>();
        }

        public void Set(IDataContextScope dataContextScope)
        {
            DataContextScope.SetAmbientScope((DataContextScope)dataContextScope);
        }
    }
}
