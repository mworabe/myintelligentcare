﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Exercises;
using PatientCare.Web.Api.Models.HepFrequencies;
using PatientCare.Web.Api.Models.HepGroups;

namespace PatientCare.Web.Api.Models.HepDetails
{
    public class HepDetailRequest
    {
        public long Id { get; set; }

        public string name { get; set; }

        public long? ExerciseId { get; set; }

        public ExerciseResponse Exercise { get; set; }

        public long? GroupId { get; set; }

        public HepGroupResponse Group { get; set; }

        public long? HepId { get; set; }

        public int? Sets { get; set; }

        public int? Reps { get; set; }

        public int? Time { get; set; }

        public TimeEnum? TimeUnit { get; set; }

        public int? Holds { get; set; }

        public TimeEnum? HoldsUnit { get; set; }

        public decimal? Weight { get; set; }

        public string WeightUnit { get; set; }

        public int? ExerciseOrder { get; set; }

        public HepFrequencyRequest[] Frequencies { get; set; }
        
        public string Comments { get; set; }

        public string frequency { get; set; }

        public string resistance { get; set; }

        public bool? IsActiveFrequency { get; set; }

        public string others { get; set; }
        public string template { get; set; }
    }   
}