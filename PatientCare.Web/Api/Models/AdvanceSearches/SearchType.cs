﻿namespace PatientCare.Web.Api.Models.ResourceScore
{
    public enum SearchType
    {
        All = 0,
        Resource = 1,
        Project = 2
    }
}
