﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.ResourceTravels;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Travel operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceTravelsController : EdzBaseApiController<ResourceTravel, ResourceTravelRequest, ResourceTravelResponse>
    {
        /// <summary>
        /// Description : In this class we pass  Resource Travel IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource Travel Repository</param>
        public ResourceTravelsController(IResourceTravelRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.Resource);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get ResourceTravel.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Resource Travel Entity</returns>
        [HttpGet]
        [Route("autocomplete/{search}")]
        public async Task<IHttpActionResult> Autocomplete(string search)
        {
            var filter = new FilterQuery<ResourceTravel>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => x.SpecialConsiderations.ToLower().StartsWith(search.ToLower()));
            }
            var autocompleteResult =
                await
                    EntityRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<ResourceTravel[], PicklistResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }

        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceTravels/" + id, UriKind.Relative);
        }
    }
}