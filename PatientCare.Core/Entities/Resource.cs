﻿using System;
using System.Collections.Generic;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource.
    /// </summary>
    public class Resource : Entity
    {
        public Resource()
        {
            SurveyResults = new HashSet<ResourceSurveyResult>();
        }
        #region Personal Info Division

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Overview { get; set; }

        public DateTime? BirthDate { get; set; }

        public MartialStatusEnum? MartialStatus { get; set; }

        public GenderEnum? Gender { get; set; }

        public LGBTEnum? LGBT { get; set; }

        public string Ethnicity { get; set; }

        public string Prefix { get; set; }

        public string Suffix { get; set; }

        public bool? Active { get; set; }

        public string PreferredFirstname { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Address4 { get; set; }

        public string Address5 { get; set; }

        public string Address6 { get; set; }

        public long? CityId { get; set; }

        public virtual City City { get; set; }

        public string ZipCode { get; set; }

        public long? CountryId { get; set; }
        public virtual Country Country { get; set; }

        public long? StateId { get; set; }
        public virtual State State { get; set; }

        public string TimekeeperNumber { get; set; }

        public long? ClinicId { get; set; }
        public virtual Clinic Clinic { get; set; }

        public long? ClinicLocationId { get; set; }
        public virtual ClinicLocation ClinicLocation { get; set; }

        #endregion

        #region Employement Detail Division

        public string EmployeeId { get; set; }

        public long? JobTitleId { get; set; }

        public virtual JobTitle JobTitle { get; set; }

        public DateTime? StartDate { get; set; }

        public string Ssn { get; set; }

        public DateTime? EndDate { get; set; }

        public string Location { get; set; }

        public string OfficeNumber { get; set; }

        public string Extension { get; set; }

        public long? SupervisorId { get; set; }

        public virtual Resource Supervisor { get; set; }

        public string TimeZoneInfo { get; set; }

        public string HomeNumber { get; set; }

        public long? DivisionId { get; set; }

        public virtual Division Division { get; set; }

        public RelationshipEnum? Relationship { get; set; }

        public string MobilePhoneNumber { get; set; }

        public long? DepartmentId { get; set; }

        public virtual Department Department { get; set; }

        public string EmergencyContactName { get; set; }

        public string EmergencyContactPhoneNumber { get; set; }

        #endregion

        #region Compensation and Benefits

        public decimal? FirmRate { get; set; }

        public decimal? NationalRate { get; set; }

        public EmploymentTypeEnum? EmploymentType { get; set; }

        public decimal? Salary { get; set; }

        public decimal? BonusOrOtherPay { get; set; }
        #endregion

        #region Education

        public int? GradeLevel { get; set; }

        public int? YearsOfCollege { get; set; }

        #endregion

        #region Working Details

        public int? YearsEmployed { get; set; }

        public bool? AvailableToTravel { get; set; }

        public bool? WorksWellInTeam { get; set; }

        public bool? ValidPassport { get; set; }

        public bool? WorksWellAlone { get; set; }

        #endregion

        #region Background Check
        public long? ArmedForcesCountryId { get; set; }

        public virtual Country ArmedForcesCountry { get; set; }

        public string ArmedForcesBranch { get; set; }

        public bool? ArmedForces { get; set; }

        public string DrugTest { get; set; }

        public string CreditReportOk { get; set; }

        public YesNoOtherEnum? MotorVehicleReport { get; set; }

        public YesNoOtherEnum? EmploymentEligibilityVerification { get; set; }

        public YesNoOtherEnum? InternationalWorkHistory { get; set; }

        public YesNoOtherEnum? ProfessionalReferenceChecks { get; set; }

        public YesNoOtherEnum? FormI9 { get; set; }

        public YesNoOtherEnum? FormEVerify { get; set; }

        public PassFailEnum? FingerPrinting { get; set; }
        public string FingerPrintingOther { get; set; }

        public PassFailEnum? DrugScreening { get; set; }
        public string DrugScreeningOther { get; set; }

        public PassFailEnum? CreditCheck { get; set; }
        public string CreditCheckOther { get; set; }

        #endregion

        #region Retirement Benefits

        public bool? RbParticipating { get; set; }
        public string RbEligibleWages { get; set; }
        public string RbEmployeeContribution { get; set; }
        public string RbEmployerMatching { get; set; }


        #endregion

        public long? ResourceTeamId { get; set; }

        public virtual ResourceTeam ResourceTeam { get; set; }

        public bool? CollegeDegree { get; set; }

        public bool? EverBeenConvictedOfACrime { get; set; }

        public bool? InformationCertifiedByEmployer { get; set; }

        public string ProofOfCitizenship { get; set; }

        public decimal? HourlyRate { get; set; }

        public RangeTypeEnum? Range { get; set; }

        public string PhotoFileName { get; set; }

        public string CvFileName { get; set; }

        public double? Productivity { get; set; }

        public double? Rating { get; set; }

        public DateTime? RatingDate { get; set; }

        public string MotorVehicleReportOther { get; set; }

        public string ProfessionalReferenceChecksOther { get; set; }

        public string EmploymentEligibilityVerificationOther { get; set; }

        public string InternationalWorkHistoryOther { get; set; }

        public YesNoOtherEnum? CredentialVerifications { get; set; }

        public string CredentialVerificationsOther { get; set; }

        public string FormI9Other { get; set; }

        public string FormEVerifyOther { get; set; }

        /// <summary>
        /// Need to show Resource entity as Picklist
        /// </summary>
        public string Name
        {
            get { return string.Concat(LastName ?? string.Empty, " ", FirstName ?? string.Empty); }
        }

        public long? BusinessUnitId { get; set; }
        public virtual DropDownConfig BusinessUnit { get; set; }

        public string CubicalOrOfficeNumber { get; set; }

        public virtual ICollection<ResourceSpokenLanguage> SpokenLanguages { get; set; }

        public virtual ICollection<ResourceEducation> Educations { get; set; }

        public virtual ICollection<ResourceTravel> Travels { get; set; }

        public virtual ICollection<ResourceBehaviouralDetail> ResourceBehaviouralDetails { get; set; }

        public virtual ICollection<ResourceIndustry> Industry { get; set; }

        public virtual ICollection<ResourceRecognition> ResourceRecognitions { get; set; }

        public virtual ICollection<ResourcePracticeArea> ResourcePracticeAreas { get; set; }

        public virtual ICollection<ResourceCriminalRecord> CriminalRecords { get; set; }

        public virtual ICollection<ResourcePreviousEmployment> PreviousEmployments { get; set; }

        public virtual ICollection<ResourceCertificate> Certificates { get; set; }

        public virtual ICollection<ResourceSkill> Skills { get; set; }

        public virtual ICollection<ResourceSurveyResult> SurveyResults { get; private set; }

        public virtual ICollection<ResourceCustomFieldValue> CustomFieldValues { get; set; }

        public virtual ICollection<ResourceWorkExperience> ResourceWorkExperiences { get; set; }

        public virtual ICollection<ResourceProfessionalOrganization> ResourceProfessionalOrganizations { get; set; }

        public virtual ICollection<ResourceInsurance> ResourceInsurances { get; set; }

        public virtual ICollection<ResourceTimeoffBenefit> ResourceTimeoffBenefits { get; set; }

    }
}
