﻿using System;
using PatientCare.Core.Enums;
using FluentValidation;
using Microsoft.Ajax.Utilities;

namespace PatientCare.Web.Api.Models.ResourcePreviousEmployments
{
    public class ResourcePreviousEmploymentRequestValidator : AbstractValidator<ResourcePreviousEmploymentRequest>
    {
        public ResourcePreviousEmploymentRequestValidator()
        {
            RuleFor(model => model.StartDate)
                .NotEmpty();

            RuleFor(model => model.EndDate)
                .NotEmpty();

            RuleFor(model => model.SalaryPer)
                .NotNull()
                .Must(itemType => itemType != null && Enum.IsDefined(typeof(SalaryPerTypeEnum), itemType));
        }
    }
}