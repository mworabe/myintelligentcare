﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IHepDetailRepository : IRepository<HepDetail>
    {
    }
}
