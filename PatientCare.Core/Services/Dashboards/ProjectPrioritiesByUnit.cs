﻿namespace PatientCare.Core.Services.Dashboards
{
    public class ProjectPrioritiesByUnit
    {
        public string UnitName { get; set; }
        public int Low { get; set; }
        public int Medium { get; set; }
        public int High { get; set; }
    }
}
