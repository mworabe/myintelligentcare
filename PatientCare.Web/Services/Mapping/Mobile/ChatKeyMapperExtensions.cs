﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.ChatSessions;
using EmitMapper.MappingConfiguration;
using PatientCare.Web.ApiMobile.Models.ChatMembers;
using System.Linq;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class ChatKeyMapperExtensions
    {
        public static EmitMapperMapperProvider ChatKeyRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ChatKey, ChatKeyResponse>
                    (new ExtendedMapConfig<ChatKey, ChatKeyResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ChatKey[], ChatKeyResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ChatKeyRequest, ChatKey>
                    (new ExtendedMapConfig<ChatKeyRequest, ChatKey>()));

            return mapperProvider;
        }
    }
}