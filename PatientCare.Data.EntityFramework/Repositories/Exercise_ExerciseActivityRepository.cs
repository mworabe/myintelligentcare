﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class Exercise_ExerciseActivityRepository : RepositoryBase<Exercise_ExerciseActivity>, IExercise_ExerciseActivityRepository
    {
        public Exercise_ExerciseActivityRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
