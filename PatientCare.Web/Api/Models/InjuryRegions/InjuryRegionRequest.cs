﻿namespace PatientCare.Web.Api.Models.InjuryRegions
{
    public class InjuryRegionRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }   
}