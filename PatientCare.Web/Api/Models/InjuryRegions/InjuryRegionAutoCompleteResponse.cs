﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.InjuryRegions
{
    [DataContract]
    public class InjuryRegionAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}