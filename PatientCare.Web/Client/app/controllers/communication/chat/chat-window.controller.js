﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("ChatWindowCtrl", [
        "$scope",
        "$state",
        'TwilioChatFactory',
        'ChatService',
        "Auth",
        '$stateParams',
        '$rootScope',
        function ($scope, $state, TwilioChatFactory, ChatService, Auth, $stateParams, $rootScope) {
            var currentUser = Auth.getCurrentUser();
            var client = TwilioChatFactory.getClient();

            $scope.patientList = [];
            $scope.stafList = [];

            $scope.tabs = [
              { id: "patient", title: "Patients" },
              { id: "staff", title: "Staff" }
            ];

            $scope.selectedTab = $scope.tabs[0].id;
            $scope.isTabSelected = function (id) {
                if (id == $scope.selectedTab) return true;
                return false;
            }
            $scope.setSelectedTab = function (id) {
                $scope.selectedTab = id;
            }

            $scope.openChat = function (item) {
                $state.go("communication.chat.chat-box", { sessionObject: item }, { reload: "communication.chat.chat-box" });
            }
            $scope.session = $stateParams.sessionObject;

            if ($scope.session) {
                
                TwilioChatFactory.setCurrentUser($scope.session);
                $scope.openChat($scope.session);
                if ($scope.session.otherUser &&
                               $scope.session.otherUser.role &&
                               $scope.session.otherUser.role == "Patient") {
                    $scope.selectedTab = $scope.tabs[0].id;
                } else {
                    $scope.selectedTab = $scope.tabs[1].id;
                }
            }

            
            
            

            $scope.loadData = function (InBackground) {
                var promise=ChatService.getChatSession({ UserId: currentUser.nameid }, function (result) {
                    $scope.patientList = [];
                    $scope.stafList = [];
                    if (result && result.chatDetails) {
                        angular.forEach(result.chatDetails, function (item) {
                            TwilioChatFactory.setCurrentUser(item);
                            if (item.otherUser &&
                                item.otherUser.role &&
                                item.otherUser.role == "Patient")
                            {
                                $scope.patientList.push(item);
                            } else {
                                $scope.stafList.push(item);
                            }
                        });
                    }
                }, function (error) {
                })
                if (!InBackground) {
                    $scope.isLoading = promise;
                }
            }

            $scope.loadData(false);

            $rootScope.$on("channelInvited", function () {
                $scope.loadData(true);
            });

          

        }]);