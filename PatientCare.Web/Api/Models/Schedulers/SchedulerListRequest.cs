﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Schedulers
{
    public class SchedulerListRequest : ListRequest
    {
        [DataMember(Name = "from")]
        public DateTime? From { get; set; }

        [DataMember(Name = "to")]
        public DateTime? To { get; set; }

        [DataMember(Name = "clinician")]
        public string Clinician { get; set; }

        [DataMember(Name = "clinic")]
        public string Clinic { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

    }
}