"use strict";
(function edzTimeCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzTimeCustomField", ["edzBaseCustomField", edzTimeCustomField]);

    ///////////////////////////////////////////////////////
    function edzTimeCustomField(baseCustomField) {
        function TimeCustomField() { }

        TimeCustomField.prototype = Object.create(baseCustomField);
        TimeCustomField.prototype.optionsTemplate = "<edz-time-custom-field-options></edz-time-custom-field-options>";
        TimeCustomField.prototype.optionsTemplateEdit = "<edz-time-custom-field-edit-options></edz-time-custom-field-edit-options>";
        return TimeCustomField;
    }
})();