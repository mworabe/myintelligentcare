﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq;
using PatientCare.Web.Api.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Web.Api.Models.HepMasters;
using PatientCare.Web.Services.FileStorage;
using System.Collections.Generic;
using PatientCare.Web.Api.Models.HepDetails;
using PatientCare.Web.Api.Models.HepGroups;
using System.Net.Http.Headers;
using PatientCare.Web.Services.PDFUtils;
using System.Text;
using System.Net.Http;
using System.IO;
using System.Net;
using PatientCare.Web.Api.Models.Patients;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.PatientCases;
using System.Data;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Api.Models.HepFrequencies;
using System.Net.Mail;
using System.Threading;
using PatientCare.Core.Services.Mailing;
using PatientCare.Core.Enums;
using System.Web.Script.Serialization;
using PatientCare.Web.ApiMobile.Models.HepMasters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Hep Master operations.
    /// </summary>
    //[Authorize]
    public sealed class HepMastersController : ApiBaseController
    {
        /// <summary>
        /// Description : Hep Master IHepService.
        /// </summary>
        private readonly IHepService _hepService;
        /// <summary>
        /// Description : Hep Master IHepGroupRepository.
        /// </summary>
        private readonly IHepGroupRepository _hepGroupRepository;
        /// <summary>
        /// Description : Hep Master IHepMasterRepository.
        /// </summary>
        private readonly IHepMasterRepository _hepMasterRepository;
        /// <summary>
        /// Description : Hep Master IHepDetailRepository.
        /// </summary>
        private readonly IHepDetailRepository _hepDetailRepository;
        /// <summary>
        /// Description : Hep Master IHepFrequencyRepository.
        /// </summary>
        private readonly IHepFrequencyRepository _hepFrequencyRepository;
        /// <summary>
        /// Description : Hep Master IExerciseRepository.
        /// </summary>
        private readonly IExerciseRepository _exerciseRepository;
        /// <summary>
        /// Description : Hep Master IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : Hep Master IEmailSender.
        /// </summary>
        private readonly IEmailSender _emailSender;
        /// <summary>
        /// Description : Hep Master IPatientRepository.
        /// </summary>
        private readonly IPatientRepository _patientRepository;
        /// <summary>
        /// Description : Hep Master IPatientCaseRepository.
        /// </summary>
        private readonly IPatientCaseRepository _patientCaseRepository;


        ReadOptions<HepMaster> _readOptions = new ReadOptions<HepMaster>();

        /// <summary>
        /// Description : Initializes a new instance of the Hep Master Class. 
        /// </summary>
        /// <param name="hepService">IHep Service</param>
        /// <param name="hepGroupRepository">IHep Group Repository</param>
        /// <param name="hepMasterRepository">IHep Master Repository</param>
        /// <param name="hepDetailRepository">IHep Detail Repository</param>
        /// <param name="hepFrequencyRepository">IHep Frequency Repository</param>
        /// <param name="exerciseRepository">IExercise Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        /// <param name="emailSender">IEmail Sender</param>
        /// <param name="patientRepository">IPatient Repository</param>
        /// <param name="patientCaseRepository">IPatient Case Repository</param>
        public HepMastersController
            (
                IHepService hepService,
                IHepGroupRepository hepGroupRepository,
                IHepMasterRepository hepMasterRepository,
                IHepDetailRepository hepDetailRepository,
                IHepFrequencyRepository hepFrequencyRepository,
                IExerciseRepository exerciseRepository,
                IDataContextScopeFactory dataContextScopeFactory,
                IEmailSender emailSender,
                IPatientRepository patientRepository,
                IPatientCaseRepository patientCaseRepository
            )
        {
            _hepService = hepService;
            _hepGroupRepository = hepGroupRepository;
            _hepMasterRepository = hepMasterRepository;
            _hepDetailRepository = hepDetailRepository;
            _hepFrequencyRepository = hepFrequencyRepository;
            _exerciseRepository = exerciseRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
            _emailSender = emailSender;
            _patientRepository = patientRepository;
            _patientCaseRepository = patientCaseRepository;

        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/HepMasters/" + id, UriKind.Relative);
        }


        /// <summary>
        /// Method Description : Using for Get All Hep Master.
        /// </summary>
        /// <param name="request">Hep Master List Request Model</param>
        /// <returns>Hep Master Response Model</returns>
        [Authorize]
        [HttpGet]
        [Route("api/HepMasters")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<HepMasterListResponse>))]
        public IHttpActionResult GetAllHepMaster([FromUri]HepMasterListRequest request)
        {
            var query = PagingExtensions<HepMaster>.CreatePagedQuery
                      (request
                           //(string.IsNullOrWhiteSpace(request.SearchField)
                           //    && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                           //        ?
                           //   GetSearchColumnExpressions(request.SearchPhrase)
                           //  : GetAutocompleteSearchExpressions(request.SearchPhrase)
                           // )
                           );

            var readOptions = new ReadOptions<HepMaster>().AsReadOnly();
            var page = _hepMasterRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<HepMaster[], HepMasterResponse[]>(page.Entities.ToArray());
            var response = new ListResponse<HepMasterResponse>(dtos, page.TotalCount);

            foreach (var exercise in response.Data)
            {
                foreach (var file1 in exercise.HepDetails)
                {
                    foreach (var file in file1.Exercise.Medias)
                    {
                        file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(exercise.Id));
                        file.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(file.Name);

                        List<FileRequest> fileAll = new List<FileRequest>();
                        FileRequest tempFile = new FileRequest();

                        if (StorageService.UseAmazonStorage == true)
                            tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(file.Name, GetExerciseMediaS3Path(file.ExerciseId.Value));
                        else
                            tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + file.Id;

                        tempFile.S3BucketName = file.Name;
                        tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                        #region

                        if (file.ThumbImageName != null)
                        {
                            file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(exercise.Id));
                            file.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);

                            if (StorageService.UseAmazonStorage == true)
                                tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(file.ThumbImageName, GetExerciseMediaS3Path(file.ExerciseId.Value));
                            else
                                tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                            tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                        }
                        #endregion

                        fileAll.Add(tempFile);
                        file.Files = fileAll.ToArray();
                    }
                }
            }

            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Hep Master.   
        /// </summary>
        /// <param name="HepMasterRequest">Hep Master Request Model</param>
        /// <returns>Hep Master Response Model</returns>
        [Authorize]
        [HttpPost]
        [Route("api/HepMasters")]
        [SwaggerResponse(201, "Created", typeof(HepMasterResponse))]
        public async Task<IHttpActionResult> CreateHepMaster([FromBody]HepMasterRequest HepMasterRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                ModelState.AddModelError("Hep Master", "your session is expired. please login again");
                return BadRequest(ModelState);
            }
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user == null && user.Resource == null)
            {
                ModelState.AddModelError("Hep Master", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }

            var HepMaster = Mapper.Map<HepMasterRequest, HepMaster>(HepMasterRequest);
            HepMaster.OwnerId = user.ResourceId;

            await _hepService.CreateAsync(HepMaster);

            return Created(GetEntityLocation(HepMaster.Id), Mapper.Map<HepMaster, HepMasterResponse>(HepMaster));
        }

        /// <summary>
        /// Method Description : Using for Update Hep Master.   
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="HepMasterRequest">Hep Master Request Model</param>
        /// <returns>Hep Master Response Model</returns>
        [Authorize]
        [HttpPut]
        [Route("api/HepMasters/{Id}")]
        public async Task<IHttpActionResult> EditHepMaster(long Id, [FromBody] HepMasterRequest HepMasterRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                ModelState.AddModelError("Hep Master", "your session is expired. please login again");
                return BadRequest(ModelState);
            }
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user == null && user.Resource == null)
            {
                ModelState.AddModelError("Hep Master", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }

            var HepMaster = Mapper.Map<HepMasterRequest, HepMaster>(HepMasterRequest);
            HepMaster.Id = Id;
            HepMaster.OwnerId = user.ResourceId;

            await _hepService.UpdateAsync(HepMaster);

            return Ok(Mapper.Map<HepMaster, HepMasterResponse>(HepMaster));
        }

        /// <summary>
        /// Method Description : Using for Delete Hep Master by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Hep Master Response Model</returns>
        [Authorize]
        [HttpDelete]
        [Route("api/HepMasters/{Id}")]
        public async Task<IHttpActionResult> DeleteHepMasterItem(long id)
        {
            //var statusCodeResult = GetStatusCodeError(Crud.Delete, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            var item = _hepMasterRepository.Read(id);
            if (item == null)
            {
                return NotFound();
            }

            var errorMessage = await _hepService.DeleteAsync(item);
            if (string.IsNullOrEmpty(errorMessage))
            {
                return Ok(item);
            }

            ModelState.AddModelError("HepMaster", errorMessage);
            return BadRequest(ModelState);

            //var success = await _hepService.DeleteHepMasterAsync(id);
            //if (!success)
            //{
            //    return NotFound();
            //}

            //return Ok(new HepMaster { Id = id });
        }

        //[HttpGet]
        //[Route("api/HepMasters/{Id}")]
        //public async Task<IHttpActionResult> GetHepMaster(long Id)
        //{
        //    var HepMaster = await _hepService.ReadAsync(Id);
        //    if (HepMaster == null)
        //        return NotFound();

        //    var viewModel = Mapper.Map<HepMaster, HepMasterResponse>(HepMaster);

        //    foreach (var file in viewModel.HepDetails)
        //    {
        //        foreach (var exercise in file.Exercise.Medias)
        //        {
        //            exercise.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.Name, GetExerciseMediaS3Path(exercise.ExerciseId.Value));
        //            exercise.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

        //            List<FileRequest> fileAll = new List<FileRequest>();
        //            FileRequest tempFile = new FileRequest();

        //            if (StorageService.UseAmazonStorage == true)
        //                tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(exercise.Name, GetExerciseMediaS3Path(exercise.ExerciseId.Value));
        //            else
        //                tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + exercise.Id;

        //            tempFile.S3BucketName = exercise.Name;
        //            tempFile.Name = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

        //            fileAll.Add(tempFile);
        //            exercise.Files = fileAll.ToArray();
        //        }
        //    }

        //    return Ok(viewModel);
        //}

        private string GetExerciseMediaS3Path(long exerciseId)
        {
            return string.Format(FileUploadHelpers.ExerciseMediaFilesTemplate, exerciseId);
        }

        /// <summary>
        /// Method Description : Using for Get Hep Master by Case Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Hep Master Response Model</returns>
        [Authorize]
        [HttpGet]
        [Route("api/HepMasters/{Id}/case")]
        [SwaggerResponse(200, "Ok", typeof(HepMasterGroupResponse))]
        public IHttpActionResult GetHepMasterByCase(long id) // id = patient case id
        {
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                bool? isDeletedCase = ctx.PatientCases.AsNoTracking().Where(x => x.Id == id && (x.IsDeleted == false || x.IsDeleted == null)).Select(x => x.IsDeleted).FirstOrDefault();
                if (isDeletedCase == true)
                    return NotFound();
            }

            var readOptions = new ReadOptions<HepMaster>().AsReadOnly();
            var response = new HepMasterGroupResponse();

            response = MapCurrentCaseHEP(id, response, "");
            if (response == null)
            {
                return Ok();
            }
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Get Hep Master by Case Id , Patient Id.   
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="patientId">Patient Id</param>
        /// <param name="type">Type</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/HepMasters/{Id}/{patientId}/{type}/export_hep")]
        public HttpResponseMessage ExportHEPDetail(long Id, string patientId, HepExportStatusEnum type) // here id = caseId
        {
            byte[] pdfByteArray = null;

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                bool? isDeletedCase = ctx.PatientCases.AsNoTracking().Where(x => x.Id == Id && (x.IsDeleted == false || x.IsDeleted == null)).Select(x => x.IsDeleted).FirstOrDefault();
                if (isDeletedCase == true)
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            var response = new HepMasterGroupResponse();

            response = MapCurrentCaseHEP(Id, response, "export");

            long hepId = response.Id;
            String caseNumber = response.PatientCase.CaseNo;
            String patientFullName = response.Patient.LastName + " " + response.Patient.FirstName;

            if (type == HepExportStatusEnum.Email)
            {
                pdfByteArray = getPDFForHEP(response);
            }

            var message = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(getPDFForHEP(response))
            };

            //message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            string pdfFileName = patientFullName + "_HEP_" + caseNumber + ".pdf";
            if (type == HepExportStatusEnum.Print)
            {
                message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
            }
            else if (type == HepExportStatusEnum.Export)
            {
                message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                //{
                //    FileName = patientFullName + "_HEP_" + caseNumber + ".pdf"
                //};
            }

            if (type == HepExportStatusEnum.Email)
            {
                string mailSubject = "Your Health Exercise Plan";
                string mailBody = "Please find the attached health exercise plan for your Case Number : " + caseNumber;
                List<string> emailList = new List<string>();
                //foreach (string patientId in patientList)
                //{
                var paatient = _patientRepository.Read(Convert.ToInt64(patientId), ReadOptions.Create<Patient>().AsReadOnly());
                if (paatient.Email != null && paatient.Email.Length > 0)
                {
                    emailList.Add(paatient.Email);
                }
                //}
                if (emailList.Count > 0)
                {
                    sendHepPDFAsEmail(mailSubject, mailBody, emailList, pdfByteArray, pdfFileName);
                }
            }

            if (type == HepExportStatusEnum.Email)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            message.Content.Headers.ContentDisposition.FileName = pdfFileName;

            return message;
        }

        private byte[] getPDFForHEP(HepMasterGroupResponse hepDetail)
        {
            #region New Colored Resume Pdf Code 
            StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Templates/PdfTemplates/PdfHepExportTemplate.html"));
            string readFile = reader.ReadToEnd();
            string StrContent = "";

            StrContent = readFile;
            StringBuilder sb = new StringBuilder();


            #region Coman Section for Converting Pdf
            StrContent = StrContent.Replace("[patientname]", hepDetail.Patient.Name);
            StrContent = StrContent.Replace("[casenumber]", hepDetail.PatientCase.CaseNo);
            StrContent = StrContent.Replace("[startdate]", hepDetail.StartDate != null ? hepDetail.StartDate.Value.Date.ToShortDateString() : null);
            StrContent = StrContent.Replace("[enddate]", hepDetail.EndDate != null ? hepDetail.EndDate.Value.Date.ToShortDateString() : null);
            #endregion

            #region HEP Group DETAIL SECTION
            if (hepDetail.HepGroups != null && hepDetail.HepGroups.Count() > 0)
            {
                //DataTable HepDetailsTable = new DataTable();

                foreach (var HepGroup in hepDetail.HepGroups)
                {
                    sb.Append(getHEPGroupHTMLTemplate(HepGroup));
                    foreach (var HepDetail in HepGroup.HepDetails)
                    {
                        sb.Append(getHEPExericseHTMLTemplate(HepDetail, true));
                    }
                }
            }

            StrContent = StrContent.Replace("[hepdetails]", sb.ToString());
            #endregion

            reader.Close();
            return PdfUtils.genratePDF(StrContent, iTextSharp.text.PageSize.A4);
            #endregion
        }

        public String getHEPExericseHTMLTemplate(HepDetailResponse hepDetail, bool insideGroup)
        {
            String image = "";
            if (hepDetail.Exercise.Medias != null && hepDetail.Exercise.Medias.Count() > 0)
            {

                if (hepDetail.Exercise.Medias[0].MediaType == Core.Enums.MediaType.Video)
                {
                    image = hepDetail.Exercise.Medias[0].ThumbURL;
                }
                else
                {
                    image = hepDetail.Exercise.Medias[0].MediaURL;
                }
            }
            HepFrequencyResponse[] frequencieArray = hepDetail.Frequencies;
            int sunFrequency = 0, monFrequency = 0, tueFrequency = 0, wedFrequency = 0, thuFrequency = 0, friFrequency = 0, satFrequency = 0;
            if (frequencieArray != null && frequencieArray.Count() > 0)
            {
                foreach (HepFrequencyResponse frequency in frequencieArray)
                {
                    if (frequency.Day == 0) sunFrequency = frequency.ExerciseFrequency != null ? frequency.ExerciseFrequency.Value : 0;
                    else if (frequency.Day == 0) sunFrequency = frequency.ExerciseFrequency != null ? frequency.ExerciseFrequency.Value : 0;
                    else if (frequency.Day == 1) monFrequency = frequency.ExerciseFrequency != null ? frequency.ExerciseFrequency.Value : 0;
                    else if (frequency.Day == 2) tueFrequency = frequency.ExerciseFrequency != null ? frequency.ExerciseFrequency.Value : 0;
                    else if (frequency.Day == 3) wedFrequency = frequency.ExerciseFrequency != null ? frequency.ExerciseFrequency.Value : 0;
                    else if (frequency.Day == 4) thuFrequency = frequency.ExerciseFrequency != null ? frequency.ExerciseFrequency.Value : 0;
                    else if (frequency.Day == 5) friFrequency = frequency.ExerciseFrequency != null ? frequency.ExerciseFrequency.Value : 0;
                    else if (frequency.Day == 6) satFrequency = frequency.ExerciseFrequency != null ? frequency.ExerciseFrequency.Value : 0;
                }
            }

            StringBuilder sb = new StringBuilder();
            return @"<table class='" + (insideGroup ? @"margin-inside" : @"") + @"' style='width:100%;margin-bottom:10px;'>
               < tr>
                <td colspan='2' style='background:#EEEEEE;padding:5px'><b>" + hepDetail.Exercise.Name + @"</b></td>
            </tr>

            <tr>
                <td style='width:210px;height:210px;'>
                    <img style='width:210px;' src='" + image + @"' />
                </td>
                <td align='left' valign='top' style='padding-left:10px'>
                    <table style='width:100%'>
                        <tr>
                            <td>
                                <span class='property_class'>Sets : " + ((hepDetail.Sets != null && hepDetail.Sets > 0) ? "" + hepDetail.Sets : "-") + @"</span>
                                <span class='property_class'>Reps : " + ((hepDetail.Reps != null && hepDetail.Reps > 0) ? "" + hepDetail.Reps : " - ") + @"</span>
                                <span class='property_class'>Weight : " + ((hepDetail.Weight != null && hepDetail.Weight > 0) ? "" + hepDetail.Weight : " - ") +
                                                  ((hepDetail.WeightUnit != null) ? " " + hepDetail.WeightUnit : "") + @"</span>
                                <span class='property_class'>Holds : " + ((hepDetail.Holds != null && hepDetail.Holds > 0) ? "" + hepDetail.Holds : " - ") +
                                                 ((hepDetail.HoldsUnit != null) ? " " + hepDetail.HoldsUnit : "") + @"</span>
                                <span class='property_class'>Time : " + ((hepDetail.Time != null && hepDetail.Time > 0) ? "" + hepDetail.Time : " - ") +
                                                 ((hepDetail.TimeUnit != null) ? " " + hepDetail.TimeUnit : "") + @"</span>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>

                                <table class='frequency-table' style='width:100%'>
                                    <thead><tr><td align='left' colspan='7'><span style='color:gray;font-size:14px'>Frequency</span></td></tr></thead>
                                    <tbody>
                                        <tr><td style='width:20px'>S</td>
                                            <td style='width:20px'>M</td>
                                            <td style='width:20px'>T</td>
                                            <td style='width:20px'>W</td>
                                            <td style='width:20px'>T</td>
                                            <td style='width:20px'>F</td>
                                            <td style='width:20px'>S</td></tr>
                                        <tr>
                                            <td> " + (sunFrequency != 0 ? "" + satFrequency : @"-") + @"</td>" +
                                            "<td> " + (monFrequency != 0 ? "" + monFrequency : @"-") + @"</td>" +
                                            "<td> " + (tueFrequency != 0 ? "" + tueFrequency : @"-") + @"</td>" +
                                            "<td> " + (wedFrequency != 0 ? "" + wedFrequency : @"-") + @"</td>" +
                                            "<td> " + (thuFrequency != 0 ? "" + thuFrequency : @"-") + @"</td>" +
                                            "<td> " + (friFrequency != 0 ? "" + friFrequency : @"-") + @"</td>" +
                                            "<td> " + (satFrequency != 0 ? "" + satFrequency : @"-") + @"</td>
                                        </tr>                                    
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class='comment_box'>
                                <table style='width:100%;'>
                                <tr><td>
                                <span style='display:block;margin-bottom:5px; text-decoration:underline'>Comments</span>
                                </td></tr>
                                <tr><td class='comment'> " + hepDetail.Comments + @" </td></tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>";

            return sb.ToString();
        }

        public String getHEPGroupHTMLTemplate(HepGroupResponse hepGroup)
        {
            //  sb.Append("<tr> <td style='vertical-align:top;text-align:justify;width:30%'>" + "<img  style='height:300px;width:210px;' src='" + ((HepDetail.Exercise != null) ? ((HepDetail.Exercise.Medias.Count() > 0) ? HepDetail.Exercise.Medias[0].MediaURL : null) : null) + "'/>" + " </td> <td style='width:30%'>" + "Name Comments" + " </td><td style='width:40%'><table style='width:100%'>  <tr><td>sets</td> <td>" + HepDetail.Sets + " </td>  </tr><tr><td>Reps</td><td>" + HepDetail.Reps + " </td></tr><tr><td colspan='2'><table style='width:100%'><tr><td>Time</td><td>" + HepDetail.Time + " </td><td>TimeUnit</td><td>" + HepDetail.TimeUnit + " </td></tr></table><table style='width:100%'><tr><td style='width:30%'>Weight</td><td>" + HepDetail.Weight + " </td><td>WeightUnit</td><td>" + HepDetail.WeightUnit + " </td></tr></table></td></tr><tr><td colspan='3'><table style='width:100%'><tr><td>Holds</td><td>" + HepDetail.Holds + " </td><td>HoldsUnit</td><td>" + HepDetail.WeightUnit + " </td></tr></table></td></tr><tr><td>Define Frequency</td><td>" + HepDetail.Frequencies + " </td></tr></table></td></tr>");
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(@"<table style='width:100%'>
                                <tr>
                                    <td colspan='2' style='background:#b5cde6;padding:5px;color:black'><b>Sample Exercise</b></td>
                                </tr>
                            </table>", hepGroup.Name);

            return sb.ToString();
        }

        public void sendHepPDFAsEmail(String subject, String body, List<String> emailList, byte[] pdfBytes, String pdfFileName)
        {
            #region SEND HEP Pdf EMAIL 
            var msgs = new List<MailMessage>();

            foreach (var reviewerEmail in emailList)
            {
                if (string.IsNullOrEmpty(reviewerEmail))
                    continue;

                var mail = new MailMessage();
                Attachment att = new Attachment(new MemoryStream(pdfBytes), pdfFileName);

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = false;
                mail.Attachments.Add(att);
                mail.To.Add(reviewerEmail);

                msgs.Add(mail);
            }

            ThreadPool.QueueUserWorkItem(o => _emailSender.SendMailAsync(msgs));
            #endregion
        }

        private HepMasterGroupResponse MapCurrentCaseHEP(long? patientCaseId, HepMasterGroupResponse response, string type)
        {
            var page = _hepMasterRepository.List(new FilterQuery<HepMaster>().AddFilter(x => x.PatientCaseId == patientCaseId)).FirstOrDefault();
            if (page != null)
            {
                var hepMaster = page;
                response.Id = hepMaster.Id;
                response.PatientId = hepMaster.PatientId;
                if (type == "export")
                {
                    response.Patient = Mapper.Map<Patient, PatientAutoCompleteResponse>(hepMaster.Patient);
                    response.Owner = Mapper.Map<Resource, PicklistDefaultResponse>(hepMaster.Owner);
                    response.PatientCase = Mapper.Map<PatientCase, PatientCaseAutoCompleteResponse>(hepMaster.PatientCase);
                }
                response.OwnerId = hepMaster.OwnerId;
                response.PatientCaseId = hepMaster.PatientCaseId;
                response.StartDate = hepMaster.StartDate;
                response.EndDate = hepMaster.EndDate;
                response.IsSaveAsDraft = hepMaster.IsSaveAsDraft;
                response.HepDetails = Mapper.Map<HepDetail[], HepDetailResponse[]>(hepMaster.HepDetails.Where(x => x.GroupId == null).ToArray());

                foreach (var file in response.HepDetails)
                {
                    if (file.Exercise != null && file.Exercise.Medias != null)
                    {
                        foreach (var exercise in file.Exercise.Medias)
                        {
                            exercise.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.Name, GetExerciseMediaS3Path(file.Exercise.Id));
                            exercise.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                            List<FileRequest> fileAll = new List<FileRequest>();
                            FileRequest tempFile = new FileRequest();

                            if (StorageService.UseAmazonStorage == true)
                                tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(exercise.Name, GetExerciseMediaS3Path(file.Exercise.Id));
                            else
                                tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + exercise.Id;

                            tempFile.S3BucketName = exercise.Name;
                            tempFile.Name = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                            #region

                            if (exercise.ThumbImageName != null)
                            {
                                exercise.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.ThumbImageName, GetExerciseMediaS3Path(file.Exercise.Id));
                                exercise.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);

                                if (StorageService.UseAmazonStorage == true)
                                    tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(exercise.ThumbImageName, GetExerciseMediaS3Path(file.Exercise.Id));
                                else
                                    tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                                tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);
                            }
                            #endregion

                            fileAll.Add(tempFile);
                            exercise.Files = fileAll.ToArray();
                        }
                    }
                }

                List<HepGroupResponse> groups = new List<HepGroupResponse>();
                foreach (var item in hepMaster.HepGroups)
                {
                    HepGroupResponse group = new HepGroupResponse();
                    group.Id = item.Id;
                    group.Name = item.Name;
                    group.PatientId = item.PatientId;
                    //group.Patient = Mapper.Map<Patient, PatientAutoCompleteResponse>(item.Patient);
                    group.PatientCaseId = item.PatientCaseId;
                    //group.PatientCase = Mapper.Map<PatientCase, PatientCaseAutoCompleteResponse>(item.PatientCase);
                    group.HepId = item.HepId;
                    group.ExerciseOrder = item.ExerciseOrder;
                    group.HepDetails = Mapper.Map<HepDetail[], HepDetailResponse[]>(item.HepDetails.ToArray());

                    foreach (var file in group.HepDetails)
                    {
                        if (file.Exercise != null && file.Exercise.Medias != null)
                        {
                            foreach (var exercise in file.Exercise.Medias)
                            {
                                exercise.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.Name, GetExerciseMediaS3Path(file.Exercise.Id));
                                exercise.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                                List<FileRequest> fileAll = new List<FileRequest>();
                                FileRequest tempFile = new FileRequest();

                                if (StorageService.UseAmazonStorage == true)
                                    tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(exercise.Name, GetExerciseMediaS3Path(file.Exercise.Id));
                                else
                                    tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + exercise.Id;

                                tempFile.S3BucketName = exercise.Name;
                                tempFile.Name = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                                #region

                                if (exercise.ThumbImageName != null)
                                {
                                    exercise.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.ThumbImageName, GetExerciseMediaS3Path(file.Exercise.Id));
                                    exercise.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);

                                    if (StorageService.UseAmazonStorage == true)
                                        tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(exercise.ThumbImageName, GetExerciseMediaS3Path(file.Exercise.Id));
                                    else
                                        tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                                    tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);
                                }
                                #endregion

                                fileAll.Add(tempFile);
                                exercise.Files = fileAll.ToArray();
                            }
                        }
                    }

                    groups.Add(group);
                }
                response.HepGroups = groups.ToArray();
                return response;
            }
            return null;
        }

        /// <summary>
        /// Method Description : Using for Get Hep Group by Patient Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Favourite Template Response Model</returns>
        [Authorize]
        [HttpGet]
        [Route("api/HepMasters/{Id}")] //Id is Patient Id
        [SwaggerResponse(200, "Ok", typeof(HepMasterGroupResponse))]
        public IHttpActionResult GetHepMaster(long id)
        {
            var readOptions = new ReadOptions<HepMaster>().AsReadOnly();
            var response = new HepMasterGroupResponse();

            response = MapPatientHEP(id, response);
            if (response == null)
            {
                return Ok();
            }
            return Ok(response);
        }

        private HepMasterGroupResponse MapPatientHEP(long? patientId, HepMasterGroupResponse response)
        {
            var page = _hepMasterRepository.List(new FilterQuery<HepMaster>().AddFilter(x => x.Id == patientId)).FirstOrDefault();
            if (page != null)
            {
                var hepMaster = page;
                response.Id = hepMaster.Id;
                response.PatientId = hepMaster.PatientId;
                //response.Patient = Mapper.Map<Patient, PatientAutoCompleteResponse>(hepMaster.Patient);
                response.OwnerId = hepMaster.OwnerId;
                //response.Owner = Mapper.Map<Resource, PicklistDefaultResponse>(hepMaster.Owner);
                response.PatientCaseId = hepMaster.PatientCaseId;
                //response.PatientCase = Mapper.Map<PatientCase, PatientCaseAutoCompleteResponse>(hepMaster.PatientCase);
                response.StartDate = hepMaster.StartDate;
                response.EndDate = hepMaster.EndDate;
                response.IsSaveAsDraft = hepMaster.IsSaveAsDraft;
                response.HepDetails = Mapper.Map<HepDetail[], HepDetailResponse[]>(hepMaster.HepDetails.Where(x => x.GroupId == null).ToArray());

                foreach (var file in response.HepDetails)
                {
                    if (file.Exercise != null && file.Exercise.Medias != null)
                    {
                        foreach (var exercise in file.Exercise.Medias)
                        {
                            exercise.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.Name, GetExerciseMediaS3Path(file.Exercise.Id));
                            exercise.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                            List<FileRequest> fileAll = new List<FileRequest>();
                            FileRequest tempFile = new FileRequest();

                            if (StorageService.UseAmazonStorage == true)
                                tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(exercise.Name, GetExerciseMediaS3Path(file.Exercise.Id));
                            else
                                tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + exercise.Id;

                            tempFile.S3BucketName = exercise.Name;
                            tempFile.Name = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                            #region

                            if (exercise.ThumbImageName != null)
                            {
                                exercise.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.ThumbImageName, GetExerciseMediaS3Path(file.Exercise.Id));
                                exercise.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);

                                if (StorageService.UseAmazonStorage == true)
                                    tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(exercise.ThumbImageName, GetExerciseMediaS3Path(file.Exercise.Id));
                                else
                                    tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                                tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);
                            }
                            #endregion

                            fileAll.Add(tempFile);
                            exercise.Files = fileAll.ToArray();
                        }
                    }
                }

                List<HepGroupResponse> groups = new List<HepGroupResponse>();
                foreach (var item in hepMaster.HepGroups)
                {
                    HepGroupResponse group = new HepGroupResponse();
                    group.Id = item.Id;
                    group.Name = item.Name;
                    group.PatientId = item.PatientId;
                    //group.Patient = Mapper.Map<Patient, PatientAutoCompleteResponse>(item.Patient);
                    group.PatientCaseId = item.PatientCaseId;
                    //group.PatientCase = Mapper.Map<PatientCase, PatientCaseAutoCompleteResponse>(item.PatientCase);
                    group.HepId = item.HepId;
                    group.ExerciseOrder = item.ExerciseOrder;
                    group.HepDetails = Mapper.Map<HepDetail[], HepDetailResponse[]>(item.HepDetails.ToArray());

                    foreach (var file in group.HepDetails)
                    {
                        if (file.Exercise != null && file.Exercise.Medias != null)
                        {
                            foreach (var exercise in file.Exercise.Medias)
                            {
                                exercise.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.Name, GetExerciseMediaS3Path(file.Exercise.Id));
                                exercise.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                                List<FileRequest> fileAll = new List<FileRequest>();
                                FileRequest tempFile = new FileRequest();

                                if (StorageService.UseAmazonStorage == true)
                                    tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(exercise.Name, GetExerciseMediaS3Path(file.Exercise.Id));
                                else
                                    tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + exercise.Id;

                                tempFile.S3BucketName = exercise.Name;
                                tempFile.Name = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                                #region

                                if (exercise.ThumbImageName != null)
                                {
                                    exercise.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.ThumbImageName, GetExerciseMediaS3Path(file.Exercise.Id));
                                    exercise.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);

                                    if (StorageService.UseAmazonStorage == true)
                                        tempFile.S3BucketThumbUrl = FileUploadHelpers.GetStorageUrl(exercise.ThumbImageName, GetExerciseMediaS3Path(file.Exercise.Id));
                                    else
                                        tempFile.S3BucketThumbUrl = StorageService.GetProjectMediaDownload + file.Id;

                                    tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(exercise.ThumbImageName);
                                }
                                #endregion

                                fileAll.Add(tempFile);
                                exercise.Files = fileAll.ToArray();
                            }
                        }
                    }

                    groups.Add(group);
                }
                response.HepGroups = groups.ToArray();
                return response;
            }
            return null;
        }

        /// <summary>
        /// Method Description : Using for Create New Hep Master Group.   
        /// </summary>
        /// <param name="hepMasterRequest">Hep Master Request Model</param>
        /// <returns>Hep Master Response Model</returns>
        [Authorize]
        [HttpPost]
        [Route("api/HepMasters/group")]
        [SwaggerResponse(201, "Created", typeof(HepMasterGroupResponse))]
        public async Task<IHttpActionResult> HepMasterGroupData([FromBody]HepMasterWithGroupRequest hepMasterRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                ModelState.AddModelError("Hep Master", "your session is expired. please login again");
                return BadRequest(ModelState);
            }
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user == null && user.Resource == null)
            {
                ModelState.AddModelError("Hep Master", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }
            var hepMaster = Mapper.Map<HepMasterWithGroupRequest, HepMaster>(hepMasterRequest);
            hepMaster.OwnerId = user.ResourceId;

            if (hepMaster.HepGroups != null)
            {
                foreach (HepGroup hepGroup in hepMaster.HepGroups)
                {
                    hepGroup.PatientId = hepMasterRequest.PatientId;
                    hepGroup.PatientCaseId = hepMasterRequest.PatientCaseId;
                }
            }

            await _hepService.InsertUpdateHepAsync(hepMaster);

            var response = new HepMasterGroupResponse();
            if (hepMasterRequest.PatientCaseId != null)
            {
                response = MapCurrentCaseHEP(hepMasterRequest.PatientCaseId, response, "");
            }

            if (hepMasterRequest.IsSaveAsDraft == false)
            {
                //var hepMobile = new HepMastersMobileController(_hepService, _hepGroupRepository, _hepMasterRepository, _hepDetailRepository, _hepFrequencyRepository, _exerciseRepository, _patientCaseRepository, _patientRepository, _dataContextScopeFactory);
                //var resp = hepMobile.GetHepMasterGroupByCase(hepMasterRequest.PatientId.Value);

                HepGetMobileResponse resp = GetHepMasterGroupByCase(hepMasterRequest.PatientId.Value);

                //var Hepjson = new JavaScriptSerializer().Serialize(resp); //Using JavaScriptSerializer (Note: enum string not saved)

                var serializerSettings = new JsonSerializerSettings();
                serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                var Hepjson = JsonConvert.SerializeObject(resp, serializerSettings);
                var userHepPlan = Hepjson.ToString();

                var patientPlanData = new HepPatientsPlan();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbSet = ctx.Set<HepPatientsPlan>();
                    patientPlanData = await dbSet.AsNoTracking().Where(x => x.HepId == response.Id).FirstOrDefaultAsync();
                }

                HepPatientsPlan hepPatientPlan = new HepPatientsPlan();
                if (patientPlanData != null)
                {
                    hepPatientPlan.Id = patientPlanData.Id;
                    hepPatientPlan.HepId = response.Id;
                    hepPatientPlan.HepPlan = userHepPlan;
                    hepPatientPlan.PatientId = hepMasterRequest.PatientId;

                }
                else
                {
                    hepPatientPlan.Id = 0;
                    hepPatientPlan.HepId = response.Id;
                    hepPatientPlan.HepPlan = userHepPlan;
                    hepPatientPlan.PatientId = hepMasterRequest.PatientId;
                }

                await _hepService.InsertUpdateHepPatientPlanAsync(hepPatientPlan);

            }
            return Created(GetEntityLocation(hepMaster.Id), response);
        }

        /// <summary>
        /// Method Description : Using for Get Hep Master by Case Id.    
        /// </summary>
        /// <param name="Id">Case Id</param>
        /// <returns>Hep Get Mobile Response Model</returns>
        public HepGetMobileResponse GetHepMasterGroupByCase(long Id)
        {
            List<HepDetailMobileResponse> listHepDetail = new List<HepDetailMobileResponse>();
            List<HepGroupMobileResponse> listHepGroup = new List<HepGroupMobileResponse>();
            List<HepExerciseMobileResponse> listHepExercise = new List<HepExerciseMobileResponse>();
            List<HepExerciseMobileResponse> listHepGroupExercise = new List<HepExerciseMobileResponse>();
            HepDetailMobileResponse hepDetailMobile = new HepDetailMobileResponse();

            var currentCaseDetail = _patientCaseRepository.List(new FilterQuery<PatientCase>()
                                .AddFilter(x => x.PatientId == Id && x.CaseStatus == CaseStatusEnum.Inprogress && (x.IsDeleted == false || x.IsDeleted == null))).FirstOrDefault();

            if (currentCaseDetail != null)
            {
                var CaseId = currentCaseDetail.Id;

                var readOptions = new ReadOptions<HepMaster>().AsReadOnly();
                var page = _hepMasterRepository.List(new FilterQuery<HepMaster>().AddFilter(x => x.PatientCaseId == CaseId), readOptions).FirstOrDefault();

                var dtos = Mapper.Map<HepMaster, HepMasterGroupMobileResponse>(page);
                if (dtos != null)
                {
                    hepDetailMobile.StartDate = dtos.StartDate;
                    hepDetailMobile.EndDate = dtos.EndDate;
                    hepDetailMobile.updatedDate = dtos.Modified;

                    foreach (var hepexercise in dtos.HepDetails.Where(x => x.GroupId == null))
                    {
                        HepExerciseMobileResponse hepExerciseMobile = new HepExerciseMobileResponse();
                        hepExerciseMobile.Id = hepexercise.Id;
                        hepExerciseMobile.ExerciseId = hepexercise.ExerciseId;
                        hepExerciseMobile.ExerciseName = hepexercise.Exercise.Name;
                        hepExerciseMobile.Comments = hepexercise.Comments;
                        hepExerciseMobile.GroupId = hepexercise.GroupId;
                        hepExerciseMobile.HepId = hepexercise.HepId;
                        hepExerciseMobile.Sets = hepexercise.Sets;
                        hepExerciseMobile.Reps = hepexercise.Reps;
                        hepExerciseMobile.Time = hepexercise.Time;
                        hepExerciseMobile.TimeUnit = hepexercise.TimeUnit;
                        hepExerciseMobile.Weight = hepexercise.Weight;
                        hepExerciseMobile.WeightUnit = hepexercise.WeightUnit;
                        hepExerciseMobile.Holds = hepexercise.Holds;
                        hepExerciseMobile.HoldsUnit = hepexercise.HoldsUnit;
                        hepExerciseMobile.ExerciseOrder = hepexercise.ExerciseOrder;
                        hepExerciseMobile.Frequencies = hepexercise.Frequencies;
                        hepExerciseMobile.IsActiveFrequency = hepexercise.IsActiveFrequency;
                        hepExerciseMobile.Medias = hepexercise.Exercise.Medias;

                        foreach (var file in hepexercise.Exercise.Medias)
                        {
                            file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(hepexercise.Exercise.Id));

                            List<FileRequest> fileAll = new List<FileRequest>();
                            FileRequest tempFile = new FileRequest();

                            tempFile.S3BucketName = file.Name;
                            tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                            #region

                            if (file.ThumbImageName != null)
                            {
                                file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(hepexercise.Exercise.Id));

                                tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                            }
                            #endregion

                            fileAll.Add(tempFile);
                        }
                        listHepExercise.Add(hepExerciseMobile);
                    }

                    foreach (var hepGroup in dtos.HepGroups)
                    {
                        HepGroupMobileResponse dataHepGroupMobile = new HepGroupMobileResponse();
                        dataHepGroupMobile.GroupId = hepGroup.Id;
                        dataHepGroupMobile.Name = (hepGroup.Name != null) ? hepGroup.Name : string.Empty;
                        dataHepGroupMobile.ExerciseOrder = hepGroup.ExerciseOrder;

                        foreach (var hepexercise in hepGroup.HepDetails.Where(x => x.GroupId != null))
                        {
                            HepExerciseMobileResponse hepExerciseMobile = new HepExerciseMobileResponse();
                            hepExerciseMobile.Id = hepexercise.Id;
                            hepExerciseMobile.ExerciseId = hepexercise.ExerciseId;
                            hepExerciseMobile.ExerciseName = hepexercise.Exercise.Name;
                            hepExerciseMobile.Comments = hepexercise.Comments;
                            hepExerciseMobile.GroupId = hepexercise.GroupId;
                            hepExerciseMobile.HepId = hepexercise.HepId;
                            hepExerciseMobile.Sets = hepexercise.Sets;
                            hepExerciseMobile.Reps = hepexercise.Reps;
                            hepExerciseMobile.Time = hepexercise.Time;
                            hepExerciseMobile.TimeUnit = hepexercise.TimeUnit;
                            hepExerciseMobile.Weight = hepexercise.Weight;
                            hepExerciseMobile.WeightUnit = hepexercise.WeightUnit;
                            hepExerciseMobile.Holds = hepexercise.Holds;
                            hepExerciseMobile.HoldsUnit = hepexercise.HoldsUnit;
                            hepExerciseMobile.ExerciseOrder = hepexercise.ExerciseOrder;
                            hepExerciseMobile.Frequencies = hepexercise.Frequencies;
                            hepExerciseMobile.IsActiveFrequency = hepexercise.IsActiveFrequency;
                            hepExerciseMobile.Medias = hepexercise.Exercise.Medias;

                            foreach (var file in hepexercise.Exercise.Medias)
                            {
                                file.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.Name, GetExerciseMediaS3Path(hepexercise.Exercise.Id));

                                List<FileRequest> fileAll = new List<FileRequest>();
                                FileRequest tempFile = new FileRequest();

                                tempFile.S3BucketName = file.Name;
                                tempFile.Name = GetOriginalFileNameWithoutBucketGuid(file.Name);

                                #region

                                if (file.ThumbImageName != null)
                                {
                                    file.ThumbURL = FileUploadHelpers.GetStorageMediaUrlPhoto(file.ThumbImageName, GetExerciseMediaS3Path(hepexercise.Exercise.Id));

                                    tempFile.ThumbImageName = GetOriginalFileNameWithoutBucketGuid(file.ThumbImageName);
                                }
                                #endregion

                                fileAll.Add(tempFile);
                            }
                            listHepGroupExercise.Add(hepExerciseMobile);
                            listHepExercise.Add(hepExerciseMobile);
                        }
                        HepGroupMobileResponse newhepGroupMobile = new HepGroupMobileResponse();
                        newhepGroupMobile.GroupId = dataHepGroupMobile.GroupId;
                        newhepGroupMobile.Name = dataHepGroupMobile.Name;
                        newhepGroupMobile.ExerciseOrder = dataHepGroupMobile.ExerciseOrder;
                        listHepGroup.Add(newhepGroupMobile);
                        listHepGroupExercise.Clear();
                    }
                }
            }

            HepGetMobileResponse newhepGetMobile = new HepGetMobileResponse();
            newhepGetMobile.HepDetails = (hepDetailMobile != null) ? hepDetailMobile : new HepDetailMobileResponse();
            newhepGetMobile.HepGroups = (listHepGroup != null || listHepGroup.Count > 0) ? listHepGroup.ToArray() : new HepGroupMobileResponse[0];
            newhepGetMobile.HepExercises = (listHepExercise != null || listHepExercise.Count > 0) ? listHepExercise.ToArray() : new HepExerciseMobileResponse[0];

            return newhepGetMobile;
            //return Ok(newhepGetMobile);
        }

    }
}
