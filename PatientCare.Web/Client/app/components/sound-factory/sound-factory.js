﻿'use strict';

angular.module("EDZoutstaffingPortalApp")
    .factory('SoundFactory', ['$rootScope', function ($rootScope) {

        
        var notifSound = new Audio('Client/assets/sounds/alert-tone.mp3');
        var msgReceivedSound = new Audio('Client/assets/sounds/msg-received-sound.mp3');
        $rootScope.playSound=function playSound() {
            notifSound.play();
        }



        return {
            playNotifSound: function () {
                notifSound.play();
            },
            playMsgReceivedSound: function () {
                msgReceivedSound.play();
            }
        }
  }]);