﻿using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.AutoCompletes
{
    public class AutoCompleteRequest
    {
        [DataMember(Name = "search")]
        public string Search { get; set; }
        
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "patientId")]
        public long? PatientId { get; set; }

    }
}