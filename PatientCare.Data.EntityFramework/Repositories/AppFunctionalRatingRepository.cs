﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class AppFunctionalRatingRepository : RepositoryBase<AppFunctionalRating>, IAppFunctionalRatingRepository
    {
        public AppFunctionalRatingRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
