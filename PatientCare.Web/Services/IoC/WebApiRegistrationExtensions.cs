﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using PatientCare.Web.Services.WebApi;

namespace PatientCare.Web.Services.IoC
{
    internal static class WebApiRegistrationExtensions
    {
        public static ContainerBuilder RegisterWebApi(this ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            containerBuilder.RegisterWebApiFilterProvider(GlobalConfiguration.Configuration);

            containerBuilder.RegisterType<DataContextScopeFilter>()
                .AsWebApiActionFilterFor<ApiController>()
                .InstancePerRequest();

            return containerBuilder;
        }
    }
}
