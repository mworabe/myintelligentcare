﻿"use strict";

angular.module('EDZoutstaffingPortalApp')
    .directive('mediaItem', function () {
        return {
            templateUrl: "Client/app/components/directives/media-item/media-item.html",
            restrict: 'EA',
            scope: {
                config: '=config',
                url: "=",
                dataItem: "=data",
                index: "=position"
            },
            controller: ['$scope', 'MediaPreview', function ($scope, MediaPreview) {
                $scope.title = "";
                $scope.mediaType = "";
                $scope.mediaURL = "";
                $scope.videoActualURL = "";

                function findMedia(type) {
                    if ($scope.dataItem && $scope.dataItem.medias && $scope.dataItem.medias.length > 0) {
                        for (var i = 0; i < $scope.dataItem.medias.length; i++) {
                            var obj = $scope.dataItem.medias[i];
                            if (obj.mediaType == type) {
                                return i;
                            }
                        }
                    }
                    return -1;
                }


                function onDataChange() {
                    if ($scope.dataItem && $scope.dataItem.name) {
                        $scope.title = $scope.dataItem.name;
                    }

                    if ($scope.dataItem && $scope.dataItem.medias && $scope.dataItem.medias.length > 0) {

                        var videoIndex = findMedia("Video");
                        var index = -1;
                        if (videoIndex > -1)
                            index = videoIndex;
                        else
                            index = findMedia("Image")

                        if (index > -1) {
                            var media = $scope.dataItem.medias[index];
                            $scope.mediaType = media.mediaType;
                            if (media.mediaType == "Video") {
                                $scope.mediaURL = media.thumbURL;
                                $scope.videoActualURL = media.mediaURL;
                            }
                                
                            else
                                $scope.mediaURL = media.mediaURL;
                            
                        }
                    }
                }


                $scope.$watch("dataItem", function () {
                    onDataChange();
                });

                $scope.openVideo = function () {
                    MediaPreview.showVideo($scope.title, $scope.videoActualURL);
                }

                $scope.showMsg = function () {
                    alert(JSON.stringify($scope.dataItem));
                }
            }]

        }

    });



