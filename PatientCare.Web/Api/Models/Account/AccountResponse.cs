﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.Account
{
    [DataContract]
    public class AccountResponse
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public PicklistResponse Resource { get; set; }

        [DataMember]
        public PicklistResponse Patient { get; set; }

        [DataMember]
        public string Role { get; set; }
    }
}