﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("ClientCompany", ["$resource", "$state", function ($resource, $state) {
      var resource = $resource("api/ClientCompanies/:id/:controller",
        null, //parameters default
            {
                'get': { method: "GET", params: {}, isArray: false },
                'update': { method: "PUT" },
                'query': { method: "GET", params: {}, isArray: false },
                'query2': { method: "GET", params: { controller: "completeList" }, isArray: false },//completeList
                'paymentTerms': { method: "GET", params: { controller: "payment-terms" }, isArray: false }
            });

      resource.getTableOption = {
          pageName: "Client Companies",
          pageSize: 20,
          hideBulkEdit: true,
          cols: function () {
              return [
                  {
                      field: "id",
                      title: "Id",
                      sortable: "id",
                      show: false,
                      dataType: "number"
                  },
                  {
                      field: "customerID",
                      title: "Client Number",
                      sortable: "customerID",
                      show: true,
                      dataType: "text",
                      required: false
                  },
                  {
                      field: "name",
                      title: "Client Name",
                      sortable: "name",
                      show: true,
                      dataType: "text",
                      dataTypeView: "href",
                      required: true,
                      buildUrl: function (row) {
                          return $state.href("clientCompanyEdit", { id: row.id });
                      }
                  },
                  {
                      field: "clientStatus",
                      title: "Client Status",
                      sortable: "clientStatus",
                      show: true,
                      dataType: "text",
                      required: false
                  },
                  {
                      field: "clientIndustryCode",
                      title: "Client Industry Code",
                      sortable: "clientIndustryCode",
                      show: true,
                      dataType: "text",
                      required: false
                  },
                  {
                      field: "clientOpenDate",
                      title: "Client Open Date",
                      sortable: "clientOpenDate",
                      show: true,
                      dataType: "text",
                      dataTypeView: "date-time",
                      required: false
                  },
                  {
                      field: "clientIndustryName",
                      title: "Client Industry Name",
                      sortable: "clientIndustryName",
                      show: true,
                      dataType: "text",
                      required: false
                  },

                  {
                      field: "action",
                      title: ".",
                      dataType: "command"
                  },/*   {
                      field: "clientContactName",
                      title: "Contact Name",
                      sortable: "clientContactName",
                      show: true,
                      dataType: "text",
                      required: false
                  },
                  {
                      field: "contractRates",
                      title: "Contract Rates",
                      sortable: "contractRates",
                      show: true,
                      dataType: "text",
                      required: false,
                  }, {
                      field: "paymentTerms",
                      title: "Payment Terms",
                      sortable: "paymentTerms",
                      show: true,
                      dataType: "ui-select",
                      required: true,
                  },
                     {
                      field: "city",
                      title: "City",
                      sortable: "city.name",
                      show: true,
                      dataType: "angucomplete-alt",
                      required: false,
                      fieldInit: 'name',
                      customViewText: function (row) {
                          if (row && row.city)
                              return row.city.name;
                          return "";
                      }
                  },
                  {
                      field: "country",
                      title: "Country",
                      sortable: "country.name",
                      show: true,
                      dataType: "angucomplete-alt",
                      required: false,
                      fieldInit: 'name',
                      customViewText: function (row) {
                          if (row && row.country)
                              return row.country.name;
                          return "";
                      }
                  }, {
                      field: "postalCode",
                      title: "Postal Code",
                      sortable: "postalCode",
                      show: true,
                      dataType: "number",
                      max: 10,
                      required: false
                  }, */
              ];
          },
          onAdd_Click: function () {
              $state.go("clientCompanyEdit");
          },
          onEdit_Click: function (row) {
              $state.go("clientCompanyEdit", { id: row.id });
          },
      };
      return resource;
  }]);
