﻿using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.UiConfiguration;
using Microsoft.AspNet.Identity;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Api.Models.DropDownConfigs;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle UiConfiguration operations.
    /// </summary>
    [Authorize]
    public sealed class UiConfigurationController : ApiBaseController
    {
        /// <summary>
        /// Description : UiConfiguration IUiConfigurationServices.
        /// </summary>
        private readonly IUiConfigurationServices _uiconfigurationService;
        /// <summary>
        /// Description : UiConfiguration IUiConfigurationRepository.
        /// </summary>
        private readonly IUiConfigurationRepository _uiconfigurationRepository;
        /// <summary>
        /// Description : UiConfiguration IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : UiConfiguration IDropDownConfigService.
        /// </summary>
        private readonly IDropDownConfigService _dropDownConfigService;
        /// <summary>
        /// Description : UiConfiguration IDropDownConfigRepository.
        /// </summary>
        private readonly IDropDownConfigRepository _dropDownConfigRepository;

        ReadOptions<UiConfiguration> _readOptions = new ReadOptions<UiConfiguration>();

        /// <summary>
        /// Description : Initializes a new instance of the UiConfiguration Class. 
        /// </summary>
        /// <param name="uiconfigurationService">IUiconfiguration Service</param>
        /// <param name="uiconfigurationRepository">IUiconfiguration Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        /// <param name="dropDownConfigService">IDropDown Config Service</param>
        /// <param name="dropDownConfigRepository">IDropDown Config Repository</param>
        public UiConfigurationController(IUiConfigurationServices uiconfigurationService,
                                         IUiConfigurationRepository uiconfigurationRepository,
                                         IDataContextScopeFactory dataContextScopeFactory,
                                         IDropDownConfigService dropDownConfigService,
                                         IDropDownConfigRepository dropDownConfigRepository
            )
        {
            _uiconfigurationService = uiconfigurationService;
            _uiconfigurationRepository = uiconfigurationRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
            _dropDownConfigService = dropDownConfigService;
            _dropDownConfigRepository = dropDownConfigRepository;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/UiConfiguration/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get UiConfiguration. 
        /// </summary>
        /// <param name="key">Key Value</param>
        /// <param name="sysConfig">Sys Config Value</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/uiconfiguration/")]
        public IHttpActionResult GetUiConfiguration(string key = null, bool? sysConfig = null)
        {
            if (key == null)
            {
                ModelState.AddModelError("UiConfiguration", "Configuration key is required.");
                return BadRequest(ModelState);
            }

            var roleInfo = GetRoleAccessRights();

            var userId = User.Identity.GetUserId();
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user != null &&  user.Resource == null)
            {
                ModelState.AddModelError("UiConfiguration", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }

            long? resourceid = null;
            if (sysConfig == true)
                resourceid = null;
            else
                resourceid = user.Resource.Id;

            var uiconfiguration = _uiconfigurationRepository.List(new FilterQuery<UiConfiguration>().AddFilter(x => x.Key == key && x.ResourceId == resourceid)).FirstOrDefault();

            if (uiconfiguration != null)
            {
                var response = Mapper.Map<UiConfiguration, UiConfigurationResponse>(uiconfiguration);
                return Ok(response);
            }
            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Create New UiConfiguration.   
        /// </summary>
        /// <param name="uiconfigurationRequest">UiConfiguration Request Model</param>
        /// <returns>UiConfiguration Response Model</returns>
        [HttpPost]
        [Route("api/uiconfiguration")]
        [SwaggerResponse(201, "Created", typeof(UiConfigurationResponse))]
        public async Task<IHttpActionResult> CreateUiConfiguration([FromBody]UiConfigurationRequest uiconfigurationRequest)
        {
            var roleInfo = GetRoleAccessRights();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userId = User.Identity.GetUserId();
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user != null &&  user.Resource == null)
            {
                ModelState.AddModelError("UiConfiguration", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }

            if (uiconfigurationRequest.KeyType == "system")
            {
                uiconfigurationRequest.ResourceId = null;
            }
            else
            {
                uiconfigurationRequest.ResourceId = (user.Resource == null) ? null : (long?)user.Resource.Id;
            }
            var uiconfiguration = Mapper.Map<UiConfigurationRequest, UiConfiguration>(uiconfigurationRequest);
            await _uiconfigurationService.CreateUiConfigurationAsync(uiconfiguration);
            return Created(GetEntityLocation(uiconfiguration.Id), Mapper.Map<UiConfiguration, UiConfigurationResponse>(uiconfiguration));

        }

        /// <summary>
        /// Method Description : Using for Update UiConfiguration.   
        /// </summary>
        /// <param name="uiconfigurationRequest">UiConfiguration Request Model</param>
        /// <returns>UiConfiguration Response Model</returns>
        [HttpPut]
        [Route("api/uiconfiguration/{id}")]
        public async Task<IHttpActionResult> EditUiConfiguration(long id, [FromBody] UiConfigurationRequest uiconfigurationRequest)
        {
            var roleInfo = GetRoleAccessRights();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userId = User.Identity.GetUserId();
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user != null &&  user.Resource == null)
            {
                ModelState.AddModelError("UiConfiguration", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }
            if (uiconfigurationRequest.KeyType == "system")
            {
                uiconfigurationRequest.ResourceId = null;
            }
            else
            {
                uiconfigurationRequest.ResourceId = user.Resource.Id;
            }
            var uiconfiguration = Mapper.Map<UiConfigurationRequest, UiConfiguration>(uiconfigurationRequest);
            uiconfiguration.Id = id;

            await _uiconfigurationService.UpdateUiConfigurationAsync(uiconfiguration);

            return Ok(Mapper.Map<UiConfiguration, UiConfigurationResponse>(uiconfiguration));
        }

        /// <summary>
        /// Method Description : Using for Create New UiConfiguration.   
        /// </summary>
        /// <param name="uiSystemConfigurationRequest">UiSystem Configuration Request Model</param>
        /// <returns>UiConfiguration Response Model</returns>
        [HttpPost]
        [Route("api/uiconfiguration/saveSystemConfigurations")]
        public async Task<IHttpActionResult> SaveSystemConfigurations([FromBody] UiSystemConfigurationRequest uiSystemConfigurationRequest)
        {
            var roleInfo = GetRoleAccessRights();
            var userId = User.Identity.GetUserId();
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user != null &&  user.Resource == null)
            {
                ModelState.AddModelError("UiConfiguration", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            #region Add or Update System Configuration 
            UiConfiguration configuration = new UiConfiguration();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                var dbset = ctx.Set<UiConfiguration>();
                configuration = dbset.Where(x => x.Key == uiSystemConfigurationRequest.Key).FirstOrDefault();
            }

            if (configuration == null) // add
            {
                configuration = new UiConfiguration();
                configuration.Key = uiSystemConfigurationRequest.Key;
                configuration.ResourceId = null;
                configuration.Value = uiSystemConfigurationRequest.Value;
                await _uiconfigurationService.CreateUiConfigurationAsync(configuration);
            }
            else // Update
            {
                configuration.Value = uiSystemConfigurationRequest.Value;
                await _uiconfigurationService.UpdateUiConfigurationAsync(configuration);
            }
            #endregion            
            
            return Ok(Mapper.Map<UiConfiguration, UiConfigurationResponse>(configuration));
        }

        /// <summary>
        /// Description : Resource List Class.    
        /// </summary>
        public class ResourceList
        {
            public long ResourceId { get; set; }
        }

        /// <summary>
        /// Method Description : Using for Delete UiConfiguration by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>UiConfiguration Entity</returns>
        [HttpDelete]
        [Route("api/uiconfiguration/{id}")]
        public async Task<IHttpActionResult> DeleteUiConfigurationItem(long id)
        {
            var roleInfo = GetRoleAccessRights();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var success = await _uiconfigurationService.DeleteUiConfigurationAsync(id);
            if (!success)
            {
                return NotFound();
            }

            return Ok(new UiConfiguration { Id = id });
        }

    }
}
