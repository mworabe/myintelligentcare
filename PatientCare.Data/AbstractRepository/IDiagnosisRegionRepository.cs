﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IDiagnosisRepository : IRepository<Diagnosis>
    {
    }
}
