﻿using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Country.
    /// </summary>
    public class Country : NamedEntity
    {
        public string IsoCode { get; set; }

        public string IsdCode { get; set; }
    }
}
