﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Cities;
using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ReferralPhysicians
{
    [DataContract]
    public class ReferralPhysicianResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Speciality { get; set; }

        [DataMember]
        public MedicalDesignationEnum? MedicalDesignation { get; set; }

        [DataMember]
        public string MedicalDesignationOther { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string ExtensionNumber { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string FacilityName { get; set; }

        [DataMember]
        public string PostalCodeTemp { get; set; }

        [DataMember]
        public PicklistResponse PostalCode { get; set; }

        [DataMember]
        public long? CityId { get; set; }

        [DataMember]
        public CityResponse City { get; set; }

        [DataMember]
        public long? CountryId { get; set; }

        [DataMember]
        public PicklistResponse Country { get; set; }

        [DataMember]
        public long? StateId { get; set; }

        [DataMember]
        public PicklistResponse State { get; set; }

        //[DataMember]
        //public string Location { get; set; }

        [DataMember]
        public long? ReferralsToDate { get; set; }

        [DataMember]
        public DateTime? LastReferralReceived { get; set; }
    }
}