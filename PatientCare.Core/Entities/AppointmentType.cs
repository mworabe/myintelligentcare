﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Appointment Type.
    /// </summary>
    public class AppointmentType : NamedEntity
    {
        public string Color { get; set; }
    }
}
