﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.UserLoginHistorys;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class UserLoginHistoryMapperExtensions
    {
        public static EmitMapperMapperProvider UserLoginHistoryRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<UserLoginHistory, UserLoginHistoryResponse>
                    (new ExtendedMapConfig<UserLoginHistory, UserLoginHistoryResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<UserLoginHistory[], UserLoginHistoryResponse[]>());

            mapperProvider.RegisterMapper(() =>
             ObjectMapperManager.DefaultInstance.GetMapper<UserLoginHistory[], UserLoginHistoryPicklistResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<UserLoginHistoryRequest, UserLoginHistory>
                    (new ExtendedMapConfig<UserLoginHistoryRequest, UserLoginHistory>()));
            
            return mapperProvider;
        }
    }
}