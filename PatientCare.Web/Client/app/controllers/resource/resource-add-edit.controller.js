﻿(function ResourceEditCtrlIIFE() {
    "use strict";
    angular.module("EDZoutstaffingPortalApp")
        .controller("ResourceEditCtrl",
            ["$scope", "$stateParams", "Resource", "$state", "$q", "ResourceCompany", "JobTitle", "Division", "Department",
                "toaster", "Skill", "Certificate", "Language", "Upload", "Country", "CreatorSearchFunction", "ResourceAvailability", "uuid2", "DateTimeHelper", "Modal",
                "CreatorLeavePage", "PermissionWorker", "customFieldsResource", "$rootScope", "ConfigLayout", "ResourceDemand", "StateService",
                "CityService", "DynamicDropDownConfigService", "DropdownConfigsFunction", "$timeout", "AvaialabilityCalculationService", "SystemFunctions",
                "$filter", "$translate", "$uibModal", "CONS_BUILD_FOR", 'ClinicService', 'ClinicLocationService', 'ClinicianService', 'ZipCodeService',
                ResourceEditCtrl]
        );
    function ResourceEditCtrl($scope, $stateParams, resource, $state, $q, resourceCompany, jobTitle, division, department,
                              toaster, skill, certificate, language, uploader, country, creatorSearchFunction, resourceAvailability, uuid2, dateTimeHelper, modal,
                              creatorLeavePage, permissionWorker, initialValues, $rootScope, configLayoutFactory, resourceDemand, StateService,
                              CityService, dynamicDropDownConfigService, dropdownConfigsFunction, $timeout, avaialabilityCalculationService, systemFunctionsService,
                              $filter, $translate, $uibModal, CONS_BUILD_FOR, clinicService, clinicLocationService, ClinicianService, ZipCodeService) {

        var self = this;

        self.IS_BUILD_FOR_STINSON = CONS_BUILD_FOR.isForStinson;
        console.info(self.IS_BUILD_FOR_STINSON)

        self.isResourceLoadedCorrectly = !(initialValues[2] && initialValues[2].errorCode === 404);

        if (!this.isResourceLoadedCorrectly) {
            toaster.pop("error", "", "We are unable to serve you at this moment, please try again later.");
            return;
        }

        self.caseList = {
            "data": [
                {
                    "id": 91, "patientId": 5, "patient": { "id": 5, "name": "Sam Den", "email": "email@gmail.com", "firstName": "Den", "middleName": "L", "lastName": "Sam" }, "patientNumber": "NL-75-5", "caseNo": "NL-75-5-4", "clinicId": 2, "clinic": { "id": 2, "name": "Performance Rehab", "clinicPrefix": "PR", "contactPerson": null, "isDeleted": null }, "clinicLocationId": 3, "clinicLocation": { "id": 3, "name": "75th and Antioch", "locationPrefix": "03", "clinicId": 2, "clinic": { "id": 2, "name": "Performance Rehab", "locationPrefix": null, "clinicId": null, "clinic": null, "isDeleted": null }, "isDeleted": null }, "caseStatus": "Inprogress", "startDate": "2017-04-13T00:30:00", "endDate": null, "birthDate": "2017-02-01T00:30:00", "age": null, "injuryRegionId": 3, "injuryRegion": { "id": 3, "name": "Lower Back/Pelvis", "email": null, "firstName": null, "middleName": null, "lastName": null }, "assignedPhysicianId": 7, "assignedPhysician": { "id": 7, "name": "Marc Allenly", "email": null, "firstName": "Allenly", "middleName": null, "lastName": "Marc" }, "referingPhysicianId": 3, "referingPhysician": { "id": 3, "name": "Abel Marcia", "firstName": "Marcia", "lastName": "Abel", "speciality": "sp", "medicalDesignation": "OT", "medicalDesignationOther": null, "phoneNumber": "1234567898", "extensionNumber": null, "email": "sample@gmail.com", "postalCode": null }, "returnTo": null, "authorizeationNumber": null, "relatedCause": null, "primayDiagnosisId": 3, "primayDiagnosis": { "id": 3, "name": "M79.604 - Pain in Left Leg", "email": null, "firstName": null, "middleName": null, "lastName": null }, "secondaryDiagnosisId": null, "secondaryDiagnosis": null, "webPTCaseId": null, "isDeleted": null
                }, {
                    "id": 90, "patientId": 5, "patient": { "id": 5, "name": "Sam Den", "email": "email@gmail.com", "firstName": "Den", "middleName": "L", "lastName": "Sam" }, "patientNumber": "NL-75-5", "caseNo": "NL-75-5-3", "clinicId": 2, "clinic": { "id": 2, "name": "Performance Rehab", "clinicPrefix": "PR", "contactPerson": null, "isDeleted": null }, "clinicLocationId": 3, "clinicLocation": { "id": 3, "name": "75th and Antioch", "locationPrefix": "03", "clinicId": 2, "clinic": { "id": 2, "name": "Performance Rehab", "locationPrefix": null, "clinicId": null, "clinic": null, "isDeleted": null }, "isDeleted": null }, "caseStatus": "Discharged", "startDate": "2017-04-19T00:30:00", "endDate": null, "birthDate": "2017-02-01T00:30:00", "age": 0, "injuryRegionId": 4, "injuryRegion": { "id": 4, "name": "Abdominal", "email": null, "firstName": null, "middleName": null, "lastName": null }, "assignedPhysicianId": 10, "assignedPhysician": { "id": 10, "name": "Stark Tony", "email": null, "firstName": "Tony", "middleName": "m", "lastName": "Stark" }, "referingPhysicianId": 3, "referingPhysician": { "id": 3, "name": "Abel Marcia", "firstName": "Marcia", "lastName": "Abel", "speciality": "sp", "medicalDesignation": "OT", "medicalDesignationOther": null, "phoneNumber": "1234567898", "extensionNumber": null, "email": "sample@gmail.com", "postalCode": null }, "returnTo": null, "authorizeationNumber": null, "relatedCause": null, "primayDiagnosisId": 4, "primayDiagnosis": { "id": 4, "name": "M79.672 - Pain in Left Foot", "email": null, "firstName": null, "middleName": null, "lastName": null }, "secondaryDiagnosisId": null, "secondaryDiagnosis": null, "webPTCaseId": null, "isDeleted": null
                }, {
                    "id": 25, "patientId": 5, "patient": { "id": 5, "name": "Sam Den", "email": "email@gmail.com", "firstName": "Den", "middleName": "L", "lastName": "Sam" }, "patientNumber": "NL-75-5", "caseNo": "NL-75-5-2", "clinicId": 1, "clinic": { "id": 1, "name": "Northland", "clinicPrefix": "NL", "contactPerson": null, "isDeleted": null }, "clinicLocationId": 2, "clinicLocation": { "id": 2, "name": "87th & Neiman", "locationPrefix": "02", "clinicId": 2, "clinic": { "id": 2, "name": "Performance Rehab", "locationPrefix": null, "clinicId": null, "clinic": null, "isDeleted": null }, "isDeleted": null }, "caseStatus": "Discharged", "startDate": "2017-03-23T21:55:00", "endDate": null, "birthDate": "2017-02-01T00:30:00", "age": 0, "injuryRegionId": 3, "injuryRegion": { "id": 3, "name": "Lower Back/Pelvis", "email": null, "firstName": null, "middleName": null, "lastName": null }, "assignedPhysicianId": 2, "assignedPhysician": { "id": 2, "name": "KD John", "email": null, "firstName": "John", "middleName": null, "lastName": "KD" }, "referingPhysicianId": null, "referingPhysician": null, "returnTo": null, "authorizeationNumber": null, "relatedCause": "AutoAccidentNoFault", "primayDiagnosisId": 2, "primayDiagnosis": { "id": 2, "name": "M79.604 - Pain in Right Leg", "email": null, "firstName": null, "middleName": null, "lastName": null }, "secondaryDiagnosisId": null, "secondaryDiagnosis": null, "webPTCaseId": null, "isDeleted": null
                }, {
                    "id": 6, "patientId": 5, "patient": { "id": 5, "name": "Sam Den", "email": "email@gmail.com", "firstName": "Den", "middleName": "L", "lastName": "Sam" }, "patientNumber": "NL-75-5", "caseNo": "NL-75-5-1", "clinicId": 1, "clinic": { "id": 1, "name": "Northland", "clinicPrefix": "NL", "contactPerson": null, "isDeleted": null }, "clinicLocationId": 3, "clinicLocation": { "id": 3, "name": "75th and Antioch", "locationPrefix": "03", "clinicId": 2, "clinic": { "id": 2, "name": "Performance Rehab", "locationPrefix": null, "clinicId": null, "clinic": null, "isDeleted": null }, "isDeleted": null }, "caseStatus": "Discharged", "startDate": "2017-02-21T11:17:32.023", "endDate": "2017-02-21T11:17:32.023", "birthDate": "1991-02-11T00:00:00", "age": 26, "injuryRegionId": 1, "injuryRegion": { "id": 1, "name": "Neck and Head", "email": null, "firstName": null, "middleName": null, "lastName": null }, "assignedPhysicianId": 2, "assignedPhysician": { "id": 2, "name": "KD John", "email": null, "firstName": "John", "middleName": null, "lastName": "KD" }, "referingPhysicianId": null, "referingPhysician": null, "returnTo": "2017-02-23T11:17:32.023", "authorizeationNumber": "456", "relatedCause": "AutoAccidentNoFault", "primayDiagnosisId": 1, "primayDiagnosis": { "id": 1, "name": "R51 - Headache", "email": null, "firstName": null, "middleName": null, "lastName": null }, "secondaryDiagnosisId": 2, "secondaryDiagnosis": { "id": 2, "name": "M79.604 - Pain in Right Leg", "email": null, "firstName": null, "middleName": null, "lastName": null }, "webPTCaseId": null, "isDeleted": null
                }
            ],
            "itemsCount": 4,
            "additionalInfo": null
        }

        self.resourceConfig = initialValues[1] || {};
        self.resourceTabs = _.unionBy(initialValues[1].tabs, configLayoutFactory.getResourceTab(), "id");

        /** Fix for IRMS5 only, Education Date Field as Text Field.*/
        if (initialValues[2] && self.IS_BUILD_FOR_STINSON) {
            var tempModel = initialValues[2];
            var educations = tempModel.educations;
            var tempCertificate = tempModel.certificates;

            _.forEach(educations, function (edu) {
                if (edu.dateObtained) edu.dateObtained = edu.dateObtained.substring(0, 4);// moment(edu.dateObtained).format("YYYY");
            })
            _.forEach(tempCertificate, function (edu) {
                if (edu.issueDate) edu.issueDate = edu.issueDate.substring(0, 4);// moment(edu.issueDate).format("YYYY");
            })
        }


        var tempResourceModel = angular.copy(initialValues[2]);
        var modelId = $stateParams.id || undefined;
        var counterFilesPromise = 0;
        var audioService = {};
        var salaryCoeff = 2080;
        var params = {};
        var MIN_YEARS = 15;
        var MAX_YEARS = 100;
        var commonConfig = {
            onMediaCaptured: function (stream) {
                audioService.stream = stream;
                if (audioService.mediaCapturedCallback) {
                    audioService.mediaCapturedCallback();
                }

                audioService.disabled = false;
            },
            onMediaStopped: function () {
                if (!audioService.disableStateWaiting) {
                    audioService.disabled = false;
                }
            },
            onMediaCapturingFailed: function (error) {
                if (error.name === 'PermissionDeniedError' && !!navigator.mozGetUserMedia) {
                    intallFirefoxScreenCapturingExtension();
                }
                commonConfig.onMediaStopped();
            }
        };

        //tabs options
        self.availability_Condition = availabilityCondition;
        self.other_Condition = isToShowOtherTab;
        self.summary_Condition = showSummaryTab;
        //self.availability_Click = availabilityTab_Click;
        //self.backgroundCheck_Condition = permissionWorker.restrictBackgroundChecks;
        //self.criminalRecords_Condition = permissionWorker.restrictCriminalRecords;
        self.previousEmployments_Condition = permissionWorker.restrictPreviousEmployments;
        self.workingDetails_Click = calculateYearsOfExperience;
        self.summary_Click = summaryTab_Click;
        self.organizationalHierarchy_Click = organizationalHierarchyTab_Click;
        self.resourceHref = $state.href("resource");
        self.leftMenuMaxHeight = (window.innerHeight - 30);


        self.model = {};
        self.model.range = "M";
        self.autocompleteMinLength = 0;
        self.availabilityArray = [];
        self.availableVsDemandData = [];
        self.availableVsDemandHeaders = [];
        self.availableList = [];
        self.projectId = modelId;
        self.model.active = true;
        self.currentTab = $stateParams.currentTab;
        //self.currentTab = "summary";
        self.availabilityModelShow = { timeRange: "Day" };
        self.callSubmit = false;
        self.companyName = "";
        self.organizationalChartData = [];
        self.availableVsDemandData = $stateParams.availabilityData || [];
        self.availableVsDemandHeaders = $stateParams.availabilityHeaders || [];
        $scope.resumeFieldList = {};


        function getValueInAnguAutocomplete(selected) {
            return (selected && selected.originalObject) ? selected.originalObject.id : null;
        }

        self.replaceSpecialChars = function (string) {
            if (!string || !string.length)
                return string;
            var replaceString = string.replace(/\n/g, "<br>");
            return replaceString;
        }

        function showSummaryTab() {
            return !(self.model.id && self.model.id > 0)
        }

        function isToShowOtherTab() {
            if (self.otherContainer) {
                if (self.otherContainer.container1 && self.otherContainer.container1.length > 0)
                    return false;
                else if (self.otherContainer.container2 && self.otherContainer.container2.length > 0)
                    return false;
                else if (self.otherContainer.container3 && self.otherContainer.container3.length > 0)
                    return false;
                else if (self.otherContainer.container4 && self.otherContainer.container4.length > 0)
                    return false;
                else if (self.otherContainer.container5 && self.otherContainer.container5.length > 0)
                    return false;
                else if (self.otherContainer.container6 && self.otherContainer.container6.length > 0)
                    return false;
                else
                    return true;
            }
            return true;
        }

        function summaryTab_Click() {
        }

        function organizationalHierarchyTab_Click() {
            self.loadCorrect = resource.getOrganizationalHierarchy({ resourceId: self.model.id })
                .$promise.then(function (response) {
                    self.organizationalChartData = response;
                    $scope.$broadcast('loadOrganizationalChart', {
                        chartData: response
                    });
                });
        }

        self.getResourceAvailabilityList = resourceAvailability.getResourceAvailabilityList;
        self.optionResourceAvailabilityList = resourceAvailability.getTableOption;
        self.availabilityListReload = resourceAvailability.reloadCommand;
        self.getResourceDemandList = resourceDemand.getResourceDemandList;
        self.optionResourceDemandList = resourceDemand.getTableOption;
        self.demandListReload = resourceDemand.reloadCommand;
        self.birthDate_max = moment().subtract(MIN_YEARS, "year");
        self.birthDate_min = moment().subtract(MAX_YEARS, "year");

        self._onResourceAvailabilityResponceTransform = function (response) {
            response.id = modelId;
        }
        self._onResourceDemandResponceTransform = function (response) {
            response.id = modelId;
        }


        self.dateFormat = function (date, formate) {
            if (!date)
                return "";
            if (!formate)
                formate = "MM/DD/YYYY"
            return moment(date).format(formate);
        }

        self.resumeSmallDateFormat = function (date) {
            if (!date)
                return "";
            return moment(date).format("MM/YYYY");
        }

        self.resumeCertificateDateFormat = function (date) {
            if (!date)
                return "";
            return moment(date).format("YYYY");
        }

        self.getPreviousEmploymentCompany_and_TitleValue = function getPreviousEmploymentCompany_and_TitleValue(companyName, titleOfPosition) {
            return (companyName) ? ((titleOfPosition) ? companyName + " / " + titleOfPosition : companyName) : "";
        }

        function getMonthsDiffrence(d1, d2) {
            var months;
            var d1Y = d1.getFullYear();
            var d2Y = d2.getFullYear();
            var d1M = d1.getMonth();
            var d2M = d2.getMonth();

            return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
        }

        self.getResourceFullName = function () {
            if (self.model) {
                if ((self.model.lastName && self.model.lastName.length > 0) && (self.model.firstName && self.model.firstName.length > 0)) {
                    return self.model.lastName + " " + self.model.firstName + " / ";
                }
                return "";
            }
        }

        self.getResourceFullNameForSummary = function () {
            if (self.model) {
                if ((self.model.lastName && self.model.lastName.length > 0) && (self.model.firstName && self.model.firstName.length > 0)) {
                    return self.model.lastName + ", " + self.model.firstName + " / ";
                }
                return "";
            }
        }

        function calculateYearsOfExperience() {
            if (self.model && self.model.startDate) {
                var totalMonths = getMonthsDiffrence(new Date(self.model.startDate), new Date());
                var years = parseInt(totalMonths / 12);
                var months = ((totalMonths / 12) - years) * 12;
                self.model.yearsEmployed = parseInt(years);
                self.model.monthsEmployed = parseInt(months);
            }
        }

        self.getEmployedFromString = function () {
            if (self.model && self.model.startDate) {
                var totalMonths = getMonthsDiffrence(new Date(self.model.startDate), new Date());
                var years = parseInt(totalMonths / 12);
                var months = ((totalMonths / 12) - years) * 12;
                return "Years Employed : " + ((years > 0) ? "<strong>" + parseInt(years) + "</strong> Years " : "") + ((months > 0) ? "<strong>" + parseInt(months) + "</strong> Months " : "");
            }
        }

        function calculateResourceExperienceBasedOnCertificate() {
            if (modelId > 0 && self.model) {
                if (self.model.certificates && self.model.certificates.length > 0) {
                    var certificateDate = self.model.certificates[0].issueDate;
                    if (certificateDate) {
                        var totalMonths = getMonthsDiffrence(new Date(certificateDate), new Date());
                        var years = parseInt(totalMonths / 12);
                        var months = ((totalMonths / 12) - years) * 12;
                        self.model.strCertificateExperience = (years > 0 || months > 0) ? "<i class='fa fa-minus'></i> Experience : " + (years > 0 ? parseInt(years) + " Years " : "") + (months > 0 ? parseInt(months) + " Months" : "") : "";
                    }
                }
            }
        }



        function resourceCompanySelected(selected) {
            self.model.resourceCompanyId = getValueInAnguAutocomplete(selected);
            self.model.resourceCompany = (selected && selected.originalObject) ? selected.originalObject.name : null;
        }

        function businessUnitSelected(selected) {
            self.model.businessUnitId = getValueInAnguAutocomplete(selected);
        }

        function IndustrySelected(selected) {
            self.model.industryId = getValueInAnguAutocomplete(selected);
        }

        function resourceDepartmentSelected(selected) {
            self.model.departmentId = getValueInAnguAutocomplete(selected);
        }

        function resourceDivisionSelected(selected) {
            self.model.divisionId = getValueInAnguAutocomplete(selected);
        }

        function resourceJobTitleSelected(selected) {
            self.model.jobTitleId = getValueInAnguAutocomplete(selected);
        }

        function resourceCountrySelected(selected) {
            self.model.countryId = getValueInAnguAutocomplete(selected);
        }

        function resourceStateSelected(selected) {
            self.model.stateId = getValueInAnguAutocomplete(selected);
        }
        self.zipCode_Enum = [];
        function resourceCitySelected(selected) {
            self.model.cityId = getValueInAnguAutocomplete(selected);
            self.zipCode_Enum = (selected && selected.originalObject && selected.originalObject.postalCodeLists) ? selected.originalObject.postalCodeLists : [];

            //if (selected && selected.title) { // manually Call city changed
            //    if (self.zipCode_Enum && self.zipCode_Enum.length > 0) {
            //        //self.model.zipCode = self.zipCode_Enum[0].zipCode;
            //        $timeout(function () {
            //            $scope.$broadcast('angucomplete-alt:changeInput', 'zipCode', self.model.zipCode);
            //        }, 1000);
            //    } else
            //        self.model.zipCode = null;
            //}
        }

        //zipCode
        self.zipCode_InputChanged = function (str) {
            self.model.zipCode = $("#zipCode_value").val();
            updateGeoNamesForZipCode();
        }
        self.zipCode_Selected = function (selected) {
            self.model.zipCode = (selected && selected.originalObject) ? selected.originalObject.name : null;
            debugger;
            updateGeoNamesForZipCode();
        }


        function updateGeoNamesForZipCode() {
            if (self.model.zipCode && self.prevZipCode != self.model.zipCode) {
                self.prevZipCode = angular.copy(self.model.zipCode);
                resource.getGeoNamesForZipCode({ zipCode: self.model.zipCode }, function (success) {
                    self.autoCityPopulate = true;
                    if (success && success.city && success.state && success.country) {
                        $timeout(function updateAutoCompleteValue() {
                            self.model.city = success.city;
                            self.model.state = success.state;
                            self.model.country = success.country;
                            self.needToUpdateStateOnCitySelection = false;

                            $scope.$broadcast('angucomplete-alt:changeInput', 'city', success.city);
                            $scope.$broadcast('angucomplete-alt:changeInput', 'state', success.state);
                            $scope.$broadcast('angucomplete-alt:changeInput', 'country', success.country);

                        }, 100);
                    } else {
                        console.info("Zip Cleared from GeoNames");
                        $scope.$broadcast('angucomplete-alt:clearInput', 'zipCode');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'city');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'state');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'country');
                        toaster.pop("error", "", "Entered ZIP code is not valid, Re-Enter it or Choose one from list.");
                    }
                }, function (error) { });
            }
        }

        function resourceArmedForcesCountrySelected(selected) {
            self.model.armedForcesCountryId = getValueInAnguAutocomplete(selected);
        }

        function resourceSupervisorSelected(selected) {
            self.model.supervisorId = getValueInAnguAutocomplete(selected);
        }

        function resourceDesignationSelected(selected) {
            var item = this.$parent.$parent.$parent.item;
            item.resourceDesignationId = (selected && selected.originalObject && selected.originalObject.id > 0) ? selected.originalObject.id : null;
            item.resourceDesignation = (selected && selected.originalObject) ? selected.originalObject : null;
        }
        function certificateStateSelected(selected) {
            self.model.certificateStateId = getValueInAnguAutocomplete(selected);
        }

        function clinicSelected(selected) {
            self.model.clinicId = getValueInAnguAutocomplete(selected);
            creatorSearchFunction(self, "clinicLocation_Search", clinicLocationService, "", { clinicId: self.model.clinicId });

        }
        function clinicLocationSelected(selected) {
            self.model.clinicLocationId = getValueInAnguAutocomplete(selected);
        }

        //creatorSearchFunction(self, "clinic_Search", clinicService);
        //creatorSearchFunction(self, "clinicLocation_Search", clinicLocationService);

        self.resourceCompany_Selected = resourceCompanySelected;
        self.businessUnit_Selected = businessUnitSelected;
        self.industry_Selected = IndustrySelected;
        self.department_Selected = resourceDepartmentSelected;
        self.division_Selected = resourceDivisionSelected;
        self.jobTitle_Selected = resourceJobTitleSelected;
        self.country_Selected = resourceCountrySelected;
        self.state_Selected = resourceStateSelected;
        self.city_Selected = resourceCitySelected;
        self.armedForcesCountry_Selected = resourceArmedForcesCountrySelected;
        self.supervisor_Selected = resourceSupervisorSelected;
        self.onResourceDesignationSelected = resourceDesignationSelected;
        self.certificateState_Selected = certificateStateSelected;

        self.clinic_Selected = clinicSelected;
        self.clinicLocation_Selected = clinicLocationSelected;

        self.uploadFileAvatarFunc = function (file, $invalidFile) {
            if ($invalidFile && $invalidFile.length > 0) {
                toaster.pop("error", "", "Max File Size 5MB");
            }
            if (!file)
                return;
            modal.confirm.templateDialog(function (data) {
                var mimeString = data.output.split(',')[0].split(':')[1].split(';')[0];
                self.uploadFileAvatar = base64ToBlob(data.output, mimeString);
                self.uploadFileAvatar.name = data.image.name;
            }, "Client/app/controllers/resource/templates/cropAvatar.html")({ image: file, output: null });

        }
        self.uploadFileResume = function (file, $invalidFile) {
            if ($invalidFile && $invalidFile.length > 0) {
                toaster.pop("error", "", "Max File Size 5MB");
            }

            if (!file)
                return;

            //Start ResumeFileType Validation 

            var nm = file.name;
            var temp = new Array();
            temp = nm.split(".");
            var tmplngth = temp.length;
            var ext = temp[parseInt(tmplngth) - 1];
            if (ext == "txt" || ext == "doc" || ext == "docx" || ext == "pdf") {
            }
            else {
                toaster.pop("error", "", "FileType .txt, .doc, .docx, .pdf");
                return;
            }
            // End ResumeFileType Validation 

            self.uploadFileCV = file;
            self.model.cvFileNameUrl = "";
            self.model.cvFileName = "";
        }
        self.deleteAvatar = function () {
            self.model.photoFileName = "";
            self.uploadFileAvatar = null;
            self.model.photoFileNameUrl = "";
        }
        self.deleteCV = function () {
            self.model.cvFileNameUrl = "";
            self.model.cvFileName = "";
            self.uploadFileCV = null;

        };

        self.visibleDeleteCvButton = function () {
            return self.model.cvFileNameUrl || self.model.cvFileName || self.uploadFileCV;

        };

        self.onCancel_Click = function () {
            $scope.ignoreRedirect = true;
            $state.go($state.current, { currentTab: self.currentTab, id: modelId }, { reload: true });
            $timeout(function () { toaster.pop("success", "", "Resource Restored Successful."); }, 2000);
        };

        self.onCancelButtonClick = function onCancelButtonClick() {
            systemFunctionsService.historyBackButtonClick($rootScope, $state, $scope);
            $scope.ignoreRedirect = true;
            window.history.back();
        }

        self.onShowAvailability_Click = function (form) {
            if (form) {
                if (!form.$submitted)
                    form.$setSubmitted();

                if (form.$invalid) {
                    return;
                }
            }
            self.callSubmit = true;
            self.availabilityModelShow.resourceId = self.model.id;
            var range = (self.availabilityModelShow.timeRange === "Month") ? "month" : (self.availabilityModelShow.timeRange === "Day") ? "day" : "year";
            var model = angular.copy(self.availabilityModelShow);
            if (self.availabilityModelShow.start) {
                model.start = self.availabilityModelShow.start;
            } else {
                model.start = moment().startOf(range);
            }
            if (self.availabilityModelShow.end) {
                model.end = self.availabilityModelShow.end;
            } else {
                model.end = moment().endOf(range);
            }

            //moment().startOf('year');
            if (model.start > model.end) {
                toaster.pop("error", "", "Start Date must be less than End date.");
                return;
            }


            model.start = model.start.format("LL");
            model.end = model.end.format("LL");

            model.timeRange = self.availabilityModelShow.timeRange;
            resourceDemand.getResourceCapacity(model, function (response) {
                generateAvailabilityDemandChartData(response);
            }, reject);
        };

        function availabilityTab_Click() {
            self.onShowAvailability_Click($scope.resourceEditForm);
        }
        /*
        self.onAvailabilityImageClick = function onAvailabilityImageClick() {
            self.currentTab = "personalInformation";
        }
        */
        self.availabilityModelSave = {};

        self.onSubmitAvailability_Click = function (form) {
            if (!form.$submitted)
                form.$setSubmitted();

            if (form.$invalid) {
                return;
            }
            self.callSubmit = true;

            self.availabilityModelSave.resourceId = self.model.id;
            self.availabilityModelSave.start = self.availabilityModelShow.start.format("LL");
            self.availabilityModelSave.end = self.availabilityModelShow.end.format("LL");
            self.availabilityModelSave.timeRange = self.availabilityModelShow.timeRange;

            resourceAvailability.save(self.availabilityModelSave, function (response) {
                toaster.pop("success", "", "availability save successful");
                self.callSubmit = false;
                if (self.availabilityListReload.reload)
                    self.availabilityListReload.reload();

                generateAvailabilityDemandChartData(response.availabilityDemandChartData);

                form.$submitted = false;
                form.$setPristine();
                self.availabilityModelSave = {};
            }, reject);
        };


        /*  If current tab is other than Personal Info, than need to call functions for that tab click manually from here. */
        if (self.currentTab == 'availability') {
            $timeout(function () {
                //availabilityTab_Click();
            }, 100);
        } else if (self.currentTab == 'summary') {
            $timeout(function () {
                summaryTab_Click();
            }, 100);
        }

        function generateAvailabilityDemandChartData(response) {
            self.availableVsDemandHeaders = [];
            self.availableVsDemandData = [];
            if (resource.length === 0)
                return;

            _.forEach(response.infoByDates, function (item) {
                self.availableVsDemandHeaders.push({
                    name: dateTimeHelper.getColumnName(moment(item.startDate), self.availabilityModelShow.timeRange),
                    id: uuid2.newguid()
                });

                // come here
                var col = {
                    available: item.available.toFixed(1),
                    demand: item.demand.toFixed(1),
                    hoursCount: item.hoursCount,
                    totalHoursCount: item.totalHoursCount,
                    reservedAvailabilityPercentage: item.reservedAvailabilityPercentage,
                    scheduledTimeOff: item.scheduledTimeOff,
                    lessHoliday: item.lessHoliday,
                    id: uuid2.newguid()
                };

                var availabilityObj = avaialabilityCalculationService.getAvailabilityData(col);
                self.availableVsDemandData.push(availabilityObj);
            });
            self.callSubmit = false;
        }

        self.demandModelSave = {};
        self.onSubmitDemand_Click = function (form) {
            if (!form.$submitted)
                form.$setSubmitted();

            if (form.$invalid) {
                return;
            }
            self.callSubmit = true;

            self.demandModelSave.resourceId = self.model.id;

            resourceDemand.save(self.demandModelSave, function () {
                toaster.pop("success", "", "demand save successful");
                self.callSubmit = false;
                if (self.demandListReload.reload)
                    self.demandListReload.reload();
            }, reject);

        };

        $scope.hideErrorTab = function () {
            $scope.formErrorList = null;
        }

        self.getAllResourceFields = function () {
            var allFieldsArray = [];
            if (configLayoutFactory.getResourceFields().length > 0) {
                allFieldsArray = allFieldsArray.concat(configLayoutFactory.getResourceFields())
            }
            if (self.skillViewModel.fields.length > 0) {
                allFieldsArray = allFieldsArray.concat(self.skillViewModel.fields)
            }
            if (self.educationViewModel.fields.length > 0) {
                allFieldsArray = allFieldsArray.concat(self.educationViewModel.fields)
            }
            if (self.certificateViewModel.fields.length > 0) {
                allFieldsArray = allFieldsArray.concat(self.certificateViewModel.fields)
            }
            if (self.languageViewModel.fields.length > 0) {
                allFieldsArray = allFieldsArray.concat(self.languageViewModel.fields)
            }
            if (self.criminalRecordViewModel.fields.length > 0) {
                allFieldsArray = allFieldsArray.concat(self.criminalRecordViewModel.fields)
            }
            if (self.previousEmploymentViewModel.fields.length > 0) {
                allFieldsArray = allFieldsArray.concat(self.previousEmploymentViewModel.fields)
            }
            if (self.workExperienceViewModel.fields.length > 0) {
                allFieldsArray = allFieldsArray.concat(self.workExperienceViewModel.fields)
            }
            if (self.travelDetailsViewModel.fields.length > 0) {
                allFieldsArray = allFieldsArray.concat(self.travelDetailsViewModel.fields)
            }
            if (self.professionalOrganization.fields.length > 0) {
                allFieldsArray = allFieldsArray.concat(self.professionalOrganization.fields)
            }
            return allFieldsArray;
        }

        /**
            Form Validation and Header Error Mechanism Functions.
        */

        function getTabName(tabId) {
            var tabList = configLayoutFactory.getResourceTab();
            var tabName = "";
            _.forEach(tabList, function (tab) {
                if (tab.container == tabId) {
                    tabName = tab.name;
                }
            });
            return tabName;
        }

        function getFieldByName(fieldId, tabName) {

            var resourceAllFields = self.getAllResourceFields();
            if (!tabName) {
                for (var fieldIndex = 0; fieldIndex < resourceAllFields.length; fieldIndex++) {
                    var field = resourceAllFields[fieldIndex];

                    if (field.id == fieldId || field.fieldName == fieldId) {
                        return field;
                    }
                }
            } else {

                for (var fieldIndex = 0; fieldIndex < resourceAllFields.length; fieldIndex++) {
                    var field = resourceAllFields[fieldIndex];

                    if ((field.id == fieldId || field.fieldName == fieldId) && field.container == tabName) {
                        return field;
                    }
                }
            }
        }
        function prepareResourceFormErrorList(form) {
            var errorList = {};
            _.forIn(form.$error, function (arr) {
                var arry = arr;
                _.forEach(arry, function (item) {

                    var currentTabName = null;


                    // Patch for Skill, Education like tabs where field name is different than model name and added _uniq value.
                    if (item.$options.tabValidationName) {
                        currentTabName = item.$options.tabValidationName;
                    }
                    var fieldName = item.$name;
                    if (fieldName === "rating_date") {
                        errorList["Performance Reviews"] = ["Rating Date"];
                        return;
                    }

                    if (fieldName.indexOf("_") > 0 && fieldName != "rating_date")
                        fieldName = fieldName.substring(0, fieldName.indexOf("_"));



                    var actualField = getFieldByName(fieldName, currentTabName);




                    if (!actualField && item.$options && item.$options.tabValidationName) {
                        //"travel Details"
                        currentTabName = item.$options.tabValidationName;
                    }
                    if (!actualField || !actualField.container) {
                        //return;
                    }
                    if (actualField) {
                        if (item.$options.tabValidationName) {
                            currentTabName = getTabName(item.$options.tabValidationName);
                        } else {
                            currentTabName = getTabName(actualField.container);
                        }
                    }

                    if (actualField) {
                        var fieldArray = errorList[currentTabName];
                        var isFieldExist = _.find(fieldArray, function (field) { return (field === actualField.name || field === actualField.label) });

                        if (actualField.container !== "") {
                            if (!fieldArray) {
                                fieldArray = [];
                                errorList[currentTabName] = fieldArray;
                            }
                            if (!isFieldExist)
                                fieldArray.push((actualField.name !== '' && actualField.name !== "undefined" && actualField.name) ? actualField.name : actualField.label);
                        }
                    } else {
                        //_.find(self.otherContainer, )
                        var tab = _.find(configLayoutFactory.getResourceTab(), { id: currentTabName });

                        if (tab && tab.name) {
                            var field = getFieldByName(fieldName);
                            if (!errorList[tab.name] && typeof errorList[tab.name] !== 'Array')
                                errorList[tab.name] = [];
                            if (field && field.name)
                                errorList[tab.name].push(field.name);
                        } else {

                        }
                    }
                });
            });
            $scope.formErrorList = errorList;
        }

        /**
            Form Validation and Header Error Mechanism Functions. ---------------------
        */

        function showSupervisorConflictResolver() {
            // Show POPUP for supervisor.
            var modalInstance = $uibModal.open({
                backdrop: 'static',
                controller: "SupervisorConflictResolverCtrl",
                controllerAs: "SupervisorConflictResolverCtrl",
                templateUrl: "Client/app/controllers/resource/templates/supervisor-conflict-resolver-modal.html",
                size: "md",
                resolve: {
                    resourceId: self.model.id
                }
            });
            modalInstance.result.then(function (data) {
                //console.info(data);
                if (!data) {
                    $state.go($state.current, { id: self.model.id }, { reload: true })
                } else {
                    self.onSave_Click('resourceEditForm');
                }

            }, function () { });
        }

        self.onSave_Click = function (form) {
            var newId = 0;
            var tempYearsEmployed;

            systemFunctionsService.scrollToTopZero();

            function successful() {
                $scope.ignoreRedirect = true;
                if (modelId && modelId > 0) {
                    toaster.pop("success", "", "Resource updated successful");
                } else {
                    toaster.pop("success", "", "Resource Added successful");
                }
                modelId = self.model.id;
                self.model.yearsEmployed = tempYearsEmployed;
                $state.go($state.current, {
                    currentTab: self.currentTab,
                    id: modelId,
                    availabilityData: $scope.tempAvailability.data,
                    availabilityHeaders: $scope.tempAvailability.headers
                }, { reload: true });
                self.callSubmit = false;
                //self.loadCorrect = false;
            }

            function openTabInErrorModel() {
                _.forIn(form.$error, function (arr) {
                    _.forEach(arr, function (item) {
                        self[item.$options.accordionOpenFieldName] = true;
                    });
                });
            }


            $scope.formErrorList = null;
            $scope.tempAvailability = {};

            function updateErrorFieldResourceType() {
                _.forEach(form.$error['autocomplete-required'], function (formItem) {
                    if (formItem.$name === "resourceType" && form.$error['autocomplete-required'].length == 1 && self.model.resourceType) {
                        form.$invalid = false;
                    }
                });

            }

            updateErrorFieldResourceType();
            if (form.$invalid) {
                openTabInErrorModel();
                prepareResourceFormErrorList(form);
                return;
            }

            if (self.model.ssn && self.model.ssn.length > 0 && self.model.ssn.length != 9) {
                $("#SSN_ERROR_MSG").html('Invalid SSN Number');
                $("#ssn").addClass('element-validation-error');
                if ($scope.formErrorList == null) {
                    $scope.formErrorList = [];
                }
                //$scope.formErrorList.push({ "Personal Info": new Array("SSN") });
                var errorVar = {};
                errorVar["Personal Info"] = ["SSN"];
                $scope.formErrorList = errorVar;
                return;
            } else {
                $scope.formErrorList = null;
            }

            //self.loadCorrect = true;
            self.callSubmit = true;
            if (counterFilesPromise !== 0) {
                toaster.pop("warning", "", "waiting file upload");
                return;
            }
            preSendChangeModel();
            var defer = $q.defer();

            $scope.tempAvailability.data = self.availableVsDemandData;
            $scope.tempAvailability.headers = self.availableVsDemandHeaders;

            if (modelId && modelId > 0) {
                tempYearsEmployed = self.model.yearsEmployed;
                self.model.yearsEmployed = parseInt(self.model.yearsEmployed);
                var copyModel = angular.copy(self.model);
                if (self.IS_BUILD_FOR_STINSON) {
                    modifyCertificatesForIRMS5(copyModel);
                    modifyEducationsForIRMS5(copyModel);
                }
                self.loadCorrect = resource.update({ id: modelId }, copyModel, function (responce) {
                    if (self.IS_BUILD_FOR_STINSON) {
                        _.forEach(responce.educations, function (edu) {
                            if (edu.dateObtained) edu.dateObtained = edu.dateObtained.substring(0, 4);// moment(edu.dateObtained).format("YYYY");
                        })
                        _.forEach(responce.certificates, function (edu) {
                            if (edu.issueDate) edu.issueDate = edu.issueDate.substring(0, 4);//moment(edu.issueDate).format("YYYY");
                        })
                    }
                    self.model = responce;
                    self.model.yearsEmployed = tempYearsEmployed;
                    defer.resolve(responce);
                }, function (error) {
                    if (error && error.data) {
                        var statusCode = error.data.modelState.errorCode;
                        if (statusCode && statusCode[0] == 1008) {
                            $scope.ignoreRedirect = true;
                            showSupervisorConflictResolver();
                        }
                    }
                    //toaster.pop("error", "", "Could not process you request, please try again later.");
                    reject();
                });
            } else {
                tempYearsEmployed = self.model.yearsEmployed;
                var copyModel = angular.copy(self.model);
                if (self.IS_BUILD_FOR_STINSON) {
                    modifyCertificatesForIRMS5(copyModel);
                    modifyEducationsForIRMS5(copyModel);
                }
                self.loadCorrect = resource.save(copyModel, function (responce) {
                    if (self.IS_BUILD_FOR_STINSON) {
                        _.forEach(responce.educations, function (edu) {
                            if (edu.dateObtained) edu.dateObtained = edu.dateObtained.substring(0, 4);// moment(edu.dateObtained).format("YYYY");
                        })
                        _.forEach(responce.certificates, function (edu) {
                            if (edu.issueDate) edu.issueDate = edu.issueDate.substring(0, 4);//moment(edu.issueDate).format("YYYY");
                        })
                    }

                    self.model = responce;
                    self.model.yearsEmployed = tempYearsEmployed;
                    defer.resolve(responce);
                }, function (error) {
                    var statusCode = error.data.modelState.errorCode;
                    if (statusCode[0] == 1008) {
                        $scope.ignoreRedirect = true;
                        showSupervisorConflictResolver();
                    }
                    //toaster.pop("error", "", "Could not process you request, please try again later.");
                    reject();
                });
            }

            defer.promise.then(function (responce) {
                newId = responce.id;
                var deferUploadCsv = $q.defer();
                var deferUploadAvatar = $q.defer();

                if (self.uploadFileAvatar) {
                    resource.uploadAvatar(uploader, self.uploadFileAvatar, responce.id, function () {
                        deferUploadAvatar.resolve();
                    }, reject);
                } else deferUploadAvatar.resolve();

                if (self.uploadFileCV) {
                    resource.uploadCV(uploader, self.uploadFileCV, responce.id, function () {
                        deferUploadCsv.resolve();
                    }, reject);
                } else deferUploadCsv.resolve();

                $q.all([deferUploadAvatar.promise, deferUploadCsv.promise]).then(function () {
                    successful();
                });

            });
        }; // Save Func ****

        self.stopRecording = function () {
            audioService.recordRTC.stopRecording(function (url) {
                audioService.recordingEndedCallback(url);
                stopStream();
            });
        }

        self.canUpdateOrInsertButtonDisabled = canUpdateOrInsertButtonDisabled;
        self.calculateWightsGren = calculateWightsGren;
        self.calculateWightsRed = calculateWightsRed;
        self.runRecord = function () {
            captureAudio(commonConfig);

            audioService.mediaCapturedCallback = function () {
                audioService.recordRTC = RecordRTC(audioService.stream, {
                    type: 'audio',
                    bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                    sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                    leftChannel: params.leftChannel || false,
                    disableLogs: params.disableLogs || false,
                    recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
                });

                audioService.recordingEndedCallback = function () {
                    if (!$scope.$$phase)
                        $scope.$apply(function () {
                            self.audioSrc = audioService.recordRTC.toURL();
                        });
                };

                audioService.recordRTC.startRecording();
            };
        };

        /*    Check these Both Functions and remove if it is under common function for AVD  */
        function calculateWightsGren(col) {
            var WightsGren = (col.available / col.hoursCount) * 100;
            return WightsGren.toFixed(2);
        }
        function calculateWightsRed(col) {
            var WightsRed = (col.demand / col.hoursCount) * 100;
            return WightsRed.toFixed(2);
        }
        /*  Need to remove ^ */

        function setFormScope(form) {
            $scope.__form = form;
        }

        function getForm() {
            return $scope.__form.resourceEditForm;
        }

        function initAvailabilityModelShow() {
            self.availabilityModelShow = {};
            var dateNow = moment().startOf('month');
            var newDate = dateNow.clone().add(1, "years").subtract(1, "days");
            self.availabilityModelShow.start = dateNow;
            self.availabilityModelShow.end = newDate;
            self.availabilityModelShow.timeRange = "Month";
        }

        function getRestrictedFieldList(fieldName) {
            var fieldList = [];
            if (fieldName === "Salary") {
                fieldList.push({ id: "salary" });
                fieldList.push({ id: "bonusOrOtherPay" });
                return fieldList;
            } else if (fieldName === "Personal Info") {
                fieldList.push({ id: "martialStatus" });
                fieldList.push({ id: "ssn" });
                fieldList.push({ id: "lgbt" });
                fieldList.push({ id: "ethnicity" });
                fieldList.push({ id: "gender" });
                fieldList.push({ id: "emergencyContactName" });
                fieldList.push({ id: "emergencyContactPhoneNumber" });
                return fieldList;
            } else if (fieldName === "Background Checks") {
                fieldList.push({ id: "armedForcesCountry" });
                fieldList.push({ id: "armedForcesBranch" });
                fieldList.push({ id: "armedForces" });
                fieldList.push({ id: "drugTest" });
                fieldList.push({ id: "creditReportOk" });
                fieldList.push({ id: "motorVehicleReport" });
                fieldList.push({ id: "motorVehicleReportOther" });
                fieldList.push({ id: "professionalReferenceChecks" });
                fieldList.push({ id: "professionalReferenceChecksOther" });
                fieldList.push({ id: "credentialVerifications" });
                fieldList.push({ id: "credentialVerificationsOther" });
                fieldList.push({ id: "employmentEligibilityVerification" });
                fieldList.push({ id: "employmentEligibilityVerificationOther" });
                fieldList.push({ id: "formI9" });
                fieldList.push({ id: "formI9Other" });
                fieldList.push({ id: "formEVerify" });
                fieldList.push({ id: "formEVerifyOther" });
                fieldList.push({ id: "internationalWorkHistory" });
                fieldList.push({ id: "internationalWorkHistoryOther" });
                return fieldList;
            }
        }

        function parceLayoutModel() {
            self.tabs = _.unionBy(initialValues[1].tabs, configLayoutFactory.getResourceTab(), "id");
            initialValues[1].containers = initialValues[1].containers || configLayoutFactory.getDefaultResourceContainer();
            self.personalInfoContainer = _.find(initialValues[1].containers, { id: "personalInfo" });
            self.workingDetailsContainer = _.find(initialValues[1].containers, { id: "workingDetails" });
            self.backgroundCheckContainer = _.find(initialValues[1].containers, { id: "backgroundCheck" });
            self.resourceRatingContainer = _.find(initialValues[1].containers, { id: "resourceRating" });
            self.otherContainer = _.find(initialValues[1].containers, { id: "other" });
            configLayoutFactory.addRequiredCustomField(initialValues[1].containers, self.model.customFieldValues, self.otherContainer);
            self.generalPoropertyList = configLayoutFactory.getResourceFields();
            var userRules = systemFunctionsService.getKeyFromLocalStorage("irms_user_rules");
            if (userRules && userRules.restrictAccess && userRules.restrictAccess.length > 0) {
                _.forEach(userRules.restrictAccess, function (fieldName) {
                    if (fieldName === "Salary" || fieldName === "Personal Info")
                        configLayoutFactory.restrictFields(getRestrictedFieldList(fieldName), self.generalPoropertyList);
                    else
                        configLayoutFactory.restrictFields(getRestrictedFieldList(fieldName), self.backgroundCheckContainer);
                });

            }
            self.__ResourceEditCtrl = "ResourceEditCtrl";
        }
        self.isTabVisible = function isTabVisible(tabId) {
            var tab = _.find(self.tabs, { id: tabId });
            return (tab && tab.visible)
        }
        /**
            Updating Data Lists for Resume
        */
        function initModelForResume() {
            _.forEach(self.model.educations, function (edu) {
                edu.resumeItemSelected = true;
            });
            _.forEach(self.model.skills, function (edu) {
                edu.resumeItemSelected = true;
            });
            _.forEach(self.model.certificates, function (edu) {
                edu.resumeItemSelected = true;
            });
            _.forEach(self.model.previousEmployments, function (edu) {
                edu.resumeItemSelected = true;
            });
            _.forEach(self.model.resourceProfessionalOrganizations, function (edu) {
                edu.resumeItemSelected = true;
            });
            _.forEach(self.model.resourceWorkExperiences, function (edu) {
                edu.resumeItemSelected = true;
            });
            _.forEach(self.model.spokenLanguages, function (edu) {
                edu.resumeItemSelected = true;
            });

        }

        self.getTabDisplayName = function (tabId) {
            if (self.tabs && self.tabs.length > 0) {
                var tab = _.find(self.tabs, { id: tabId });
                if (tab && tab.name)
                    return $filter('translate')(tab.name);
            }
        }


        self.addAssignment = function () {
            $state.go("assignmentEdit", { resourceId: self.model.id });
        }

        /**
            Fix for Required AnguAutoCOmplete not working with Value also.
        *
        if (self.model && !self.model.department)
            self.model.department = {};
        if (self.model && !self.model.resourceCompany)
            self.model.resourceCompany = {};
        if (self.model && !self.model.jobTitle)
            self.model.jobTitle = {};
        /*---------------*/

        if (modelId) {
            modelId = parseInt(modelId);
            self.model = initialValues[2];
            self.titleText = self.model.name;
            if (!tempResourceModel)
                tempResourceModel = angular.copy(self.model); // for locally storing model.
            calculateResourceExperienceBasedOnCertificate();
        } else {
            self.model.customFieldValues = initialValues[0];
            self.titleText = "New Resource";
        }
        parceLayoutModel();

        initAvailabilityModelShow();

        initModelForResume();

        self.restrictPersonalInfo = permissionWorker.restrictPersonalInfo;
        self.restrictSalary = permissionWorker.restrictSalary;
        self.restrictBackgroundChecks = permissionWorker.restrictBackgroundChecks;
        self.restrictCriminalRecords = permissionWorker.restrictCriminalRecords;
        self.restrictPreviousEmployments = permissionWorker.restrictPreviousEmployments;

        creatorSearchFunction(self, "searchCompanies", resourceCompany);
        creatorSearchFunction(self, "searchSkills", skill);
        creatorSearchFunction(self, "searchCertificate", certificate);
        creatorSearchFunction(self, "searchCertificateState", StateService);
        creatorSearchFunction(self, "searchLanguage", language);
        creatorSearchFunction(self, "searchCountry", country);
        creatorSearchFunction(self, "country_Search", country);
        creatorSearchFunction(self, "zipCode_Search", ZipCodeService);
        

        creatorSearchFunction(self, "armedForcesCountry_Search", country);
        creatorSearchFunction(self, "jobTitle_Search", jobTitle);
        creatorSearchFunction(self, "resourceCompany_Search", resourceCompany);
        creatorSearchFunction(self, "state_Search", StateService);
        creatorSearchFunction(self, "city_Search", CityService);
        creatorSearchFunction(self, "divisions_Search", division);
        creatorSearchFunction(self, "supervisor_Search", resource, "firstName");
        //creatorSearchFunction(self, "supervisor_Search", ClinicianService);

        creatorSearchFunction(self, "department_Search", department);
        creatorSearchFunction(self, "resourceCompany2_Search", resourceCompany);
        creatorSearchFunction(self, "division_Search", division);
        creatorSearchFunction(self, "clinic_Search", clinicService);
        creatorSearchFunction(self, "clinicLocation_Search", clinicLocationService);


        // For Dynamic Dropdowns
        dropdownConfigsFunction(self, "committeeMembershipSearch", dynamicDropDownConfigService, "committeeMembership");
        dropdownConfigsFunction(self, "committeeTitleSearch", dynamicDropDownConfigService, "committeeTitle");
        dropdownConfigsFunction(self, "businessUnit_Search", dynamicDropDownConfigService, "businessUnits");
        dropdownConfigsFunction(self, "industry_Search", dynamicDropDownConfigService, "industry", { pageIndex: 1, pageSize: 999 });
        dropdownConfigsFunction(self, "resourceDesignation_Search", dynamicDropDownConfigService, "resourceDesignation");
        dropdownConfigsFunction(self, "resourcePracticeAreas_Search", dynamicDropDownConfigService, "practiceArea");
        //dropdownConfigsFunction(self, "country_Search", country, "country");

        generateList();
        listModelInitialization();

        /*  Industry Configurations */
        self.industry_Search().then(function (data) {
            self.industry_Options = data;
            if (!(self.model.industry && self.model.industry.length > 0)) {
                self.model.industry = [];
            }
        });

        self.industry_Settings = {
            externalIdProp: 'industryId',
            closeOnBlur: true,
            showCheckAll: false,
            showUncheckAll: false,
            displayProp: 'name',
            smartButtonMaxItems: 2,
            buttonClasses: "dropdown-toggle btn btn-default tabable",
            smartButtonTextConverter: function (itemText, originalItem) {
                return itemText;
            }
        };
        self.divisions_Search().then(function (data) {
            self.divisions_Options = data;
            if (!(self.model.divisions && self.model.divisions.length > 0)) {
                self.model.divisions = [];
            }
        });

        self.divisions_Settings = {
            externalIdProp: 'divisionId',
            closeOnBlur: true,
            showCheckAll: false,
            showUncheckAll: false,
            displayProp: 'name',
            smartButtonMaxItems: 2,
            buttonClasses: "dropdown-toggle btn btn-default tabable",
            smartButtonTextConverter: function (itemText, originalItem) {
                return itemText;
            }
        };

        self.resourcePracticeAreas_Search().then(function (data) {
            self.resourcePracticeAreas_Options = data;
            if (!(self.model.resourcePracticeAreas && self.model.resourcePracticeAreas.length > 0)) {
                self.model.resourcePracticeAreas = [];
            }
        });

        self.resourcePracticeAreas_Settings = {
            externalIdProp: 'practiceAreaId',
            closeOnBlur: true,
            showCheckAll: false,
            showUncheckAll: false,
            displayProp: 'name',
            smartButtonMaxItems: 2,
            buttonClasses: "dropdown-toggle btn btn-default tabable",
            smartButtonTextConverter: function (itemText, originalItem) {
                return itemText;
            }
        };

        self.timeZoneInfo_Enum = initialValues[3];

        self.exportResource = function () {
            window.open("api/Resources/export?id=" + modelId, "_blank", "");
        };

        ////////////////////////////////////////////////////////////////////

        function canUpdateOrInsertButtonDisabled() {
            if (modelId > 0) {
                return permissionWorker.canResourceUpdate();
            }

            return permissionWorker.canResourceInsert();
        }

        function intallFirefoxScreenCapturingExtension() {
            InstallTrigger.install({
                'Foo': {
                    // URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                    URL: 'https://addons.mozilla.org/firefox/downloads/file/355418/enable_screen_capturing_in_firefox-1.0.006-fx.xpi?src=cb-dl-hotness',
                    toString: function () {
                        return this.URL;
                    }
                }
            });
        }

        function stopStream() {
            if (audioService.stream && audioService.stream.stop) {
                audioService.stream.getTracks()[0].stop();
                audioService.stream = null;
            }
        }

        function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
            navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
        }

        function captureAudio(config) {
            captureUserMedia({ audio: true }, function (audioStream) {

                config.onMediaCaptured(audioStream);

                audioStream.onended = function () {
                    config.onMediaStopped();
                };
            }, function (error) {
                config.onMediaCapturingFailed(error);
            });
        }

        function employmentTypeIsSalary() {
            return self.model.employmentType === "Salary";
        }

        function getBlobFileToRecord() {
            var blob = recordRTC.getBlob();
            blob.name = "record.wav";
        }

        function reject() {
            self.callSubmit = false;
        }

        function generateList() {
            self.range = ["L", "M", "H"];
            self.prefix_Enum = ["Ms", "Miss", "Mrs", "Mr", "Dr", "Atty", "Prof", "Hon", ];
            self.lgbt_Enum = [{
                title: "Not Applicable",
                value: "NotApplicable"
            },
            {
                title: "Lesbian",
                value: "Lesbian"
            }, {
                title: "Gay",
                value: "Gay"
            }, {
                title: "Bisexual",
                value: "Bisexual"
            }, {
                title: "Transgender",
                value: "Transgender"
            }];
            self.suffix_Enum = ["Sr.", "Jr.", "M.D.", "Ph.D."];
            self.visaOrWorkPermitEnum = ["Visa", "Permit"];
            self.salaryPer = ["Hour", "Week", "Month", "Year"];
            self.resourceType_Enum = ["Internal", "External", "Consultant"];
            self.gender_Enum = ["Male", "Female"];
            self.relationship_Enum = [
                {
                    title: "Daughter",
                    value: "Daughter"
                }, {
                    title: "Friend",
                    value: "Friend"
                }, {
                    title: "Parent",
                    value: "Parent"
                }, {
                    title: "Partner",
                    value: "Partner"
                }, {
                    title: "Son",
                    value: "Son"
                }, {
                    title: "Sibling",
                    value: "Sibling"
                }, {
                    title: "Spouse",
                    value: "Spouse"
                }, {
                    title: "Other",
                    value: "Other"
                }
            ];
            self.employmentType_Enum = [
            {
                title: "Exempt",
                value: "Salary"
            },
            {
                title: "Non-Exempt",
                value: "Hourly"
            }];
            self.yesNoOtherEnum = ["NA", "Yes", "No", "Other"];
            self.passFailENUM = ["Pass", "Fail"];
            self.martialStatus_Enum = ["Single", "Married"];
            self.ethnicity_Enum = [
                "Native American or Alaska Native",
                "Caucasian",
                "Hispanic",
                "Black or African American",
                "Asian",
                "Native Hawaiian or Pacific Islander",
                "Other"
            ];
            self.availabilityTimeRange = dateTimeHelper.dateTypeEnum();
        }

        function setResourceIdInList(item) {
            if (modelId)
                item.resourceId = modelId;
        }

        self.isFieldVisibleOnSummarySection = function (field) {
            return true;
        }

        function listModelInitialization() {

            self.travelDetailsViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonName: "Add Travel Details",
                fields: [
                    {
                        label: "Visa or Work Permit",
                        fieldName: "visaOrPermit",
                        container: 'travelDetails',
                        dataType: "ui-select",
                        list: self.visaOrWorkPermitEnum,
                        classContainer: "col-md-3",
                        required: true
                    },
                    {
                        label: "Country",
                        fieldName: "countryId",
                        container: 'travelDetails',
                        initValue: "country",
                        dataType: "angucomplete-alt",
                        classContainer: "col-md-3",
                        search: self.searchCountry,
                        required: true
                    },
                    {
                        label: "Remaining Days",
                        fieldName: "timeRemainingInDays",
                        container: 'travelDetails',
                        dataType: "number",
                        classContainer: "col-md-3",
                        required: true,
                        min: 1
                    },
                    {
                        label: "Special Consideration",
                        container: 'travelDetails',
                        fieldName: "specialConsiderations",
                        classContainer: "col-md-3"
                    },
                ]
            };

            self.workingDetailViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonName: "Add Behavioral Detail",
                fields: [
                    {
                        label: "Behavioral Detail",
                        fieldName: "behaviour",
                        container: 'travelDetails',
                        dataType: "text",
                        classContainer: "col-md-4",
                        required: true
                    }
                ]
            };

            self.previousEmploymentViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonText: "Add Previous Employment",
                mainContainerClass: "row row-md-4",
                fields: [
                    {
                        label: "Company Name",
                        container: 'previousEmployments',
                        fieldName: "name",
                        classContainer: "col-md-4",
                        required: false
                    },
                    {
                        label: "Address",
                        container: 'previousEmployments',
                        fieldName: "address",
                        classContainer: "col-md-4",
                        required: false
                    },
                    {
                        label: "Title of Position",
                        container: 'previousEmployments',
                        fieldName: "titleOfPosition",
                        classContainer: "col-md-4",
                        required: false
                    },
                    {
                        label: "Duties of Position",
                        container: 'previousEmployments',
                        fieldName: "dutiesOfPosition",
                        classContainer: "col-md-4",
                        required: false
                    },
                    {
                        label: "Reason For Leaving",
                        container: 'previousEmployments',
                        fieldName: "reasonForLeaving",
                        classContainer: "col-md-4",
                        required: false
                    },
                    {
                        label: "Start Date",
                        container: 'previousEmployments',
                        fieldName: "startDate",
                        dataType: "date-time",
                        classContainer: "col-md-4",
                        required: true,
                        equalDate: "endDate"
                    },
                    {
                        label: "Number of Supervised",
                        container: 'previousEmployments',
                        fieldName: "numberOfSupervised",
                        dataType: "ng-currency",
                        classContainer: "col-md-4",
                        fractionSize: 0,
                        currencySymbol: "",
                        required: false,
                        max: 100
                    },
                    {
                        label: "Salary",
                        container: 'previousEmployments',
                        fieldName: "salary",
                        dataType: "ng-currency",
                        classContainer: "col-md-4",
                        required: false,
                        min: 0,
                        max: 1000000
                    },
                    {
                        label: "Salary Per",
                        container: 'previousEmployments',
                        fieldName: "salaryPer",
                        dataType: "ui-select",
                        list: self.salaryPer,
                        classContainer: "col-md-4",
                        required: function (item) {
                            if (item.salary) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    },
                    {
                        label: "Hours per Week",
                        container: 'previousEmployments',
                        fieldName: "hoursPerWeek",
                        dataType: "ng-currency",
                        classContainer: "col-md-4",
                        required: false,
                        placeholder: "Hours",
                        min: 0,
                        max: 100,
                        fractionSize: 0,
                        currencySymbol: ""
                    },
                    {
                        label: "End Date",
                        container: 'previousEmployments',
                        fieldName: "endDate",
                        dataType: "date-time",
                        classContainer: "col-md-4",
                        required: true
                    }
                ]
            };

            self.criminalRecordViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonName: "Add Criminal Record",
                fields: [
                    {
                        label: "Date of Offense",
                        fieldName: "dateOfOffense",
                        container: 'criminalRecords',
                        dataType: "date-time",
                        classContainer: "col-md-3",
                        required: true
                    },
                    {
                        label: "Location of Offense",
                        fieldName: "locationOfOffense",
                        container: 'criminalRecords',
                        classContainer: "col-md-3",
                        required: true
                    },
                    {
                        label: "Offense",
                        fieldName: "offense",
                        container: 'criminalRecords',
                        classContainer: "col-md-3",
                        required: true
                    },
                    {
                        label: "Penalty or Disposition",
                        fieldName: "penaltyOrDisposition",
                        container: 'criminalRecords',
                        classContainer: "col-md-3",
                        required: true
                    }
                ]
            };

            self.workExperienceViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonName: "Add Work Experience",
                fields: [
                    {
                        label: "Position",
                        container: 'workExperience',
                        fieldName: "workExperienceJobTitle",
                        classContainer: "col-md-3",
                        initValue: "workExperienceJobTitle",
                        dataType: "angucomplete-alt",
                        required: true
                    },
                    {
                        label: "Start Date",
                        container: 'workExperience',
                        fieldName: "startDate",
                        dataType: "date-time",
                        classContainer: "col-md-3",
                        required: false,
                        maxDateField: "endDate",

                    },
                    {
                        label: "End Date",
                        container: 'workExperience',
                        fieldName: "endDate",
                        dataType: "date-time",
                        classContainer: "col-md-3",
                        required: false

                    }
                ]
            };

            self.educationViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonText: "Add School",
                mainContainerClass: "row  row-delete row-education", //
                fields: [
                    {
                        label: "Degree",
                        fieldName: "subject",
                        container: 'educations',
                        list: self.subjectList,
                        classContainer: "col-md-3",
                        required: true
                    },
                    {
                        label: "Designation",
                        fieldName: "resourceDesignation",
                        initValue: "resourceDesignation",
                        classContainer: "col-md-3",
                        dataType: "angucomplete-alt",
                        container: 'personalInfo',
                        search: self.resourceDesignation_Search,
                        callBackFunc: self.onResourceDesignationSelected,
                        required: false
                    },
                    {
                        label: "Name Of School",
                        fieldName: "nameOfSchool",
                        container: 'educations',
                        classContainer: "col-md-3",
                        required: true
                    },
                    {
                        label: "Date Obtained",
                        fieldName: "dateObtained",
                        container: 'educations',
                        dataType: (self.IS_BUILD_FOR_STINSON) ? "text" : "date-time",  // fix for IRMS5 Only.. making it TEXT Field, and allowed manually typed year.
                        //minView: (self.IS_BUILD_FOR_STINSON) ?"year":,
                        //format: "YYYY",
                        classContainer: "col-md-3",
                        required: false
                    },

                    /*{
                        label: "Start Date",
                        fieldName: "startDate",
                        container: 'educations',
                        dataType: "date-time",
                        classContainer: "col-md-4",
                        required: false
                    },
                    {
                        label: "End Date",
                        fieldName: "endDate",
                        container: 'educations',
                        dataType: "date-time",
                        classContainer: "col-md-4",
                        required: false
                    }*/
                ]
            };

            self.professionalOrganization = {
                transformModelPrePush: setResourceIdInList,
                addButtonName: "Add Organization",
                fields: [
                    {
                        label: "Organization / Activity",
                        fieldName: "organization",
                        initValue: "skill",
                        dataType: "text",
                        container: 'personalInfo',
                        classContainer: "col-md-3",
                        required: true
                    },
                    {
                        label: "Committee Membership",
                        fieldName: "committeeMembership",
                        dataType: "angucomplete-alt",
                        container: 'personalInfo',
                        initValue: "committeeMembership",
                        search: self.committeeMembershipSearch,
                        classContainer: "col-md-3",
                        required: false,
                        min: 0,
                        max: 100
                    },
                    {
                        label: "Committee Title",
                        fieldName: "committeeTitle",
                        initValue: "committeeTitle",
                        dataType: "angucomplete-alt",
                        container: 'personalInfo',
                        search: self.committeeTitleSearch,
                        classContainer: "col-md-3",
                        required: false,
                        min: 0,
                        max: 10,
                    }
                ]
            };

            self.resourceRecognitionViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonName: "Add Recognition",
                fields: [
                    {
                        label: "Recognition",
                        fieldName: "recognition",
                        dataType: "textarea",
                        container: 'recognitionSection',
                        classContainer: "col-md-3",
                        rows: 3,
                        required: false
                    },
                ]
            };

            self.skillViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonName: "Add Skill",
                fields: [
                    {
                        label: "Skill",
                        fieldName: "skillId",
                        initValue: "skill",
                        container: "skills",
                        dataType: "angucomplete-alt",
                        classContainer: "col-md-3",
                        search: self.searchSkills,
                        required: true
                    }, {
                        label: "Description",
                        fieldName: "description",
                        container: "skills",
                        dataType: "text",
                        classContainer: "col-md-3",
                        required: false
                    },
                    {
                        label: "Years of Experience",
                        fieldName: "yearsOfExpirience",
                        dataType: "number",
                        container: "skills",
                        classContainer: "col-md-3",
                        required: false,
                        min: 0,
                        max: 100
                    },
                    {
                        label: "Proficiency",
                        fieldName: "proficiency",
                        container: "skills",
                        classContainer: "col-md-3",
                        required: false,
                        min: 0,
                        max: 10,
                        dataType: "number"
                    },
                    {
                        label: "Last Updated",
                        fieldName: "lastUpdated",
                        container: "skills",
                        dataType: "date-time",
                        classContainer: "col-md-3"
                    }
                ]
            };

            self.availabilityViewModel = {
                fields: [
                {
                    fieldName: "availabilityFrom",
                    label: "From date",
                    dataType: "date-time",
                    inputClass: "from-date",
                    classContainer: "col-md-3"
                },
                {
                    fieldName: "maxAvailabilityPercentage",
                    label: "Availability %",
                    dataType: "number",
                    inputClass: "availability",
                    classContainer: "col-md-3"
                },
                {
                    fieldName: "created",
                    label: "Post Date",
                    dataType: "date-time",
                    inputClass: "created",
                    classContainer: "col-md-3"
                },
                {
                    fieldName: "changedBy",
                    label: "Changed By",
                    dataType: "text",
                    inputClass: "changed-by",
                    classContainer: "col-md-3"
                }
                ]
            };

            self.certificateViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonName: "Add Certificate",
                fields: [
                    {
                        label: "Certificate",
                        fieldName: "certificateId",
                        initValue: "certificate",
                        container: 'certificates',
                        dataType: "angucomplete-alt",
                        classContainer: "col-md-4",
                        search: self.searchCertificate,
                        required: true
                    }, {
                        label: "State",
                        fieldName: "certificateStateId",
                        initValue: "certificateState",
                        container: 'certificates',
                        dataType: "angucomplete-alt",
                        classContainer: "col-md-4",
                        search: self.searchCertificateState,
                        required: false
                    }, {
                        label: "Issue Date",
                        fieldName: "issueDate",
                        container: 'certificates',
                        dataType: (self.IS_BUILD_FOR_STINSON && self.IS_BUILD_FOR_STINSON == true) ? "text" : "date-time",// Fix for IRMS5 Only......
                        //minView: "year",
                        //format: "YYYY",
                        classContainer: "col-md-2",
                        required: false
                    }, {
                        label: "Expiration Date",
                        fieldName: "expirationDate",
                        container: 'certificates',
                        dataType: "date-time",
                        classContainer: "col-md-2",
                        required: false
                    }
                ]
            };

            self.languageViewModel = {
                transformModelPrePush: setResourceIdInList,
                addButtonName: "Add Language",
                mainContainerClass: "row row-language row-delete",
                fields: [
                    {
                        label: "Language",
                        fieldName: "languageId",
                        container: 'languages',
                        initValue: "language",
                        dataType: "angucomplete-alt",
                        classContainer: "col-md-3",
                        search: self.searchLanguage,
                        required: true
                    },
                    {
                        label: "Years of Experience",
                        fieldName: "yearsOfExpirience",
                        container: 'languages',
                        dataType: "ng-currency",
                        classContainer: "col-md-2",
                        required: true,
                        fractionSize: 0,
                        currencySymbol: "",
                        min: 1,
                        max: 100
                    },
                    {
                        label: "Speaking Proficiency",
                        fieldName: "speakingProficiency",
                        classContainer: "col-md-2",
                        container: 'languages',
                        dataType: "ng-currency",
                        required: true,
                        fractionSize: 0,
                        currencySymbol: "",
                        min: 1,
                        max: 10
                    },
                    {
                        label: "Writing Proficiency",
                        fieldName: "writingProficiency",
                        container: 'languages',
                        classContainer: "col-md-2",
                        dataType: "ng-currency",
                        required: true,
                        fractionSize: 0,
                        currencySymbol: "",
                        min: 1,
                        max: 10
                    },
                    {
                        label: "Voice File",
                        fieldName: "files",
                        classContainer: "col-md-3 resume_add addFile-wrap",
                        container: 'languages',
                        dataType: "ng-custom-template",
                        templateSrc: "Client/app/controllers/resource/templates/LanguageFileItemList.html",
                        noValidate: true,
                        fieldDownload: "s3BucketUrl",
                        deleteItem: function (file, arr) {
                            if (!file.s3BucketUrl) {
                                var languageItemIndex = arr.indexOf(file);
                                arr.splice(languageItemIndex, 1);
                            }
                            file.isDelete = true;
                        },
                        upload: function (file, $invalidFile, item) {
                            if ($invalidFile && $invalidFile.length > 0) {
                                toaster.pop("error", "", "Max File Size 5MB");
                            }
                            if (!file)
                                return;

                            //Start AudioFileType Validation 
                            var nm = file.name;
                            var temp = new Array();
                            temp = nm.split(".");
                            var tmplngth = temp.length;
                            var ext = temp[parseInt(tmplngth) - 1];
                            if (ext == "wma" || ext == "mp3" || ext == "wav" || ext == "ra" || ext == "ra" || ext == "ram" || ext == "rm" || ext == "mid" || ext == "ogg") {
                            }
                            else {
                                toaster.pop("error", "", "Allow FileType are .wma, .mp3, .wav, .ra, .ra, .ram, .rm, .mid, .ogg");
                                return;
                            }
                            // End ResumeFileType Validation 
                            if (!item.files)
                                item.files = [];
                            var fileResult = {
                                name: file.name
                            };

                            file.upload = resource.uploadFile(uploader, file, function () {
                            }, reject);
                            counterFilesPromise++;
                            file.upload.then(function (response) {
                                counterFilesPromise--;
                                fileResult.S3BucketName = response.data.fileName;
                            }, function (response) {
                                if (response.status > 0) {
                                    toaster.pop("error", "", "file upload error");
                                    counterFilesPromise--;
                                }
                            });
                            item.files.push(fileResult);
                        }
                    }
                ]

            };

        }


        self.getNA = function (fieldName, propertyName) {
            if (propertyName) {
                return (self.model[fieldName] && self.model[fieldName][propertyName]) || "-";
            }
            return self.model[fieldName] || "-";
        }

        self.getSummaryFieldValue = function (field) {
            var value = "";
            if (typeof self.model[field.id] === "object" && self.model[field.id] && self.model[field.id].hasOwnProperty("name")) {
                value = self.model[field.id].name;
            } else {
                value = self.model[field.id];
            }
            if (!value) {
                return value;
            }

            var actualField = _.find(self.generalPoropertyList, { id: field.id });
            if (actualField && actualField.type) {
                switch (actualField.type) {
                    case "DATE_TIME":
                        value = self.dateFormat(new Date(value));
                        break;
                    case "PHONE":
                        if (value && value.length > 10)
                            value = value.substring(0, 10);

                        if (value && value.length > 6)
                            value = ["(", value.slice(0, 3), ") ", value.slice(3, 6), "-", value.slice(6)].join('');
                        break;
                    case "CHECKBOX":
                        if (field.id == "active") {
                            if (value == true || value == "true") {
                                value = "Yes";
                            } else {
                                value = "No";
                            }
                        } else {
                            if (value == true || value == "true") {
                                value = "True";
                            } else {
                                value = "False";
                            }
                        }
                        break;
                    default:
                        value = value;
                        break;
                }
            }
            return value;
        }

        self.getResourcePracticeAreasValueForSummary = function (field) {

            if (self.model.resourcePracticeAreas && self.model.resourcePracticeAreas.length > 0) {
                var str = "";
                for (var index = 0; (index < 2 && self.model.resourcePracticeAreas.length > 1) ; index++) {
                    str += self.model.resourcePracticeAreas[index].name + ", ";
                }
                str = (self.model.resourcePracticeAreas.length > 1) ? str.substring(0, (str.length - 2)) + "..." : str.substring(0, (str.length - 2));
                return (str.length > 32) ? str.substring(0, 32) + "..." : str;
            }
            return "";
        }

        self.getDivisionValueForSummary = function (field) {
            if (self.model.divisions && self.model.divisions.length > 0) {
                var str = "";
                for (var index = 0; (index < 2 && self.model.divisions.length > 1) ; index++) {
                    str += self.model.divisions[index].name + ", ";
                }
                str = (self.model.divisions.length > 1) ? str.substring(0, (str.length - 2)) + "..." : str.substring(0, (str.length - 2));
                return (str.length > 32) ? str.substring(0, 32) + "..." : str;
            }
            return "";
        }
        self.getIndustryValueForSummary = function (field) {
            if (self.model.industry && self.model.industry.length > 0) {
                var str = "";
                for (var index = 0; (index < 2 && self.model.industry.length > 1) ; index++) {
                    str += self.model.industry[index].name + ", ";
                }
                str = (self.model.industry.length > 1) ? str.substring(0, (str.length - 2)) + "..." : str.substring(0, (str.length - 2));
                return (str.length > 32) ? str.substring(0, 32) + "..." : str;
            }
            return "";
        }

        self.getSliderClass = function (fieldName) {
            var value = self.model[fieldName];
            if (!value)
                return "0%";
            return (value * 10) + "%";
        }

        function availabilityCondition() {
            return !self.projectId;
        }

        /*  Fix for IRMS5 to convert only year to full date for Certificate and Educations. */
        function modifyEducationsForIRMS5(tempModel) {
            _.forEach(tempModel.educations, function (edu) {
                if (edu.dateObtained && !(edu.dateObtained.length > 4) && typeof edu.dateObtained === 'string') {
                    edu.dateObtained = new Date("01-05-" + edu.dateObtained);// moment("01-01-" + edu.dateObtained);
                }
            });
        }
        function modifyCertificatesForIRMS5(tempModel) {
            _.forEach(tempModel.certificates, function (edu) {
                if (edu.issueDate && !(edu.issueDate.length > 4) && typeof edu.issueDate === 'string') {
                    edu.issueDate = new Date("01-05-" + edu.issueDate);//  moment("01-01-" + edu.issueDate);
                }
            });
        }

        function modifySkillsForNullValue() {
            var skills = self.model.skills;
            _.forEach(skills, function (skill) {
                if (!skill.yearsOfExpirience || skill.yearsOfExpirience == null)
                    skill.yearsOfExpirience = 0;
                if (!skill.proficiency || skill.proficiency == null)
                    skill.proficiency = 0;
            });
        }

        function modifyLanguagesForNullValue() {
            var spokenLanguages = self.model.spokenLanguages;
            _.forEach(spokenLanguages, function (language) {
                if (!language.yearsOfExpirience || language.yearsOfExpirience == null)
                    language.yearsOfExpirience = 0;
                if (!language.speakingProficiency || language.speakingProficiency == null)
                    language.speakingProficiency = 0;
                if (!language.writingProficiency || language.writingProficiency == null)
                    language.writingProficiency = 0;

            });
        }

        function preSendChangeModel() {
            if (self.model.zipCode === '' && self.model.zipCode.length == 0) {
                self.model.zipCode = null;
            }
            calculateYearsOfExperience();


            modifySkillsForNullValue();
            modifyLanguagesForNullValue();

        }


        function base64ToBlob(base64Data, contentType) {
            contentType = contentType || '';
            var sliceSize = 1024;
            var byteCharacters = atob(base64Data.split(',')[1]);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            return new Blob(byteArrays, { type: contentType });
        }

        self.setFormScope = setFormScope;
        creatorLeavePage($scope, getForm);

        $rootScope.$on("custom-file-upload-start", function () {
            counterFilesPromise++;
        });
        $rootScope.$on("custom-file-upload-end", function () {
            counterFilesPromise--;
        });



        /*
            Resume Functionality
        */
        $scope.showGenerateResumeScreen = false;
        $scope.showCheckBox = false;

        $scope.showHideGenerateResumeScreen = function () {
            $scope.showGenerateResumeScreen = !$scope.showGenerateResumeScreen;
            $scope.showCheckBox = true;
            systemFunctionsService.scrollToTopZero();
            self.resumeFieldList = {};
        };
        $scope.resumeFieldList = [];
        $scope.removeFieldFromResume = function (fieldId) {
            if ($scope.resumeFieldList.indexOf(fieldId) == -1) {
                $scope.resumeFieldList.push(fieldId);
                return;
            }
            if ($scope.resumeFieldList.indexOf(fieldId) > -1) {
                $scope.resumeFieldList.splice($scope.resumeFieldList.indexOf(fieldId), 1);
                return;
            }
        };

        $scope.toShowField = function (fieldId) {
            if ($scope.resumeFieldList.indexOf(fieldId) > -1) {
                return false;
            }
        }
        $scope.showCanvas = false;

        $scope.clearData = function (sourceArray, selectedArray) {
            if (!selectedArray) {
                selectedArray = [];
            }
            var removeEduArray = [];
            for (var edIndex = 0; edIndex < sourceArray.length; edIndex++) {
                var isExist = selectedArray[edIndex];
                if (!isExist) {
                    removeEduArray.push(edIndex);
                }
            }
            for (var removeItem = 0; removeItem < removeEduArray.length; removeItem++) {
                sourceArray[removeEduArray[removeItem]] = null;
            }
        }


        $scope.addIdListForFieldArrayInForm = function (sourceArray, valueFieldName, fieldName, form) {
            for (var edIndex = 0; edIndex < sourceArray.length; edIndex++) {
                var item = sourceArray[edIndex];
                if (!item) continue;

                var value = item[valueFieldName];
                if (!value) continue;

                var element = document.createElement("input");
                element.name = fieldName + "[]";
                element.value = value;

                form.appendChild(element);
            }
        }


        $scope.previewResume = function (firstName, lastName) {
            if ($scope.showGenerateResumeScreen && $scope.showCheckBox) { // Preview Call
                $scope.showCheckBox = false;
                $scope.showCanvas = true;

            } else if ($scope.showGenerateResumeScreen && !$scope.showCheckBox) { // Generate Resume and Downlaod File.

                var localModel = angular.copy(self.model);

                $scope.clearData(localModel.educations, localModel.educations);
                $scope.clearData(localModel.skills, localModel.skills);
                $scope.clearData(localModel.certificates, localModel.certificates);
                $scope.clearData(localModel.previousEmployments, localModel.previousEmployments);
                $scope.clearData(localModel.resourceWorkExperiences, localModel.resourceWorkExperiences);
                $scope.clearData(localModel.spokenLanguages, localModel.spokenLanguages);
                $scope.clearData(localModel.resourceProfessionalOrganizations, localModel.resourceProfessionalOrganizations);

                //Prepare form to sumbit to server to generate the resume
                var previousForm = document.getElementById("resume_export_form");
                if (previousForm) previousForm.remove();
                var form = document.createElement("form");
                form.method = "POST";
                form.id = "resume_export_form"
                form.name = "resume_export_form"
                form.target = "_blank";
                form.action = "api/Resources/export_resume_download";
                form.style.display = "none";

                form.enctype = "application/x-www-form-urlencoded";

                var element1 = document.createElement("input");
                element1.name = "id";
                element1.value = self.model.id;
                form.appendChild(element1);


                var element2 = document.createElement("input");
                element2.name = "experienceData";
                element2.value = (self.model.strCertificateExperience && self.model.strCertificateExperience.length > 0) ? self.model.strCertificateExperience : "";
                form.appendChild(element2);

                var element3 = document.createElement("input");
                element3.name = "yearExperienceData";
                var str = self.getEmployedFromString();
                if (str && str.length > 0)
                    element3.value = str;
                else
                    element3.value = "";
                form.appendChild(element3);

                var element4 = document.createElement("input");
                element4.name = "isForStinson";
                element4.value = self.IS_BUILD_FOR_STINSON;
                form.appendChild(element4);


                //Generate INput field array for each selected items into resume generate
                //                                  (sourceArray, valueFieldName, fieldName, form)
                $scope.addIdListForFieldArrayInForm(localModel.educations, "id", "educations", form);
                $scope.addIdListForFieldArrayInForm(localModel.skills, "id", "skills", form);
                $scope.addIdListForFieldArrayInForm(localModel.certificates, "id", "certificates", form);
                $scope.addIdListForFieldArrayInForm(localModel.previousEmployments, "id", "previousEmployments", form);
                $scope.addIdListForFieldArrayInForm(localModel.resourceWorkExperiences, "id", "resourceWorkExperiences", form);
                $scope.addIdListForFieldArrayInForm(localModel.spokenLanguages, "id", "spokenLanguages", form);
                $scope.addIdListForFieldArrayInForm(localModel.resourceProfessionalOrganizations, "id", "professionalOrganizations", form);

                document.body.appendChild(form);

                form.submit();
            }
            systemFunctionsService.scrollToTopZero();
        };

        /*
            Resume Functionality ---------
        */
    }

})();
