-- setting to null resource state fields
update r set state_id = null
from resources r
where state_id is not null 

-- setting to null patient_addresss state fields
update pa set state_id = null
from patient_addresss pa
where state_id is not null

-- setting to null patient_parents_guardians state fields
update ppg set state_id = null
from patient_parents_guardians ppg
where state_id is not null

-- setting to null referral_physicians state fields
update ref set state_id = null
from referral_physicians ref
where state_id is not null

-- setting to null resource_travels states fields
update rt set certificate_state_id = null
from resource_certificates rt
where certificate_state_id is not null

-- deleting old states values
delete from states

-- reset index from 1
DBCC CHECKIDENT ('[states]', RESEED, 0);

-- adding new values to states
SET IDENTITY_INSERT [dbo].[states] ON 

GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (1, N'Alaska', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'AK', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (2, N'Wisconsin', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'WI', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (3, N'Pennsylvania', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'PA', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (4, N'South Dakota', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'SD', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (5, N'Virginia', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'VA', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (6, N'Nebraska', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'NE', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (7, N'Vermont', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'VT', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (8, N'Michigan', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'MI', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (9, N'Oklahoma', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'OK', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (10, N'Missouri', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'MO', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (11, N'Illinois', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'IL', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (12, N'Kansas', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'KS', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (13, N'New Hampshire', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'NH', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (14, N'Arizona', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'AZ', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (15, N'North Dakota', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'ND', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (16, N'Oregon', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'OR', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (17, N'Florida', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'FL', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (18, N'Indiana', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'IN', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (19, N'Colorado', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'CO', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (20, N'North Carolina', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'NC', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (21, N'Texas', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'TX', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (22, N'Massachusetts', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'MA', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (23, N'Nevada', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'NV', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (24, N'District of Columbia', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'DC', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (25, N'Connecticut', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'CT', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (26, N'Georgia', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'GA', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (27, N'Maryland', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'MD', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (28, N'Delaware', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'DE', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (29, N'West Virginia', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'WV', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (30, N'New York', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'NY', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (31, N'New Jersey', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'NJ', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (32, N'Iowa', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'IA', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (33, N'Ohio', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'OH', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (34, N'Montana', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'MT', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (35, N'Hawaii', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'HI', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (36, N'Alabama', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'AL', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (37, N'Arkansas', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'AR', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (38, N'California', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'CA', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (39, N'Wyoming', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'WY', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (40, N'Minnesota', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'MN', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (41, N'South Carolina', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'SC', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (42, N'Rhode Island', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'RI', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (43, N'Washington', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'WA', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (44, N'Maine', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'ME', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (45, N'Mississippi', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'MS', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (46, N'New Mexico', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'NM', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (47, N'Louisiana', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'LA', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (48, N'Tennessee', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'TN', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (49, N'Idaho', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'ID', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (50, N'Kentucky', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'KY', 223)
GO
INSERT [dbo].[states] ([Id], [name], [created], [modified], [state_code], [country_id]) VALUES (51, N'Utah', CAST(N'2017-05-04 07:02:16.780' AS DateTime), CAST(N'2017-05-04 07:02:16.780' AS DateTime), N'UT', 223)
GO
SET IDENTITY_INSERT [dbo].[states] OFF
GO
