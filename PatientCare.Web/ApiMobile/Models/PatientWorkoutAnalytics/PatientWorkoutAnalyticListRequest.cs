﻿using PatientCare.Web.Api.Models;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.PatientWorkoutAnalytics
{
    public class PatientWorkoutAnalyticListRequest : ListRequest
    {
        [DataMember(Name = "from")]
        public DateTime? From { get; set; }

        [DataMember(Name = "to")]
        public DateTime? To { get; set; }

        [DataMember(Name = "caseid")]
        public long? CaseId { get; set; }

        [DataMember(Name = "patientid")]
        public long? PatientId { get; set; }

    }
}