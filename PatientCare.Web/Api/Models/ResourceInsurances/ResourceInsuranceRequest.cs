﻿namespace PatientCare.Web.Api.Models.ResourceInsurances
{
    public class ResourceInsuranceRequest
    {
        public long Id { get; set; }

        public long? ResourceId { get; set; }

        public long? InsuranceTypeId { get; set; }

        public string Participating { get; set; }
        public string EmployeeCost { get; set; }
        public string EmployerContribution { get; set; }
    }
}