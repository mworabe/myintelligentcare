namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addexercise : DbMigration
    {
        public override void Up()
        {

            AddColumn("dbo.exercises", "time", c => c.Int(nullable: false));
            AddColumn("dbo.exercises", "timeUnit", c => c.String());
            AddColumn("dbo.exercises", "resistance", c => c.String());
            AddColumn("dbo.hep_detail", "name", c => c.String());
            AddColumn("dbo.hep_detail", "frequency", c => c.String());
            AddColumn("dbo.hep_detail", "resistance", c => c.String());
            AddColumn("dbo.hep_detail", "others", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.hep_detail", "others");
            DropColumn("dbo.hep_detail", "resistance");
            DropColumn("dbo.hep_detail", "frequency");
            DropColumn("dbo.hep_detail", "name");
            DropColumn("dbo.exercises", "resistance");
            DropColumn("dbo.exercises", "timeUnit");
            DropColumn("dbo.exercises", "time");
        }
    }
}
