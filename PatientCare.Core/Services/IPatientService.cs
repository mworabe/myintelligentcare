﻿using System.Threading.Tasks;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Services
{
    public interface IPatientService
    {
        #region patients
        Task<bool> UpdatePatientAsync(Patient patient);
        Task<bool> CreatePatientAsync(Patient patient);
        Task<Patient> ReadPatientAsync(long id);
        Task<bool> DeletePatientAsync(Patient patient);
        Task<bool> UpdatePatientNoAsync(long patientId);
        #endregion

        #region Patients Case
        Task<bool> UpdatePatientCaseAsync(PatientCase patientCase);
        Task<bool> CreatePatientCaseAsync(PatientCase patientCase);
        Task<PatientCase> ReadPatientCaseAsync(long id);
        Task<bool> DeletePatientCaseAsync(long id);
        //Task<string> DeletePatientCaseAsync(PatientCase patientCase);
        Task<bool> UpdatePatientCaseNoAsync(long? patientId, long? caseId);
        #endregion

        #region Appointment Type

        Task<bool> UpdateApointmentTypeAsync(AppointmentType appointmenttype);
        Task<bool> CreateApointmentTypeAsync(AppointmentType appointmenttype);
        Task<AppointmentType> ReadApointmentTypeAsync(long id);
        Task<bool> DeleteApointmentTypeAsync(long id);

        #endregion

        #region Patient Appointment

        Task<bool> UpdatePatientApointmentAsync(PatientAppointment patientappointment);
        Task<bool> CreatePatientApointmentAsync(PatientAppointment patientappointment);
        Task<PatientAppointment> ReadPatientApointmentAsync(long id);
        Task<bool> DeletePatientApointmentAsync(long id);

        #endregion

        #region Patient Workout AnalyticRepository 

        Task<bool> InsertUpdatePatientWorkoutAnalyticAsync(PatientWorkoutAnalytic patientWorkoutAnalytic);
        Task<PatientWorkoutAnalytic> ReadPatientWorkoutAnalyticAsync(long id);

        #endregion
    }
}
