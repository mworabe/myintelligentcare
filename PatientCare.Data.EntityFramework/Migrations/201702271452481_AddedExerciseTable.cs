namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedExerciseTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.exercises",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    sets = c.Int(),
                    keywords = c.String(),
                    comments = c.String(),
                    ExerciseImage = c.String(),
                    ExerciseVideo = c.String(),
                    name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropTable("dbo.exercises");
        }
    }
}
