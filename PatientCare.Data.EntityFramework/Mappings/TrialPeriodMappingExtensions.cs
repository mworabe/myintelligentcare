﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class TrialPeriodMappingExtensions
    {
        public static DbModelBuilder MapTrialPeriod(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<TrialPeriod>().ToTable("trial_period").HasKey(i => i.Id);

            modelBuilder.Entity<TrialPeriod>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<TrialPeriod>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<TrialPeriod>()
               .Property(p => p.TrialPeriodDate)
               .HasColumnName("trial_period_date")
               .IsOptional();
            
            return modelBuilder;
        }
    }
}
