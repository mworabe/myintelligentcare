﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface ILeaveRequestRepository : IRepository<LeaveRequest>
    {
    }
}
