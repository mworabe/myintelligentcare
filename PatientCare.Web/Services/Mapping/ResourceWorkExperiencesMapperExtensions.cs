﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceWorkExperience;
using EmitMapper;
using EmitMapper.MappingConfiguration;
using PatientCare.Web.Api.Models.ResourceWorkAssignment;
using System.Linq;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceWorkExperiencesMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceWorkExperiencesRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");
            
            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkExperience, ResourceWorkExperienceResponse>
                (new ExtendedMapConfig<ResourceWorkExperience, ResourceWorkExperienceResponse>()));

            var newResResourceWorkAssignmentMapper =
               ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkAssignmentResponse, ResourceWorkAssignment>(
                   new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
             ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkExperienceResponse, ResourceWorkExperience>(new ExtendedMapConfig<ResourceWorkExperienceResponse, ResourceWorkExperience>()
                .ForMember(x => x.ResourceWorkAssignments, y => y.ResourceWorkAssignments != null ? y.ResourceWorkAssignments.Select(i => newResResourceWorkAssignmentMapper.Map(i)).ToList() : null)                
             ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkExperience[], ResourceWorkExperienceResponse[]>());
            
            var resourceWorkAssignementMapper =
              ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkAssignmentRequest, ResourceWorkAssignment>(
                   new DefaultMapConfig());
                        
            mapperProvider.RegisterMapper(() =>
             ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkExperienceRequest, ResourceWorkExperience>
                (new ExtendedMapConfig<ResourceWorkExperienceRequest, ResourceWorkExperience>()
                .ForMember(x => x.ResourceWorkAssignments, y => y.ResourceWorkAssignments != null ? y.ResourceWorkAssignments.Select(i => resourceWorkAssignementMapper.Map(i)).ToList() : null)
                ));
                        
            return mapperProvider;
        }
    }
}