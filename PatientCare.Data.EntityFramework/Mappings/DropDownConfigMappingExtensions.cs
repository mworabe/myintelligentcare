﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class DropDownConfigMappingExtensions
    {
        public static DbModelBuilder MapDropDownConfig(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<DropDownConfig>().ToTable("drop_down_configs").HasKey(i => i.Id);

            modelBuilder.Entity<DropDownConfig>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<DropDownConfig>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<DropDownConfig>()
               .Property(p => p.Value)
               .HasColumnName("value")
               .HasMaxLength(255)
               .IsOptional();

            modelBuilder.Entity<DropDownConfig>()
               .Property(p => p.Name)
               .HasColumnName("display_name")
               .HasMaxLength(255)
               .IsOptional();

            modelBuilder.Entity<DropDownConfig>()
                .Property(p => p.DropDownType)
                .HasColumnName("drop_down_type")
                .HasMaxLength(255)
                .IsOptional();

            modelBuilder.Entity<DropDownConfig>()
               .Property(p => p.ConfigField1)
               .HasColumnName("config_field_1")
               .IsOptional();

            modelBuilder.Entity<DropDownConfig>()
               .Property(p => p.ConfigField2)
               .HasColumnName("config_field_2")
               .IsOptional();

            modelBuilder.Entity<DropDownConfig>()
              .Property(p => p.ConfigField3)
              .HasColumnName("config_field_3")
              .IsOptional();

            modelBuilder.Entity<DropDownConfig>()
               .Property(p => p.ConfigField4)
               .HasColumnName("config_field_4")
               .IsOptional();

            modelBuilder.Entity<DropDownConfig>()
              .Property(p => p.ConfigField5)
              .HasColumnName("config_field_5")
              .IsOptional();

            modelBuilder.Entity<DropDownConfig>()
           .HasMany<Exercise_injury>(p => p.exerciseinjuries)
           .WithOptional()
           .HasForeignKey(p => p.injury_Id);


            return modelBuilder;
        }
    }
}
