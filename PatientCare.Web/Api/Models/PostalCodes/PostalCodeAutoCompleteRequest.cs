﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PostalCodes
{
    public class PostalCodeAutoCompleteRequest
    {
        [DataMember(Name = "search")]
        public string Search { get; set; }

        [DataMember(Name = "countryId")]
        public long? CountryId { get; set; }

        [DataMember(Name = "stateId")]
        public long? StateId { get; set; }

        [DataMember(Name = "cityId")]
        public long? CityId { get; set; }
    }
}