﻿using System.Collections.Generic;
using PatientCare.Core.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PatientCare.Data.EntityFramework.Identity
{
    public class ApplicationRole : IdentityRole
    {

        public ApplicationRole()
        {
            IsActive = false;
        }

        public ApplicationRole(string name)
            : base(name)
        {
            IsActive = false;
        }

        public string JsonSetup { get; set; }

        public string Description { get; set; }

        public bool? IsActive { get; set; }

        public long? DashboardSetupId { get; set; }

        public virtual DashboardSetup DashboardSetup { get; set; }

        public virtual ICollection<RoleConfigureLayout> RoleConfigureLayouts { get; set; }

    }
}
