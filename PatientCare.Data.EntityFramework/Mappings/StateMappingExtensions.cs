﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class StateMappingExtensions
    {
        public static DbModelBuilder MapState(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<State>().ToTable("states")
                .HasKey(i => i.Id);

            modelBuilder.Entity<State>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            modelBuilder.Entity<State>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<State>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<State>()
                .Property(p => p.StateCode)
                .HasColumnName("state_code")
                .IsOptional();

            modelBuilder.Entity<State>()
                .Property(p => p.CountryId)
                .HasColumnName("country_id")
                .IsOptional();

            return modelBuilder;
        }
    }
}