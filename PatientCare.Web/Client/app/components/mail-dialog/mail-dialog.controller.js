﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("MailDialogController", [
		"$scope",
		'toaster',
        'dataItem',
        '$uibModalInstance',
		function ($scope, toaster, dataItem, $uibModalInstance) {

		    $scope.mailItem = dataItem;
		    $scope.title = dataItem.title;


			$scope.close = function () {
				$uibModalInstance.dismiss('');
			}

			$scope.sendBtn = function () {
			    toaster.pop("Success", "", "Mail send successfully.")
			    $uibModalInstance.dismiss('');
			}







		}
    ]);