﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceSkillRepository : IRepository<ResourceSkill>
    {
    }
}
