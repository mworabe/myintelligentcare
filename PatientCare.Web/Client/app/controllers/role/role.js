﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
angular.module("EDZoutstaffingPortalApp")
    .config(function($stateProvider) {
        $stateProvider
            .state("role", {
                url: "/role",
                templateUrl: "Client/app/controllers/role/role.html",
                controller: "RoleCtrl as RoleCtrl",
                data: {
                    security: {
                        authenticated: true
                    }
                }
            })
            .state("roleEdit", {
                url: "/role/edit/:id",
                templateUrl: "Client/app/controllers/role/roleEditV2.html",
                controller: "RoleEditCtrl as RoleEditCtrl",
                data: {
                    security: {
                        authenticated: true
                    }
                }
            })
            .state("configLayout", {
                url: "/role/config-layout/:id",
                templateUrl: "Client/app/controllers/role/configLayoutV2.html",
                controller: "ConfigLayoutCtrl as ConfigLayoutCtrl",
                resolve: {
                    loadedInitialData: [
                        "$q", "$stateParams", "RoleSetup", function($q, $stateParams, roleSetupService) {
                            return $q(function loadRole(resolve) {
                                if (!$stateParams.id) {
                                    resolve();
                                } else {
                                    roleSetupService.get({ id: $stateParams.id }).$promise.then(function returnProject(response) {
                                        resolve(response);
                                    });
                                }
                            });

                        }
                    ]
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });