﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Industry.
    /// </summary>
    public class ResourceIndustry : Entity
    {
        public long? ResourceId { get; set; }
        public virtual Resource Resource { get; set; }

        public long? IndustryId { get; set; }
        public virtual DropDownConfig Industry { get; set; }

        // for the Display we are added field and not created in Table
        public string Name { get { return Industry != null ? Industry.Name : string.Empty; } }
        public string DropDownType { get { return Industry != null ? Industry.DropDownType : string.Empty; } }
        public string ConfigField1 { get { return Industry != null ? Industry.ConfigField1 : string.Empty; } }
    }
}
