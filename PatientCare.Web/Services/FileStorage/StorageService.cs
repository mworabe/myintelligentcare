﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace PatientCare.Web.Services.FileStorage
{
    public class StorageService
    {
        private readonly IAmazonS3 Client;
        private static readonly string AwsBucket = ConfigurationManager.AppSettings["AWSBucket"];
        private static readonly string LocalBucket = ConfigurationManager.AppSettings["LocalBucket"];
        public static readonly bool UseAmazonStorage = Convert.ToBoolean(ConfigurationManager.AppSettings["useAmazonStorage"] ?? "true");
        public static readonly int AWSFileExpiresInMinit = Convert.ToInt32(ConfigurationManager.AppSettings["awsFileExpiresInMinit"] ?? "1440");
        public static readonly int FrameStartInSecond = Convert.ToInt32(ConfigurationManager.AppSettings["frameStartInSecond"] ?? "6");

        public static readonly string GetProjectMediaDownload = "api/Medias/exerciseMedia?mediaId=";

        public StorageService()
        {
            var accessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
            var secretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
            var awsRegion = ConfigurationManager.AppSettings["AWSRegion"];

            if (Client == null)
            {
                Client = AWSClientFactory.CreateAmazonS3Client(accessKey, secretKey, RegionEndpoint.GetBySystemName(awsRegion));
            }
        }

        private string BuildBucket(string folderPath)
        {
            if (UseAmazonStorage)
            {
                return string.IsNullOrWhiteSpace(folderPath) ? AwsBucket : string.Format(@"{0}/{1}", AwsBucket, folderPath);
            }
            return string.IsNullOrWhiteSpace(folderPath) ? LocalBucket : string.Format(@"{0}\{1}", LocalBucket, folderPath);
        }

        public async Task<bool> UploadFileAsync(string fileName, Stream stream)
        {
            return await UploadFileAsync(fileName, string.Empty, stream);
        }

        public async Task<bool> UploadFileAsync(string fileName, string directoryPath, Stream stream)
        {
            if (UseAmazonStorage)
            {
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = stream,
                    BucketName = BuildBucket(directoryPath),
                    CannedACL = S3CannedACL.PublicRead,
                    Key = fileName
                };
                var fileTransferUtility = new TransferUtility(Client);

                await fileTransferUtility.UploadAsync(uploadRequest);
                return true;
            }
            directoryPath = BuildBucket(directoryPath);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            using (var file = File.Create(string.Format("{0}/{1}", directoryPath, fileName)))
            {
                stream.Seek(0, SeekOrigin.Begin);
                await stream.CopyToAsync(file);
                return true;
            }
        }

        public async Task<bool> CopyFileAsync(string fileName, string folderPath, string destFileName, string destFolderPath)
        {
            if (UseAmazonStorage)
            {
                var request = new CopyObjectRequest
                {
                    SourceBucket = BuildBucket(folderPath),
                    SourceKey = fileName,
                    DestinationBucket = BuildBucket(destFolderPath),
                    DestinationKey = destFileName,
                    CannedACL = S3CannedACL.PublicRead
                };
                await Client.CopyObjectAsync(request);
                return true;
            }
            destFolderPath = BuildBucket(destFolderPath);
            if (!Directory.Exists(destFolderPath))
            {
                Directory.CreateDirectory(destFolderPath);
            }
            var source = string.Format("{0}/{1}", folderPath, fileName);
            var destination = string.Format("{0}/{1}", destFolderPath, destFileName);
            File.Copy(BuildBucket(source), destination);
            return true;
        }

        public async Task<bool> DeleteObjects(string[] files)
        {
            if (files == null || !files.Any())
                return false;
            if (UseAmazonStorage)
            {
                var keys = files.Select(file => new KeyVersion
                {
                    Key = file,
                    VersionId = null
                }).ToList();
                var request = new DeleteObjectsRequest
                {
                    BucketName = AwsBucket,
                    Objects = keys
                };

                await Client.DeleteObjectsAsync(request);
                return true;
            }
            foreach (var file in files)
            {
                File.Delete(file);
            }
            return true;
        }

        public string GetStorageUrl(string fileName, string directoryPath)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }
            if (UseAmazonStorage)
            {
                directoryPath = string.IsNullOrEmpty(directoryPath) ? AwsBucket : BuildBucket(directoryPath);
                var request = new GetPreSignedUrlRequest
                {
                    BucketName = directoryPath,
                    Key = fileName,
                    Expires = DateTime.Now.AddMinutes(AWSFileExpiresInMinit)
                };
                return Client.GetPreSignedURL(request);
            }
            return string.Format("FileStorage{0}/{1}", directoryPath, fileName);
        }

        public string GetStorageMediaUrlPhoto(string fileName, string directoryPath)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }
            if (UseAmazonStorage)
            {
                directoryPath = string.IsNullOrEmpty(directoryPath) ? AwsBucket : BuildBucket(directoryPath);
                var request = new GetPreSignedUrlRequest
                {
                    BucketName = directoryPath,
                    Key = fileName,
                    Expires = DateTime.Now.AddMinutes(AWSFileExpiresInMinit)
                };
                return Client.GetPreSignedURL(request);
            }
            else
            {
                directoryPath = string.IsNullOrEmpty(directoryPath) ? AwsBucket : BuildBucket(directoryPath);
                return string.Format("{0}/{1}", directoryPath, fileName);
            }
        }

        public string GetStorageUrlPhoto(string fileName, string directoryPath)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }
            if (UseAmazonStorage)
            {
                directoryPath = string.IsNullOrEmpty(directoryPath) ? AwsBucket : BuildBucket(directoryPath);
                var request = new GetPreSignedUrlRequest
                {
                    BucketName = directoryPath,
                    Key = fileName,
                    Expires = DateTime.Now.AddMinutes(AWSFileExpiresInMinit)
                };
                return Client.GetPreSignedURL(request);
            }
            else
            {
                directoryPath = string.IsNullOrEmpty(directoryPath) ? AwsBucket : BuildBucket(directoryPath);
            }
            return string.Format("{0}/{1}", directoryPath, fileName);
        }

        public void DeleteFolder(string directoryPath, bool recurcive = true)
        {
            if (UseAmazonStorage)
            {
                directoryPath = directoryPath.Replace("/", "\\");
                var directory = new S3DirectoryInfo(Client, AwsBucket, directoryPath);
                directory.Delete(recurcive);
            }
            else
            {
                directoryPath = BuildBucket(directoryPath);
                if (Directory.Exists(directoryPath))
                {
                    Directory.Delete(directoryPath, true);
                }
            }
        }

        public IEnumerable<S3DirectoryInfo> GetS3DirectoryInfos(string directoryPath)
        {
            directoryPath = directoryPath.Replace("/", "\\");
            var directory = new S3DirectoryInfo(Client, AwsBucket, directoryPath);
            return directory.GetDirectories();
        }

        public IEnumerable<string> GetDirectoryInfos(string directoryPath)
        {
            directoryPath = BuildBucket(directoryPath);
            if (Directory.Exists(directoryPath))
            {
                return Directory.GetDirectories(directoryPath);
            }
            return new List<string>();
        }

        public IList<string> GetFileNames(string directoryPath)
        {
            directoryPath = directoryPath.Replace("/", "\\");
            if (UseAmazonStorage)
            {
                var directoryInfo = new S3DirectoryInfo(Client, AwsBucket, directoryPath);
                return directoryInfo.GetFiles().Select(x => x.Name).ToArray();
            }
            directoryPath = BuildBucket(directoryPath);
            if (Directory.Exists(directoryPath))
            {
                return Directory.GetFiles(directoryPath);
            }
            return new List<string>();
        }
    }
}