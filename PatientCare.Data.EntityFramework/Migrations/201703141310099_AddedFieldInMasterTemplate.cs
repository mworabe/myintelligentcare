namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldInMasterTemplate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hep_master", "is_save_as_draft", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.hep_master", "is_save_as_draft");
        }
    }
}
