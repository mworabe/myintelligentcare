﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("LocationService", ["$resource", function ($resource) {
    var resource = $resource("api/ClinicLocations/:controller/:id",
        null, //parameters default
        {
            'query': { method: "GET", params: {}, isArray: false },
            'update': { method: "PUT", params: {}, isArray: false },
        });
    return resource;
}
]);
