﻿namespace PatientCare.Core.Mapping
{
    public interface IMapperProvider
    {
        IMapper<TS, TD> GetMapper<TS, TD>();
    }
}
