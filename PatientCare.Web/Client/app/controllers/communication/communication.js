
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("communication", {
                url: "/communication",
                templateUrl: "Client/app/controllers/communication/communication-view.html",
                controller: "CommunicationCtrl",
                data: {
                    requireLogin: true
                }

            })
            .state("communication.chat", {
                url: "/chat",
                data: {
                    requireLogin: true
                },
                params: { sessionObject: null },
                views: {
                    "chatView": {
                        templateUrl: 'Client/app/controllers/communication/chat/chat-window.html',
                        controller: "ChatWindowCtrl",
                    }
                }

            })

            .state("communication.chat.chat-box", {
                url: "/",
                data: {
                    requireLogin: true
                },
                params: { sessionObject: null },
                views: {
                    "chatBox": {
                        templateUrl: 'Client/app/controllers/communication/chat/chat-box.html',
                        controller: "ChatBoxCntrl",
                    }
                }

            })
            .state("communication.chat.contacts", {
                url: "/contacts/:view",
                onEnter: ['$state', '$uibModal',
                  function ($state, $uibModal) {
                      $uibModal.open({
                          templateUrl: "Client/app/controllers/communication/chat-contacts/chat-contacts.html",
                          controller: "ChatContactCntrl",
                          size: "md",
                          resolve: {
                              chatData: ["$q", "ChatService", '$state',
                                  function loadCaseInitialData($q, ChatService, $state) {
                                      function loadContacts() {
                                          var defer = $q.defer();
                                          ChatService.getChatContacts().$promise.then(function (success) {
                                              defer.resolve(success);
                                          }, function (error) {
                                              defer.resolve(error);
                                          });
                                          return defer.promise;
                                      }
                                      return $q.all([loadContacts()])
                                          .then(function (resolutions) {
                                              return resolutions;
                                          });

                                  }]
                          }

                      }).result.then(function (result) {
                          $state.go("communication.chat", { sessionObject: result }, { reload: "communication.chat" });
                      }, function () {
                          $state.go("communication.chat", {});
                      });
                  }
                ]

            })











    });