﻿using FluentValidation;

namespace PatientCare.Web.Api.Models.AppointmentTypes
{
    public class AppointmentTypesRequestValidator : AbstractValidator<AppointmentTypesRequest>
    {
        public AppointmentTypesRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);            
        }
    }
}