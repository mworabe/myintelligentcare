﻿
angular.module("EDZoutstaffingPortalApp")
.directive("organizationalChart", [
    function () {
        return {
            templateUrl: "Client/app/components/directives/organizationalChart/chart-view.html",
            restrict: "E",
            replace: false,
            scope: {
                dataContainer: '=data',
                panelTitle: '=ngPanelTitle'
            },
            link: function (scope, window) {
                var i = 0, duration = 750, rectW = 130, rectH = 30;
                var margin = { top: 20, right: 120, bottom: 20, left: 120 },


                width = 960 - margin.right - margin.left,
                height = 400 - margin.top - margin.bottom;

                scope.$on('loadOrganizationalChart', function (event, data) { scope.drawChart(data.chartData); });

                scope.drawChart = function (root) {
                    var zm;
                    d3.select("#organization_chart").select("svg").remove();
                    var svg = d3.select("#organization_chart").append("svg").attr("width", width).attr("height", height)
                                .call(zm = d3.behavior.zoom().scaleExtent([1, 3]).on("zoom", redraw)).append("g")
                                .attr("transform", "translate(" + 480 + "," + 20 + ")");

                    //svg.call(tip);


                    var tree = d3.layout.tree().nodeSize([140, 40]);
                    var diagonal = d3.svg.diagonal()
                        .projection(function (d) {
                            return [d.x + rectW / 2, d.y + rectH / 2];
                        });

                    //necessary so that zoom knows where to zoom and unzoom from
                    zm.translate([350, 20]);

                    root.x0 = 0;
                    root.y0 = height / 2;

                    function collapse(d) {
                        if (d.children) {
                            d._children = d.children;
                            d._children.forEach(collapse);
                            d.children = null;
                        }
                    }

                    root.children.forEach(collapse);
                    update(root);

                    function update(source) {
                        // Compute the new tree layout.
                        var nodes = tree.nodes(root).reverse(),
                            links = tree.links(nodes);

                        // Normalize for fixed-depth.
                        nodes.forEach(function (d) {
                            d.y = d.depth * 180;
                        });

                        // Update the nodes…
                        var node = svg.selectAll("g.node")
                            .data(nodes, function (d) {
                                return d.id || (d.id = ++i);
                            });

                        // Enter any new nodes at the parent's previous position.
                        var nodeEnter = node.enter().append("g")
                            .attr("class", "node")
                            .attr("transform", function (d) {
                                return "translate(" + source.x0 + "," + source.y0 + ")";
                            }).on("click", click);

                        nodeEnter.append("rect")
                            .attr("width", rectW)
                            .attr("height", rectH)
                            .attr("stroke", "black")
                            .attr("stroke-width", 1)
                            .style("fill", function (d) {
                                var color = (d._children && d._children.length > 0) ? "lightsteelblue" : "#fff";
                                return color;
                            });

                        nodeEnter.append("text")
                            .attr("x", rectW / 2)
                            .attr("y", (rectH / 2))
                            .attr("dy", ".35em")
                            .attr("text-anchor", "middle")
                            .text(function (d) {
                                //console.info(d._children);
                                return d.resourceFullName;// + " " + (d && d._children && d._children.length > 0) ? d._children.length : 0;
                            });

                        // Transition nodes to their new position.
                        var nodeUpdate = node.transition()
                            .duration(duration)
                            .attr("transform", function (d) {
                                return "translate(" + d.x + "," + d.y + ")";
                            });

                        nodeUpdate.select("rect")
                            .attr("width", rectW)
                            .attr("height", rectH)
                            .attr("stroke", "black")
                            .attr("stroke-width", 1)
                            .style("fill", function (d) {
                                var color = (d._children && d._children.length > 0) ? "lightsteelblue" : "#fff";
                                return color;
                            });

                        nodeUpdate.select("text")
                            .style("fill-opacity", 1);

                        // Transition exiting nodes to the parent's new position.
                        var nodeExit = node.exit().transition()
                            .duration(duration)
                            .attr("transform", function (d) {
                                return "translate(" + source.x + "," + source.y + ")";
                            })
                            .remove();

                        nodeExit.select("rect")
                                .attr("width", rectW)
                                .attr("height", rectH)
                                .attr("stroke", "black")
                                .attr("stroke-width", 1);

                        nodeExit.select("text");

                        // Update the links…
                        var link = svg.selectAll("path.link")
                            .data(links, function (d) {
                                return d.target.id;
                            });

                        // Enter any new links at the parent's previous position.
                        link.enter().insert("path", "g")
                            .attr("class", "link")
                            .attr("x", rectW / 2)
                            .attr("y", rectH / 2)
                            .attr("d", function (d) {
                                var o = { x: source.x0, y: source.y0 };
                                return diagonal({ source: o, target: o });
                            });

                        // Transition links to their new position.
                        link.transition()
                            .duration(duration)
                            .attr("d", diagonal);

                        // Transition exiting nodes to the parent's new position.
                        link.exit().transition()
                            .duration(duration)
                            .attr("d", function (d) {
                                var o = { x: source.x, y: source.y };
                                return diagonal({ source: o, target: o });
                            }).remove();

                        // Stash the old positions for transition.
                        nodes.forEach(function (d) { d.x0 = d.x; d.y0 = d.y; });
                    }

                    // Toggle children on click.
                    function click(d) {
                        if (d.children) {
                            d._children = d.children;
                            d.children = null;
                        } else {
                            d.children = d._children;
                            d._children = null;
                        }
                        update(d);
                    }

                    //Redraw for zoom
                    function redraw() {
                        svg.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
                        self.center = d3.event.translate;
                    }
                }
            }

        }
    }
]);