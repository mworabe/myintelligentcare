namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesClinicAndClinicLocationColumnINResourceTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.resources", "clinic_id", c => c.Long());
            AddColumn("dbo.resources", "clinic_location_id", c => c.Long());
            CreateIndex("dbo.resources", "clinic_id");
            CreateIndex("dbo.resources", "clinic_location_id");
            AddForeignKey("dbo.resources", "clinic_id", "dbo.clinics", "Id");
            AddForeignKey("dbo.resources", "clinic_location_id", "dbo.clinic_locations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.resources", "clinic_location_id", "dbo.clinic_locations");
            DropForeignKey("dbo.resources", "clinic_id", "dbo.clinics");
            DropIndex("dbo.resources", new[] { "clinic_location_id" });
            DropIndex("dbo.resources", new[] { "clinic_id" });
            DropColumn("dbo.resources", "clinic_location_id");
            DropColumn("dbo.resources", "clinic_id");
        }
    }
}
