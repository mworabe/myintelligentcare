﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.AppointmentTypes;
using EmitMapper;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Services.Mapping
{
    public static class AppointmentTypeMapperExtensions
    {
        public static EmitMapperMapperProvider AppointmentTypeRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<AppointmentType, AppointmentTypesResponse>
                    (new ExtendedMapConfig<AppointmentType, AppointmentTypesResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<AppointmentType[], AppointmentTypesResponse[]>());


            mapperProvider.RegisterMapper(() =>
               ObjectMapperManager.DefaultInstance.GetMapper<AppointmentType, PicklistDefaultResponse>
                   (new ExtendedMapConfig<AppointmentType, PicklistDefaultResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<AppointmentType[], PicklistDefaultResponse[]>());
            
            mapperProvider.RegisterMapper(() =>
              ObjectMapperManager.DefaultInstance.GetMapper<AppointmentType[], AppointmentTypesPicklistResponse[]>());

            mapperProvider.RegisterMapper(() =>
             ObjectMapperManager.DefaultInstance.GetMapper<AppointmentType[], AppointmentTypeAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<AppointmentTypesRequest, AppointmentType>
                    (new ExtendedMapConfig<AppointmentTypesRequest, AppointmentType>()));

            return mapperProvider;
        }
    }
}