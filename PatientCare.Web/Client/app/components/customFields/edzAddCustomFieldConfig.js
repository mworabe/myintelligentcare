"use strict";

(function edzAddCustomFieldConfigIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .constant("EDZ_CUSTOM_FIELD_TYPES", {
            TYPE_ALPHA: {
                name: "Alpha",
                label: "Alpha"
            },
            TYPE_ALPHA_NUMERIC: {
                name: "Alphanumeric",
                label: "Alphanumeric"
            },
            TYPE_NUMERIC: {
                name: "Numeric",
                label: "Numeric"
            },
            TYPE_SELECT: {
                name: "Select",
                label: "Select"
            },
            TYPE_DATE: {
                name: "Date",
                label: "Date"
            },
            TYPE_CHECKBOX: {
                name: "Checkbox",
                label: "Checkbox"
            },
            TYPE_BIGTEXT: {
                name: "Bigtext",
                label: "Text field(big text)"
            },
            TYPE_FILE: {
                name: "File",
                label: "File"
            },
            TYPE_TIME: {
                name: "Time",
                label: "Time"
            },
            TYPE_WEEKTIME: {
                name: "WeekTime",
                label: "Week Time"
            }
        });
})();