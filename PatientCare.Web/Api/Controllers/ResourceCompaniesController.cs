﻿using System;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Companies operations.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/ResourceCompanies")]
    public sealed class ResourceCompaniesController : EdzPicklistApiController<ResourceCompany>
    {
        /// <summary>
        /// Description : In this class we pass  Resource Companies IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="picklistRepository">Resource Company Entity</param>
        public ResourceCompaniesController(IPicklistRepository<ResourceCompany> picklistRepository)
            : base(picklistRepository)
        {
            GetPicklistStatusCodeErrorFunc = () => GetPicklistStatusCodeError(Picklist.ResourceCompanies);
        }

        protected override Uri GetPicklistLocation(long id)
        {
            return new Uri("/ResourceCompanies/" + id, UriKind.Relative);
        }
    }
}