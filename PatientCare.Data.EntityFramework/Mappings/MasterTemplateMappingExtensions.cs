﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class MasterTemplateMappingExtensions
    {
        public static DbModelBuilder MapMasterTemplate(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<MasterTemplate>().ToTable("master_templates")
                .HasKey(i => i.Id);

            modelBuilder.Entity<MasterTemplate>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<MasterTemplate>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<MasterTemplate>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();
            
            modelBuilder.Entity<MasterTemplate>()
                .Property(p => p.OwnerId)
                .HasColumnName("owner_id")
                .IsOptional();

            modelBuilder.Entity<MasterTemplate>()
                .HasOptional<Resource>(p => p.Owner)
                .WithMany()
                .HasForeignKey(p => p.OwnerId);

            modelBuilder.Entity<MasterTemplate>()
                .Property(p => p.IsMaster)
                .HasColumnName("is_master")
                .IsOptional();

            modelBuilder.Entity<MasterTemplate>()
                .Property(p => p.IsFavorite)
                .HasColumnName("is_favorite")
                .IsOptional();

            modelBuilder.Entity<MasterTemplate>()
                .Property(p => p.IsPublic)
                .HasColumnName("is_public")
                .IsOptional();

            modelBuilder.Entity<MasterTemplate>()
                .HasMany<MasterTemplateExercise>(p => p.TemplateExercises)
                .WithOptional()
                .HasForeignKey(item => item.MasterTemplateId);

            modelBuilder.Entity<MasterTemplate>()
                .Property(p => p.IsDeleted)
                .HasColumnName("is_deleted")
                .IsOptional();
            return modelBuilder;
        }
    }
}
