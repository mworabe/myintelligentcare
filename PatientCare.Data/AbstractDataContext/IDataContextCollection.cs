﻿namespace PatientCare.Data.AbstractDataContext
{
    public interface IDataContextCollection
    {
        T Get<T>() where T : class;
    }
}
