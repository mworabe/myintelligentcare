﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.UserLoginHistorys;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using PatientCare.Web.Api.Models;
using System.Linq.Expressions;
using System.Linq;
using Microsoft.AspNet.Identity;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using System.Text;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle User Login History operations.
    /// </summary>
    [Authorize]
    public sealed class UserLoginHistorysController : ApiBaseController
    {
        /// <summary>
        /// Description : User Login History ITimeManagementService.
        /// </summary>
        private readonly ITimeManagementService _timeManagementService;
        /// <summary>
        /// Description : User Login History IUserLoginHistoryRepository.
        /// </summary>
        private readonly IUserLoginHistoryRepository _userLoginHistoryRepository;
        /// <summary>
        /// Description : User Login History IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<UserLoginHistory> _readOptions = new ReadOptions<UserLoginHistory>();

        /// <summary>
        /// Description : Initializes a new instance of the User Login Historys class.  
        /// </summary>
        /// <param name="timeManagementService">ITimeManagement Service</param>
        /// <param name="userLoginHistoryRepository">IUser Login History Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public UserLoginHistorysController
            (ITimeManagementService timeManagementService,
             IUserLoginHistoryRepository userLoginHistoryRepository,
             IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _timeManagementService = timeManagementService;
            _userLoginHistoryRepository = userLoginHistoryRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/UserLoginHistory/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Create user Login History 
        /// </summary>
        /// <param name="history">User Login History Entity</param>
        /// <returns>User Login History Response Model</returns>
        [Authorize]
        [HttpPost]
        [Route("api/UserLoginHistorys/Logout")]
        [SwaggerResponse(201, "Created", typeof(UserLoginHistoryResponse))]
        public async Task<IHttpActionResult> LogoffUserLoginHistory(UserLoginHistory history)
        {
            try
            {
                var userId = User.Identity.GetUserId();
                var userName = User.Identity.Name;
                //var user = AppUserManager.Users.FirstOrDefault(x => x.Id == userId);

                UserLoginHistory userLoginHistoryRequest = new UserLoginHistory();
                userLoginHistoryRequest.Action = "Logout";
                userLoginHistoryRequest.UserId = history.UserId;
                userLoginHistoryRequest.LogDateTime = DateTime.Now;
                userLoginHistoryRequest.Username = history.Username;

                if (userId != history.UserId || userName != history.Username)
                    return Ok("Invalid Request : User Id and User Name does not match");

                await _timeManagementService.CreateuserLoginHistoryAsync(userLoginHistoryRequest);

                return Created(GetEntityLocation(userLoginHistoryRequest.Id), Mapper.Map<UserLoginHistory, UserLoginHistoryResponse>(userLoginHistoryRequest));
            }
            catch (Exception ex)
            {
                return Ok("Invalid Request : User Id and User Name does not match");
            }
        }

        /// <summary>
        /// Method Description : Using for Get All User Login 
        /// </summary>
        /// <param name="request">Use rLogin History List Request Model</param>
        /// <returns>User Login History Response Model</returns>
        [HttpGet]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<UserLoginHistoryResponse>))]
        public IHttpActionResult GetAllUserLogin([FromUri]UserLoginHistoryListRequest request)
        {
            var lst = new ListResponse<UserLoginHistoryResponse>();
            List<UserLoginHistoryResponse> data = new List<UserLoginHistoryResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                StringBuilder sb = new StringBuilder();
                sb.Append("select Id,user_name as Username,action,log_date_time as LogDateTime");
                sb.Append("\r\n");
                sb.Append("from user_login_history");
                sb.Append("\r\n");
                sb.Append("where 1 = 1");
                sb.Append("\r\n");
                if (request.SearchPhrase != null)
                {
                    sb.AppendFormat(" and ((user_name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                    sb.Append("\r\n");
                    sb.AppendFormat("	OR (FORMAT(log_date_time,'MM/dd/yyyy') LIKE '%{0}%' ESCAPE N'~'))", request.SearchPhrase);
                    sb.Append("\r\n");
                }
                if (string.IsNullOrEmpty(request.SortField))
                    sb.Append("ORDER BY log_date_time ");
                else
                    sb.Append("ORDER BY " + request.SortField);

                if (string.IsNullOrEmpty(request.SortField))
                    sb.Append(" DESC");
                else if (request.SortOrder.Value.ToString() == "Descending")
                    sb.Append(" DESC");
                else
                    sb.Append(" ASC");

                sb.Append("\r\n");

                data = ctx.Database.SqlQuery<UserLoginHistoryResponse>(sb.ToString()).ToList();
                lst.ItemsCount = data.Count;
                lst.Data = data.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToArray();

                // To Convert In Local Time Zone
                foreach (var item in lst.Data)
                {
                    item.LogDateTime = TimeZone.CurrentTimeZone.ToLocalTime(item.LogDateTime.Value);
                }
            }
            return Ok(lst);
        }

        private static Expression<Func<UserLoginHistory, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return loginuser => loginuser.UserId.Contains(searchPhrase)
                                || loginuser.Action.Contains(searchPhrase)
                                || loginuser.IPAddress.Contains(searchPhrase)
                                || loginuser.Username.Contains(searchPhrase)
                                || loginuser.SessionKey.Contains(searchPhrase);
        }

        private static Expression<Func<UserLoginHistory, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return loginuser => loginuser.UserId.Contains(searchPhrase)
                                || loginuser.Action.Contains(searchPhrase)
                                || loginuser.IPAddress.Contains(searchPhrase)
                                || loginuser.Username.Contains(searchPhrase)
                                || loginuser.SessionKey.Contains(searchPhrase);
        }
    }
}
