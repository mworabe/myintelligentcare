﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceCertificateMappingExtensions
    {
        public static DbModelBuilder MapResourceCertificate(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceCertificate>().ToTable("resource_certificates").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceCertificate>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceCertificate>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceCertificate>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsRequired();

            modelBuilder.Entity<ResourceCertificate>()
                .HasRequired<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceCertificate>()
                .Property(p => p.CertificateId)
                .HasColumnName("certificate_id")
                .IsRequired();

            modelBuilder.Entity<ResourceCertificate>()
                .HasRequired<Certificate>(p => p.Certificate)
                .WithMany()
                .HasForeignKey(p => p.CertificateId);

            modelBuilder.Entity<ResourceCertificate>()
                .Property(p => p.ExpirationDate)
                .HasColumnName("expirationDate")
                .IsOptional();

            modelBuilder.Entity<ResourceCertificate>()
                .Property(p => p.CertificateStateId)
                .HasColumnName("certificate_state_id")
                .IsOptional();

            modelBuilder.Entity<ResourceCertificate>()
                .HasOptional<State>(p => p.CertificateState)
                .WithMany()
                .HasForeignKey(p => p.CertificateStateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceCertificate>()
                .Property(p => p.IssueDate)
                .HasColumnName("issue_date")
                .IsOptional();

            return modelBuilder;
        }
    }
}
