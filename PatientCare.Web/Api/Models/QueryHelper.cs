﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace PatientCare.Web.Api.Models
{
    public class QueryHelper
    {
        public static bool PropertyExists<T>(string propertyName)
        {
            return typeof(T).GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance) != null;
        }

        public static Expression<Func<T, object>> GetPropertyExpression<T>(string propertyName)
        {
            if (propertyName == null) throw new ArgumentNullException("propertyName");
            ParameterExpression paramterExpression;
            UnaryExpression conversion;
            if (propertyName.Contains("."))
            {
                var props = propertyName.Split('.');
                if (!PropertyExists<T>(props[0]))
                {
                    return null;
                }
                paramterExpression = Expression.Parameter(typeof(T));
                var expr = props.Aggregate<string, Expression>(paramterExpression, Expression.PropertyOrField);
                conversion = Expression.Convert(expr, typeof(object));
            }
            else
            {
                if (!PropertyExists<T>(propertyName))
                {
                    return null;
                }
                paramterExpression = Expression.Parameter(typeof(T));
                conversion = Expression.Convert(Expression.PropertyOrField(paramterExpression, propertyName), typeof(object));
            }
            return Expression.Lambda<Func<T, object>>(conversion, paramterExpression);
        }
    }
}