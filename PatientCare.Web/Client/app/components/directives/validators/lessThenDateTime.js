﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngLessThanDateTime", [
        function () {
            return {
                restrict: "A",
                replace: false,
                require: "ngModel",
                link: function ($scope, $element, $attrs, ctrl) {
                    if (!$attrs.ngLessThanDateTime || $attrs.ngLessThanDateTimeField === "false")
                        return;
                    var newValue = null;

                    function compareDate(currValue, lessValue) {
                        return lessValue.isSame(currValue, "day") || lessValue.isAfter(currValue);
                    }

                    function setValidation(currValue, lessValue) {
                        if (!currValue || !lessValue || (currValue.isValid && !currValue.isValid()) || (lessValue.isValid && !lessValue.isValid()))
                            return;
                        ctrl.$setValidity("lessThenDateTime", compareDate(ctrl.$modelValue, newValue));
                    }

                    $scope.$watch($attrs.ngLessThanDateTime, function (newVal) {
                        if (newVal)
                            newValue = moment(newVal);
                        setValidation(ctrl.$modelValue, newValue);
                    });

                    $scope.$watch(function () {
                        return ctrl.$modelValue;
                    }, function (newVal) {
                        if (newVal) {
                            setValidation(ctrl.$modelValue, newValue);
                        } else
                            ctrl.$setValidity("lessThenDateTime", true);
                    });

                }
            };
        }
    ]);