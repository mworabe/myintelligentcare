﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceEducations;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceEducationMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceEducationRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceEducation, ResourceEducationResponse>(new ExtendedMapConfig<ResourceEducation, ResourceEducationResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceEducation[], ResourceEducationResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceEducationRequest, ResourceEducation>(new ExtendedMapConfig<ResourceEducationRequest, ResourceEducation>()));

            return mapperProvider;
        }
    }
}