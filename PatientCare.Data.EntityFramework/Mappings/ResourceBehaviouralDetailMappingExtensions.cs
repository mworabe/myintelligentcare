﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceBehaviouralDetailMappingExtensions
    {
        public static DbModelBuilder MapResourceBehaviouralDetail(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceBehaviouralDetail>().ToTable("resource_behavioural_details").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceBehaviouralDetail>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceBehaviouralDetail>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceBehaviouralDetail>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<ResourceBehaviouralDetail>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceBehaviouralDetail>()
                .Property(p => p.Behaviour)
                .HasColumnName("behaviour")
                .IsOptional();

            return modelBuilder;
        }
    }
}
