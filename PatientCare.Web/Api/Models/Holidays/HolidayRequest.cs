﻿using System;

namespace PatientCare.Web.Api.Models.Holidays
{
    public class HolidayRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long Location { get; set; }

        public DateTime? HolidayDate { get; set; }

        public string Description { get; set; }
        
        
    }
}