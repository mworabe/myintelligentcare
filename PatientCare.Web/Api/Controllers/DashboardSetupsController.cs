﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.DashboardSetups;
using Microsoft.AspNet.Identity;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using System.Text;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Dashboard Setup operations.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/DashboardSetups")]
    public sealed class DashboardSetupsController : ApiBaseController
    {
        //private readonly IDataContextScopeFactory _dataContextScopeFactory;
        //private readonly IPicklistRepository<DashboardSetup> _dashboardSetupRepository;

        //public DashboardSetupsController(IPicklistRepository<DashboardSetup> dashboardSetupRepository, IDataContextScopeFactory dataContextScopeFactory)
        //{
        //    _dashboardSetupRepository = dashboardSetupRepository;
        //    _dataContextScopeFactory = dataContextScopeFactory;
        //}

        //[ResponseType(typeof(IList<DashboardSetupListResponse>))]
        //[HttpGet]
        //[Route("GetDashBoards")]
        //[Authorize(Users = ApplicationUser.AdminRole)]
        //public IHttpActionResult GetItems([FromUri]ListRequest request)
        //{
        //    var lst = new ListResponse<DashboardSetupListResponse>();
        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        StringBuilder sb = new StringBuilder();
        //        sb.Append("select dashboard_setups.name, dashboard_setups.[setup-json] as setupJson,dashboard_setups.id,");
        //        sb.Append("\r\n");
        //        sb.Append(" STUFF((select distinct ', '+  Name from AspNetRoles where dashboard_setup_id = dashboard_setups.Id for xml path('') , TYPE).value('.','NVARCHAR(max)'),1,1,'') as RoleName ");
        //        sb.Append("\r\n");
        //        sb.Append("from dashboard_setups");
        //        sb.Append("\r\n");
        //        if (request.SearchPhrase != null && request.SearchPhrase.Length > 0)
        //        {
        //            sb.AppendFormat("where name like '%{0}%' ESCAPE N'~'", request.SearchPhrase.ToLower());
        //            sb.Append("\r\n");
        //        }
        //        if (request.SortOrder == SortOrders.Descending && request.SortOrder != null)
        //            sb.Append("Order by Name desc");
        //        else
        //            sb.Append("Order by Name asc");
        //        sb.Append("\r\n");

        //        sb.AppendFormat(" OFFSET({0} * {1}) ROWS FETCH NEXT {1} ROWS ONLY", (request.PageIndex - 1), request.PageSize);
        //        sb.Append("\r\n");

        //        var query = ctx.Database.SqlQuery<DashboardSetupListResponse>(sb.ToString()).ToList();
        //        List<DashboardSetupListResponse> lstData = query.ToList();

        //        lst = new ListResponse<DashboardSetupListResponse>(lstData.ToArray(), lstData.Count());
        //    }
        //    return Ok(lst);
        //}

        //[HttpGet]
        //[ResponseType(typeof(DashboardSetupResponse))]
        //[Route("GetDashBoard/{id}")]
        //public IHttpActionResult GetItem(long id)
        //{
        //    var userId = User.Identity.GetUserId();
        //    var user = AppUserManager.Users.FirstOrDefault(x => x.Id == userId);
        //    if (user == null)
        //    {
        //        //return NotFound();
        //        return Ok(new DashboardSetupResponse());
        //    }
        //    var roleName = AppUserManager.GetRoles(userId).FirstOrDefault();
        //    var item = new DashboardSetup();
        //    if (id == 0)
        //    {
        //        var existedRole = AppRoleManager.FindByName(roleName);
        //        //var item = _dashboardSetupRepository.Read(user.DashboardSetupId ?? existedRole.DashboardSetupId ?? 0);
        //        item = _dashboardSetupRepository.Read(existedRole.DashboardSetupId ?? 0);
        //    }
        //    else
        //    {
        //        item = _dashboardSetupRepository.Read(id);
        //    }

        //    if (item == null)
        //    {
        //        return Ok(new DashboardSetupResponse());
        //    }

        //    return Ok(Mapper.Map<DashboardSetup, DashboardSetupResponse>(item));
        //}

        //[HttpDelete]
        ////[Route("api/DashboardSetups/{id}")]
        //public IHttpActionResult DeleteItem(long id)
        //{
        //    var entity = _dashboardSetupRepository.Read(id);
        //    DeleteDashBoard(entity);

        //    _dashboardSetupRepository.Delete(entity);

        //    return Ok(Mapper.Map<DashboardSetup, DashboardSetupResponse>(entity));
        //}

        //[HttpPost]
        //[ResponseType(typeof(DashboardSetupResponse))]
        //[Route("UpsertDashBoard")]
        //public IHttpActionResult EditItem([FromBody] DashboardSetupRequest itemVm)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var userId = User.Identity.GetUserId();
        //    var user = AppUserManager.Users.FirstOrDefault(x => x.Id == userId);
        //    if (user == null)
        //    {
        //        //return NotFound();
        //        return Ok(new DashboardSetupResponse());
        //    }
        //    var roleName = AppUserManager.GetRoles(userId).FirstOrDefault();
        //    var existedRole = AppRoleManager.FindByName(roleName);
        //    var dashboardId = user.DashboardSetupId ?? existedRole.DashboardSetupId ?? 0;


        //    //if (!_dashboardSetupRepository.Exists(FilterQuery.Create<DashboardSetup>().AddFilter(p => p.Id == dashboardId)))
        //    if (!_dashboardSetupRepository.Exists(FilterQuery.Create<DashboardSetup>().AddFilter(p => p.Id == itemVm.Id)))
        //    {
        //        var newEntity = Mapper.Map<DashboardSetupRequest, DashboardSetup>(itemVm);
        //        _dashboardSetupRepository.Create(newEntity,
        //            UpdateOptions.Create<DashboardSetup>()
        //            .WithNotModified(p => p.Created)
        //            .WithNotModified(p => p.Modified));

        //        //UpdateCurrentUser(newEntity);

        //        return Created(GetEntityLocation(newEntity.Id), Mapper.Map<DashboardSetup, DashboardSetupResponse>(newEntity));
        //    }

        //    //var existedEntity = _dashboardSetupRepository.Read(dashboardId);
        //    var existedEntity = _dashboardSetupRepository.Read(itemVm.Id);
        //    existedEntity.Name = itemVm.Name;
        //    existedEntity.SetupJson = itemVm.SetupJson;
        //    _dashboardSetupRepository.Update(existedEntity,
        //        new UpdateOptions<DashboardSetup>()
        //        .WithNotModified(p => p.Created));

        //    // UpdateCurrentUser(existedEntity);

        //    return Ok(Mapper.Map<DashboardSetup, DashboardSetupResponse>(existedEntity));

        //}

        //private void UpdateCurrentUser(DashboardSetup entity)
        //{
        //    var userId = User.Identity.GetUserId();
        //    var user = AppUserManager.Users.FirstOrDefault(x => x.Id == userId);
        //    if (user != null)
        //    {
        //        user.DashboardSetupId = entity.Id;
        //        AppUserManager.Update(user);
        //    }
        //}

        //private void DeleteDashBoard(DashboardSetup entity)
        //{
        //    var userId = User.Identity.GetUserId();
        //    var user = AppUserManager.Users.FirstOrDefault(x => x.Id == userId);
        //    if (user != null)
        //    {
        //        user.DashboardSetupId = null;
        //        AppUserManager.Update(user);

        //        var otherUserDashboardList = AppUserManager.Users.Where(x => x.DashboardSetupId == entity.Id).ToList();
        //        if (otherUserDashboardList.Count > 0)
        //        {
        //            foreach (var item in otherUserDashboardList)
        //            {
        //                item.DashboardSetupId = null;
        //                AppUserManager.Update(item);
        //            }
        //        }
        //    }
        //    var roleName = AppUserManager.GetRoles(userId).FirstOrDefault();
        //    var existedRole = AppRoleManager.FindByName(roleName);
        //    if (existedRole != null)
        //    {
        //        existedRole.DashboardSetupId = null;
        //        AppRoleManager.Update(existedRole);
        //    }

        //}

        //private static Uri GetEntityLocation(long id)
        //{
        //    return new Uri("/DashboardSetups/" + id, UriKind.Relative);
        //}
    }
}