﻿using System.Runtime.Serialization;
using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.CustomFieldProperties;

namespace PatientCare.Web.Api.Models.CustomFields
{
    public class CustomFieldRequest
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool ResourceAvailable { get; set; }
        public bool ProjectAvailable { get; set; }
        public bool TaskAvailable { get; set; }
        public bool IntakeAvailable { get; set; }

        public CustomFieldTypeEnum Type { get; set; }
        public bool Require { get; set; }
        public string DefaultValue { get; set; }
        public string Description { get;set; }
        public long CustomFieldId { get; set; }
        public bool IsSearchable { get; set; }

        public CustomFieldPropertyRequest[] CustomFieldProperties { get; set; }
    }
}