﻿using PatientCare.Core.Enums;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Forgotpassword
{
    [DataContract]
    public class ForgotpasswordResponse
    {
        [DataMember]
        public string Username  { get; set; }

        [DataMember]
        public string ResponseType { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }
        
    }
}