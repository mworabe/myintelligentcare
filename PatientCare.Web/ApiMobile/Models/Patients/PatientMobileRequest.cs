﻿using PatientCare.Core.Enums;
using System;

namespace PatientCare.Web.ApiMobile.Models.Patients
{
    public class PatientMobileRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Prefix { get; set; }
        
        public string FirstName { get; set; }
        
        public string MiddleName { get; set; }
        
        public string LastName { get; set; }
        
        public string NickName { get; set; }
        
        public DateTime? BirthDate { get; set; }
        
        public string SSN { get; set; }
        
        public GenderEnum? Gender { get; set; }
        
        public string Email { get; set; }
        
        public string CellNumber { get; set; }
        
        public long? LanguageId { get; set; }
        
        public MartialStatusEnum? MaritialStatus { get; set; }
        
        public string EmergencyContact { get; set; }

        public RelationshipEnum? RelationshipToContact { get; set; }

        public string EmergencyPhone { get; set; }

        public bool? IsDeleted { get; set; }
    }   
}