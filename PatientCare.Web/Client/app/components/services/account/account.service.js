﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("Account", ["$resource", "CreatorSearchFunction", "Resource", function ($resource, creatorSearchFunction, resourceService) {
      var resource = $resource("api/Accounts/:id/:controller",
        null, //parameters default
          {
              'update': { method: "PUT" },
              'query': { method: "GET", params: {}, isArray: false },
              'userRules': { method: "GET", params: { controller: "user-rules" }, isArray: false }
          });

      resource.roles = [];

      creatorSearchFunction(self, "searchResource", resourceService, "firstName", { showInactive: true });
      resource.getTableOptionUsers = {
          pageName: "Users",
          cols: function () {
              return [
                  { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                  { field: "userName", title: "User Name", sortable: "userName", show: true, dataType: "text", required: true },
                  { field: "role", title: "Role", sortable: "role", show: true, dataType: "ui-select", required: true, list: function () { return resource.roles; } },
                  {
                      title: "Resource",
                      fieldInit: "resource",
                      field: "resourceId",
                      initValue: "resource",
                      dataType: "angucomplete-alt",
                      search: self.searchResource,
                      sortable: "resource.FirstName",
                      required: false,
                      customViewText: function (row) {
                          if (row.resource)
                              return row.resource.name;
                          return "";
                      },
                      angucompleteSelect: function (select) {
                          var item = this.$parent.$parent.$parent.$parent.row;
                          if (select && select.originalObject && select.originalObject.id > 0) {
                              item.resourceId = select.originalObject.id;
                              item.resource = select.originalObject;
                          } else {
                              item.resource = null;
                              item.resourceId = null;
                          }

                      }
                  },
                  { field: "password", title: "Password", sortable: false, show: true, dataType: "password", required: false, dataTypeView: "empty" },
                  { field: "action", title: ".", dataType: "command" },
              ];
          }
      };
      return resource;
  }]);
