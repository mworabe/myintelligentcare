﻿using System;
using System.Data;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle clinic operations And implementing all methods which is initialize in IClinicService.
    /// </summary>
    public class ClinicService : IClinicService
    {
        /// <summary>
        ///Description : Clinic Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Clinic Service Field IClinicRepository
        /// </summary>
        private readonly IClinicRepository _clinicRepository;

        /// <summary>
        ///Description : Clinic Service Field IClinicLocationRepository
        /// </summary>
        private readonly IClinicLocationRepository _clinicLocationRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Clinic class.
        /// </summary>
        /// <param name="dataContextScopeFactory">Data Context Scoper Factory</param>
        /// <param name="clinicRepository">Clininc Repository</param>
        /// <param name="clinicLocationRepository">Clininc Location Repository</param>
        public ClinicService
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IClinicRepository clinicRepository,
                IClinicLocationRepository clinicLocationRepository
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _clinicLocationRepository = clinicLocationRepository;
            _clinicRepository = clinicRepository;
        }

        #region Clinic Methods

        /// <summary>
        /// Method Description : Using for Update clinic data.
        /// </summary>
        /// <param name="clinic">Clinic Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateClinicAsync(Clinic clinic)
        {
            if (clinic == null) throw new ArgumentNullException("clininc");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originLeave = _clinicRepository.Read(clinic.Id, new ReadOptions<Clinic>().AsReadOnly());

                await _clinicRepository.UpdateAsync(clinic, UpdateOptions.Create<Clinic>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new clinic.
        /// </summary>
        /// <param name="clinic">Clinic Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateClinicAsync(Clinic clinic)
        {
            if (clinic == null) throw new ArgumentNullException("clinic");

            await _clinicRepository.CreateAsync(clinic, UpdateOptions.Create<Clinic>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get clinic data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Clininc Entity</returns>
        public async Task<Clinic> ReadClinicAsync(long id)
        {
            var readOptions = new ReadOptions<Clinic>().AsReadOnly();

            var clinic = await _clinicRepository.ReadAsync(id, readOptions);

            return clinic;

        }

        /// <summary>
        /// Method Description : Using for Delete clinic data.
        /// </summary>
        /// <param name="clinic">Clinic Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteClinicAsync(Clinic clinic)
        {
            //Note : here we are not delete Clinic but we are keep entry is deleted.

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                clinic.IsDeleted = true;

                var originPatient = _clinicRepository.Read(clinic.Id, new ReadOptions<Clinic>().AsReadOnly());

                await _clinicRepository.UpdateAsync(clinic, UpdateOptions.Create<Clinic>()
                    .WithNotModified(p => p.Created));

                return true;
            }
        }

        #endregion

        #region Clinic Location Methods

        /// <summary>
        /// Method Description : Using for Update clinic location da.ta.
        /// </summary>
        /// <param name="cliniclocation">Clinic Location Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateClinicLocationAsync(ClinicLocation cliniclocation)
        {
            if (cliniclocation == null) throw new ArgumentNullException("clininclocation");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originLeave = _clinicLocationRepository.Read(cliniclocation.Id, new ReadOptions<ClinicLocation>().AsReadOnly());

                await _clinicLocationRepository.UpdateAsync(cliniclocation, UpdateOptions.Create<ClinicLocation>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new clinic location.
        /// </summary>
        /// <param name="cliniclocation">Clinic Location Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateClinicLocationAsync(ClinicLocation cliniclocation)
        {
            if (cliniclocation == null) throw new ArgumentNullException("cliniclocations");

            cliniclocation.Created = DateTime.UtcNow;
            await _clinicLocationRepository.CreateAsync(cliniclocation, UpdateOptions.Create<ClinicLocation>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get clinic location data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Clinic Location Entity</returns>
        public async Task<ClinicLocation> ReadClinicLocationAsync(long id)
        {
            var readOptions = new ReadOptions<ClinicLocation>().AsReadOnly();

            var cliniclocation = await _clinicLocationRepository.ReadAsync(id, readOptions);

            return cliniclocation;

        }

        /// <summary>
        /// Method Description : Using for Delete clinic location data.
        /// </summary>
        /// <param name="cliniclocation">Clinic Location Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteClinicLocationAsync(ClinicLocation cliniclocation)
        {
            //Note : here we are not delete Clinic Location but we are keep entry is deleted.
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                cliniclocation.IsDeleted = true;

                var originPatient = _clinicLocationRepository.Read(cliniclocation.Id, new ReadOptions<ClinicLocation>().AsReadOnly());

                await _clinicLocationRepository.UpdateAsync(cliniclocation, UpdateOptions.Create<ClinicLocation>()
                    .WithNotModified(p => p.Created));

                return true;
            }
        }

        #endregion
    }
}
