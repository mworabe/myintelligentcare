﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.AppointmentTypes;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using PatientCare.Web.Api.Models;
using System.Linq.Expressions;
using System.Linq;
using System.Web.Http.Description;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Appointment Type operations.
    /// </summary>
    [Authorize]
    public sealed class AppointmentTypesController : ApiBaseController
    {
        /// <summary>
        /// Description : Appointment Type Field IPatientService.
        /// </summary>
        private readonly IPatientService _patientService;

        /// <summary>
        /// Description : Appointment Type Field IAppointmentTypeRepository.
        /// </summary>
        private readonly IAppointmentTypeRepository _appointmentTypeRepository;

        ReadOptions<AppointmentType> _readOptions = new ReadOptions<AppointmentType>();

        /// <summary>
        /// Description : Initializes a new instance of the Appointment Type class. 
        /// </summary>
        /// <param name="patientService"></param>
        /// <param name="appointmentTypeRepository"></param>
        public AppointmentTypesController
            (
                 IPatientService patientService,
                 IAppointmentTypeRepository appointmentTypeRepository
            )
        {
            _patientService = patientService;
            _appointmentTypeRepository = appointmentTypeRepository;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/AppointmentTypes/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Create new Appointment Type. 
        /// </summary>
        /// <param name="appointmenttypesrequest">Appointment Types Request Model</param>
        /// <returns>Appointment Types Response Model</returns>
        [HttpPost]
        [Route("api/AppointmentTypes")]
        [SwaggerResponse(201, "Created", typeof(AppointmentTypesResponse))]
        public async Task<IHttpActionResult> CreateAppointmentType([FromBody]AppointmentTypesRequest appointmenttypesrequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var appointmenttype = Mapper.Map<AppointmentTypesRequest, AppointmentType>(appointmenttypesrequest);
                await _patientService.CreateApointmentTypeAsync(appointmenttype);

                return Created(GetEntityLocation(appointmenttype.Id), Mapper.Map<AppointmentType, AppointmentTypesResponse>(appointmenttype));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Update Appointment Type.  
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="appointmenttypesrequest">Appointment Types Request Model</param>
        /// <returns>Appointment Types Response Model</returns>
        [HttpPut]
        [Route("api/AppointmentTypes/{id}")]
        public async Task<IHttpActionResult> EditAppointmentType(long id, [FromBody] AppointmentTypesRequest appointmenttypesrequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var AppointmentType = Mapper.Map<AppointmentTypesRequest, AppointmentType>(appointmenttypesrequest);
                AppointmentType.Id = id;

                await _patientService.UpdateApointmentTypeAsync(AppointmentType);

                return Ok(Mapper.Map<AppointmentType, AppointmentTypesResponse>(AppointmentType));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Delete Appointment Type by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Appointment Type Entity</returns>
        [HttpDelete]
        [Route("api/AppointmentTypes/{id}")]
        public async Task<IHttpActionResult> DeleteAppointmentTypeItem(long id)
        {
            try
            {
                var success = await _patientService.DeleteApointmentTypeAsync(id);
                if (!success)
                {
                    return NotFound();
                }

                return Ok(new AppointmentType { Id = id });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Get Appointment Type by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Appointment Types Response Model</returns>
        [HttpGet]
        [Route("api/AppointmentTypes/{id}")]
        public async Task<IHttpActionResult> GetAppointmentType(long id)
        {
            try
            {
                var AppointmentType = await _patientService.ReadApointmentTypeAsync(id);
                if (AppointmentType == null)
                {
                    return NotFound();
                }

                var viewModel = Mapper.Map<AppointmentType, AppointmentTypesResponse>(AppointmentType);
                return Ok(viewModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Get All Appointment Type.     
        /// </summary>
        /// <param name="request">Appointment Types List Request Model</param>
        /// <returns>Appointment Types List Response Model</returns>
        [HttpGet]
        [Route("api/AppointmentTypes")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<AppointmentTypesResponse>))]
        public IHttpActionResult GetAllAppointmentType([FromUri]AppointmentTypesListRequest request)
        {
            try
            {
                var query = PagingExtensions<AppointmentType>.CreatePagedQuery
                          (request,
                              (string.IsNullOrWhiteSpace(request.SearchField)
                                  && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                      ?
                                 GetSearchColumnExpressions(request.SearchPhrase)
                                : GetAutocompleteSearchExpressions(request.SearchPhrase)
                               ));

                var readOptions = new ReadOptions<AppointmentType>().AsReadOnly();
                var page = _appointmentTypeRepository.PagedList(query, readOptions);
                var dtos = Mapper.Map<AppointmentType[], AppointmentTypesPicklistResponse[]>(page.Entities.ToArray());

                var response = new ListResponse<AppointmentTypesPicklistResponse>(dtos, page.TotalCount);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private static Expression<Func<AppointmentType, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return AppointmentType => AppointmentType.Name.Contains(searchPhrase)
                || AppointmentType.Color.Contains(searchPhrase);
        }

        private static Expression<Func<AppointmentType, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return AppointmentType => AppointmentType.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Appointment Type.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Appointment Type AutoComplete Response Model</returns>
        [HttpGet]
        [Route("api/AppointmentTypes/autocomplete")]
        [ResponseType(typeof(AppointmentTypeAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            try
            {
                var filter = new FilterQuery<AppointmentType>();
                if (!string.IsNullOrWhiteSpace(search))
                    filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));

                var autocompleteResult = await _appointmentTypeRepository.AutocompleteAsync(filter);

                var result = Mapper.Map<AppointmentType[], AppointmentTypeAutoCompleteResponse[]>(autocompleteResult.ToArray());

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
