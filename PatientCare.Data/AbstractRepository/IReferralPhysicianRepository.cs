﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IReferralPhysicianRepository : IRepository<ReferralPhysician>
    {
    }
}
