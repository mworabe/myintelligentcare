﻿using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PatientCases
{
    [DataContract]
    public class PatientCurrentCaseData
    {
        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public long? CaseId { get; set; }

        [DataMember]
        public string CaseNo { get; set; }

        [DataMember]
        public string PatientNumber { get; set; }

        [DataMember]
        public long? InjuryRegionId { get; set; }

        [DataMember]
        public string InjuryRegion { get; set; }

        [DataMember]
        public long? AssignedPhysicianId { get; set; }

        [DataMember]
        public PicklistDefaultResponse AssignedPhysician { get; set; }

    }
}