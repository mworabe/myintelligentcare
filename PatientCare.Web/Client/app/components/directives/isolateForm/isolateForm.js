﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngIsolateForm", [
        function() {
            return {
                restrict: "A",
                require: "?form",
                link: function (scope, elm, attrs, ctrl) {
                    if (!ctrl) {
                        return;
                    }

                    // Do a copy of the controller
                    var ctrlCopy = {};
                    angular.copy(ctrl, ctrlCopy);

                    // Get the parent of the form
                    var parent = elm.parent().controller("form");
                    // Remove parent link to the controller
                    parent.$removeControl(ctrl);

                    // Replace form controller with a "isolated form"
                    var isolatedFormCtrl = {
                        $setValidity: function(validationToken, isValid, control) {
                            ctrlCopy.$setValidity(validationToken, isValid, control);
                            parent.$setValidity(validationToken, true, ctrl);
                        },
                        $setDirty: function() {
                            elm.removeClass("ng-pristine").addClass("ng-dirty");
                            ctrl.$dirty = true;
                            ctrl.$pristine = false;
                        }
                    };
                    angular.extend(ctrl, isolatedFormCtrl);
                }
            };
        }
    ]);