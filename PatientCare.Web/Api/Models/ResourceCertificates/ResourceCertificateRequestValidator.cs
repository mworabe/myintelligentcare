﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceCertificates
{
    public class ResourceCertificateRequestValidator : AbstractValidator<ResourceCertificateRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IResourceRepository _resourceRepository;
        private readonly IPicklistRepository<Certificate> _certificateRepository;

        public ResourceCertificateRequestValidator(IDataContextScopeFactory dbContextScopeFactory, IResourceRepository resourceRepository, IPicklistRepository<Certificate> certificateRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _resourceRepository = resourceRepository;
            _certificateRepository = certificateRepository;

            //RuleFor(model => model.Certified)
            //    .NotEmpty();

            //RuleFor(model => model.ResourceId)
            //    .NotEmpty()
            //    .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _resourceRepository, l))
            //    .WithMessage("The resource with provided Id doesn't exists");

            RuleFor(model => model.CertificateId)
                .NotEmpty()
                .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _certificateRepository, l))
                .WithMessage("The certificate with provided Id doesn't exists");

        }
    }
}