﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("AvaialabilityCalculationService", [
    function () {
        var self = this;

        self.getDemandHours = function (col) {
            /*  returns row demand hours form a column   */
            return (parseFloat(col.demand)).toFixed(2);
        }
        self.getCalculatedActualHours = function (col) {
            /*  returns calculated Actual hours form a column   */
            var totalTimeOff = (parseFloat(col.scheduledTimeOff) + parseFloat(col.lessHoliday));
            return (col.totalHoursCount - totalTimeOff).toFixed(2);
        }
        self.getCalculatedReservedAvailabilityHours = function (col) {
            /*  returns calculated reserved availability hours form a column   */
            var actualHours = self.getCalculatedActualHours(col);
            var reservedAvailabilityPercentage = parseFloat(col.reservedAvailabilityPercentage).toFixed(2);
            return ((actualHours * reservedAvailabilityPercentage) / 100).toFixed(2);
        }
        self.getTotalDemandHoursForIndividualResource = function (col) {
            var scheduledTimeOff = col.scheduledTimeOff;
            var lessHoliday = col.lessHoliday;
            var reservedAvailableHours = self.getCalculatedReservedAvailabilityHours(col);
            var demandHours = col.demand;
            var totalWeightRed = (parseFloat(scheduledTimeOff) + parseFloat(lessHoliday) + parseFloat(reservedAvailableHours) + parseFloat(demandHours));
            return totalWeightRed.toFixed(2);
        }
        self.getAvailableWrokHoursForIndividualResource = function (col) {
            var actualHours = parseFloat(self.getCalculatedActualHours(col));
            var reservedAvailableHours = parseFloat(self.getCalculatedReservedAvailabilityHours(col));
            var demandHours = parseFloat(self.getDemandHours(col));
            return parseFloat(actualHours - reservedAvailableHours - demandHours).toFixed(2);
        }
        self.getHolidayHours = function (col) {
            return (parseFloat(col.lessHoliday)).toFixed(2);
        }
        self.getScheduledTimeOff = function (col) {
            return parseFloat(col.scheduledTimeOff).toFixed(2);
        }
        self.getTotalHoursCount = function (col) {
            return parseFloat(col.totalHoursCount).toFixed(2);
        },
        // Calculations for Bar
        self.calculateResourceDemandPercentage = function (col) {
            var scheduledTimeOff = col.scheduledTimeOff;
            var lessHoliday = col.lessHoliday;
            var reservedAvailableHours = self.getCalculatedReservedAvailabilityHours(col);
            var demandHours = col.demand;
            var totalWeightRed = (parseFloat(scheduledTimeOff) + parseFloat(lessHoliday) + parseFloat(reservedAvailableHours) + parseFloat(demandHours));
            return ((totalWeightRed * 100) / col.totalHoursCount).toFixed(0);
        }
        self.calculateResourceAvailabilityPercentage = function (col) {
            var reservedAvailableHours = self.getAvailableWrokHoursForIndividualResource(col);
            return ((reservedAvailableHours * 100) / col.totalHoursCount).toFixed(0);
        }



        function getAvailabilityData(col) {
            var availabilityObj = {};
            /* BAR Calculations availabilityPercentage */
            availabilityObj.demandPercentage = parseFloat(self.calculateResourceDemandPercentage(col));
            availabilityObj.availabilityPercentage = parseFloat(self.calculateResourceAvailabilityPercentage(col));
            /* POPUP - Data Calculations    */
            availabilityObj.totalDemandHours = parseFloat(self.getTotalDemandHoursForIndividualResource(col));
            availabilityObj.availableWrokHours = parseFloat(self.getAvailableWrokHoursForIndividualResource(col));
            availabilityObj.scheduledTimeOff = parseFloat(self.getScheduledTimeOff(col));
            availabilityObj.holidayHours = parseFloat(self.getHolidayHours(col));
            availabilityObj.demandHours = parseFloat(self.getDemandHours(col));
            availabilityObj.reservedAvailabilityPercentage = parseFloat(col.reservedAvailabilityPercentage).toFixed(2);
            availabilityObj.calculatedActualHours = parseFloat(self.getCalculatedActualHours(col));
            availabilityObj.calculatedReservedAvailabilityHours = parseFloat(self.getCalculatedReservedAvailabilityHours(col));
            availabilityObj.totalHoursCount = parseFloat(self.getTotalHoursCount(col));


            availabilityObj.utilization = parseFloat((availabilityObj.totalDemandHours * 100) / availabilityObj.totalHoursCount).toFixed(2);
            availabilityObj.id = col.id;

            return availabilityObj;
        }



        return {
            getAvailabilityData: getAvailabilityData,
        };
    }
]);