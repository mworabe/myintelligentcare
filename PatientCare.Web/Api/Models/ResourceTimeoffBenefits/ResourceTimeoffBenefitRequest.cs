﻿namespace PatientCare.Web.Api.Models.ResourceTimeoffBenefits
{
    public class ResourceTimeoffBenefitRequest
    {
        public long Id { get; set; }

        public long? ResourceId { get; set; }

        public long? TimeoffTypeId { get; set; }

        public decimal? TotalBalance { get; set; }
        public decimal? Balance { get; set; }
        public decimal? AccruedCurrent { get; set; }
    }
}