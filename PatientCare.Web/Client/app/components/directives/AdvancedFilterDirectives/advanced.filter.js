﻿"use strict";
var EOPApp = angular.module("EDZoutstaffingPortalApp");
//EOPApp.config(function ($stateProvider) { });
EOPApp.directive("advancedFilter", [function () {
    return {
        templateUrl: "Client/app/components/directives/AdvancedFilterDirectives/advanced-filter-view.html",
        restrict: "E",
        controller: "AdvancedFilterController",
        controllerAs: "AdvancedFilterCtrl",
        replace: false,
        scope: {
            filterFieldsList: '=ngFilterFieldList',
            parentController: '=parentController',
        },
        link: function (scope) { }
    };
}]);

EOPApp.controller("AdvancedFilterController",
    ["$rootScope", "$scope", "$state", "AdvancedFilterConstants", "AdvancedFilterService", "toaster", "ResourceScoreFilter", "Modal", "$uibModal",
        function ($rootScope, $scope, $state, advancedFilterConstants, advancedFilterService, toaster, resourceScoreFilter, modal, $uibModal) {
            var self = this;
            self.isDataFiltered = false;
            self.filterItemList = [];
            if (typeof $scope.filterFieldsList === undefined) {
                console.error("ng-filter-field-list is required", $scope.filterFieldsList);
                return;
            }
            $scope.filterFieldsList = self.localFilterFieldsList = advancedFilterService.getAdvancedFilterFileds({ fields: "resources" });
            //angular.copy($scope.filterFieldsList);
            self.allAdvancedFilterDataTypes = advancedFilterConstants.advancedFilterDataTypes;
            self.comparisionOperators = [];
            $scope.logicalOperators = advancedFilterConstants.allLogicalOperators;
            $scope.logicalOperatorItem = {};
            $scope.logicalOperators.selected = $scope.logicalOperators[0];

            $scope.$on("edz:clear-filter", function (event, data) {
                console.info("edz:clear-filter");
                self.filterItemList = [];
            });


            self.onFilterCriteriaChanged = function (selectedFieldItem, currentFilterItem) {
                //currentFilterItem.comparisionOperators = [];
                var currentFieldDataType = _.find(self.allAdvancedFilterDataTypes, { dataType: selectedFieldItem.fieldType });
                currentFilterItem.comparisionOperators = currentFieldDataType.supportedComparisionOperators;
                currentFilterItem.fieldValue1 = "";
                currentFilterItem.fieldValue2 = "";
                currentFilterItem.selectedComparisionOperator = "";
                //self.comparisionOperators = selectedFieldItem.comparision_operators;
                $scope.$apply();
            }
            self.onFilterComparisionOperatorChanged = function (selectedComparisionOperator) {

            }
            self.isFilterButtonDisabled = function (form) {
                return true;
            }
            function applyFilters(data) {
                var loadedFilter = $scope.parentController.checkAndReturnValueInAngucomplete(data.selectedFilter);
                if (loadedFilter != null) {
                    self.loadedFilterId = (loadedFilter.id) ? loadedFilter.id : 0;
                    self.filterName = loadedFilter.name;
                    self.filterItemList = angular.fromJson(loadedFilter.jsonFilter);
                    //self.filterButtonClick();
                }
            }

            /*
            *   Basic Button functions.
            */

            self.addInFilterValue = function (currentFilterItem) {
                if (currentFilterItem.field.fieldType === "UI_SELECT") {
                    if (currentFilterItem && currentFilterItem.fieldValue1.value && currentFilterItem.InValueList) {
                        var obj = {};
                        obj.displayName = currentFilterItem.fieldValue1.name;
                        obj.id = currentFilterItem.fieldValue1.value;
                        var existsObj = _.find(currentFilterItem.InValueList, function (item) { return item.id === obj.id });
                        console.info(existsObj);
                        if (existsObj && existsObj.id) {
                            toaster.pop("error", "", "Already Exists in List");
                            return;
                        }
                        currentFilterItem.InValueList.push(obj);
                    }
                } else {
                    if (currentFilterItem && currentFilterItem.fieldValue1.originalObject && currentFilterItem.InValueList) {
                        var existObj = _.find(currentFilterItem.InValueList, currentFilterItem.fieldValue1.originalObject);
                        if (existObj && existObj.id && existObj.id > 0) {
                            toaster.pop("error", "", "Already Exists in List");
                            return;
                        }
                        currentFilterItem.InValueList.push(currentFilterItem.fieldValue1.originalObject);
                        $scope.$broadcast('angucomplete-alt:clearInput', 'field_in_operator_value_selector');
                    }
                }
            }

            self.removeInFilterValue = function (valueList, value) {
                console.info(valueList);
                console.info(value);
                var index = valueList.indexOf(value);
                console.info(index);
                if (index >= 0 && index <= valueList.length) {
                    valueList.splice(index, 1);
                }
            }
            self.addFilterCriteria = function () {
                var item = {};
                self.filterItemList.push({
                    InValueList: []
                });
            }
            self.addFilterCriteriaGroupItem = function (filterItem) {
                filterItem.condition = [];
                filterItem.condition.push({});
            }
            /*self.addFilterCriteriaGroup = function () {
                var item = {};
                self.filterItemList.push({});
            }*/
            self.removeFilterCriteria = function (item) {
                var index = self.filterItemList.indexOf(item);
                self.filterItemList.splice(index, 1);
                if (self.isDataFiltered) {
                    self.isDataFiltered = !self.isDataFiltered;
                    self.filterButtonClick();
                }
            }
            self.clearFilter = function () {
                $scope.parentController.advancedFilterServerCall("1=1");
                self.filterItemList = [];
            }
            self.onCancelButtonClick = function () {
                $state.go("resource");
            }
            self.loadStoredFilters = function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: "Client/app/controllers/filters/loadFiltersModal.html",
                    controller: "LoadFiltersCtrl as LoadFiltersCtrl",
                    resolve: {
                        data: function () {
                            return {
                                searchResourceScoreFilters: self.searchResourceScoreFilters,
                                filterType: "advanceFilter",
                            }
                        }
                    }
                });
                modalInstance.result.then(applyFilters);
            }

            $rootScope.$on('edz:advanced-filter-loaded', function (event, args) {
                console.info(args.data);
                applyFilters(args.data);
            })

            self.saveCurrentFilters = function () {
                if (self.loadedFilterId > 0) {
                    var saveFiltersModal = modal.confirm.templateDialog(function (data) {
                        var filter = {};
                        filter.id = self.loadedFilterId;
                        filter.name = self.filterName;
                        filter.filterType = "advanceFilter";
                        filter.jsonFilter = angular.toJson(self.filterItemList);
                        resourceScoreFilter.update({ id: self.loadedFilterId }, filter, function () {
                            toaster.pop("success", "", "Filter updated successfully.");
                        }, function () { });
                    }, "Client/app/controllers/filters/saveFiltersModal.html");
                    saveFiltersModal({ name: self.filterName });
                } else {

                    var saveFiltersModal = modal.confirm.templateDialog(function (data) {
                        var filter = {};
                        filter.name = data.name;
                        filter.filterType = "advanceFilter";
                        filter.jsonFilter = angular.toJson(self.filterItemList);
                        resourceScoreFilter.save(filter, function () {
                            toaster.pop("success", "", "save succesfull");
                        });
                    }, "Client/app/controllers/filters/saveFiltersModal.html");
                    saveFiltersModal({});
                }
            }
            self.filterButtonClick = function () {
                self.isDataFiltered = true;
                var generatedWhereStatement = generateWhereStatement(self.filterItemList);
                if (generatedWhereStatement.length > 0)
                    $scope.parentController.advancedFilterServerCall(generatedWhereStatement, 1);
            }

            /*
            *   Basic Button functions.
            */

            function generateWhereStatement(group) {
                if (!group && !(group.length > 0)) {
                    toaster.info("", "Advanced Filter is Empty");
                    return
                }

                function getSelectedFilterValue(filterValue, fieldType) {
                    if (fieldType === 'AUTOCOMPLETE') {
                        if (typeof filterValue === 'object' && filterValue.originalObject)
                            return filterValue.originalObject.id;
                    } else if (fieldType === "DATE_TIME") {
                        if (filterValue) {
                            return "" + moment(new Date(filterValue)).format("YYYY-MM-DD") + "";
                        } else {
                            return "" + moment(new Date()).format("YYYY-MM-DD") + "";
                        }
                    } else if (fieldType === "UI_SELECT") {
                        return (filterValue.value) ? filterValue.value : filterValue;
                    }
                    return filterValue;

                }

                for (var str = "", index = 0; index < group.length; index++) {
                    var logicalOperator = (index > 0 && group[index].logicalOperator && group[index].logicalOperator.value) ? group[index].logicalOperator.value : "";

                    if (index > 0) {
                        str += "" + logicalOperator + " ";
                    }

                    var selectedField = group[index].field;
                    var selectedFieldType = (selectedField && selectedField.fieldType) ? selectedField.fieldType : "";
                    var selectedComparisionOperator = group[index].selectedComparisionOperator;
                    var selectedFilterValue = getSelectedFilterValue(group[index].fieldValue1, selectedFieldType);
                    if (selectedComparisionOperator && selectedComparisionOperator.value === "LIKE")
                        selectedFilterValue = "%" + getSelectedFilterValue(group[index].fieldValue1, selectedFieldType) + "%";
                    else if (selectedComparisionOperator && selectedComparisionOperator.value === "BETWEEN" && selectedFieldType === "DATE_TIME")
                        selectedFilterValue = "" + getSelectedFilterValue(group[index].fieldValue1, selectedFieldType) + "'' AND ''" + getSelectedFilterValue(group[index].fieldValue2, selectedFieldType) + "";
                    else if (selectedComparisionOperator && selectedComparisionOperator.value === "IN" && (selectedFieldType === "AUTOCOMPLETE" || selectedFieldType === "UI_SELECT")) {
                        var inValueList = group[index].InValueList;
                        for (var inQueryStr = "(", i = 0; i < inValueList.length; i++) {
                            i > 0 && (inQueryStr += ",");
                            inQueryStr += "" + inValueList[i].id;
                        }
                        selectedFilterValue = (inQueryStr.length > 1) ? inQueryStr + ")" : "";
                    }

                    if (!!selectedField && !!selectedField.fieldName && !!selectedComparisionOperator && !!selectedComparisionOperator.value && !!selectedFilterValue) {
                        var filterValue = ((selectedFieldType === 'AUTOCOMPLETE' || selectedFieldType === "UI_SELECT") && selectedComparisionOperator.value == "IN") ? selectedFilterValue : "''" + selectedFilterValue + "''";//(selectedFieldType === 'TEXT') ? "''" + selectedFilterValue + "''" : selectedFilterValue;
                        str += selectedField.fieldName + " " + selectedComparisionOperator.value + " " + filterValue + " "; // Generating further Query.
                    } else {
                        toaster.pop("error", "", "Invalid Filter");
                        return "";
                    }

                }
                if (str && str.length > 0)
                    return str;
                // Hack to Run Application without throughing errors -> if we sent empty string it will break system.
                return "1 = 1";
            }


        }
    ]
);