﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.LeaveRequests;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class LeaveRequestMapperExtensions
    {
        public static EmitMapperMapperProvider LeaveRequestRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveRequest, LeaveRequestsResponse>
                    (new ExtendedMapConfig<LeaveRequest, LeaveRequestsResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveRequest[], LeaveRequestsResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveRequest[], LeaveRequestsPicklistAdminResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveRequest[], LeaveRequestsPicklistUserResponse[]>());
            
            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveRequest[], LeaveRequestsAdminApprovalListResponseView[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveRequestsRequest, LeaveRequest>
                    (new ExtendedMapConfig<LeaveRequestsRequest, LeaveRequest>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveRequestsAdminApprovalListResponseView, LeaveRequest>
                    (new ExtendedMapConfig<LeaveRequestsAdminApprovalListResponseView, LeaveRequest>()));

            return mapperProvider;
        }
    }
}