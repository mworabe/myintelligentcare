// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright � 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("AnalyticsCtrl", [
        "$scope", "$state", function ($scope, $state) {
            $state.go("forbidden");
            var self = this;

            $scope.projectTypeData = [
                {
                    key: "Cumulative Return",
                    values: [
                        ["Acquisitions", 1],
                        ["Compliance", 2],
                        ["Infrastructure", 9],
                        ["Maintenance", 2],
                        ["Outsourcing", 4],
                        ["Security", 2],
                        ["Supports Growth", 5]
                    ]
                }
            ];

            $scope.projectPriorityData = [
                {
                    key: "Cumulative Return",
                    values: [
                        ["High", 14],
                        ["Medium", 5],
                        ["Low", 6]
                    ]
                }
            ];

            $scope.projectStatusData = [
                {
                    key: "Cumulative Return",
                    values: [
                        ["Not Approved", 2],
                        ["Approved", 11],
                        ["In Progress", 9],
                        ["Completed", 0],
                        ["On Hold", 3]
                    ]
                }
            ];

            $scope.projectsByDivisionData = [9, 1, 4, 11];
            $scope.projectsByDivisionLabels = [
            "Ameriprise",
            "AM Mutual Funds",
            "AM Insurance",
            "AM Brokerage"
            ];

            $scope.projectHealthLabels = [
                "On Track",
                "Critical"
            ];
            $scope.projectHealthData = [20, 5];

        }
    ]);