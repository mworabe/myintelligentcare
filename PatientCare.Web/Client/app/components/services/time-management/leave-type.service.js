﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("LeaveTypeService", ["$resource", function ($resource) {
      var resource = $resource("api/leaveType/:id/:controller",
        null, //parameters default
            {
                'update': { method: "PUT" },
                'query': { method: "GET", params: {}, isArray: false }
            });

      resource.getTableOption = {
          pageName: "Charge Code",
          hideSearch: true,
          hideBulkEdit: true,
          cols: function () {
              return [
                  { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                 /* {
                      field: "color",
                      title: "Color",
                      sortable: "color",
                      show: true,
                      dataTypeView: "color-picker",
                      dataType: "color-picker",
                      required: true,
                      getClasses: function (row) {
                          return "color-column";
                      },
                      getColumnClasses: function () {
                          return "col-lg-1";
                      }
                  },*/
                  {
                      field: "name",
                      title: "Name",
                      color: function (row) {
                          return row.color || "transparent";
                      },
                      sortable: "name",
                      show: true,
                      dataType: "text",
                      required: true,
                      getColumnClasses: function () {
                          return "col-lg-6";
                      }
                  },
                  {
                      field: "isPaid",
                      title: "Type",
                      sortable: "isPaid",
                      show: true,
                      dataType: "ui-select",
                      required: true,
                      list: [{ title: "Paid", value: 1 }, { title: "Unpaid", value: 0 }],
                      getColumnClasses: function () {
                          return "col-lg-2";
                      }
                  },
                  {
                      field: "totalLeaves",
                      title: "Total Time Off",
                      sortable: "totalLeaves",
                      show: true,
                      dataType: "text",
                      required: true,
                      getColumnClasses: function (row) {
                          return "col-lg-2";
                      }
                  },
                  {
                      field: "action",
                      title: ".",
                      dataType: "command",
                      class: 'blank-cell',
                      getColumnClasses: function (row) {
                          return "col-lg-1 ";
                      }
                  },
              ];
          }
      };
      return resource;
  }]);
