﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Case Status Enum.
    /// </summary>
    public enum CaseStatusEnum
    {
        New = 0,
        Inprogress = 1,
        Discharged = 2,
        Inactive = 3,
        Pending=4
    }
}
