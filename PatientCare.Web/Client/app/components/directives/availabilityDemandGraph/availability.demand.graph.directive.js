﻿"use strict";
var app = angular.module("EDZoutstaffingPortalApp");
app.directive("availabilityGraph", ["uuid2", "$uibModal", "Modal", "toaster",
    function (uuid2, $uibModal, modal, toaster) {
        return {
            templateUrl: "Client/app/components/directives/availabilityDemandGraph/AvailabilityDemandGraphView.html",
            restrict: "E",
            replace: false,
            scope: {
                availableVsDemandHeaders: '=ngHeaderList',
                availableVsDemandData: '=ngDataList'
            },
            link: function (scope) {

                scope.calculateWightsGren = calculateWightsGren;
                scope.calculateWightsRed = calculateWightsRed;

                function calculateWightsGren(col) {
                    var WightsGren = (col.available / col.hoursCount) * 100;
                    // (55 / 184) * 100
                    return WightsGren.toFixed(2);
                }
                function calculateWightsRed(col) {
                    var WightsRed = (col.demand / col.hoursCount) * 100;
                    return WightsRed.toFixed(2);
                }
            }
        };
    }]);