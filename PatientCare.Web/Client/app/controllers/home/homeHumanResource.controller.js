'use strict';

angular.module('EDZoutstaffingPortalApp')
  .controller('HomeHumanResourceCtrl', ["PermissionWorker",
    function (permissionWorker) {
        this.message = "hello HumanResource";
        this.canProjectRead = permissionWorker.canProjectRead;
        this.canResourceRead = permissionWorker.canResourceRead;
        this.canFinanceRead = permissionWorker.canFinanceRead;
    }]);
