﻿"use strict";

(function edzAddCustomFieldFactoryIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzDynamicFormControllerTemplateFactory", ["DYNAMIC_FORM_CONSTROLLER", edzAddCustomFieldFactory]);

    function edzAddCustomFieldFactory(DYNAMIC_FORM_CONTROLLER) {
        return getTemplate;

        function getTemplate(field) {
            switch (field.type) {
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.TEXT:
                    return "Client/app/components/dynamicFormController/temlates/text.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.MASKEDTEXT:
                    return "Client/app/components/dynamicFormController/temlates/maskedText.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.DATE_TIME:
                    return "Client/app/components/dynamicFormController/temlates/dateTime.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.TIME:
                    return "Client/app/components/dynamicFormController/temlates/time-picker.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.UI_SELECT:
                    return "Client/app/components/dynamicFormController/temlates/uiSelect.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.AUTOCOMPLETE:
                    return "Client/app/components/dynamicFormController/temlates/autocomplete.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.PHONE:
                    return "Client/app/components/dynamicFormController/temlates/phone.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.NUMBER:
                    return "Client/app/components/dynamicFormController/temlates/number.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.CHECKBOX:
                    return "Client/app/components/dynamicFormController/temlates/checkbox.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.TEXTAREA:
                    return "Client/app/components/dynamicFormController/temlates/textarea.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.CURRENCY:
                    return "Client/app/components/dynamicFormController/temlates/currency.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.MULTI_SELECT:
                    return "Client/app/components/dynamicFormController/temlates/multiSelectDropDown.html";
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.AUTO_COMPLETE_TYPE_ABLE:
                    return "Client/app/components/dynamicFormController/temlates/auto-complete-select-type-field.html";

                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.AUTO_COMPLETE_WITH_BUTTON:
                    return "Client/app/components/dynamicFormController/temlates/autocomplete-with-top-button.html";

                    
                case DYNAMIC_FORM_CONTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE:
                    return field.templateUrl;

                default:
                    throw new Error("There is no field type with type " + field.type);
            }
        }
    }
})();
