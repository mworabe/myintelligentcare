﻿using FluentValidation;

namespace PatientCare.Web.Api.Models.ClinicLocations
{
    public class ClinicLocationRequestValidato : AbstractValidator<ClinicLocationRequest>
    {
        public ClinicLocationRequestValidato()
        {
            RuleFor(model => model.LocationPrefix)
                .NotEmpty()
                .WithMessage("Prefix is Required.");

            RuleFor(model => model.Name)
             .NotEmpty()
             .WithMessage("Location is Required.");
        }
    }
}