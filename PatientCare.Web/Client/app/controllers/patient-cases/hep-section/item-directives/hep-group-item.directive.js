﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('hepGroupItem', function ($compile) {
    return {
        scope: {
            item: "=data",
            moveUp: "&",
            moveDown: "&",
            isFirst: "=",
            isLast: "=",
            isToShowCheck: "=",
            editGroupFn: '&',
            deleteGroupFn: '&',
            unGroupItemFn: '&'
        },

        templateUrl: "Client/app/controllers/patient-cases/hep-section/item-directives/hep-group-item.html",
        controller: ['$scope', 'Modal', 'ConstantFacory', function ($scope, modal, ConstantFacory) {
            $scope.timeUnit = [
               { id: 1, title: "Seconds" },
               { id: 2, title: "Minutes" },
               { id: 3, title: "Hours" }
            ]

            $scope.timeMeasurements = ConstantFacory.getTimeUnit();
            $scope.weightMeasurements = ConstantFacory.getWeightUnit();
            $scope.frequencyList = ConstantFacory.getFrequencies();
            $scope.resistanceList = ConstantFacory.getResistances();
            $scope.visible = false;
            $scope.othersvisible = false;
            $scope.myfilteringData = {};
            $scope.myFirstGroup = {};
            $scope.exVisibility = function () {
                if ($scope.visible) {
                    $scope.visible = false;
                }
                else {
                    $scope.visible = true;
                }
            }

            $scope.myChanges = function () {
                if (_.isEqual($scope.myfilteringData.frequenyy,'Other'))
                {
                    $scope.othersvisible = true;
                }
            }

            $scope.editExercises = function (itemss) {
                _.forEach($scope.item.hepDetails, function (items) {
                    items.sets = $scope.myfilteringData.setts;
                    items.reps = $scope.myfilteringData.repps;
                    items.time = $scope.myfilteringData.timme;
                    items.timeUnit = $scope.myfilteringData.tttt;
                    items.weight = $scope.myfilteringData.weightt;
                    items.weightUnit = $scope.myfilteringData.weighttUnits;
                    items.holds = $scope.myfilteringData.holdds;
                    items.holdsUnit = $scope.myfilteringData.holdinggsUnit;
                    items.resistance = $scope.myfilteringData.resistancessss;
                    items.frequency = $scope.myfilteringData.frequenyy;

                    if (_.isEqual(items.frequency, "1x Day"))
                    {
                        _.forEach(items.frequencies, function (ff) {
                            ff.exerciseFrequency = 1;
                          });
                    }
                    else if (_.isEqual(items.frequency, "2x Day")) {
                        _.forEach(items.frequencies, function (ff) {
                            ff.exerciseFrequency = 2;
                        });
                    }
                    else if (_.isEqual(items.frequency, "3x Day")) {
                        _.forEach(items.frequencies, function (ff) {
                            ff.exerciseFrequency = 3;
                        });
                    }
                    else if (_.isEqual(items.frequency, '4x Day')) {
                        _.forEach(items.frequencies, function (ff) {
                            ff.exerciseFrequency = 4;
                        });
                    }
                    else if (_.isEqual(items.frequency, '5x Day')) {
                        _.forEach(items.frequencies, function (ff) {
                            ff.exerciseFrequency = 5;
                        });
                    }
                    else if (_.isEqual(items.frequency, "Other")) {
                        items.frequencies[0].exerciseFrequency = $scope.myFirstGroup.sunday;
                        items.frequencies[1].exerciseFrequency = $scope.myFirstGroup.monday;
                        items.frequencies[2].exerciseFrequency = $scope.myFirstGroup.tuesday;
                        items.frequencies[3].exerciseFrequency = $scope.myFirstGroup.wednesday;
                        items.frequencies[4].exerciseFrequency = $scope.myFirstGroup.thursday;
                        items.frequencies[5].exerciseFrequency = $scope.myFirstGroup.friday;
                        items.frequencies[6].exerciseFrequency = $scope.myFirstGroup.saturday;
                    }

                });
                $scope.visible = false;
                $scope.othersvisible = false;
            }


            function findInList(item) {
                if ($scope.item.hepDetails) {
                    for (var i = 0; i < $scope.item.hepDetails.length > 0; i++) {
                        var obj = $scope.item.hepDetails[i];
                        if (item.id || item.tempId==0 && item.id == obj.id) { //changed
                            return i;
                        }
                    }
                }
                return -1;
            }

            $scope.deleteNormalItem = function (item) {
                modal.confirm.delete(function () {
                    var index = findInList(item);
                    if (index > -1) {
                        $scope.item.hepDetails.splice(index, 1);
                        $scope.arrageItems();
                        $scope.sortList();
                    }
                })("Warning", "Item");
            }

            $scope.sortList = function (item) {
                if (item.hepDetails) {
                    item.hepDetails = $filter('orderBy')(item.hepDetails, 'exerciseOrder');
                }
            }

            $scope.arrageItems = function (item) {
                if (item.hepDetails) {
                    for (var i = 0; i < item.hepDetails.length; i++) {
                        item.hepDetails[i].exerciseOrder = i;
                    }
                }
            }


            $scope.moveGroupUp = function (index) {
                if (index > 0) {
                    var newIndex = index - 1;
                    var currentObj = $scope.item.hepDetails[index];
                    $scope.item.hepDetails[index] = $scope.item.hepDetails[newIndex];
                    $scope.item.hepDetails[newIndex] = currentObj;
                    $scope.item.hepDetails[index].exerciseOrder = index;
                    $scope.item.hepDetails[newIndex].exerciseOrder = newIndex;
                    $scope.sortList();
                }
            }

            $scope.moveGroupDown = function (index) {
                if (index < ($scope.item.hepDetails.length - 1)) {
                    var newIndex = index + 1;
                    var currentObj = $scope.item.hepDetails[index];
                    $scope.item.hepDetails[index] = $scope.item.hepDetails[newIndex];
                    $scope.item.hepDetails[newIndex] = currentObj;
                    $scope.item.hepDetails[index].exerciseOrder = index;
                    $scope.item.hepDetails[newIndex].exerciseOrder = newIndex;
                    $scope.sortList();
                }
            }

            function setUngroupData(exerciseItem) {
                var copyOfItem = angular.copy(exerciseItem);
                if (copyOfItem) {
                    copyOfItem.id = 0;
                    copyOfItem.groupId = null;
                    copyOfItem.type = "Normal";
                    if (copyOfItem.frequencies) {
                        angular.forEach(copyOfItem.frequencies, function (freq) {
                            freq.id = 0;
                            freq.hepDetailId = 0;
                        });
                    }
                }
                return copyOfItem;
            }

            $scope.unGroupItem = function (exerciseItem) {
                if ($scope.unGroupItemFn) {
                    $scope.unGroupItemFn({ dataObj: setUngroupData(exerciseItem) });
                    var index = findInList(exerciseItem);
                    if (index > -1) {
                        $scope.item.hepDetails.splice(index, 1);
                        $scope.arrageItems();
                        $scope.sortList();
                    }                   
                    if ($scope.item.hepDetails.length == 0) {
                        $scope.deleteGroupFn({ obj: $scope.item })
                    }
                }
            }

            $scope.unGroup = function () {
                if ($scope.item && $scope.item.hepDetails) {
                    angular.forEach($scope.item.hepDetails, function (exItem) {
                        $scope.unGroupItemFn({ dataObj: setUngroupData(exItem) });
                    });
                    $scope.deleteGroupFn({ obj: $scope.item })
                }
            }

            $scope.deleteGroup = function () {
                modal.confirm.delete(function () {
                    $scope.deleteGroupFn({ obj: $scope.item })
                })("Warning", "Group");

            }

        }]
    }
});



