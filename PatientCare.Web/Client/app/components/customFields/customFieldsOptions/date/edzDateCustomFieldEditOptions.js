﻿"use strict";

(function edzDateCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzDateCustomFieldEditOptions", [
            edzDateCustomFieldOptions
        ]);

    function edzDateCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/date/edzDateCustomFieldEdit.html",
            restrict: "E",
            bindToController: {
                model: "=model"
            },
            controller: ["$scope", function edzAddCustomFieldCtrl($scope) {
            }],
            controllerAs: "edzDateCustomFieldEditOptionsCtrl"
        };
    }
})();
