﻿
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("ChatService", ["$resource", function ($resource) {
        return $resource("api/Calender/MyTimeOff", null, {
            getTwilioToken: {
                method: "GET",
                url: "api/mobile/v1/auth/twilio-token"
            },
            getChatContacts: {
                method: "GET",
                url: "api/mobile/v1/chatuserlist?type=all"
            },
            getChatSession: {
                method: "GET",
                url: "api/mobile/v1/auth/chat-session-list"
            },
            checkSessionExist: {
                method: "GET",
                url: "api/mobile/v1/auth/chat-session"
            },
            createNewSession: {
                method: "POST",
                url: "api/mobile/v1/auth/chat-session"
            },
            //ChannelsId
            getSessionByChannelId: {
                method: "GET",
                url: "api/mobile/v1/auth/chat-session-detail"
            },

            getUserKeys: {
                method: "GET",
                url: "api/mobile/v1/auth/chat-user_key",
                isArray: true
            },
            
            




        });
    }]);