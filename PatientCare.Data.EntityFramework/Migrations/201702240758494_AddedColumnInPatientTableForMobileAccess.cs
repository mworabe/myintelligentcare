namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedColumnInPatientTableForMobileAccess : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "is_mobile_access", c => c.Boolean(defaultValue: false));
            AddColumn("dbo.patients", "photo_file_name", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.patients", "photo_file_name");
            DropColumn("dbo.patients", "is_mobile_access");
        }
    }
}
