﻿
"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("InvoiceConfig", ["$resource", function ($resource) {
      var resource = $resource("api/InvoiceConfig/:id/:controller",
        null, //parameters default
        {
            'update': { method: "PUT" },
            'save': { method: "POST" },
            'query': { method: "GET", params: {}, isArray: false }
        });

      resource.logoSrc = "api/InvoiceConfig/UploadPhoto";
      resource.uploadLogo = function (uploader, file, configId, cb) {
          cb = cb || angular.noop;
          var url = this.logoSrc + "?id=" + configId;
          uploader.upload({
              url: url,
              data: { file: file }
          }).then(cb);
      };


      return resource;
  }]);
