﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceWorkExperienceRepository : RepositoryBase<ResourceWorkExperience>, IResourceWorkExperienceRepository
    {
        public ResourceWorkExperienceRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
