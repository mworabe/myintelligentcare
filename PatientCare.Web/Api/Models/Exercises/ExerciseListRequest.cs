﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Exercises
{
    public class ExerciseListRequest : ListRequest
    {
        [DataMember(Name = "keywords")]
        public string Keywords { get; set; }

        [DataMember(Name = "regionid")]
        public string RegionId { get; set; }

        [DataMember(Name = "activityid")]
        public string ActivityId { get; set; }

        [DataMember(Name ="set")]
        public string set { get; set; }

        [DataMember(Name ="reps")]
        public string reps { get; set; }

        [DataMember(Name ="holds")]
        public string holds { get; set; }

        [DataMember(Name ="weight")]
        public string weight { get; set; }

        [DataMember(Name ="weightUnit")]
        public string weightUnit { get; set; }

        [DataMember(Name ="holdUnit")]
        public string holdUnit { get; set; }

        [DataMember(Name ="frequency")]
        public string frequency { get; set; }
    }
}