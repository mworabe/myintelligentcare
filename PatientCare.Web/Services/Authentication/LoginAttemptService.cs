﻿using System;
using System.Configuration;

namespace PatientCare.Web.Services.Authentication
{
    public class LoginAttemptService
    {
        public static readonly int LoggingFailedAttemptNo = Convert.ToInt32(ConfigurationManager.AppSettings["LoggingFailedAttemptNo"] ?? "0");
        public static readonly int LoggingAttemptTimeIntervalMinit = Convert.ToInt32(ConfigurationManager.AppSettings["LoggingAttemptTimeIntervalMinit"] ?? "0");
        public static readonly int ForgotPasswordLinkValidTime = Convert.ToInt32(ConfigurationManager.AppSettings["forgotPasswordLinkValidTime"] ?? "0");
        public static readonly int ApplicationSessionExpiryTime = Convert.ToInt32(ConfigurationManager.AppSettings["applicationSessionExpiryTime"] ?? "30");
        public static readonly int ApplicationRefreshTokenExpiryTime = Convert.ToInt32(ConfigurationManager.AppSettings["applicationRefreshTokenExpiryTime"] ?? (ApplicationSessionExpiryTime + 10).ToString());

        public static readonly int MobileApplicationSessionExpiryTime = Convert.ToInt32(ConfigurationManager.AppSettings["mobileApplicationSessionExpiryTime"] ?? "30");
        public static readonly int MobileApplicationRefreshTokenExpiryTime = Convert.ToInt32(ConfigurationManager.AppSettings["mobileApplicationRefreshTokenExpiryTime"] ?? (ApplicationSessionExpiryTime + 10).ToString());

        public LoginAttemptService()
        {

        }
    }
}