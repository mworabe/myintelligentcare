﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http.Routing;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.Api.Models.Account;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PatientCare.Web.Models
{
    public class ModelFactory
    {

        private readonly UrlHelper _urlHelper;
        private readonly ApplicationUserManager _appUserManager;

        public ModelFactory(HttpRequestMessage request, ApplicationUserManager appUserManager)
        {
            _urlHelper = new UrlHelper(request);
            _appUserManager = appUserManager;
        }

        public void SetApplicationUserModel(ApplicationUser model ,AccountRequest request)
        {
            model.Id = request.Id;
            model.UserName = request.UserName;
            model.ResourceId = request.ResourceId;
        }

        public UserReturnModel Create(ApplicationUser appUser)
        {
            return new UserReturnModel
            {
                Url = _urlHelper.Link("GetUserById", new { id = appUser.Id }),
                Id = appUser.Id,
                UserName = appUser.UserName,
                //FullName = string.Format("{0} {1}", appUser.FirstName, appUser.LastName),
                //Email = appUser.Email,
                //EmailConfirmed = appUser.EmailConfirmed,
                //Level = appUser.Level,
                //JoinDate = appUser.JoinDate,
                Roles = _appUserManager.GetRoles(appUser.Id)
                //Claims = _AppUserManager.GetClaimsAsync(appUser.Id).Result
            };

        }

        public RoleReturnModel Create(ApplicationRole appRole)
        {

            return new RoleReturnModel
            {
                Url = _urlHelper.Link("GetRoleById", new { id = appRole.Id }),
                Id = appRole.Id,
                Name = appRole.Name
            };

        }
    }

    public class UserReturnModel
    {
        public string Url { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        //public bool EmailConfirmed { get; set; }
        //public int Level { get; set; }
        //public DateTime JoinDate { get; set; }
        public IList<string> Roles { get; set; }
        //public IList<System.Security.Claims.Claim> Claims { get; set; }

    }

    public class RoleReturnModel
    {
        public string Url { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }
}