﻿using PatientCare.Core.Entities;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceScoreFilters
{
    public class ResourceScoreFilterRequestValidator : AbstractValidator<ResourceScoreFilterRequest>
    {
        public ResourceScoreFilterRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);

            RuleFor(model => model.JsonFilter)
                .NotEmpty();
        }
    }
}