﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.PatientWorkoutAnalytics
{
    [DataContract]
    public class PatientWorkoutAnalyticResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public long? CaseId { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public int? PreExercisesPainRating { get; set; }

        [DataMember]
        public int? WorkoutFeelResult { get; set; }

        [DataMember]
        public string WorkoutComment { get; set; }
    }
}