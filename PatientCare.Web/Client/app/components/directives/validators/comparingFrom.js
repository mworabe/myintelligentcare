// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.
//
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngComparingFrom", [
        function () {
            return {
                restrict: "A",
                scope: {
                  'ngComparingFrom': '='
                },
                require: "ngModel",
                link: function ($scope, $elem, $attrs) {
                    $scope.$watch('ngComparingFrom', function(newValue) {
                        if(newValue === undefined) {
                            $scope.ngComparingFrom = parseInt($attrs.min);
                        }
                    });
                }
            };
        }
    ]);