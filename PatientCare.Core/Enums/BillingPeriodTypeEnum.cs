﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Billing Period Type Enum.
    /// </summary>
    public enum BillingPeriodTypeEnum
    {
        Hourly,
        Daily,
        Weekly,
        Monthly,
        TiedToAMilestone
    }
}
