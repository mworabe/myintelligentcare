﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.DashboardSetups
{
    [DataContract]
    public class DashboardSetupListResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string SetupJson { get; set; }

        [DataMember]
        public string RoleName { get; set; }

        [DataMember]
        public string RoleId { get; set; }
    }
}