﻿using System;

namespace PatientCare.Web.Api.Models.ResourceBehaviouralDetails
{
    public class ResourceBehaviouralDetailRequest
    {
        public long Id { get; set; }
        
        public string Behaviour { get; set; }
        
        public long? ResourceId { get; set; }
    }
}