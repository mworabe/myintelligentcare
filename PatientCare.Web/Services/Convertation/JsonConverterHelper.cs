﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace PatientCare.Web.Services.Convertation
{
    public static class JsonConverterHelper
    {
        private static readonly JsonSerializerSettings JsonSerializerSettings;
        static JsonConverterHelper()
        {

            JsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            JsonSerializerSettings.Converters.Add(new StringEnumConverter());
        }

        public static string SerializeObject(object obj)
        {
            return JsonConvert.SerializeObject(obj, JsonSerializerSettings);
        }
    }
}