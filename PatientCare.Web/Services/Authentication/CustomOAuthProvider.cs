﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using PatientCare.Data.EntityFramework.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using PatientCare.Web.Api.Controllers;
using PatientCare.Core.Entities;
using System;
using System.Collections.Generic;


namespace PatientCare.Web.Services.Authentication
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //IECode allow = new IECode();
            //bool? status = allow.GetStatus(null);
        
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

            if (allowedOrigin == null) allowedOrigin = "*";
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            //var allowedOrigin = "*";
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            var roleManager = context.OwinContext.GetUserManager<ApplicationRoleManager>();

            var user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            if (user.Resource != null && (user.Resource.Active == null || user.Resource.Active == false))
            {
                context.SetError("invalid_grant", "Resource is inactive, contact to administrator.");
                return;
            }

            if (user != null)
            {
                LoginHistory temp = new LoginHistory();
                bool isExpiry = false;
                isExpiry = temp.GetUserExpiryDate();

                if (isExpiry == true)
                {
                    context.SetError("invalid_grant", "The user trial period has expired.");
                    return;
                }
            }

            var oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, "JWT");

            var props = new AuthenticationProperties();

            LoginHistory controller = new LoginHistory();
            UserLoginHistory userLoginHistoryRequest = new UserLoginHistory();
            userLoginHistoryRequest.UserId = user.Id;
            userLoginHistoryRequest.Action = "Login";
            userLoginHistoryRequest.LogDateTime = DateTime.Now;
            userLoginHistoryRequest.Username = user.UserName;
            controller.CreateUserLoginHistory(userLoginHistoryRequest);

            var roleLink = user.Roles.FirstOrDefault();
            //if (status == true)
                if (roleLink != null)
                {
                    var role = await roleManager.FindByIdAsync(roleLink.RoleId);
                    if (role != null)
                    {
                        if (role.Name != ApplicationUser.PatientRole)
                        {
                            string json = controller.ModificationInApplicationRoleOfActivity(role);
                            props.Dictionary.Add("user-rules", json);

                            string configurationValues = "[]";
                            if (user.ResourceId != null)
                                configurationValues = controller.GetUserUiConfiguration(user.ResourceId);
                            props.Dictionary.Add("ui-configuration", configurationValues);

                            props.Dictionary.Add("sessionExpirationTimeInMinit", LoginAttemptService.ApplicationSessionExpiryTime.ToString());
                            props.Dictionary.Add("refreshTokenExpirationTimeInMinit", LoginAttemptService.ApplicationRefreshTokenExpiryTime.ToString());

                            if (user.ResourceId.HasValue)
                            {
                                long? ClinicId = user.Resource.ClinicId;
                                long? ClinicLocationId = user.Resource.ClinicLocationId;

                                oAuthIdentity.AddClaim(new Claim("ResourceId", user.ResourceId.ToString()));
                                oAuthIdentity.AddClaim(new Claim("ResourceName", user.Resource.Name));
                                oAuthIdentity.AddClaim(new Claim("ResourceFirstName", user.Resource.FirstName));
                                oAuthIdentity.AddClaim(new Claim("ResourceLastName", user.Resource.LastName));
                                if (ClinicId != null)
                                {
                                    props.Dictionary.Add("clinicId", ClinicId.ToString());
                                    oAuthIdentity.AddClaim(new Claim("ClinicId", ClinicId.ToString()));
                                }

                                if (ClinicLocationId != null)
                                {
                                    props.Dictionary.Add("clinicLocationId", ClinicLocationId.ToString());
                                    oAuthIdentity.AddClaim(new Claim("ClinicLocationId", ClinicLocationId.ToString()));
                                }
                            }

                            bool? isNDA = user.IsNDA;
                            if (isNDA != null)
                            {
                                props.Dictionary.Add("isNDA", isNDA.ToString());
                                oAuthIdentity.AddClaim(new Claim("IsNDA", isNDA.ToString()));
                            }
                            else
                            {
                                bool? tempIsNDA = false;
                                props.Dictionary.Add("isNDA", tempIsNDA.ToString());
                                oAuthIdentity.AddClaim(new Claim("IsNDA", tempIsNDA.ToString()));
                            }
                        }
                        else
                        {
                            context.SetError("invalid_grant", "The user name or password is incorrect.");
                            return;
                        }
                    }
                }

            var ticket = new AuthenticationTicket(oAuthIdentity, props);
            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            //if (context.Properties.Dictionary.ContainsKey("user-rules"))
            //    context.AdditionalResponseParameters.Add("user-rules", context.Properties.Dictionary["user-rules"]);

            //if (context.Properties.Dictionary.ContainsKey("ui-configuration"))
            //    context.AdditionalResponseParameters.Add("ui-configuration", context.Properties.Dictionary["ui-configuration"]);

            //#region Set Patient Information

            //if (context.Properties.Dictionary.ContainsKey("ResourceId"))
            //    context.AdditionalResponseParameters.Add("ResourceId", context.Properties.Dictionary["ResourceId"]);

            //if (context.Properties.Dictionary.ContainsKey("ResourceName"))
            //    context.AdditionalResponseParameters.Add("ResourceName", context.Properties.Dictionary["ResourceName"]);

            //if (context.Properties.Dictionary.ContainsKey("ResourceFirstName"))
            //    context.AdditionalResponseParameters.Add("ResourceFirstName", context.Properties.Dictionary["ResourceFirstName"]);

            //if (context.Properties.Dictionary.ContainsKey("ResourceLastName"))
            //    context.AdditionalResponseParameters.Add("ResourceLastName", context.Properties.Dictionary["ResourceLastName"]);

            //#endregion

            return Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

            if (allowedOrigin == null) allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            //validate your client
            //var currentClient = context.ClientId;

            //if (Client does not match)
            //{
            //    context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
            //    return Task.FromResult<object>(null);
            //}

            // Change authentication ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

    }
}