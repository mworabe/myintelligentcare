
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("schedules", {
                url: "/schedules",
                templateUrl: "Client/app/controllers/schedules/schedules.html",
                controller: "ScheduleCtrl as ScheduleCtrl",
                data: {
                    security: {
                        authenticated: true
                    }
                },
                resolve: {
                    initialData: ["$q", 'ClinicService', 'ClinicianService', 'ClinicLocationService', function ($q, ClinicService, ClinicianService, ClinicLocationService) {
                        function loadClinics() {
                            var defer = $q.defer();
                            ClinicService.all().$promise.then(function (response) {
                                defer.resolve(response);
                            });
                            return defer.promise;
                        }
                        function loadClinician() {
                            var defer = $q.defer();
                            ClinicianService.all().$promise.then(function (response) {
                                defer.resolve(response);
                            });
                            return defer.promise;
                        }

                        function loadClinicians() {
                            var defer = $q.defer();
                            ClinicLocationService.all().$promise.then(function (response) {
                                defer.resolve(response);
                            });
                            return defer.promise;
                        }
                        return $q.all([loadClinics(), loadClinician(), loadClinicians()]).then(function (resolutions) { return resolutions; });
                    }]
                }
            }).state("schedules.patients-add", {
                url: "/add",
                params: {},
                data: {
                    security: {
                        authenticated: true
                    }
                },
                onEnter: ['$state', '$uibModal',
                    function ($state, $uibModal) {
                        $uibModal.open({
                            templateUrl: "Client/app/controllers/patients/patients-add-model.html",
                            controller: "PatientEditCtrl as PatientEditCtrl",
                            size: "large-modal",
                            resolve: {
                                customFieldsResource: [
                                    "$q", "$stateParams", "CustomField", "RoleSetup", "patientService", 'userTimeZoneFactory', "SystemFunctions",
                                    function loadResourceInitialData($q, $stateParams, customFieldService, roleSetupService, patientService, userTimeZoneFactory, systemFunctions) {
                                        function loadPatient() {
                                            var defer = $q.defer();
                                            if (!$stateParams.id) {
                                                defer.resolve();
                                            } else {
                                                defer.resolve();
                                            }
                                            return defer.promise;
                                        }

                                        function loadTimeZone() {
                                            var defer = $q.defer();
                                            userTimeZoneFactory.getTimeZone(defer);
                                            return defer.promise;
                                        }

                                        function loadCustomFields() {
                                            var defer = $q.defer();
                                            defer.resolve();
                                            /* if ($stateParams.id) {
                                                 defer.resolve();
                                             } else {
                                                 customFieldService.customFieldValuesByResource().$promise.then(function (response) {
                                                     defer.resolve(response);
                                                 });
                                             }
                                             */
                                            return defer.promise;
                                        }

                                        function loadConfigLayout() {
                                            var defer = $q.defer();
                                            userTimeZoneFactory.getResourceDefaultConfigs(defer);
                                            return defer.promise;
                                        }
                                        return $q.all([loadCustomFields(), {}, loadPatient(), loadTimeZone()])
                                            .then(function (resolutions) {
                                                systemFunctions.scrollToTopZero();
                                                return resolutions;
                                            });
                                    }
                                ]
                            }
                        }).result.then(function (result) {

                        }, function () {

                        }
                        )
                    }
                ]
            })
    });