﻿using PatientCare.Web.ApiMobile.Models.ChatMembers;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.ChatSessions
{
    [DataContract]
    public class ChatKeyResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string PrivateKey { get; set; }
      
    }
}