﻿namespace PatientCare.Web.Api.Models.ResourceRecognitions
{
    public class ResourceRecognitionRequest
    {
        public long Id { get; set; }
        
        public string Recognition { get; set; }
        
        public long? ResourceId { get; set; }
    }
}