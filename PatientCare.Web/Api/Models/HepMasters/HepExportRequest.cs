﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.HepDetails;
using System;

namespace PatientCare.Web.Api.Models.HepMasters
{
    public class HepExportRequest
    {
        public HepExportStatusEnum ? type { get; set; }
        public long? caseId { get; set; }
        public string patientId { get; set; }
    }   
}