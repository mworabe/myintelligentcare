﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PatientCare.Web.Api.Models.Resources;
using OfficeOpenXml;

namespace PatientCare.Web.Services.Excel
{
    public static class ResourceExport
    {
        public static Stream Export(IList<ResourceResponse> resourceDtos, string title)
        {
            using (var excelPackage = new ExcelPackage(new MemoryStream()))
            {
                excelPackage.Workbook.Properties.Author = "IRMS";
                excelPackage.Workbook.Properties.Title = title;

                var worksheet = excelPackage.Workbook.Worksheets.Add("Resources");

                ApplyHeader(worksheet);
                
                for (var i = 0; i < resourceDtos.Count; i++)
                {
                    FillUpTheRow(worksheet, resourceDtos[i], i + 2);
                }


                excelPackage.Save();
                return excelPackage.Stream;
            }
        }

        private static void ApplyHeader(ExcelWorksheet worksheet)
        {
            worksheet.Cells[1, 1].Value = ExportImportExtensions.NeedToChangeColumnName;
            worksheet.Cells[1, 1].Style.Font.Bold = true;
            for (int i = 0; i < ExportImportExtensions.ResourceHeaders.Length; i++)
            {
                worksheet.Cells[1, i + 2].Value = ExportImportExtensions.ResourceHeaders[i];
                worksheet.Cells[1, i + 2].Style.Font.Bold = true;
            }
        }

        private static void FillUpTheRow(ExcelWorksheet worksheet, ResourceResponse resourceDto, int i)
        {
            const int shift = 1;
            worksheet.Cells[i, 1].Value = ExportImportExtensions.NoNeedToChangeIndicator;

            //var timeZoneNames = TimeZoneInfo.GetSystemTimeZones().Select(x => new { name = x.DisplayName, id = x.Id }).ToList();
            var timeZoneNames = TimeZoneData.GetTimeZoneData();
            var timeZoneInfo = timeZoneNames.FirstOrDefault(x => x.id.Equals(resourceDto.TimeZoneInfo, StringComparison.InvariantCultureIgnoreCase));

            worksheet.Cells[i, 1 + shift].Value = resourceDto.FirstName;
            worksheet.Cells[i, 2 + shift].Value = resourceDto.LastName;
            worksheet.Cells[i, 3 + shift].Value = (resourceDto.JobTitle != null) ? resourceDto.JobTitle.Name : string.Empty;
            worksheet.Cells[i, 4 + shift].Value = resourceDto.Address;
            worksheet.Cells[i, 5 + shift].Value = (resourceDto.City != null) ? resourceDto.City.Name : string.Empty;
            worksheet.Cells[i, 6 + shift].Value = (resourceDto.State != null) ? resourceDto.State.Name : string.Empty;            
            worksheet.Cells[i, 8 + shift].Value = resourceDto.YearsEmployed;
            worksheet.Cells[i, 9 + shift].Value = resourceDto.GradeLevel;

            worksheet.Cells[i, 10 + shift].Value = resourceDto.YearsOfCollege;
            worksheet.Cells[i, 11 + shift].Value = resourceDto.CollegeDegree;
            worksheet.Cells[i, 12 + shift].Value = timeZoneInfo != null ? timeZoneInfo.name : string.Empty;
            worksheet.Cells[i, 13 + shift].Value = resourceDto.Location;
            worksheet.Cells[i, 14 + shift].Value = resourceDto.ValidPassport;
            worksheet.Cells[i, 15 + shift].Value = resourceDto.AvailableToTravel;
            worksheet.Cells[i, 16 + shift].Value = resourceDto.EverBeenConvictedOfACrime;
            worksheet.Cells[i, 17 + shift].Value = resourceDto.InformationCertifiedByEmployer;
            worksheet.Cells[i, 18 + shift].Value = resourceDto.Ethnicity;
            worksheet.Cells[i, 19 + shift].Value = resourceDto.ArmedForces;

            worksheet.Cells[i, 20 + shift].Value = resourceDto.ArmedForcesBranch;
            worksheet.Cells[i, 21 + shift].Value = resourceDto.DrugTest;
            worksheet.Cells[i, 22 + shift].Value = resourceDto.CreditReportOk;
            worksheet.Cells[i, 23 + shift].Value = resourceDto.ProofOfCitizenship;
            worksheet.Cells[i, 24 + shift].Value = resourceDto.HourlyRate;
            worksheet.Cells[i, 25 + shift].Value = (resourceDto.ResourceTeam != null) ? resourceDto.ResourceTeam.Name : string.Empty;
            worksheet.Cells[i, 26 + shift].Value = BuildSsnPretty(resourceDto.Ssn);
            worksheet.Cells[i, 27 + shift].Value = resourceDto.EmployeeId;
            worksheet.Cells[i, 28 + shift].Value = resourceDto.Range.ToString();
            worksheet.Cells[i, 29 + shift].Value = (resourceDto.Division != null) ? resourceDto.Division.Name : string.Empty;

            worksheet.Cells[i, 30 + shift].Value = (resourceDto.Department != null) ? resourceDto.Department.Name : string.Empty;
            worksheet.Cells[i, 31 + shift].Value = resourceDto.PhotoFileName;
            worksheet.Cells[i, 32 + shift].Value = resourceDto.CvFileName;
            worksheet.Cells[i, 33 + shift].Value = resourceDto.WorksWellAlone;
            worksheet.Cells[i, 34 + shift].Value = resourceDto.WorksWellInTeam;
            worksheet.Cells[i, 35 + shift].Value = resourceDto.EmploymentType.ToString();
            worksheet.Cells[i, 36 + shift].Value = resourceDto.Productivity;
            worksheet.Cells[i, 37 + shift].Value = resourceDto.Rating;
            worksheet.Cells[i, 38 + shift].Value = resourceDto.MotorVehicleReport;
            worksheet.Cells[i, 39 + shift].Value = resourceDto.MotorVehicleReportOther;

            worksheet.Cells[i, 40 + shift].Value = resourceDto.ProfessionalReferenceChecks;
            worksheet.Cells[i, 41 + shift].Value = resourceDto.ProfessionalReferenceChecksOther;
            worksheet.Cells[i, 42 + shift].Value = resourceDto.EmploymentEligibilityVerification;
            worksheet.Cells[i, 43 + shift].Value = resourceDto.EmploymentEligibilityVerificationOther;
            worksheet.Cells[i, 44 + shift].Value = resourceDto.InternationalWorkHistory;
            worksheet.Cells[i, 45 + shift].Value = resourceDto.InternationalWorkHistoryOther;
            worksheet.Cells[i, 46 + shift].Value = resourceDto.CredentialVerifications;
            worksheet.Cells[i, 47 + shift].Value = resourceDto.CredentialVerificationsOther;
            worksheet.Cells[i, 48 + shift].Value = (resourceDto.Country != null) ? resourceDto.Country.Name : string.Empty;
            worksheet.Cells[i, 49 + shift].Value = (resourceDto.ArmedForcesCountry != null) ? resourceDto.ArmedForcesCountry.Name : string.Empty;

            worksheet.Cells[i, 50 + shift].Value = resourceDto.FormI9;
            worksheet.Cells[i, 51 + shift].Value = resourceDto.FormI9Other;
            worksheet.Cells[i, 52 + shift].Value = resourceDto.FormEVerify;
            worksheet.Cells[i, 53 + shift].Value = resourceDto.FormEVerifyOther;
            worksheet.Cells[i, 54 + shift].Value = resourceDto.StartDate;
            worksheet.Cells[i, 54].Style.Numberformat.Format = "yyyy-mm-dd";
            worksheet.Cells[i, 55 + shift].Value = resourceDto.EndDate;
            worksheet.Cells[i, 55].Style.Numberformat.Format = "yyyy-mm-dd";
            worksheet.Cells[i, 56 + shift].Value = resourceDto.Gender;
            worksheet.Cells[i, 57 + shift].Value = resourceDto.MartialStatus;
            worksheet.Cells[i, 58 + shift].Value = resourceDto.BirthDate;
            worksheet.Cells[i, 58].Style.Numberformat.Format = "yyyy-mm-dd";
            worksheet.Cells[i, 59 + shift].Value = resourceDto.OfficeNumber;

            worksheet.Cells[i, 60 + shift].Value = resourceDto.ResourceType;
            worksheet.Cells[i, 61 + shift].Value = resourceDto.BonusOrOtherPay;
            worksheet.Cells[i, 62 + shift].Value = resourceDto.Address2;
            worksheet.Cells[i, 63 + shift].Value = resourceDto.ZipCode;
            worksheet.Cells[i, 64 + shift].Value = resourceDto.Salary;
            worksheet.Cells[i, 65 + shift].Value = resourceDto.MobilePhoneNumber;
            worksheet.Cells[i, 66 + shift].Value = resourceDto.EmergencyContactName;
            worksheet.Cells[i, 67 + shift].Value = resourceDto.EmergencyContactPhoneNumber;
            worksheet.Cells[i, 68 + shift].Value = resourceDto.Relationship;
            worksheet.Cells[i, 69 + shift].Value = resourceDto.Supervisor == null ? string.Empty : resourceDto.Supervisor.Name;
            worksheet.Cells[i, 70 + shift].Value = (resourceDto.Active == null || resourceDto.Active == false) ? false : true;
        }

        private static string BuildSsnPretty(string value)
        {
            var val = (value ?? string.Empty);
            if (val.Length < 10)
            {
                return value;
            }
            return string.Format("{0}-{1}-{2}", val.Substring(0, 3), val.Substring(3, 2), val.Substring(5, 4));
        }
    }

}