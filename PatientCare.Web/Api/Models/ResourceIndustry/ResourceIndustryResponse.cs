﻿using PatientCare.Web.Api.Models.DropDownConfigs;
using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceIndustry
{
    [DataContract]
    public class ResourceIndustryResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public PicklistResponse Resource { get; set; }

        [DataMember]
        public long? IndustryId { get; set; }

        [DataMember]
        public DropDownConfigResponse Industry { get; set; }

        [DataMember]
        public string Name { get; set; }
        //{
        //    get { return (Industry != null ? Industry.Name : ""); }
        //    set { Name = value; }
        //}

        [DataMember]
        public string DropDownType { get; set; }
        //{
        //    get { return (Industry != null ? Industry.DropDownType : ""); }
        //    set { DropDownType = value; }
        //}

        [DataMember]
        public string ConfigField1 { get; set; }
        //{
        //    get { return (Industry != null ? Industry.ConfigField1 : ""); }
        //    set { ConfigField1 = value; }
        //}

        //[DataMember]
        //public string Name { get { return Industry != null ? Industry.Name : string.Empty; } }
        //[DataMember]
        //public string DropDownType { get { return Industry != null ? Industry.DropDownType : string.Empty; } }
        //[DataMember]
        //public string ConfigField1 { get { return Industry != null ? Industry.ConfigField1 : string.Empty; } }
    }
}