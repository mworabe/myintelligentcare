﻿using System;
using System.Data.Entity;
using PatientCare.Core.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class UiConfigurationMappingExtensions
    {
        public static DbModelBuilder MapUiConfiguration(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<UiConfiguration>().ToTable("ui_configurations").HasKey(i => i.Id);
            
            modelBuilder.Entity<UiConfiguration>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<UiConfiguration>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<UiConfiguration>()
               .Property(p => p.ResourceId)
               .HasColumnName("resource_id")
               .IsOptional();

            modelBuilder.Entity<UiConfiguration>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<UiConfiguration>()
               .Property(p => p.Key)
               .HasColumnName("key")
               .IsOptional()
               .HasMaxLength(255);

            modelBuilder.Entity<UiConfiguration>()
              .Property(p => p.Value)
              .HasColumnName("value")
              .IsOptional();
          
            return modelBuilder;
        }
    }
}
