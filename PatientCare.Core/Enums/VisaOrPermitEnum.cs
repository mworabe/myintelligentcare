﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Visa or Permit Enum.
    /// </summary>
    public enum VisaOrPermitEnum
    {
        Visa = 0,
        Permit = 1
    }
}
