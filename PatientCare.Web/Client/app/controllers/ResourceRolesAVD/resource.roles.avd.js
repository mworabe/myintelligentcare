﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("resourceRoleAVD", {
                url: "/resource/avd",
                params: { jobTitleId: null, jobTitleName: null, isDetailedMode: 0 },
                templateUrl: "Client/app/controllers/ResourceRolesAVD/resource-roles-avd-view.html",
                controller: "ResourceRoleAVDController as ResourceRoleAVDCtrl",
                data: {
                    requireLogin: true
                }
            })
        .state('resourceAvd', {
            url: "/resource/avd/{jobTitle}/{roleId:int}",
            params: { jobTitleId: null, jobTitleName: null, isDetailedMode: null },
            templateUrl: "Client/app/controllers/ResourceRolesAVD/resource-avd-view.html",
            controller: "ResourceAVDController as ResourceRoleAVDCtrl",
            data: {
                requireLogin: true
            }
        });
    });