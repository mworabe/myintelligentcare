﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.ClinicLocations;

namespace PatientCare.Web.Services.Mapping
{
    public static class ClinicLocationMapperExtensions
    {
        public static EmitMapperMapperProvider ClinicLocationRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ClinicLocation, ClinicLocationResponse>
                    (new ExtendedMapConfig<ClinicLocation, ClinicLocationResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ClinicLocation[], ClinicLocationResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ClinicLocation, ClinicLocationAutoCompleteResponse>
                    (new ExtendedMapConfig<ClinicLocation, ClinicLocationAutoCompleteResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ClinicLocation[], ClinicLocationAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ClinicLocationRequest, ClinicLocation>
                    (new ExtendedMapConfig<ClinicLocationRequest, ClinicLocation>()));

            return mapperProvider;
        }
    }
}