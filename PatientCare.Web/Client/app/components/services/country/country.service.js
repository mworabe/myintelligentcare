﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("Country", ["$resource", function ($resource) {
      var resource = $resource("api/Countries/:id/:controller",
        null,
            {
                'update': { method: "PUT" },
                'query': { method: "GET", params: {}, isArray: false }
            });

      resource.getTableOption = {
          pageName: "Countries",
          cols: function () {
              return [
                  { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                  { field: "name", title: "Name", sortable: "name", show: true, dataType: "text", required: true },
                  { field: "action", title: ".", dataType: "command" },
              ];
          }
      };
      return resource;
  }]);
