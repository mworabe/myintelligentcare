﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("NewCnfModelCntrl", [
		"$scope",
        'config',
        '$uibModalInstance',
		function ($scope, config, $uibModalInstance) {
		    $scope.config = config;
		    $scope.onYes = function () {
		        $uibModalInstance.dismiss($scope.myCroppedImage);
		    }
		    $scope.close = function () {
		        $uibModalInstance.dismiss("");
            }

		}
    ]);