﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("ImageCropCntrl", [
		"$scope",
		'toaster',
        'dataItem',
        '$uibModalInstance',
		function ($scope, toaster, dataItem, $uibModalInstance) {
		    
		    $scope.imageFile = dataItem.imgFile;


		    $scope.onFileSelect = function (file) {
		        $scope.imageFile = file;
		    }

		    $scope.save = function () {
		        $uibModalInstance.dismiss($scope.myCroppedImage);
                
		    }

		    $scope.close = function () {
		        $uibModalInstance.dismiss("");
            }

		}
    ]);