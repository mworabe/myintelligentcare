﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.DropDownConfigs;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class DropDownConfigMapperExtensions
    {
        public static EmitMapperMapperProvider DropDownConfigRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<DropDownConfig, DropDownConfigResponse>
                    (new ExtendedMapConfig<DropDownConfig, DropDownConfigResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<DropDownConfig[], DropDownConfigResponse[]>());

            mapperProvider.RegisterMapper(() =>
            ObjectMapperManager.DefaultInstance.GetMapper<DropDownConfig[], DropDownConfigPicklistResponse[]>());
            
            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<DropDownConfigRequest, DropDownConfig>
                    (new ExtendedMapConfig<DropDownConfigRequest, DropDownConfig>()));

            return mapperProvider;
        }
    }
}