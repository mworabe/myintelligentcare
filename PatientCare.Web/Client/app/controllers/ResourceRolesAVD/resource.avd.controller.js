﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ResourceAVDController", ["resourceRolesAVDService", "$scope", "$state", "DateTimeHelper", "toaster",
        "resourceAVDHelper", "NgTableParams", "SystemFunctions",
        ResourceAVDControllerFunc]);

function ResourceAVDControllerFunc(resourceRolesAVDService, $scope, $state, dateTimeHelper, toaster,
    resourceAVDHelper, ngTableParams, systemFunctionsService) {
    var self = this;

    self.jobTitleId = $state.params.roleId || 0;
    self.jobTitleName = $state.params.jobTitle;

    self.isDetailedMode = $state.params.isDetailedMode || false;
    self.isFormSubmitted = false;
    self.data = [];
    self.totalCount = undefined;
    self.selectedJobTitleId = undefined;
    self.timeRangeEnum = dateTimeHelper.dateTypeEnum();

    self.tableRequest = {};
    self.tableParams = {};
    self.pagination = {};
    self.queryParams = {};

    self.queryParams.pageIndex = 1;
    self.queryParams.pageSize = 20;
    //self.queryParams.sortType = "asc";

    self.tableParams = new ngTableParams({ count: 100 }, {
        counts: [],
        getData: function () {
            return [];
        }
    });




    self.tableRequest.start = moment().startOf('month');
    self.tableRequest.end = self.tableRequest.start.clone().add(1, "years").endOf('month');// moment().endOf('month');
    ////.subtract(1, "days");// moment().endOf('month');
    self.tableRequest.timeRange = "Month";

    function processServerData(data) {
        var groupByData = _.groupBy(data, "resourceId");
        return _.orderBy(groupByData, ["resourceName"], ["asc"]);
    }


    function onServerErrorCallback(response) {
        self.isFormSubmitted = false;
        var msg = "Unable to process your request, please try again later.";
        if (response.data && response.data.messageDetail)
            msg = response.data.messageDetail;
        toaster.pop("error", "", msg);
    }

    function onServerSuccessCallback(response) {
        self.isFormSubmitted = false;
        self.data = processServerData(response.data);
        self.pagination = systemFunctionsService.generatePaginationPageArray(response, self.queryParams.pageIndex, self.queryParams.pageSize);
        self.totalCount = response.itemsCount;
    }
    self.onResourceNameClick = function onResourceNameClick(row) {
        console.info(row);
        $state.go("resourceEdit", { id: row.resourceId, currentTab: "availability" });
    }
    self.onBackButtonClick = function onBackButtonClick() {
        $state.go("resourceRoleAVD", { reload: true });
    }

    /**
        Main Call to read Data and Initialize View.
        @_form == resourceAVDForm
    */
    function initializedResourceAVD(_form) {
        if (!_form) {
            return;
        }
        if (_form.$invalid) {
            toaster.pop("error", "", "Availability Filter Data is not Valid, please check missing required data.");
            return;
        }

        self.isFormSubmitted = true;
        if (!_form.$submitted)
            _form.$submitted = true;

        var tempModel = {};
        tempModel.start = self.tableRequest.start.format("LL");
        tempModel.end = self.tableRequest.end.format("LL");
        tempModel.timeRange = self.tableRequest.timeRange;
        tempModel.pageIndex = self.queryParams.pageIndex;
        tempModel.pageSize = self.queryParams.pageSize;
        tempModel.sortType = self.queryParams.sortType;
        tempModel.jobTitleId = self.jobTitleId;
        self.isPageBusy = resourceRolesAVDService.getResourceAVD(tempModel, onServerSuccessCallback, onServerErrorCallback);
    }

    self.calculateTotalResourceDemandPercentage = function calculateTotalResourceDemandPercentage(resourcesCount, totalDemandResource) {
        return ((totalDemandResource * 100) / resourcesCount).toFixed(2);
    }

    self.onPageChange = function (page) {
        systemFunctionsService.scrollToTopZero();
        self.queryParams.pageIndex = parseInt(page.number);
        initializedResourceAVD(resourceAVDForm);
    }

    /**
        Roles Utilization Functions
    */
    self.getRoleUtilization = function getRoleUtilization(col) {
        var demandResource = parseFloat(col.demandFromTasks);
        var availableResource = parseFloat(col.totalAvailableHours);
        var roleUtilization = ((demandResource * 100) / (demandResource + availableResource));
        return (isNaN(roleUtilization) || !roleUtilization) ? 0 : parseFloat(roleUtilization).toFixed(2);
    }
    self.isCriticalClass = function isCriticalClass(col) {
        return resourceAVDHelper.checkCritical(parseFloat(self.getRoleUtilization(col)));
    }
    self.isMediumClass = function isMediumClass(col) {
        return resourceAVDHelper.checkMedium(parseFloat(self.getRoleUtilization(col)));
    }

    self.onAllResourceButtonClick = function onAllResourceButtonClick() {
        $state.go("resource");
    }

    self.getEllipsesName = function getEllipsesName(name) {
        if (name)
            return (name.length > 40) ? name.substring(0, 40) + "..." : name;
        return name;
    }

    self.onSortButtonClick = function onSortButtonClick() {
        if (!self.queryParams.sortType)
            self.queryParams.sortType = "asc";
        else
            self.queryParams.sortType = self.queryParams.sortType === "asc" ? "desc" : "asc";
        initializedResourceAVD(resourceAVDForm);
    }


    self.submitAVDForm = initializedResourceAVD;

    // Default Call.
    initializedResourceAVD(resourceAVDForm);
}