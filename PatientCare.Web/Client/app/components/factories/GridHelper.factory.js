﻿
"use strict";
angular.module("EDZoutstaffingPortalApp")
    .factory('GridHelper', ['NgTableParams', function (NgTableParams) {
        var pageCount = 20;

        function getDefaultCallParams(params) {
            
            var sorting = params.sorting();
            var callParams = {
                pageIndex: params.page(),
                pageSize: pageCount
            }
            var sorting = params.sorting();
            var sortingKeys = Object.keys(sorting);
            if (sortingKeys.length > 0) {
                var first = sortingKeys[0];
                callParams.sortField = first;
                callParams.sortOrder = sorting[callParams.sortField] === "asc" ? "Ascending" : "Descending";
            }
            
            return callParams;
        }

        function getGridOptions() {
            return {
                count: pageCount,
                paginationMaxBlocks: 5,
                paginationMinBlocks: 2
            };
        }

        function getTableSettings(callBack) {
            
            var tableSettings = {
                counts: [],// defines page size right buttons
                getData: callBack
                
            }
            return tableSettings;
        }

        function getDefaultTableParams(callBackFunction) {
            var options = getGridOptions();
            var settings = getTableSettings(callBackFunction);
            var tp = new NgTableParams(options, settings);
            return tp;
        }

        return {

            getCallParams: function (params) {
                return getDefaultCallParams(params);
            },

            getTableParams: function (callBack) {
                return getDefaultTableParams(callBack);
            }

        };

    }]);

