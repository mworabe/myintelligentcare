﻿using FluentValidation;
using PatientCare.Web.Api.Models.ExercisePositions;

namespace PatientCare.Web.Api.Models.DropDownConfigs
{
    public class ExercisePositionRequestValidator : AbstractValidator<ExercisePositionRequest>
    {
        public ExercisePositionRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);            
        }
    }
}