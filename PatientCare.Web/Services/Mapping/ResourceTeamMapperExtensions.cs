﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceTeams;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceTeamMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceTeamRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTeam, ResourceTeamResponse>(new ExtendedMapConfig<ResourceTeam, ResourceTeamResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTeam[], ResourceTeamResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTeamRequest, ResourceTeam>(new ExtendedMapConfig<ResourceTeamRequest, ResourceTeam>()));

            return mapperProvider;
        }
    }
}