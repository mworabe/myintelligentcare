namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class CteateTableChatUser : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.chat_user_key",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    user_id = c.String(),
                    private_key = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropTable("dbo.chat_user_key");
        }
    }
}
