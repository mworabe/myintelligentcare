﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PatientWorkoutAnalyticsMappingExtensions
    {
        public static DbModelBuilder MapPatientWorkoutAnalytics(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<PatientWorkoutAnalytic>().ToTable("patient_workout_analytics")
                .HasKey(i => i.Id);

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .Property(p => p.CaseId)
                .HasColumnName("case_id")
                .IsOptional();

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .HasOptional<PatientCase>(p => p.Case)
                .WithMany()
                .HasForeignKey(p => p.CaseId);

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .Property(p => p.StartDate)
                .HasColumnName("start_date")
                .IsOptional();

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .Property(p => p.PreExercisesPainRating)
                .HasColumnName("pre_exercises_pain_rating")
                .IsOptional();

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .Property(p => p.WorkoutFeelResult)
                .HasColumnName("workout_feel_result")
                .IsOptional();

            modelBuilder.Entity<PatientWorkoutAnalytic>()
                .Property(p => p.WorkoutComment)
                .HasColumnName("workout_comment")
                .IsOptional();
          
            return modelBuilder;
        }
    }
}
