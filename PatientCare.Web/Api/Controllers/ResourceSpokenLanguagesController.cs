﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.ResourceSpokenLanguages;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Spoken Languages operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceSpokenLanguagesController : EdzBaseApiController<ResourceSpokenLanguage, ResourceSpokenLanguageRequest, ResourceSpokenLanguageResponse>
    {
        /// <summary>
        /// Description : In this class we pass  Resource Spoken Languages IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource Spoken Language Repository</param>
        public ResourceSpokenLanguagesController(IResourceSpokenLanguageRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.Language)
                .WithIncludePredicate(item => item.Resource);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }

        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceSpokenLanguages/" + id, UriKind.Relative);
        }
    }
}