﻿using FluentValidation;

namespace PatientCare.Web.Api.Models.LeaveTypes
{
    public class LeaveTypeRequestValidator : AbstractValidator<LeaveTypeRequest>
    {
        public LeaveTypeRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);            
        }
    }
}