﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Clinics
{
    [DataContract]
    public class ClinicAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ClinicPrefix { get; set; }

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public bool? IsDeleted { get; set; }
    }
}