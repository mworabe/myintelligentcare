﻿using FluentValidation;

namespace PatientCare.Web.Api.Models.DropDownConfigs
{
    public class DropDownConfigRequestValidator : AbstractValidator<DropDownConfigRequest>
    {
        public DropDownConfigRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);            
        }
    }
}