﻿using System;
using System.Linq;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.ResourceScoreCalculation;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceScore;
using EmitMapper;
using EmitMapper.MappingConfiguration;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceScoreMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceScoreRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");


            var spokenLanguageMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSpokenLanguageFilterRequest, ResourceSpokenLanguage>(
                    new DefaultMapConfig());

            var skillMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSkillFilterRequest, ResourceSkill>(
                    new DefaultMapConfig());

            var resourceAvalaibilityMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<AvailabilityVsDemandFilterRequest, AvailabilityVsDemandFilter>(
                    new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ScoreCalculationFilterRequest, ScoreCalculationFilter>(new ExtendedMapConfig
                    <ScoreCalculationFilterRequest, ScoreCalculationFilter>()
                    .ForMember(x => x.SpokenLanguages,
                        y =>
                            y.SpokenLanguages != null
                                ? y.SpokenLanguages.Select(
                                    i =>
                                        new ScoreFilterItem<ResourceSpokenLanguage>(spokenLanguageMapper.Map(i.Item),
                                            i.Importance, i.Mandatory, spokenLanguageMapper.Map(i.StartRange), spokenLanguageMapper.Map(i.EndRange))).ToList()
                                : null)
                    .ForMember(x => x.WrittenLanguages,
                        y =>
                            y.WrittenLanguages != null
                                ? y.WrittenLanguages.Select(
                                    i =>
                                        new ScoreFilterItem<ResourceSpokenLanguage>(spokenLanguageMapper.Map(i.Item),
                                            i.Importance, i.Mandatory, spokenLanguageMapper.Map(i.StartRange), spokenLanguageMapper.Map(i.EndRange))).ToList()
                                : null)
                    .ForMember(x => x.Skills,
                        r =>
                            r.Skills != null
                                ? r.Skills.Select(
                                    i => new ScoreFilterItem<ResourceSkill>(skillMapper.Map(i.Item), i.Importance, i.Mandatory, skillMapper.Map(i.StartRange), skillMapper.Map(i.EndRange)))
                                    .ToList()
                                : null)
                    .ForMember(x => x.Certificates,
                        r =>
                            r.Certificates != null
                                ? r.Certificates.Select(
                                    i => new ScoreFilterItem<long>(i.Item, i.Importance, i.Mandatory, i.StartRange, i.EndRange))
                                    .ToList()
                                : null)
                    .ForMember(x => x.AvailabilityVsDemand,
                        r =>
                            r.AvailabilityVsDemand != null
                                ? r.AvailabilityVsDemand.Select(
                                    i => new ScoreFilterItem<AvailabilityVsDemandFilter>(resourceAvalaibilityMapper.Map(i.Item),
                                        i.Importance, i.Mandatory, resourceAvalaibilityMapper.Map(i.StartRange), resourceAvalaibilityMapper.Map(i.EndRange))).ToList()
                                : null)
                    ));

            return mapperProvider;
        }
    }
}