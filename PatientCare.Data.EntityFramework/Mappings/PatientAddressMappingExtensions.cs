﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PatientAddressMappingExtensions
    {
        public static DbModelBuilder MapPatientAddress(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<PatientAddress>().ToTable("patient_addresss").HasKey(i => i.Id);

            modelBuilder.Entity<PatientAddress>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<PatientAddress>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<PatientAddress>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<PatientAddress>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<PatientAddress>()
               .Property(p => p.Address)
               .HasColumnName("address")
               .IsOptional()
               .HasMaxLength(255);

            modelBuilder.Entity<PatientAddress>()
                .Property(p => p.AddressIsPrimary)
                .HasColumnName("address_is_primary")
                .IsOptional();

            modelBuilder.Entity<PatientAddress>()
                .Property(p => p.ZipCode)
                .HasColumnName("zipcode")
                .IsOptional()
                .HasMaxLength(10);

            modelBuilder.Entity<PatientAddress>()
               .Property(p => p.CityId)
               .HasColumnName("city_id")
               .IsOptional();

            modelBuilder.Entity<PatientAddress>()
                .HasOptional<City>(p => p.City)
                .WithMany()
                .HasForeignKey(p => p.CityId);

            modelBuilder.Entity<PatientAddress>()
                .Property(p => p.CountryId)
                .HasColumnName("country_id")
                .IsOptional();

            modelBuilder.Entity<PatientAddress>()
                .HasOptional<Country>(p => p.Country)
                .WithMany()
                .HasForeignKey(p => p.CountryId);

            modelBuilder.Entity<PatientAddress>()
               .Property(p => p.StateId)
               .HasColumnName("state_id")
               .IsOptional();

            modelBuilder.Entity<PatientAddress>()
                .HasOptional<State>(p => p.State)
                .WithMany()
                .HasForeignKey(p => p.StateId);

            return modelBuilder;
        }
    }
}
