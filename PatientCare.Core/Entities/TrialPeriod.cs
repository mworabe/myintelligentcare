﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Trial Period.
    /// </summary>
    public class TrialPeriod : Entity
    {
       public DateTime? TrialPeriodDate { get; set; }
    }
}
