namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedThumbImageNameInExerciseMediaTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.exercise_medias", "thumb_image_name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.exercise_medias", "thumb_image_name");
        }
    }
}
