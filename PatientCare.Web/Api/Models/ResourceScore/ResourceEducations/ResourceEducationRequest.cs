﻿using System;

namespace PatientCare.Web.Api.Models.ResourceEducations
{
    public class ResourceEducationRequest
    {
        public long Id { get; set; }

        public long ResourceId { get; set; }

        public string Subject { get; set; }

        public string NameOfSchool { get; set; }

        public DateTime? DateObtained { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        
        public long? ResourceDesignationId { get; set; }
    }
}