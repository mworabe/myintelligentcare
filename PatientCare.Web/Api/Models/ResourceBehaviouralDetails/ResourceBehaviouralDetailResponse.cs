﻿using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PatientCare.Web.Api.Models.ResourceBehaviouralDetails
{
    [DataContract]
    public class ResourceBehaviouralDetailResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public PicklistResponse Resource { get; set; }

        [DataMember]
        public string Behaviour { get; set; }
    }
}