﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Related Cause Enum.
    /// </summary>
    public enum RelatedCauseEnum
    {
        AutoAccidentAtFault = 0,
        AutoAccidentNoFault = 1,
        Fall = 2,
        Abuse = 3,
        AnotherPartyResponsible=4,
        EmploymentInjury=5,
        SportsInjurySurgery=6,
        OtherAccident=7,
        NoneOfTheAbove=8
    }
}
