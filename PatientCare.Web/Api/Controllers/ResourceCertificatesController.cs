﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ResourceCertificates;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Certificates operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceCertificatesController : EdzBaseApiController<ResourceCertificate, ResourceCertificateRequest, ResourceCertificateResponse>
    {
        /// <summary>
        /// Description : In this class we pass  Resource Certificate IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource Certificate Repository</param>
        public ResourceCertificatesController(IResourceCertificateRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.Resource)
                .WithIncludePredicate(item => item.Certificate);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }


        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceCertificates/" + id, UriKind.Relative);
        }
    }
}