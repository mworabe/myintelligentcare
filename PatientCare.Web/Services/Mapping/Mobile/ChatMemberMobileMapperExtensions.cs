﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.ChatSessions;
using PatientCare.Web.ApiMobile.Models.ChatMembers;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class ChatMemberMobileMapperExtensions
    {
        public static EmitMapperMapperProvider ChatMemberMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ChatMember, ChatMemberResponse>
                    (new ExtendedMapConfig<ChatMember, ChatMemberResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ChatMember[], ChatMemberResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ChatMemberRequest, ChatMember>
                    (new ExtendedMapConfig<ChatMemberRequest, ChatMember>()));

            return mapperProvider;
        }
    }
}