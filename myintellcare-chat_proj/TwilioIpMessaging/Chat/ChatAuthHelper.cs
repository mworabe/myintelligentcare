﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Twilio.Jwt.AccessToken;

namespace TwilioIpMessaging.Chat
{
    public class ChatAuthHelper
    {
        private readonly static string VIRGIL_ACCESS_TOKEN = ConfigurationManager.AppSettings["VirgilKey"];
        private readonly static string VIRGIL_APP_ID = ConfigurationManager.AppSettings["VirgilAppId"];
        private readonly static string VIRGIL_KEY_FILE = ConfigurationManager.AppSettings["VirgilKeyFile"];
        private readonly static string VIRGIL_KEY_PWD = ConfigurationManager.AppSettings["VirgilKeyPwd"];

        private string TWILIO_ACCOUNT_SID = ConfigurationManager.AppSettings["TwilioAccountSid"];
        private string TWILIO_API_KEY = ConfigurationManager.AppSettings["TwilioApiKey"];
        private string TWILIO_API_SECRET = ConfigurationManager.AppSettings["TwilioApiSecret"];
        private string TWILIO_CHAT_SERVICE_ID = ConfigurationManager.AppSettings["TwilioIpmServiceSid"];

        public async Task<object> getTwilioToken(string userId, string deviceType)
        {
            // Create a random identity for the client
            var identity = userId;

            // Create an IP messaging grant for this token
            var grant = new IpMessagingGrant();
            grant.EndpointId = $"TwilioChatDemo:{identity}:{deviceType}";
            grant.ServiceSid = TWILIO_CHAT_SERVICE_ID;
            var grants = new HashSet<IGrant>
            {
                { grant }
            };

            var Token = new Token(TWILIO_ACCOUNT_SID, TWILIO_API_KEY, TWILIO_API_SECRET, identity: identity, grants: grants);

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("identity", userId);
            dic.Add("token", Token.ToJwt());

            return dic;
        }

    }
}