﻿using System;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ResourceWorkExperience;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Work Experiance operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceWorkExperienceController : EdzBaseApiController<ResourceWorkExperience, ResourceWorkExperienceRequest, ResourceWorkExperienceResponse>
    {
        /// <summary>
        /// Description : In this class we pass  Resource  Work Experiance IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource  Work Experiance Repository</param>
        public ResourceWorkExperienceController(IResourceWorkExperienceRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.ResourceId);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }


        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceWorkExperience/" + id, UriKind.Relative);
        }
    }
}