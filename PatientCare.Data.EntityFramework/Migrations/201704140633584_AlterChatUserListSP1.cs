namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterChatUserListSP1 : DbMigration
    {
        public override void Up()
        {
            AlterStoredProcedure(
         "dbo.sp_chat_userlist",
         p => new
         {
             Role = p.String(defaultValueSql: null),
             Type = p.String(defaultValueSql: null),
             DataCount = p.Int(defaultValueSql: null),
             Search = p.String(defaultValueSql: null),
             loginUserId = p.String(defaultValueSql: null),

         },
         body:
                       @"if(@Role = 'Therapist')
                        begin
                        if (@Type = 'all')
                        begin
    	                                select Distinct res.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,res.Email ,
    						                                res.photo_file_name as PhotoFileName,netuser.Id as UserId
    						                                from resources res
    						                                left join AspNetUsers netuser on res.Id = netuser.ResourceId
    						                                where res.Id in (
    							                                select Distinct  users.ResourceId from AspNetUsers users
    							                                inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    							                                inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist') 
                                                                and netuser.Id != @loginUserId
    					
                        end
                        else if (@Type = 'withSearch')
                        begin
    	                                select Distinct TOP (@DataCount) res.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,
    							                                res.Email ,res.photo_file_name as PhotoFileName,netuser.Id as UserId
    							                                from resources  res
    							                                left join AspNetUsers netuser on res.Id = netuser.ResourceId
    							                                where res.Id in (
    							                                select Distinct  users.ResourceId from AspNetUsers users
    							                                inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    							                                inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist')
    	                                                        and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
											                    and netuser.Id != @loginUserId
    
                        end
                        end
	                    else if(@Role = 'FrontOffice')
                        begin
                        if (@Type = 'all')
                        begin
    	                                    select Distinct res.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,res.Email ,
    						                                res.photo_file_name as PhotoFileName,netuser.Id as UserId
    						                                from resources res
    						                                left join AspNetUsers netuser on res.Id = netuser.ResourceId
    						                                where res.Id in (
    							                                select Distinct  users.ResourceId from AspNetUsers users
    							                                inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    							                                inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='FrontOffice') 
                                                                and netuser.Id != @loginUserId
    					
                        end
                        else if (@Type = 'withSearch')
                        begin
    	                                select Distinct TOP (@DataCount) res.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,
    							                                res.Email ,res.photo_file_name as PhotoFileName,netuser.Id as UserId
    							                                from resources  res
    							                                left join AspNetUsers netuser on res.Id = netuser.ResourceId
    							                                where res.Id in (
    							                                select Distinct  users.ResourceId from AspNetUsers users
    							                                inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    							                                inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='FrontOffice')
    	                                                        and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
											                    and netuser.Id != @loginUserId
    
    
                        end
                        end
                        else if(@Role = 'Patient')
                        begin
                        if (@Type = 'all')
                        begin
    	                                select Distinct pat.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,pat.Email ,
    				                                pat.photo_file_name as PhotoFileName,netuser.Id as UserId
    				                                from patients pat
    				                                left join AspNetUsers netuser on pat.Id = netuser.PatientId
    				                                where pat.Id in (
    					                                select Distinct  users.PatientId from AspNetUsers users
    					                                inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    					                                inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Patient') 
    					
                        end
                        else if (@Type = 'withSearch')
                        begin
    	                                select Distinct TOP (@DataCount) pat.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,
    							                                pat.Email ,pat.photo_file_name as PhotoFileName,netuser.Id as UserId
    							                                from patients  pat
    							                                left join AspNetUsers netuser on pat.Id = netuser.PatientId
    							                                where pat.Id in (
    							                                select Distinct  users.PatientId from AspNetUsers users
    							                                inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    							                                inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Patient')
    	                                                        and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
    
    
                        end
                        end");
        }
        
        public override void Down()
        {
            AlterStoredProcedure(
         "dbo.sp_chat_userlist",
         p => new
         {
             Role = p.String(defaultValueSql: null),
             Type = p.String(defaultValueSql: null),
             DataCount = p.Int(defaultValueSql: null),
             Search = p.String(defaultValueSql: null),
             loginUserId = p.String(defaultValueSql: null),

         },
         body:
                       @"if(@Role = 'Therapist')
            begin
                if (@Type = 'all')
                begin
    	            select Distinct res.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,res.Email ,
    						            res.photo_file_name as PhotoFileName,netuser.Id as UserId
    						            from resources res
    						            left join AspNetUsers netuser on res.Id = netuser.ResourceId
    						            where res.Id in (
    							            select Distinct  users.ResourceId from AspNetUsers users
    							            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    							            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist') 
                                            and netuser.Id != @loginUserId
    					
                end
                else if (@Type = 'withSearch')
                begin
    	            select Distinct TOP (@DataCount) res.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,
    							            res.Email ,res.photo_file_name as PhotoFileName,netuser.Id as UserId
    							            from resources  res
    							            left join AspNetUsers netuser on res.Id = netuser.ResourceId
    							            where res.Id in (
    							            select Distinct  users.ResourceId from AspNetUsers users
    							            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    							            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist')
    	                                    and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
                                            and netuser.Id != @loginUserId
    
                end
            end
            else if(@Role = 'Patient')
            begin
                if (@Type = 'all')
                begin
    	            select Distinct pat.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,pat.Email ,
    				            pat.photo_file_name as PhotoFileName,netuser.Id as UserId
    				            from patients pat
    				            left join AspNetUsers netuser on pat.Id = netuser.PatientId
    				            where pat.Id in (
    					            select Distinct  users.PatientId from AspNetUsers users
    					            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    					            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Patient') 
    					
                end
                else if (@Type = 'withSearch')
                begin
    	            select Distinct TOP (@DataCount) pat.Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,
    							            pat.Email ,pat.photo_file_name as PhotoFileName,netuser.Id as UserId
    							            from patients  pat
    							            left join AspNetUsers netuser on pat.Id = netuser.PatientId
    							            where pat.Id in (
    							            select Distinct  users.PatientId from AspNetUsers users
    							            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    							            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Patient')
    	                                    and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
    
    
                end
            end");
        }
    }
}
