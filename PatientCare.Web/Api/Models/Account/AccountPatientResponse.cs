﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Account
{
    [DataContract]
    public class AccountPatientResponse
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public string Role { get; set; }
    }
}