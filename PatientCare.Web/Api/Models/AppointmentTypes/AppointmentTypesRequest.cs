﻿using System;

namespace PatientCare.Web.Api.Models.AppointmentTypes
{
    public class AppointmentTypesRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }

    }
}