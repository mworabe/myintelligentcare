﻿
"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("ReportService", ["$resource", "$state", "$injector", "$translate", "$filter", function ($resource, $state, $injector, $translate, $filter) {
    var resource = $resource("api/Report/:id/:controller",
        null, //parameters default
        {
            'update': { method: "PUT" },
            'query': { method: "GET", params: {}, isArray: false },

            'getResourcesReport': { method: "POST", params: { controller: "GetResourcesReport" }, isArray: false },
            'getResourcesReportExport': { method: "GET", params: { controller: "export-resources-report" }, isArray: false },

            'getResourcesUtilizationReport': { method: "POST", params: { controller: "GetResourcesUtilitationReport" }, isArray: false },
            'getResourcesUtilizationReportExport': { method: "GET", params: { controller: "export-resources-utilization-report" }, isArray: false },

            'getResourcesRateOrSalaryReport': { method: "POST", params: { controller: "GetResourcesRateOrSalaryReport" }, isArray: false },
            'getResourcesRateOrSalaryReportExport': { method: "GET", params: { controller: "export-resources-rateOrSalary-report" }, isArray: false },


            'getResourcesWorkDetailsReport': { method: "POST", params: { controller: "GetResourcesWorkDetailsReport" }, isArray: false },
            'getResourcesWorkDetailsReportExport': { method: "GET", params: { controller: "export-resources-workDetails-report" }, isArray: false },

            'getProjectsReport': { method: "POST", params: { controller: "GetProjectsReport" }, isArray: false },
            'getProjectsReportExport': { method: "GET", params: { controller: "export-projects-report" }, isArray: false },

            'getProjectOwnersReport': { method: "POST", params: { controller: "GetProjectOwnersReport" }, isArray: false },
            'getProjectOwnersReportExport': { method: "GET", params: { controller: "export-projects-owners-report" }, isArray: false },

            'getProjectActualCostReport': { method: "POST", params: { controller: "GetProjecActualCostReport" }, isArray: false },
            'getProjectActualCostReportExport': { method: "GET", params: { controller: "export-projects-actualCost-report" }, isArray: false },

            'getProjectStatusReport': { method: "POST", params: { controller: "GetProjecStatusReport" }, isArray: false },
            'getProjectStatusReportExport': { method: "GET", params: { controller: "export-projects-status-report" }, isArray: false },

            'getProjectDivisionDepartamentReport': { method: "POST", params: { controller: "GetProjecDivisionDepartamentReport" }, isArray: false },
            'getProjectDivisionDepartamentReportExport': { method: "GET", params: { controller: "export-projects-divisionDepartament-report" }, isArray: false },

            'getFinancialPlanReport': { method: "POST", params: { controller: "GetFinancialPlanReport" }, isArray: false },
            'getFinancialPlanReportExport': { method: "GET", params: { controller: "export-financialPlan-report" }, isArray: false },

            'getWeeklyProjectReportData': { method: "POST", params: { controller: "GetProjectDetailWeeklyReport" }, isArray: false },

            'getIntakeReport': { method: "POST", params: { controller: "GetIntakeReport" }, isArray: false },

            'getProjectResourceWorkedReport': { method: "POST", params: { controller: "GetProjectResourceTargetVsWorkedReport" }, isArray: false },

        });

    resource.getTableOption = {
        pageName: "Reports",
        disabledAddButton: true,
        hideBulkEdit: true,
        hideAddButton: true,
        hideGridRightOptions: true,
        hideHeaderCounter: true,
        cols: function () {
            return [
                { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                {
                    field: "name",
                    title: "Name",                  
                    show: true,
                    dataType: "text",
                    dataTypeView: "href",
                    required: true,
                    buildUrl: function (row) {
                        return $state.href("reportView", { id: row.id });
                    }
                },
            ];
        }
    };

    resource.getResourceReportTableOption = {
        pageName: "Resources Report",
        cols: function () {
            return [
                //{ field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "first_name", title: "Last name", sortable: "first_name", show: true, dataType: "text" },
                { field: "last_name", title: "First name", sortable: "last_name", show: true, dataType: "text" },
                { field: "job_Title", title: "Job title", sortable: "Job_Title", show: true, dataType: "text" },
                { field: "department", title: "Department", sortable: "department", show: true, dataType: "text" },
                { field: "division", title: "Division", sortable: "division", show: true, dataType: "text" },
                { field: "supervisor", title: "Supervisor", sortable: "supervisor", show: true, dataType: "text" },
                { field: "start_date", title: "Hire Date", sortable: "start_date", show: true, dataType: "datetime" },

            ];
        },
        colsArray: [
                //{ field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "first_name", title: "Last name", sortable: "first_name", show: true, dataType: "text" },
                { field: "last_name", title: "First name", sortable: "last_name", show: true, dataType: "text" },
                { field: "job_Title", title: "Job title", sortable: "Job_Title", show: true, dataType: "text" },
                { field: "department", title: "Department", sortable: "department", show: true, dataType: "text" },
                { field: "division", title: "Division", sortable: "division", show: true, dataType: "text" },
                { field: "supervisor", title: "Supervisor", sortable: "supervisor", show: true, dataType: "text" },
                { field: "start_date", title: "Hire Date", sortable: "start_date", show: true, dataType: "datetime" },

        ]
    };

    resource.getResourceUtilizationReportTableOption = {
        pageName: "Resources Report",
        colsArray: [
                //{ field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "lastName", title: "Last name", sortable: "last_name", show: true, dataType: "text" },
                { field: "firstName", title: "First name", sortable: "first_name", show: true, dataType: "text" },
                { field: "jobTitle", title: "Job title", sortable: "Job_Title", show: true, dataType: "text" },
                { field: "lastMonthUtilization", title: "Utilization last Month", sortable: "division", show: true, dataType: "text" },
                { field: "lastQuarterUtilization", title: "Utilization last Quarter", sortable: "supervisor", show: true, dataType: "text" },
                { field: "lastYearUtilization", title: "Utilization last Year", sortable: "start_date", show: true, dataType: "datetime" },

        ]
    };

    resource.getProjectReportTableOption = {
        pageName: "Projects Report",
        cols: function () {
            return [
                //{ field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "projectName", title: $filter('translate')("Project") + " Name", sortable: "first_name", show: true, dataType: "text" },
                { field: "start_date", title: "Start Date", sortable: "last_name", show: true, dataType: "text" },
                { field: "end_datee", title: "End Date", sortable: "Job_Title", show: true, dataType: "text" },
                { field: "task", title: "Task", sortable: "department", show: true, dataType: "text" },
                { field: "task_startDate", title: "Start Date", sortable: "division", show: true, dataType: "text" },
                { field: "task_endDate", title: "End Date", sortable: "supervisor", show: true, dataType: "text" },
                { field: "Assigne", title: "Assignees", sortable: "start_date", show: true, dataType: "datetime" },
                { field: "Owner", title: "Owners", sortable: "start_date", show: true, dataType: "datetime" },
            ];
        },
        colsArray: [
                //{ field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "projectName", title: $filter('translate')("Project") + " Name", sortable: "first_name", show: true, dataType: "text" },
                { field: "start_date", title: "Start Date", sortable: "last_name", show: true, dataType: "text" },
                { field: "end_datee", title: "End Date", sortable: "Job_Title", show: true, dataType: "text" },
                { field: "task", title: "Task", sortable: "department", show: true, dataType: "text" },
                { field: "task_startDate", title: "Start Date", sortable: "division", show: true, dataType: "text" },
                { field: "task_endDate", title: "End Date", sortable: "supervisor", show: true, dataType: "text" },
                { field: "Assigne", title: "Assignees", sortable: "start_date", show: true, dataType: "datetime" },
                { field: "Owner", title: "Owners", sortable: "start_date", show: true, dataType: "datetime" },
        ]
    };
    return resource;
}
]);