namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedColumnInPatientAndReferralPhysicianTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "clinic_id", c => c.Long());
            AddColumn("dbo.patients", "clinic_location_id", c => c.Long());
            AddColumn("dbo.referral_physicians", "extension_number", c => c.String());
            CreateIndex("dbo.patients", "clinic_id");
            CreateIndex("dbo.patients", "clinic_location_id");
            AddForeignKey("dbo.patients", "clinic_id", "dbo.clinics", "Id");
            AddForeignKey("dbo.patients", "clinic_location_id", "dbo.clinic_locations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.patients", "clinic_location_id", "dbo.clinic_locations");
            DropForeignKey("dbo.patients", "clinic_id", "dbo.clinics");
            DropIndex("dbo.patients", new[] { "clinic_location_id" });
            DropIndex("dbo.patients", new[] { "clinic_id" });
            DropColumn("dbo.referral_physicians", "extension_number");
            DropColumn("dbo.patients", "clinic_location_id");
            DropColumn("dbo.patients", "clinic_id");
        }
    }
}
