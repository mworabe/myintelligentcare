﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
       .controller("AddDashboardCtrl", [
           "$scope",
           "DashboardResource",
           "dashboard",
           "$uibModalInstance",
           "$state", "$timeout",
           "toaster",
           addDashboardCtrl
       ]);
function addDashboardCtrl($scope, DashboardResource, dashboard, $uibModalInstance, $state, $timeout, toaster) {
    var self = this;
    self.cancel = function () {
        $uibModalInstance.dismiss();
    }
    self.dashboard = {
        title: "",
        structure: "6-6",
        rows: [{
            columns: [{
                styleClass: "col-md-6",
                widgets: []
            }, {
                styleClass: "col-md-6",
                widgets: []
            }]
        }]
    };
    function validateDashboardModel() {
        if (self.dashboard.title && self.dashboard.title.length > 0)
            return true;
        return false;
    }

    self.createDashboard = function () {
        if (!validateDashboardModel()) {
            toaster.pop("error", "", "Dashboard Name is Required");
            return;
        }
        //DashboardResource.saveDashboard(self.dashboard);
        DashboardResource.saveDashboard(self.dashboard);
        $timeout(function () {
            self.dashboard = DashboardResource.getLastInsertedDashboard();
            $uibModalInstance.dismiss();
            toaster.pop("success", "", "Dashboard " + self.dashboard.name + " created successfully.");
            if (self.dashboard && self.dashboard.id) {
                $state.go("dashboard-edit", { id: self.dashboard.id });
            }
        }, 3000);
    }
}