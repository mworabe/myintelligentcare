// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright � 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("RolesAvailabilityVsDemandPopUpCtrl", [
        "NgTableParams", "ResourceDemand", "DateTimeHelper", "uuid2", "$state", "$uibModalInstance", "data", "$scope",
function (ngTableParams, resourceDemand, dateTimeHelper, uuid2, $state, $uibModalInstance, taskData, $scope) {
    var self = this;

    var changeTypeTable;
    var resourceResponce = [];
    self.taskFilter = taskData.taskFilter;

    self.taskFilterFromDate = moment(self.taskFilter.fromDate).format("MM/DD/YYYY");
    self.taskFilterToDate = moment(self.taskFilter.toDate).format("MM/DD/YYYY");

    var dateNow = moment(self.taskFilter.fromDate);
    var newDate = moment(self.taskFilter.toDate);


    function getFormatUtilization(percentage) {
        if (percentage === Number.POSITIVE_INFINITY || percentage === Number.NEGATIVE_INFINITY)
            return "-";
        return percentage.toFixed(0) + "%";
    }

    function checkNumberIsInfinity(number) {
        return number === Number.POSITIVE_INFINITY || number === Number.NEGATIVE_INFINITY;
    }

    function checkMedium(percentage) {
        if (checkNumberIsInfinity(percentage))
            return false;
        return percentage >= 80 && percentage < 100;

    }

    function checkCritical(percentage) {
        if (checkNumberIsInfinity(percentage))
            return true;
        return percentage >= 100;
    }

    function getClassesFromImage(percentage) {
        var value = ((percentage / 10) | 0);
        value = value > 10 ? 10 : value;
        return "donut" + value;
    }

    function updateServerItems(data) {

        function calcPercentage(available, demand) {

            var actualCup = parseFloat(available.toFixed(1));
            var demandFixed = parseFloat(demand.toFixed(1));
            //actual cup = 100%

            var percentage = (demandFixed / actualCup);
            if (isNaN(percentage)) {
                if (demandFixed === 0)
                    percentage = 0;
                else percentage = 1;

            }
            if (checkNumberIsInfinity(percentage)) {
                if (demandFixed === 0)
                    percentage = 0;
                else percentage = 1;
            }
            return percentage * 100;

        }

        _.forEach(data, function (item, index) {
            item.id = uuid2.newguid();
            _.forEach(item.infoByDates, function (item) {
                if (index === 0) {
                    self.headers.push({
                        name: dateTimeHelper.getColumnName(moment(item.startDate), self.tableRequest.timeRange),
                        id: uuid2.newguid()
                    });
                }
                item.percentage = calcPercentage(item.available, item.demand);
                item.utilization = getFormatUtilization(item.percentage);
                item.isMediumClass = checkMedium(item.percentage);
                item.isCriticalClass = checkCritical(item.percentage);
                item.imageClass = getClassesFromImage(item.percentage);
            });
        });
    };


    function availabilityItemCreator(jobTitleId, jobTitleName, resourceId, resourceName, infoByDates) {
        return {
            jobTitleId: jobTitleId,
            jobTitleName: jobTitleName,
            resourceId: resourceId,
            resourceName: resourceName,
            infoByDates: infoByDates || []
        }
    }


    function transformResponseByJobTitle(response) {
        var result = [];
        var jobTitleGroup = _.groupBy(response, "jobTitleId");

        //hard hack
        var defaultWorkingTime = 8;
        if (self.tableRequest.timeRange === "Month") {
            defaultWorkingTime = 168;
        }
        else if (self.tableRequest.timeRange === "Year") {
            defaultWorkingTime = 2080;
        }

        _.forEach(jobTitleGroup, function (values) {
            var firstItem = values[0];
            var availabilityItem = availabilityItemCreator(firstItem.jobTitleId,
                firstItem.jobTitleName, firstItem.resourceId, firstItem.resourceName);
            var infoByDatesSelect = _.flatten(_.map(values, 'infoByDates'), true);
            var groupByInfoByDates = _.groupBy(infoByDatesSelect, "startDate");
            var numberOfResourcesByRole = values.length;

            _.forEach(groupByInfoByDates, function (values, key) {
                var workingTime = (values[0] ? values[0].hoursCount : defaultWorkingTime) || defaultWorkingTime;
                var available = _.sumBy(values, "available") / workingTime;
                var demand = _.sumBy(values, "demand") / workingTime;
                var startDate = key;
                availabilityItem.infoByDates.push({
                    startDate: startDate,
                    available: available,
                    demand: demand,
                    hoursCount: numberOfResourcesByRole
                });
            });

            result.push(availabilityItem);
        });
        result = _.orderBy(result, ["jobTitleName"], ["asc"]);

        return result;

    }

    function updateModel(needReloadData) {
        self.headers = [];
        function setAvailabilityArray(itemName) {

            var sortType = self.sortType;


            updateServerItems(self[itemName]);
            self.totalCount = self[itemName].length;

            var parameterSort = self.jobTitleSelectId > 0 ? "resourceName" : "jobTitleName";


            if (sortType === "asc") {
                self[itemName] = _.sortBy(self[itemName], function (item) {
                    return item[parameterSort];
                });
            }
            if (sortType === "desc") {
                self[itemName] = _.sortBy(self[itemName], function (item) {
                    return item[parameterSort].charCodeAt() * -1;
                });
            }

            self.data = self[itemName];
        }

        function setAvailability() {

            if (self.jobTitleSelectId > 0) {
                self.resourceResponceSelect = _.filter(resourceResponce, { "jobTitleId": self.jobTitleSelectId });
                setAvailabilityArray("resourceResponceSelect");
            } else {
                setAvailabilityArray("jobTitles");
            }
        }

        function getEndDate(startDate, endDate) {

            var dateFormated = endDate.format("LL");

            var start = new Date(startDate);
            var end = new Date(endDate);

            var limitDate = new Date(start.getFullYear() + 3, start.getMonth(), start.getDate())

            if (end > limitDate) {
                self.tableRequest.end = moment(limitDate);
                return moment(limitDate).format("LL")
            }

            return dateFormated;
        }

        var model = angular.copy(self.tableRequest);
        model.start = model.start.format("LL");
        model.end = getEndDate(model.start, model.end);//model.end.format("LL");
        // ajax request to api
        if (needReloadData) {
            self.loadPage = resourceDemand.buildCapacityReport(model).$promise.then(function (response) {
                resourceResponce = response;
                if (response.length === 0) {
                    return;
                }
                self.jobTitles = transformResponseByJobTitle(response);
                setAvailability();
            });
        }
        else {
            setAvailability();
        }
    }

    function getDataFromServier(form) {
        if (form.$invalid)
            return;
        updateModel(true);
    }

    function sort() {
        if (!self.sortType) {
            self.sortType = "asc";
        } else {
            self.sortType = self.sortType === "asc" ? "desc" : "asc";
        }
        updateModel(false);
    }

    function onJobTitleClick(jobTitleId) {
        changeTypeTable = true;
        self.jobTitleSelectId = jobTitleId;
        updateModel(false);
    }

    function onResourceClick(resourceId) {
        $state.go("resourceEdit", { id: resourceId });
    }

    function backClick() {
        changeTypeTable = true;

        self.jobTitleSelectId = null;
        updateModel(false);
    }


    this.tableParams = new ngTableParams({
        count: 100
    }, {
        counts: [],
        getData: function () {
            return [];
        }

    });

    function initialParameter() {
        self.isDetailedMode = false;
        self.headers = [];
        self.timeRangeEnum = dateTimeHelper.dateTypeEnum();
        self.minDate = moment();
        self.jobTitleSelectId = null;
        self.data = [];
        self.tableRequest = {
            timeRange: "Month",
            start: dateNow,
            end: newDate
        }
    }

    function actviate() {
        initialParameter();
        updateModel(true);
    }

    this.calculateWightsGren = calculateWightsGren;
    this.calculateWightsRed = calculateWightsRed;

    function calculateWightsGren(col) {
        var WightsGren = (col.available / col.hoursCount) * 100;
        return WightsGren.toFixed(2);
    }
    function calculateWightsRed(col) {
        var WightsRed = (col.demand / col.hoursCount) * 100;
        return WightsRed.toFixed(2);
    }

    /*PopUp*/

    function cancel() {
        $uibModalInstance.dismiss();
    }

    function assignResource() {
        console.log(self.taskFilter);
        console.log(self.data);

        $uibModalInstance.close(self.data);
    }

    function redirectOnResourceEdit(row) {
        $uibModalInstance.dismiss();
        $scope.ignoreRedirect = true;
        $state.go("resourceEdit", { id: row.resourceId });
    }


    /* pagination*/
    function pageChanged() { };
    this.totalItems = 64;
    this.maxSize = 5;
    this.numPages = 5;


    actviate();

    self.checkCritical = checkCritical;
    self.checkMedium = checkMedium;
    self.back_Click = backClick;
    self.onJobTitle_Click = onJobTitleClick;
    self.sort = sort;
    self.getDataFromServier = getDataFromServier;
    self.getClassesFromImage = getClassesFromImage;
    self.onResource_Click = onResourceClick;
    self.pageChanged = pageChanged;
    self.redirectOnResourceEdit = redirectOnResourceEdit;

    self.cancel = cancel;
    self.assignResource = assignResource;
}
    ]);