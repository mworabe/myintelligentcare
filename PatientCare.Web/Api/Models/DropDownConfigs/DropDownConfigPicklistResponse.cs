﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.DropDownConfigs
{
    [DataContract]
    public class DropDownConfigPicklistResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string ProjectName { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DropDownType { get; set; }

        [DataMember]
        public string DisplayName
        {
            get { return (Name != null ? Name : ""); }
            set { DisplayName = value; }
        }
        
        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string ConfigField1 { get; set; }

        [DataMember]
        public string ConfigField2 { get; set; }

        [DataMember]
        public string ConfigField3 { get; set; }

        [DataMember]
        public string ConfigField4 { get; set; }

        [DataMember]
        public string ConfigField5 { get; set; }       

    }
}