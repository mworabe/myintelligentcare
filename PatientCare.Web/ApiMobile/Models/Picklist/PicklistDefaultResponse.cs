﻿using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.Picklist
{
    [DataContract]
    public class PicklistDefaultResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string PhotoFileName { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Role { get; set; }
    }
}