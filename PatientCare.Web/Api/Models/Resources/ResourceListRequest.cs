﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Resources
{
    public class ResourceListRequest : ListRequest
    {
        [DataMember(Name = "showInactive")]
        public bool ShowInactive { get; set; }
        [DataMember]
        public long? DepartmentId { get; set; }
        [DataMember]
        public long? DivisionId { get; set; }
    }
}