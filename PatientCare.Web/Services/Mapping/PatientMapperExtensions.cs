﻿using System;
using System.Linq;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.Patients;
using PatientCare.Web.Api.Models.PatientParentGuardians;
using PatientCare.Web.Api.Models.PatientAddresses;
using PatientCare.Web.Services.FileStorage;
using EmitMapper;
using EmitMapper.MappingConfiguration;

namespace PatientCare.Web.Services.Mapping
{
    public static class PatientMapperExtensions
    {
        public static EmitMapperMapperProvider PatientRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient, PatientResponse>
                    (new ExtendedMapConfig<Patient, PatientResponse>()
                        .ForMember(x => x.PhotoFileNameUrl, y => FileUploadHelpers.GetStorageUrlPhoto(y.PhotoFileName, FileUploadHelpers.PatientFolderTemplate))
                    ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient[], PatientResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient, PatientListResponse>
                    (new ExtendedMapConfig<Patient, PatientListResponse>()
                        .ForMember(x => x.PhotoFileNameUrl, y => FileUploadHelpers.GetStorageUrlPhoto(y.PhotoFileName, FileUploadHelpers.PatientFolderTemplate))
                    ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient[], PatientListResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient, PicklistResponse>
                    (new ExtendedMapConfig<Patient, PicklistResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient, PicklistDefaultResponse>
                    (new ExtendedMapConfig<Patient, PicklistDefaultResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient, PatientAutoCompleteResponse>
                    (new ExtendedMapConfig<Patient, PatientAutoCompleteResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Patient[], PatientAutoCompleteResponse[]>());

            var patientAddressMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAddressRequest, PatientAddress>(
                    new DefaultMapConfig());

            var patientParentGuardianMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<PatientParentGuardianRequest, PatientParentGuardian>(
                    new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientRequest, Patient>(
                    new ExtendedMapConfig<PatientRequest, Patient>()
                        .ForMember(x => x.Addresses, y => y.Addresses != null ? y.Addresses.Select(i => patientAddressMapper.Map(i)).ToList() : null)
                        .ForMember(x => x.ParentGaurdians, y => y.ParentGaurdians != null ? y.ParentGaurdians.Select(i => patientParentGuardianMapper.Map(i)).ToList() : null)
                    ));

            return mapperProvider;
        }
    }
}