﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models
{
    public class FileRequest
    {
        public string S3BucketUrl { get; set; }
        public string S3BucketName { get; set; }
        public string Name { get; set; }
        public bool IsDelete { get; set; }
        public string S3BucketThumbUrl { get; set; }
        public string S3BucketThumbName { get; set; }
        public string ThumbImageName { get; set; }
    }
}