﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models.ClinicLocations;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.AutoCompletes;
using PatientCare.Data.EntityFramework;
using System.Collections.Generic;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Clinic Locations operations.
    /// </summary>
    [Authorize]
    public sealed class ClinicLocationsController : ApiBaseController
    {
        /// <summary>
        /// Description : Clinic Locations Field IClinicService.
        /// </summary>
        private readonly IClinicService _clinicService;

        /// <summary>
        /// Description : Clinic Locations Field IClinicLocationRepository.
        /// </summary>
        private readonly IClinicLocationRepository _clinicLocationRepository;

        /// <summary>
        /// Description : Clinic Locations Field IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<ClinicLocation> _readOptions = new ReadOptions<ClinicLocation>();

        /// <summary>
        /// Description : Initializes a new instance of the Clinic Locations Class.
        /// </summary>
        /// <param name="clinicService">Clinic Service</param>
        /// <param name="clinicLocationRepository">Clinic Location Repository</param>
        /// <param name="dataContextScopeFactory">Data Context Scope Factory</param>
        public ClinicLocationsController
            (
                IClinicService clinicService,
                IClinicLocationRepository clinicLocationRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _clinicService = clinicService;
            _clinicLocationRepository = clinicLocationRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/ClinicLocations/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Clinic Location.    
        /// </summary>
        /// <param name="request">Clinic Location List Request Model</param>
        /// <returns>Clinic Location Response Model</returns>
        [HttpGet]
        [Route("api/ClinicLocations")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ClinicLocationResponse>))]
        public IHttpActionResult GetAllClinicLocation([FromUri]ClinicLocationListRequest request)
        {
            try
            {
                var query = PagingExtensions<ClinicLocation>.CreatePagedQuery
                          (request,
                              (string.IsNullOrWhiteSpace(request.SearchField)
                                  && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                      ?
                                 GetSearchColumnExpressions(request.SearchPhrase)
                                : GetAutocompleteSearchExpressions(request.SearchPhrase)
                               ));

                // here we not take deleted clinic location in list screen
                query.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);

                var readOptions = new ReadOptions<ClinicLocation>().AsReadOnly();
                var page = _clinicLocationRepository.PagedList(query, readOptions);
                var dtos = Mapper.Map<ClinicLocation[], ClinicLocationResponse[]>(page.Entities.ToArray());

                var response = new ListResponse<ClinicLocationResponse>(dtos, page.TotalCount);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Create New Clinic Location.    
        /// </summary>
        /// <param name="clinicLocationRequest">Clinic Location Request Model</param>
        /// <returns>Clinic Location Response Model</returns>
        [HttpPost]
        [Route("api/ClinicLocations")]
        [SwaggerResponse(201, "Created", typeof(ClinicLocationResponse))]
        public async Task<IHttpActionResult> CreateClinicLocation([FromBody]ClinicLocationRequest clinicLocationRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (_clinicLocationRepository.Exists(FilterQuery.Create<ClinicLocation>().AddFilter(p => p.Name == clinicLocationRequest.Name)))
                {
                    ModelState.AddModelError(ReflectionHelper.GetMemberName<ClinicLocation>(p => p.Name), DisplayResources.Error_ItemNameExists);
                    return BadRequest(ModelState);
                }

                if (_clinicLocationRepository.Exists(FilterQuery.Create<ClinicLocation>().AddFilter(p => p.LocationPrefix == clinicLocationRequest.LocationPrefix)))
                {
                    ModelState.AddModelError(ReflectionHelper.GetMemberName<ClinicLocation>(p => p.LocationPrefix), DisplayResources.Error_LocationPrefixExists);
                    return BadRequest(ModelState);
                }

                var clinicLocation = Mapper.Map<ClinicLocationRequest, ClinicLocation>(clinicLocationRequest);
                await _clinicService.CreateClinicLocationAsync(clinicLocation);

                return Created(GetEntityLocation(clinicLocation.Id), Mapper.Map<ClinicLocation, ClinicLocationResponse>(clinicLocation));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Update Clinic Location.    
        /// </summary>
        /// <param name="cliniclocationRequest">Clinic Location Request Model</param>
        /// <returns>Clinic Location Response Model</returns>
        [HttpPut]
        [Route("api/ClinicLocations/{id}")]
        public async Task<IHttpActionResult> EditClinicLocation(long id, [FromBody] ClinicLocationRequest cliniclocationRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (_clinicLocationRepository.Exists(FilterQuery.Create<ClinicLocation>().AddFilter(p => p.Name == cliniclocationRequest.Name && p.Id != cliniclocationRequest.Id)))
                {
                    ModelState.AddModelError(ReflectionHelper.GetMemberName<ClinicLocation>(p => p.Name), DisplayResources.Error_ItemNameExists);
                    return BadRequest(ModelState);
                }

                if (_clinicLocationRepository.Exists(FilterQuery.Create<ClinicLocation>().AddFilter(p => p.LocationPrefix == cliniclocationRequest.LocationPrefix && p.Id != cliniclocationRequest.Id)))
                {
                    ModelState.AddModelError(ReflectionHelper.GetMemberName<ClinicLocation>(p => p.LocationPrefix), DisplayResources.Error_LocationPrefixExists);
                    return BadRequest(ModelState);
                }

                var cliniclocation = Mapper.Map<ClinicLocationRequest, ClinicLocation>(cliniclocationRequest);
                cliniclocation.Id = id;

                await _clinicService.UpdateClinicLocationAsync(cliniclocation);

                return Ok(Mapper.Map<ClinicLocation, ClinicLocationResponse>(cliniclocation));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Delete Clinic Locatione by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Clinic Location Response Model</returns>
        [HttpDelete]
        [Route("api/ClinicLocations/{id}")]
        public async Task<IHttpActionResult> DeleteClinicLocationItem(long id)
        {
            try
            {
                var clinicLocation = _clinicLocationRepository.Read(id);
                if (clinicLocation == null)
                    return NotFound();

                await _clinicService.DeleteClinicLocationAsync(clinicLocation);

                return Ok(new ClinicLocationResponse { Id = clinicLocation.Id });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Method Description : Using for Get Clinic Location by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Clinic Location Response Model</returns>
        [HttpGet]
        [Route("api/ClinicLocations/{id}")]
        public async Task<IHttpActionResult> GetClinicLocation(long id)
        {
            try
            {
                var cliniclocation = await _clinicService.ReadClinicLocationAsync(id);
                if (cliniclocation == null)
                {
                    return NotFound();
                }

                var viewModel = Mapper.Map<ClinicLocation, ClinicLocationResponse>(cliniclocation);
                return Ok(viewModel);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private static Expression<Func<ClinicLocation, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return cliniclocation => cliniclocation.Name.Contains(searchPhrase);
        }

        private static Expression<Func<ClinicLocation, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return cliniclocation => cliniclocation.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Clinic Location Type.      
        /// </summary>
        /// <param name="autoCompleteRequest">AutoComplete Request</param>
        /// <returns>Clinic Location Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/ClinicLocations/AutoComplete")]
        [ResponseType(typeof(ClinicLocationAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]AutoCompleteRequest autoCompleteRequest = null)
        {
            try
            {
                var filter = new FilterQuery<ClinicLocation>();

                if (!string.IsNullOrWhiteSpace(autoCompleteRequest.Search))
                {
                    filter.AddFilter(x => x.Name.ToLower().StartsWith(autoCompleteRequest.Search.ToLower()));
                }

                if (autoCompleteRequest.ClinicId != null)
                {
                    filter.AddFilter(x => x.ClinicId == autoCompleteRequest.ClinicId);
                }

                filter.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);
                var autocompleteResult = await _clinicLocationRepository.AutocompleteAsync(filter);


                var result = Mapper.Map<ClinicLocation[], ClinicLocationAutoCompleteResponse[]>(autocompleteResult.ToArray());
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet]
        [Route("api/ClinicLocations/autocompletee")]
        [ResponseType(typeof(List<ClinicLocationAutoCompleteResponse>))]
        public async Task<IHttpActionResult> Autocompletee([FromUri] long search)
        {
            List<ClinicLocationAutoCompleteResponse> dd = new List<ClinicLocationAutoCompleteResponse>();
            List<ClinicLocationAutoCompleteResponse> respons = new List<ClinicLocationAutoCompleteResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC SP_ClinicLocations @Id =" + search;
                respons = await ctx.Database.SqlQuery<ClinicLocationAutoCompleteResponse>(query).ToListAsync();
            }
            return Ok(respons);
        }
    }
}
