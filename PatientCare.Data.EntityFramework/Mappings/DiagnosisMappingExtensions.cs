﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class DiagnosisMappingExtensions
    {
        public static DbModelBuilder MapDiagnosis(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<Diagnosis>().ToTable("diagnosis").HasKey(i => i.Id);

            modelBuilder.Entity<Diagnosis>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<Diagnosis>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();
            
           modelBuilder.Entity<Diagnosis>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            return modelBuilder;
        }
    }
}
