﻿using System;
using System.Data;
using PatientCare.Data.AbstractDataContext;

namespace PatientCare.Data.EntityFramework.DataContext
{
    public sealed class DataContextScopeFactory : IDataContextScopeFactory
    {
        private readonly IDataContextFactory _dataContextFactory;

        public DataContextScopeFactory(IDataContextFactory dataContextFactory)
        {
            if (dataContextFactory == null)
            {
                throw new ArgumentNullException("dataContextFactory");
            }
            _dataContextFactory = dataContextFactory;
        }

        public IDataContextScope Create(DataContextCreationOptions creationOptions = DataContextCreationOptions.JoinExisting)
        {
            return new DataContextScope(
                creationOptions: creationOptions,
                readOnly: false,
                isolationLevel: null,
                dataContextFactory: _dataContextFactory);
        }

        public IDataContextReadOnlyScope CreateReadOnly(DataContextCreationOptions creationOptions = DataContextCreationOptions.JoinExisting)
        {
            return new DataContextReadOnlyScope(
                creationOptions: creationOptions,
                isolationLevel: null,
                dbContextFactory: _dataContextFactory);
        }

        public IDataContextScope CreateWithTransaction(IsolationLevel isolationLevel)
        {
            return new DataContextScope(
                creationOptions: DataContextCreationOptions.ForceCreateNew,
                readOnly: false,
                isolationLevel: isolationLevel,
                dataContextFactory: _dataContextFactory);
        }

        public IDisposable SuppressAmbientContext()
        {
            return new AmbientContextSuppressor();
        }
    }
}
