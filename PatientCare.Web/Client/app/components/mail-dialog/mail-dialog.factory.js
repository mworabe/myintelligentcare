"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("MailDialogFactory", ["$rootScope", "$uibModal", function ($rootScope, $modal) {

        function showMailDialog(config) {
            $modal.open({
                templateUrl: "Client/app/components/mail-dialog/mail-dialog.html",
                controller: "MailDialogController",
                size: "lg",
                keyboard: false,
                resolve: {
                    dataItem: function () {
                        return config;
                    }
                }
            });
        }

        return {
            showMailDialog: function (config) {
                showMailDialog(config)
            }
        }


    }
    ]);