﻿'use strict';

angular.module("EDZoutstaffingPortalApp")
    .factory('VirgilFactory', ['$rootScope', '$timeout', 'Auth', 'ChatService', 'APP_CONFIG', function ($rootScope, $timeout, Auth, ChatService, APP_CONFIG) {
        var currentUser = null;
        var virgilClient = virgil.API(APP_CONFIG.virgilToken);
        var currentUserCard = null;
        function loadCurrentUserCard() {
            currentUser = Auth.getCurrentUser();
            virgilClient.cards.find([currentUser.nameid])
                   .then(function (userCard) {
                       currentUserCard = userCard[0];
                   });
        }

        function shutDown() {
            currentUserCard = null;
            currentUser = null;
            console.info("VirgilFactory shut down");
        }

        
        return {
            init: function () {
                loadCurrentUserCard();
            },
            shutDown: function () {
                shutDown();
            },
            getClient: function () {
                return virgilClient;
            },

            encryptData: function (message, card) {
                return virgil.crypto.encrypt(message, card.publicKey).toString("base64");;
            },

            decryptData: function (base64PrivateKey, base64Data) {
                var privateKey = virgilClient.keys.import(base64PrivateKey);
                return privateKey.decrypt(base64Data).toString();
            },

            getCurrentUserCard: function () {
                return currentUserCard;
            }
            

        }
    }]);