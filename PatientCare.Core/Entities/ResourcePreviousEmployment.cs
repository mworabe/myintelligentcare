﻿using System;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Previous Employment.
    /// </summary>
    public class ResourcePreviousEmployment :NamedEntity
    {
        public long ResourceId { get; set; }

        public virtual Resource Resource { get; set; }

        public string Address { get; set; }

        public string TitleOfPosition { get; set; }

        public string DutiesOfPosition { get; set; }

        public string ReasonForLeaving { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? NumberOfSupervised { get; set; }

        public decimal? Salary { get; set; }

        public SalaryPerTypeEnum? SalaryPer { get; set; }

        public int? HoursPerWeek { get; set; }
    }
}
