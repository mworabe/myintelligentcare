"use strict";

(function edzDateCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzDateCustomFieldOptions", [
            edzDateCustomFieldOptions
        ]);

    function edzDateCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/date/edzDateCustomField.html",
            restrict: "E",
            scope: false,
            controller: ["$scope", function ($scope) {

                function activate() {
                    $scope.validation = {};
                    $scope.validation.require = $scope.require;
                    var date = moment($scope.value);
                    if (date.format() === 'Invalid date')
                        $scope.value = undefined;
                    else $scope.value = moment($scope.value);
                }

                $scope.setDate = function (action, value) {
                    $scope.value = value;
                }
                activate();
            }]
        };
    }
})();
