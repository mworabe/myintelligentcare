namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPatientCasesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.patients_cases",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        patient_id = c.Long(),
                        patient_number = c.String(),
                        case_no = c.String(),
                        clinic_id = c.Long(),
                        clinic_location_id = c.Long(),
                        case_status = c.Int(),
                        start_date = c.DateTime(),
                        end_date = c.DateTime(),
                        birth_date = c.DateTime(),
                        age = c.Int(),
                        injury_region_id = c.Long(),
                        assigned_physician_id = c.Long(),
                        refering_physician_id = c.Long(),
                        return_to = c.DateTime(),
                        authorizeation_required = c.Int(),
                        authorizeation_number = c.String(),
                        related_cause = c.Int(),
                        primay_diagnosis_id = c.Long(),
                        secondary_diagnosis_id = c.Long(),
                        Name = c.String(),
                        created = c.DateTime(nullable: false),
                        modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.assigned_physician_id)
                .ForeignKey("dbo.clinics", t => t.clinic_id)
                .ForeignKey("dbo.clinic_locations", t => t.clinic_location_id)
                .ForeignKey("dbo.injury_regions", t => t.injury_region_id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.diagnosis", t => t.primay_diagnosis_id)
                .ForeignKey("dbo.referral_physicians", t => t.refering_physician_id)
                .ForeignKey("dbo.diagnosis", t => t.secondary_diagnosis_id)
                .Index(t => t.patient_id)
                .Index(t => t.clinic_id)
                .Index(t => t.clinic_location_id)
                .Index(t => t.injury_region_id)
                .Index(t => t.assigned_physician_id)
                .Index(t => t.refering_physician_id)
                .Index(t => t.primay_diagnosis_id)
                .Index(t => t.secondary_diagnosis_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.patients_cases", "secondary_diagnosis_id", "dbo.diagnosis");
            DropForeignKey("dbo.patients_cases", "refering_physician_id", "dbo.referral_physicians");
            DropForeignKey("dbo.patients_cases", "primay_diagnosis_id", "dbo.diagnosis");
            DropForeignKey("dbo.patients_cases", "patient_id", "dbo.patients");
            DropForeignKey("dbo.patients_cases", "injury_region_id", "dbo.injury_regions");
            DropForeignKey("dbo.patients_cases", "clinic_location_id", "dbo.clinic_locations");
            DropForeignKey("dbo.patients_cases", "clinic_id", "dbo.clinics");
            DropForeignKey("dbo.patients_cases", "assigned_physician_id", "dbo.resources");
            DropIndex("dbo.patients_cases", new[] { "secondary_diagnosis_id" });
            DropIndex("dbo.patients_cases", new[] { "primay_diagnosis_id" });
            DropIndex("dbo.patients_cases", new[] { "refering_physician_id" });
            DropIndex("dbo.patients_cases", new[] { "assigned_physician_id" });
            DropIndex("dbo.patients_cases", new[] { "injury_region_id" });
            DropIndex("dbo.patients_cases", new[] { "clinic_location_id" });
            DropIndex("dbo.patients_cases", new[] { "clinic_id" });
            DropIndex("dbo.patients_cases", new[] { "patient_id" });
            DropTable("dbo.patients_cases");
        }
    }
}
