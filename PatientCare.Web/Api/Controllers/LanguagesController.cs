﻿using System;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Languages operations.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Languages")]
    public sealed class LanguagesController : EdzPicklistApiController<Language>
    {
        /// <summary>
        /// Description : In this class we pass Languages IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="picklistRepository">Certificate Entity</param>
        public LanguagesController(IPicklistRepository<Language> picklistRepository)
            : base(picklistRepository)
        {
            GetPicklistStatusCodeErrorFunc = () => GetPicklistStatusCodeError(Picklist.Languages);
        }

        protected override Uri GetPicklistLocation(long id)
        {
            return new Uri("/Languages/" + id, UriKind.Relative);
        }
    }
}