﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ExerciseActivities;
using EmitMapper;
using PatientCare.Web.Api.Models.Exercise_injury;

namespace PatientCare.Web.Services.Mapping
{
    public static class Exercise_InjuryMapperExtensions
    {
        public static EmitMapperMapperProvider Exercise_InjuryMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise_injury, Exercise_injuryResponse>
                    (new ExtendedMapConfig<Exercise_injury, Exercise_injuryResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise_injury[], Exercise_injuryResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise_injuryRequest, Exercise_injury>
                    (new ExtendedMapConfig<Exercise_injuryRequest, Exercise_injury>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise_injuryRequest[], Exercise_injury[]>());
            return mapperProvider;
        }
    }
}