﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PostalCodes
{
    [DataContract]
    public class CodeResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string StateCode { get; set; }

        [DataMember]
        public long? StateId { get; set; }

        [DataMember]
        public string StateName { get; set; }
    }
}