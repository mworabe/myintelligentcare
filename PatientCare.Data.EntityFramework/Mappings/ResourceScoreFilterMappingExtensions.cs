﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceScoreFilterMappingExtensions
    {
        public static DbModelBuilder MapResourceScoreFilter(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceScoreFilter>().ToTable("resource_score_filter").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceScoreFilter>().Property(p => p.Name)
                .HasColumnName("name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255);

            modelBuilder.Entity<ResourceScoreFilter>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceScoreFilter>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceScoreFilter>()
                .Property(p => p.JsonFilter)
                .HasColumnName("json_filter")
                .IsRequired();

            modelBuilder.Entity<ResourceScoreFilter>()
                .Property(p => p.UserId)
                .HasColumnName("user_id")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255);

            modelBuilder.Entity<ResourceScoreFilter>()
               .Property(p => p.FilterType)
               .HasColumnName("filter_type")
               .IsOptional();
            


            return modelBuilder;
        }
    }
}
