﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ReferralPhysicians;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Data.EntityFramework;
using System.Collections.Generic;
using PatientCare.Web.Api.Models.PostalCodes;
using System.Data.Entity;
using PatientCare.Web.Api.Models.Cities;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Referral Physicians operations.
    /// </summary>
    [Authorize]
    public sealed class ReferralPhysiciansController : ApiBaseController
    {
        private PatientCareDbContext db = new PatientCareDbContext();

        /// <summary>
        /// Description : Referral Physicians IReferralService.
        /// </summary>
        private readonly IReferralService _referralService;
        /// <summary>
        /// Description : Referral Physicians IReferralPhysicianRepository.
        /// </summary>
        private readonly IReferralPhysicianRepository _referralPhysicianRepository;
        /// <summary>
        /// Description : Referral Physicians IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<ReferralPhysician> _readOptions = new ReadOptions<ReferralPhysician>();

        /// <summary>
        /// Description : Initializes a new instance of the Referral Physicians Class.  
        /// </summary>
        /// <param name="referralService">IReferral Service</param>
        /// <param name="referralPhysicianRepository">IReferral Physician Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public ReferralPhysiciansController
            (
                IReferralService referralService,
                IReferralPhysicianRepository referralPhysicianRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _referralService = referralService;
            _referralPhysicianRepository = referralPhysicianRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/referralPhysicians/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Referral Physicians.
        /// </summary>
        /// <param name="request">Referral Physicians List Request Model</param>
        /// <returns>Referral Physicians List Response Model</returns>
        [HttpGet]
        [Route("api/ReferralPhysicians/all")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ReferralPhysicianResponse>))]
        public IHttpActionResult GetAllReferralPhysician([FromUri]ReferralPhysicianListRequest request)
        {
            var query = PagingExtensions<ReferralPhysician>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            var readOptions = new ReadOptions<ReferralPhysician>().AsReadOnly();
            var page = _referralPhysicianRepository.PagedList(query, readOptions);

            var dtos = Mapper.Map<ReferralPhysician[], ReferralPhysicianResponse[]>(page.Entities.ToArray());

            foreach (var item in dtos)
            {
                using (db = new PatientCareDbContext())
                {
                    var patientCount = db.Patients.AsNoTracking().Where(x => x.ReferralPhysicianId == item.Id).Count();
                    item.ReferralsToDate = patientCount;
                    if (patientCount > 0)
                    {
                        item.LastReferralReceived = db.Patients.AsNoTracking()
                              .Where(x => x.ReferralPhysicianId == item.Id)
                              .OrderByDescending(x => x.Created)
                              .Select(x => x.Created)
                              .FirstOrDefault();
                    }
                }
            }

            var response = new ListResponse<ReferralPhysicianResponse>(dtos, page.TotalCount);



            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Referral Physicians.   
        /// </summary>
        /// <param name="referralPhysicianRequest">Referral Physicians Request Model</param>
        /// <returns>Referral Physicians Response Model</returns>
        [HttpPost]
        [Route("api/ReferralPhysicians")]
        [SwaggerResponse(201, "Created", typeof(ReferralPhysicianResponse))]
        public async Task<IHttpActionResult> CreateReferralPhysician([FromBody]ReferralPhysicianRequest referralPhysicianRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            #region Verify Country State City Zip Code Combination 

            //if (referralPhysicianRequest.CountryId != null && referralPhysicianRequest.StateId != null
            //           && referralPhysicianRequest.CityId != null && referralPhysicianRequest.PostalCode != null && referralPhysicianRequest.PostalCode != "")
            if (referralPhysicianRequest.StateId != null
                      && referralPhysicianRequest.CityId != null && referralPhysicianRequest.PostalCode != null && referralPhysicianRequest.PostalCode != "")
            {
                PostalCodeResponse code = await GetPostalCodeDetail(referralPhysicianRequest.PostalCode);
                if (code == null)
                {
                    ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    return BadRequest(ModelState);
                }
                else
                {
                    //if (code.Country != null)
                    //{
                    //    if (referralPhysicianRequest.CountryId != code.Country.Id)
                    //    {
                    //        ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    //        return BadRequest(ModelState);
                    //    }
                    //}
                    if (code.State != null)
                    {
                        if (referralPhysicianRequest.StateId != code.State.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.City != null)
                    {
                        if (referralPhysicianRequest.CityId != code.City.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }

            #endregion

            var referralPhysician = Mapper.Map<ReferralPhysicianRequest, ReferralPhysician>(referralPhysicianRequest);
            await _referralService.CreateReferralPhysicianAsync(referralPhysician);

            return Created(GetEntityLocation(referralPhysician.Id), Mapper.Map<ReferralPhysician, ReferralPhysicianResponse>(referralPhysician));
        }

        /// <summary>
        /// Method Description : Using for Update Referral Physicians.   
        /// </summary>
        /// <param name="referralPhysicianRequest">Referral Physicians Request Model</param>
        /// <returns>Referral Physicians Response Model</returns>
        [HttpPut]
        [Route("api/ReferralPhysicians/{id}")]
        public async Task<IHttpActionResult> EditReferralPhysician(long id, [FromBody] ReferralPhysicianRequest referralPhysicianRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            #region Verify Country State City Zip Code Combination 

            //if (referralPhysicianRequest.CountryId != null && referralPhysicianRequest.StateId != null
            //           && referralPhysicianRequest.CityId != null && referralPhysicianRequest.PostalCode != null && referralPhysicianRequest.PostalCode != "")
            if (referralPhysicianRequest.StateId != null
                       && referralPhysicianRequest.CityId != null && referralPhysicianRequest.PostalCode != null && referralPhysicianRequest.PostalCode != "")
            {
                PostalCodeResponse code = await GetPostalCodeDetail(referralPhysicianRequest.PostalCode);
                if (code == null)
                {
                    ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    return BadRequest(ModelState);
                }
                else
                {
                    //if (code.Country != null)
                    //{
                    //    if (referralPhysicianRequest.CountryId != code.Country.Id)
                    //    {
                    //        ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    //        return BadRequest(ModelState);
                    //    }
                    //}
                    if (code.State != null)
                    {
                        if (referralPhysicianRequest.StateId != code.State.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.City != null)
                    {
                        if (referralPhysicianRequest.CityId != code.City.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }

            #endregion

            var referralPhysician = Mapper.Map<ReferralPhysicianRequest, ReferralPhysician>(referralPhysicianRequest);
            referralPhysician.Id = id;

            await _referralService.UpdateReferralPhysicianAsync(referralPhysician);

            return Ok(Mapper.Map<ReferralPhysician, ReferralPhysicianResponse>(referralPhysician));
        }

        private async Task<PostalCodeResponse> GetPostalCodeDetail(string zipCode)
        {
            PostalCodeResponse postalCode = new PostalCodeResponse();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var query = await (from code in ctx.PostalCodes.AsNoTracking()
                                   join db_city in ctx.Cities.AsNoTracking() on code.CityId equals db_city.Id into city
                                   join db_state in ctx.States.AsNoTracking() on code.StateId equals db_state.Id into state
                                   join db_country in ctx.Countries.AsNoTracking() on code.CountryId equals db_country.Id into country

                                   from db_city in city.DefaultIfEmpty()
                                   from db_state in state.DefaultIfEmpty()
                                   from db_country in country.DefaultIfEmpty()

                                   where code.ZipCode == zipCode
                                   select new PostalCodeResponse
                                   {
                                       Id = code.Id,
                                       ZipCode = code.ZipCode,
                                       City = city.Select(c => new CodeResponse { Id = db_city.Id, Name = (db_city.Name + " - " + db_city.StateCode), StateCode = db_city.StateCode }).FirstOrDefault(),
                                       State = state.Select(c => new CodeResponse { Id = db_state.Id, Name = db_state.Name, StateCode = db_state.StateCode }).FirstOrDefault(),
                                       Country = country.Select(c => new CodeResponse { Id = db_country.Id, Name = db_country.Name, StateCode = null }).FirstOrDefault(),
                                   }).ToListAsync();

                postalCode = query.FirstOrDefault();
                return postalCode;
            }
        }

        /// <summary>
        /// Method Description : Using for Delete Referral Physicians by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Referral Physicians Response Model</returns>
        [HttpDelete]
        [Route("api/ReferralPhysicians/{id}")]
        public async Task<IHttpActionResult> DeleteReferralPhysicianItem(long id)
        {
            var item = _referralPhysicianRepository.Read(id);
            if (item == null)
                return NotFound();

            var errorMessage = await _referralService.DeleteReferralPhysicianAsync(item);
            if (string.IsNullOrEmpty(errorMessage))
                return Ok(item);

            ModelState.AddModelError("Resource", errorMessage);
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Method Description : Using for Get Referral Physicians by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Referral Physicians Response Model</returns>
        [HttpGet]
        [Route("api/ReferralPhysicians/{id}")]
        public async Task<IHttpActionResult> GetReferralPhysician(long id)
        {
            var referralphysician = await _referralService.ReadReferralPhysicianAsync(id);
            if (referralphysician == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<ReferralPhysician, ReferralPhysicianResponse>(referralphysician);

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                #region City Postal Code
                if (viewModel.CityId != null)
                {
                    var cityQuery = (from city in ctx.Cities.AsNoTracking()
                                     where city.Id == referralphysician.CityId
                                     select new CityResponse
                                     {
                                         Id = city.Id,
                                         Name = city.Name,
                                         StateId = city.StateId,
                                         StateCode = city.StateCode
                                     }).FirstOrDefault();

                    if (cityQuery != null)
                    {
                        cityQuery.State = (cityQuery.StateId != null) ? (ctx.States.AsNoTracking().Where(x => x.Id == cityQuery.StateId).Select(y => new CodeResponse { Id = y.Id, Name = y.Name }).FirstOrDefault()) : null;
                        cityQuery.StateName = (cityQuery.State != null) ? cityQuery.State.Name : cityQuery.StateCode;
                        viewModel.City = cityQuery;
                    }
                }
                #endregion

                #region zipCode
                string tempZipcode = ctx.ReferralPhysicians.AsNoTracking().Where(x => x.Id == referralphysician.Id).Select(x => x.PostalCode).FirstOrDefault();
                if (tempZipcode != null)
                {
                    var zipCodeQuery = (from code in ctx.PostalCodes.AsNoTracking()
                                        where code.ZipCode == tempZipcode
                                        select new PicklistResponse
                                        {
                                            Id = code.Id,
                                            Name = code.ZipCode
                                        }).FirstOrDefault();
                    viewModel.PostalCode = zipCodeQuery;
                }
                #endregion
            }


            return Ok(viewModel);
        }

        private static Expression<Func<ReferralPhysician, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            //return s => s.Name.Contains(searchPhrase);
            return refphysician => string.Concat(refphysician.LastName, " ", refphysician.FirstName).Contains(searchPhrase);

        }

        private static Expression<Func<ReferralPhysician, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            //return s => s.Name.Contains(searchPhrase);
            return refphysician => string.Concat(refphysician.LastName, " ", refphysician.FirstName).Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Referral Physicians.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Referral Physicians Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/ReferralPhysicians/AutoComplete")]
        [ResponseType(typeof(ReferralPhysicianAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<ReferralPhysician>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                //filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
                filter.AddFilter(x => (x.LastName + " " + x.FirstName).Contains(search.ToLower()));
            }

            var autocompleteResult = await _referralPhysicianRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<ReferralPhysician[], ReferralPhysicianAutoCompleteResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }

        /// <summary>
        /// Method Description : Using for All Get Referral Physicians.      
        /// </summary>
        /// <param name="request">Referral Physician List Request Model</param>
        /// <returns>Referral Physician Detail Response Model</returns>
        [HttpGet]
        [Route("api/ReferralPhysicians")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ReferralPhysicianDetailResponse>))]
        public async Task<IHttpActionResult> GetAllReferralPhysicianDetail([FromUri]ReferralPhysicianListRequest request)
        {
            request.PageIndex = request.PageIndex - 1;

            List<ReferralPhysicianDetailResponse> referralList = new List<ReferralPhysicianDetailResponse>();
            var response = new ListResponse<ReferralPhysicianDetailResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                //For Data
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_referral_detail_list @responseType='Data',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@sortField = '" + request.SortField + "',@sortOrder = '" + request.SortOrder + "',@searchPhrase = '" + request.SearchPhrase + "'";
                var referralDataList = await ctx.Database.SqlQuery<ReferralPhysicianDetailResponse>(query).ToListAsync();
                response.Data = referralDataList.ToArray();

                //For Item Count                
                string itemCountQuery = "EXEC sp_referral_detail_list @responseType='DataCount',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@sortField = '" + request.SortField + "',@sortOrder = '" + request.SortOrder + "',@searchPhrase = '" + request.SearchPhrase + "'";
                response.ItemsCount = await ctx.Database.SqlQuery<long>(itemCountQuery).FirstOrDefaultAsync();

                return Ok(response);
            }
        }
    }
}
