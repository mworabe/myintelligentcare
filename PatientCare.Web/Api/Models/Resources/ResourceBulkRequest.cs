﻿using System;
using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.ResourceCertificates;
using PatientCare.Web.Api.Models.ResourceCriminalRecords;
using PatientCare.Web.Api.Models.ResourceEducations;
using PatientCare.Web.Api.Models.ResourceWorkExperience;
using PatientCare.Web.Api.Models.ResourcePreviousEmployments;
using PatientCare.Web.Api.Models.ResourceSkills;
using PatientCare.Web.Api.Models.ResourceSpokenLanguages;
using PatientCare.Web.Api.Models.ResourceTravels;

namespace PatientCare.Web.Api.Models.Resources
{
    public class ResourceBulkRequest
    {
        public long[] Ids{ get; set; }
        public ResourceBulkRequestModel Model { get; set; }
    }

    public class ResourceBulkRequestModel:ResourceRequest
    { }
}