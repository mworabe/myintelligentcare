﻿using System;
using System.Data;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Services
{
    /// <summary>
    ///  This Service is used to handle configuration Data operations And implementing all methods which is initialize in IConfigurationDataService.
    /// </summary>
    public class ConfigurationDataService : IConfigurationDataService
    {
        /// <summary>
        ///Description : Configuration Data Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Configuration Data Service Field IInjuryRegionRepository
        /// </summary>
        private readonly IInjuryRegionRepository _injuryRegionRepository;

        /// <summary>
        ///Description : Configuration Data Service Field IDiagnosisRepository
        /// </summary>
        private readonly IDiagnosisRepository _diagnosisRepository;

        /// <summary>
        ///Description : Configuration Data Service Field IExercisePositionRepository
        /// </summary>
        private readonly IExercisePositionRepository _exercisePositionRepository;

        /// <summary>
        ///Description : Configuration Data Service Field IExerciseActivityRepository
        /// </summary>
        private readonly IExerciseActivityRepository _exerciseActivityRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Configuration Data class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="injuryRegionRepository">Injury Region Repository</param>
        /// <param name="diagnosisRepository">Diagnosis Repository</param>
        /// <param name="exercisePositionRepository">Exercise Position Repository</param>
        /// <param name="exerciseActivityRepository">Exercise Activity Repository</param>
        public ConfigurationDataService
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IInjuryRegionRepository injuryRegionRepository,
                IDiagnosisRepository diagnosisRepository,
                IExercisePositionRepository exercisePositionRepository,
                IExerciseActivityRepository exerciseActivityRepository
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _injuryRegionRepository = injuryRegionRepository;
            _diagnosisRepository = diagnosisRepository;
            _exercisePositionRepository = exercisePositionRepository;
            _exerciseActivityRepository = exerciseActivityRepository;
        }

        #region InjuryRegion Methods

        /// <summary>
        /// Method Description : Using for Update InjuryRegion data.
        /// </summary>
        /// <param name="InjuryRegion">Injury Region Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateInjuryRegionAsync(InjuryRegion InjuryRegion)
        {
            if (InjuryRegion == null) throw new ArgumentNullException("clininc");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originLeave = _injuryRegionRepository.Read(InjuryRegion.Id, new ReadOptions<InjuryRegion>().AsReadOnly());

                await _injuryRegionRepository.UpdateAsync(InjuryRegion, UpdateOptions.Create<InjuryRegion>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new InjuryRegion.
        /// </summary>
        /// <param name="InjuryRegion">Injury Region Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateInjuryRegionAsync(InjuryRegion InjuryRegion)
        {
            if (InjuryRegion == null) throw new ArgumentNullException("InjuryRegion");

            await _injuryRegionRepository.CreateAsync(InjuryRegion, UpdateOptions.Create<InjuryRegion>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Injury Region data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Injury Region Entity</returns>
        public async Task<InjuryRegion> ReadInjuryRegionAsync(long id)
        {
            var readOptions = new ReadOptions<InjuryRegion>().AsReadOnly();

            var InjuryRegion = await _injuryRegionRepository.ReadAsync(id, readOptions);

            return InjuryRegion;

        }

        /// <summary>
        /// Method Description : Using for Injury Region data.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteInjuryRegionAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var InjuryRegion = _injuryRegionRepository.Read(id);

                if (InjuryRegion != null)
                {
                    _injuryRegionRepository.Delete(InjuryRegion);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region InjuryRegion Location Methods

        /// <summary>
        /// Method Description : Using for Update Diagnosis data.
        /// </summary>
        /// <param name="Diagnosis">Diagnosis Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateDiagnosisAsync(Diagnosis Diagnosis)
        {
            if (Diagnosis == null) throw new ArgumentNullException("clininclocation");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originLeave = _diagnosisRepository.Read(Diagnosis.Id, new ReadOptions<Diagnosis>().AsReadOnly());

                await _diagnosisRepository.UpdateAsync(Diagnosis, UpdateOptions.Create<Diagnosis>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new Diagnosis.
        /// </summary>
        /// <param name="Diagnosis">Diagnosis Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateDiagnosisAsync(Diagnosis Diagnosis)
        {
            if (Diagnosis == null) throw new ArgumentNullException("Diagnosiss");

            Diagnosis.Created = DateTime.UtcNow;
            await _diagnosisRepository.CreateAsync(Diagnosis, UpdateOptions.Create<Diagnosis>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Diagnosis data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Diagnosis Entity</returns>
        public async Task<Diagnosis> ReadDiagnosisAsync(long id)
        {
            var readOptions = new ReadOptions<Diagnosis>().AsReadOnly();

            var Diagnosis = await _diagnosisRepository.ReadAsync(id, readOptions);

            return Diagnosis;

        }

        /// <summary>
        /// Method Description : Using for Diagnosis data.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteDiagnosisAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var Diagnosis = _diagnosisRepository.Read(id);

                if (Diagnosis != null)
                {
                    _diagnosisRepository.Delete(Diagnosis);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region ExercisePosition Methods

        /// <summary>
        /// Method Description : Using for Update Exercise Position data.
        /// </summary>
        /// <param name="exercisePosition">Exercise Position Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateExercisePositionAsync(ExercisePosition exercisePosition)
        {
            if (exercisePosition == null) throw new ArgumentNullException("exercisePosition");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originLeave = _exercisePositionRepository.Read(exercisePosition.Id, new ReadOptions<ExercisePosition>().AsReadOnly());

                await _exercisePositionRepository.UpdateAsync(exercisePosition, UpdateOptions.Create<ExercisePosition>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new Exercise Position.
        /// </summary>
        /// <param name="exercisePosition">Exercise Position Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateExercisePositionAsync(ExercisePosition exercisePosition)
        {
            if (exercisePosition == null) throw new ArgumentNullException("exercisePosition");

            await _exercisePositionRepository.CreateAsync(exercisePosition, UpdateOptions.Create<ExercisePosition>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Exercise Position data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Exercise Position Entity</returns>
        public async Task<ExercisePosition> ReadExercisePositionAsync(long id)
        {
            var readOptions = new ReadOptions<ExercisePosition>().AsReadOnly();

            var exercisePosition = await _exercisePositionRepository.ReadAsync(id, readOptions);

            return exercisePosition;

        }

        /// <summary>
        /// Method Description : Using for Delete Exercise Position data.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteExercisePositionAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var exercisePosition = _exercisePositionRepository.Read(id);

                if (exercisePosition != null)
                {
                    _exercisePositionRepository.Delete(exercisePosition);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region ExerciseActivity Methods

        /// <summary>
        /// Method Description : Using for Update Exercise Activity data.
        /// </summary>
        /// <param name="exerciseActivity">Exercise Activity Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateExerciseActivityAsync(ExerciseActivity exerciseActivity)
        {
            if (exerciseActivity == null) throw new ArgumentNullException("exerciseActivity");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originLeave = _exerciseActivityRepository.Read(exerciseActivity.Id, new ReadOptions<ExerciseActivity>().AsReadOnly());

                await _exerciseActivityRepository.UpdateAsync(exerciseActivity, UpdateOptions.Create<ExerciseActivity>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new Exercise Activity.
        /// </summary>
        /// <param name="exerciseActivity">Exercise Activity Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateExerciseActivityAsync(ExerciseActivity exerciseActivity)
        {
            if (exerciseActivity == null) throw new ArgumentNullException("exercisePosition");

            await _exerciseActivityRepository.CreateAsync(exerciseActivity, UpdateOptions.Create<ExerciseActivity>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Exercise Activity data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Exercise Activity Entity</returns>
        public async Task<ExerciseActivity> ReadExerciseActivityAsync(long id)
        {
            var readOptions = new ReadOptions<ExerciseActivity>().AsReadOnly();

            var exerciseActivity = await _exerciseActivityRepository.ReadAsync(id, readOptions);

            return exerciseActivity;

        }

        /// <summary>
        /// Method Description : Using for Exercise Activity data.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteExerciseActivityAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var exerciseActivity = _exerciseActivityRepository.Read(id);

                if (exerciseActivity != null)
                {
                    _exerciseActivityRepository.Delete(exerciseActivity);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        #endregion
    }
}
