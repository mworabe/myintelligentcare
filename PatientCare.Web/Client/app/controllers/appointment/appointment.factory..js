"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("AppointmentFactory", ["$uibModal", function ($uibModal) {

        var APPOINTMENT_STATUS = [
            { id: 0, title: "Confirmed", icon: 'fa-thumbs-o-up', key: 'Confirmed', boxClass: "status-box-blue" },
            { id: 1, title: "Checked In", icon: "fa-sign-out", key: 'CheckedIn', boxClass: "status-box-yellow" },        
            { id: 3, title: "Cancelled", icon: 'fa-ban', key: 'Cancelled', boxClass: "status-box-red" },
            { id: 4, title: "No Show", icon: 'fa-reply-all', key: 'NoShow', boxClass: "status-box-pink" },
            { id: 5, title: "Complete", icon: 'fa-check', key: 'Complete', boxClass: "status-box-green" },
            { id: 7, title: "Request", icon: 'fa-clock-o', key: 'Request', boxClass: "status-box-purple" },
        ];

        var APPOINTMENT_TYPE = [{ id: 0, title: "Custom: Pilates", icon: 'fa fa-tags' },
        { id: 1, title: "Initial Evaluation", icon: 'fa fa-sliders' },
        { id: 2, title: "Re-Examination", icon: 'fa fa-bar-chart' },
        { id: 3, title: "Dry Needle", icon: 'fa fa-adjust' },
        { id: 4, title: "Consultation", icon: 'fa fa-handshake-o' },
        { id: 5, title: "After Care Session", icon: 'fa fa-balance-scale' },
        { id: 6, title: "Encounter Session", icon: 'fa fa-retweet' },
        { id: 7, title: "Custom: Running", icon: 'fa fa-files-o' },
        { id: 8, title: "progression", icon: 'fa fa-tint' },
        { id: 9, title: "Progress Report/Note", icon: 'fa fa-pencil' }
        ];

        return {
            newAppointment: function (args, modalListener) {
                $uibModal.open({
                    backdrop: "static",
                    templateUrl: "Client/app/controllers/appointment/add-appointment-modal-template.html",
                    resolve: {
                        data: args
                    },
                    controller: "AddAppoinmentCtrl as AddAppoinmentCtrl",
                }).result.then(modalListener);
            },

            getAllStatus: function () {
                return APPOINTMENT_STATUS;
            },

            getStatusFromId: function (id) {
                for (var i = 0; i < APPOINTMENT_STATUS.length; i++) {
                    var obj = APPOINTMENT_STATUS[i];
                    if (obj.id == id)
                        return obj;
                }
                return null;
            },
            getStatusFromText: function (key) {
                for (var i = 0; i < APPOINTMENT_STATUS.length; i++) {
                    var obj = APPOINTMENT_STATUS[i];
                    if (obj.key == key)
                        return obj;
                }
                return null;
            },
            getTypeFromText: function (key) {
                for (var i = 0; i < APPOINTMENT_TYPE.length; i++) {
                    var obj = APPOINTMENT_TYPE[i];
                    if (obj.title == key)
                        return obj;
                }
                return null;
            },
        }
    }]);