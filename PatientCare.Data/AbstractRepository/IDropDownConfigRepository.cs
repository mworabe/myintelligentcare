﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IDropDownConfigRepository : IRepository<DropDownConfig>
    {
    }
}
