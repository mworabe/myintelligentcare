﻿namespace PatientCare.Core.Services.Dashboards
{
    public class RestrictionDataModel
    {
        public long? DepartmentId { get; set; }
        public long? DivisionId { get; set; }
        public bool ProjectsOnly { get; set; }
        public long? ResourceId { get; set; }
    }
}
