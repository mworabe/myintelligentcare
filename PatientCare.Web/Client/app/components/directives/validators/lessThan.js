﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngLessThan", [
        function () {
            return {
                restrict: "A",
                require: "ngModel",
                link: function ($scope, $element, $attrs, ctrl) {
                    var validate = function(viewValue) {
                        var comparisonModel = $attrs.ngLessThan;

                        if(!viewValue || !comparisonModel){
                            // It's valid because we have nothing to compare against
                            ctrl.$setValidity('lessThan', true);
                        }

                        $scope.$watch(comparisonModel, setValidity);

                        function setValidity(toBeLessThan) {
                            if(toBeLessThan) {
                                ctrl.$setValidity('lessThan', Number(viewValue) < Number(toBeLessThan));
                            }
                        }

                        return viewValue;
                    };

                    ctrl.$parsers.unshift(validate);
                    ctrl.$formatters.push(validate);
                }
            };
        }
    ]);