﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IHepPatientPlanRepository : IRepository<HepPatientsPlan>
    {
    }
}
