﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.ApiMobile.Models.Patients;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Web.ApiMobile.Models.PatientWorkoutAnalytics;
using PatientCare.Web.Hubs;
using PatientCare.Data.EntityFramework;
using System.Linq;
using System.Data.Entity;
using Newtonsoft.Json;
using PatientCare.Web.Services.FileStorage;
using System.Net.Http;
using System.Net;
using System.IO;
using PatientCare.Web.Api.Controllers;
using PatientCare.Web.Api.Models.PatientCases;
using System.Collections.Generic;
using PatientCare.Web.ApiMobile.Models;
using PatientCare.Data.EntityFramework.Identity;

namespace PatientCare.Web.ApiMobile.Controllers
{
    /// <summary>
    /// This Controller is used to handle Patient Mobile operations.
    /// </summary>
    //[Authorize(Roles = ApplicationUser.PatientRole)]
    public sealed class PatientsMobileController : ApiBaseController
    {
        /// <summary>
        /// Description : Patient IPatientService.
        /// </summary>
        private readonly IPatientService _patientService;
        /// <summary>
        /// Description : Patient IPatientRepository.
        /// </summary>
        private readonly IPatientRepository _patientRepository;
        /// <summary>
        /// Description : Patient IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : Patient IPatientWorkoutAnalyticRepository.
        /// </summary>
        private readonly IPatientWorkoutAnalyticRepository _patientWorkoutAnalyticRepository;
        /// <summary>
        /// Description : Patient IMobileService.
        /// </summary>
        private readonly IMobileService _mobileService;
        /// <summary>
        /// Description : Patient IPatientMobileProfileRepository.
        /// </summary>
        private readonly IPatientMobileProfileRepository _patientMobileProfileRepository;

        ReadOptions<Patient> _readOptions = new ReadOptions<Patient>();

        /// <summary>
        /// Description : Initializes a new instance of the Patient Mobile Class.  
        /// </summary>
        /// <param name="patientService">IPatient Service</param>
        /// <param name="patientRepository">IPatient Repository</param>
        /// <param name="dataContextScopeFactory">IDataContext Scope Factory</param>
        /// <param name="patientWorkoutAnalyticRepository">IPatient Workout Analytic Repository</param>
        /// <param name="mobileService">IMobile Service</param>
        /// <param name="patientMobileProfileRepository">IPatient Mobile Profile Repository</param>
        public PatientsMobileController
            (
                IPatientService patientService,
                IPatientRepository patientRepository,
                IDataContextScopeFactory dataContextScopeFactory,
                IPatientWorkoutAnalyticRepository patientWorkoutAnalyticRepository,
                IMobileService mobileService,
                IPatientMobileProfileRepository patientMobileProfileRepository
            )
        {
            _patientService = patientService;
            _patientRepository = patientRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
            _patientWorkoutAnalyticRepository = patientWorkoutAnalyticRepository;
            _mobileService = mobileService;
            _patientMobileProfileRepository = patientMobileProfileRepository;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Patients/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Update Patient.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="patientMobileRequest">Patient Mobile Request Model</param>
        /// <returns>Patient Mobile Response Model</returns>
        [HttpPut]
        [Route("api/mobile/v1/Patients/{id}")]
        public async Task<IHttpActionResult> EditPatient(long id, [FromBody] PatientMobileRequest patientMobileRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var patient = await _patientService.ReadPatientAsync(id);
            if (patient == null)
                return NotFound();

            patient.FirstName = patientMobileRequest.FirstName;
            patient.MiddleName = patientMobileRequest.MiddleName;
            patient.LastName = patientMobileRequest.LastName;
            patient.Prefix = patientMobileRequest.Prefix;
            patient.NickName = patientMobileRequest.NickName;
            patient.BirthDate = patientMobileRequest.BirthDate;

            patient.SSN = patientMobileRequest.SSN;
            patient.Gender = patientMobileRequest.Gender;
            patient.Email = patientMobileRequest.Email;
            patient.CellNumber = patientMobileRequest.CellNumber;
            patient.LanguageId = patientMobileRequest.LanguageId;
            patient.MaritialStatus = patientMobileRequest.MaritialStatus;
            patient.EmergencyContact = patientMobileRequest.EmergencyContact;
            patient.RelationshipToContact = patientMobileRequest.RelationshipToContact;
            patient.EmergencyPhone = patientMobileRequest.EmergencyPhone;

            await _patientService.UpdatePatientAsync(patient);

            return Ok(Mapper.Map<Patient, PatientMobileResponse>(patient));
        }

        /// <summary>
        /// Method Description : Using for Get Patients by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patients Mobile Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/Patients/{id}")]
        public async Task<IHttpActionResult> GetPatient(long id)
        {
            var patient = await _patientService.ReadPatientAsync(id);
            if (patient == null)
                return NotFound();

            var viewModel = Mapper.Map<Patient, PatientMobileResponse>(patient);
            return Ok(viewModel);
        }

        /// <summary>
        /// Method Description : Using for Create/Update Patient Workout.   
        /// </summary>
        /// <param name="patientWorkoutAnalytic">Patient Mobile Request Model</param>
        /// <returns>Patient Workout Analytic Entity</returns>
        [HttpPost]
        [Route("api/mobile/v1/PatientsWorkout")]
        public async Task<IHttpActionResult> PatientsWorkout([FromBody] PatientWorkoutAnalyticRequest patientWorkoutAnalytic)
        {
            bool isUpdate = patientWorkoutAnalytic.Id > 0 ? true : false;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var patientWorkout = Mapper.Map<PatientWorkoutAnalyticRequest, PatientWorkoutAnalytic>(patientWorkoutAnalytic);
            await _patientService.InsertUpdatePatientWorkoutAnalyticAsync(patientWorkout);

            var Patient = await _patientService.ReadPatientAsync(patientWorkoutAnalytic.PatientId.Value);
            NotificationHelper notifHelper = new NotificationHelper(_dataContextScopeFactory);

            if (patientWorkoutAnalytic.StartDate != null && patientWorkoutAnalytic.EndDate == null)
            {
                if (!isUpdate && patientWorkout.PreExercisesPainRating >= 8)
                {
                    string title = Patient.FirstName + " " + Patient.LastName + " has Reported high pain level (" + patientWorkout.PreExercisesPainRating + ")";
                    await notifHelper.sendPainRatingNotif(title, Patient);
                }
            }

            return Ok(patientWorkout);
        }

        /// <summary>
        /// Method Description : Using for Get Patients Profile by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patient Mobile Profile Mobile Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/GetPatientProfile/{id}")]// patientId
        public IHttpActionResult GetPatientProfile(long id)
        {
            var response = new PatientMobileProfileMobileResponse();
            LoginHistory tempClass = new LoginHistory();
            response = tempClass.GetPatientProfileData(id);
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Update Patient Profile.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="patientMobileProfileRequest">Patient Mobile Profile Request Model</param>
        /// <returns>Mobile Error Response Model</returns>
        [HttpPut]
        [Route("api/mobile/v1/PatientProfile/{id}")]// patientId
        public async Task<IHttpActionResult> PatientProfile(long id, [FromBody] PatientMobileProfileMobileRequest patientMobileProfileRequest)
        {
            MobileErrorResponse res = new MobileErrorResponse();
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new Exception("Data is missing.");
                }

                var mobileProfile = new PatientMobileProfile();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbSet = ctx.Set<PatientMobileProfile>();
                    mobileProfile = await dbSet.AsNoTracking().Where(x => x.PatientId == id).FirstOrDefaultAsync();
                }

                if (mobileProfile != null)
                {
                    var mainProfile = Mapper.Map<PatientMobileProfileMobileRequest, PatientMobileProfile>(patientMobileProfileRequest);
                    mainProfile.Id = mobileProfile.Id;

                    #region Updated Items from Mobile
                    mainProfile.FullName = patientMobileProfileRequest.PatientFullName;
                    mainProfile.Language = patientMobileProfileRequest.PatientLanguage;
                    mainProfile.PatientId = id;
                    if (mainProfile.Goals != null)
                    {
                        foreach (var item in mainProfile.Goals.ToList())
                        {
                            item.PatientId = mainProfile.PatientId;
                        }
                    }
                    #endregion

                    mainProfile.City = mobileProfile.City;
                    mainProfile.State = mobileProfile.State;
                    mainProfile.Country = mobileProfile.Country;
                    mainProfile.Email = mobileProfile.Email;
                    mainProfile.Phone = mobileProfile.Phone;
                    mainProfile.Photo = mobileProfile.Photo;

                    await _mobileService.InsertUpdatePatientMobileProfileAsync(mainProfile);
                }
                else
                {
                    res.Type = ErrorTypeEnum.Error;
                    res.Message = "No data available.";
                    //Here we are not created Patient Mobile Profile because if patient is not on web then no need to create mobile profile

                    /*var patientMobileDetail = Mapper.Map<PatientMobileProfileMobileRequest, PatientMobileProfile>(patientMobileProfileRequest);
                    patientMobileDetail.Id = 0;
                    patientMobileDetail.FullName = patientMobileProfileRequest.PatientFullName;
                    patientMobileDetail.Language = patientMobileProfileRequest.PatientLanguage;
                    patientMobileDetail.PatientId = id;
                    foreach (var item in patientMobileDetail.Goals.ToList())
                    {
                        item.PatientId = patientMobileDetail.PatientId;
                    }
                    await _mobileService.InsertUpdatePatientMobileProfileAsync(patientMobileDetail);*/

                }

                var response = new PatientMobileProfileMobileResponse();
                LoginHistory tempClass = new LoginHistory();
                response = tempClass.GetPatientProfileData(id);

                res.Type = ErrorTypeEnum.Success;
                res.Message = "Data is updated.";
            }
            catch (Exception ex)
            {
                res.Type = ErrorTypeEnum.Error;
                res.Message = ex.Message.ToString();
            }
            return Ok(res);
        }

        /// <summary>
        /// Method Description : Using for Upload Photo.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Mobile Error Response Model</returns>
        [HttpPost]
        [Route("api/mobile/v1/UploadPhoto/{id}")]//here id is patient Id
        public async Task<IHttpActionResult> UploadPhoto(long id)
        {
            MobileErrorResponse res = new MobileErrorResponse();
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new Exception("You are uploading unsupported MediaType.");
                    //return StatusCode(HttpStatusCode.UnsupportedMediaType);
                }

                if (!_patientMobileProfileRepository.Exists(FilterQuery.Create<PatientMobileProfile>().AddFilter(p => p.PatientId == id)))
                {
                    throw new Exception("The patient doesn't exists");
                    //ModelState.AddModelError("id", "The patient doesn't exists");
                    //return BadRequest(ModelState);
                }

                var result = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());

                var originalFileName = result.Files.First().Headers.ContentDisposition.FileName;
                var ext = Path.GetExtension(JsonConvert.DeserializeObject(originalFileName).ToString());
                var newfileName = string.Format(FileUploadHelpers.PatientMobilePhotoFileNameTemplate, id, ext).ToLower();

                var file = result.Files[0];
                var fileStream = await file.ReadAsStreamAsync();

                var wasS3UploadSuccessful = await FileUploadHelpers.S3PatientPhotoUploadAsync(newfileName, fileStream);

                if (!wasS3UploadSuccessful)
                    return BadRequest();

                #region Default Upload Photo from mobile
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbset = ctx.Set<PatientMobileProfile>();
                    var patientProfileDetail = ctx.PatientMobileProfiles.Where(x => x.PatientId == id).FirstOrDefault();
                    if ((patientProfileDetail != null))
                    {
                        string updatePhotoQuery = "update patient_mobile_profiles set photo = N'" + newfileName + "' where patient_Id = " + patientProfileDetail.PatientId;
                        ctx.Database.ExecuteSqlCommand(updatePhotoQuery);
                    }
                }
                res.Type = ErrorTypeEnum.Success;
                res.Message = "Your profile photo is updated.";
            }
            catch (Exception ex)
            {
                res.Type = ErrorTypeEnum.Error;
                res.Message = ex.Message.ToString();
            }
            #endregion
            return Ok(res);
        }

        /// <summary>
        /// Method Description : Using for Upload File.   
        /// </summary>
        /// <returns>Mobile Error Response Model</returns>
        [HttpPost]
        [Route("api/mobile/v1/UploadFile")]
        public async Task<IHttpActionResult> UploadFile()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            var result = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());
            var originalFileName = result.Files.First().Headers.ContentDisposition.FileName;
            originalFileName = originalFileName.Substring(1, originalFileName.Length - 2);
            var newfileName = string.Format(FileUploadHelpers.FileNameTemplate, Guid.NewGuid(), originalFileName).ToLower();

            var file = result.Files[0];
            var fileStream = await file.ReadAsStreamAsync();

            var wasS3UploadSuccessful = await FileUploadHelpers.S3UploadAsync(newfileName, fileStream, "temp");

            if (wasS3UploadSuccessful)
            {
                return Ok(new
                {
                    FileName = newfileName
                });
            }
            return BadRequest();
        }

        /// <summary>
        /// Method Description : Using for Delete Patient Profile Photo.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Dictionary</returns>
        [HttpDelete]
        [Route("api/mobile/v1/PatientProfilePhoto/{id}")] // patientId
        public async Task<IHttpActionResult> PatientProfilePhotoDelete(long id)
        {
            Dictionary<string, string> response = new Dictionary<string, string>();

            try
            {
                using (var dbContextScope2 = _dataContextScopeFactory.Create())
                {
                    var ctx2 = dbContextScope2.DataContexts.Get<PatientCareDbContext>();
                    var profile = ctx2.PatientMobileProfiles.FirstOrDefault(x => x.PatientId == id);
                    if (profile == null)
                        return Ok();

                    profile.Photo = null;
                    ctx2.SaveChanges();

                    response.Add("Status","True");
                    response.Add("Message", "Profile Photo Deleted.");
                }
            }
            catch (Exception ex)
            {
                response.Add("Status", "True");
                response.Add("Message", ex.Message);
                return Ok(response);
            }
            return Ok(response);
        }
    }
}