﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using PatientCare.Services.Validation;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle custom field operations And implementing all methods which is initialize in ICustomFieldService.
    /// </summary>
    public class CustomFieldService : ICustomFieldService
    {
        /// <summary>
        ///Description : Custom Field Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Custom Field Service Field ICustomFieldRepository
        /// </summary>
        private readonly ICustomFieldRepository _customFieldRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Custom Field class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="customFieldRepository">Custom Field Repository</param>
        public CustomFieldService(IDataContextScopeFactory dataContextScopeFactory,
            ICustomFieldRepository customFieldRepository)
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _customFieldRepository = customFieldRepository;
        }

        /// <summary>
        /// Method Description : Using for get Custom field of Resource.
        /// </summary>
        /// <returns>Resource Custom Field List</returns>
        public async Task<IList<ResourceCustomFieldValue>> CustomFieldValuesForResource()
        {
            var customFields = await _customFieldRepository.ListAsync(new FilterQuery<CustomField>()
                .AddFilter(x => x.ResourceAvailable),
                new ReadOptions<CustomField>().WithIncludePredicate(x => x.CustomFieldProperties)
                 );
            return customFields.Select(customField => new ResourceCustomFieldValue
            {
                CustomField = customField,
                CustomFieldId = customField.Id
            }).ToList();
        }

        /// <summary>
        /// Method Description : Using for Update Custom Field data.
        /// </summary>
        /// <param name="customField">Custom Field Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateAsync(CustomField customField)
        {
            if (customField == null) throw new ArgumentNullException("customField");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                ResourceChidlrenProcessing(ctx, customField);
                await UpdateValues(ctx, customField);

                await _customFieldRepository.UpdateAsync(customField, UpdateOptions.Create<CustomField>().WithNotModified(p => p.Created));
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Update Custom Field value.
        /// </summary>
        /// <param name="ctx">Patient Care DbContext</param>
        /// <param name="customField">Custom Field Entity</param>
        /// <returns>Returns True/False</returns>
        private async static Task<bool> UpdateValues(PatientCareDbContext ctx, CustomField customField)
        {
            var oldCustomField = await ctx.CustomFields.AsNoTracking().FirstOrDefaultAsync(x => x.Id == customField.Id);            
            var resourceIds = new List<long>();

            if (oldCustomField.ResourceAvailable != customField.ResourceAvailable && !customField.ResourceAvailable)
            {
                foreach (var s in ctx.ResourceCustomFieldValues.Where(x => x.CustomFieldId == customField.Id))
                {
                    ctx.Entry(s).State = EntityState.Deleted;
                    resourceIds.Add(s.Id);
                }
            }
            
            if (oldCustomField.Type != customField.Type || !CustomFieldValidation.CompareValidationField(oldCustomField, customField))
            {
                foreach (
                    var resourceCustomFieldValue in
                        ctx.ResourceCustomFieldValues.Where(x => x.CustomFieldId == customField.Id && !resourceIds.Contains(x.Id)))
                {
                    var result = CustomFieldValidation.Validation(customField, resourceCustomFieldValue.Value);
                    if (result != null)
                        ctx.Entry(resourceCustomFieldValue).State = EntityState.Deleted;
                }                
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Update Custom Field Properties .
        /// </summary>
        /// <param name="ctx">DbContext</param>
        /// <param name="customField">Custom Field Entity</param>
        private static void ResourceChidlrenProcessing(DbContext ctx, CustomField customField)
        {
            var customFieldProperties = customField.CustomFieldProperties;
            ctx.ChildrenProcessing(customFieldProperties, x => x.CustomFieldId == customField.Id);
        }
    }
}
