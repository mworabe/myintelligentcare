﻿namespace PatientCare.Web.ApiMobile.Models.Ratings
{
    public class ExerciseRatingMobileRequest
    {
        public long Id { get; set; }

        public long? ExerciseId { get; set; }

        public long? HepDetailId { get; set; }

        public long? PatientId { get; set; }

        public long? PatientCaseId { get; set; }

        public int? Rating { get; set; }        
    }   
}