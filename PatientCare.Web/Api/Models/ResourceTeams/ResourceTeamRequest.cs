﻿namespace PatientCare.Web.Api.Models.ResourceTeams
{
    public class ResourceTeamRequest
    {
        public string Name { get; set; }

        public long ResourceCompanyId { get; set; }
    }
}