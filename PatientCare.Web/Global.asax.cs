﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using PatientCare.Web.Services.Scheduler;
using FluentValidation.WebApi;
using System.IO;

namespace PatientCare.Web
{
    public class IrmsApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            FluentValidationModelValidatorProvider.Configure(GlobalConfiguration.Configuration);

            //JobScheduler.Start(GlobalConfiguration.Configuration.DependencyResolver);

            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));
        }
    }
}
