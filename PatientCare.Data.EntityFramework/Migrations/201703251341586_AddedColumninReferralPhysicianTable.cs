namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedColumninReferralPhysicianTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.referral_physicians", "postal_code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.referral_physicians", "postal_code");
        }
    }
}
