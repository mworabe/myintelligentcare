﻿/// <reference path="login-history.html" />
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("login-history", {
                url: "/login-history",
                templateUrl: "Client/app/controllers/login-history/login-history.html",
                controller: "LoginHistoryCtrl as LoginHistoryCtrl",
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });
