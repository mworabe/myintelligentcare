﻿using Microsoft.AspNet.SignalR;
using PatientCare.Web.Api.Models.Schedulers;

namespace PatientCare.Web.Hubs
{
    public class AppointmentBroadCasterHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello("Hiren");
        }

        public void sendUpdate(SchedulerListResponse appointments)
        {
            Clients.All.broadcastAppointmentUpdate(appointments);
        }
    }
}