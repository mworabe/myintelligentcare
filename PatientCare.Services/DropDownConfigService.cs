﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using System.Collections.Generic;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle drop down config operations And implementing all methods which is initialize in IDropDownConfigService.
    /// </summary>
    public class DropDownConfigService : IDropDownConfigService
    {
        /// <summary>
        ///Description : Configuration Data Service Field IInjuryRegionRepository
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Configuration Data Service Field IDropDownConfigRepository
        /// </summary>
        private readonly IDropDownConfigRepository _dropDownConfigRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Dropdown Configuration Data class.
        /// </summary>
        /// <param name="dataContextScopeFactory"></param>
        /// <param name="dropDownConfigRepository"></param>
        public DropDownConfigService(
                IDataContextScopeFactory dataContextScopeFactory,
                IDropDownConfigRepository dropDownConfigRepository)
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _dropDownConfigRepository = dropDownConfigRepository;
        }

        /// <summary>
        /// Method Description : Using for Update Dropdown configuration data.
        /// </summary>
        /// <param name="dropDownConfig">DropDown Config Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateDropDownConfigAsync(DropDownConfig dropDownConfig)
        {
            if (dropDownConfig == null) throw new ArgumentNullException("dropDownConfigs");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originDropDownConfig = _dropDownConfigRepository.Read(dropDownConfig.Id, new ReadOptions<DropDownConfig>().AsReadOnly());

                await _dropDownConfigRepository.UpdateAsync(dropDownConfig, UpdateOptions.Create<DropDownConfig>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new Dropdown configuration data.
        /// </summary>
        /// <param name="dropDownConfig">DropDown Config Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateDropDownConfigAsync(DropDownConfig dropDownConfig)
        {
            if (dropDownConfig == null) throw new ArgumentNullException("dropDownConfigs");

            await _dropDownConfigRepository.CreateAsync(dropDownConfig, UpdateOptions.Create<DropDownConfig>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for Dropdown configuration data.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteDropDownConfigItemAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var dropDownConfigItem = _dropDownConfigRepository.Read(id);

                if (dropDownConfigItem != null)
                {
                    _dropDownConfigRepository.Delete(dropDownConfigItem);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Method Description : Using for get all Dropdown configuration data.
        /// </summary>
        /// <returns>DropDown Config List</returns>
        public List<DropDownConfig> ReadAllDropDownConfigAsync()
        {
            var dropDownConfigList = new List<DropDownConfig>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                var dbSet = ctx.Set<DropDownConfig>();
                dropDownConfigList = dbSet.ToList();
            }
            return dropDownConfigList;
        }

        /// <summary>
        /// Method Description : Using for get Dropdown configuration data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>DropDown Config Entity</returns>
        public async Task<DropDownConfig> ReadDropDownConfigAsync(long id)
        {
            var readOptions = new ReadOptions<DropDownConfig>().AsReadOnly();

            var dropDownConfig = await _dropDownConfigRepository.ReadAsync(id, readOptions);

            return dropDownConfig;
        }

        /// <summary>
        /// Method Description : Using for get Dropdown data by search parameter
        /// </summary>
        /// <param name="dropDownType">DropDown Type</param>
        /// <param name="search">Search</param>
        /// <param name="searchField">Search Field</param>
        /// <param name="searchParameter">Search Parameter</param>
        /// <param name="activityTypeId">Activity Type Id</param>
        /// <returns></returns>
        public List<DropDownConfig> ReadDropDownTypeAsync(string dropDownType, string search, string searchField = null, string searchParameter = null, long? activityTypeId = null)
        {
            var dropDownConfigList = new List<DropDownConfig>();
            if (dropDownType != null || dropDownType != "")
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbSet = ctx.Set<DropDownConfig>();

                    if (searchField == "configField1" && searchParameter != null)
                        dropDownConfigList = dbSet.Where(x => x.DropDownType == dropDownType && (x.Value.StartsWith(search) || search == null) && x.ConfigField1 == searchParameter).ToList();
                    else
                        dropDownConfigList = dbSet.Where(x => x.DropDownType == dropDownType && (x.Value.StartsWith(search) || search == null)).ToList();
                }
            }
            return dropDownConfigList;
        }
    }
}
