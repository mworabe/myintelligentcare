﻿"use strict";

(function edzBigtextCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzBigtextCustomFieldEditOptions", [
            edzBigtextCustomFieldOptions
        ]);

    function edzBigtextCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/bigtext/edzBigtextCustomFieldEdit.html",
            restrict: "E",
            bindToController: {
                model: "=model"
            },
            controller: ["$scope", function edzAddCustomFieldCtrl($scope) {
            }],
            controllerAs: "edzBigtextCustomFieldEditOptionsCtrl"
        };
    }
})();
