﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Professional Organization.
    /// </summary>
    public class ResourceProfessionalOrganization : Entity
    {
        public long? ResourceId { get; set; }

        public virtual Resource Resource { get; set; }

        public string Organization { get; set; }

        public long? CommitteeMembershipId { get; set; }

        public virtual DropDownConfig CommitteeMembership { get; set; }

        public long? CommitteeTitleId { get; set; }

        public virtual DropDownConfig CommitteeTitle { get; set; }       

    }
}
