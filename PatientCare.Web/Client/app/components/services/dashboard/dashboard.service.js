﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("Dashboard", [
        "$resource", function ($resource) {
            var resource = $resource("api/Dashboards/:id/:controller",
                null, //parameters default
                {
                    'update': { method: "PUT" },
                    'query': { method: "GET", params: {}, isArray: false },
                    'deleteDashboard': { method: "DELETE", params: {}, isArray: false },
                    'capacityVsDemand': { method: "GET", params: { controller: "capacity-vs-demand" }, isArray: true },
                    'avgResourceProductivityByProject': { method: "GET", params: { controller: "avg-productivity-by-project" }, isArray: true },
                    'resourceTimeByRole': { method: "GET", params: { controller: "resource-time-by-role" }, isArray: true },

                    'projectDetails': { method: "GET", params: { controller: "project-details" }, isArray: false },
                    'portfolioFinancialStatus': { method: "GET", params: { controller: "portfolio-financial-status" }, isArray: true },
                    'projectStatus': { method: "GET", params: { controller: "project-status" }, isArray: true },
                    'projectHealth': { method: "GET", params: { controller: "project-health" }, isArray: true },
                    'projectType': { method: "GET", params: { controller: "project-type" }, isArray: true },
                    'projectBudgetVSActualStatus': { method: "GET", params: { controller: "project-budget-actual-status" }, isArray: false },
                    'resourceStatistics': { method: "GET", params: { controller: "resource-statistics" }, isArray: true },
                    'projectDivision': { method: "GET", params: { controller: "project-division" }, isArray: true },
                    'getProjectVSResources': { method: "GET", params: { controller: "external-resources-by-project" }, isArray: true },

                    'getIntakeStatusData': { method: "GET", params: { controller: "intake-status" }, isArray: true },
                    'getIntakeApprovedData': { method: "GET", params: { controller: "intake-approved" }, isArray: true },
                    'getResourcesByExperience': { method: "GET", params: { controller: "resource-experience" }, isArray: true },
                    'getResourcesByDivision': { method: "GET", params: { controller: "resource-division" }, isArray: true },

                    'getResourcesByCity': { method: "GET", params: { controller: "resource-city" }, isArray: true },
                    'getResourcesByState': { method: "GET", params: { controller: "resource-state" }, isArray: true },
                    'resourcesByCountry': { method: "GET", params: { controller: "resource-country" }, isArray: true },

                    'getResourcesByDepartment': { method: "GET", params: { controller: "resource-department" }, isArray: true },
                    'getResourcesByJobTitle': { method: "GET", params: { controller: "resource-job-title" }, isArray: true },
                    'getResourcesByGender': { method: "GET", params: { controller: "resource-gender" }, isArray: true },
                    'getResourcesByLanguage': { method: "GET", params: { controller: "resource-language" }, isArray: true },
                    'getResourcesByLocation': { method: "GET", params: { controller: "resource-location" }, isArray: true },
                    'getResourcesByNameOfSchool': { method: "GET", params: { controller: "resource-name-of-school" }, isArray: true },
                    'getResourcesByRatings': { method: "GET", params: { controller: "resource-rating" }, isArray: true },

                    'getActivityProjectData': { method: "GET", params: { controller: "activity-project-details" }, isArray: false },

                    //activity-project-details
                });

            return resource;
        }
    ]);

angular.module("EDZoutstaffingPortalApp")
    .service("DashboardSetups",
    ["$resource", "$state",
        function ($resource, $state) {
            var resource = $resource("api/DashboardSetups/:id/:controller", null, {
                'getDashboardList': { method: 'get', params: { controller: "GetDashBoards" }, isArray: false },
            });
            resource.getTableOption = {
                pageName: "Dashboards",
                hideBulkEdit: true,
                cols: function () {
                    return [
                        {
                            field: "id",
                            title: "Id",
                            sortable: "id",
                            show: false,
                            dataType: "number"
                        }, {
                            field: "name",
                            title: "Name",
                            sortable: "name",
                            show: true,
                            dataType: "text"
                        }, {
                            field: "action",
                            title: ".",
                            class: "blank-cell",
                            dataType: "command"
                        },
                    ];
                },
                onAdd_Click: function () {
                    //$state.go("dashboard-edit");
                },
                onEdit_Click: function (row) {
                    $state.go("dashboard-edit", { id: row.id });
                },
            };
            return resource;
        }
    ]);