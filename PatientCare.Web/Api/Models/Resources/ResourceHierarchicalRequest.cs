﻿using System.Runtime.Serialization;
using PatientCare.Core.Entities;
using System.Collections.Generic;

namespace PatientCare.Web.Api.Models.Resources
{
    [DataContract]
    public class ResourceHierarchicalRequest
    {
        public long? ResourceId { get; set; }
        public string ResourceName  { get; set; }
        public string ResourceFullName { get; set; }
        public string JobTitle { get; set; }
        public IEnumerable<ResourceHierarchicalResponse> Children { get; set; }
    }
}