﻿using PatientCare.Core.Entities;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Api.Models.Cities;
using PatientCare.Web.Api.Models.PostalCodes;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle City operations.
    /// </summary>
    [Authorize]
    public class CityController : ApiController
    {
        private PatientCareDbContext db = new PatientCareDbContext();

        /// <summary>
        /// Description : Used for Get City. 
        /// </summary>
        /// <param name="search">Search Field</param>
        /// <param name="stateId">State Id</param>
        /// <param name="countryId">Country Id</param>
        /// <returns></returns>
        public List<CityResponse> GetCity(string search, long? stateId = null, long? countryId = null)
        {
            List<City> list = new List<City>();

            if (countryId != null && stateId == null)
                list = db.Cities.Where(m => (m.Name.StartsWith(search) || search == null) && m.CountryId == countryId).OrderBy(x => x.Name).Take(20).ToList();

            if (countryId != null && stateId != null)
                list = db.Cities.Where(m => (m.Name.StartsWith(search) || search == null) && m.StateId == stateId && m.CountryId == countryId).OrderBy(x => x.Name).Take(20).ToList();

            if (countryId == null && stateId != null)
                list = db.Cities.Where(m => (m.Name.StartsWith(search) || search == null) && m.StateId == stateId).OrderBy(x => x.Name).Take(20).ToList();

            if (countryId == null && stateId == null)
                list = db.Cities.Where(m => m.Name.StartsWith(search) || search == null).OrderBy(x => x.Name).Take(20).ToList();

            List<CityResponse> cityList = new List<CityResponse>();

            foreach (var item in list)
            {
                CityResponse city = new CityResponse();
                city.Id = item.Id;
                city.Name = item.Name;
                city.StateId = item.StateId;
                city.State = db.States.AsNoTracking().Where(x => x.Id == item.StateId).Select(y => new CodeResponse { Id = y.Id, Name = y.Name }).FirstOrDefault();

                city.StateName = (city.State != null) ? city.State.Name : item.StateCode;

                //List<CodeResponse> postalcodes = db.PostalCodes.AsNoTracking().Where(x => x.CityId == item.Id).Select(y => new CodeResponse { Id = y.Id, Name = y.ZipCode }).ToList();
                //city.PostalCodeLists = postalcodes.ToArray();
                cityList.Add(city);
            }

            return cityList;
        }
    }
}
