﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceCertificates;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceCertificateMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceCertificateRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCertificate, ResourceCertificateResponse>(new ExtendedMapConfig<ResourceCertificate, ResourceCertificateResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCertificate[], ResourceCertificateResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCertificateRequest, ResourceCertificate>(new ExtendedMapConfig<ResourceCertificateRequest, ResourceCertificate>()));

            return mapperProvider;
        }
    }
}