﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("LeaveRequestService", ["$resource", function ($resource) {
      var resource = $resource("api/leaverequests/:id/:controller",
        null,
            {
                'update': { method: "PUT" },
                'query': { method: "GET", params: {}, isArray: false },
                'currentUserLeaveReq': { metho: 'GET', params: {}, isArray: false },
                'getAdminApproveLeave': { method: 'GET', params: {}, isArray: false, url: "api/leaverequest/AdminApproval" },
                'getLeaveRequestDetail': { method: 'GET', params: {}, isArray: false, url: "api/leaveRequests/:id" },
                'approveLeave': { method: 'PUT', params: { id: '@id', status: '@status', comments: '@comments' }, url: "api/leaverequests/:id" },
                'rejectLeave': { method: 'PUT', params: { id: '@id', status: '@status', comments: '@comments' }, url: "api/leaverequests/:id" },
                'getAllProjectTasks': { method: 'GET', params: { pageIndex: 1, pageSize: 10000 }, url: "api/ProjectTasks/GetAllProjectTasks/" },
                'getTimeEntryList': { method: 'GET', params: {}, url: "api/projects/projects-tasks-timeEntries/" },
            });
      return resource;
  }]);