"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function($stateProvider) {
        $stateProvider
            .state("login", {
                url: "/login",
                templateUrl: "Client/app/controllers/account/login/login.html",
                controller: "LoginCtrl as LoginCtrl",
                data: {
                    requireLogin: false
                }
            })
            .state("register", {
                url: "/register",
                templateUrl: "Client/app/controllers/account/register/register.html",
                controller: "RegisterCtrl as RegisterCtrl",
                data: {
                    requireLogin: false
                }
            })
            .state("changePassword", {
                url: "/change-password",
                templateUrl: "Client/app/controllers/account/changePassword/changePasswordV2.html",
                controller: "ChangePasswordCtrl as ChangePasswordCtrl",
                data: {
                    requireLogin: true
                }
            })
            .state("forgotPassword", {
                url: "/forgot-password",
                templateUrl: "Client/app/controllers/account/forgotPassword/forgot-password.html",
                controller: "ForgotPasswordCtrl as ForgotPasswordCtrl",
                data: {
                    requireLogin: false
                }
            })
            .state("resetPassword", {
                url: "/reset-password",
                templateUrl: "Client/app/controllers/account/resetPassword/reset-password-view.html",
                controller: "ResetPasswordController as ResetPasswordController",
                data: {
                    requireLogin: false
                }
            });
    });