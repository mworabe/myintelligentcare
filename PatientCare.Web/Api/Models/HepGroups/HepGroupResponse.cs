﻿using PatientCare.Web.Api.Models.HepDetails;
using PatientCare.Web.Api.Models.PatientCases;
using PatientCare.Web.Api.Models.Patients;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.HepGroups
{
    [DataContract]
    public class HepGroupResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        //[DataMember]
        //public PatientAutoCompleteResponse Patient { get; set; }

        [DataMember]
        public long? PatientCaseId { get; set; }

        //[DataMember]
        //public PatientCaseAutoCompleteResponse PatientCase { get; set; }

        [DataMember]
        public int? ExerciseOrder { get; set; }

        [DataMember]
        public long? HepId { get; set; }        

        [DataMember]
        public HepDetailResponse[] HepDetails { get; set; }
    }
}