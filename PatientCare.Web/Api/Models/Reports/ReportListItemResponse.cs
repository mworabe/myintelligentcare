﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PatientCare.Web.Api.Models.Reports
{
    [DataContract]
    public class ReportListItemResponse
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string type { get; set; }
    }
}