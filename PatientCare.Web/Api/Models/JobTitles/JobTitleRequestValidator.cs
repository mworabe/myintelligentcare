﻿using PatientCare.Core.Entities;
using FluentValidation;

namespace PatientCare.Web.Api.Models.JobTitles
{
    public class JobTitleRequestValidator : AbstractValidator<JobTitle>
    {
        public JobTitleRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);

            RuleFor(model => model.Rate)
                .GreaterThanOrEqualTo(0);
        }
    }
}