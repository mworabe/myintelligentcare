﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ChangePasswordCtrl", [
        "Auth", "toaster", "ChangePassword", "$state",
function (auth, toaster, changePasswordService, $state) {
    var self = this;
    self.changePasswordModel = {};

    function initialData() {
        self.submitted = false;
    }

    function activate() {
        initialData();
    }

    function changePassword(form) {
        if (form.$valid && !self.submitted) {
            self.submitted = true;
            if (!self.changePasswordModel.currentPassword) {
                toaster.pop("error", "", "Current Password is required.");
                return false;
            }
            if (!self.changePasswordModel.newPassword) {
                toaster.pop("error", "", "New Password is required.");
                return false;
            }
            if (!self.changePasswordModel.retypePassword) {
                toaster.pop("error", "", "Confirm Password is required.");
                return false;
            }
            if (self.changePasswordModel.newPassword != self.changePasswordModel.retypePassword) {
                toaster.pop("error", "", "Passwords does not match, try again later.");
                return false;
            }

            changePasswordService.changePassword({},
                {
                    oldPassword: self.changePasswordModel.currentPassword,
                    newPassword: self.changePasswordModel.newPassword,
                    confirmPassword: self.changePasswordModel.retypePassword
                },
                function (success) {
                    toaster.pop('success', "", "Your password has been updated successfully, please re-login using new passowrd.");
                    auth.logout();
                    $state.go("login");
                },
                function (error) {
                    toaster.pop('error', "", "Unable to change Password");
                }
            )
            self.submitted = false;
        }
    }

    this.changePassword = changePassword;
    activate();
}
    ]);