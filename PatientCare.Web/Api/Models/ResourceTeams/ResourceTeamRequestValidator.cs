﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceTeams
{
    public class ResourceTeamRequestValidator : AbstractValidator<ResourceTeamRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IPicklistRepository<ResourceCompany> _resourceCompanyRepository;

        public ResourceTeamRequestValidator(IDataContextScopeFactory dbContextScopeFactory, IPicklistRepository<ResourceCompany> resourceCompanyRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _resourceCompanyRepository = resourceCompanyRepository;

            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(1, 255);


            RuleFor(model => model.ResourceCompanyId)
                .NotEmpty()
                .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _resourceCompanyRepository, l))
                .WithMessage("The resource company with provided Id doesn't exists");

        }
    }
}