﻿"use strict";

(function edzSelectCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzSelectCustomFieldEditOptions", [
            "edzCustomFieldEditHelper",
            "CUSTOM_FIELD",
            "uuid2",
            edzSelectCustomFieldOptions
        ]);

    function edzSelectCustomFieldOptions(edzCustomFieldEditHelper, CUSTOM_FIELD, uuid2) {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/select/edzSelectCustomFieldEdit.html",
            restrict: "E",
            scope : true,
            controller: ["$scope", function edzAddCustomFieldCtrl($scope) {

                var self = this;
                function loadData() {
                    self.model = {};
                    _.remove($scope.data, function(item) {
                        return item.name !== CUSTOM_FIELD.PROPERTY_NAME.SELECT.SELECT;
                    });
                    self.model.data = $scope.data;

                    _.forEach($scope.data, function(item) {
                        item.unique = uuid2.newguid();
                    });

                }

                function activate() {
                    loadData();
                }

                function addItem() {
                    if (!self.selectValue || self.selectValue.trim() === "")
                        return;
                    var item = edzCustomFieldEditHelper.generatePropertyByName(CUSTOM_FIELD.PROPERTY_NAME.SELECT.SELECT, $scope.id);
                    item.unique = uuid2.newguid();
                    item.value = self.selectValue;
                    
                    self.model.data.push(item);
                    self.selectValue = "";
                }

                function deleteItem(item) {
                    _.remove(self.model.data, { unique: item.unique });
                }

                activate();

                this.addItem = addItem;
                this.deleteItem = deleteItem;
            }],
            controllerAs: "edzSelectCustomFieldEditOptionsCtrl"
        };
    }
})();
