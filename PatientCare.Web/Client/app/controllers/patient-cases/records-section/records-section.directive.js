﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('recordSection', function ($compile) {
    return {
        scope: {
            config: "=config",
            //loadingBarFlag: "=isLoading",
            patientId: "=",
            caseId: "="
        },
        templateUrl: "Client/app/controllers/patient-cases/records-section/records-section.html",
        controller: ['$scope',
                    'toaster',
        function ($scope, toaster) {

            $scope.dropDownTypes = [
                { key: "Select", value: "Select" },
                { key: "InitialEvaluation", value: "Initial Evaluation" },
                { key: "CaseNote", value: "Case Note" },
                { key: "FollowUpEncounterSession", value: "Follow Up Encounter Session" },
                { key: "Re-Examination", value: "Re-Examination" },
                { key: "ProgressReport", value: "Progress Report" },
                { key: "Discharge-Full", value: "Discharge (Full)" },
                { key: "Discharge-quick", value: "Discharge (quick)" }
            ]

            $scope.currentSelectedOption = $scope.dropDownTypes[0];;

            $scope.showItems = function () {
                $scope.selectedOption = $scope.currentSelectedOption;
            }

            $scope.showItems();



            $scope.tabs = [{
                id: "Subjective",
                title: "Subjective",
            }, {
                id: "Objective",
                title: "Objective"
            }, {
                id: "Assessment",
                title: "Assessment"
            }, {
                id: "Plan",
                title: "Plan"
            }, {
                id: "Flowsheet",
                title: "Flowsheet"
            }, {
                id: "billing",
                title: "Billing"
            }];

            $scope.selectedTab = $scope.tabs[0].id;

            $scope.isTabSelected = function (id) {
                if (id == $scope.selectedTab) return true;
                return false;
            }
            $scope.setSelectedTab = function (id) {
                $scope.selectedTab = id;
            }


            $scope.recordHistory = [


                {
                    date: "08/26/2016  10:30 AM",
                    recordType: "Re-Examination",
                    clinicianName: "Mark Taylor",
                    douments: true
                },
                {
                    date: "08/20/2016  11:30 AM",
                    recordType: "Case Note",
                    clinicianName: "John M",
                    douments: true
                },
                  {
                      date: "08/16/2016  09:30 AM",
                      recordType: "Initial Evaluation",
                      clinicianName: "John M",
                      douments: true
                  }

                /*,
                {
                    date: "08/16/2016  12:30 PM",
                    recordType: "Initial Evaluation",
                    clinicianName: "Kepler W",
                    douments: true
                }*/

            ];

        }]
    }
});



