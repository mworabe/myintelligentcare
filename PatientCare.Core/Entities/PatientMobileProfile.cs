﻿using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Patient Mobile Profile.
    /// </summary>
    public class PatientMobileProfile : Entity
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Photo { get; set; }

        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }
        
        public string Language { get; set; }
        
        public string City { get; set; }
        
        public string State { get; set; }
        
        public string Country { get; set; }

        public virtual ICollection<PatientGoal> Goals { get; set; }
    }
}
