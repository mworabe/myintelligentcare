namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedPatientsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.patients",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Prefix = c.String(),
                    first_name = c.String(nullable: false, maxLength: 255),
                    middle_name = c.String(maxLength: 255),
                    last_name = c.String(nullable: false, maxLength: 255),
                    nick_name = c.String(maxLength: 255),
                    birth_date = c.DateTime(),
                    age = c.Int(),
                    is_minor = c.Boolean(),
                    ssn = c.String(maxLength: 255),
                    gender = c.Int(),
                    email = c.String(),
                    preffered_contact_method = c.Int(),
                    cell_number = c.String(),
                    cell_number_is_primary = c.Boolean(),
                    home_number = c.String(),
                    HomeNumberIsPrimary = c.Boolean(),
                    language_id = c.Long(nullable: false),
                    special_accomodations = c.String(),
                    maritial_status = c.Int(),
                    emergency_contact = c.String(),
                    relationship_to_contact = c.String(),
                    emergency_phone = c.String(),
                    employer_name = c.String(),
                    employer_phone = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.languages", t => t.language_id, cascadeDelete: true)
                .Index(t => t.first_name, name: "non_first_name")
                .Index(t => t.last_name, name: "non_last_name")
                .Index(t => t.birth_date, name: "non_birth_date")
                .Index(t => t.ssn, name: "non_ssn")
                .Index(t => t.language_id)
                .Index(t => t.maritial_status, name: "non_maritial_status");

        }

        public override void Down()
        {
            DropForeignKey("dbo.patients", "language_id", "dbo.languages");
            DropIndex("dbo.patients", "non_maritial_status");
            DropIndex("dbo.patients", new[] { "language_id" });
            DropIndex("dbo.patients", "non_ssn");
            DropIndex("dbo.patients", "non_birth_date");
            DropIndex("dbo.patients", "non_last_name");
            DropIndex("dbo.patients", "non_first_name");
            DropTable("dbo.patients");
        }
    }
}
