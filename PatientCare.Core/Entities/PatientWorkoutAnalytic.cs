﻿using PatientCare.Core.Entities.Abstract;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Patient Workout Analytic.
    /// </summary>
    public class PatientWorkoutAnalytic : Entity
    {
        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public long? CaseId { get; set; }
        public virtual PatientCase Case { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? PreExercisesPainRating { get; set; }

        public int? WorkoutFeelResult { get; set; }

        public string WorkoutComment { get; set; }
    }
}
