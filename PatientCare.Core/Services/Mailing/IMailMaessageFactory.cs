﻿using System.Net.Mail;

namespace PatientCare.Core.Services.Mailing
{
    public interface IMailMessageFactory
    {
        MailMessage ForgotPasswordMailMessage(string emailId, string userName, string link);

        MailMessage MobileAccessMailMessage(string emailId, string patientName, string userName, string password);
    }
}
