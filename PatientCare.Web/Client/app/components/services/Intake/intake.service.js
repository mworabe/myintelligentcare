﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("IntakeService", [
        "$resource", "$state", "$injector", "$translate", "$filter",
        function ($resource, $state, $injector, $translate, $filter) {
            var intakePermissionWorker = null;

            function getPermissionWorker() {
                if (!intakePermissionWorker) {
                    intakePermissionWorker = $injector.get("PermissionWorker");
                }
                return intakePermissionWorker;
            }

            var resource = $resource("api/intake/:controller/:id", null,
                {
                    "updateIntake": { method: "PUT" },
                    "getIntakeList": { method: "GET", params: { controller: '' }, isArray: false },
                    "submitApproverList": { method: "POST", params: { controller: 'submit-approver' }, isArray: true },
                    "getApproverList": { method: "GET", params: { controller: 'submit-approver' } },
                    "getIntakeApproverList": { method: "GET", params: { controller: 'submit-approver' }, isArray: false },
                    "getIntakeListForApproval": { method: "GET", params: { controller: 'intake-list-for-approval' }, isArray: false },
                    "getIntakeNotifyGroup": { method: "GET", params: {}, isArray: false, url: "api/intakeResourceGroup" },
                    "saveNotifyGroup": { method: "POST", params: {}, isArray: false, url: "api/intakeResourceGroup" },
                    "updateNotifyGroup": { method: "PUT", params: { id: '@id' }, isArray: false, url: "api/intakeResourceGroup/:id" },
                    "deleteIntakeNotifyGroup": { method: "DELETE", params: { id: '@id' }, isArray: false, url: "api/intakeResourceGroup/:id" },
                    "declineIntake": { method: "POST", params: { controller: 'intake-status-change' }, isArray: false, },
                    "createProjectForIntake": { method: "PUT", params: { id: '@id', controller: 'create-intake-project' }, isArray: false, },
                    "submitToReviewer": { method: "POST", params: { controller: 'intake-request-fulfilled' }, isArray: false, },
                }
            );

            resource.getTableOptions = {
                pageName: $filter('translate')("Project") + " Requests",
                hideSearch: true,
                formatDelete: function (row) {
                    return row.projectName;
                },
                /* -- Permission Checks -- */
                disabledAddButton: function () {
                    return !getPermissionWorker().canIntakeInsert();
                },
                disabledDeleteButton: function () {
                    return getPermissionWorker().canIntakeDelete();
                },
                disabledEditButton: function () {
                    return getPermissionWorker().canIntakeUpdate();
                },
                /* -- Permission Checks -- */

                onAdd_Click: function () {
                    $state.go("editIntake");
                },
                onEdit_Click: function (row) {
                    $state.go("editIntake", { id: row.id });
                },
                cols: function () {
                    return [
                        {
                            field: "projectName",
                            title: $filter('translate')("Project") + " Name",
                            sortable: "projectName",
                            show: true,
                            dataType: "text",
                            dataTypeView: "href",
                            buildUrl: function (row) {
                                return $state.href("editIntake", { id: row.id });
                            }
                        },
                        {
                            field: "projectTypeName",
                            title: $filter('translate')("Project") + " Type",
                            sortable: "projectType.name",// donot changes this because for sorting
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "businessOwnerName",
                            title: "Business Owner",
                            sortable: "businessLeadResource.lastName",// donot changes this because for sorting
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "divisionName",
                            title: "Division",
                            sortable: "division.name",// donot changes this because for sorting
                            show: true,
                            dataType: "text",
                        },
                        {
                            field: "estimatedCost",
                            title: "Cost",
                            sortable: "estimatedCost",
                            show: true,
                            dataType: "text",
                            dataTypeView: "currency",
                        },
                        {
                            field: "businessPriority",
                            title: "Business Priority",
                            sortable: "businessPriority.name",// donot changes this because for sorting
                            show: true,
                            dataType: "text",
                            customViewText: function (row) {
                                return (row && row.businessPriority) ? row.businessPriority.name : "";
                            },
                        },
                        {
                            field: "corporateGoals",
                            title: "Corporate Goals",
                            show: true,
                            dataType: "text",
                            customViewText: function (row) {
                                return (row.corporateGoals && row.corporateGoals.name) ? row.corporateGoals.name : "";
                            },
                        },
                        {
                            field: "status",
                            title: "Status",
                            show: true,
                            dataType: "text",
                            dataTypeView: "text",
                        },
                        {
                            field: "submissionDate",
                            title: "Submission Date",
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "action",
                            title: ".",
                            class: "blank-cell",
                            dataType: "command"
                        }
                    ];
                },
                headerButtons: [
                    {
                        text: "View " + $filter('translate')("Project") + " Request For Review",
                        click: function () {
                            $state.go('intakeApprovalList', { controller: 'review-project-request' }, true);
                        },
                        iconClass: ""
                    },
                ]
            }

            resource.getIntakeApprovalTableOptions = {
                pageName: $filter('translate')("Project") + "  Request For Review",
                hideSearch: true,
                hideAddButton: true,
                formatDelete: function (row) {
                    return row.projectName;
                },
                /* -- Permission Checks -- */
                disabledAddButton: function () {
                    return !getPermissionWorker().canResourceInsert();
                },
                disabledDeleteButton: function () {
                    return false;// getPermissionWorker().canResourceDelete();
                },
                disabledEditButton: function () {
                    return getPermissionWorker().canResourceUpdate();
                },
                /* -- Permission Checks -- */

                onAdd_Click: function () {
                    $state.go("editIntake");
                },
                onEdit_Click: function (row) {
                    $state.go("editIntake", { id: row.id, isForReviewer: true });
                },
                cols: function () {
                    return [
                        {
                            field: "projectName",
                            title: $filter('translate')("Project") + " Name",
                            sortable: "projectName",
                            show: true,
                            dataType: "text",
                            dataTypeView: "href",
                            buildUrl: function (row) {
                                return $state.href("editIntake", { id: row.id });
                            }
                        },
                        {
                            field: "projectTypeName",
                            title: $filter('translate')("Project") + " Type",
                            sortable: "projectType.name",// donot changes this because for sorting
                            show: true,
                            dataType: "text",
                        },
                        {
                            field: "businessOwnerName",
                            title: "Business Owner",
                            sortable: "businessLeadResource.lastName",// donot changes this because for sorting
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "businessUnitName",
                            title: "Business Unit",
                            sortable: "businessUnit.name",// donot changes this because for sorting
                            show: true,
                            dataType: "text"
                        },
                       
                        {
                            field: "cost",
                            title: "Cost",
                            sortable: "estimatedCost",
                            show: true,
                            dataType: "text",
                            dataTypeView: "currency",
                            customViewText: function (row) {
                                return row.estimatedCost;
                                //estimatedCost
                            },
                        },
                        {
                            field: "businessPriority",
                            title: "Business Priority",
                            sortable: "businessPriority.name",// donot changes this because for sorting
                            show: true,
                            dataType: "text",
                            customViewText: function (row) {
                                return (row && row.businessPriority) ? row.businessPriority.name : "";
                            },
                        },
                        {
                            field: "corporateGoals",
                            title: "Corporate Goals",
                            show: true,
                            dataType: "text",
                            customViewText: function (row) {
                                return (row.corporateGoals && row.corporateGoals.name) ? row.corporateGoals.name : "";
                            },
                        },
                        {
                            field: "status",
                            title: "Status",
                            show: true,
                            dataType: "text",
                            dataTypeView: "text",
                        },
                        {
                            field: "submissionDate",
                            title: "Submission Date",
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "action",
                            title: ".",
                            class: "blank-cell",
                            dataType: "command"
                        }
                    ];
                },
                headerButtons: [
                    {
                        text: "Back",
                        click: function () {
                            $state.go('intakeList', {}, true);
                        },
                        iconClass: "go-back"
                    },
                ]
            }

            resource.getIntakeNotifyGroupTableOptions = {
                pageName: "Intake Group",
                hideSearch: true,
                //hideAddButton: true,
                formatDelete: function (row) {
                    return row.groupName;
                },
                /* -- Permission Checks -- */
                disabledAddButton: function () {
                    return !getPermissionWorker().canResourceInsert();
                },
                disabledDeleteButton: function () {
                    return getPermissionWorker().canResourceDelete();
                },
                disabledEditButton: function () {
                    return getPermissionWorker().canResourceUpdate();
                },
                /* -- Permission Checks -- */

                onAdd_Click: function () {
                    $state.go("editIntakeNotifyGroup");
                },
                onEdit_Click: function (row) {
                    $state.go("editIntakeNotifyGroup", { id: row.id });
                },
                onDelete_Click: function (row) {

                },
                cols: function () {
                    return [
                        {
                            field: "groupName",
                            title: "Group Name",
                            sortable: "groupName",
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "resource",
                            title: "Resources",
                            sortable: "resource",
                            show: true,
                            dataType: "text",
                            /*customViewText: function (row) {
                                if (row.projectType && row.projectType.name) return row.projectType.name
                                return "";
                            },*/
                        },
                        {
                            field: "action",
                            title: ".",
                            class: "blank-cell",
                            dataType: "command"
                        }
                    ];
                },
                headerButtons: []
            }

            return resource;
        }
    ]
);