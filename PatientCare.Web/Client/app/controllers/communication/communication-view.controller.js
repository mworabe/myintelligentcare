﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("CommunicationCtrl", ["$scope", '$stateParams', function ($scope, $stateParams) {

        
        
        $scope.tabs = [{
            id: "chat-window",
            title: "Chat",
        }];

        if ($stateParams.tab) {
            $scope.selectedTab = $scope.tabs[$stateParams.tab].id;
        } else {
            $scope.selectedTab = $scope.tabs[0].id;
        }
        $scope.isTabSelected = function (id) {
            if (id == $scope.selectedTab) return true;
            return false;
        }
        $scope.setSelectedTab = function (id) {
            $scope.selectedTab = id;
        }


    }]);