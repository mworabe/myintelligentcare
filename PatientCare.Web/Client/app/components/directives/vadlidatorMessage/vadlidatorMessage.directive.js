﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngVadlidatorMessage", [
        function () {
            return {
                templateUrl: "Client/app/components/directives/vadlidatorMessage/vadlidatorMessage.html",
                restrict: "E",
                replace: false,
                scope: {
                    elementName: "@ngElementName",
                    $submitted: "=ngSubmitted",
                    containerClass: "@ngContainerClass",
                    messageErrorClass: "@ngMessageErrorClass",
                    customMessage: "@ngCustomMessage"
                },
                require: "^form",
                link: function (scope, el, attrs, form) {
                    var element;
                    var domElement;
                    var submitted = false;
                    var value = undefined;
                    scope.form = form;
                    scope.containerClass = scope.containerClass || "validatorErrorContainer";
                    scope.messageErrorClass = scope.messageErrorClass || "error-message";
                    scope.element = form[scope.elementName];
                    element = scope.element;
                    scope.getMessage = getMessage;

                    scope.$watch(
                        function () { return scope.form.$submitted; },
                        function (newVal) {
                            submitted = newVal;
                            if (value === undefined)
                                return;
                            if (element && element != "undefined" && !element.$valid)
                                setClassElement();
                        }
                    );
                    scope.$watch(
                        function () {
                            if (element == "undefined" && !element)
                                element = form[scope.elementName];
                            if (element && element != "undefined")
                                return element.$valid;
                            return "";
                        },
                        function (newVal) {

                            value = newVal;
                            if (!submitted)
                                return;
                            setClassElement();
                        }
                    );

                    function getMessage(message, postfix) {
                        return attrs["ngCustomMessage" + postfix] || scope.customMessage || message;
                    }

                    function getDomElement() {
                        return angular.element(document.getElementsByName(element.$name));
                    }

                    function setClassElement() {
                        function setClass() {

                            if (!element.$valid) {
                                domElement.addClass("element-validation-error");
                            } else {
                                domElement.removeClass("element-validation-error");
                            }

                        }

                        if (!domElement)
                            domElement = getDomElement();

                        if (domElement.length === 0)
                            domElement.ready(function () {
                                domElement = getDomElement();
                                setClass();
                            });
                        else setClass();
                    }


                }
            };
        }
    ]);