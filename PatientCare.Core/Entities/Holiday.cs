﻿using PatientCare.Core.Entities.Abstract;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Holiday.
    /// </summary>
    public class Holiday : NamedEntity
    {
        public long? Location { get; set; }

        public DateTime? HolidayDate { get; set; }

        public string Description { get; set; }
    }
}
