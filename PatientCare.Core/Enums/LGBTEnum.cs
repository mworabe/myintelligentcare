﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : LGBT Enum.
    /// </summary>
    public enum LGBTEnum
    {
        Lesbian = 0,
        Gay = 1,
        Bisexual = 2,
        Transgender = 3,
        NotApplicable =4
    }
}
