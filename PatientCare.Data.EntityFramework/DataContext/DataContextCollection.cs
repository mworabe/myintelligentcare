﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using PatientCare.Data.AbstractDataContext;

namespace PatientCare.Data.EntityFramework.DataContext
{
    /// <summary>
    /// As its name suggests, DbContextCollection maintains a collection of DbContext instances.
    /// 
    /// What it does in a nutshell:
    /// - Lazily instantiates DbContext instances when its Get Of TDbContext () method is called
    /// (and optionally starts an explicit database transaction).
    /// - Keeps track of the DbContext instances it created so that it can return the existing
    /// instance when asked for a DbContext of a specific type.
    /// - Takes care of committing / rolling back changes and transactions on all the DbContext
    /// instances it created when its Commit() or Rollback() method is called.
    /// 
    /// </summary>
    internal sealed class DataContextCollection : IDataContextCollection
    {
        private readonly Dictionary<Type, DbContext> _initializedDbContexts;
        private readonly Dictionary<DbContext, DbContextTransaction> _transactions;
        private IsolationLevel? _isolationLevel;
        private readonly IDataContextFactory _dataContextFactory;
        private bool _disposed;
        private bool _completed;
        private readonly bool _readOnly;

        internal Dictionary<Type, DbContext> InitializedDbContexts { get { return _initializedDbContexts; } }

        public DataContextCollection(IDataContextFactory dataContextFactory, bool readOnly = false, IsolationLevel? isolationLevel = null)
        {
            if (dataContextFactory == null) { throw new ArgumentNullException("dataContextFactory"); }

            _disposed = false;
            _completed = false;

            _initializedDbContexts = new Dictionary<Type, DbContext>();
            _transactions = new Dictionary<DbContext, DbContextTransaction>();

            _readOnly = readOnly;
            _isolationLevel = isolationLevel;
            _dataContextFactory = dataContextFactory;
        }

        public T Get<T>() where T : class
        {
            if (_disposed) { throw new ObjectDisposedException("DbContextCollection"); }

            var requestedType = typeof(T);

            if (!_initializedDbContexts.ContainsKey(requestedType))
            {
                // First time we've been asked for this particular DbContext type.
                // Create one, cache it and start its database transaction if needed.
                var dbContext = (DbContext)_dataContextFactory.CreateDataContext();
                _initializedDbContexts.Add(requestedType, dbContext);
                if (_readOnly) { dbContext.Configuration.AutoDetectChangesEnabled = false; }
                if (_isolationLevel.HasValue)
                {
                    var tran = dbContext.Database.BeginTransaction(_isolationLevel.Value);
                    _transactions.Add(dbContext, tran);
                }
            }

            return _initializedDbContexts[requestedType] as T;
        }

        public int Commit()
        {
            if (_disposed) { throw new ObjectDisposedException("DbContextCollection"); }
            if (_completed) { throw new InvalidOperationException("You can't call Commit() or Rollback() more than once on a DbContextCollection. All the changes in the DbContext instances managed by this collection have already been saved or rollback and all database transactions have been completed and closed. If you wish to make more data changes, create a new DbContextCollection and make your changes there."); }

            var commitCount = 0;
            var exceptions = new List<Exception>();
            foreach (var dbContext in _initializedDbContexts.Values)
            {
                try
                {
                    if (!_readOnly) { commitCount += dbContext.SaveChanges(); }
                    // If we've started an explicit database transaction, time to commit it now.
                    var tran = GetValueOrDefault(_transactions, dbContext);
                    if (tran != null)
                    {
                        tran.Commit();
                        tran.Dispose();
                    }
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            _transactions.Clear();
            _completed = true;

            if (exceptions.Count > 0) { throw new AggregateException(exceptions); }
            return commitCount;
        }

        public Task<int> CommitAsync()
        {
            return CommitAsync(CancellationToken.None);
        }

        public async Task<int> CommitAsync(CancellationToken cancelToken)
        {
            if (cancelToken == null) { throw new ArgumentNullException("cancelToken"); }
            if (_disposed) { throw new ObjectDisposedException("DbContextCollection"); }
            if (_completed) { throw new InvalidOperationException("You can't call Commit() or Rollback() more than once on a DbContextCollection. All the changes in the DbContext instances managed by this collection have already been saved or rollback and all database transactions have been completed and closed. If you wish to make more data changes, create a new DbContextCollection and make your changes there."); }

            // See comments in the sync version of this method for more details.
            var exceptions = new List<Exception>();
            var commitCount = 0;

            foreach (var dbContext in _initializedDbContexts.Values)
            {
                try
                {
                    if (!_readOnly) { commitCount += await dbContext.SaveChangesAsync(cancelToken).ConfigureAwait(false); }
                    // If we've started an explicit database transaction, time to commit it now.
                    var tran = GetValueOrDefault(_transactions, dbContext);
                    if (tran != null)
                    {
                        tran.Commit();
                        tran.Dispose();
                    }
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            _transactions.Clear();
            _completed = true;

            if (exceptions.Count > 0) { throw new AggregateException(exceptions); }
            return commitCount;
        }

        public void Rollback()
        {
            if (_disposed) { throw new ObjectDisposedException("DbContextCollection"); }
            if (_completed) { throw new InvalidOperationException("You can't call Commit() or Rollback() more than once on a DbContextCollection. All the changes in the DbContext instances managed by this collection have already been saved or rollback and all database transactions have been completed and closed. If you wish to make more data changes, create a new DbContextCollection and make your changes there."); }

            var exceptions = new List<Exception>();
            foreach (var dbContext in _initializedDbContexts.Values)
            {
                // There's no need to explicitly rollback changes in a DbContext as
                // DbContext doesn't save any changes until its SaveChanges() method is called.
                // So "rolling back" for a DbContext simply means not calling its SaveChanges()
                // method. 

                // But if we've started an explicit database transaction, then we must roll it back.
                var tran = GetValueOrDefault(_transactions, dbContext);
                if (tran != null)
                {
                    try
                    {
                        tran.Rollback();
                        tran.Dispose();
                    }
                    catch (Exception e)
                    {
                        exceptions.Add(e);
                    }
                }
            }

            _transactions.Clear();
            _completed = true;

            if (exceptions.Count > 0) { throw new AggregateException(exceptions); }
        }

        public void Dispose()
        {
            if (_disposed) { return; }
            var exceptions = new List<Exception>();
            if (!_completed)
            {
                try
                {
                    if (_readOnly) { Commit(); }
                    else { Rollback(); }
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }

            foreach (var dbContext in _initializedDbContexts.Values)
            {
                try
                {
                    dbContext.Dispose();
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                    Debug.WriteLine(e);
                }
            }

            _initializedDbContexts.Clear();
            _disposed = true;
            if (exceptions.Count > 0) { throw new AggregateException(exceptions); }
        }

        /// <summary>
        /// Returns the value associated with the specified key or the default 
        /// value for the TValue  type.
        /// </summary>
        private static TValue GetValueOrDefault<TKey, TValue>(IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : default(TValue);
        }
    }
}
