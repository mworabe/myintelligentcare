﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IMasterTemplateRepository : IRepository<MasterTemplate>
    {
    }
}
