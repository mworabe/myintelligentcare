﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;
using System;

namespace PatientCare.Web.Api.Models.Holidays
{
    [DataContract]
    public class HolidayPicklistResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long Location { get; set; }

        [DataMember]
        public DateTime? HolidayDate { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}