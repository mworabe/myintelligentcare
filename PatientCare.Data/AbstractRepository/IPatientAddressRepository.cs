﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IPatientAddressRepository : IRepository<PatientAddress>
    {
    }
}
