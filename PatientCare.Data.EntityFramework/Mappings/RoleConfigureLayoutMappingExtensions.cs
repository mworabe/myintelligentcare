﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class RoleConfigureLayoutMappingExtensions
    {
        public static DbModelBuilder MapRoleConfigureLayout(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<RoleConfigureLayout>().ToTable("ApplicationRoleConfigureLayouts");


            modelBuilder.Entity<RoleConfigureLayout>()
                .Property(r => r.Configure)
                .HasColumnName("configure")
                .IsUnicode()
                .IsRequired();

            modelBuilder.Entity<RoleConfigureLayout>()
                .Property(p => p.RoleId)
                .HasColumnName("role_id")
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("ix_role_configure_layout_unique")
                    {
                        IsUnique = true,
                        Order = 2
                    }))
                .IsOptional();


            modelBuilder.Entity<RoleConfigureLayout>()
                .Property(p => p.Type)
                .HasColumnName("type")
                .IsRequired()
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("ix_role_configure_layout_unique")
                    {
                        IsUnique = true,
                        Order = 1
                    }));

            modelBuilder.Entity<RoleConfigureLayout>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<RoleConfigureLayout>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();


            return modelBuilder;
        }
    }
}