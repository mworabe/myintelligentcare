﻿using PatientCare.Core.Enums;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ReferralPhysicians
{
    [DataContract]
    public class ReferralPhysicianDetailResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Speciality { get; set; }

        [DataMember]
        public MedicalDesignationEnum? MedicalDesignation { get; set; }
        //public int? MedicalDesignation { get; set; }

        [DataMember]
        public string MedicalDesignationOther { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string ExtensionNumber { get; set; }

        [DataMember]
        public string Fax { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string FacilityName { get; set; }

        [DataMember]
        public long? CityId { get; set; }
        [DataMember]
        public string City { get; set; }

        [DataMember]
        public long? StateId { get; set; }
        [DataMember]
        public string State { get; set; }

        [DataMember]
        public long? CountryId { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public Int32? ReferralsToDate { get; set; }

        [DataMember]
        public DateTime? LastReferralReceived { get; set; }

        [DataMember]
        public string PostalCode { get; set; }
    }
}