﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.Ratings;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class ExerciseRatingMobileMapperExtensions
    {
        public static EmitMapperMapperProvider ExerciseRatingMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseRating, ExerciseRatingMobileResponse>
                    (new ExtendedMapConfig<ExerciseRating, ExerciseRatingMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseRating[], ExerciseRatingMobileResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseRatingMobileRequest, ExerciseRating>(
                        new ExtendedMapConfig<ExerciseRatingMobileRequest, ExerciseRating>()
                    ));

            return mapperProvider;
        }
    }
}