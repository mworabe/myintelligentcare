﻿using PatientCare.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceRepository : IRepository<Resource>
    {
        Task<IEnumerable<Resource>> AutocompleteAsync(FilterQuery<Resource> filter = null, ReadOptions<Resource> readOptions = null);
    }
}
