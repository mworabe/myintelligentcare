﻿using PatientCare.Web.Api.Models.ExerciseActivities;
using PatientCare.Web.Api.Models.ExerciseMedias;
using PatientCare.Web.Api.Models.ExercisePositions;
using PatientCare.Web.Api.Models.InjuryRegions;
using PatientCare.Web.ApiMobile.Models.ExerciseMedias;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.Exercises
{
    [DataContract]
    public class ExerciseMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? RegionId { get; set; }

        [DataMember]
        public InjuryRegionAutoCompleteResponse Region { get; set; }

        [DataMember]
        public long? PositionId { get; set; }

        [DataMember]
        public ExercisePositionAutoCompleteResponse Position { get; set; }

        [DataMember]
        public long? ActivityId { get; set; }

        [DataMember]
        public ExerciseActivityAutoCompleteResponse Activity { get; set; }

        [DataMember]
        public string Keywords { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public ExerciseMediaMobileResponse[] Medias { get; set; }
    }
}