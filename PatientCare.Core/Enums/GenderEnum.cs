﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Gender Enum.
    /// </summary>
    public enum GenderEnum
    {
        Male = 0,
        Female = 1
    }
}
