﻿
angular.module("EDZoutstaffingPortalApp")
        .controller("DashboardListCtrl", [
             "$scope", "DashboardSetups", "$q", "$state", "$uibModal", "DashboardResource", "Modal", "toaster",
            function ($scope, DashboardSetupService, $q, $state, $uibModal, DashboardResource, modal, toaster) {

                $scope.getAllDashboards = function () {
                    var defer = $q.defer();
                    DashboardSetupService.getDashboardList({
                        pageIndex: 1,
                        pageSize: 1000,
                        sortField: 'created',
                        sortOrder: 'ascending'
                    }).$promise.then(function (responce) {
                        $scope.dashboardList = responce.data || [];
                        defer.resolve(responce.data || []);
                    });
                    return defer.promise;
                }
                $scope.getAllDashboards();

                $scope.getStructure = function (item) {
                    var setupJSON = JSON.parse(item.setupJson);
                    return setupJSON.structure;
                }
                $scope.addNewDashboard = function () {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'Client/app/controllers/dashboard/add-dashboard-modal.html',
                        controller: 'AddDashboardCtrl as AddDashboardCtrl',
                        resolve: {}
                    });
                    //$state.go("dashboard-edit");
                }
                $scope.rowEdit = function (item) {
                    $state.go("dashboard-edit", { id: item.id });
                }
                $scope.deleteDashboard = function (row) {
                    var message = row.name;
                    modal.confirm.delete(function () {
                        DashboardResource.deleteDashboard(row);
                        _.remove($scope.dashboardList, function (item) {
                            return row === item;
                        });
                        toaster.pop("success", "", "dashboard deleted successfully");
                    })("Warning", message);
                }
            }
        ]);