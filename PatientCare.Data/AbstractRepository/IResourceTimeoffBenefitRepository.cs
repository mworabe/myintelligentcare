﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceTimeoffBenefitRepository : IRepository<ResourceTimeoffBenefit>
    {
    }
}
