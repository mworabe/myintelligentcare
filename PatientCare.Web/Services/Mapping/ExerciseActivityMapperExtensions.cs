﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ExerciseActivities;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ExerciseActivityMapperExtensions
    {
        public static EmitMapperMapperProvider ExerciseActivityMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseActivity, ExerciseActivityResponse>
                    (new ExtendedMapConfig<ExerciseActivity, ExerciseActivityResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseActivity[], ExerciseActivityResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseActivity, ExerciseActivityAutoCompleteResponse>
                    (new ExtendedMapConfig<ExerciseActivity, ExerciseActivityAutoCompleteResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseActivity[], ExerciseActivityAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseActivityRequest, ExerciseActivity>
                    (new ExtendedMapConfig<ExerciseActivityRequest, ExerciseActivity>()));

            return mapperProvider;
        }
    }
}