﻿namespace PatientCare.Web
{
    public class Const
    {
        public const string DefaultDisplayDateTimeFormat = "yyyy-MM-dd hh:mm:ss tt";
        public const string DefaultDisplayDateFormat = "yyyy-MM-dd";
        public const string PhoneRegexPattern = @"^\+?[0-9]{1,4}-?[0-9]{7,15}$";
    }
}