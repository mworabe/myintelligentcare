﻿using PatientCare.Core.Entities;

namespace PatientCare.Core.Services.RoleSetupService
{
    public class RoleAccessRights
    {
        public string RoleId { get; set; }

        public string RoleName { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public DashboardSetup DashboardSetup { get; set; }

        public AccessRights AccessRights { get; set; }
    }
}
