﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Behaviourl Detail.
    /// </summary>
    public class ResourceBehaviouralDetail : Entity
    {
        public long? ResourceId { get; set; }
        public virtual Resource Resource { get; set; }

        public string Behaviour { get; set; }       
    }
}
