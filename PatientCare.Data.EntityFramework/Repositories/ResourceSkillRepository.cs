﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceSkillRepository : RepositoryBase<ResourceSkill>, IResourceSkillRepository
    {
        public ResourceSkillRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
