﻿using System.Runtime.Serialization;
using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.ResourceTravels
{
    [DataContract]
    public class ResourceTravelResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public VisaOrPermitEnum? VisaOrPermit { get; set; }

        [DataMember]
        public long? CountryId { get; set; }

        [DataMember]
        public PicklistResponse Country { get; set; }

        [DataMember]
        public int? TimeRemainingInDays { get; set; }

        [DataMember]
        public string SpecialConsiderations { get; set; }

        [DataMember]
        public long ResourceId { get; set; }

    }
}