﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IExerciseCommentRepository : IRepository<ExerciseComment>
    {
    }
}
