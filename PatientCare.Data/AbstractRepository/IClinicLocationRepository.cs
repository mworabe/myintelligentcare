﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IClinicLocationRepository : IRepository<ClinicLocation>
    {
    }
}
