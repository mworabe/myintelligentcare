﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.EntityFramework.Identity;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using PatientCare.Core.Services.Dashboards;
using PatientCare.Core.Services;
using PatientCare.Core.Services.Mailing;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Reports;
using PatientCare.Web.Api.Models.Resources;
using PatientCare.Web.Services.Excel;
using System.Data;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Report operations.
    /// </summary>
    [Authorize]
    public sealed class ReportController : ApiBaseController
    {
        /// <summary>
        /// Description : Report IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : Report IEmailSender.
        /// </summary>
        private readonly IEmailSender _emailSender;
        /// <summary>
        /// Description : Report IResourceRepository.
        /// </summary>
        private readonly IResourceRepository _resourceRepository;
        /// <summary>
        /// Description : Report IAppUtilityService.
        /// </summary>
        private readonly IAppUtilityService _appUtilityService;

        public static string INTAKE_STATUS_REQUIRED_ADDITIONAL_INFO = "addtional_info";
        public static string INTAKE_STATUS_REQUIRED_DECLINE = "decline";
        public static string INTAKE_STATUS_APPROVE = "approve";
        public static string INTAKE_STATUS_REQUIRED_WAITING_FOR_APPROVAL = "waiting_for_approval";

        /// <summary>
        /// Description : Initializes a new instance of the Report Class. 
        /// </summary>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        /// <param name="emailSender">IEmail Sender</param>
        /// <param name="resourceRepository">IResource Repository</param>
        /// <param name="appUtilityService">IAppUtility Service</param>
        public ReportController
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IEmailSender emailSender,
                IResourceRepository resourceRepository,
                IAppUtilityService appUtilityService
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _emailSender = emailSender;
            _resourceRepository = resourceRepository;
            _appUtilityService = appUtilityService;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Report/" + id, UriKind.Relative);
        }

        //[HttpGet]
        //[Route("api/Report")]
        //public IHttpActionResult GetReports([FromUri]ResourceListRequest request)
        //{
        //    var roleInfo = GetRoleAccessRights();
            
        //    List<ReportListItemResponse> reportItemList = new List<ReportListItemResponse>();
        //    reportItemList.Add(new ReportListItemResponse { Id = 1, Name = "Resources", type = "RESOURCE" });
        //    reportItemList.Add(new ReportListItemResponse { Id = 2, Name = "Resources Utilization", type = "RESOURCE" });
        //    reportItemList.Add(new ReportListItemResponse { Id = 3, Name = "Resources Salary/Rate", type = "RESOURCE" });
        //    reportItemList.Add(new ReportListItemResponse { Id = 4, Name = "Resources Work Details", type = "RESOURCE" });

        //    var resp = new ListResponse<ReportListItemResponse>(reportItemList.OrderBy(x => x.Name).ToArray(), reportItemList.Count);

        //    return Ok(resp);
        //}

        //#region GetResourcesReport
        //[HttpPost]
        //[Route("api/Report/GetResourcesReport")]
        //public IHttpActionResult GetResourcesReport([FromBody]ResourceReportRequest request)
        //{
        //    #region  restrict permission
        //    var roleInfo = GetRoleAccessRights();
           
        //    var userId = User.Identity.GetUserId();
        //    var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
        //    if (user == null && user.Resource == null)
        //    {
        //        ModelState.AddModelError("Reports", "Logged in user is not associated with Resource, contact admin to assign resource");
        //        return BadRequest(ModelState);
        //    }

        //    RestrictionDataModel tempRestrictionData = new RestrictionDataModel();
        //    if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
        //    {
        //        if (roleInfo.AccessRights.IsForDepartmentOnly)
        //            tempRestrictionData.DepartmentId = user.Resource.DepartmentId;
        //        if (roleInfo.AccessRights.IsForDivisionOnly)
        //            tempRestrictionData.DivisionId = user.Resource.DivisionId;               
        //    }
        //    #endregion

        //    var lst = LoadResourceReport(request, tempRestrictionData, user.ResourceId);
        //    return Ok(lst);
        //}

        //[HttpGet]
        //[Route("api/Report/export-resources-report/")]
        //public HttpResponseMessage Export(string Division = null, DateTime? HireDateFrom = null, DateTime? HireDateTo = null, string currentUserId = null)
        //{
        //    #region  restrict permission
        //    var roleInfo = GetRoleAccessRightsByResourceId(currentUserId);
            
        //    var userId = currentUserId;
        //    var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
        //    if (user != null && user.Resource == null)
        //    {
        //        ModelState.AddModelError("Reports", "Logged in user is not associated with Resource, contact admin to assign resource");
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //    }

        //    RestrictionDataModel tempRestrictionData = new RestrictionDataModel();
        //    if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
        //    {
        //        if (roleInfo.AccessRights.IsForDepartmentOnly)
        //            tempRestrictionData.DepartmentId = user.Resource.DepartmentId;
        //        if (roleInfo.AccessRights.IsForDivisionOnly)
        //            tempRestrictionData.DivisionId = user.Resource.DivisionId;
        //    }
        //    #endregion

        //    var report = LoadResourceReport((new ResourceReportRequest() { Division = Division, HireDateFrom = HireDateFrom, HireDateTo = HireDateTo }), tempRestrictionData, user.ResourceId);

        //    var stream = ResourceReportExport.ExportReport(report.Data, "Resources Report") as MemoryStream;
        //    var message = new HttpResponseMessage(HttpStatusCode.OK)
        //    {
        //        // ReSharper disable once PossibleNullReferenceException
        //        Content = new ByteArrayContent(stream.ToArray())
        //    };
        //    message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
        //    message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        //    {
        //        FileName = "EdzResourceReport.xlsx"
        //    };

        //    return message;
        //}

        //private ListResponse<ResourcesReportView> LoadResourceReport(ResourceReportRequest request, RestrictionDataModel restrictData, long? resourceId)
        //{
        //    var lst = new ListResponse<ResourcesReportView>();

        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        var query = ctx.Database.SqlQuery<ResourcesReportView>(LoadResourceReportQuery(restrictData, resourceId));
        //        IEnumerable<ResourcesReportView> result = new List<ResourcesReportView>();

        //        result = query.ToList();

        //        if (request != null)
        //        {
        //            if (!string.IsNullOrEmpty(request.Division))
        //                result = result.Where(x => x.Division == request.Division);

        //            if (request.HireDateFrom.HasValue && !request.HireDateTo.HasValue)
        //                result = result.Where(x => x.StartDate.Value.Date.ToUniversalTime() >= request.HireDateFrom.Value.Date.ToUniversalTime());

        //            if (!request.HireDateFrom.HasValue && request.HireDateTo.HasValue)
        //                result = result.Where(x => x.StartDate.Value.Date.ToUniversalTime() <= request.HireDateTo.Value.Date.ToUniversalTime());

        //            if (request.HireDateFrom.HasValue && request.HireDateTo.HasValue)
        //                result = result.Where(x => x.StartDate.Value.Date.ToUniversalTime() >= request.HireDateFrom.Value.Date.ToUniversalTime() && x.StartDate.Value.Date.ToUniversalTime() <= request.HireDateTo.Value.Date.ToUniversalTime());
        //        }

        //        lst.Data = result.ToArray();
        //    }

        //    return lst;
        //}

        //private static string LoadResourceReportQuery(RestrictionDataModel restrictData, long? resourceId)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("SELECT (R.[last_name] +' '+ R.[first_name])as ResourceName,r.employee_id as EmployeeId, R.Id as Id,R.[first_name] as FirstName,R.[last_name] as LastName,JT.name as JobTitle,D2.Name as Division,D.name as Department,R.[start_date] as StartDate ,R.[supervisor_id]as SupervisorId ,superv.last_name + ' '+superv.first_name as Supervisor  ");
        //    sb.Append("\r\n");
        //    sb.Append("FROM resources R ");
        //    sb.Append("\r\n");
        //    sb.Append("  left join Job_Titles JT on R.job_title_id = JT.id");
        //    sb.Append("\r\n");
        //    sb.Append("  left join Departments D on R.department_id = D.id");
        //    sb.Append("\r\n");
        //    sb.Append("  left join Divisions D2 on R.division_id = D2.id");
        //    sb.Append("\r\n");
        //    sb.Append("  left join resources superv on superv.id  = R.supervisor_id");
        //    sb.Append("\r\n");

        //    if (restrictData != null)
        //    {
        //        if ((restrictData.DepartmentId != null) || (restrictData.DivisionId != null))
        //            sb.Append("where ");
        //        if (restrictData.DepartmentId != null && restrictData.DivisionId != null)
        //        {
        //            sb.AppendFormat(" (R.department_id = {0})", restrictData.DepartmentId);
        //            sb.AppendFormat(" and ( R.division_id = {0})", restrictData.DivisionId);
        //        }
        //        else
        //        {
        //            if (restrictData.DepartmentId != null)
        //                sb.AppendFormat(" R.department_id = {0}", restrictData.DepartmentId);
        //            if (restrictData.DivisionId != null)
        //                sb.AppendFormat(" R.division_id = {0}", restrictData.DivisionId);
        //        }
        //    }
        //    sb.Append("\r\n");
        //    sb.Append("order by ResourceName asc ,D2.Name asc ,D.Name asc");
        //    sb.Append("\r\n");

        //    return sb.ToString();
        //}

        //#endregion
        
        //#region GetResourcesRateOrSalaryReport
        //[HttpPost]
        //[Route("api/Report/GetResourcesRateOrSalaryReport")]
        //public IHttpActionResult GetResourceSalaryOrRateReport([FromBody]ResourceReportRequest request)
        //{
        //    #region  restrict permission
        //    var roleInfo = GetRoleAccessRights();
            
        //    var userId = User.Identity.GetUserId();
        //    var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
        //    if (user != null && user.Resource == null)
        //    {
        //        ModelState.AddModelError("Reports", "Logged in user is not associated with Resource, contact admin to assign resource");
        //        return BadRequest(ModelState);
        //    }

        //    RestrictionDataModel tempRestrictionData = new RestrictionDataModel();
        //    if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
        //    {
        //        if (roleInfo.AccessRights.IsForDepartmentOnly)
        //            tempRestrictionData.DepartmentId = user.Resource.DepartmentId;
        //        if (roleInfo.AccessRights.IsForDivisionOnly)
        //            tempRestrictionData.DivisionId = user.Resource.DivisionId;                
        //    }
        //    #endregion

        //    var lst = LoadResourceSalaryOrRateReport(request, tempRestrictionData, user.ResourceId);

        //    return Ok(lst);
        //}

        //[HttpGet]
        //[Route("api/Report/export-resources-rateOrSalary-report/")]
        //public HttpResponseMessage ExportResourceSalaryOrRateReport(string jobTitle = null, string Division = null, string Departament = null, string currentUserId = null)
        //{
        //    #region  restrict permission
        //    var roleInfo = GetRoleAccessRightsByResourceId(currentUserId);
            
        //    var userId = currentUserId;
        //    var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
        //    if (user != null && user.Resource == null)
        //    {
        //        ModelState.AddModelError("Reports", "Logged in user is not associated with Resource, contact admin to assign resource");
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //    }

        //    RestrictionDataModel tempRestrictionData = new RestrictionDataModel();
        //    if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
        //    {
        //        if (roleInfo.AccessRights.IsForDepartmentOnly)
        //            tempRestrictionData.DepartmentId = user.Resource.DepartmentId;
        //        if (roleInfo.AccessRights.IsForDivisionOnly)
        //            tempRestrictionData.DivisionId = user.Resource.DivisionId;                
        //    }
        //    #endregion

        //    var report = LoadResourceSalaryOrRateReport((new ResourceReportRequest() { JobTitle = jobTitle }), tempRestrictionData, user.ResourceId);

        //    var stream = ResourceRateOrSalaryReportExport.ExportReport(report.Data, "Resources Utilitation Report") as MemoryStream;

        //    var message = new HttpResponseMessage(HttpStatusCode.OK)
        //    {
        //        // ReSharper disable once PossibleNullReferenceException
        //        Content = new ByteArrayContent(stream.ToArray())
        //    };
        //    message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
        //    message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        //    {
        //        FileName = "EdzResourceReport.xlsx"
        //    };

        //    return message;
        //}

        //private ListResponse<ResourceSalaryOrRateReportView> LoadResourceSalaryOrRateReport(ResourceReportRequest request, RestrictionDataModel restrictData, long? resourceId)
        //{
        //    var lst = new ListResponse<ResourceSalaryOrRateReportView>();

        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        var query = ctx.Database.SqlQuery<ResourceSalaryOrRateReportView>(LoadResourceSalaryOrRateReportQuery(restrictData, resourceId));
        //        IEnumerable<ResourceSalaryOrRateReportView> result = new List<ResourceSalaryOrRateReportView>();

        //        result = query.ToList();

        //        if (request != null && request.Department != null)
        //        {
        //            if (!string.IsNullOrEmpty(request.Department))
        //                result = result.Where(x => x.Department == request.Department);
        //        }

        //        if (request != null && request.Division != null)
        //        {
        //            if (!string.IsNullOrEmpty(request.Division))
        //                result = result.Where(x => x.Division == request.Division);
        //        }

        //        if (request != null && request.JobTitle != null)
        //        {
        //            if (!string.IsNullOrEmpty(request.JobTitle))
        //                result = result.Where(x => x.JobTitle == request.JobTitle);
        //        }

        //        lst.Data = result.ToArray();
        //    }

        //    return lst;
        //}

        //private static string LoadResourceSalaryOrRateReportQuery(RestrictionDataModel restrictData, long? resourceId)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("select R.employee_id as EmployeeId ,(R.last_name + ' '+ R.first_name) as ResourceName,JT.name as JobTitle,D.name as Department,div.name as Division,r.employment_type as EmploymentType,isnull(r.salary,0) as Salary,isnull(r.bonus_or_other_pay,0) as BonusOrOtherPay,isnull((isnull(r.salary,0)+isnull(r.bonus_or_other_pay,0))/2080,0) as Rate  ");
        //    sb.Append("\r\n");
        //    sb.Append("FROM resources R ");
        //    sb.Append("\r\n");
        //    sb.Append("	left join job_titles JT on JT.Id = R.job_title_id");
        //    sb.Append("\r\n");
        //    sb.Append("	left join departments D on D.id = R.department_id");
        //    sb.Append("\r\n");
        //    sb.Append("	left join divisions Div on Div.id = R.division_id");
        //    sb.Append("\r\n");

        //    if (restrictData != null)
        //    {
        //        if ((restrictData.DepartmentId != null) || (restrictData.DivisionId != null))
        //            sb.Append("where ");
        //        if (restrictData.DepartmentId != null && restrictData.DivisionId != null)
        //        {
        //            sb.AppendFormat(" (R.department_id = {0})", restrictData.DepartmentId);
        //            sb.AppendFormat(" and ( R.division_id = {0})", restrictData.DivisionId);
        //        }
        //        else
        //        {
        //            if (restrictData.DepartmentId != null)
        //                sb.AppendFormat(" R.department_id = {0}", restrictData.DepartmentId);
        //            if (restrictData.DivisionId != null)
        //                sb.AppendFormat(" R.division_id = {0}", restrictData.DivisionId);
        //        }
        //    }
        //    sb.Append("\r\n");
        //    sb.Append("order by ResourceName asc ,div.name asc ,D.name asc");
        //    sb.Append("\r\n");

        //    return sb.ToString();
        //}
        //#endregion

        //#region GetResourcesWorkDetailsReport
        //[HttpPost]
        //[Route("api/Report/GetResourcesWorkDetailsReport")]
        //public IHttpActionResult GetResourceWorkDetailsReport([FromBody]ResourceReportRequest request)
        //{
        //    #region  restrict permission
        //    var roleInfo = GetRoleAccessRights();

        //    var userId = User.Identity.GetUserId();
        //    var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
        //    if (user != null && user.Resource == null)
        //    {
        //        ModelState.AddModelError("Reports", "Logged in user is not associated with Resource, contact admin to assign resource");
        //        return BadRequest(ModelState);
        //    }

        //    RestrictionDataModel tempRestrictionData = new RestrictionDataModel();
        //    if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
        //    {
        //        if (roleInfo.AccessRights.IsForDepartmentOnly)
        //            tempRestrictionData.DepartmentId = user.Resource.DepartmentId;
        //        if (roleInfo.AccessRights.IsForDivisionOnly)
        //            tempRestrictionData.DivisionId = user.Resource.DivisionId;
        //    }
        //    #endregion

        //    var lst = LoadResourceWorkDetailsUtilizationReport(request, tempRestrictionData, user.ResourceId);

        //    return Ok(lst);
        //}

        //[HttpGet]
        //[Route("api/Report/export-resources-workDetails-report/")]
        //public HttpResponseMessage GetResourceWorkDetailsReportExport(string jobTitle = null, string currentUserId = null)
        //{
        //    #region  restrict permission
        //    var roleInfo = GetRoleAccessRightsByResourceId(currentUserId);
        //    if (roleInfo.RoleName != ApplicationUser.AdminRole)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.Forbidden);
        //    }
        //    else
        //    {
        //        var statusCodeResultResource = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
        //        if (statusCodeResultResource != null)
        //        {
        //            return Request.CreateResponse(HttpStatusCode.Forbidden);
        //        }
        //    }
        //    var userId = currentUserId;
        //    var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
        //    if (user != null && user.Resource == null)
        //    {
        //        ModelState.AddModelError("Reports", "Logged in user is not associated with Resource, contact admin to assign resource");
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //    }

        //    RestrictionDataModel tempRestrictionData = new RestrictionDataModel();
        //    if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
        //    {
        //        if (roleInfo.AccessRights.IsForDepartmentOnly)
        //            tempRestrictionData.DepartmentId = user.Resource.DepartmentId;
        //        if (roleInfo.AccessRights.IsForDivisionOnly)
        //            tempRestrictionData.DivisionId = user.Resource.DivisionId;                
        //    }
        //    #endregion

        //    var report = LoadResourceWorkDetailsUtilizationReport((new ResourceReportRequest() { JobTitle = jobTitle }), tempRestrictionData, user.ResourceId);
        //    var stream = ResourceWorkDetailsReportExport.ExportReport(report.Data, "Resources Report") as MemoryStream;
        //    var message = new HttpResponseMessage(HttpStatusCode.OK)
        //    {
        //        Content = new ByteArrayContent(stream.ToArray())
        //    };

        //    message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
        //    message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        //    {
        //        FileName = "EdzResourcesWorkDetailsReport.xlsx"
        //    };

        //    return message;
        //}

        //private ListResponse<ResourceWorkDetailsReportView> LoadResourceWorkDetailsUtilizationReport(ResourceReportRequest request, RestrictionDataModel restrictData, long? resourceId)
        //{
        //    var lst = new ListResponse<ResourceWorkDetailsReportView>();

        //    using (var dbContextScope = _dataContextScopeFactory.Create())
        //    {
        //        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

        //        var query = ctx.Database.SqlQuery<ResourceWorkDetailsReportView>(LoadResourceWorkDetailsUtilizationReportQuery(restrictData, resourceId));
        //        IEnumerable<ResourceWorkDetailsReportView> result = new List<ResourceWorkDetailsReportView>();

        //        result = query.ToList();

        //        if (request != null && request.JobTitle != null)
        //        {
        //            if (!string.IsNullOrEmpty(request.JobTitle))
        //                result = result.Where(x => x.JobTitle == request.JobTitle);
        //        }

        //        lst.Data = result.ToArray();
        //    }

        //    return lst;
        //}

        //private static string LoadResourceWorkDetailsUtilizationReportQuery(RestrictionDataModel restrictData, long? resourceId)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("select R.employee_id as EmployeeId ,(R.last_name + ' '+ R.first_name) as ResourceName,D.name as Department,div.name as Division,JT.name as JobTitle,");
        //    sb.Append("\r\n");
        //    sb.Append("(SELECT STUFF((SELECT ', ' + subject FROM resource_educations ED");
        //    sb.Append("\r\n");
        //    sb.Append("				WHERE R.Id =  ED.resource_id ");
        //    sb.Append("\r\n");
        //    sb.Append("				FOR XML PATH(''), TYPE ");
        //    sb.Append("\r\n");
        //    sb.Append("				).value('.', 'NVARCHAR(MAX)'), 1, 1, '')) as Education, ");
        //    sb.Append("\r\n");
        //    sb.Append("(SELECT STUFF((SELECT ',' + name  ");
        //    sb.Append("\r\n");
        //    sb.Append("				FROM skills ");
        //    sb.Append("\r\n");
        //    sb.Append("					left join resource_skills R_skill on R_skill.skill_id = skills.Id ");
        //    sb.Append("\r\n");
        //    sb.Append("				WHERE R.Id =  R_skill.resource_id ");
        //    sb.Append("\r\n");
        //    sb.Append("				FOR XML PATH(''), TYPE ");
        //    sb.Append("\r\n");
        //    sb.Append("				).value('.', 'NVARCHAR(MAX)'), 1, 1, '')) as Skills, ");
        //    sb.Append("\r\n");
        //    sb.Append("(SELECT STUFF((SELECT ',' + name ");
        //    sb.Append("\r\n");
        //    sb.Append("				FROM certificates ");
        //    sb.Append("\r\n");
        //    sb.Append("					left join resource_certificates R_cert on R_cert.certificate_id = certificates.Id ");
        //    sb.Append("\r\n");
        //    sb.Append("				WHERE R.Id =  R_cert.resource_id ");
        //    sb.Append("\r\n");
        //    sb.Append("				FOR XML PATH(''), TYPE ");
        //    sb.Append("\r\n");
        //    sb.Append("				).value('.', 'NVARCHAR(MAX)'), 1, 1, '')) as Certifications, ");
        //    sb.Append("\r\n");
        //    sb.Append("(Case when R.works_well_alone  = 1 AND  R.works_well_in_team = 1 Then 'Works well alone / in team'  ");
        //    sb.Append("\r\n");
        //    sb.Append("		when R.works_well_alone  = 1 AND  R.works_well_in_team = 0 then 'Works well alone'  ");
        //    sb.Append("\r\n");
        //    sb.Append("		when R.works_well_alone  = 0 AND  R.works_well_in_team = 1 then 'Works well in team'  ");
        //    sb.Append("\r\n");
        //    sb.Append("	else'' End) as WorkDetails, ");
        //    sb.Append("\r\n");
        //    sb.Append("(SELECT STUFF((SELECT ',' + name  ");
        //    sb.Append("\r\n");
        //    sb.Append("				FROM languages ");
        //    sb.Append("\r\n");
        //    sb.Append("					left join resource_spoken_languages R_lan on R_lan.language_id = languages.Id ");
        //    sb.Append("\r\n");
        //    sb.Append("				WHERE R.Id =  R_lan.resource_id ");
        //    sb.Append("\r\n");
        //    sb.Append("				FOR XML PATH(''), TYPE ");
        //    sb.Append("\r\n");
        //    sb.Append("				).value('.', 'NVARCHAR(MAX)'), 1, 1, '')) as Languages ");
        //    sb.Append("\r\n");
        //    sb.Append("from resources R ");
        //    sb.Append("\r\n");
        //    sb.Append("	left join divisions div on div.Id = R.division_id ");
        //    sb.Append("\r\n");
        //    sb.Append("	left join departments D on D.id = R.department_id");
        //    sb.Append("\r\n");
        //    sb.Append("	left join job_titles JT on JT.Id = R.job_title_id ");
        //    sb.Append("\r\n");
        //    if (restrictData != null)
        //    {
        //        if ((restrictData.DepartmentId != null) || (restrictData.DivisionId != null))
        //        {
        //            sb.Append("where ");
        //            sb.Append("\r\n");
        //        }
        //        if (restrictData.DepartmentId != null && restrictData.DivisionId != null)
        //        {
        //            sb.AppendFormat(" (R.department_id = {0})", restrictData.DepartmentId);
        //            sb.AppendFormat(" and ( R.division_id = {0})", restrictData.DivisionId);
        //            sb.Append("\r\n");
        //        }
        //        else
        //        {
        //            if (restrictData.DepartmentId != null)
        //            {
        //                sb.AppendFormat(" R.department_id = {0}", restrictData.DepartmentId);
        //                sb.Append("\r\n");
        //            }
        //            if (restrictData.DivisionId != null)
        //            {
        //                sb.AppendFormat(" R.division_id = {0}", restrictData.DivisionId);
        //                sb.Append("\r\n");
        //            }

        //        }
        //    }
        //    sb.Append("order by ResourceName asc ,div.name asc ,D.name asc");
        //    sb.Append("\r\n");

        //    return sb.ToString();
        //}
        //#endregion
        
    }
}