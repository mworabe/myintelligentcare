﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Practice Area.
    /// </summary>
    public class ResourcePracticeArea : Entity
    {
        public long? ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        
        public long? PracticeAreaId { get; set; }
        public virtual DropDownConfig PracticeArea { get; set; }

        // for the Display we are added field and not created in Table
        public string Name { get { return PracticeArea != null ? PracticeArea.Name : string.Empty; } }

    }
}
