﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.LeaveTypes
{
    [DataContract]
    public class LeaveTypeAutoCompleteResponse 
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

    }
}