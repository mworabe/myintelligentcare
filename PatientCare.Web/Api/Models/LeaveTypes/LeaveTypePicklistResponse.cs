﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.LeaveTypes
{
    [DataContract]
    public class LeaveTypePicklistResponse
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string IsPaid { get; set; }
        [DataMember]
        public int TotalLeaves { get; set; }
        [DataMember]
        public string Color { get; set; }
    }
}