﻿'use strict';

angular.module("EDZoutstaffingPortalApp").factory('backendHubProxy', ['$rootScope', 
  function ($rootScope) {

      function backendFactory(serverUrl, hubName) {
          var connection = $.hubConnection(serverUrl);
          var proxy = connection.createHubProxy(hubName);
          proxy.on("broadcastAppointmentUpdate", function (data) {
              console.info("broadcastAppointmentUpdate");
          });

          connection.start().done(function () {
              console.info("connection establidfsdf");
          });

          return {
              on: function (eventName, callback) {
                  console.info("on->" + eventName);
                  proxy.on(eventName, function (result) {
                      console.info("on->" + eventName + " result:" + result);
                      $rootScope.$apply(function () {
                          if (callback) {
                              callback(result);
                          }
                      });
                  });
              },
              invoke: function (methodName, callback) {
                  proxy.invoke(methodName)
                  .done(function (result) {
                      console.info("invoke->" + methodName + " result:" + result);
                      $rootScope.$apply(function () {
                          if (callback) {
                              callback(result);
                          }
                      });
                  });
              }
          };
      };

      return backendFactory;
  }]);