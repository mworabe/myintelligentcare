﻿namespace PatientCare.Web.Api.Models.CustomFieldProperties
{
    public class CustomFieldPropertyRequest
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CustomFieldId { get; set; }
        public string Value { get; set; }
    }
}