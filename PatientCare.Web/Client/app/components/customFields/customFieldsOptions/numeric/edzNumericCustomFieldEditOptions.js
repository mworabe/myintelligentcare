﻿"use strict";

(function edzNumericCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzNumericCustomFieldEditOptions", [
            "edzCustomFieldEditHelper",
            "CUSTOM_FIELD",
            edzNumericCustomFieldOptions
        ]);

    function edzNumericCustomFieldOptions(edzCustomFieldEditHelper, CUSTOM_FIELD) {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/numeric/edzNumericCustomFieldEdit.html",
            restrict: "E",
            scope: true,
            controller: ["$scope", function ($scope) {
                var self = this;
                function loadData() {
                    self.model = {};
                    self.model.from = edzCustomFieldEditHelper.
                        getOrCreatePropertyWithPush($scope.data, CUSTOM_FIELD.PROPERTY_NAME.NUMERIC.FROM, $scope.id);
                    self.model.to = edzCustomFieldEditHelper.getOrCreatePropertyWithPush($scope.data, CUSTOM_FIELD.PROPERTY_NAME.NUMERIC.TO, $scope.id);
                    self.model.numberOfDecimals = edzCustomFieldEditHelper.getOrCreatePropertyWithPush($scope.data, CUSTOM_FIELD.PROPERTY_NAME.NUMERIC.NUMBER_OF_DECIMALS, $scope.id);

                    self.model.from.value = parseInt(self.model.from.value);
                    self.model.to.value = parseInt(self.model.to.value);
                    self.model.numberOfDecimals.value = parseInt(self.model.numberOfDecimals.value);
                }

                function activate() {
                    loadData();
                }

                activate();
            }],
            controllerAs: "edzNumericCustomFieldEditCtrl"
        };
    }
})();
