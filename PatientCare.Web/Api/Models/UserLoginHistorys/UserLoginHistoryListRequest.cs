﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PatientCare.Web.Api.Models.UserLoginHistorys
{
    public class UserLoginHistoryListRequest : ListRequest
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Action { get; set; }        

        [DataMember]
        public string IPAddress { get; set; }

        [DataMember]
        public string SessionKey { get; set; }
        
        [DataMember]
        public string Username { get; set; }
    }
}