(function customFieldCtrlIIFE() {
    "use strict";
    angular.module("EDZoutstaffingPortalApp")
        .controller("CustomFieldCtrl", [ "CustomField",
            customFieldCtrl
        ]);

    function customFieldCtrl(customFieldService) {
        this.get = customFieldService.query;
        this.option = customFieldService.getTableOption;
        this.delete = customFieldService.delete;
    }

})();

