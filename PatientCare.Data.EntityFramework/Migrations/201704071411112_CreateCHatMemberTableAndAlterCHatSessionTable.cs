namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class CreateCHatMemberTableAndAlterCHatSessionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.chat_members",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    chat_session_id = c.Long(),
                    user_id = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.chat_Sessions", t => t.chat_session_id)
                .Index(t => t.chat_session_id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.chat_members", "chat_session_id", "dbo.chat_Sessions");
            DropIndex("dbo.chat_members", new[] { "chat_session_id" });
            DropTable("dbo.chat_members");
        }
    }
}
