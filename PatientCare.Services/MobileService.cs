﻿using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle mobile application operations And implementing all methods which is initialize in IMobileService.
    /// </summary>
    public class MobileService : IMobileService
    {
        /// <summary>
        ///Description : Mobile Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Mobile Service Field IExerciseRatingRepository
        /// </summary>
        private readonly IExerciseRatingRepository _exerciseRatingRepository;

        /// <summary>
        ///Description : Mobile Service Field IExerciseCommentRepository
        /// </summary>
        private readonly IExerciseCommentRepository _exerciseCommentRepository;

        /// <summary>
        ///Description : Mobile Service Field IAppFunctionalRatingRepository
        /// </summary>
        private readonly IAppFunctionalRatingRepository _appFunctionalRatingRepository;

        /// <summary>
        ///Description : Mobile Service Field IPatientMobileProfileRepository
        /// </summary>
        private readonly IPatientMobileProfileRepository _patientMobileProfileRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Mobile class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="exerciseRatingRepository">Exercise Rating Repository</param>
        /// <param name="exerciseCommentRepository">Exercise Comment Repository</param>
        /// <param name="appFunctionalRatingRepository">App Functional Rating Repository</param>
        /// <param name="patientMobileProfileRepository">Patient Mobile Profile Repository</param>
        public MobileService
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IExerciseRatingRepository exerciseRatingRepository,
                IExerciseCommentRepository exerciseCommentRepository,
                IAppFunctionalRatingRepository appFunctionalRatingRepository,
                IPatientMobileProfileRepository patientMobileProfileRepository
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _exerciseRatingRepository = exerciseRatingRepository;
            _exerciseCommentRepository = exerciseCommentRepository;
            _appFunctionalRatingRepository = appFunctionalRatingRepository;
            _patientMobileProfileRepository = patientMobileProfileRepository;
        }

        #region Exercise Rating Methods

        /// <summary>
        /// Method Description : Using for Create / Update Exercise Rating.
        /// </summary>
        /// <param name="exerciseRating">Exercise Rating Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> InsertUpdateExerciseRatingAsync(ExerciseRating exerciseRating)
        {
            if (exerciseRating == null) throw new ArgumentNullException("exerciseRatings");
            if (exerciseRating.Id == 0)
            {
                await _exerciseRatingRepository.CreateAsync(exerciseRating, UpdateOptions.Create<ExerciseRating>()
                    .WithNotModified(p => p.Created)
                    .WithNotModified(p => p.Modified));
            }
            else
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var originExerciseRating = _exerciseRatingRepository.Read(exerciseRating.Id, new ReadOptions<ExerciseRating>().AsReadOnly());
                    await _exerciseRatingRepository.UpdateAsync(exerciseRating, UpdateOptions.Create<ExerciseRating>()
                        .WithNotModified(p => p.Created));
                }
            }

            return true;
        }
        #endregion

        #region Exercise Comment Methods

        /// <summary>
        /// Method Description : Using for Create / Update Exercise Comment.
        /// </summary>
        /// <param name="exerciseComment">Exercise Comment Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> InsertUpdateExerciseCommentAsync(ExerciseComment exerciseComment)
        {
            if (exerciseComment == null) throw new ArgumentNullException("exerciseComments");
            if (exerciseComment.Id == 0)
            {
                await _exerciseCommentRepository.CreateAsync(exerciseComment, UpdateOptions.Create<ExerciseComment>()
                    .WithNotModified(p => p.Created)
                    .WithNotModified(p => p.Modified));
            }
            else
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var originExerciseComment = _exerciseCommentRepository.Read(exerciseComment.Id, new ReadOptions<ExerciseComment>().AsReadOnly());
                    await _exerciseCommentRepository.UpdateAsync(exerciseComment, UpdateOptions.Create<ExerciseComment>()
                        .WithNotModified(p => p.Created));
                }
            }

            return true;
        }
        #endregion

        #region App Functional Rating Methods

        /// <summary>
        /// Method Description : Using for Create / Update App Functional Rating.
        /// </summary>
        /// <param name="appFunctionalRating">App Functional Rating Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> InsertUpdateAppFunctionalRatingAsync(AppFunctionalRating appFunctionalRating)
        {
            if (appFunctionalRating == null) throw new ArgumentNullException("AppFunctionalRatings");
            if (appFunctionalRating.Id == 0)
            {
                await _appFunctionalRatingRepository.CreateAsync(appFunctionalRating, UpdateOptions.Create<AppFunctionalRating>()
                    .WithNotModified(p => p.Created)
                    .WithNotModified(p => p.Modified));
            }
            else
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var originAppFunctionalRating = _appFunctionalRatingRepository.Read(appFunctionalRating.Id, new ReadOptions<AppFunctionalRating>().AsReadOnly());
                    await _appFunctionalRatingRepository.UpdateAsync(appFunctionalRating, UpdateOptions.Create<AppFunctionalRating>()
                        .WithNotModified(p => p.Created));
                }
            }

            return true;
        }
        #endregion

        #region Patient Mobile Profile Methods

        /// <summary>
        /// Method Description : Using for Create / Update Patient Mobile Profile.
        /// </summary>
        /// <param name="patientMobileProfile">Patient Mobile Profile Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> InsertUpdatePatientMobileProfileAsync(PatientMobileProfile patientMobileProfile)
        {
            if (patientMobileProfile == null) throw new ArgumentNullException("patientmobileprofile");
            if (patientMobileProfile.Id == 0)
            {
                await _patientMobileProfileRepository.CreateAsync(patientMobileProfile, UpdateOptions.Create<PatientMobileProfile>()
                    .WithNotModified(p => p.Created)
                    .WithNotModified(p => p.Modified));
            }
            else
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                    ProfileChidlrenProcessing(ctx, patientMobileProfile);

                    var originProfile = _patientMobileProfileRepository.Read(patientMobileProfile.Id, new ReadOptions<PatientMobileProfile>().AsReadOnly());
                    await _patientMobileProfileRepository.UpdateAsync(patientMobileProfile, UpdateOptions.Create<PatientMobileProfile>()
                        .WithNotModified(p => p.Created));
                }
            }

            return true;
        }

        /// <summary>
        /// Method Description : Using for Update patient profile goal.
        /// </summary>
        /// <param name="ctx">DbContext</param>
        /// <param name="profile">Patient Mobile Profile Entity</param>
        private static void ProfileChidlrenProcessing(DbContext ctx, PatientMobileProfile profile)
        {
            var goals = profile.Goals;
            ctx.ChildrenProcessing(goals, x => x.PatientMobileProfileId == profile.Id);
        }
        #endregion
    }
}
