﻿using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;

namespace PatientCare.Services
{
    /// <summary>
    /// This service is used to handle App utility operation And Implementing all methods which is initialize in IAppUtilityService.
    /// </summary>
    public class AppUtilityService : IAppUtilityService
    {
        /// <summary>
        ///Description : AppUtility Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Initializes a new instance of the  App Utility class Constructor
        /// </summary>
        /// <param name="dataContextScopeFactory"></param>
        public AppUtilityService
            (
              IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
        }
    }
}
