﻿using PatientCare.Core.Entities;
using PatientCare.Data.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle State Controller operations.
    /// </summary>
    [Authorize]
    public class StateController : ApiController
    {
        private PatientCareDbContext db = new PatientCareDbContext();

        /// <summary>
        /// Method Description : Using for Get States by Country Id,Sate Id. 
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <param name="countryId">Country Id</param>
        /// <param name="cityId">City Id</param>
        /// <returns></returns>
        public IQueryable<State> getstates(string search, long? countryId = null, long? cityId = null)
        {
            List<State> tempList = new List<State>();
            long? stateId = null;
            if (cityId != null)
                stateId = db.Cities.AsNoTracking().Where(x => x.Id == cityId).Select(x => x.StateId).FirstOrDefault();

            if (countryId != null && cityId == null)
                tempList = db.States.Where(m => (m.Name.StartsWith(search) || search == null) && m.CountryId == countryId).OrderBy(x => x.Name).Take(20).ToList();

            if (countryId != null && cityId != null)
                tempList = db.States.Where(m => (m.Name.StartsWith(search) || search == null) && m.Id == stateId && m.CountryId == countryId).OrderBy(x => x.Name).Take(20).ToList();

            if (countryId == null && cityId != null)
                tempList = db.States.Where(m => (m.Name.StartsWith(search) || search == null) && m.Id == stateId).OrderBy(x => x.Name).Take(20).ToList();

            if (countryId == null && cityId == null)
                tempList = db.States.Where(m => m.Name.StartsWith(search) || search == null).OrderBy(x => x.Name).Take(20).ToList();

            IQueryable<State> list = tempList.AsQueryable();
            return list;
        }
    }
}