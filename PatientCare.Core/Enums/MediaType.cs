﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Media Type Enum.
    /// </summary>
    public enum MediaType
    {
        Image = 0,
        Video = 1
    }
}
