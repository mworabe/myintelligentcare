﻿using PatientCare.Web.Api.Models.Reports;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PatientCare.Web.Services.Excel
{
    public static class ResourceRateOrSalaryReportExport
    {
        public static Stream ExportReport(IList<ResourceSalaryOrRateReportView> reportData, string title)
        {
            using (var excelPackage = new ExcelPackage(new MemoryStream()))
            {
                excelPackage.Workbook.Properties.Author = "IRMS";
                excelPackage.Workbook.Properties.Title = title;

                var worksheet = excelPackage.Workbook.Worksheets.Add("Resources");

                ApplyHeader(worksheet);


                for (var i = 0; i < reportData.Count; i++)
                {
                    FillUpTheRow(worksheet, reportData[i], i + 2);
                }

                excelPackage.Save();
                return excelPackage.Stream;
            }
        }

        private static void FillUpTheRow(ExcelWorksheet worksheet, ResourceSalaryOrRateReportView reporData, int i)
        {
            worksheet.Cells[i, 1].Value = reporData.EmployeeId;
            worksheet.Cells[i, 2].Value = reporData.ResourceName;
            worksheet.Cells[i, 3].Value = reporData.Division;
            worksheet.Cells[i, 4].Value = reporData.Department;
            worksheet.Cells[i, 5].Value = reporData.JobTitle;
            worksheet.Cells[i, 6].Value = reporData.EmploymentType.ToString(); ;
            worksheet.Cells[i, 7].Value = ((reporData.Salary != 0) ? "$" : " ") + reporData.Salary;
            worksheet.Cells[i, 8].Value = ((reporData.BonusOrOtherPay != 0) ? "$" : " ") + reporData.BonusOrOtherPay;
            worksheet.Cells[i, 9].Value = ((reporData.Rate != 0) ? "$" : " ") + reporData.Rate;
        }

        private static void ApplyHeader(ExcelWorksheet worksheet)
        {
            worksheet.Cells[1, 1].Value = "Employee Id";
            worksheet.Cells[1, 1].Style.Font.Bold = true;

            worksheet.Cells[1, 2].Value = "Resource Name";
            worksheet.Cells[1, 2].Style.Font.Bold = true;

            worksheet.Cells[1, 3].Value = "Division";
            worksheet.Cells[1, 3].Style.Font.Bold = true;

            worksheet.Cells[1, 4].Value = "Department";
            worksheet.Cells[1, 4].Style.Font.Bold = true;

            worksheet.Cells[1, 5].Value = "Job Title";
            worksheet.Cells[1, 5].Style.Font.Bold = true;
            
            worksheet.Cells[1, 6].Value = "Employment Type";
            worksheet.Cells[1, 6].Style.Font.Bold = true;

            worksheet.Cells[1, 7].Value = "Salary";
            worksheet.Cells[1, 7].Style.Font.Bold = true;

            worksheet.Cells[1, 8].Value = "Bonus Or Other Pay";
            worksheet.Cells[1, 8].Style.Font.Bold = true;

            worksheet.Cells[1, 9].Value = "Hourly Rate";
            worksheet.Cells[1, 9].Style.Font.Bold = true;

        }
    }
}