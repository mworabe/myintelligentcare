﻿using PatientCare.Web.Api.Models.ResourceSkills;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Picklist
{
    [DataContract]
    public class PicklistResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DisplayName
        {
            get { return (Name != null ? Name : ""); }
            set { DisplayName = value; }
        }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string ConfigField1 { get; set; }

        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string MobilePhoneNumber { get; set; }

        [DataMember]
        public bool? IsActive { get; set; }

        [DataMember]
        public string TimekeeperNumber { get; set; }
    }
}