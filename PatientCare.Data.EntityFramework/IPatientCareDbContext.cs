using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework
{
    public interface IPatientCareDbContext
    {
        System.Data.Entity.DbSet<Certificate> Certificates { get; set; }
        System.Data.Entity.DbSet<CustomFieldProperty> CustomFieldProperties { get; set; }
        System.Data.Entity.DbSet<CustomField> CustomFields { get; set; }
        System.Data.Entity.DbSet<Department> Departments { get; set; }
        System.Data.Entity.DbSet<Division> Divisions { get; set; }
        System.Data.Entity.DbSet<Language> Languages { get; set; }
        System.Data.Entity.DbSet<JobTitle> Positions { get; set; }
        System.Data.Entity.DbSet<ResourceCertificate> ResourceCertificates { get; set; }
        System.Data.Entity.DbSet<ResourceCompany> ResourceCompanies { get; set; }
        System.Data.Entity.DbSet<ResourceCriminalRecord> ResourceCriminalRecords { get; set; }
        System.Data.Entity.DbSet<ResourceBehaviouralDetail> ResourceBehaviouralDetails { get; set; }
        System.Data.Entity.DbSet<ResourceRecognition> ResourceRecognitions { get; set; }
        System.Data.Entity.DbSet<ResourceIndustry> Industry { get; set; }
        System.Data.Entity.DbSet<ResourceCustomFieldValue> ResourceCustomFieldValues { get; set; }
        System.Data.Entity.DbSet<ResourceEducation> ResourceEducations { get; set; }
        System.Data.Entity.DbSet<ResourcePracticeArea> ResourcePracticeAreas { get; set; }
        System.Data.Entity.DbSet<ResourcePreviousEmployment> ResourcePreviousEmployments { get; set; }
        System.Data.Entity.DbSet<Resource> Resources { get; set; }
        System.Data.Entity.DbSet<ResourceWorkExperience> ResourceWorkExperiences { get; set; }
        System.Data.Entity.DbSet<ResourceWorkAssignment> ResourceWorkAssignments { get; set; }
        System.Data.Entity.DbSet<ResourceScoreFilter> ResourceScoreFilters { get; set; }
        System.Data.Entity.DbSet<ResourceSkill> ResourceSkills { get; set; }
        System.Data.Entity.DbSet<ResourceSpokenLanguage> ResourceSpokenLanguages { get; set; }
        System.Data.Entity.DbSet<ResourceSurveyResult> ResourceSurveyResults { get; set; }
        System.Data.Entity.DbSet<ResourceTeam> ResourceTeams { get; set; }
        System.Data.Entity.DbSet<ResourceTravel> ResourceTravels { get; set; }
        System.Data.Entity.DbSet<PostalCode> PostalCodes { get; set; }
        System.Data.Entity.DbSet<RoleConfigureLayout> RoleConfigureLayouts { get; set; }
        System.Data.Entity.DbSet<ResourceProfessionalOrganization> ResourceProfessionalOrganizations { get; set; }
        System.Data.Entity.DbSet<Country> Countries { get; set; }
        System.Data.Entity.DbSet<State> States { get; set; }
        System.Data.Entity.DbSet<City> Cities { get; set; }
        System.Data.Entity.DbSet<Holiday> Holidays { get; set; }
        System.Data.Entity.DbSet<LeaveType> LeaveTypes { get; set; }
        System.Data.Entity.DbSet<EntryType> EntryTypes { get; set; }
        System.Data.Entity.DbSet<DropDownConfig> DropDownConfigs { get; set; }
        System.Data.Entity.DbSet<LeaveRequest> LeaveRequests { get; set; }
        System.Data.Entity.DbSet<UserLoginHistory> UserLoginHistorys { get; set; }
        System.Data.Entity.DbSet<DashboardSetup> DashboardSetups { get; set; }
        System.Data.Entity.DbSet<TrialPeriod> TrialPeriods { get; set; }
        System.Data.Entity.DbSet<UiConfiguration> UiConfigurations { get; set; }

        System.Data.Entity.DbSet<ResourceInsurance> ResourceInsurances { get; set; }
        System.Data.Entity.DbSet<ResourceTimeoffBenefit> ResourceTimeoffBenefits { get; set; }

        System.Data.Entity.DbSet<Patient> Patients { get; set; }
        System.Data.Entity.DbSet<PatientAddress> PatientAddresss { get; set; }
        System.Data.Entity.DbSet<PatientParentGuardian> PatientParentGuardians { get; set; }

        System.Data.Entity.DbSet<Clinic> Clinics { get; set; }
        System.Data.Entity.DbSet<ClinicLocation> ClinicLocations { get; set; }
        System.Data.Entity.DbSet<ReferralPhysician> ReferralPhysicians { get; set; }
        System.Data.Entity.DbSet<AppointmentType> AppointmentTypes { get; set; }

        System.Data.Entity.DbSet<SurveyCategory> SurveyCategories { get; set; }
        System.Data.Entity.DbSet<SurveyQuestion> SurveyQuestions { get; set; }

        System.Data.Entity.DbSet<InjuryRegion> InjuryRegions { get; set; }
        System.Data.Entity.DbSet<Diagnosis> Diagnosiss { get; set; }
        System.Data.Entity.DbSet<PatientCase> PatientCases { get; set; }
        System.Data.Entity.DbSet<PatientAppointment> PatientAppointments { get; set; }

        System.Data.Entity.DbSet<ExercisePosition> ExercisePositions { get; set; }
        System.Data.Entity.DbSet<ExerciseActivity> ExerciseActivities { get; set; }
        System.Data.Entity.DbSet<Exercise> Exercises { get; set; }
        System.Data.Entity.DbSet<ExerciseMedia> ExerciseMedias { get; set; }
        System.Data.Entity.DbSet<Exercise_ExerciseActivity> exerciseActivity { get; set; }
        System.Data.Entity.DbSet<Exercise_injury> injury { get; set; }

        System.Data.Entity.DbSet<MasterTemplate> MasterTemplates { get; set; }
        System.Data.Entity.DbSet<MasterTemplateExercise> MasterTemplateExercises { get; set; }

        System.Data.Entity.DbSet<HepGroup> HepGroups { get; set; }
        System.Data.Entity.DbSet<HepMaster> HepMasters { get; set; }
        System.Data.Entity.DbSet<HepDetail> HepDetails { get; set; }
        System.Data.Entity.DbSet<HepFrequency> HepFrequencys { get; set; }
        System.Data.Entity.DbSet<PatientWorkoutAnalytic> PatientWorkoutAnalytics { get; set; }

        System.Data.Entity.DbSet<ExerciseComment> ExerciseComments { get; set; }
        System.Data.Entity.DbSet<ExerciseRating> ExerciseRatings { get; set; }
        System.Data.Entity.DbSet<PatientMobileProfile> PatientMobileProfiles { get; set; }
        System.Data.Entity.DbSet<AppFunctionalRating> AppFunctionalRatings { get; set; }
        //System.Data.Entity.DbSet<PatientGoal> PatientGoals { get; set; }
        System.Data.Entity.DbSet<ChatSession> ChatSessions { get; set; }
        System.Data.Entity.DbSet<ChatMember> ChatMembers { get; set; }
        System.Data.Entity.DbSet<ChatKey> ChatKeys { get; set; }
        System.Data.Entity.DbSet<HepPatientsPlan> HepPatientsPlans { get; set; }
        int SaveChanges();

        System.Data.Entity.DbSet<Skill> Skills { get; set; }

    }
}
