namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyCreatedModifyInChatSessionTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.chat_Sessions", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.chat_Sessions", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.chat_Sessions", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.chat_Sessions", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
    }
}
