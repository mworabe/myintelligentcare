﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.UiConfiguration;

namespace PatientCare.Web.Services.Mapping
{
    public static class UiConfigurationMapperExtensions
    {
        public static EmitMapperMapperProvider UiConfigurationRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
             if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<UiConfiguration, UiConfigurationResponse>
                (new ExtendedMapConfig<UiConfiguration, UiConfigurationResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<UiConfiguration[], UiConfigurationResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<UiConfigurationRequest, UiConfiguration>
                (new ExtendedMapConfig<UiConfigurationRequest, UiConfiguration>()));

           return mapperProvider;
        }
    }
}