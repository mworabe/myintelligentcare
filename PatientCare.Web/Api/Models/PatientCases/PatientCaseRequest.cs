﻿using PatientCare.Core.Enums;
using System;

namespace PatientCare.Web.Api.Models.PatientCases
{
    public class PatientCaseRequest
    {
        public long Id { get; set; }
        public long? PatientId { get; set; }
        public string PatientNumber { get; set; }
        public string CaseNo { get; set; }
        public long? ClinicId { get; set; }
        public long? ClinicLocationId { get; set; }
        public CaseStatusEnum? CaseStatus { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? Age { get; set; }
        public long? InjuryRegionId { get; set; }
        public long? AssignedPhysicianId { get; set; }
        public long? ReferingPhysicianId { get; set; }
        public DateTime? ReturnTo { get; set; }
        public AuthorizeationRequiredEnum? AuthorizeationRequired { get; set; }
        public string AuthorizeationNumber { get; set; }
        public RelatedCauseEnum? RelatedCause { get; set; }
        public long? PrimayDiagnosisId { get; set; }
        public long? SecondaryDiagnosisId { get; set; }
        public string WebPTCaseId { get; set; }
        public bool? IsDeleted { get; set; }
    }   
}