﻿
using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.ResourceProfessionalOrganizations;
using PatientCare.Web.Api.Models.Resources;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.AdvanceSearches
{
    [DataContract]
    public class SelectValueResponse
    {
        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string Name { get; set; }

        public SelectValueResponse(string value, string name)
        {
            this.Value = value;
            this.Name = name;
        }
    }
    public class TableNameModel
    {
        public static string TABLE_RESOURCE = "resources";
        public static string TABLE_PROJECT = "projects";
        public static string TABLE_COUNTRY = "countries";
        public static string TABLE_DEPARTMENT = "departments";
        public static string TABLE_DIVISION = "divisions";
        public static string TABLE_DROP_DOWN_CONFIGS = "drop_down_configs";
        public static string TABLE_CITY = "Cities";
        public static string TABLE_CERTIFICATE = "certificates";
        public static string TABLE_JOB_TITLE = "job_titles";
        public static string TABLE_LANGUAGES = "languages";
        public static string TABLE_RESOURCE_AVAILABILITY = "resource_availabilities";
        public static string TABLE_RESOURCE_CERTITIFICATES = "resource_certificates";
        public static string TABLE_RESOURCE_COMPANY = "resource_companies";
        public static string TABLE_RESOURCE_CRIMINAL_RECORD = "resource_criminal_records";
        public static string TABLE_RESOURCE_DEMAND = "resource_demands";
        public static string TABLE_RESOURCE_EDUCATION = "resource_educations";
        public static string TABLE_RESOURCE_PREVIOUS_EMPLOYMENT = "resource_previous_employments";
        public static string TABLE_RESOURCE_PROFESSIONAL_ORGANIZATION = "resource_professional_organizations";
        public static string TABLE_RESOURCE_SKILLS = "resource_skills";
        public static string TABLE_RESOURCE_SPOKEN_LANGUAGES = "resource_spoken_languages";
        public static string TABLE_RESOURCE_TEAMS = "resource_teams";
        public static string TABLE_RESOURCE_TRAVELS = "resource_travels";
        public static string TABLE_RESOURCE_WORK_ASSIGNMENT = "ResourceWorkAssignments";
        public static string TABLE_RESOURCE_WORK_EXPERIENCE = "ResourceWorkExperiences";
        public static string TABLE_SKILL = "skills";
        public static string TABLE_STATE = "states";
    }

    public class FieldTypeModel
    {
        public static string TEXT = "TEXT";
        public static string DATE_TIME = "DATE_TIME";
        public static string UI_SELECT = "UI_SELECT";
        public static string AUTOCOMPLETE = "AUTOCOMPLETE";
        public static string PHONE = "PHONE";
        public static string NUMBER = "NUMBER";
        public static string CHECKBOX = "CHECKBOX";
        public static string TEXTAREA = "TEXTAREA";
        public static string CURRENCY = "CURRENCY";
        public static string CUSTOM_TEMPLATE = "CUSTOM_TEMPLATE";

    }

    [DataContract]
    public class FieldsListResponse
    {
        [DataMember]
        public string TableName { get; set; }

        [DataMember]
        public string FieldName { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string FieldType { get; set; }

        [DataMember]
        public bool IsApiCall { get; set; }

        [DataMember]
        public string ApiCallUrl { get; set; }

        [DataMember]
        public bool IsCustomField { get; set; }

        [DataMember]
        public SelectValueResponse[] DropdownOptions { get; set; }

        public FieldsListResponse
            (string tableName, string fieldName, string displayName, string fieldType,
                bool isApiCall, string apiCallUrl, bool isCustomField,
                SelectValueResponse[] selectValue
            )
        {
            this.TableName = tableName;
            this.FieldName = fieldName;
            this.DisplayName = displayName;
            this.FieldType = fieldType;
            this.IsApiCall = isApiCall;
            this.ApiCallUrl = apiCallUrl;
            this.IsCustomField = isCustomField;
            this.DropdownOptions = selectValue;
        }

        public static List<FieldsListResponse> GetResourceFilterFields(string fields)
        {
            List<FieldsListResponse> list = new List<FieldsListResponse>();

            #region Resource Table Field Loaded            
            if (fields == TableNameModel.TABLE_RESOURCE)
            {
                #region genderList
                List<SelectValueResponse> genderList = new List<SelectValueResponse>();
                genderList.Add(new SelectValueResponse("0", GenderEnum.Male.ToString()));
                genderList.Add(new SelectValueResponse("1", GenderEnum.Female.ToString()));
                #endregion

                #region martialStatusList
                List<SelectValueResponse> martialStatusList = new List<SelectValueResponse>();
                martialStatusList.Add(new SelectValueResponse("0", MartialStatusEnum.Single.ToString()));
                martialStatusList.Add(new SelectValueResponse("1", MartialStatusEnum.Married.ToString()));
                #endregion

                #region timeZoneInfoList
                List<SelectValueResponse> timeZoneInfoList = new List<SelectValueResponse>();
                var timeZoneNames = TimeZoneData.GetTimeZoneData();
                foreach (var item in timeZoneNames)
                {
                    timeZoneInfoList.Add(new SelectValueResponse(item.id, item.name));
                }
                #endregion

                #region Relationship
                List<SelectValueResponse> relationshipList = new List<SelectValueResponse>();
                relationshipList.Add(new SelectValueResponse("0", RelationshipEnum.Spouse.ToString()));
                relationshipList.Add(new SelectValueResponse("1", RelationshipEnum.Partner.ToString()));
                relationshipList.Add(new SelectValueResponse("2", RelationshipEnum.Parent.ToString()));
                relationshipList.Add(new SelectValueResponse("3", RelationshipEnum.Son.ToString()));
                relationshipList.Add(new SelectValueResponse("4", RelationshipEnum.Daughter.ToString()));
                relationshipList.Add(new SelectValueResponse("5", RelationshipEnum.Sibling.ToString()));
                relationshipList.Add(new SelectValueResponse("6", RelationshipEnum.Friend.ToString()));
                relationshipList.Add(new SelectValueResponse("7", RelationshipEnum.Other.ToString()));
                #endregion

                #region Employment Type
                List<SelectValueResponse> employmentTypeList = new List<SelectValueResponse>();
                employmentTypeList.Add(new SelectValueResponse("0", EmploymentTypeEnum.Hourly.ToString()));
                employmentTypeList.Add(new SelectValueResponse("1", EmploymentTypeEnum.Salary.ToString()));
                #endregion

                #region resourceTypeList
                List<SelectValueResponse> resourceTypeList = new List<SelectValueResponse>();
                resourceTypeList.Add(new SelectValueResponse("0", ResourceTypeEnum.Internal.ToString()));
                resourceTypeList.Add(new SelectValueResponse("1", ResourceTypeEnum.External.ToString()));
                resourceTypeList.Add(new SelectValueResponse("2", ResourceTypeEnum.Consultant.ToString()));
                #endregion

                #region Visa or Work Permit List
                List<SelectValueResponse> visaOrPermitList = new List<SelectValueResponse>();
                visaOrPermitList.Add(new SelectValueResponse("0", VisaOrPermitEnum.Visa.ToString()));
                visaOrPermitList.Add(new SelectValueResponse("1", VisaOrPermitEnum.Permit.ToString()));
                #endregion

                #region Salary Per List
                List<SelectValueResponse> salaryPerTypeList = new List<SelectValueResponse>();
                salaryPerTypeList.Add(new SelectValueResponse("0", SalaryPerTypeEnum.Hour.ToString()));
                salaryPerTypeList.Add(new SelectValueResponse("1", SalaryPerTypeEnum.Week.ToString()));
                salaryPerTypeList.Add(new SelectValueResponse("2", SalaryPerTypeEnum.Month.ToString()));
                salaryPerTypeList.Add(new SelectValueResponse("3", SalaryPerTypeEnum.Year.ToString()));
                #endregion

                #region check box yes no list
                List<SelectValueResponse> activeList = new List<SelectValueResponse>();
                activeList.Add(new SelectValueResponse("1", "Yes"));
                activeList.Add(new SelectValueResponse("0", "No"));
                #endregion

                #region RangeType List
                List<SelectValueResponse> rangeTypeList = new List<SelectValueResponse>();
                rangeTypeList.Add(new SelectValueResponse("10", "Low"));
                rangeTypeList.Add(new SelectValueResponse("20", "Medium"));
                rangeTypeList.Add(new SelectValueResponse("30", "High"));
                #endregion

                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.first_name", "First Name ", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.middle_name", "Middle Name ", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.last_name", "Last Name", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.birth_date", "Birth Date", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.gender", "Gender", FieldTypeModel.UI_SELECT, false, "", false, genderList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.martial_status", "Martial Status", FieldTypeModel.UI_SELECT, false, "", false, martialStatusList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.ethnicity", "Ethnicity", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.Prefix", "Prefix", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.Suffix", "Suffix", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.PreferredFirstname", "Preferred First Name", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.Email", "Email", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.address", "Address 1", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.address2", "Address 2", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.Address3", "Address 3", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_CITY, "r.city_id", "City", FieldTypeModel.AUTOCOMPLETE, true, "api/city?controller=autocomplete&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_STATE, "r.state_id", "State Province", FieldTypeModel.AUTOCOMPLETE, true, "api/state?controller=autocomplete&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_COUNTRY, "r.country_id", "Country", FieldTypeModel.AUTOCOMPLETE, true, "api/Countries/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.zipCode", "Postal Code", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.Active", "Active", FieldTypeModel.UI_SELECT, false, "", false, activeList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.NationalRate", "National Rate", FieldTypeModel.CURRENCY, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.employee_Id", "Employee ID", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_JOB_TITLE, "r.job_title_id", "Job Title", FieldTypeModel.AUTOCOMPLETE, true, "api/JobTitles/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.start_date", "Start Date", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.ssn", "SSN", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_COMPANY, "r.resource_company_id", "Company ", FieldTypeModel.AUTOCOMPLETE, true, "api/ResourceCompanies/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.end_date", "Last Day Employed", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.location", "Location", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.extension", "Extension", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_DROP_DOWN_CONFIGS, "r.business_unit_id", "Business Unit", FieldTypeModel.AUTOCOMPLETE, true, "api/DropDownConfigs/autocomplete?dropdowntype=businessUnit&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.office_Number", "Office Phone Number", FieldTypeModel.PHONE, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.supervisor_id", "Supervisor", FieldTypeModel.AUTOCOMPLETE, true, "api/Resources/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.time_zone_info", "Time Zone (UTC)", FieldTypeModel.UI_SELECT, false, "", false, timeZoneInfoList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_DROP_DOWN_CONFIGS, "industry.Id", "Industry", FieldTypeModel.AUTOCOMPLETE, true, "api/DropDownConfigs/autocomplete?dropdowntype=industry&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_DIVISION, "r.division_id", "Division", FieldTypeModel.AUTOCOMPLETE, true, "api/Divisions/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_DEPARTMENT, "r.department_id", "Department", FieldTypeModel.AUTOCOMPLETE, true, "api/Departments/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.HomeNumber", "Home Phone Number", FieldTypeModel.PHONE, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.mobile_phone_number", "Mobile Phone number ", FieldTypeModel.PHONE, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.emergency_contact_phone", "Emergency Contact Phone Number", FieldTypeModel.PHONE, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.emergency_contact_name", "Emergency Contact Name", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.Relationship", "Relationship", FieldTypeModel.UI_SELECT, false, "", false, relationshipList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.employment_type", "Employment Type", FieldTypeModel.UI_SELECT, false, "", false, employmentTypeList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.salary", "Salary", FieldTypeModel.CURRENCY, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.bonus_or_other_pay", "Bonus or Other Pay", FieldTypeModel.CURRENCY, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.resource_type", "Resource Type", FieldTypeModel.UI_SELECT, false, "", false, resourceTypeList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.rating", "Rating", FieldTypeModel.NUMBER, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.productivity", "Productivity", FieldTypeModel.NUMBER, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.range", "Range", FieldTypeModel.UI_SELECT, false, "", false, rangeTypeList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.NationalRate", "National Rate", FieldTypeModel.CURRENCY, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PROFESSIONAL_ORGANIZATION, "rpo.organization", "Professional & Civic Activities : Organization / Activity", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_DROP_DOWN_CONFIGS, "rpo.committee_membership_id", "Professional & Civic Activities : Committee Membership", FieldTypeModel.AUTOCOMPLETE, true, "api/DropDownConfigs/autocomplete?dropdowntype=committeeMembership&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_DROP_DOWN_CONFIGS, "rpo.committee_title_id", "Professional & Civic Activities : Committee Title", FieldTypeModel.AUTOCOMPLETE, true, "api/DropDownConfigs/autocomplete?dropdowntype=committeeTitle&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_SKILL, "rs.skill_id", "Skills  : Skill ", FieldTypeModel.AUTOCOMPLETE, true, "api/Skills/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_SKILLS, "rs.years_of_expirience", "Skills  : Years of Experience", FieldTypeModel.NUMBER, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_SKILLS, "rs.proficiency", "Skills  : Proficiency", FieldTypeModel.NUMBER, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_SKILLS, "rs.last_updated", "Skills  : Last Updated", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.grade_level", "Education : Grade Level", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.years_of_college", "Education : Years of College", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_EDUCATION, "re.subject", "Education : Degree", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_WORK_ASSIGNMENT, "re.resource_designation_id", "Education : Designation", FieldTypeModel.AUTOCOMPLETE, true, "api/DropDownConfigs/autocomplete?dropdowntype=resourceDesignation&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_EDUCATION, "re.name_of_school", "Education : Name Of School", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_EDUCATION, "re.date_obtained", "Education : Date Obtained", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_CERTIFICATE, "rc.certificate_id", "Certificates : Certificate ", FieldTypeModel.AUTOCOMPLETE, true, "api/Certificates/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_CERTIFICATE, "rc.certificate_state_id", "Certificates : State ", FieldTypeModel.AUTOCOMPLETE, true, "api/state?controller=autocomplete&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_CERTITIFICATES, "rc.issue_date", "Certificates : Issue Date", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_CERTITIFICATES, "rc.expirationDate", "Certificates : Expiration Date", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_LANGUAGES, "rsl.language_id", "Languages : Language", FieldTypeModel.AUTOCOMPLETE, true, "api/Languages/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_SPOKEN_LANGUAGES, "rsl.years_of_expirience", "Languages : Years of Experience", FieldTypeModel.NUMBER, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_SPOKEN_LANGUAGES, "rsl.speaking_proficiency", "Languages : Speaking Proficiency", FieldTypeModel.NUMBER, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_SPOKEN_LANGUAGES, "rsl.writing_proficiency", "Languages : Writing Proficiency", FieldTypeModel.NUMBER, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.years_employed", "Years Employed ", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.available_to_travel", "Travel Details : Available to Travel ", FieldTypeModel.UI_SELECT, false, "", false, activeList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.valid_passport", "Travel Details : Valid Passport ", FieldTypeModel.UI_SELECT, false, "", false, activeList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_TRAVELS, "rt.visa_or_permit", "Travel Details : Visa or Work Permit", FieldTypeModel.UI_SELECT, false, "", false, visaOrPermitList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_COUNTRY, "rt.country_id", "Travel Details : Country", FieldTypeModel.AUTOCOMPLETE, true, "api/Countries/autocomplete?dropdowntype=country&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_TRAVELS, "rt.time_remaining_in_days", "Travel Details : Remaining Days", FieldTypeModel.NUMBER, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_TRAVELS, "rt.special_considerations", "Travel Details : Special Consideration", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.armed_forces_country_id", "Background Check : Armed Forces Country", FieldTypeModel.AUTOCOMPLETE, true, "api/Countries/autocomplete?dropdowntype=country&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.armed_forces_branch", "Background Check : Armed Forces Branch", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE, "r.armed_forces", "Background Check : Active Duty", FieldTypeModel.UI_SELECT, false, "", false, activeList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_CRIMINAL_RECORD, "rcr.date_of_offense", "Criminal Records : Date of Offense ", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_CRIMINAL_RECORD, "rcr.location_of_offense", "Criminal Records : Location of Offense ", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_CRIMINAL_RECORD, "rcr.offense", "Criminal Records : Offense ", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_CRIMINAL_RECORD, "rcr.penalty_or_disposition", "Criminal Records : Penalty or Disposition", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.name", "Previous Employment : Company Name ", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.address", "Previous Employment : Address", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.title_of_position", "Previous Employment : Title of Position", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.duties_of_position", "Previous Employment : Duties of Position", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.reason_for_leaving", "Previous Employment : Reason For Leaving", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.start_date", "Previous Employment : Start Date ", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.number_of_supervised", "Previous Employment : Number of Supervised", FieldTypeModel.NUMBER, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.salary", "Previous Employment : Salary", FieldTypeModel.CURRENCY, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.salary_per", "Previous Employment : Salary Per", FieldTypeModel.UI_SELECT, false, "", false, salaryPerTypeList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.hours_per_week", "Previous Employment : Hours per Week", FieldTypeModel.CURRENCY, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_PREVIOUS_EMPLOYMENT, "rpe.end_date", "Previous Employment : End Date ", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_JOB_TITLE, "rwe.job_title_id", "Work Experience : Position", FieldTypeModel.AUTOCOMPLETE, true, "api/JobTitles/autocomplete?search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_WORK_EXPERIENCE, "rwe.StartDate", "Work Experience : Start Date", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_WORK_ASSIGNMENT, "rwa.AssignmentDescription", "Work Experience : Assignment -> Assignment", FieldTypeModel.TEXT, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_WORK_ASSIGNMENT, "rwa.StartDate", "Work Experience : Assignment -> Start Date", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_WORK_ASSIGNMENT, "rwa.EndDate", "Work Experience : Assignment -> End Date", FieldTypeModel.DATE_TIME, false, "", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_WORK_ASSIGNMENT, "rwa.assignment_industry_id", "Work Experience : Assignment -> Industry", FieldTypeModel.AUTOCOMPLETE, true, "api/DropDownConfigs/autocomplete?dropdowntype=industry&search=", false, new SelectValueResponse[0]));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_WORK_ASSIGNMENT, "rwa.of_note", "Work Experience : Assignment -> Of Note", FieldTypeModel.UI_SELECT, false, "", false, activeList.ToArray()));
                list.Add(new FieldsListResponse(TableNameModel.TABLE_RESOURCE_WORK_ASSIGNMENT, "rwa.judicial_clerk_ship", "Work Experience : Assignment -> Judicial Clerkship", FieldTypeModel.UI_SELECT, false, "", false, activeList.ToArray()));

            }
            #endregion

            return list;
        }
    }
}
