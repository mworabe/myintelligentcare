﻿using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceRecognitions
{
    [DataContract]
    public class ResourceRecognitionResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public PicklistResponse Resource { get; set; }

        [DataMember]
        public string Recognition { get; set; }
    }
}