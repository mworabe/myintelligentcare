﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("elastic-search", {
                url: "/search?:filter",
                templateUrl: "Client/app/controllers/Elastic-Search/elastic-search-view.html",
                controller: "ElasticSearchController as ElasticSearchController",
                data: {
                    security: {
                        authenticated: false
                    }
                }
            });
    });