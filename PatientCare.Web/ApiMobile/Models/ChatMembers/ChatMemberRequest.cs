﻿namespace PatientCare.Web.ApiMobile.Models.ChatMembers
{
    public class ChatMemberRequest
    {
        public long Id { get; set; }

        public long? ChatSessionId { get; set; }

        public string UserId { get; set; }

    }
}