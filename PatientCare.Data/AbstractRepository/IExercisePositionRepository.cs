﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IExercisePositionRepository : IRepository<ExercisePosition>
    {
    }
}
