﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("edzTabLink", [function () {
        return {
            templateUrl: "Client/app/components/directives/tabs/tabLink/tabLink.html",
            restrict: "E",
            replace: false,
            transclude: true,
            require: ["^edzTabLinks", "?^form"],
            scope: {
                tabId: "@edzTabId",
                onClick: "=?edzTabClick"
            },
            link: function ($scope, iElement, iAttrs, controller, $location, $anchorScroll) {
                $scope.validTab = true;
                $scope.onClick = $scope.onClick || angular.noop;
                var form = controller[1];

                function validationTab() {

                    if (!form.$submitted) {
                        return;
                    }

                    if (!form.$invalid) {
                        $scope.validTab = true;
                        return;
                    }
                    _.forIn(form.$error, function (arr) {

                        var valid = _.findIndex(arr, function (item) {
                            return item.$options.tabValidationName === $scope.tabId;
                        }) === -1;
                        $scope.validTab = valid;
                        return valid;
                    });
                }

                $scope.$watch(function () {
                    return form.$invalid && form.$submitted;
                }, function () {
                    validationTab();
                });
                $scope.$on("edz-tab:validation", function () {
                    validationTab();
                });
                $scope.onTabLink_Click = function () {
                    var result = $scope.onClick();
                    if (result === false)
                        return;
                    controller[0].currentTab = $scope.tabId;
                    $("html, body").animate({
                        scrollTop: 0
                    }, 200);
                }
                $scope.isActive = function () {
                    return controller[0].currentTab === $scope.tabId;
                }
            }
        };
    }
    ]);