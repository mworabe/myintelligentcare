"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("FavoriteTemplatesAddEditCntrl",
    ["$scope", 'ExerciseService', '$state', 'FavoriteTemplateService', 'toaster', '$stateParams', 'dataObject', '$timeout',
    function ($scope, exerciseService, $state, FavoriteTemplateService, toaster, $stateParams, dataObject, $timeout) {
        self = this;
        self.selectedPan = 1;
        
        self.model = {
            templateExercises:[]
        }

        var modelId = $stateParams.id || undefined;
        self.title = "Favorite Template";

       

        self.onChooseBackClick = function () {
            $state.go("favorite-templates");
        }

        self.onChooseSaveClick = function (selectedItems) {
            if (selectedItems && selectedItems.length > 0) {
                self.selectedPan = 2;
                self.model.templateExercises = [];
                for (var i = 0; i < selectedItems.length; i++) {
                    var exerciseItem = selectedItems[i];
                    var obj = {
                        exerciseId: exerciseItem.id,
                        exerciseOrder: i,
                        exercise: exerciseItem

                    }
                    self.model.templateExercises.push(obj);
                }

                self.onSave();

            } else {
                toaster.pop("error", "", "Please select at least one exercise item.");
            }

        }

        var onDeleteItem = function (item) {
            var index = -1;
            for (var i = 0; i < self.model.templateExercises.length; i++) {
                var obj = self.model.templateExercises[i];
                if (obj.exercise.id = item.id) {
                    index = i;
                    break;
                }
                
            }
            if (index > -1) {
                self.model.templateExercises.splice(index, 1);
            }

            
        }

        self.imageItemConfig = {
            hasDeleteBtn: true,
            deleteBtnClick: onDeleteItem
        }

        self.chooseScreenConfig = {
            onBackBtn: self.onChooseBackClick,
            onSaveBtn: self.onChooseSaveClick,
            showCopyFromMaster: true,
            
        };

        self.moveUp = function (item) {
            if (item.exerciseOrder >= 0) {
                var currentIndex=item.exerciseOrder ;
                var newIndex = item.exerciseOrder - 1;
                for (var i = 0; i < self.model.templateExercises.length; i++) {
                    var obj=self.model.templateExercises[i];
                    if (obj.exerciseOrder == newIndex) {
                        obj.exerciseOrder = currentIndex;
                        break;
                    }
                }
                item.exerciseOrder = newIndex;
            }
        }
        self.moveDown = function (item) {
            if (item.exerciseOrder < (self.model.templateExercises.length - 1)) {
                var currentIndex = item.exerciseOrder;
                var newIndex = item.exerciseOrder + 1;
                for (var i = 0; i < self.model.templateExercises.length; i++) {
                    var obj = self.model.templateExercises[i];
                    if (obj.exerciseOrder == newIndex) {
                        obj.exerciseOrder = currentIndex;
                        break;
                    }
                }
                item.exerciseOrder = newIndex;
            }
        }

        self.onBack = function () {
            
            var items = [];
            if (self.model.templateExercises) {
                
                for (var i = 0; i < self.model.templateExercises.length; i++) {
                    var obj = self.model.templateExercises[i];
                    items.push(obj.exercise);
                }
            }
            
            self.chooseScreenConfig.directiveScope.selectedExerciseItems = [];
            self.chooseScreenConfig.directiveScope.selectedExerciseItems = items;
            self.selectedPan = 1;
        }

        if (modelId && dataObject) {
            self.model = dataObject[0];
            self.title = "Edit Favorite Template";
            //self.selectedPan = 2;

            $timeout(function () {
                self.onBack();
            }, 1000);
            

        }

        function getRequest() {
            return self.model;
        }

        function validate(form) {

            if (!self.model.name) {
                toaster.pop("error", "", "Template name is required.");
                return false;
            }

            if (!(self.model.templateExercises && self.model.templateExercises.length > 0)) {
                toaster.pop("error", "", "Please select at least one exercise.");
                return false;
            }

            return true;
        }

        self.onSave = function (form) {
            if (validate()) {
                if (modelId) {
                    self.isLoading = FavoriteTemplateService.update({ id: modelId }, getRequest(), onSuccess, onError);
                } else {
                    self.isLoading = FavoriteTemplateService.save(getRequest(), onSuccess, onError);
                }
            }
        }
        function onSuccess(response) {
            if (response && response.id) {
                toaster.pop("success", "", "Exercise Template Saved Successfully.");
                $state.go("favorite-templates");
            }
        }
        function onError(Error) {

        }


    }
    ]);