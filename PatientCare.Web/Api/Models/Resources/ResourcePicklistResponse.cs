﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.Resources
{
    [DataContract]
    public class ResourcePicklistResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string EmployeeId { get; set; }

        [DataMember]
        public PicklistResponse Division { get; set; }

        [DataMember]
        public PicklistResponse Department { get; set; }

        [DataMember]
        public PicklistResponse JobTitle { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public int? YearsEmployed { get; set; }

        [DataMember]
        public string PhotoFileNameUrl { get; set; }

        [DataMember]
        public string PhotoFileName { get; set; }

        [DataMember]
        public decimal HourlyRate { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }
}