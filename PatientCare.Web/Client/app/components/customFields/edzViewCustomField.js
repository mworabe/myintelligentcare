﻿"use strict";

(function edzViewCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzViewCustomField", [
            "edzAddCustomFieldFactory",
            "uuid2",
            edzViewCustomField
        ]);

    function edzViewCustomField(edzAddCustomFieldFactory, uuid2) {
        return {
            templateUrl: "Client/app/components/customFields/edzViewCustomField.html",
            restrict: "E",
            link: function (scope, element) {
                var vm = {
                    customField: scope.customField,
                    value: scope.value
                };
                var setNewElementHtml = _.flowRight(
                                setFieldHtml,
                                _.curry(getCustomFieldEditHtml)(scope),
                                function () {
                                    return vm.customField;
                                }
                            );

                vm.customField = edzAddCustomFieldFactory(vm.customField.type);

                setNewElementHtml();

                function getCustomFieldEditHtml(scope, field) {
                    var scopeNew = scope.$new();
                    scopeNew.value = scope.value;
                    scopeNew.fieldId = uuid2.newguid();
                    scopeNew.option = scope.customField.customFieldProperties;
                    scopeNew.require = scope.customField.require;
                    scopeNew.info = scope.info;
                    scope.$watch(function() {
                        return scopeNew.value;
                    }, function() {
                        scope.value = scopeNew.value;
                    });
                    return field.getHtmlForCreating(scopeNew);
                }

                function setFieldHtml(htmlToSet) {
                    element.replaceWith(htmlToSet);
                }
            },
            scope: {
                customField: "=edzCustomField",
                value: "=edzValue",
                info: "=?edzInfo"
            }
        };
    }
})();