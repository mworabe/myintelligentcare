﻿using System.Data.Entity;
using System.Linq;
using PatientCare.Data.AbstractRepository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PatientCare.Data.EntityFramework.Identity
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {

        public ApplicationRoleManager(IRoleStore<ApplicationRole, string> roleStore)
            : base(roleStore)
        {
        }

        protected virtual IQueryable<ApplicationRole> CreatePagedQuery(PagedQuery<ApplicationRole> query, ReadOptions<ApplicationRole> readOptions = null)
        {
            var entityQuery = Roles.AsQueryable();
            if (query != null)
            {
                if (readOptions != null)
                {
                    entityQuery = entityQuery.AttachIncludePredicates(readOptions.IncludePredicates);
                    if (readOptions.ReadOnly)
                    {
                        entityQuery = entityQuery.AsNoTracking();
                    }
                }
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions)
                    .ApplyOrderExpressions(query.OrderByExpressions, query.SortOrder);

                entityQuery = entityQuery.Skip(query.ItemsToSkip);
                entityQuery = entityQuery.Take(query.ItemsToGet);
            }
            return entityQuery;
        }
        protected long PagedCount(PagedQuery<ApplicationRole> query)
        {
            var entityQuery = Roles.AsQueryable();
            if (query != null)
            {
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions);
            }
            return entityQuery.LongCount();
        }


        public PagedResult<ApplicationRole> PagedList(PagedQuery<ApplicationRole> query, ReadOptions<ApplicationRole> readOptions = null)
        {
            var entities = CreatePagedQuery(query, readOptions).ToArray();
            var totalCount = PagedCount(query);

            return new PagedResult<ApplicationRole>
            {
                Entities = entities,
                TotalCount = totalCount
            };
        }
    }
}
