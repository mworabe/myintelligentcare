﻿namespace PatientCare.Core.Services.RoleSetupService
{
    public enum ConfigurationSection
    {
        GeneralConfigurationData = 0,
        ResourceConfigurationData = 1,
        PatientConfigurationData = 2,
    }
}
