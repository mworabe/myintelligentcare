﻿using System.Threading.Tasks;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Services
{
    public interface IConfigurationDataService
    {
        #region InjuryRegion

        Task<bool> UpdateInjuryRegionAsync(InjuryRegion injuryregion);
        Task<bool> CreateInjuryRegionAsync(InjuryRegion injuryregion);
        Task<InjuryRegion> ReadInjuryRegionAsync(long id);
        Task<bool> DeleteInjuryRegionAsync(long id);

        #endregion

        #region Diagnosis

        Task<bool> UpdateDiagnosisAsync(Diagnosis diagnosis);
        Task<bool> CreateDiagnosisAsync(Diagnosis diagnosis);
        Task<Diagnosis> ReadDiagnosisAsync(long id);
        Task<bool> DeleteDiagnosisAsync(long id);

        #endregion

        #region Exercise Position

        Task<bool> UpdateExercisePositionAsync(ExercisePosition exerciseposition);
        Task<bool> CreateExercisePositionAsync(ExercisePosition exerciseposition);
        Task<ExercisePosition> ReadExercisePositionAsync(long id);
        Task<bool> DeleteExercisePositionAsync(long id);

        #endregion

        #region Exercise Activity

        Task<bool> UpdateExerciseActivityAsync(ExerciseActivity exerciseactivity);
        Task<bool> CreateExerciseActivityAsync(ExerciseActivity exerciseactivity);
        Task<ExerciseActivity> ReadExerciseActivityAsync(long id);
        Task<bool> DeleteExerciseActivityAsync(long id);

        #endregion


    }
}
