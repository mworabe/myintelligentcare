﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Data;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using Microsoft.AspNet.Identity;
using PatientCare.Core.Entities;
using PatientCare.Core.Enums;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.CustomFields;
using PatientCare.Web.Api.Models.CustomFieldProperties;
using PatientCare.Web.Api.Models.DropDownConfigs;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.Resources;
using PatientCare.Web.Api.Models.ResourceBehaviouralDetails;
using PatientCare.Web.Api.Models.ResourceRecognitions;
using PatientCare.Web.Api.Models.ResourceIndustry;
using PatientCare.Web.Api.Models.ResourcePracticeAreas;
using PatientCare.Web.Api.Models.ResourceSpokenLanguages;
using PatientCare.Web.Api.Models.ResourceEducations;
using PatientCare.Web.Api.Models.ResourceTravels;
using PatientCare.Web.Api.Models.ResourceCertificates;
using PatientCare.Web.Api.Models.ResourceSkills;
using PatientCare.Web.Api.Models.ResourceCriminalRecords;
using PatientCare.Web.Api.Models.ResourcePreviousEmployments;
using PatientCare.Web.Api.Models.ResourceWorkExperience;
using PatientCare.Web.Api.Models.ResourceWorkAssignment;
using PatientCare.Web.Api.Models.ResourceProfessionalOrganizations;
using PatientCare.Web.Api.Models.ResourceInsurances;
using PatientCare.Web.Api.Models.ResourceTimeoffBenefits;
using PatientCare.Web.Services.Excel;
using PatientCare.Web.Services.PDFUtils;
using PatientCare.Web.Services.FileStorage;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Api.Models.Account;
using PatientCare.Web.Api.Models.Clinics;
using PatientCare.Web.Api.Models.ClinicLocations;
using PatientCare.Web.Api.Models.PostalCodes;
using PatientCare.Web.Api.Models.Cities;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resources operations.
    /// </summary>
    //[Authorize]
    [System.Web.Mvc.SessionState(System.Web.SessionState.SessionStateBehavior.Disabled)]
    public sealed class ResourcesController : ApiBaseController
    {
        public static byte[] userResumeData;

        /// <summary>
        /// Description : Resources IResourceService.
        /// </summary>
        private readonly IResourceService _resourceService;
        /// <summary>
        /// Description : Resources IResourceRepository.
        /// </summary>
        private readonly IResourceRepository _resourceRepository;
        /// <summary>
        /// Description : Resources IPicklistRepository Division.
        /// </summary>
        private readonly IPicklistRepository<Division> _divisionRepository;
        /// <summary>
        /// Description : Resources IPicklistRepository Department.
        /// </summary>
        private readonly IPicklistRepository<Department> _departmentRepository;
        /// <summary>
        /// Description : Resources IPicklistRepository Country.
        /// </summary>
        private readonly IPicklistRepository<Country> _countryRepository;
        /// <summary>
        /// Description : Resources IPicklistRepository Resource Company.
        /// </summary>
        private readonly IPicklistRepository<ResourceCompany> _resourceCompanyRepository;
        /// <summary>
        /// Description : Resources IPicklistRepository Job Title.
        /// </summary>
        private readonly IPicklistRepository<JobTitle> _jobTitleRepository;
        /// <summary>
        /// Description : Resources IPicklistRepository City.
        /// </summary>
        private readonly IPicklistRepository<City> _cityRepository;
        /// <summary>
        /// Description : Resources IPicklistRepository State.
        /// </summary>
        private readonly IPicklistRepository<State> _stateRepository;
        /// <summary>
        /// Description : Resources IResourceTeamRepository.
        /// </summary>
        private readonly IResourceTeamRepository _resourceTeamRepository;
        /// <summary>
        /// Description : Resources IResourceCustomFieldValueRepository.
        /// </summary>
        private readonly IResourceCustomFieldValueRepository _customFieldValueRepository;
        /// <summary>
        /// Description : Resources IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : Resources IResourceSkillRepository.
        /// </summary>
        private readonly IResourceSkillRepository _resourceSkillRepository;

        ReadOptions<Resource> _readOptions = new ReadOptions<Resource>()
                .WithIncludePredicate(x => x.Country)
                .WithIncludePredicate(x => x.Division)
                .WithIncludePredicate(x => x.Department)
                .WithIncludePredicate(x => x.JobTitle)
                .WithIncludePredicate(x => x.ArmedForcesCountry)
                .WithIncludePredicate(x => x.ResourceTeam);

        /// <summary>
        /// Description : Initializes a new instance of the Resources Class.     
        /// </summary>
        /// <param name="resourceService">IResource Service</param>
        /// <param name="resourceRepository">IResource Repository</param>
        /// <param name="divisionRepository">IDivision Repository</param>
        /// <param name="departmentRepository">IDepartment Repository</param>
        /// <param name="countryRepository">ICountry Repository</param>
        /// <param name="resourceCompanyRepository">IResource Company Repository</param>
        /// <param name="jobTitleRepository">IJob Title Repository</param>
        /// <param name="cityRepository">ICity Repository</param>
        /// <param name="stateRepository">IState Repository</param>
        /// <param name="resourceTeamRepository">IResource Team Repository</param>
        /// <param name="customFieldValueRepository">ICustom Field Value Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        /// <param name="resourceSkillRepository">IResource Skill Repository</param>
        public ResourcesController(IResourceService resourceService,
            IResourceRepository resourceRepository,
            IPicklistRepository<Division> divisionRepository,
            IPicklistRepository<Department> departmentRepository,
            IPicklistRepository<Country> countryRepository,
            IPicklistRepository<ResourceCompany> resourceCompanyRepository,
            IPicklistRepository<JobTitle> jobTitleRepository,
            IPicklistRepository<City> cityRepository,
            IPicklistRepository<State> stateRepository,
            IResourceTeamRepository resourceTeamRepository,
            IResourceCustomFieldValueRepository customFieldValueRepository,
            IDataContextScopeFactory dataContextScopeFactory,
            IResourceSkillRepository resourceSkillRepository
            )
        {
            _resourceService = resourceService;
            _resourceRepository = resourceRepository;
            _divisionRepository = divisionRepository;
            _departmentRepository = departmentRepository;
            _countryRepository = countryRepository;
            _resourceCompanyRepository = resourceCompanyRepository;
            _jobTitleRepository = jobTitleRepository;
            _cityRepository = cityRepository;
            _stateRepository = stateRepository;
            _resourceTeamRepository = resourceTeamRepository;
            _customFieldValueRepository = customFieldValueRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
            _resourceSkillRepository = resourceSkillRepository;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Resources/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Resource.
        /// </summary>
        /// <param name="request">Resource List Request Model</param>
        /// <returns>Resource Main Picklist Response Model</returns>
        [Authorize]
        [HttpGet]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ResourceMainPicklistResponse>))]
        public IHttpActionResult GetResourcesNew([FromUri]ResourceListRequest request,
                string tableName = null, string fieldName = null, string fieldValue = null)
        {
            var roleInfo = GetRoleAccessRights();
            var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }

            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                ModelState.AddModelError("Resources", "your session is expired. please login again");
                return BadRequest(ModelState);
            }
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user == null && user.Resource == null)
            {
                ModelState.AddModelError("Resources", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }

            if ((tableName != null || tableName != null) && (fieldName != null || fieldName != null) && (fieldValue != null || fieldValue != null))
                request.ShowInactive = true;

            request.SearchPhrase = (request.SearchPhrase != null) ? request.SearchPhrase.Replace(",", "") : request.SearchPhrase;

            var lst = new ListResponse<ResourceMainPicklistResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                string dataQuery = GetResourceListQueryData(roleInfo, request, user, tableName, fieldName, fieldValue);
                string countQuery = GetResourceListQueryDataCount(roleInfo, request, user, tableName, fieldName, fieldValue);

                var queryData = ctx.Database.SqlQuery<ResourceMainPicklistResponse>(dataQuery);
                var queryCount = ctx.Database.SqlQuery<long>(countQuery);

                lst.Data = queryData.ToArray();
                lst.ItemsCount = queryCount.FirstOrDefault();//query.Select(x => x.TotalDataCount).FirstOrDefault();

                lst = new ListResponse<ResourceMainPicklistResponse>(lst.Data, lst.ItemsCount);

                foreach (var item in lst.Data)
                {
                    item.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                }
            }
            return Ok(lst);
        }

        private string GetResourceListQueryData(RoleAccessRights roleInfo, ResourceListRequest request,
            ApplicationUser user,
                string tableName = null, string fieldName = null, string fieldValue = null)
        {
            StringBuilder sb = new StringBuilder();

            #region Base Query
            sb.Append("WITH TempResult AS(");
            sb.Append("\r\n");
            sb.Append("select distinct res.Id as Id,(res.last_name+' '+res.first_name)as Name,res.first_name as FirstName,res.last_name as LastName,");
            sb.Append("\r\n");
            sb.Append("	res.employee_id as EmployeeId,res.department_id as DepartmentId, dept.Name as DepartmentName,");
            sb.Append("\r\n");
            sb.Append("	res.country_id as CountryId, country.name as CountryName,");
            sb.Append("\r\n");
            sb.Append("	res.job_title_id as JobTitleId, jobTitle.name as JobTitleName, ");
            sb.Append("\r\n");
            sb.Append("	res.location as Location ,res.years_employed as YearsEmployed,res.[photo_file_name] as PhotoFileName,");
            sb.Append("\r\n");
            sb.Append("	'' as PhotoFileNameUrl,res.hourly_rate as HourlyRate,res.Active as Active,res.rating as Rating,res.gender as Gender, ");
            sb.Append("\r\n");
            sb.Append("	res.state_id as StateId,res.city_id as CityId,");
            sb.Append("\r\n");
            sb.Append("	resourceDivision.name as DivisionName,res.division_id as DivisionId, ");
            sb.Append("\r\n");
            sb.Append("	res.clinic_id as ClinicId,clinic.name as ClinicName,Clinic.clinic_prefix as ClinicPrefix, ");
            sb.Append("\r\n");
            sb.Append("	res.clinic_location_id as ClinicLocationId,clinicLocation.name as ClinicLocationName,clinicLocation.location_prefix as LocationPrefix ");
            sb.Append("\r\n");
            sb.Append("\r\n");
            sb.Append("from resources as res");
            sb.Append("\r\n");
            sb.Append("	left outer join departments dept on res.department_id = dept.Id");
            sb.Append("\r\n");
            sb.Append("	left outer join countries country on res.country_id = country.Id");
            sb.Append("\r\n");
            sb.Append("	left outer join job_titles jobTitle on res.job_title_id = jobTitle.Id	");
            sb.Append("\r\n");
            sb.Append("	left outer join resource_spoken_languages spokenLanguage on res.id = spokenLanguage.resource_id	");
            sb.Append("\r\n");
            sb.Append("	left outer join resource_educations education on res.id = education.resource_id	");
            sb.Append("\r\n");
            sb.Append("	left Outer Join divisions as resourceDivision on res.division_id = resourceDivision.Id ");
            sb.Append("\r\n");
            sb.Append("	Left outer join clinics Clinic on res.clinic_id = Clinic.id	");
            sb.Append("\r\n");
            sb.Append("	left Outer Join clinic_locations as clinicLocation on res.clinic_location_id = clinicLocation.Id ");
            sb.Append("\r\n");
            sb.Append("where 1 = 1 ");
            sb.Append("\r\n");

            if (request.ShowInactive == false && request.SearchPhrase == null)
            {
                sb.AppendFormat("and res.Active ={0}", 1);// 1 for true
                sb.Append("\r\n");
            }
            #endregion

            #region For Resource AccessRights
            if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
            {
                if (user != null && user.Resource != null)
                {
                    if (roleInfo.AccessRights.IsForDepartmentOnly && user.Resource.DepartmentId != null)
                    {
                        sb.AppendFormat("and res.department_id ={0}", user.Resource.DepartmentId);
                        sb.Append("\r\n");
                    }
                    if (roleInfo.AccessRights.IsForDivisionOnly && user.Resource.DivisionId != null)
                    {
                        sb.AppendFormat("and res.division_id ={0}", user.Resource.DivisionId);
                        sb.Append("\r\n");
                    }
                }
            }
            #endregion

            #region For DashBoard Redirect
            if ((tableName != null || tableName != null) && (fieldName != null || fieldName != null) && (fieldValue != null || fieldValue != null))
            {
                if (fieldName == "ResourceCountry")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and res.country_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesDivision")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.division_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesDepartment")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.department_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourceExperienceLevel")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.years_employed = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesJobTitle")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and   res.job_title_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesLanguage")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  spokenLanguage.language_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "EducationNameOfSchool")
                {
                    sb.AppendFormat(" and  education.name_of_school like '%{0}%'", fieldValue);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourceGender")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.gender = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesState")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.state_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesCity")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.city_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourceLocation")
                {
                    sb.AppendFormat(" and  res.location like '%{0}%'", fieldValue);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourceRating")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.rating = {0}", fieldValue);
                    sb.Append("\r\n");
                }
                if (fieldName == "ExternalResourceVsCompany")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.resource_company_id = {0} and res.resource_type = 1 ", id);//and res.Active = 1
                    sb.Append("\r\n");
                }
            }
            #endregion

            #region For the Searching
            if (request.SearchPhrase != null)
            {
                sb.AppendFormat(" and ((res.last_name + N' ' + res.first_name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (jobTitle.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (resourceDivision.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (res.employee_id LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (( CAST(CHARINDEX(CASE WHEN (N'active' = '{0}') THEN N'true' WHEN (N'inactive' = '{0}') THEN N'false' ELSE N'' END, CASE WHEN (res.Active = 1) THEN N'True' WHEN (res.Active = 0) THEN N'False' ELSE N'' END) AS int)) > 0) ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (dept.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (country.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (Clinic.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (clinicLocation.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (CASE WHEN (res.years_employed IS NULL) THEN N'' ELSE  CAST( res.years_employed AS nvarchar(max)) END LIKE '%{0}%' ESCAPE N'~')", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (res.location LIKE '%{0}%' ESCAPE N'~'))", request.SearchPhrase);
                sb.Append("\r\n");
            }
            #endregion

            #region For Total records count
            sb.Append(") ");
            sb.Append("\r\n");
            // sb.Append("TempCount AS(SELECT convert(bigint,COUNT(*)) as TotalDataCount FROM TempResult)");
            sb.Append("\r\n");
            sb.Append("SELECT * FROM TempResult");
            sb.Append("\r\n");
            #endregion

            #region for the Sorting and Pagging

            if (string.IsNullOrEmpty(request.SortField))
                sb.Append("ORDER BY LastName ");
            else
                sb.Append("ORDER BY " + request.SortField);

            if (string.IsNullOrEmpty(request.SortField))
                sb.Append(" ASC");
            else if (request.SortOrder.Value.ToString() == "Descending")
                sb.Append(" DESC");
            else
                sb.Append(" ASC");

            #endregion

            #region For Pagging By Pageindex and Pagesize

            sb.Append("\r\n");
            sb.AppendFormat(" OFFSET({0} * {1}) ROWS FETCH NEXT {1} ROWS ONLY", (request.PageIndex - 1), request.PageSize);
            sb.Append("\r\n");

            #endregion

            return sb.ToString();
        }

        private string GetResourceListQueryDataCount(RoleAccessRights roleInfo, ResourceListRequest request,
            ApplicationUser user,
            string tableName = null, string fieldName = null, string fieldValue = null)
        {
            StringBuilder sb = new StringBuilder();

            #region Base Query
            sb.Append("select convert(bigint,count(distinct res.Id)) as TotalDataCount");
            sb.Append("\r\n");
            sb.Append("from resources as res");
            sb.Append("\r\n");
            sb.Append("	left outer join departments dept on res.department_id = dept.Id");
            sb.Append("\r\n");
            sb.Append("	left outer join countries country on res.country_id = country.Id");
            sb.Append("\r\n");
            sb.Append("	left outer join job_titles jobTitle on res.job_title_id = jobTitle.Id	");
            sb.Append("\r\n");
            sb.Append("	left outer join resource_spoken_languages spokenLanguage on res.id = spokenLanguage.resource_id	");
            sb.Append("\r\n");
            sb.Append("	left outer join resource_educations education on res.id = education.resource_id	");
            sb.Append("\r\n");
            sb.Append("	left Outer Join divisions as resourceDivision on res.division_id = resourceDivision.Id ");
            sb.Append("\r\n");
            sb.Append("	Left outer join clinics Clinic on res.clinic_id = Clinic.id	");
            sb.Append("\r\n");
            sb.Append("	left Outer Join clinic_locations as clinicLocation on res.clinic_location_id = clinicLocation.Id ");
            sb.Append("\r\n");
            sb.Append("where 1 = 1 ");
            sb.Append("\r\n");

            if (request.ShowInactive == false && request.SearchPhrase == null)
            {
                sb.AppendFormat(" and res.Active ={0}", 1);// 1 for true
                sb.Append("\r\n");
            }
            #endregion

            #region For Resource AccessRights
            if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
            {
                if (user != null && user.Resource != null)
                {
                    if (roleInfo.AccessRights.IsForDepartmentOnly && user.Resource.DepartmentId != null)
                    {
                        sb.AppendFormat(" and  res.department_id ={0}", user.Resource.DepartmentId);
                        sb.Append("\r\n");
                    }
                    if (roleInfo.AccessRights.IsForDivisionOnly && user.Resource.DivisionId != null)
                    {
                        sb.AppendFormat(" and res.division_id ={0}", user.Resource.DivisionId);
                        sb.Append("\r\n");
                    }
                }
            }
            #endregion

            #region For DashBoard Redirect
            if ((tableName != null || tableName != null) && (fieldName != null || fieldName != null) && (fieldValue != null || fieldValue != null))
            {
                if (fieldName == "ResourceCountry")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.country_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesDivision")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.division_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesDepartment")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.department_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourceExperienceLevel")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and  res.years_employed = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesJobTitle")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and res.job_title_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesLanguage")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and spokenLanguage.language_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "EducationNameOfSchool")
                {
                    sb.AppendFormat(" and education.name_of_school like '%{0}%'", fieldValue);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourceGender")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and res.gender = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesState")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and res.state_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourcesCity")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and res.city_id = {0}", id);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourceLocation")
                {
                    sb.AppendFormat(" and res.location like '%{0}%'", fieldValue);
                    sb.Append("\r\n");
                }
                if (fieldName == "ResourceRating")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and res.rating = {0}", fieldValue);
                    sb.Append("\r\n");
                }
                if (fieldName == "ExternalResourceVsCompany")
                {
                    long id = Convert.ToInt64(fieldValue);
                    sb.AppendFormat(" and res.resource_company_id = {0} and res.resource_type = 1", id); //and res.Active = 1
                    sb.Append("\r\n");
                }
            }
            #endregion

            #region For the Searching
            if (request.SearchPhrase != null)
            {
                sb.AppendFormat(" and ((res.last_name + N' ' + res.first_name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (jobTitle.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (resourceDivision.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (res.employee_id LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (( CAST(CHARINDEX(CASE WHEN (N'active' = '{0}') THEN N'true' WHEN (N'inactive' = '{0}') THEN N'false' ELSE N'' END, CASE WHEN (res.Active = 1) THEN N'True' WHEN (res.Active = 0) THEN N'False' ELSE N'' END) AS int)) > 0) ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (dept.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (country.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (Clinic.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (clinicLocation.name LIKE '%{0}%' ESCAPE N'~') ", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (CASE WHEN (res.years_employed IS NULL) THEN N'' ELSE  CAST( res.years_employed AS nvarchar(max)) END LIKE '%{0}%' ESCAPE N'~')", request.SearchPhrase);
                sb.Append("\r\n");
                sb.AppendFormat("	OR (res.location LIKE '%{0}%' ESCAPE N'~'))", request.SearchPhrase);
                sb.Append("\r\n");
            }
            #endregion

            return sb.ToString();
        }

        /// <summary>
        /// Method Description : Using for Get Resource by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Resource Response Model</returns>
        [Authorize]
        [HttpGet]
        public async Task<IHttpActionResult> GetResource(long id)
        {
            var roleInfo = GetRoleAccessRights();
            var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }

            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                ModelState.AddModelError("Resources", "your session is expired. please login again");
                return BadRequest(ModelState);
            }
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user == null && user.Resource == null)
            {
                ModelState.AddModelError("Resources", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }

            ResourceResponse newresource = new ResourceResponse();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                #region Query
                var query = (from res in ctx.Resources.AsNoTracking()
                             join db_city in ctx.Cities.AsNoTracking() on res.CityId equals db_city.Id into city
                             join db_country in ctx.Countries.AsNoTracking() on res.CountryId equals db_country.Id into country
                             join db_state in ctx.States.AsNoTracking() on res.StateId equals db_state.Id into state
                             join db_jobTitle in ctx.Positions.AsNoTracking() on res.JobTitleId equals db_jobTitle.Id into jobTitle
                             join db_division in ctx.Divisions.AsNoTracking() on res.DivisionId equals db_division.Id into division
                             join db_department in ctx.Departments.AsNoTracking() on res.DepartmentId equals db_department.Id into department
                             join db_clinic in ctx.Clinics.AsNoTracking() on res.ClinicId equals db_clinic.Id into clinic
                             join db_cliniclocation in ctx.ClinicLocations.AsNoTracking() on res.ClinicLocationId equals db_cliniclocation.Id into clinicLocation
                             join db_armedForcesCountry in ctx.Countries.AsNoTracking() on res.ArmedForcesCountryId equals db_armedForcesCountry.Id into armedForcesCountry
                             join db_resourceTeam in ctx.ResourceTeams.AsNoTracking() on res.ResourceTeamId equals db_resourceTeam.Id into resourceTeam
                             from db_city in city.DefaultIfEmpty()
                             from db_country in country.DefaultIfEmpty()
                             from db_state in state.DefaultIfEmpty()
                             from db_jobTitle in jobTitle.DefaultIfEmpty()
                             from db_division in division.DefaultIfEmpty()
                             from db_department in department.DefaultIfEmpty()
                             from db_clinic in clinic.DefaultIfEmpty()
                             from db_cliniclocation in clinicLocation.DefaultIfEmpty()
                             from db_armedForcesCountry in armedForcesCountry.DefaultIfEmpty()
                             from db_resourceTeam in resourceTeam.DefaultIfEmpty()
                             where res.Id == id
                             select new ResourceResponse
                             {
                                 Id = res.Id,
                                 Name = res.LastName + " " + res.FirstName,
                                 FirstName = res.FirstName,
                                 LastName = res.LastName,
                                 MiddleName = res.MiddleName,
                                 BirthDate = res.BirthDate,
                                 Gender = res.Gender,
                                 LGBT = res.LGBT,
                                 MartialStatus = res.MartialStatus,
                                 Ethnicity = res.Ethnicity,
                                 Prefix = res.Prefix,
                                 Suffix = res.Suffix,
                                 Active = res.Active,
                                 PreferredFirstname = res.PreferredFirstname,
                                 Email = res.Email,
                                 Address = res.Address,
                                 Address2 = res.Address2,
                                 Address3 = res.Address3,
                                 Address4 = res.Address4,
                                 Address5 = res.Address5,
                                 Address6 = res.Address6,
                                 CityId = res.CityId,
                                 //City = city.Select(c => new PicklistResponse { Id = db_city.Id, Name = db_city.Name }).FirstOrDefault(),
                                 ZipCodeTemp = res.ZipCode,
                                 Country = country.Select(c => new PicklistResponse { Id = db_country.Id, Name = db_country.Name }).FirstOrDefault(),
                                 State = state.Select(c => new PicklistResponse { Id = db_state.Id, Name = db_state.Name }).FirstOrDefault(),
                                 TimekeeperNumber = res.TimekeeperNumber,
                                 EmployeeId = res.EmployeeId,
                                 JobTitle = jobTitle.Select(c => new PicklistResponse { Id = db_jobTitle.Id, Name = db_jobTitle.Name }).FirstOrDefault(),
                                 StartDate = res.StartDate,
                                 Ssn = res.Ssn,
                                 EndDate = res.EndDate,
                                 Location = res.Location,
                                 OfficeNumber = res.OfficeNumber,
                                 Extension = res.Extension,
                                 SupervisorId = res.SupervisorId,
                                 TimeZoneInfo = res.TimeZoneInfo,
                                 HomeNumber = res.HomeNumber,
                                 Division = division.Select(c => new PicklistResponse { Id = db_division.Id, Name = db_division.Name }).FirstOrDefault(),
                                 DivisionId = res.DivisionId,
                                 ClinicId = res.ClinicId,
                                 Clinic = clinic.Select(c => new ClinicAutoCompleteResponse { Id = db_clinic.Id, Name = db_clinic.Name, ClinicPrefix = db_clinic.ClinicPrefix }).FirstOrDefault(),
                                 ClinicLocationId = res.ClinicLocationId,
                                 ClinicLocation = clinicLocation.Select(c => new ClinicLocationAutoCompleteResponse { Id = db_cliniclocation.Id, Name = db_cliniclocation.Name, LocationPrefix = db_cliniclocation.LocationPrefix, ClinicId = db_cliniclocation.ClinicId }).FirstOrDefault(),
                                 Relationship = res.Relationship,
                                 MobilePhoneNumber = res.MobilePhoneNumber,
                                 Department = department.Select(c => new PicklistResponse { Id = db_department.Id, Name = db_department.Name }).FirstOrDefault(),
                                 DepartmentId = res.DepartmentId,
                                 EmergencyContactName = res.EmergencyContactName,
                                 EmergencyContactPhoneNumber = res.EmergencyContactPhoneNumber,
                                 FirmRate = res.FirmRate,
                                 NationalRate = res.NationalRate,
                                 EmploymentType = res.EmploymentType,
                                 Salary = res.Salary,
                                 BonusOrOtherPay = res.BonusOrOtherPay,
                                 GradeLevel = res.GradeLevel,
                                 YearsOfCollege = res.YearsOfCollege,
                                 YearsEmployed = res.YearsEmployed,
                                 AvailableToTravel = res.AvailableToTravel,
                                 WorksWellInTeam = res.WorksWellInTeam,
                                 ValidPassport = res.ValidPassport,
                                 WorksWellAlone = res.WorksWellAlone,
                                 ArmedForcesCountry = armedForcesCountry.Select(c => new PicklistResponse { Id = db_armedForcesCountry.Id, Name = db_armedForcesCountry.Name }).FirstOrDefault(),
                                 ArmedForcesBranch = res.ArmedForcesBranch,
                                 ArmedForces = res.ArmedForces,
                                 DrugTest = res.DrugTest,
                                 CreditReportOk = res.CreditReportOk,
                                 MotorVehicleReport = res.MotorVehicleReport,
                                 EmploymentEligibilityVerification = res.EmploymentEligibilityVerification,
                                 InternationalWorkHistory = res.InternationalWorkHistory,
                                 ProfessionalReferenceChecks = res.ProfessionalReferenceChecks,
                                 FormI9 = res.FormI9,
                                 FormEVerify = res.FormEVerify,
                                 BusinessUnitId = res.BusinessUnitId,
                                 ResourceTeam = resourceTeam.Select(c => new PicklistResponse { Id = db_resourceTeam.Id, Name = db_resourceTeam.Name }).FirstOrDefault(),
                                 ProfessionalReferenceChecksOther = res.ProfessionalReferenceChecksOther,
                                 EmploymentEligibilityVerificationOther = res.EmploymentEligibilityVerificationOther,
                                 MotorVehicleReportOther = res.MotorVehicleReportOther,
                                 InternationalWorkHistoryOther = res.InternationalWorkHistoryOther,
                                 CredentialVerifications = res.CredentialVerifications,
                                 CredentialVerificationsOther = res.CredentialVerificationsOther,
                                 FormEVerifyOther = res.FormEVerifyOther,
                                 FormI9Other = res.FormI9Other,
                                 FingerPrinting = res.FingerPrinting,
                                 FingerPrintingOther = res.FingerPrintingOther,
                                 DrugScreening = res.DrugScreening,
                                 DrugScreeningOther = res.DrugScreeningOther,
                                 CreditCheck = res.CreditCheck,
                                 CreditCheckOther = res.CreditCheckOther,
                                 CollegeDegree = res.CollegeDegree,
                                 EverBeenConvictedOfACrime = res.EverBeenConvictedOfACrime,
                                 InformationCertifiedByEmployer = res.InformationCertifiedByEmployer,
                                 ProofOfCitizenship = res.ProofOfCitizenship,
                                 HourlyRate = res.HourlyRate,
                                 Range = res.Range,
                                 PhotoFileNameUrl = null,
                                 PhotoFileName = res.PhotoFileName,
                                 CvFileNameUrl = null,
                                 CvFileName = res.CvFileName,
                                 Productivity = res.Productivity,
                                 Rating = res.Rating,
                                 RatingDate = res.RatingDate,
                                 CubicalOrOfficeNumber = res.CubicalOrOfficeNumber,
                                 Overview = res.Overview
                             }).ToList();

                newresource = query.FirstOrDefault();
                #endregion

                #region Business Unit
                if (newresource.BusinessUnitId != null)
                {
                    var businessUnitQuery = (from businessUnit in ctx.DropDownConfigs.AsNoTracking()
                                             where businessUnit.Id == newresource.BusinessUnitId
                                             select new PicklistResponse
                                             {
                                                 Id = businessUnit.Id,
                                                 Name = businessUnit.Name
                                             }).FirstOrDefault();
                    newresource.BusinessUnit = businessUnitQuery;
                }
                #endregion

                #region Resource Indutry
                var resourceIndustryQuery = (from resourceIndustry in ctx.Industry.AsNoTracking()
                                             join industryDropDown in ctx.DropDownConfigs.AsNoTracking() on resourceIndustry.IndustryId equals industryDropDown.Id into industry
                                             from industryDropDown in industry.DefaultIfEmpty()
                                             where resourceIndustry.ResourceId == id
                                             select new ResourceIndustryResponse
                                             {
                                                 Id = resourceIndustry.Id,
                                                 ResourceId = resourceIndustry.ResourceId,
                                                 IndustryId = resourceIndustry.IndustryId,
                                                 Industry = industry.Select(c => new DropDownConfigResponse { Id = industryDropDown.Id, Name = industryDropDown.Name, DropDownType = industryDropDown.DropDownType, ConfigField1 = industryDropDown.ConfigField1 }).FirstOrDefault(),
                                                 DropDownType = industryDropDown.DropDownType,
                                                 Name = industryDropDown.Name,
                                                 ConfigField1 = industryDropDown.ConfigField1
                                             });
                newresource.Industry = resourceIndustryQuery.ToArray();
                #endregion

                #region Resource Practice Areas
                var resourcePracticeAreaQuery = (from resourcePracticeArea in ctx.ResourcePracticeAreas.AsNoTracking()
                                                 join db_practiceArea in ctx.DropDownConfigs.AsNoTracking() on resourcePracticeArea.PracticeAreaId equals db_practiceArea.Id into practiceArea
                                                 from db_practiceArea in practiceArea.DefaultIfEmpty()
                                                 where resourcePracticeArea.ResourceId == id
                                                 select new ResourcePracticeAreaResponse
                                                 {
                                                     Id = resourcePracticeArea.Id,
                                                     ResourceId = resourcePracticeArea.ResourceId,
                                                     PracticeAreaId = resourcePracticeArea.PracticeAreaId,
                                                     PracticeArea = practiceArea.Select(c => new PicklistResponse { Id = db_practiceArea.Id, Name = db_practiceArea.Name }).FirstOrDefault(),
                                                     Name = db_practiceArea.Name
                                                 });
                newresource.ResourcePracticeAreas = resourcePracticeAreaQuery.ToArray();
                #endregion

                #region ResourceSpokenLanguages
                var spokenLanguageQuery = (from spokenLanguage in ctx.ResourceSpokenLanguages.AsNoTracking()
                                           join lang in ctx.Languages.AsNoTracking() on spokenLanguage.LanguageId equals lang.Id into language
                                           from lang in language.DefaultIfEmpty()
                                           where spokenLanguage.ResourceId == id
                                           select new ResourceSpokenLanguageResponse
                                           {
                                               Id = spokenLanguage.Id,
                                               Language = language.Select(c => new PicklistResponse { Id = lang.Id, Name = lang.Name }).FirstOrDefault(),
                                               SpeakingProficiency = spokenLanguage.SpeakingProficiency,
                                               WritingProficiency = spokenLanguage.WritingProficiency,
                                               YearsOfExpirience = spokenLanguage.YearsOfExpirience,
                                               ResourceId = spokenLanguage.ResourceId,
                                           });
                newresource.SpokenLanguages = spokenLanguageQuery.ToArray();

                #endregion

                #region ResourceEducations
                var resourceEducationQuery = (from resourceEducation in ctx.ResourceEducations.AsNoTracking()
                                              join resourceDesignation in ctx.DropDownConfigs.AsNoTracking() on resourceEducation.ResourceDesignationId equals resourceDesignation.Id into designation
                                              from resourceDesignation in designation.DefaultIfEmpty()
                                              where resourceEducation.ResourceId == id
                                              select new ResourceEducationResponse
                                              {
                                                  Id = resourceEducation.Id,
                                                  Subject = resourceEducation.Subject,
                                                  NameOfSchool = resourceEducation.NameOfSchool,
                                                  DateObtained = resourceEducation.DateObtained,
                                                  StartDate = resourceEducation.StartDate,
                                                  EndDate = resourceEducation.EndDate,
                                                  ResourceId = resourceEducation.ResourceId,
                                                  ResourceDesignationId = resourceEducation.ResourceDesignationId,
                                                  ResourceDesignation = designation.Select(c => new PicklistResponse { Id = resourceDesignation.Id, Name = resourceDesignation.Name }).FirstOrDefault(),
                                              });
                newresource.Educations = resourceEducationQuery.ToArray();
                //if (newresource.Educations != null)
                //{
                //    foreach (var item in newresource.Educations)
                //    {
                //        item.DateObtained = (item.DateObtained != null) ? (item.DateObtained.Value.ToLocalTime()) : item.DateObtained;
                //        item.StartDate = (item.StartDate != null) ? (item.StartDate.Value.ToLocalTime()) : item.StartDate;
                //    }
                //}

                #endregion

                #region ResourceTravels
                var resourceTravelQuery = (from resourceTravel in ctx.ResourceTravels.AsNoTracking()
                                           join db_resourceTravelCountry in ctx.Countries.AsNoTracking() on resourceTravel.CountryId equals db_resourceTravelCountry.Id into resourceTravelCountry
                                           from db_resourceTravelCountry in resourceTravelCountry.DefaultIfEmpty()
                                           where resourceTravel.ResourceId == id
                                           select new ResourceTravelResponse
                                           {
                                               Id = resourceTravel.Id,
                                               VisaOrPermit = resourceTravel.VisaOrPermit,
                                               CountryId = resourceTravel.CountryId,
                                               Country = resourceTravelCountry.Select(c => new PicklistResponse { Id = db_resourceTravelCountry.Id, Name = db_resourceTravelCountry.Name }).FirstOrDefault(),
                                               TimeRemainingInDays = resourceTravel.TimeRemainingInDays,
                                               SpecialConsiderations = resourceTravel.SpecialConsiderations,
                                               ResourceId = resourceTravel.ResourceId
                                           });
                newresource.Travels = resourceTravelQuery.ToArray();

                #endregion

                #region ResourceBehaviouralDetails
                var resourceBehaviouralDetailsQuery = (from behaviouralDetail in ctx.ResourceBehaviouralDetails.AsNoTracking()
                                                       where behaviouralDetail.ResourceId == id
                                                       select new ResourceBehaviouralDetailResponse
                                                       {
                                                           Id = behaviouralDetail.Id,
                                                           ResourceId = behaviouralDetail.ResourceId,
                                                           Behaviour = behaviouralDetail.Behaviour
                                                       });
                newresource.ResourceBehaviouralDetails = resourceBehaviouralDetailsQuery.ToArray();

                #endregion

                #region Resource Recognitions
                var resourceRecognitionsQuery = (from resourceRecognition in ctx.ResourceRecognitions.AsNoTracking()
                                                 where resourceRecognition.ResourceId == id
                                                 select new ResourceRecognitionResponse
                                                 {
                                                     Id = resourceRecognition.Id,
                                                     ResourceId = resourceRecognition.ResourceId,
                                                     Recognition = resourceRecognition.Recognition
                                                 });
                newresource.ResourceRecognitions = resourceRecognitionsQuery.ToArray();

                #endregion

                #region ResourceCertificates
                var resourceCertificateQuery = (from resourceCertificate in ctx.ResourceCertificates.AsNoTracking()
                                                join db_certificate in ctx.Certificates.AsNoTracking() on resourceCertificate.CertificateId equals db_certificate.Id into certificate
                                                join db_certificateState in ctx.States.AsNoTracking() on resourceCertificate.CertificateStateId equals db_certificateState.Id into certificateState
                                                from db_certificate in certificate.DefaultIfEmpty()
                                                from db_certificateState in certificateState.DefaultIfEmpty()
                                                where resourceCertificate.ResourceId == id
                                                select new ResourceCertificateResponse
                                                {
                                                    Id = resourceCertificate.Id,
                                                    Certificate = certificate.Select(c => new PicklistResponse { Id = db_certificate.Id, Name = db_certificate.Name }).FirstOrDefault(),
                                                    ExpirationDate = resourceCertificate.ExpirationDate,
                                                    CertificateStateId = resourceCertificate.CertificateStateId,
                                                    CertificateState = certificateState.Select(c => new PicklistResponse { Id = db_certificateState.Id, Name = db_certificateState.Name }).FirstOrDefault(),
                                                    ResourceId = resourceCertificate.ResourceId,
                                                    IssueDate = resourceCertificate.IssueDate
                                                });
                newresource.Certificates = resourceCertificateQuery.ToArray();
                //if (newresource.Certificates != null)
                //{
                //    foreach (var item in newresource.Certificates)
                //    {
                //        item.IssueDate = (item.IssueDate != null) ? (item.IssueDate.Value.ToLocalTime()) : item.IssueDate;
                //        item.Certified = (item.Certified != null) ? (item.Certified.ToLocalTime()) : item.Certified;
                //    }
                //}
                #endregion

                #region ResourceSkills
                var resourceSkillQuery = (from resourceSkill in ctx.ResourceSkills.AsNoTracking()
                                          join db_Skill in ctx.Skills.AsNoTracking() on resourceSkill.SkillId equals db_Skill.Id into skill
                                          from db_Skill in skill.DefaultIfEmpty()
                                          where resourceSkill.ResourceId == id
                                          select new ResourceSkillResponse
                                          {
                                              Id = resourceSkill.Id,
                                              Skill = skill.Select(c => new PicklistResponse { Id = db_Skill.Id, Name = db_Skill.Name }).FirstOrDefault(),
                                              YearsOfExpirience = resourceSkill.YearsOfExpirience,
                                              Proficiency = resourceSkill.Proficiency,
                                              ResourceId = resourceSkill.ResourceId,
                                              LastUpdated = resourceSkill.LastUpdated,
                                              Description = resourceSkill.Description
                                          });
                newresource.Skills = resourceSkillQuery.ToArray();
                #endregion

                #region ResourceCriminalRecords

                var resourceCriminalRecordQuery = (from resourceCriminalRecord in ctx.ResourceCriminalRecords.AsNoTracking()
                                                   where resourceCriminalRecord.ResourceId == id
                                                   select new ResourceCriminalRecordResponse
                                                   {
                                                       Id = resourceCriminalRecord.Id,
                                                       DateOfOffense = resourceCriminalRecord.DateOfOffense,
                                                       LocationOfOffense = resourceCriminalRecord.LocationOfOffense,
                                                       Offense = resourceCriminalRecord.Offense,
                                                       ResourceId = resourceCriminalRecord.ResourceId,
                                                       PenaltyOrDisposition = resourceCriminalRecord.PenaltyOrDisposition
                                                   });
                newresource.CriminalRecords = resourceCriminalRecordQuery.ToArray();
                #endregion

                #region ResourcePreviousEmployments

                var resourcePreviousEmploymentQuery = (from resourcePreviousEmployment in ctx.ResourcePreviousEmployments.AsNoTracking()
                                                       where resourcePreviousEmployment.ResourceId == id
                                                       select new ResourcePreviousEmploymentResponse
                                                       {
                                                           Id = resourcePreviousEmployment.Id,
                                                           Name = resourcePreviousEmployment.Name,
                                                           Address = resourcePreviousEmployment.Address,
                                                           TitleOfPosition = resourcePreviousEmployment.TitleOfPosition,
                                                           DutiesOfPosition = resourcePreviousEmployment.DutiesOfPosition,
                                                           ReasonForLeaving = resourcePreviousEmployment.ReasonForLeaving,
                                                           StartDate = resourcePreviousEmployment.StartDate,
                                                           EndDate = resourcePreviousEmployment.EndDate,
                                                           NumberOfSupervised = resourcePreviousEmployment.NumberOfSupervised,
                                                           Salary = resourcePreviousEmployment.Salary,
                                                           SalaryPer = resourcePreviousEmployment.SalaryPer,
                                                           HoursPerWeek = resourcePreviousEmployment.HoursPerWeek,
                                                           ResourceId = resourcePreviousEmployment.ResourceId
                                                       });
                newresource.PreviousEmployments = resourcePreviousEmploymentQuery.ToArray();
                #endregion

                #region CustomFieldValues
                var resourceCustomFieldValuesQuery = (from resourceCustomFieldValues in ctx.ResourceCustomFieldValues.AsNoTracking()
                                                      join db_CustomField in ctx.CustomFields.AsNoTracking() on resourceCustomFieldValues.CustomFieldId equals db_CustomField.Id into customField
                                                      from db_CustomField in customField.DefaultIfEmpty()
                                                      where resourceCustomFieldValues.ResourceId == id
                                                      select new ResourceCustomFieldValueResponse
                                                      {
                                                          Id = resourceCustomFieldValues.Id,
                                                          CustomField = customField
                                                                        .Select(c => new CustomFieldResponse
                                                                        {
                                                                            Id = db_CustomField.Id,
                                                                            Name = db_CustomField.Name,
                                                                            ResourceAvailable = db_CustomField.ResourceAvailable,
                                                                            Require = db_CustomField.Require,
                                                                            DefaultValue = db_CustomField.DefaultValue,
                                                                            Description = db_CustomField.Description,
                                                                            IsSearchable = db_CustomField.IsSearchable,
                                                                            Type = db_CustomField.Type
                                                                        }).FirstOrDefault(),
                                                          CustomFieldId = resourceCustomFieldValues.CustomFieldId,
                                                          ResourceId = resourceCustomFieldValues.ResourceId,
                                                          Value = resourceCustomFieldValues.Value
                                                      });
                newresource.CustomFieldValues = resourceCustomFieldValuesQuery.ToArray();

                if (newresource.CustomFieldValues != null)
                {
                    foreach (var item in newresource.CustomFieldValues.Select(x => x.CustomField))
                    {
                        var customFieldPropertiesQuery = (from CustomFieldProperty in ctx.CustomFieldProperties.AsNoTracking()
                                                          where CustomFieldProperty.CustomFieldId == item.Id
                                                          select new CustomFieldPropertyResponse
                                                          {
                                                              Id = CustomFieldProperty.Id,
                                                              CustomFieldId = CustomFieldProperty.CustomFieldId,
                                                              Name = CustomFieldProperty.Name,
                                                              Value = CustomFieldProperty.Value
                                                          }).ToList();

                        item.CustomFieldProperties = customFieldPropertiesQuery.ToArray();
                    }
                }
                #endregion

                #region ResourceWorkExperiences
                var resourceWorkExperienceQuery = (from resourceWorkExperience in ctx.ResourceWorkExperiences.AsNoTracking()
                                                   join db_WorkExperienceJobTitle in ctx.Positions.AsNoTracking() on resourceWorkExperience.JobTitleId equals db_WorkExperienceJobTitle.Id into workExperienceJobTitle
                                                   //join db_resourceWorkAssignments in ctx.ResourceWorkAssignments on resourceWorkExperience.Id equals db_resourceWorkAssignments.ResourceWorkExperienceId into resourceWorkAssignments
                                                   from db_WorkExperienceJobTitle in workExperienceJobTitle.DefaultIfEmpty()
                                                       //from db_resourceWorkAssignments in resourceWorkAssignments.DefaultIfEmpty()
                                                   where resourceWorkExperience.ResourceId == id
                                                   select new ResourceWorkExperienceResponse
                                                   {
                                                       Id = resourceWorkExperience.Id,
                                                       JobTitleId = resourceWorkExperience.JobTitleId,
                                                       WorkExperienceJobTitle = workExperienceJobTitle.Select(c => new PicklistResponse { Id = db_WorkExperienceJobTitle.Id, Name = db_WorkExperienceJobTitle.Name }).FirstOrDefault(),
                                                       StartDate = resourceWorkExperience.StartDate,
                                                       EndDate = resourceWorkExperience.EndDate,
                                                       ResourceId = resourceWorkExperience.ResourceId,
                                                       //ResourceWorkAssignments = resourceWorkAssignments.Select(c => new ResourceWorkAssignmentResponse
                                                       //{
                                                       //    Id = db_resourceWorkAssignments.Id,
                                                       //    AssignmentDescription = db_resourceWorkAssignments.AssignmentDescription,
                                                       //    StartDate = db_resourceWorkAssignments.StartDate,
                                                       //    EndDate = db_resourceWorkAssignments.EndDate,
                                                       //    ResourceWorkExperienceId = db_resourceWorkAssignments.ResourceWorkExperienceId
                                                       //}).ToArray()
                                                   });
                newresource.ResourceWorkExperiences = resourceWorkExperienceQuery.ToArray();

                if (newresource.ResourceWorkExperiences != null)
                {
                    foreach (var item in newresource.ResourceWorkExperiences)
                    {
                        var workExperienceAssignmentQuery = (from assignment in ctx.ResourceWorkAssignments.AsNoTracking()
                                                             join assignemntIndustry in ctx.DropDownConfigs.AsNoTracking() on assignment.AssignmentIndustryId equals assignemntIndustry.Id into industry
                                                             from assignemntIndustry in industry.DefaultIfEmpty()
                                                             where assignment.ResourceWorkExperienceId == item.Id
                                                             select new ResourceWorkAssignmentResponse
                                                             {
                                                                 Id = assignment.Id,
                                                                 AssignmentDescription = assignment.AssignmentDescription,
                                                                 AssignmentIndustryId = assignment.AssignmentIndustryId,
                                                                 AssignmentIndustry = industry.Select(c => new PicklistResponse { Id = assignemntIndustry.Id, Name = assignemntIndustry.Name, ConfigField1 = assignemntIndustry.ConfigField1 }).FirstOrDefault(),
                                                                 StartDate = assignment.StartDate,
                                                                 EndDate = assignment.EndDate,
                                                                 ResourceWorkExperienceId = assignment.ResourceWorkExperienceId,
                                                                 OfNote = assignment.OfNote,
                                                                 JudicialClerkShip = assignment.JudicialClerkShip
                                                             }).ToList();

                        item.ResourceWorkAssignments = workExperienceAssignmentQuery.ToArray();
                    }
                }

                #endregion

                #region ResourceProfessionalOrganizations
                var resourceProfessionalOrganizationQuery = (from resourceProfessionalOrganization in ctx.ResourceProfessionalOrganizations.AsNoTracking()
                                                             join db_committeeMembership in ctx.DropDownConfigs.AsNoTracking() on resourceProfessionalOrganization.CommitteeMembershipId equals db_committeeMembership.Id into committeeMembership
                                                             join db_committeeTitle in ctx.DropDownConfigs.AsNoTracking() on resourceProfessionalOrganization.CommitteeTitleId equals db_committeeTitle.Id into committeeTitle
                                                             from db_committeeMembership in committeeMembership.DefaultIfEmpty()
                                                             from db_committeeTitle in committeeTitle.DefaultIfEmpty()
                                                             where resourceProfessionalOrganization.ResourceId == id
                                                             select new ResourceProfessionalOrganizationResponse
                                                             {
                                                                 Id = resourceProfessionalOrganization.Id,
                                                                 CommitteeTitle = committeeTitle.Select(c => new PicklistResponse { Id = db_committeeTitle.Id, Name = db_committeeTitle.Name }).FirstOrDefault(),//Id = resourceProfessionalOrganization.CommitteeMembershipId,//
                                                                 CommitteeMembership = committeeMembership.Select(c => new PicklistResponse { Id = db_committeeMembership.Id, Name = db_committeeMembership.Name }).FirstOrDefault(),//Id = resourceProfessionalOrganization.CommitteeMembershipId,//
                                                                 CommitteeMembershipId = resourceProfessionalOrganization.CommitteeMembershipId,
                                                                 CommitteeTitleId = resourceProfessionalOrganization.CommitteeTitleId,
                                                                 ResourceId = resourceProfessionalOrganization.ResourceId,
                                                                 Organization = resourceProfessionalOrganization.Organization
                                                             });

                newresource.ResourceProfessionalOrganizations = resourceProfessionalOrganizationQuery.ToArray();
                #endregion

                #region Resource Insurances
                var resourceInsuranceQuery = (from resourceInsurance in ctx.ResourceInsurances.AsNoTracking()
                                              where resourceInsurance.ResourceId == id
                                              select new ResourceInsuranceResponse
                                              {
                                                  Id = resourceInsurance.Id,
                                                  ResourceId = resourceInsurance.ResourceId,
                                                  Participating = resourceInsurance.Participating,
                                                  EmployeeCost = resourceInsurance.EmployeeCost,
                                                  EmployerContribution = resourceInsurance.EmployerContribution
                                              });
                newresource.ResourceInsurances = resourceInsuranceQuery.ToArray();

                #endregion

                #region Resource Timeoff Benefit
                var resourceTimeoffBenefitQuery = (from ResourceTimeoffBenefit in ctx.ResourceTimeoffBenefits.AsNoTracking()
                                                   where ResourceTimeoffBenefit.ResourceId == id
                                                   select new ResourceTimeoffBenefitResponse
                                                   {
                                                       Id = ResourceTimeoffBenefit.Id,
                                                       ResourceId = ResourceTimeoffBenefit.ResourceId,
                                                       TotalBalance = ResourceTimeoffBenefit.TotalBalance,
                                                       Balance = ResourceTimeoffBenefit.Balance,
                                                       AccruedCurrent = ResourceTimeoffBenefit.AccruedCurrent
                                                   });
                newresource.ResourceTimeoffBenefits = resourceTimeoffBenefitQuery.ToArray();

                #endregion
            }

            #region Role configuration 
            if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
            {
                if (roleInfo.AccessRights.IsForDepartmentOnly && newresource.DepartmentId != user.Resource.DepartmentId)
                    return StatusCode(HttpStatusCode.Forbidden);
                if (roleInfo.AccessRights.IsForDivisionOnly && newresource.DivisionId != user.Resource.DivisionId)
                {
                    return StatusCode(HttpStatusCode.Forbidden);
                }

            }

            #endregion

            var resource = Mapper.Map<ResourceResponse, Resource>(newresource);
            resource.ZipCode = newresource.ZipCodeTemp;
            var fincalResource = ApplyRestrictions(roleInfo.AccessRights.RestrictAccess, resource, new Resource());

            if (fincalResource == null)
                return NotFound();

            await _resourceService.AdditionResourceCustomFields(fincalResource);

            var viewModel = Mapper.Map<Resource, ResourceResponse>(fincalResource);
            viewModel.ZipCodeTemp = resource.ZipCode;

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                #region supervisior 

                if (viewModel.SupervisorId != null)
                {
                    viewModel.Supervisor = new PicklistResponse();
                    var resourceSupervisorQuery123 = (from supervisor in ctx.Resources.AsNoTracking()
                                                      where supervisor.Id == newresource.SupervisorId
                                                      select new PicklistResponse
                                                      {
                                                          Id = supervisor.Id,
                                                          Name = supervisor.LastName + " " + supervisor.FirstName,
                                                          Email = supervisor.Email
                                                      }).FirstOrDefault();
                    viewModel.Supervisor = resourceSupervisorQuery123;
                }

                #endregion

                #region City Postal Code
                if (viewModel.CityId != null)
                {
                    var cityQuery = (from city in ctx.Cities.AsNoTracking()
                                     where city.Id == newresource.CityId
                                     select new CityResponse
                                     {
                                         Id = city.Id,
                                         Name = city.Name,
                                         StateId = city.StateId,
                                         StateCode = city.StateCode
                                     }).FirstOrDefault();

                    if (cityQuery != null)
                    {
                        cityQuery.State = (cityQuery.StateId != null) ? (ctx.States.AsNoTracking().Where(x => x.Id == cityQuery.StateId).Select(y => new CodeResponse { Id = y.Id, Name = y.Name }).FirstOrDefault()) : null;
                        cityQuery.StateName = (cityQuery.State != null) ? cityQuery.State.Name : cityQuery.StateCode;

                        //List<CodeResponse> postalcodes = ctx.PostalCodes.AsNoTracking()
                        //                    .Where(x => x.CityId == cityQuery.Id)
                        //                    .Select(y => new CodeResponse { Id = y.Id, Name = y.ZipCode }).ToList();

                        //cityQuery.PostalCodeLists = (postalcodes != null) ? postalcodes.ToArray() : null;
                        viewModel.City = cityQuery;
                    }
                }
                #endregion

                #region zipCode
                if (viewModel.ZipCodeTemp != null)
                {
                    var zipCodeQuery = (from code in ctx.PostalCodes.AsNoTracking()
                                        where code.ZipCode == viewModel.ZipCodeTemp
                                        select new PicklistResponse
                                        {
                                            Id = code.Id,
                                            Name = code.ZipCode
                                        }).FirstOrDefault();
                    viewModel.ZipCode = zipCodeQuery;
                }
                #endregion
            }

            #region Load SpokenLanguages from AWS Server
            foreach (var resourceSpokenLanguageResponse in viewModel.SpokenLanguages)
            {
                resourceSpokenLanguageResponse.Files =
                    (FileUploadHelpers.GetFileNames(GetResourceLanguageS3Path(resource.Id,
                        resourceSpokenLanguageResponse.Language.Id)))
                        .Select(fileName => new FileRequest
                        {
                            S3BucketUrl =
                                FileUploadHelpers.GetStorageUrl(fileName,
                                    GetResourceLanguageS3Path(resource.Id, resourceSpokenLanguageResponse.Language.Id)),
                            S3BucketName = fileName,
                            Name = GetOriginalFileNameWithoutBucketGuid(fileName)
                        }).ToArray();
            }
            #endregion

            if (viewModel != null)
            {
                foreach (var item in viewModel.CustomFieldValues)
                {
                    CustomFieldResponse pro = newresource.CustomFieldValues.Where(x => x.CustomFieldId == item.CustomFieldId).Select(x => x.CustomField).FirstOrDefault();
                    if (pro != null)
                    {
                        item.CustomField.CustomFieldProperties = pro.CustomFieldProperties;
                    }
                }
            }

            #region Load Custome field File From AWS
            foreach (var file in viewModel.CustomFieldValues.Where(x => x.CustomField.Type == CustomFieldTypeEnum.File))
            {
                file.AdditionalInfo = FileUploadHelpers.GetStorageUrl(file.Value,
                    GetResourceCustomFieldS3Path(resource.Id));
            }
            #endregion

            return Ok(viewModel);
        }

        /// <summary>
        /// Method Description : Using for Create New Resource.   
        /// </summary>
        /// <param name="resourceRequest">Resource Request Model</param>
        /// <returns>Resource Response Model</returns>
        [Authorize]
        [HttpPost]
        [SwaggerResponse(201, "Created", typeof(ResourceResponse))]
        public async Task<IHttpActionResult> CreateResource([FromBody]ResourceRequest resourceRequest)
        {
            var roleInfo = GetRoleAccessRights();
            var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Create, Section.Resources);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }

            #region Verify Country State City Zip Code Combination 
            if (resourceRequest.CountryId != null && resourceRequest.StateId != null
                       && resourceRequest.CityId != null && resourceRequest.ZipCode != null && resourceRequest.ZipCode != "")
            {
                PostalCodeResponse code = await GetPostalCodeDetail(resourceRequest.ZipCode);
                if (code == null)
                {
                    ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    return BadRequest(ModelState);
                }
                else
                {
                    if (code.Country != null)
                    {
                        if (resourceRequest.CountryId != code.Country.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.State != null)
                    {
                        if (resourceRequest.StateId != code.State.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.City != null)
                    {
                        if (resourceRequest.CityId != code.City.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }
            #endregion

            if (resourceRequest.ResourceProfessionalOrganizations != null)
            {
                foreach (var item in resourceRequest.ResourceProfessionalOrganizations)
                {
                    item.CommitteeMembershipId = (item.CommitteeMembership != null) ? (long?)Convert.ToDouble(item.CommitteeMembership) : null;
                    item.CommitteeTitleId = (item.CommitteeTitle != null) ? (long?)Convert.ToDouble(item.CommitteeTitle) : null;
                    item.CommitteeTitle = null;
                    item.CommitteeMembership = null;
                }
            }
            //for the emaiiId validation
            if (resourceRequest.Email != null && resourceRequest.Email != "")
            {
                bool validate = IsValidEmailAddress(resourceRequest.Email);
                if (validate == false)
                {
                    ModelState.AddModelError("Resource", " Invalid Email Address");
                    return BadRequest(ModelState);
                }
            }

            if (resourceRequest.Educations != null)
            {
                foreach (var item in resourceRequest.Educations)
                {
                    item.DateObtained = (item.DateObtained != null) ? (TimeZoneInfo.ConvertTimeToUtc(item.DateObtained.Value)) : item.DateObtained;
                    item.StartDate = (item.StartDate != null) ? (TimeZoneInfo.ConvertTimeToUtc(item.StartDate.Value)) : item.StartDate;
                }
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            resourceRequest.Salary = (resourceRequest.Salary == null) ? 0 : resourceRequest.Salary;
            resourceRequest.BonusOrOtherPay = (resourceRequest.BonusOrOtherPay == null) ? 0 : resourceRequest.BonusOrOtherPay;
            decimal? sumSalaryAndBonus = (resourceRequest.Salary + resourceRequest.BonusOrOtherPay);
            resourceRequest.HourlyRate = (sumSalaryAndBonus == null) ? 0 : (sumSalaryAndBonus.Value / 2080);

            if (resourceRequest.Ssn != null)
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbset = ctx.Set<Resource>();
                    var resourceSSNList = dbset.Where(x => x.Ssn != null && x.Ssn != "").Select(v => new { ssn = v.Ssn }).ToList();

                    if (resourceSSNList.Count > 0)
                    {
                        bool test = resourceSSNList.Any(x => x.ssn == resourceRequest.Ssn);
                        if (resourceSSNList.Any(x => x.ssn == resourceRequest.Ssn))
                        {
                            ModelState.AddModelError("Resource", "SSN number is already exists with another resource");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }

            var resource = Mapper.Map<ResourceRequest, Resource>(resourceRequest);
            await _resourceService.CreateAsync(resource, roleInfo.AccessRights.RestrictAccess);
            await ResourceLanguageFileAmazonSave(resource.Id, resourceRequest, true);
            await CustomFieldFileAmazonSave(resource.Id, true);
            return Created(GetEntityLocation(resource.Id), Mapper.Map<Resource, ResourceResponse>(resource));
        }

        private async Task<PostalCodeResponse> GetPostalCodeDetail(string zipCode)
        {
            PostalCodeResponse postalCode = new PostalCodeResponse();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var query = await (from code in ctx.PostalCodes.AsNoTracking()
                                   join db_city in ctx.Cities.AsNoTracking() on code.CityId equals db_city.Id into city
                                   join db_state in ctx.States.AsNoTracking() on code.StateId equals db_state.Id into state
                                   join db_country in ctx.Countries.AsNoTracking() on code.CountryId equals db_country.Id into country

                                   from db_city in city.DefaultIfEmpty()
                                   from db_state in state.DefaultIfEmpty()
                                   from db_country in country.DefaultIfEmpty()

                                   where code.ZipCode == zipCode
                                   select new PostalCodeResponse
                                   {
                                       Id = code.Id,
                                       ZipCode = code.ZipCode,
                                       City = city.Select(c => new CodeResponse { Id = db_city.Id, Name = (db_city.Name + " - " + db_city.StateCode), StateCode = db_city.StateCode }).FirstOrDefault(),
                                       State = state.Select(c => new CodeResponse { Id = db_state.Id, Name = db_state.Name, StateCode = db_state.StateCode }).FirstOrDefault(),
                                       Country = country.Select(c => new CodeResponse { Id = db_country.Id, Name = db_country.Name, StateCode = null }).FirstOrDefault(),
                                   }).ToListAsync();

                postalCode = query.FirstOrDefault();
                return postalCode;
            }
        }

        /// <summary>
        /// Method Description : Using for Update Resource.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="resourceRequest">Resource Request Model</param>
        /// <returns>Resource Response Model</returns>
        [Authorize]
        [HttpPut]
        public async Task<IHttpActionResult> EditResource(long id, [FromBody] ResourceRequest resourceRequest)
        {
            if (resourceRequest.GradeLevel == "")
                resourceRequest.GradeLevel = null;

            if (resourceRequest.ZipCode != null)
                resourceRequest.ZipCodeTemp = resourceRequest.ZipCode;
            else
                resourceRequest.ZipCodeTemp = null;

            //for the emaiiId validation(1574)
            if (resourceRequest.Email != null && resourceRequest.Email != "")
            {
                bool validate = IsValidEmailAddress(resourceRequest.Email);
                if (validate == false)
                {
                    ModelState.AddModelError("Resource", " Invalid Email Address");
                    return BadRequest(ModelState);
                }
            }

            #region Verify Country State City Zip Code Combination 
            if (resourceRequest.CountryId != null && resourceRequest.StateId != null
                       && resourceRequest.CityId != null && resourceRequest.ZipCode != null && resourceRequest.ZipCode != "")
            {
                PostalCodeResponse code = await GetPostalCodeDetail(resourceRequest.ZipCode);
                if (code == null)
                {
                    ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                    return BadRequest(ModelState);
                }
                else
                {
                    if (code.Country != null)
                    {
                        if (resourceRequest.CountryId != code.Country.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.State != null)
                    {
                        if (resourceRequest.StateId != code.State.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                    if (code.City != null)
                    {
                        if (resourceRequest.CityId != code.City.Id)
                        {
                            ModelState.AddModelError("Resource", "The City, State and Postal Code combination is incorrect.");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }
            #endregion


            if (resourceRequest.SupervisorId != null)
            {
                string query = "select convert(bigint,count(*)) from resources where supervisor_id = " + id + " and id in (" + resourceRequest.SupervisorId + ")";
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var data = ctx.Database.SqlQuery<long>(query).FirstOrDefault();
                    if (data > 0)
                    {
                        ModelState.AddModelError("Resource", "You are supervisor for this resource , cannot assign as a supervisor");
                        return BadRequest(ModelState);
                    }
                }
            }

            resourceRequest.Salary = (resourceRequest.Salary == null) ? 0 : resourceRequest.Salary;
            resourceRequest.BonusOrOtherPay = (resourceRequest.BonusOrOtherPay == null) ? 0 : resourceRequest.BonusOrOtherPay;
            decimal? sumSalaryAndBonus = (resourceRequest.Salary + resourceRequest.BonusOrOtherPay);
            resourceRequest.HourlyRate = (sumSalaryAndBonus == null) ? 0 : (sumSalaryAndBonus.Value / 2080);

            if (resourceRequest.Ssn != null)
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbset = ctx.Set<Resource>();
                    var resourceSSNList = dbset.Where(x => x.Ssn != null && x.Ssn != "" && x.Id != id).Select(v => new { ssn = v.Ssn }).ToList();

                    if (resourceSSNList.Count > 0)
                    {
                        bool test = resourceSSNList.Any(x => x.ssn == resourceRequest.Ssn);
                        if (resourceSSNList.Any(x => x.ssn == resourceRequest.Ssn))
                        {
                            ModelState.AddModelError("Resource", "SSN number is already exists with another resource");
                            return BadRequest(ModelState);
                        }
                    }
                }
            }
            if (resourceRequest.Active == false)
            {
                bool isSuperVisor = IsAsSupervisor(id);
                if (isSuperVisor == true)
                {
                    ModelState.AddModelError("Resource", "Resource is associated with other resources as supervisor.");
                    ModelState.AddModelError("ErrorCode", "1008");
                    return BadRequest(ModelState);
                }
            }
            if (resourceRequest.SupervisorId != null)
            {
                if (resourceRequest.SupervisorId == id)
                {
                    ModelState.AddModelError("Resource", "Resource doesn’t assign himself as a Supervisor");
                    return BadRequest(ModelState);
                }
            }
            if (resourceRequest.Educations != null)
            {
                foreach (var item in resourceRequest.Educations)
                {
                    item.DateObtained = (item.DateObtained != null) ? (TimeZoneInfo.ConvertTimeToUtc(item.DateObtained.Value)) : item.DateObtained;
                    item.StartDate = (item.StartDate != null) ? (TimeZoneInfo.ConvertTimeToUtc(item.StartDate.Value)) : item.StartDate;
                }
            }
            if (resourceRequest.Certificates != null)
            {
                foreach (var item in resourceRequest.Certificates)
                {
                    item.IssueDate = (item.IssueDate != null) ? (TimeZoneInfo.ConvertTimeToUtc(item.IssueDate.Value)) : item.IssueDate;
                    item.Certified = (item.Certified != null) ? (TimeZoneInfo.ConvertTimeToUtc(item.Certified)) : item.Certified;
                }
            }

            var roleInfo = GetRoleAccessRights();
            var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Update, Section.Resources);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (resourceRequest.ResourceProfessionalOrganizations != null)
            {
                foreach (var item in resourceRequest.ResourceProfessionalOrganizations)
                {
                    item.CommitteeMembershipId = (item.CommitteeMembership != null) ? (long?)Convert.ToDouble(item.CommitteeMembership) : null;
                    item.CommitteeTitleId = (item.CommitteeTitle != null) ? (long?)Convert.ToDouble(item.CommitteeTitle) : null;
                    item.CommitteeTitle = null;
                    item.CommitteeMembership = null;
                }
            }
            #region Update Into Resource AVD Detail Data when Job Title is Change
            long? oldJobTitleId = 0;
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                var dbset = ctx.Set<Resource>();
                oldJobTitleId = dbset.Where(x => x.Id == id).Select(v => v.JobTitleId).FirstOrDefault();
            }
            #endregion

            var resource = Mapper.Map<ResourceRequest, Resource>(resourceRequest);
            resource.Id = id;

            if (resourceRequest.ZipCodeTemp != null)
                resource.ZipCode = resourceRequest.ZipCodeTemp;

            await _resourceService.UpdateAsync(resource, roleInfo.AccessRights.RestrictAccess);
            await ResourceLanguageFileAmazonSave(id, resourceRequest);
            await CustomFieldFileAmazonSave(resource.Id);
            return Ok(Mapper.Map<Resource, ResourceResponse>(resource));
        }

        /// <summary>
        /// Method Description : Using for Delete Resource by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Model State</returns>
        [Authorize]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteItem(long id)
        {
            var statusCodeResult = GetStatusCodeError(Crud.Delete, Section.Resources);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            var item = _resourceRepository.Read(id);
            if (item == null)
            {
                return NotFound();
            }

            var user = AppUserManager.Users.FirstOrDefault(x => x.ResourceId == item.Id && x.UserName == ApplicationUser.SystemAdminUser);
            if (user != null)
            {
                ModelState.AddModelError("Resource", "Cannot delete resource with link to Admin user");
                return BadRequest(ModelState);
            }

            if (item != null)
            {
                bool isSuperVisor = IsAsSupervisor(id);
                if (isSuperVisor == true)
                {
                    ModelState.AddModelError("Resource", "Resource is associated with other resources as supervisor.");
                    ModelState.AddModelError("ErrorCode", "1008");
                    return BadRequest(ModelState);
                }
            }

            var errorMessage = await _resourceService.DeleteResourceAsync(item);
            if (string.IsNullOrEmpty(errorMessage))
            {
                FileUploadHelpers.DeleteFolder(string.Format(FileUploadHelpers.ResourceFolderTemplate, id));
                return Ok(item);
            }

            ModelState.AddModelError("Resource", errorMessage);
            return BadRequest(ModelState);
        }

        private bool IsAsSupervisor(long resourceId)
        {
            bool isSupervisored = false;

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                var dbset = ctx.Set<Resource>();
                var resourceSupervisorList = dbset.Where(x => x.SupervisorId != null && x.SupervisorId == resourceId).Select(v => new { supervisorId = v.SupervisorId }).ToList();
                if (resourceSupervisorList.Count > 0)
                {
                    isSupervisored = true;
                }
            }
            return isSupervisored;
        }

        /// <summary>
        /// Method Description : Using for Get Resource List of Supervisor by Resource Id.    
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <returns>Resource List With Supervisor Response Model</returns>
        [Authorize]
        [HttpGet]
        [Route("api/Resources/conflict-resources")]
        public async Task<IHttpActionResult> GetResourceListOfSupervisor(long? resourceId)
        {
            var roleInfo = GetRoleAccessRights();
            var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            List<ResourceListWithSupervisorResponse> responceList = new List<ResourceListWithSupervisorResponse>();
            if (resourceId != null)
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    string query = "select r.Id as ResourceId,(r.last_name+' '+r.first_name) as ResourceName,r.supervisor_id as SupervisorId,(select (rs.last_name+' '+ rs.first_name) from resources rs where rs.Id = r.supervisor_id) as SupervisorName from resources r where supervisor_id = " + resourceId + " order by id asc";
                    responceList = await ctx.Database.SqlQuery<ResourceListWithSupervisorResponse>(query).ToListAsync();
                }
            }
            return Ok(responceList);
        }

        /// <summary>
        /// Method Description : Using for Update Resource.   
        /// </summary>
        /// <param name="request">Resource List With Supervisor Requet Model</param>
        [Authorize]
        [HttpPost]
        [Route("api/Resources/conflict-resources")]
        public async Task<IHttpActionResult> UpdateSupervisorOfResource(ResourceListWithSupervisorRequet[] request)
        {
            if (request != null && request.Length > 0)
            {
                foreach (var item in request)
                {
                    using (var dbContextScope = _dataContextScopeFactory.Create())
                    {
                        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                        string query = "update resources set supervisor_id =" + item.SupervisorId + " where Id = " + item.ResourceId;
                        var res = await ctx.Database.ExecuteSqlCommandAsync("update resources set supervisor_id ={0} where Id = {1}", item.SupervisorId, item.ResourceId);
                    }
                }
            }
            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Update Resource.   
        /// </summary>
        /// <param name="resourceBulkRequest">Resource Bulk Request Model</param>
        /// <returns>Model State</returns>
        [Authorize]
        [HttpPost]
        [Route("api/Resources/update-bulk/")]
        public async Task<IHttpActionResult> BulkEditResource([FromBody]ResourceBulkRequest resourceBulkRequest)
        {
            try
            {
                await _resourceService.MergeBulkUpdate(resourceBulkRequest.Ids, Mapper.Map<ResourceRequest, Resource>((ResourceRequest)resourceBulkRequest.Model));
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest(ModelState);
            }
        }

        private async Task CustomFieldFileAmazonSave(long id, bool isNew = false)
        {
            var taskList = new List<Task>();

            var customFieldsFiles = (await _customFieldValueRepository.ListAsync(
                new FilterQuery<ResourceCustomFieldValue>().AddFilter(
                    x => x.ResourceId == id && x.CustomField.Type == CustomFieldTypeEnum.File))).ToList();

            var s3Files = isNew ? new string[0] : FileUploadHelpers.GetFileNames(
                string.Format(FileUploadHelpers.ResourceCustomFieldFilesFolderTemplate, id));


            foreach (var resourceCustomFieldValue in customFieldsFiles)
            {
                if (string.IsNullOrWhiteSpace(resourceCustomFieldValue.Value))
                    continue;

                if (s3Files.Contains(resourceCustomFieldValue.Value))
                {
                    continue;
                }

                taskList.Add(FileUploadHelpers.CopyAsync(resourceCustomFieldValue.Value, "temp",
                    resourceCustomFieldValue.Value,
                    GetResourceCustomFieldS3Path(id)));
            }

            if (!isNew)
            {
                var deleteItems = s3Files.Where(x => !customFieldsFiles.Select(s => s.Value).Contains(x)).ToList();

                var deletedFiles =
                    deleteItems.Select(deleteItem => GetResourceCustomFieldS3Path(id) + "/" + deleteItem).ToList();
                if (deletedFiles.Any())
                    taskList.Add(FileUploadHelpers.S3DeleteFilesAsync(deletedFiles.ToArray()));
            }
            await Task.WhenAll(taskList.ToArray());
        }

        private async Task ResourceLanguageFileAmazonSave(long id, ResourceRequest resourceRequest, bool isNew = false)
        {
            var taskList = new List<Task>();
            var deletedFiles = new List<string>();
            var spokenLanguageIds = new List<long>();
            if (resourceRequest.SpokenLanguages != null && resourceRequest.SpokenLanguages.Any())
                foreach (
                    var spokenLanguages in resourceRequest.SpokenLanguages.Where(x => x.Files != null && x.Files.Any()))
                {
                    spokenLanguageIds.Add(spokenLanguages.LanguageId);
                    foreach (var fileRequest in spokenLanguages.Files)
                    {
                        if (string.IsNullOrWhiteSpace(fileRequest.S3BucketUrl))
                            taskList.Add(FileUploadHelpers.CopyAsync(fileRequest.S3BucketName, "temp",
                                fileRequest.S3BucketName,
                                GetResourceLanguageS3Path(id, spokenLanguages.LanguageId)));
                        if (fileRequest.IsDelete)
                        {
                            deletedFiles.Add(GetResourceLanguageS3Path(id, spokenLanguages.LanguageId) + "/" +
                                             fileRequest.S3BucketName);
                        }
                    }
                }
            if (!isNew)
            {
                if (StorageService.UseAmazonStorage)
                {
                    foreach (
                        var directory in
                            FileUploadHelpers.GetS3DirectoryInfos(
                                string.Format(FileUploadHelpers.ResourceLanguageFolderTemplate, id)).
                                Where(directory => !spokenLanguageIds.Contains(Convert.ToInt64(directory.Name))))
                    {
                        directory.Delete(true);
                    }
                }
                else
                {
                    var tmp =
                        FileUploadHelpers.GetDirectoryInfos(
                            string.Format(FileUploadHelpers.ResourceLanguageFolderTemplate, id)).ToList();
                    foreach (
                        var directory in tmp.Where(directory => !spokenLanguageIds.Contains(Convert.ToInt64(Path.GetFileName(directory)))))
                    {
                        Directory.Delete(directory, true);
                    }
                }

                if (deletedFiles.Any())
                    taskList.Add(FileUploadHelpers.S3DeleteFilesAsync(deletedFiles.ToArray()));

            }

            await Task.WhenAll(taskList.ToArray());
        }

        /// <summary>
        /// Method Description : Using for Get Hierarchical Resource by Resource Id.    
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <returns>Resource Hierarchical Response Model</returns>
        [Authorize]
        [HttpGet]
        [Route("api/Resources/GetChild")]
        public ResourceHierarchicalResponse GetHierarchicalResources(long resourceId)
        {
            // var parentResourceList = new List<ResourceHierarchicalRequest>();
            var parentResource = new ResourceHierarchicalRequest();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                parentResource = ctx.Resources.Where(x => x.Id == resourceId)
                                    .Select(x => new ResourceHierarchicalRequest
                                    {
                                        ResourceName = x.FirstName,
                                        ResourceFullName = (x.LastName + " " + x.FirstName),
                                        ResourceId = x.Id,
                                        JobTitle = x.JobTitle.Name
                                    }).FirstOrDefault();

                parentResource.Children = GetResourceChildren(parentResource.ResourceId, 0).ToList();
            }

            var ViewModel = Mapper.Map<ResourceHierarchicalRequest, ResourceHierarchicalResponse>(parentResource);
            return ViewModel;
        }

        public IEnumerable<ResourceHierarchicalResponse> GetResourceChildren(long? resourceId, long? previousresourceID)
        {
            List<ResourceHierarchicalResponse> alllist;
            List<ResourceHierarchicalResponse> listRemove = new List<ResourceHierarchicalResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                alllist = ctx.Resources.Where(x => x.SupervisorId == resourceId)
                                        .Select(x => new ResourceHierarchicalResponse
                                        {
                                            ResourceName = x.FirstName,
                                            ResourceFullName = (x.LastName + " " + x.FirstName),
                                            ResourceId = x.Id,
                                            JobTitle = x.JobTitle.Name
                                        }).ToList();

                foreach (ResourceHierarchicalResponse childResource in alllist)
                {
                    if (childResource.ResourceId != previousresourceID)
                    {
                        childResource.Children = GetResourceChildren(childResource.ResourceId, resourceId);
                    }
                    else
                    {
                        listRemove.Add(childResource);
                    }
                }
                if (listRemove.Count > 0)
                {
                    foreach (var item in listRemove)
                    {
                        alllist.Remove(item);
                    }
                }

            }
            return alllist;
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Resource.      
        /// </summary>
        /// <param name="additionalParameter">Additional Parameter</param>
        /// <param name="search">Search Value</param>
        /// <param name="showInactive">show Inactive</param>
        /// <returns>Resource Entity</returns>
        [Authorize]
        [HttpGet]
        [Route("api/Resources/autocomplete")]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search, [FromUri]string additionalParameter = "", [FromUri]bool? showInactive = null)
        {
            var filter = new FilterQuery<Resource>();
            dynamic data = System.Web.Helpers.Json.Decode(additionalParameter);
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => (x.LastName + " " + x.FirstName).Contains(search.ToLower()));
            }
            if (showInactive == null || showInactive == false)
            {
                filter.AddFilter(x => x.Active == true);
            }

            var autocompleteResult = await _resourceRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<Resource[], PicklistResponse[]>((autocompleteResult as IEnumerable<Resource>).OrderBy(x => x.Name).ToArray());

            return Ok(result);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Resource skill by Resource Id.      
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <returns>Picklist Response With Skills</returns>
        [HttpGet]
        [Route("api/Resources/skill-autocomplete")]
        public async Task<IHttpActionResult> Autocomplete([FromUri]long resourceId)
        {
            var result = new PicklistResponseWithSkills[0];
            if (resourceId != 0)
            {
                var skillList = await _resourceSkillRepository.ListAsync(new FilterQuery<ResourceSkill>().AddFilter(x => x.ResourceId == resourceId));
                result = Mapper.Map<ResourceSkill[], PicklistResponseWithSkills[]>(skillList.ToArray());
            }
            return Ok(result);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Resource Time Zone.      
        /// </summary>
        /// <returns>Time Zone Data Model</returns>
        [HttpGet]
        [Route("api/Resources/GetTimeZones")]
        public IHttpActionResult GetTimeZoneList()
        {
            //var response = TimeZoneInfo.GetSystemTimeZones().Select(x => new { name = x.DisplayName, id = x.Id }).ToList().OrderBy(x => x.name);

            List<TimeZoneData> timezone = TimeZoneData.GetTimeZoneData();

            return Ok(timezone);
        }

        /// <summary>
        /// Method Description : Using for Upload Photo.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Resources/UploadPhoto")]
        public async Task<IHttpActionResult> UploadPhoto(long id)
        {
            //var statusCodeResult = GetStatusCodeError(Crud.Update, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            if (!_resourceRepository.Exists(FilterQuery.Create<Resource>().AddFilter(p => p.Id == id)))
            {
                ModelState.AddModelError("id", "The resource with provided Id doesn't exists");
                return BadRequest(ModelState);
            }

            var result = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());
            var originalFileName = result.Files.First().Headers.ContentDisposition.FileName;
            var ext = Path.GetExtension(JsonConvert.DeserializeObject(originalFileName).ToString());
            var newfileName = string.Format(FileUploadHelpers.ResourcePhotoFileNameTemplate, id, ext).ToLower();

            var file = result.Files[0];
            var fileStream = await file.ReadAsStreamAsync();

            var wasS3UploadSuccessful = await FileUploadHelpers.S3ResourcePhotoUploadAsync(newfileName, fileStream);

            if (!wasS3UploadSuccessful) return BadRequest();
            var resource = _resourceRepository.Read(id);
            resource.PhotoFileName = newfileName;
            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Upload File.   
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Resources/UploadFile")]
        public async Task<IHttpActionResult> UploadFile()
        {
            //var statusCodeResult = GetStatusCodeError(Crud.Update, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            var result = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());
            var originalFileName = result.Files.First().Headers.ContentDisposition.FileName;
            originalFileName = originalFileName.Substring(1, originalFileName.Length - 2);
            var newfileName = string.Format(FileUploadHelpers.FileNameTemplate, Guid.NewGuid(), originalFileName).ToLower();

            var file = result.Files[0];
            var fileStream = await file.ReadAsStreamAsync();

            var wasS3UploadSuccessful = await FileUploadHelpers.S3UploadAsync(newfileName, fileStream, "temp");

            if (wasS3UploadSuccessful)
            {
                return Ok(new
                {
                    FileName = newfileName
                });
            }
            return BadRequest();
        }

        /// <summary>
        /// Method Description : Using for Upload CV.   
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Resources/UploadCv")]
        public async Task<IHttpActionResult> UploadCv(long id)
        {
            //var statusCodeResult = GetStatusCodeError(Crud.Update, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            if (!_resourceRepository.Exists(FilterQuery.Create<Resource>().AddFilter(p => p.Id == id)))
            {
                ModelState.AddModelError("id", "The resource with provided Id doesn't exists");
                return BadRequest(ModelState);
            }

            var result = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());
            var originalFileName = result.Files.First().Headers.ContentDisposition.FileName;
            var ext = Path.GetExtension(JsonConvert.DeserializeObject(originalFileName).ToString());
            //var newfileName = string.Format(FileUploadHelpers.CvFileNameTemplate, id, ext).ToLower();
            var resource = _resourceRepository.Read(id);
            var newfileName = resource.Id + "_" + resource.FirstName + resource.LastName + "_Resume" + ".pdf";

            var file = result.Files[0];
            var fileStream = await file.ReadAsStreamAsync();

            var wasS3UploadSuccessful = await FileUploadHelpers.S3UploadAsync(newfileName, fileStream);

            if (!wasS3UploadSuccessful) return BadRequest();
            //var resource = _resourceRepository.Read(id);
            resource.CvFileName = newfileName;
            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Export by Id.   
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Resources/export/")]
        public async Task<HttpResponseMessage> Export(long id)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return new HttpResponseMessage(statusCodeResult.StatusCode);
            //}
            //var resource = await _resourceService.ReadAsync(id, roleInfo.AccessRights.RestrictAccess);
            var resource = await _resourceRepository.ReadAsync(id, readOptions: _readOptions);
            var resourceDto = Mapper.Map<Resource, ResourceResponse>(resource);
            var resourcesResponse = new List<ResourceResponse>();
            resourcesResponse.Add(resourceDto);
            var stream = ResourceExport.Export(resourcesResponse, string.Format("{0}", resource.Name)) as MemoryStream;

            var message = new HttpResponseMessage(HttpStatusCode.OK)
            {
                // ReSharper disable once PossibleNullReferenceException
                Content = new ByteArrayContent(stream.ToArray())
            };
            message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "EdzResourceExport.xlsx"
            };

            return message;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resumeRequest">Resume Download Request Data</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        [Route("api/Resources/export_resume_download")]
        public async Task<HttpResponseMessage> ExportResourceResumeDownload(ResumeDownloadRequestData resumeRequest)
        {
            long id = resumeRequest.Id;

            var resource = await _resourceRepository.ReadAsync(id, readOptions: _readOptions);
            // string s = form["test"];

            var message = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(getPDFBytes(resumeRequest, resource))
            };
            message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = resource.Id + "_" + resource.FirstName + resource.LastName + "_Resume" + ".pdf"
            };

            return message;
        }

        private byte[] getPDFBytes(ResumeDownloadRequestData resourceRequest, Resource resource)
        {
            string name = resource.LastName + " " + resource.FirstName;
            string position = resource.JobTitle.Name;
            string experiencedata = (resourceRequest.ExperienceData != "") ? resourceRequest.ExperienceData : "";
            string yearexperiencedata = (resourceRequest.YearExperienceData != "") ? resourceRequest.YearExperienceData : "";

            #region New Colored Resume Pdf Code 
            StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Templates/PdfTemplates/PdfTemplateResume.html"));
            string readFile = reader.ReadToEnd();
            string StrContent = "";

            StrContent = readFile;
            StringBuilder sb = new StringBuilder();

            #region Coman Section for Converting Pdf
            StrContent = StrContent.Replace("[name]", name);
            StrContent = (experiencedata != null && experiencedata != "") ? StrContent.Replace("[experiencedata]", " - " + experiencedata) : StrContent.Replace("[experiencedata]", "");
            StrContent = (yearexperiencedata != null && yearexperiencedata != "") ? StrContent.Replace("[yearexperiencedata]", yearexperiencedata) : StrContent.Replace("[yearexperiencedata]", "");
            StrContent = StrContent.Replace("[position]", position);

            StringBuilder sbBasicDetail = new StringBuilder();

            string address = ((resource.Address == null || resource.Address == "") ? string.Empty : resource.Address + ", ")
                            + ((resource.Address2 == null || resource.Address2 == "") ? string.Empty : resource.Address2 + ", ")
                            + ((resource.City == null) ? string.Empty : resource.City.Name + ", ")
                            + ((resource.State == null) ? string.Empty : resource.State.Name + ", ")
                            + ((resource.Country == null) ? string.Empty : resource.Country.Name + ", ")
                            + ((resource.ZipCode == null) ? string.Empty : " -" + resource.ZipCode);
            string addressDetail = string.IsNullOrEmpty(Convert.ToString(address)) ? string.Empty : address;

            string phone = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(resource.MobilePhoneNumber)))
            {
                string phmask = string.Empty;
                string ph = Convert.ToString(resource.MobilePhoneNumber);
                phmask = "(" + ph.Substring(0, 3) + ") " + ph.Substring(3, 3) + "-" + ph.Substring(6, 4);
                phone = (phmask != null || phmask != "") ? ("Phone : " + phmask) : string.Empty;
            }

            string email = string.Empty;
            email = (string.IsNullOrEmpty(Convert.ToString(resource.Email)) ? string.Empty : ("Email : " + resource.Email));
            if (addressDetail != string.Empty || phone != string.Empty || email != string.Empty)
            {
                sbBasicDetail.Append("<table class='format-table' width='100' cellspacing='0' cellpadding='2' style='padding-top:8px'>");
                if (addressDetail != string.Empty)
                    sbBasicDetail.AppendFormat("<tr><td colspan ='2'>{0}</td></tr>", addressDetail);
                if (phone != string.Empty)
                    sbBasicDetail.AppendFormat("<tr><td colspan ='2'>{0}</td></tr>", phone);
                if (email != string.Empty)
                    sbBasicDetail.AppendFormat("<tr><td colspan ='2'>{0}</td></tr>", email);
                sbBasicDetail.Append("</table>");
            }

            StrContent = StrContent.Replace("[basicDetail]", sbBasicDetail.ToString());

            #endregion

            #region MyRegion
            //#overview-section
            sb = new StringBuilder();
            if (resource.Overview != null && resource.Overview.Length > 0)
            {
                sb.AppendFormat("<div class='div-h4'><h4 class='sect-head'>Overview</h4></div><div id='overview-section'><div class='grid-table'><table><tr><th>Overview</th></tr>");
                DataTable educationDataTable = new DataTable();
                // educationDataTable.Columns.AddRange(new DataColumn[1] { new DataColumn("Education") });
                sb.Append("<tr><td style='vertical-align:top;text-align:justify'>" + resource.Overview + " </td></tr>");
                sb.Append("</table></div></div>");
                StrContent = StrContent.Replace("[overview]", sb.ToString());
            }
            else
            {
                StrContent = StrContent.Replace("[overview]", "");
            }
            #endregion

            #region Education 
            sb = new StringBuilder();
            if (resourceRequest.Educations != null && resourceRequest.Educations.Length > 0)
            {
                sb.AppendFormat("<div class='div-h4'><h4 class='sect-head'>Educations</h4></div><div id='education-section'><div class='grid-table'><table><tr><th>Degree</th><th>Designation</th><th>Name Of School</th><th>Date Obtained</th></tr>");
                DataTable educationDataTable = new DataTable();
                educationDataTable.Columns.AddRange(new DataColumn[1] { new DataColumn("Education") });
                foreach (var educationId in resourceRequest.Educations)
                {
                    foreach (var education in resource.Educations)
                    {
                        if (education.Id == educationId)
                        {
                            string dateObtainedValue = "";
                            if (resourceRequest.IsForStinson == true)
                                dateObtainedValue = String.Format("{0:yyyy}", education.DateObtained);
                            else
                                dateObtainedValue = String.Format("{0:MM/dd/yyyy}", education.DateObtained);
                            sb.Append("<tr><td valign='top'>" + education.Subject + " </td><td valign='top'>" + ((education.ResourceDesignation == null) ? string.Empty : education.ResourceDesignation.Name) + "</td> <td valign='top'>" + education.NameOfSchool + "</td><td valign='top'>" + dateObtainedValue + "</td></tr>");
                        }
                    }
                }
                sb.Append("</table></div></div>");
                StrContent = StrContent.Replace("[education]", sb.ToString());
            }
            else
            {
                StrContent = StrContent.Replace("[education]", "");
            }

            #endregion

            #region Skills 
            sb = new StringBuilder();
            if (resourceRequest.Skills != null && resourceRequest.Skills.Length > 0)
            {
                sb.AppendFormat("<div class='div-h4'><h4 class='sect-head'>Skills</h4></div><div id='skills-section'><div class='grid-table'><table><tr><th>Skill</th><th>Year Of Experience</th><th>Proficiency</th></tr>");
                foreach (var skillId in resourceRequest.Skills)
                {
                    foreach (var skill in resource.Skills)
                    {
                        if (skill.Id == skillId)
                        {
                            //sb.Append("<tr><td style='vertical-align:top;'>" + ((skill.Skill == null) ? string.Empty : skill.Skill.Name) + "</td><td style='vertical-align:top;'>" + skill.Description + "</td><td style='vertical-align:top;'>" + skill.YearsOfExpirience + "</td><td style='vertical-align:top;'>" + skill.Proficiency + "</td></tr>");
                            sb.Append("<tr style='background-color:#e4e4e4;'>");
                            sb.AppendFormat("	<td style='padding-bottom:2px;padding-top:2px;'>{0}</td>", ((skill.Skill == null) ? string.Empty : skill.Skill.Name));// skill name 
                            sb.AppendFormat("	<td style='padding-bottom:2px;padding-top:2px;'>{0}</td>", skill.YearsOfExpirience);// Year Of Experience
                            sb.AppendFormat("	<td style='padding-bottom:2px;padding-top:2px;'>{0}</td>", skill.Proficiency);// Proficiency
                            sb.Append("</tr>");
                            sb.Append("<tr>");
                            if (skill.Description != null && skill.Description != "")
                            {
                                if (skill.Description.Length > 0)
                                {
                                    sb.Append("	<td colspan='3' style='padding-bottom:10px;text-align:justify;padding-left:30px;'>");
                                    sb.AppendFormat("		<span style='color:black;font-weight:bold;padding-top:8px;'>Description : </span><br />{0}<br />", skill.Description);
                                    sb.Append("	</td>");
                                }
                            }
                            sb.Append("</tr> ");
                        }
                    }
                }
                sb.Append("</table></div></div>");
                StrContent = StrContent.Replace("[skills]", sb.ToString());
            }
            else
            {
                StrContent = StrContent.Replace("[skills]", "");
            }

            #endregion

            #region Certifications
            sb = new StringBuilder();
            if (resourceRequest.Certificates != null && resourceRequest.Certificates.Length > 0)
            {
                string issueDateTitle = "";
                if (resourceRequest.IsForStinson == true)
                    issueDateTitle = "Bar Admit Date";
                else
                    issueDateTitle = "Issue Date";

                sb.AppendFormat("<div class='div-h4'><h4 class='sect-head'>Certificates</h4></div><div id='certifications-section'><div class='grid-table'><table><tr><th>Certificate</th><th>State</th><th>" + issueDateTitle + "</th><th>Expiration Date</th></tr>");

                foreach (var certificateId in resourceRequest.Certificates)
                {
                    foreach (var certificate in resource.Certificates)
                    {
                        if (certificate.Id == certificateId)
                        {
                            string issueDateValue = "";
                            if (resourceRequest.IsForStinson == true)
                                issueDateValue = String.Format("{0:yyyy}", certificate.IssueDate);
                            else
                                issueDateValue = String.Format("{0:MM/dd/yyyy}", certificate.IssueDate);

                            sb.Append("<tr><td style='vertical-align:top;'>" + certificate.Name + "</td><td style='vertical-align:top;'>" + ((certificate.CertificateState == null) ? string.Empty : certificate.CertificateState.Name) + "</td><td style='vertical-align:top;'>" + issueDateValue + "</td><td>" + String.Format("{0:MM/dd/yyyy}", certificate.ExpirationDate) + "</td></tr>");
                        }
                    }
                }
                sb.Append("</table></div></div>");
                StrContent = StrContent.Replace("[certifications]", sb.ToString());
            }
            else
            {
                StrContent = StrContent.Replace("[certifications]", "");
            }

            #endregion

            #region Previous Employement
            sb = new StringBuilder();
            if (resourceRequest.PreviousEmployments != null && resourceRequest.PreviousEmployments.Length > 0)
            {
                sb.AppendFormat("<div class='div-h4'><h4 class='sect-head'>Previous Employements</h4></div><div id='previousemployement-section'><div class='grid-table'><table><tr><th>Company Name / Title of Position</th><th>Date</th></tr>");
                foreach (var previousEmployementId in resourceRequest.PreviousEmployments)
                {
                    foreach (var previousEmployement in resource.PreviousEmployments)
                    {
                        if (previousEmployement.Id == previousEmployementId)
                        {
                            sb.Append("<tr><td style='vertical-align:top;text-align:justify'>" + previousEmployement.Name + "/" + previousEmployement.TitleOfPosition + "</td><td style='vertical-align:top;'>" + String.Format("{0:MM/dd/yyyy}", previousEmployement.StartDate) + " - " + String.Format("{0:MM/dd/yyyy}", previousEmployement.EndDate) + "</td></tr>");
                        }
                    }
                }
                sb.Append("</table></div></div>");
                StrContent = StrContent.Replace("[previousemployement]", sb.ToString());
            }
            else
            {
                StrContent = StrContent.Replace("[previousemployement]", "");
            }

            #endregion

            #region Experience
            sb = new StringBuilder();
            if (resourceRequest.ResourceWorkExperiences != null && resourceRequest.ResourceWorkExperiences.Length > 0)
            {
                sb.AppendFormat("<div class='div-h4'><h4 class='sect-head'>Work Experiences</h4></div><div id='workexperience-section'><div class='grid-table'><table><tr><th>Position</th><th>Date</th></tr>");
                //sb.AppendFormat("<div class='div-h4'> <h4 class='sect-head'>Work Experiences</h4> </div> <div id='workexperience-section' style='padding-top:8px;'> <div class='grid-table'> <table> <tr style='background-color:transparent;'> <th width='90%' style='text-align:left;'> Position </th> <th width='5%' style='text-align:right; padding-right:5px; font-weight:normal;'> Date </th></tr>");
                foreach (var workExperienceId in resourceRequest.ResourceWorkExperiences)
                {
                    foreach (var workExperience in resource.ResourceWorkExperiences)
                    {
                        if (workExperience.Id == workExperienceId)
                        {
                            string experienceDurationString = (workExperience.StartDate != null) ? (string.Format("{0:MM/dd/yyyy}", workExperience.StartDate) + "&nbsp;-&nbsp;" + ((workExperience.EndDate == null) ? "Current" : (string.Format("{0:MM/dd/yyyy}", workExperience.EndDate)))) : "-";
                            sb.Append("<tr style='background-color: #e4e4e4;'> <td>" + ((workExperience.WorkExperienceJobTitle != null) ? workExperience.WorkExperienceJobTitle.Name : string.Empty) + "</td> <td> <span>" + experienceDurationString + "</span> </td> </tr>");
                            if (workExperience.ResourceWorkAssignments != null && workExperience.ResourceWorkAssignments.Count > 0)
                            {
                                if (workExperience.ResourceWorkAssignments.Count > 0)
                                {
                                    sb.Append("<tr><td colspan='2' style='color:black;font-weight:bold;padding-top:8px;padding-left:30px;'>Assignments</td></tr>");
                                }
                                //sb.Append("<tr><td colspan='2' style='border-width:0'>&nbsp;</td></tr>");
                                foreach (var assignment in workExperience.ResourceWorkAssignments)
                                {
                                    sb.Append("<tr> <td colspan='2' style='padding-left:30px;'>");
                                    sb.Append("<div style='padding-bottom:8px;font-size:14px;'> ");
                                    //Duration
                                    string durationString = (assignment.StartDate != null) ? (string.Format("{0:MM/dd/yyyy}", assignment.StartDate) + "&nbsp;-&nbsp;" + ((assignment.EndDate == null) ? "Current" : (string.Format("{0:MM/dd/yyyy}", assignment.EndDate)))) : "-";
                                    sb.Append("<span style='padding-right:15px'><strong style='padding-right:8px;'> Duration </strong> <span> " + durationString + "</span> </span> ");
                                    if (resourceRequest.IsForStinson == true)
                                    {
                                        //Of Note                                    
                                        sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;<span style='padding-right:15px'> <strong style='padding-right:8px;'>Of Note</strong> <span>" + ((assignment.OfNote == null || assignment.OfNote == false) ? "No" : "Yes") + "</span> </span> ");
                                        //Judicial Clerkship
                                        sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;<span style='padding-right:15px'> <strong style='padding-right:8px;'>Judicial Clerkship</strong> <span> " + ((assignment.JudicialClerkShip == null || assignment.JudicialClerkShip == false) ? "No" : "Yes") + "</span> </span> ");
                                    }
                                    sb.Append("</div>");
                                    //Descrption
                                    sb.Append("<div style='line-height:18px;font-size: 14px;padding-bottom:8px;text-align:justify'><span style='text-align:justify'>" + assignment.AssignmentDescription + "</span> </div> ");
                                    sb.Append("</td> </tr>");
                                }
                            }
                        }
                    }
                }

                sb.Append("</table> </div> </div>");
                StrContent = StrContent.Replace("[workexperience]", sb.ToString());
            }
            else
            {
                StrContent = StrContent.Replace("[workexperience]", "");
            }

            #endregion

            #region Language
            sb = new StringBuilder();
            if (resourceRequest.SpokenLanguages != null && resourceRequest.SpokenLanguages.Length > 0)
            {
                sb.AppendFormat("<div class='div-h4'><h4 class='sect-head'>Languages</h4></div><div id='language-section'><div class='grid-table'><table><tr><th>Language</th><th>Year Of Experience</th><th>Speaking Proficiency</th><th>Writing Proficiency</th></tr>");
                foreach (var languageId in resourceRequest.SpokenLanguages)
                {
                    foreach (var language in resource.SpokenLanguages)
                    {
                        if (language.Id == languageId)
                        {
                            sb.Append("<tr><td style='vertical-align:top;'>" + ((language.Language == null) ? string.Empty : language.Language.Name) + "</td><td style='vertical-align:top;'>" + language.YearsOfExpirience + "</td><td style='vertical-align:top;'>" + language.SpeakingProficiency + "</td><td style='vertical-align:top;'>" + language.WritingProficiency + "</td></tr>");
                        }
                    }
                }
                sb.Append("</table></div></div>");
                StrContent = StrContent.Replace("[languages]", sb.ToString());
            }
            else
            {
                StrContent = StrContent.Replace("[languages]", "");
            }


            #endregion

            #region ProfessionalOrganizations
            sb = new StringBuilder();
            if (resourceRequest.ProfessionalOrganizations != null && resourceRequest.ProfessionalOrganizations.Length > 0)
            {
                sb.AppendFormat("<div class='div-h4'><h4 class='sect-head'>Professional & &nbsp;Civic Activities</h4></div><div id='professionalorganizations-section'><div class='grid-table'><table><tr><th>Organization / Activity </th><th>Committee Membership</th><th>Committee Title</th></tr>");
                foreach (var ProfessionalOrganizationsId in resourceRequest.ProfessionalOrganizations)
                {
                    foreach (var professionalorganizations in resource.ResourceProfessionalOrganizations)
                    {
                        if (professionalorganizations.Id == ProfessionalOrganizationsId)
                        {
                            sb.Append("<tr><td style='vertical-align:top;text-align:justify'>" + professionalorganizations.Organization + "</td><td style='vertical-align:top;'>" + ((professionalorganizations.CommitteeMembership == null) ? string.Empty : professionalorganizations.CommitteeMembership.Name) + "</td><td style='vertical-align:top;'>" + ((professionalorganizations.CommitteeTitle == null) ? string.Empty : professionalorganizations.CommitteeTitle.Name) + "</td></tr>");
                        }
                    }
                }
                sb.Append("</table></div></div>");
                StrContent = StrContent.Replace("[professionalorganizations]", sb.ToString());
            }
            else
            {
                StrContent = StrContent.Replace("[professionalorganizations]", "");
            }
            #endregion

            reader.Close();
            return PdfUtils.genratePDF(StrContent, iTextSharp.text.PageSize.A4.Rotate());
            #endregion
        }

        private void addSectionData(StringBuilder sb, DataTable dataTable, String sectionTitle, String thRow)
        {
            if (sectionTitle != null && sectionTitle.Length > 0)
            {
                sb.AppendFormat("<h4 style='display:block;height:40px;border:1px solid'>{0}</h4>", sectionTitle);
                sb.Append("<br />");

            }
            sb.Append("<table>");

            sb.Append(thRow);

            foreach (DataRow row in dataTable.Rows)
            {
                sb.Append("<tr>");
                foreach (DataColumn column in dataTable.Columns)
                {
                    sb.AppendFormat("<td><h5>{0}</h5></td>", row[column]);
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            sb.Append("<br />");
        }

        /// <summary>
        /// Method Description : Using for Export All.   
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Resources/export-all/")]
        public async Task<HttpResponseMessage> ExportAll()
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return new HttpResponseMessage(statusCodeResult.StatusCode);
            //}

            //var resources = await _resourceService.ReadAsync(roleInfo.AccessRights.RestrictAccess);

            var resources = await _resourceRepository.ListAsync(readOptions: _readOptions);
            var resourceDtos = Mapper.Map<Resource[], ResourceResponse[]>(resources.ToArray());

            var resourcesResponse = new List<ResourceResponse>();
            resourcesResponse.AddRange(resourceDtos);
            var stream = ResourceExport.Export(resourcesResponse, "All resources") as MemoryStream;

            var message = new HttpResponseMessage(HttpStatusCode.OK)
            {
                // ReSharper disable once PossibleNullReferenceException
                Content = new ByteArrayContent(stream.ToArray())
            };
            message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "EdzAllResourcesExport.xlsx"
            };

            return message;
        }

        /// <summary>
        /// Method Description : Using for Import.   
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("api/Resources/import")]
        public async Task<IHttpActionResult> Import()
        {
            //var statusCodeResult = GetStatusCodeError(Crud.Update, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            var data = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());
            var excelFile = await GetExcelPackage(data);

            var resourceDtos = new List<ResourceImportDto>();
            var importResult = ResourceImport.Import(excelFile, resourceDtos);

            switch (importResult)
            {
                case ImportResult.Success:
                    {
                        var result = await ResourceImport.StoreAsync(resourceDtos, _resourceService,
                            _divisionRepository,
                            _departmentRepository,
                            _countryRepository,
                            _resourceCompanyRepository,
                            _jobTitleRepository,
                            _cityRepository,
                             _stateRepository,
                            _resourceTeamRepository,
                            _resourceRepository);
                        if (result.Item1 == StoreResult.Success)
                        {
                            return Ok(result.Item2);
                        }
                        //ModelState.AddModelError("Storing", "The file which you tried to import as a resource is invalid. Check it please and try again");
                        ModelState.AddModelError("Storing", result.Item2.ToString());
                        return BadRequest(ModelState);
                    }
                case ImportResult.NoData:
                    {
                        ModelState.AddModelError("Import", "No data in excel file");
                        return BadRequest(ModelState);
                    }
                case ImportResult.WrongFormat:
                    {
                        ModelState.AddModelError("Import", "Excel file has wrong format");
                        return BadRequest(ModelState);
                    }
                case ImportResult.WrongData:
                    {
                        ModelState.AddModelError("Import", "Excel file has wrong data");
                        return BadRequest(ModelState);
                    }
            }

            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Set Inactive by Respurce Id.   
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("api/Resources/{respurceId}/set-inactive")]
        public async Task<IHttpActionResult> SetInactive(long respurceId)
        {

            var roleInfo = GetRoleAccessRights();
            var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Update, Section.Resources);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var resource = await _resourceRepository.ReadAsync(respurceId);

            resource.EndDate = DateTime.Now;

            await _resourceService.UpdateAsync(resource, roleInfo.AccessRights.RestrictAccess);


            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Get Geo Names For Zipcode.   
        /// </summary>
        /// <returns>Postal Code Response Model</returns>
        [Authorize]
        [HttpGet]
        [Route("api/Resources/get-geo-names-for-zipcode")]
        public async Task<IHttpActionResult> GetGeoNamesForZipcode(string zipCode = null)
        {
            if (zipCode != null && zipCode != "")
            {
                PostalCodeResponse postalCode = new PostalCodeResponse();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                    var query = await (from code in ctx.PostalCodes.AsNoTracking()
                                       join db_city in ctx.Cities.AsNoTracking() on code.CityId equals db_city.Id into city
                                       join db_state in ctx.States.AsNoTracking() on code.StateId equals db_state.Id into state
                                       join db_country in ctx.Countries.AsNoTracking() on code.CountryId equals db_country.Id into country

                                       from db_city in city.DefaultIfEmpty()
                                       from db_state in state.DefaultIfEmpty()
                                       from db_country in country.DefaultIfEmpty()

                                       where (code.ZipCode == zipCode)
                                       select new PostalCodeResponse
                                       {
                                           Id = code.Id,
                                           ZipCode = code.ZipCode,
                                           City = city.Select(c => new CodeResponse { Id = db_city.Id, Name = db_city.Name, StateCode = db_city.StateCode }).FirstOrDefault(),
                                           State = state.Select(c => new CodeResponse { Id = db_state.Id, Name = db_state.Name, StateCode = db_state.StateCode }).FirstOrDefault(),
                                           Country = country.Select(c => new CodeResponse { Id = db_country.Id, Name = db_country.Name, StateCode = null }).FirstOrDefault(),
                                       }).FirstOrDefaultAsync();

                    postalCode = query;
                    if (postalCode != null)
                    {
                        if (postalCode.City != null)
                        {
                            postalCode.City.StateId = await ctx.Cities.AsNoTracking().Where(x => x.Id == postalCode.City.Id).Select(x => x.StateId).FirstOrDefaultAsync();
                            postalCode.City.StateName = await ctx.States.AsNoTracking().Where(x => x.Id == postalCode.City.StateId).Select(y => y.Name).FirstOrDefaultAsync();
                        }
                    }
                    return Ok(postalCode);
                }
            }
            else
            {
                ModelState.AddModelError("Resource", "Postal code is wrong.");
                return BadRequest(ModelState);
            }
        }

        private string GetOriginalFileNameWithoutBucketGuid(string bucketName)
        {
            var fileName = Path.GetFileName(bucketName);
            return fileName != null ? fileName.Substring(36) : string.Empty;
        }

        private string GetResourceLanguageS3Path(long resourceId, long languageId)
        {
            return string.Format(FileUploadHelpers.ResourceLanguageTemplate, resourceId, languageId);
        }

        private string GetResourceCustomFieldS3Path(long resourceId)
        {
            return string.Format(FileUploadHelpers.ResourceCustomFieldFilesTemplate, resourceId);
        }

        private static Expression<Func<Resource, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return resource => string.Concat(resource.FirstName, " ", resource.LastName).Contains(searchPhrase)
                || resource.JobTitle.Name.Contains(searchPhrase)
                || resource.Division.Name.Contains(searchPhrase)
                || resource.Department.Name.Contains(searchPhrase)
                || resource.Location.Contains(searchPhrase)
                || resource.CustomFieldValues.Where(c => c.CustomField.IsSearchable).Select(x => x.Value).Any(y => y.Contains(searchPhrase));
        }

        private static Expression<Func<Resource, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return resource => string.Concat(resource.FirstName, " ", resource.LastName).Contains(searchPhrase);
        }

        /// <summary>
        /// Description : Resume Download Request Data Class.    
        /// </summary>
        public class ResumeDownloadRequestData
        {
            public long Id { get; set; }
            public long[] Educations { get; set; }
            public long[] Skills { get; set; }
            public long[] Certificates { get; set; }
            public long[] PreviousEmployments { get; set; }
            public long[] ResourceWorkExperiences { get; set; }
            public long[] SpokenLanguages { get; set; }
            public long[] ProfessionalOrganizations { get; set; }
            public string ExperienceData { get; set; }
            public string YearExperienceData { get; set; }
            public bool IsForStinson { get; set; }
        }

        private Resource ApplyRestrictions(IEnumerable<RestrictAccessTo> restrictAccess, Resource currentResource, Resource originResource)
        {
            foreach (var restrictAccessTo in restrictAccess)
            {
                var restrictFunc = _restrictAccessFuncMap[restrictAccessTo];
                if (restrictFunc != null)
                {
                    restrictFunc(currentResource, originResource);
                }
            }
            return currentResource;
        }

        private readonly Dictionary<RestrictAccessTo, Action<Resource, Resource>> _restrictAccessFuncMap = new Dictionary<RestrictAccessTo, Action<Resource, Resource>>
        {
            {
                RestrictAccessTo.CriminalRecords,
                (current, origin) =>
                {
                    current.CriminalRecords = origin.CriminalRecords;
                }
            },
            {
                RestrictAccessTo.PreviousEmployments,
                (current, origin) =>
                {
                    current.PreviousEmployments = origin.PreviousEmployments;
                }
            },
            {
                RestrictAccessTo.Salary,
                (current, origin) =>
                {
                    current.HourlyRate = origin.HourlyRate;
                    current.Salary = origin.Salary;
                    current.BonusOrOtherPay = origin.BonusOrOtherPay;
                }
            },
            {
                RestrictAccessTo.PersonalInfo,
                (current, origin) =>
                {
                    current.MartialStatus = origin.MartialStatus;
                    current.Ssn = origin.Ssn;
                    current.EmergencyContactName = origin.EmergencyContactName;
                    current.LGBT = null;
                    current.Ethnicity = null;
                    current.Gender = null;
                    current.EmergencyContactPhoneNumber = origin.EmergencyContactPhoneNumber;
                }
            },
            {
                RestrictAccessTo.BackgroundChecks,
                (current, origin) =>
                {
                    current.ArmedForces = origin.ArmedForces;
                    current.ArmedForcesBranch = origin.ArmedForcesBranch;
                    current.ArmedForcesCountryId = origin.ArmedForcesCountryId;
                    current.DrugTest = origin.DrugTest;
                    current.CreditReportOk = origin.CreditReportOk;
                    current.MotorVehicleReport = origin.MotorVehicleReport;
                    current.MotorVehicleReportOther = origin.MotorVehicleReportOther;
                    current.ProfessionalReferenceChecks = origin.ProfessionalReferenceChecks;
                    current.ProfessionalReferenceChecksOther = origin.ProfessionalReferenceChecksOther;
                    current.CredentialVerifications = origin.CredentialVerifications;
                    current.CredentialVerificationsOther = origin.CredentialVerificationsOther;
                    current.EmploymentEligibilityVerification = origin.EmploymentEligibilityVerification;
                    current.EmploymentEligibilityVerificationOther = origin.EmploymentEligibilityVerificationOther;
                    current.FormI9 = origin.FormI9;
                    current.FormI9Other = origin.FormI9Other;
                    current.FormEVerify = origin.FormEVerify;
                    current.FormEVerifyOther = origin.FormEVerifyOther;
                    current.InternationalWorkHistory = origin.InternationalWorkHistory;
                    current.InternationalWorkHistoryOther = origin.InternationalWorkHistoryOther;
                    current.FingerPrinting = origin.FingerPrinting;
                    current.FingerPrintingOther = origin.FingerPrintingOther;
                    current.DrugScreening = origin.DrugScreening;
                    current.DrugScreeningOther = origin.DrugScreeningOther;
                    current.CreditCheck = origin.CreditCheck;
                    current.CreditCheckOther = origin.CreditCheckOther;
                }
            }
        };

        private static Expression<Func<ApplicationUser, bool>> GetSearchColumnExpressions2(string searchPhrase)
        {
            return user => user.UserName.Contains(searchPhrase)
                || string.Concat(user.Resource.FirstName, " ", user.Resource.LastName).Contains(searchPhrase);
        }

    }
}
