"use strict";

(function DashboardCtrlIIFE() {

    angular.module("EDZoutstaffingPortalApp")
        .controller("DashboardCtrl", [
            "$scope",
            "DashboardResource",
            "dashboard",
            "Auth",
            "$rootScope",
            "$uibModal",
            DashboardCtrl
        ]);

    function DashboardCtrl($scope, DashboardResource, dashboard, Auth, $rootScope, $uibModal) {

        
        $scope.currentUser = Auth.getCurrentUser();
        $rootScope.$on("edz:success-login", function () {
            $scope.currentUser = Auth.getCurrentUser();
        });


        $scope.showAddDialog = function () {
            $uibModal.open({
                templateUrl: "Client/app/controllers/dashboard/add-widget/add-widget-dialog.html",
                controller: "AddWidgetCntrl",
                size: "lg",
                keyboard: false,
               
            });
        }


        var vm = this;
        vm.showDashboard = false;

        if (!(dashboard.id > 0)) {
            return;
        }
        vm.showDashboard = true;

        activate();

        ///////////////////////////////////////////////////////////////////////////
        function activate() {
            var dashboardData = JSON.parse(dashboard.setupJson);
            dashboardData.id = dashboard.id;
            vm.model = dashboardData;
            initWatchers();
        }

        function initWatchers() {
            $scope.$watch(function () {
                return vm.model;
            }, function (newValue, prevValue) {
                if (!_.isEqual(newValue, prevValue)) {
                    vm.model = newValue;
                    DashboardResource.saveDashboard(vm.model);
                }
            }, true);
        }

        /*
        $scope.toggleEditMode = function () {
            DashboardResource.saveDashboard(vm.model);
        }
        */
    }
})();