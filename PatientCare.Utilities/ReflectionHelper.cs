﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;

namespace PatientCare.Utilities
{
    public static class ReflectionHelper
    {
        public static MemberInfo FindProperty(LambdaExpression lambdaExpression)
        {
            var expression = lambdaExpression as Expression;
            var flag = false;
            while (!flag)
            {
                switch (expression.NodeType)
                {
                    case ExpressionType.Convert:
                        expression = ((UnaryExpression)expression).Operand;
                        break;
                    case ExpressionType.Lambda:
                        expression = ((LambdaExpression)expression).Body;
                        break;
                    case ExpressionType.MemberAccess:
                        MemberExpression memberExpression = (MemberExpression)expression;
                        if (memberExpression.Expression.NodeType != ExpressionType.Parameter && memberExpression.Expression.NodeType != ExpressionType.Convert)
                        {
                            throw new ArgumentException(
                                string.Format("Expression '{0}' must resolve to top-level member.", lambdaExpression),
                                "lambdaExpression");
                        }
                        return memberExpression.Member;
                    default:
                        flag = true;
                        break;
                }
            }
            return null;
        }

        public static string GetMemberName<T>(Expression<Func<T, object>> expression)
        {
            if (expression == null) { throw new ArgumentNullException("expression"); }

            var convertExpression = expression.Body as UnaryExpression;

            var memberExpression = (convertExpression != null ? convertExpression.Operand : expression.Body) as MemberExpression;

            if (memberExpression == null) { throw new ArgumentException("Expression body must be of type 'System.Linq.Expressions.MemberExpression' or System.Linq.Expressions.UnaryExpression"); }

            return memberExpression.Member.Name;
        }

        public static object GetValue(string property, object obj)
        {
            var pi = obj.GetType().GetProperty(property);
            return pi.GetValue(obj, null);
        }

        public static T GetAttributeForMember<T>(LambdaExpression lambdaExpression) where T : Attribute
        {
            var prop = FindProperty(lambdaExpression);

            var attrs = prop.GetCustomAttributes(typeof(T), true);
            if (attrs.Length > 0)
            {
                return (T)attrs[0];
            }
            return null;
        }

        public static bool TryResolvePropertyNameFromDataMemeberAttribute<T>(string propertyName, out string result)
        {
            if (propertyName == null) throw new ArgumentNullException("propertyName");

            result = null;
            var contractProperties = new List<string>();
            var propertyNames = propertyName.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            var containerType = typeof(T);

            foreach (var propName in propertyNames)
            {
                var members = containerType.GetMember(propName);
                if (members.Length <= 0)
                {
                    return false;
                }
                var member = members[0];
                var attrs = member.GetCustomAttributes(typeof(DataMemberAttribute), true);
                if (attrs.Length > 0)
                {
                    contractProperties.Add(((DataMemberAttribute)attrs[0]).Name);
                }
                else
                {
                    contractProperties.Add(member.Name);
                }
                if (member.MemberType == MemberTypes.Property)
                {
                    containerType = ((PropertyInfo)member).PropertyType;
                }
                else if (member.MemberType == MemberTypes.Field)
                {
                    containerType = ((FieldInfo)member).FieldType;
                }
                else
                {
                    throw new InvalidOperationException("Property names for type " + typeof(T).FullName + " should contain properties or fields only");
                }
            }
            result = string.Join(".", contractProperties);
            return true;
        }
    }
}
