﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ActivityCtrl", [
        "$scope", "$state", "ActivityService", "$translate", "$filter",
        "ProjectStakeHolderRole", "ProjectType", "ProjectPortfolio", "PermissionWorker", "DynamicDropDownConfigService",
        "activityPreLoadData",
        ActivityCtrl]);

function ActivityCtrl($scope, $state, activityService, $translate, $filter,
    projectStakeHolderRoleService, projectTypeService, projectPortfolioService, permissionWorkerService, dynamicDropDownConfigService,
    activityPreLoadData) {
    var self = this;
    self.activityModel = activityPreLoadData[0];
    self.activityName = self.activityModel.name || "Project";
    self.currentTab = "projectStackHolderRoles";

    self.tabClick = function (tab) {
        console.info(tab);
    }
    self.activityBackButtonClick = function () {
        $state.go("configurationData", { currentView: "projects" });
    }
    self.requestTransform = function (row) {
        console.info(row);
    }
    self.changeHeaderParams = function (data) {
        if (data.dropdowntype !== "taskComplexity" && data.dropdowntype !== "defectSeverity" && data.dropdowntype !== "impactOnBusiness") {
            data.activityTypeId = self.activityModel.id;
        }
    }

    self.tabList = [
        {
            id: "projectStackHolderRoles",
            name: self.activityName + " Stakeholder Roles",
            container: 'projectStackHolderRoles',
            service: {
                get: projectStakeHolderRoleService.query,
                delete: projectStakeHolderRoleService.delete,
                update: projectStakeHolderRoleService.update,
                save: projectStakeHolderRoleService.save,
                requestTransformCallback: function (row) {
                    row.activityTypeId = self.activityModel.id;
                }
            },
            tableConfigurations: defaultTabConfigurations("projectStackHolderRoles", self.activityName + " Stakeholder Roles", "ProjectConfigurationData"),
            required: true,
            visible: true,
        }, {
            id: "projectTypes",
            name: self.activityName + " " + $filter('translate')("Types"),
            container: 'projectTypes',
            service: {
                get: projectTypeService.query,
                delete: projectTypeService.delete,
                update: projectTypeService.update,
                save: projectTypeService.save,
                requestTransformCallback: function (row) {
                    row.activityTypeId = self.activityModel.id;
                }
            },
            tableConfigurations: defaultTabConfigurations("projectTypes", self.activityName + " " + $filter('translate')("Types"), "ProjectConfigurationData"),
            required: true,
            visible: true,
        }, {
            id: "projectPortfolio",
            name: self.activityName + " Portfolio",
            container: 'projectPortfolio',
            service: {
                get: projectPortfolioService.query,
                delete: projectPortfolioService.delete,
                update: projectPortfolioService.update,
                save: projectPortfolioService.save,
                requestTransformCallback: function (row) {
                    row.activityTypeId = self.activityModel.id;
                }
            },
            tableConfigurations: getProjectPortfolioConfigurations("projectPortfolio", self.activityName + " Portfolio", "ProjectConfigurationData"),
            required: true,
            visible: true,
        }, {
            id: "reasonCode",
            name: self.activityName + " Reason Code",
            container: 'reasonCode',
            service: defaultTabServices("reasonCode"),
            tableConfigurations: defaultTabConfigurations("reasonCode", self.activityName + " Reason Code", "ProjectConfigurationData"),
            required: true,
            visible: true,
        }, {
            id: "projectPriority",
            name: self.activityName + " Priority",
            container: 'projectPriority',
            service: defaultTabServices("projectPriority"),
            tableConfigurations: getProjectPriorityConfigurations("projectPriority", self.activityName + " Priority", "ProjectConfigurationData"),
            required: true,
            visible: true,
        }, {
            id: "impactOnBusiness",
            name: self.activityName + " Impact On Business",
            container: 'impactOnBusiness',
            service: defaultTabServices("impactOnBusiness"),
            tableConfigurations: defaultTabConfigurations("impactOnBusiness", self.activityName + " Impact On Business", "ProjectConfigurationData"),
            required: true,
            visible: true,
        }, ];

    self.getPageName = function () {
        return (self.activityName || "Project") + " Configurations";
    }

    function defaultTabServices(dropDownType) {
        return {
            get: dynamicDropDownConfigService.query,
            save: dynamicDropDownConfigService.save,
            update: dynamicDropDownConfigService.update,
            delete: dynamicDropDownConfigService.delete,
            requestTransformCallback: function (row) {
                row.dropDownType = dropDownType;
                row.displayName = (row.displayName) ? row.displayName : row.name;
                row.value = row.name;
                row.activityTypeId = self.activityModel.id;
            }
        }
    }


    function getProjectPriorityConfigurations(dropDownType, pageHeading) {
        return {
            pageName: "" + pageHeading,
            hideBulkEdit: true,
            dropdowntype: dropDownType,
            cols: function () {
                return [
                    {
                        field: "name",
                        title: self.activityName + " Priority",
                        sortable: "name",
                        show: true,
                        dataType: "text"
                    }, {
                        field: "configField1",
                        title: "Description",
                        sortable: "configField1",
                        show: true,
                        dataType: "text"
                    }, {
                        field: "configField2",
                        title: "Color Code",
                        sortable: "configField2",
                        show: true,
                        dataType: "color-picker",
                        dataTypeView: "color-picker",
                    }, {
                        field: "configField3",
                        title: "Order",
                        sortable: "configField3",
                        show: true,
                        dataType: "text",
                        dataTypeView: "text",
                    }, {
                        field: "action",
                        title: ".",
                        class: "blank-cell",
                        dataType: "command"
                    }
                ];
            },
        };
    }

    function getProjectPortfolioConfigurations(dropDownType, pageHeading, hasPermissionFlag) {
        return {
            pageName: pageHeading,
            hideBulkEdit: true,
            dropdowntype: dropDownType,
            cols: function () {
                return [
                    {
                        field: "name",
                        title: "Name",
                        sortable: "name",
                        show: true,
                        dataType: "text"
                    }, {
                        field: "action",
                        title: ".",
                        class: "blank-cell",
                        dataType: "command"
                    }
                ];
            }
        };
    }
    function defaultTabConfigurations(dropDownType, pageHeading, hasPermissionFlag) {

        function disabledAddButton(permissionFlag) {
            if ("ResourceConfigurationData" == permissionFlag)
                return !permissionWorkerService.canResourceConfigDataInsert();
            if ("ProjectConfigurationData" == permissionFlag)
                return !permissionWorkerService.canProjectConfigDataInsert();
            if ("FinanceConfigurationData" == permissionFlag)
                return !permissionWorkerService.canFinanceConfigDataInsert();
            if ("IntakeConfigurationData" == permissionFlag)
                return !permissionWorkerService.canIntakeConfigDataInsert();
            if ("GeneralConfigurationData" == permissionFlag)
                return !permissionWorkerService.canGeneralConfigDataInsert();
            return false;
        }
        function disabledDeleteButton(permissionFlag) {
            if ("ResourceConfigurationData" == permissionFlag)
                return permissionWorkerService.canResourceConfigDataDelete();
            if ("ProjectConfigurationData" == permissionFlag)
                return permissionWorkerService.canProjectConfigDataDelete();
            if ("FinanceConfigurationData" == permissionFlag)
                return permissionWorkerService.canFinanceConfigDataDelete();
            if ("IntakeConfigurationData" == permissionFlag)
                return permissionWorkerService.canIntakeConfigDataDelete();
            if ("GeneralConfigurationData" == permissionFlag)
                return permissionWorkerService.canGeneralConfigDataDelete();
            return false;
        }
        function disabledEditButton(permissionFlag) {
            if ("ResourceConfigurationData" == permissionFlag)
                return permissionWorkerService.canResourceConfigDataUpdate();
            if ("ProjectConfigurationData" == permissionFlag)
                return permissionWorkerService.canProjectConfigDataUpdate();
            if ("FinanceConfigurationData" == permissionFlag)
                return permissionWorkerService.canFinanceConfigDataUpdate();
            if ("IntakeConfigurationData" == permissionFlag)
                return permissionWorkerService.canIntakeConfigDataUpdate();
            if ("GeneralConfigurationData" == permissionFlag)
                return permissionWorkerService.canGeneralConfigDataUpdate();
            return false;
        }

        return {
            pageName: "" + pageHeading,
            hideBulkEdit: true,
            dropdowntype: dropDownType,
            disabledAddButton: disabledAddButton(hasPermissionFlag),
            disabledDeleteButton: disabledDeleteButton(hasPermissionFlag),
            disabledEditButton: disabledEditButton(hasPermissionFlag),
            cols: function () {
                return [
                    {
                        field: "name",
                        title: "Name",
                        sortable: "name",
                        show: true,
                        dataType: "text"
                    }, {
                        field: "action",
                        title: ".",
                        class: "blank-cell",
                        dataType: "command"
                    }
                ];
            }
        };
    }

}
