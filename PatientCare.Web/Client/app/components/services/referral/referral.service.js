﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("ReferralPhysicianService", [
        "$resource", "$state", "ReferralDialogFactory", function ($resource, $state, referralDialogFactory) {
            var resource = $resource("api/ReferralPhysicians/:id/:controller",
                null, //parameters default
                {
                    'update': { method: "PUT" },
                    'delete': { method: "DELETE" },
                    'query': { method: "GET", params: {}, isArray: false },
                });
            resource.reloadCommand = {};

            resource.getTableOption = {
                pageName: "Referral Physicians",
                hideBulkEdit: true,
                hideAddButton: true,
                showDropDownAddButton: true,
                cols: function () {
                    return [
                        { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                        {
                            field: "name",
                            title: "Name",
                            sortable: "name",
                            show: true,
                            dataType: "text",
                            required: true,
                        },
                        { field: "speciality", title: "Speciality", sortable: "speciality", show: true, dataType: "text", required: true },
                        { field: "medicalDesignation", title: "Medical Title", sortable: "medicalDesignation", show: true, dataType: "text", required: true },
                        { field: "facilityName", title: "Facility Name", sortable: "facilityName", show: true, dataType: "text", required: true },
                        { field: "city", title: "Location", sortable: "city", show: true, dataType: "text", required: true },
                        //{ field: "city", title: "Location", sortable: "city", show: true, dataType: "text", required: true, customViewText: function (row) { return (row.city) ? row.city.name : "" } },
                        { field: "referralsToDate", title: "Referrals to Date", sortable: "referralsToDate", show: true, dataType: "text", required: true },
                        {
                            field: "lastReferralReceived", title: "Last Referral Received", sortable: "lastReferralReceived", show: true, dataType: "text", required: true, customViewText: function (row) {
                                return (row.lastReferralReceived) ? moment(row.lastReferralReceived).format("MM/DD/YYYY") : "";
                            }
                        },
                        { field: "action", title: "", dataType: "command", class: "blank-cell" },
                    ];
                },
                onAdd_Click: function () {
                    $state.go("referralEdit");
                },
                onEdit_Click: function (row) {
                    referralDialogFactory.showPhysicianModal(row, function (data) {
                        if (data)
                            $state.go($state.current, {}, { reload: true });
                    });
                    //$state.go("referralEdit", { referralId: row.id });
                },
                dropDownAddButtonOptions: [{
                    title: "New Physician",
                    state: "patientsEdit",
                    onOptionClick: function () {
                        // $state.go("patientsEdit");
                        // Open Modal Dialog for New Physician
                        var temoEmptyArray = [];
                        referralDialogFactory.showPhysicianModal({}, function (data) {
                            if (data)
                                $state.go($state.current, {}, { reload: true });
                        });
                    }
                },/* {
                    title: "New Marketing",
                    state: "patientsEdit",
                    onOptionClick: function () {
                        //$state.go("patientsEdit");
                        // Open Modal Dialog for New Marketing
                        // referralDialogFactory
                    }
                },*/ ],
                headerButtons: [],
                menuButtons: []
            };
            return resource;
        }
    ]);