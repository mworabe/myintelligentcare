namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateFavouriteDetailLIstSP : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
                "dbo.sp_favourite_detail_list",
                p => new
                {
                pageIndex = p.Int(defaultValueSql: null),
                pageSize = p.Int(defaultValueSql: null),
                responseType = p.String(defaultValueSql: null),
                sortField = p.String(defaultValueSql: null),
                sortOrder = p.String(defaultValueSql: null),
                searchPhrase = p.String(defaultValueSql: null)
                },
                body:
             @" IF(@responseType = 'DataCount')
                BEGIN
                select CAST(Count(distinct Id) AS  BIGINT) 
                from master_templates 
                where master_templates.is_favorite = 1 and master_templates.is_deleted IS NULL	      
                END
                IF(@responseType = 'Data')
                BEGIN
                select maintable.Id,maintable.Name,maintable.IsFavorite,maintable.OwnerId,maintable.IsMaster,maintable.IsPublic,maintable.TemplateExercisesCount from (
                select Id,name as Name,owner_id as OwnerId,is_master as IsMaster,is_favorite as IsFavorite ,is_public as IsPublic,
                (select COUNT(Id) from master_template_exercises where master_template_id = master_templates.Id) as templateExercisesCount 
                from master_templates 
                where master_templates.is_favorite = 1 and master_templates.is_deleted IS NULL ) as maintable
                where (( maintable.Name like ('%'+@searchPhrase+'%'))
	                or (maintable.templateExercisesCount like ('%'+@searchPhrase+'%')))   

                order by 
                case
                when @sortOrder <> 'Ascending' then 0
                when @sortField = 'Id' then maintable.Id
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'Name' then maintable.Name
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'templateExercisesCount' then maintable.templateExercisesCount
                end ASC,

                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'templateExercisesCount' then maintable.templateExercisesCount
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'Name' then maintable.Name
                end DESC,
                case
                when @sortOrder <> 'Descending' then 0
                when @sortField = 'Id' then maintable.Id
                end DESC
                OFFSET(@pageIndex * @pageSize) ROWS FETCH NEXT @pageSize ROWS ONLY
                END"
                        );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_favourite_detail_list");
        }
    }
}
