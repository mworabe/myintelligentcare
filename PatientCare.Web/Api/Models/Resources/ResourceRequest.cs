﻿using System;
using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.ResourceCertificates;
using PatientCare.Web.Api.Models.ResourceCriminalRecords;
using PatientCare.Web.Api.Models.ResourceEducations;
using PatientCare.Web.Api.Models.ResourcePreviousEmployments;
using PatientCare.Web.Api.Models.ResourceSkills;
using PatientCare.Web.Api.Models.ResourceSpokenLanguages;
using PatientCare.Web.Api.Models.ResourceTravels;
using PatientCare.Web.Api.Models.ResourceWorkExperience;
using PatientCare.Web.Api.Models.ResourceProfessionalOrganizations;
using PatientCare.Web.Api.Models.ResourceBehaviouralDetails;
using PatientCare.Web.Api.Models.ResourceRecognitions;
using PatientCare.Web.Api.Models.ResourceIndustry;
using PatientCare.Web.Api.Models.ResourcePracticeAreas;
using PatientCare.Web.Api.Models.ResourceInsurances;
using PatientCare.Web.Api.Models.ResourceTimeoffBenefits;

namespace PatientCare.Web.Api.Models.Resources
{
    public class ResourceRequest
    {
        #region Personal Info Division
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }

        public string Overview { get; set; }

        public MartialStatusEnum? MartialStatus { get; set; }

        public GenderEnum? Gender { get; set; }

        public LGBTEnum? LGBT { get; set; }

        public string Ethnicity { get; set; }

        public string Prefix { get; set; }

        public string Suffix { get; set; }

        public bool? Active { get; set; }

        public string PreferredFirstname { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Address4 { get; set; }

        public string Address5 { get; set; }

        public string Address6 { get; set; }

        public string ZipCode { get; set; }

        public string ZipCodeTemp { get; set; }

        public long? CountryId { get; set; }

        public long? StateId { get; set; }

        public long? CityId { get; set; }

        public string TimekeeperNumber { get; set; }

        public long? ClinicId { get; set; }

        public long? ClinicLocationId { get; set; }

        #endregion

        #region Employement Detail Division

        public string EmployeeId { get; set; }

        public long? JobTitleId { get; set; }

        public DateTime? StartDate { get; set; }

        public string Ssn { get; set; }
        
        public DateTime? EndDate { get; set; }

        public string Location { get; set; }

        public string OfficeNumber { get; set; }

        public string Extension { get; set; }

        public long? SupervisorId { get; set; }

        public string TimeZoneInfo { get; set; }

        public string HomeNumber { get; set; }

        public long? DivisionId { get; set; }

        public RelationshipEnum? Relationship { get; set; }

        public string MobilePhoneNumber { get; set; }

        public long? DepartmentId { get; set; }

        public string EmergencyContactName { get; set; }

        public string EmergencyContactPhoneNumber { get; set; }

        #endregion

        #region Compensation and Benefits

        public decimal? FirmRate { get; set; }

        public decimal? NationalRate { get; set; }

        public EmploymentTypeEnum? EmploymentType { get; set; }

        public decimal? Salary { get; set; }

        public decimal? BonusOrOtherPay { get; set; }

        #endregion

        #region Education
        public string GradeLevel { get; set; }

        public int? YearsOfCollege { get; set; }

        #endregion

        #region Working Details
        public int? YearsEmployed { get; set; }        

        public bool? AvailableToTravel { get; set; }

        public bool? WorksWellInTeam { get; set; }

        public bool? ValidPassport { get; set; }

        public bool? WorksWellAlone { get; set; }

        #endregion

        #region Background Check
        public long? ArmedForcesCountryId { get; set; }

        public string ArmedForcesBranch { get; set; }

        public bool? ArmedForces { get; set; }

        public string DrugTest { get; set; }

        public string CreditReportOk { get; set; }

        public YesNoOtherEnum? MotorVehicleReport { get; set; }

        public YesNoOtherEnum? EmploymentEligibilityVerification { get; set; }

        public YesNoOtherEnum? InternationalWorkHistory { get; set; }

        public YesNoOtherEnum? ProfessionalReferenceChecks { get; set; }

        public YesNoOtherEnum? FormI9 { get; set; }

        public YesNoOtherEnum? FormEVerify { get; set; }

        public PassFailEnum? FingerPrinting { get; set; }
        public string FingerPrintingOther { get; set; }

        public PassFailEnum? DrugScreening { get; set; }
        public string DrugScreeningOther { get; set; }

        public PassFailEnum? CreditCheck { get; set; }
        public string CreditCheckOther { get; set; }

        #endregion

        #region Retirement Benefits

        public bool? RbParticipating { get; set; }
        public string RbEligibleWages { get; set; }
        public string RbEmployeeContribution { get; set; }
        public string RbEmployerMatching { get; set; }


        #endregion

        public long? BusinessUnitId { get; set; }

        public string CubicalOrOfficeNumber { get; set; }

        public bool? CollegeDegree { get; set; }

        public bool? EverBeenConvictedOfACrime { get; set; }

        public bool? InformationCertifiedByEmployer { get; set; }

        public string ProofOfCitizenship { get; set; }

        public decimal HourlyRate { get; set; }

        public double? Productivity { get; set; }

        public double? Rating { get; set; }

        public DateTime? RatingDate { get; set; }

        public RangeTypeEnum Range { get; set; }

        public long? ResourceTeamId { get; set; }

        public string PhotoFileName { get; set; }

        public string CvFileName { get; set; }

        public string MotorVehicleReportOther { get; set; }

        public string ProfessionalReferenceChecksOther { get; set; }

        public string EmploymentEligibilityVerificationOther { get; set; }

        public string InternationalWorkHistoryOther { get; set; }

        public YesNoOtherEnum? CredentialVerifications { get; set; }

        public string CredentialVerificationsOther { get; set; }

        public string FormI9Other { get; set; }

        public string FormEVerifyOther { get; set; }

        public ResourceSpokenLanguageRequest[] SpokenLanguages { get; set; }

        public ResourceEducationRequest[] Educations { get; set; }

        public ResourceTravelRequest[] Travels { get; set; }

        public ResourceBehaviouralDetailRequest[] ResourceBehaviouralDetails { get; set; }

        public ResourceRecognitionRequest[] ResourceRecognitions { get; set; }

        public ResourceIndustryRequest[] Industry { get; set; }

        public ResourcePracticeAreaRequest[] ResourcePracticeAreas { get; set; }

        public ResourceCertificateRequest[] Certificates { get; set; }

        public ResourceSkillRequest[] Skills { get; set; }

        public ResourceCriminalRecordRequest[] CriminalRecords { get; set; }

        public ResourcePreviousEmploymentRequest[] PreviousEmployments { get; set; }

        public ResourceCustomFieldValueRequest[] CustomFieldValues { get; set; }

        public ResourceWorkExperienceRequest[] ResourceWorkExperiences { get; set; }

        public ResourceProfessionalOrganizationRequest[] ResourceProfessionalOrganizations { get; set; }

        public ResourceInsuranceRequest[] ResourceInsurances { get; set; }

        public ResourceTimeoffBenefitRequest[] ResourceTimeoffBenefits { get; set; }
    }
}