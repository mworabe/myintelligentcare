﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.LeaveTypes;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class LeaveTypeMapperExtensions
    {
        public static EmitMapperMapperProvider LeaveTypeRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveType, LeaveTypeResponse>
                    (new ExtendedMapConfig<LeaveType, LeaveTypeResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveType[], LeaveTypeResponse[]>());

            mapperProvider.RegisterMapper(() =>
              ObjectMapperManager.DefaultInstance.GetMapper<LeaveType[], LeaveTypePicklistResponse[]>());

            mapperProvider.RegisterMapper(() =>
             ObjectMapperManager.DefaultInstance.GetMapper<LeaveType[], LeaveTypeAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<LeaveTypeRequest, LeaveType>
                    (new ExtendedMapConfig<LeaveTypeRequest, LeaveType>()));

            return mapperProvider;
        }
    }
}