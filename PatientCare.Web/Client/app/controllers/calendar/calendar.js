
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function($stateProvider) {
        $stateProvider
            .state("calendar", {
                url: "/calendar",
                templateUrl: "Client/app/controllers/calendar/calendar.html",
                controller: "CalendarCtrl as CalendarCtrl",
                data: {
                    requireLogin: true
                }
            });
    });