namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedAppointmentTypeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.appointment_type",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    color = c.String(maxLength: 20),
                    Name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),

                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropTable("dbo.appointment_type");
        }
    }
}
