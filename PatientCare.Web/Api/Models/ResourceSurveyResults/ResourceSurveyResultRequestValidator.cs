﻿using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceSurveyResults
{
    public class ResourceSurveyResultRequestValidator : AbstractValidator<ResourceSurveyResultRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IResourceRepository _resourceRepository;

        public ResourceSurveyResultRequestValidator(IDataContextScopeFactory dbContextScopeFactory, IResourceRepository resourceRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _resourceRepository = resourceRepository;

            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(1, 255);

            RuleFor(model => model.Score)
                .NotEmpty()
                .GreaterThanOrEqualTo(0);
            
            RuleFor(model => model.SurveyDate)
                .NotEmpty();

            RuleFor(model => model.ResourceId)
                .NotEmpty()
                .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _resourceRepository, l))
                .WithMessage("The resource with provided Id doesn't exists");

        }
    }
}