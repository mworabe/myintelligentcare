﻿
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceWorkExperience
{
    public class ResourceWorkExperienceRequestValidator : AbstractValidator<ResourceWorkExperienceRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IResourceRepository _resourceRepository;
        public ResourceWorkExperienceRequestValidator(IDataContextScopeFactory dbContextScopeFactory, IResourceRepository resourceRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _resourceRepository = resourceRepository;

            //    RuleFor(model => model.Position)
            //        .NotEmpty()
            //        .Length(1, 255);

            //    RuleFor(model => model.StartDate)
            //        .NotEmpty();

            //    RuleFor(model => model.EndDate)
            //        .NotEmpty()
            //        .GreaterThanOrEqualTo(m => m.StartDate);
        }
    }
}