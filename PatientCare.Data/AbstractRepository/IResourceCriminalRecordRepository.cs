﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceCriminalRecordRepository : IRepository<ResourceCriminalRecord>
    {
    }
}
