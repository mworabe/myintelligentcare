﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceSkillMappingExtensions
    {
        public static DbModelBuilder MapResourceSkill(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceSkill>().ToTable("resource_skills").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceSkill>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceSkill>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceSkill>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsRequired();

            modelBuilder.Entity<ResourceSkill>()
                .HasRequired<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceSkill>()
                .Property(p => p.SkillId)
                .HasColumnName("skill_id")
                .IsRequired();

            modelBuilder.Entity<ResourceSkill>()
                .HasRequired<Skill>(p => p.Skill)
                .WithMany()
                .HasForeignKey(p => p.SkillId);

            modelBuilder.Entity<ResourceSkill>()
                .Property(p => p.YearsOfExpirience)
                .HasColumnName("years_of_expirience")
                .IsRequired();

            modelBuilder.Entity<ResourceSkill>()
                .Property(p => p.Proficiency)
                .HasColumnName("proficiency")
                .IsOptional();

            modelBuilder.Entity<ResourceSkill>()
                .Property(p => p.LastUpdated)
                .HasColumnName("last_updated")
                .IsOptional();

            modelBuilder.Entity<ResourceSkill>()
                .Property(p => p.Description)
                .HasColumnName("description")
                .IsOptional()
                .IsMaxLength();

            return modelBuilder;
        }
    }
}
