namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedColumnndRemoveColumninAppointmentTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patient_appointments", "is_approved", c => c.Boolean());
            DropColumn("dbo.patient_appointments", "appointment_date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.patient_appointments", "appointment_date", c => c.DateTime());
            DropColumn("dbo.patient_appointments", "is_approved");
        }
    }
}
