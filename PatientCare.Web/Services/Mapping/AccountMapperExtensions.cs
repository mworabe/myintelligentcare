﻿using System;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.Account;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class AccountMapperExtensions
    {

        public static EmitMapperMapperProvider AccountRegisterMappings(
        this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ApplicationUser, AccountResponse>
                    (new ExtendedMapConfig<ApplicationUser, AccountResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ApplicationUser[], AccountResponse[]>());

            mapperProvider.RegisterMapper(() =>
               ObjectMapperManager.DefaultInstance.GetMapper<ApplicationUser, AccountPatientResponse>
                (new ExtendedMapConfig<ApplicationUser, AccountPatientResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ApplicationUser[], AccountPatientResponse[]>());
            
            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<AccountRequest, ApplicationUser>
                    (new ExtendedMapConfig<AccountRequest, ApplicationUser>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<AccountRequest[], ApplicationUser[]>());

            return mapperProvider;
        }
    }
}