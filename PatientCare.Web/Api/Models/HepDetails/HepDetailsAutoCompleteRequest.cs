﻿namespace PatientCare.Web.Api.Models.HepDetails
{
    public class HepDetailsAutoCompleteRequest
    {
        public string Search { get; set; }

        public long? ExerciseId { get; set; }

        public long? GroupId { get; set; }

        public long? HepId { get; set; }

        public int? Sets { get; set; }
    }
}