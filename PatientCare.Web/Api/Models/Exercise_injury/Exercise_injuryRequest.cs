﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.Exercise_injury
{
    public class Exercise_injuryRequest 
    {
        public long Id { get; set; }

        public long? injury_Id { get; set; }

        public long? exercise_Id { get; set; }
    }   
}