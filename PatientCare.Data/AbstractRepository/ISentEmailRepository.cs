﻿
using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface ISentEmailRepository : IRepository<SentEmail>
    {
    }
}
