﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Chat Key.
    /// </summary>
    public class ChatKey : Entity
    {
        public string UserId { get; set; }

        public string PrivateKey { get; set; }
    }
}
