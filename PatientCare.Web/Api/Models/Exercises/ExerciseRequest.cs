﻿
using PatientCare.Web.Api.Models.Exercise_injury;
using PatientCare.Web.Api.Models.Exercise_ExerciseActivities;
using PatientCare.Web.Api.Models.ExerciseMedias;

namespace PatientCare.Web.Api.Models.Exercises
{
    public class ExerciseRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }
        
        public string Keywords { get; set; }

        public string Comments { get; set; }

        public bool? IsDeleted { get; set; }

        public long? set { get; set; }

        public long? reps { get; set; }

        public long? holds { get; set; }

        public int time { get; set; }

        public string timeUnit { get; set; }

        public long? weight { get; set; }

        public string weightUnit { get; set; }

        public string holdUnit { get; set; }

        public string frequency { get; set; }

        public string resistance { get; set; }

        public Exercise_injuryRequest[] injuries { get; set; }

        public Exercise_ExerciseActivityRequest[] exercises { get; set; }

        public ExerciseMediaRequest[] Medias { get; set; }

       
    }   
}