﻿"use strict";

(function edzTimeCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzTimeCustomFieldEditOptions", [
            edzTimeCustomFieldOptions
        ]);

    function edzTimeCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/time/edzTimeCustomFieldEdit.html",
            restrict: "E",
            bindToController: {
                model: "=model"
            },
            controller: ["$scope", function edzAddCustomFieldCtrl($scope) {
            }],
            controllerAs: "edzTimeCustomFieldEditOptionsCtrl"
        };
    }
})();
