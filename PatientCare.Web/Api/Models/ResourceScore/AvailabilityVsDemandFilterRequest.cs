﻿using System;
namespace PatientCare.Web.Api.Models.ResourceScore
{
    public class AvailabilityVsDemandFilterRequest
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public double MinimalAvailability { get; set; }
    }
}