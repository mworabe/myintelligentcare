﻿using PatientCare.Core.Entities;
using PatientCare.Web.Api.Models.DropDownConfigs;
using PatientCare.Web.Api.Models.ExerciseActivities;
using PatientCare.Web.Api.Models.ExerciseMedias;
using PatientCare.Web.Api.Models.ExercisePositions;
using PatientCare.Web.Api.Models.Exercise_ExerciseActivities;
using PatientCare.Web.Api.Models.InjuryRegions;
using System;
using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Exewrcise_ExerciseActivities;
using PatientCare.Web.Api.Models.Exercise_injury;
//using PatientCare.Web.Api.Models.Exercise_InjuryRegion;

namespace PatientCare.Web.Api.Models.Exercises
{
    [DataContract]
    public class ExerciseResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Keywords { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public bool? IsDeleted { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public long? set { get; set; }

        [DataMember]
        public long? reps { get; set; }

        [DataMember]
        public long? holds { get; set; }

        [DataMember]
        public long? weight { get; set; }

        [DataMember]
        public string weightUnit { get; set; }

        [DataMember]
        public string holdUnit { get; set; }

        [DataMember]
        public string frequency { get; set; }

        [DataMember]
        public int time { get; set; }

        [DataMember]
        public string timeUnit { get; set; }

        [DataMember]
        public string resistance { get; set; }

        [DataMember]
        public ExerciseMediaResponse[] Medias { get; set; }

        [DataMember]
        public Exercise_ExerciseActivityResponse[] exercises { get; set; }

        [DataMember]
        public Exercise_injuryResponse[] injuries { get; set; }

        [DataMember]
        public string todisplay { get; set; }

        [DataMember]
        public string template { get; set; }


    }
}