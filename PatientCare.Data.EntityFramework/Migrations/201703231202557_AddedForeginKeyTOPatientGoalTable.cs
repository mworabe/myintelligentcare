namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedForeginKeyTOPatientGoalTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patient_goals", "patient_mobile_profile_id", c => c.Long());
            CreateIndex("dbo.patient_goals", "patient_mobile_profile_id");
            AddForeignKey("dbo.patient_goals", "patient_mobile_profile_id", "dbo.patient_mobile_profiles", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.patient_goals", "patient_mobile_profile_id", "dbo.patient_mobile_profiles");
            DropIndex("dbo.patient_goals", new[] { "patient_mobile_profile_id" });
            DropColumn("dbo.patient_goals", "patient_mobile_profile_id");
        }
    }
}
