namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCaseIdColumnInPatientapponintmentTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patient_appointment", "case_id", c => c.Long());
            CreateIndex("dbo.patient_appointment", "case_id");
            AddForeignKey("dbo.patient_appointment", "case_id", "dbo.patient_cases", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.patient_appointment", "case_id", "dbo.patient_cases");
            DropIndex("dbo.patient_appointment", new[] { "case_id" });
            DropColumn("dbo.patient_appointment", "case_id");
        }
    }
}
