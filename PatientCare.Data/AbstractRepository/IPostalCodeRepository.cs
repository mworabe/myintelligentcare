﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IPostalCodeRepository : IRepository<PostalCode>
    {
    }
}
