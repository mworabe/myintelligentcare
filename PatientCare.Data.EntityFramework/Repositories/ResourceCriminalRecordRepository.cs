﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceCriminalRecordRepository : RepositoryBase<ResourceCriminalRecord>, IResourceCriminalRecordRepository
    {
        public ResourceCriminalRecordRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
