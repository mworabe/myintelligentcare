﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface ISurveyQuestionRepository : IRepository<SurveyQuestion>
    {
    }
}
