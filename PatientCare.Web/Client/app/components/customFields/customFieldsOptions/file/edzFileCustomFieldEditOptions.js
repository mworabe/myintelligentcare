﻿"use strict";

(function edzFileCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzFileCustomFieldEditOptions", [
            edzFileCustomFieldOptions
        ]);

    function edzFileCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/file/edzFileCustomFieldEdit.html",
            restrict: "E",
            bindToController: {
                model: "=model"
            },
            controller: ["$scope", function edzAddCustomFieldCtrl($scope) {
            }],
            controllerAs: "edzFileCustomFieldEditOptionsCtrl"
        };
    }
})();
