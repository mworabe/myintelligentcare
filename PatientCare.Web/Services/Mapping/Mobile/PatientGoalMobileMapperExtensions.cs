﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.PatientGoals;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class PatientGoalMobileMapperExtensions
    {
        public static EmitMapperMapperProvider PatientGoalMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientGoal, PatientGoalMobileResponse>
                    (new ExtendedMapConfig<PatientGoal, PatientGoalMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientGoal[], PatientGoalMobileResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientGoalMobileRequest, PatientGoal>(
                        new ExtendedMapConfig<PatientGoalMobileRequest, PatientGoal>()
                    ));

            return mapperProvider;
        }
    }
}