﻿using FluentValidation;
using PatientCare.Web.Services.Validation;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Web.Api.Models.LeaveRequests
{
    public class LeaveRequestsRequestValidator : AbstractValidator<LeaveRequestsRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IResourceRepository _resourceRepository;
        public LeaveRequestsRequestValidator(
            IDataContextScopeFactory dbContextScopeFactory,
            IResourceRepository resourceRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _resourceRepository = resourceRepository;


            //RuleFor(model => model.ResourceId)
            //    .NotEmpty()
            //    .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _resourceRepository, l))
            //    .WithMessage("The resource with provided Id doesn't exists");

            RuleFor(model => model.ResourceId)
               .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _resourceRepository, l))
               .WithMessage("The resource is not exists.");
        }
    }
}