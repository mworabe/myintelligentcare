using System;

namespace PatientCare.Core.Services.ResourceScoreCalculation
{
    public class ScoreFilterItem<T>
    {
        public ScoreFilterItem(T item, int importance, bool mandatory, T startRange, T endRange)
        {
            Item = item;
            Importance = importance;
            Mandatory = mandatory;
            StartRange = startRange;
            EndRange = endRange;
        }

        public T Item { get; set; }

        public int Importance { get; set; }

        public bool Mandatory { get; set; }

        public T StartRange { get; set; }
        public T EndRange { get; set; }
    }
}