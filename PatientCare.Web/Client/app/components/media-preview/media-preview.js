
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("video-view", {
                url: "/video-view/{url}",
                params: { dataObj: null },
                onEnter: ['$state', "$stateParams", '$uibModal', "$window",
                  function ($state,$stateParams, $uibModal,$window) {
                      
                      $uibModal.open({
                          templateUrl: "Client/app/components/media-preview/video-preview/video-view.html",
                          controller: "VideoViewCntrl",
                          size: "lg",
                          keyboard: false,
                          resolve: {
                              dataItem: function () {
                                  return {
                                      title: "Video",
                                      url: $stateParams.url,
                                  }
                              }
                          }
                      }).result.then(function (result) {
                          debugger;
                          $window.history.back();
                      }, function () {
                          debugger;
                          $window.history.back();
                      });
                  }
                ]
            })

    });