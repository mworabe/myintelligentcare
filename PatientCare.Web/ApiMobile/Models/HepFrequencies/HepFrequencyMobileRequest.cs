﻿using PatientCare.Core.Enums;

namespace PatientCare.Web.ApiMobile.Models.HepFrequencies
{
    public class HepFrequencyMobileRequest
    {
        public long Id { get; set; }

        public long? HepId { get; set; }

        public long? HepDetailId { get; set; }

        public int? Day { get; set; }

        public int? ExerciseFrequency { get; set; }

        public bool? IsActive { get; set; }
    }   
}