﻿using System.Collections.Generic;
using System.Linq;
using PatientCare.Core.Services.Dashboards;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Services.Dashboards
{
    public class DashboardService : IDashboardService
    {
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        
        public DashboardService
            (
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
        }
        
        #region Resource Charts
        public List<ProjectTypeViewModel> ResourceCountry()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select countries.name as Status,count(resources.Id) as Count,resources.country_id as Id,'Resource' as TableName,'ResourceCountry' as FieldName,convert(varchar(10), resources.country_id) as FieldValue from resources left outer join countries on resources.country_id = countries.Id where resources.country_id is not null group by countries.name,resources.country_id").ToList();                
                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByDivision()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();
                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();

                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select div.name as Status,Count(res.Id) as Count, res.division_id as Id, 'Resources' as TableName, 'ResourcesDivision' as FieldName, convert(varchar(10), res.division_id) as FieldValue From resources res left outer join divisions as div on res.division_id = div.Id where res.division_id is not null group by div.name, res.division_id order by res.division_id asc, count(res.Id) asc").ToList();
                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByDepartment()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();
                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();

                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select departments.name as Status,count(resources.Id) as Count,resources.department_id as Id,'Resources' as TableName,'ResourcesDepartment' as FieldName,convert(varchar(10), resources.department_id) as FieldValue from resources left outer join departments on resources.department_id = departments.Id where resources.department_id is not null group by departments.name,resources.department_id order by count(resources.Id) asc").ToList();
                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByExperienceLevel()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select (CONVERT(varchar(50),isnull(years_employed,0)) +' Years') as Status,count(id) as Count,convert(bigint,years_employed) as Id,'Resources' as TableName,'ResourceExperienceLevel' as FieldName,convert(varchar(50), isnull(years_employed,0)) as FieldValue from resources where years_employed is not null group by years_employed order by years_employed").ToList();

                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByJobTitle()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select job_titles.name as Status,count(resources.Id) as Count, resources.job_title_id as Id, 'Resources' as TableName,'ResourcesJobTitle' as FieldName, convert(varchar(10), resources.job_title_id) as FieldValue From resources left outer join job_titles on resources.job_title_id = job_titles.Id where resources.job_title_id is not null group by job_titles.name, resources.job_title_id order by count(resources.Id) asc").ToList();
                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByLanguage()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select languages.name as Status,Count(rsl.Id) as Count,rsl.language_id as Id,'Resources' as TableName, 'ResourcesLanguage' as FieldName, convert(varchar(10), rsl.language_id) as FieldValue From resource_spoken_languages rsl	left outer join languages on rsl.language_id = languages.Id where rsl.language_id is not null group by languages.name, rsl.language_id order by rsl.language_id asc, count(rsl.Id) asc").ToList();

                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByGender()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select case when gender = 0 then 'Male' when gender = 1 then 'Female' end as Status,	count(id) as Count,convert(bigint,gender) as Id,'Resources' as TableName,'ResourceGender' as FieldName,	convert(varchar(50), gender) as FieldValue from resources where gender is not null group by gender order by gender").ToList();

                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByState()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select states.name as Status,count(resources.Id) as Count,resources.state_id as Id, 'Resources' as TableName,'ResourcesState' as FieldName, convert(varchar(10), resources.state_id) as FieldValue from resources left outer join states on resources.state_id = states.Id where resources.state_id is not null group by states.name, resources.state_id order by count(resources.Id) asc").ToList();

                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByCity()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select Cities.name as Status,count(resources.Id) as Count,resources.city_id as Id,'Resources' as TableName,	'ResourcesCity' as FieldName,convert(varchar(10), resources.city_id) as FieldValue from resources left outer join Cities on resources.city_id = Cities.Id where resources.city_id is not null group by Cities.name,resources.city_id order by count(resources.Id) asc").ToList();

                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByLocation()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select location as Status,count(id) as Count,'Resources' as TableName,'ResourceLocation' as FieldName,location as FieldValue From resources where location is not null group by location order by location ").ToList();

                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByEducationNameOfSchool()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select resource_educations.name_of_school as Status,count(resource_educations.Id) as Count,	'Resources' as TableName, 'EducationNameOfSchool' as FieldName, resource_educations.name_of_school as FieldValue from resource_educations group by resource_educations.name_of_school order by resource_educations.name_of_school asc ").ToList();

                return query.OrderBy(x => x.Id).ToList();
            }
        }
        public List<ProjectTypeViewModel> ResourceByRating()
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateReadOnly())
            {
                IEnumerable<ProjectTypeViewModel> result = new List<ProjectTypeViewModel>();

                var ctx = dbContextScope.DbContexts.Get<PatientCareDbContext>();
                var query = ctx.Database.SqlQuery<ProjectTypeViewModel>("select convert(varchar(10),rating) as Status,count(id) as Count,convert(bigint,rating) as Id,'Resources' as TableName,'ResourceRating' as FieldName, convert(varchar(10),rating) as FieldValue From resources where rating is not null group by rating order by rating").ToList();

                return query.OrderBy(x => x.Count).ToList();
            }
        }
        #endregion

    }
}
