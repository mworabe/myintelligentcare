﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("HolidaysServices", ["$resource", function ($resource) {
      var resource = $resource("api/holiday/:id/:controller",
        null, //parameters default
            {
                'update': { method: "PUT" },
                'query': { method: "GET", params: {}, isArray: false },
                'getLeaveList': { method: "GET", params: {}, isArray: false }
            });

      resource.getTableOption = {
          pageName: "Holidays",
          hideSearch: true,
          hideBulkEdit: true,
          cols: function () {
              return [
                  { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                  { field: "name", title: "Name", sortable: "name", show: true, dataType: "text", required: true },
                  { field: "holidayDate", title: "Holiday Date", sortable: "holidayDate", show: true, dataTypeView: "date-time", dataType: "date-time", required: true },
                  { field: "description", title: "Description", sortable: "description", show: true, dataType: "text", required: false },
                  { field: "action", title: ".", class:'blank-cell', dataType: "command" },
              ];
          }
      };
      return resource;
  }]);
