﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Data.EntityFramework
{
    public static class DbContextHelpers
    {
        public static void ChildrenProcessing<T>(this DbContext ctx, ICollection<T> children, Func<T, bool> predicate) where T : Entity
        {
            if(children == null) return;

            var dbSet = ctx.Set<T>();
            var currentChildren = dbSet.Where(predicate);

            //Create or Update
            foreach (var s in children)
            {
                dbSet.Attach(s);
                ctx.Entry(s).State = s.Id > 0 ? EntityState.Modified : EntityState.Added;
            }
            //Delete
            foreach (var child in currentChildren.Where(child => children.All(x => x.Id != child.Id)))
            {
                ctx.Entry(child).State = EntityState.Deleted;
            }
        }
        
        public static void ChildrenProcessingExperience<T>(this DbContext ctx, ICollection<T> children, Func<T, bool> predicate) where T : Entity
        {
            if (children == null) return;

            var dbSet = ctx.Set<T>();
            var currentChildren = dbSet.Where(predicate);

            //Create or Update
            foreach (var s in children)
            {
                dbSet.Add(s);
                ctx.Entry(s).State = s.Id > 0 ? EntityState.Modified : EntityState.Added;
            }
            //Delete
            foreach (var child in currentChildren.Where(child => children.All(x => x.Id != child.Id)))
            {
                ctx.Entry(child).State = EntityState.Deleted;
            }
        }

        public static void ChildrenProcessingHepFrequency<T>(this DbContext ctx, ICollection<T> children, Func<T, bool> predicate) where T : Entity
        {
            if (children == null) return;

            var dbSet = ctx.Set<T>();
            var currentChildren = dbSet.Where(predicate);

            //Create or Update
            foreach (var s in children)
            {
                //dbSet.Attach(s);
                dbSet.Add(s);
                ctx.Entry(s).State = s.Id > 0 ? EntityState.Modified : EntityState.Added;
            }
            //Delete
            foreach (var child in currentChildren.Where(child => children.All(x => x.Id != child.Id)))
            {
                ctx.Entry(child).State = EntityState.Deleted;
            }
        }
        
        public static void ChildrenDelete<T>(this DbContext ctx, ICollection<T> children, Func<T, bool> predicate) where T : Entity
        {
            var dbSet = ctx.Set<T>();
            var currentChildren = dbSet.Where(predicate);
            if (children == null) return;
            //Delete
            foreach (var child in currentChildren)
            {
                ctx.Entry(child).State = EntityState.Deleted;
            }
        }
    }
}
