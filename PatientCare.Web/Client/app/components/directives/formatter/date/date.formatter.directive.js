﻿"use strict";
var app = angular.module("EDZoutstaffingPortalApp");
app.directive("uiDateFormat", ["$filter", "$locale", function ($filter, $locale) {
    return {
        restrict: 'AE',
        require: 'ngModel',
        link: function ($scope, element, attrs, controller, ngModel) {

            if (!(ngModel || attrs.ngModel)) return;

            if (!ngModel)
                ngModel = attrs.ngModel;

            ngModel.$formatters.unshift(function (value) {
                if (!value || isNaN(value)) {
                    return "";
                }
                if (value.length == 6) {
                    value = [value.slice(0, 1), '/', value.slice(1, 2), '/', value.slice(2, 3)].join();
                }
                return value;
                /*
                if (value.length == 8) {

                }
                */
            });
        }
    };
}]);

