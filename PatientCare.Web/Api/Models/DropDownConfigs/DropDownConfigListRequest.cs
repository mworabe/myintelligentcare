﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.DropDownConfigs
{
    public class DropDownConfigListRequest : ListRequest
    {
        [DataMember(Name = "dropdowntype")]
        public string DropDownType { get; set; }

        [DataMember(Name = "activityTypeId")]
        public long? ActivityTypeId { get; set; }
    }
}