namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class myMigration : DbMigration
    {

        public override void Up()
        {

            CreateTable(
                "dbo.exercise_injury",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    exercise_Id = c.Long(),
                    injury_Id = c.Long(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
               .ForeignKey("dbo.exercises", t => t.exercise_Id)
                .ForeignKey("dbo.injury_regions", t => t.injury_Id)
                .Index(t => t.exercise_Id)
                .Index(t => t.injury_Id);

            CreateTable(
                "dbo.exercise_exerciseactivity",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    exercise_Id = c.Long(),
                    activity_Id = c.Long(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.exercise_activities", t => t.activity_Id)
                .ForeignKey("dbo.exercises", t => t.exercise_Id)
                .Index(t => t.exercise_Id)
                .Index(t => t.activity_Id);

            AddColumn("dbo.exercises", "set", c => c.Long());
            AddColumn("dbo.exercises", "reps", c => c.Long());
            AddColumn("dbo.exercises", "holds", c => c.Long());
            AddColumn("dbo.exercises", "weight", c => c.Long());
            AddColumn("dbo.exercises", "weightUnit", c => c.String());
            AddColumn("dbo.exercises", "holdUnit", c => c.String());
            AddColumn("dbo.exercises", "frequency", c => c.String());
        }

        public override void Down()
        {
            DropForeignKey("dbo.exercise_injury", "injury_Id", "dbo.injury_regions");
            DropForeignKey("dbo.exercise_injury", "exercise_Id", "dbo.exercises");
            DropForeignKey("dbo.exercise_exerciseactivity", "exercise_Id", "dbo.exercises");
            DropForeignKey("dbo.exercise_exerciseactivity", "activity_Id", "dbo.exercise_activities");
            DropIndex("dbo.exercise_exerciseactivity", new[] { "activity_Id" });
            DropIndex("dbo.exercise_exerciseactivity", new[] { "exercise_Id" });
            DropIndex("dbo.exercise_injury", new[] { "injury_Id" });
            DropIndex("dbo.exercise_injury", new[] { "exercise_Id" });
            DropColumn("dbo.exercises", "frequency");
            DropColumn("dbo.exercises", "holdUnit");
            DropColumn("dbo.exercises", "weightUnit");
            DropColumn("dbo.exercises", "weight");
            DropColumn("dbo.exercises", "holds");
            DropColumn("dbo.exercises", "reps");
            DropColumn("dbo.exercises", "set");
            DropTable("dbo.exercise_exerciseactivity");
            DropTable("dbo.exercise_injury");
            CreateIndex("dbo.exercises", "exercise_region_id");
            CreateIndex("dbo.exercises", "activity_id");
            CreateIndex("dbo.exercises", "position_id");
            CreateIndex("dbo.exercises", "region_id");
        }
    }
}


