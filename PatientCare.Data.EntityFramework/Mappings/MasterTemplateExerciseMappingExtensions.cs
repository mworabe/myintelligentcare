﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class MasterTemplateExerciseMappingExtensions
    {
        public static DbModelBuilder MapMasterTemplateExercise(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<MasterTemplateExercise>().ToTable("master_template_exercises")
                .HasKey(i => i.Id);

            modelBuilder.Entity<MasterTemplateExercise>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<MasterTemplateExercise>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<MasterTemplateExercise>()
                .Property(p => p.ExerciseId)
                .HasColumnName("exercise_id")
                .IsOptional();

            modelBuilder.Entity<MasterTemplateExercise>()
                .HasOptional<Exercise>(p => p.Exercise)
                .WithMany()
                .HasForeignKey(p => p.ExerciseId);

            modelBuilder.Entity<MasterTemplateExercise>()
                .Property(p => p.MasterTemplateId)
                .HasColumnName("master_template_id")
                .IsOptional();

            modelBuilder.Entity<MasterTemplateExercise>()
                .HasOptional<MasterTemplate>(p => p.MasterTemplate)
                .WithMany()
                .HasForeignKey(p => p.MasterTemplateId);

            modelBuilder.Entity<MasterTemplateExercise>()
                .Property(p => p.ExerciseOrder)
                .HasColumnName("exercise_order")
                .IsOptional();

            return modelBuilder;
        }
    }
}
