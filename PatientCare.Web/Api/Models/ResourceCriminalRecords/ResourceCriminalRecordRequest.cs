﻿using System;

namespace PatientCare.Web.Api.Models.ResourceCriminalRecords
{
    public class ResourceCriminalRecordRequest
    {
        public long Id { get; set; }

        public long ResourceId { get; set; }

        public DateTime DateOfOffense { get; set; }

        public string LocationOfOffense { get; set; }

        public string Offense { get; set; }

        public string PenaltyOrDisposition { get; set; }
    }
}