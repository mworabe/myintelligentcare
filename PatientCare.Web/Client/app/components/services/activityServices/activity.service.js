﻿"use strict";
var EOPApp = angular.module("EDZoutstaffingPortalApp")
EOPApp.service("ActivityService", ["$resource", "$state", "$filter", "$translate", "$injector",
    function ($resource, $state, $filter, $translate, $injector) {
        var permissionWorker = null;

        function getPermissionWorker() {
            if (!permissionWorker) {
                permissionWorker = $injector.get("PermissionWorker");
            }
            return permissionWorker;
        };

        var resource = $resource("api/activityType/:id/:controller", null, {
            "query": { method: "GET", params: {}, isArray: false },
            "update": { method: "put", params: {}, isArray: false },
            "activityAccessibleList": { method: "GET", params: { controller: "autocomplete" }, isArray: true },
            'all': { method: "GET", params: {}, isArray: true, url: "api/ExerciseActivities/autocomplete?search=" },
        });

        resource.getTableOption = {
            pageName: "Activity",
            hideBulkEdit: true,
            disabledDeleteButton: function () { return false; },
            disabledViewButton: function () { return false; },
            disableViewButton: function () { return false },
            disabledAddButton: function () {
                return !getPermissionWorker().canProjectConfigDataInsert();
            },
            disabledEditButton: function () {
                return getPermissionWorker().canProjectConfigDataUpdate();
            },
            formatDelete: function (row) {
                return row.lastName + " " + row.firstName;
            },
            cols: function () {
                return [
                    {
                        field: "name",
                        title: $filter('translate')('Projects_title') + " Name",
                        sortable: "name",
                        dataType: "text",
                        required: true,
                        show: true,
                        dataTypeView: "href",
                        buildUrl: function (row) {
                            return $state.href("activityEdit", { id: row.id });
                        }
                    }, {
                        field: "activityColorCode",
                        title: "Color Code",
                        sortable: true,
                        show: true,
                        dataType: "color-ui-select",
                        dataTypeView: "color-picker",
                        list: ["#ED7D31", "#68CCFF", "#3122F6", "#9E09CD", "#080808", "#7F7F7F", "#FFCC9B"],
                    }, {
                        field: "isActive",
                        title: "Active",
                        sortable: false,
                        show: true,
                        dataType: "always-checkbox",
                        dataTypeView: "always-checkbox",
                        customViewText: function (row) {
                            return (row.isActive) ? "Active" : "Inactive";
                        }
                    },
                    {
                        field: "action",
                        title: ".",
                        class: "blank-cell",
                        dataType: "command"
                    }
                ];
            },
            //onAdd_Click: function () {
            //    $state.go("assignmentEdit");
            //},
            /*
            onEdit_Click: function (row) {
                $state.go("activityEdit", { id: row.id });
            },*/
            moreExtraButtonList: [
                {
                    label: "Button1",
                    click: function (row) {
                        $state.go("activityEdit", { id: row.id });
                    },
                    iconClass: "icon-cog",
                }
            ],
            headerButtons: [],
            headerRightItems: []
        };
        return resource;
    }]);
