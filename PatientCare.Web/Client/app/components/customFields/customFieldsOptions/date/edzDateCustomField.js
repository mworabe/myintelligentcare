"use strict";
(function edzDateCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzDateCustomField", ["edzBaseCustomField", edzDateCustomField]);

    ///////////////////////////////////////////////////////
    function edzDateCustomField(baseCustomField) {
        function DateCustomField() { }

        DateCustomField.prototype = Object.create(baseCustomField);
        DateCustomField.prototype.optionsTemplate = "<edz-date-custom-field-options></edz-date-custom-field-options>";
        DateCustomField.prototype.optionsTemplateEdit = "<edz-date-custom-field-edit-options></edz-date-custom-field-edit-options>";
        return DateCustomField;
    }
})();