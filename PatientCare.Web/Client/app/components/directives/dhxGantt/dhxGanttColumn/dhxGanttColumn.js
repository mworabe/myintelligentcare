﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ganttColumn", [
        "DhxHelper", "Project", "ProjectTask", "ProjectPortfolio", function (dhxHelper, projectService, projectTaskService, projectPortfolioService) {
            return {
                restrict: "AE",
                terminal: true,
                link: function ($scope, $element, $attrs) {

                    var needToAddCols = $attrs.showcols || false;


                    var label = $attrs.label || " ";
                    var width = $attrs.width || "*";
                    var align = $attrs.align || "left";
                    var name = $attrs.name || "name";
                    var resize = $attrs.resize || true;
                    var showTreeView = $attrs.tree || false;
                    var showColumn = $attrs.hide || false;
                    var isProject = $attrs.isprj;

                    var showOnHiddenDetails = $attrs.edzshowonfulldata;

                    var template1 = Function("task", "return \"" + dhxHelper.templateHelper($element) + "\"");
                    var config = {
                        template: function (task) {
                            return template1(task);
                        },
                        label: label,
                        min_grid_column_width: width,
                        width: parseInt(width),
                        align: align,
                        id: $attrs.id,
                        name: name,
                        sort: name,
                        hide: showColumn,
                        resize: resize,
                        showOnHiddenDetails: !!showOnHiddenDetails

                    };

                    if ($attrs.sort && $attrs.sort === "false") {
                        config.sort = false;
                    }

                    if (!gantt.config.columnsSet)
                        gantt.config.columnsSet = gantt.config.columns = [];

                    if (!gantt.config.columns.length)
                        config.tree = true;

                    gantt.config.columns.push(config);


                    if (needToAddCols) {
                        console.info(isProject);
                        if (isProject && isProject == "project") {
                            _.forEach(projectService.getProjectGanttFieldList(), function (column) {
                                var existingCol = _.find(gantt.config.columns, { name: column.name });
                                if (!existingCol)
                                    gantt.config.columns.push(column);
                            })
                        } else if (isProject && isProject == "task") {
                            _.forEach(projectTaskService.getProjectTasksGanttFields(), function (column) {
                                var existingCol = _.find(gantt.config.columns, { name: column.name });
                                if (!existingCol)
                                    gantt.config.columns.push(column);
                            })
                        } else {
                            _.forEach(projectPortfolioService.getGanttColumns(), function (column) {
                                var existingCol = _.find(gantt.config.columns, { name: column.name });
                                if (!existingCol)
                                    gantt.config.columns.push(column);
                            })
                        }
                    }
                }
            };
        }
    ]);