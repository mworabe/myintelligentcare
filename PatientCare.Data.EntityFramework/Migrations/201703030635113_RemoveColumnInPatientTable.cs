namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class RemoveColumnInPatientTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "status", c => c.Int());
            DropColumn("dbo.patients", "patient_status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.patients", "patient_status", c => c.Int());
            DropColumn("dbo.patients", "status");
        }
    }
}
