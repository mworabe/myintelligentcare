﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PatientCare.Data.AbstractRepository
{
    public class ReadOptions
    {
        public static ReadOptions<T> Create<T>()
        {
            return new ReadOptions<T>();
        }
    }

    public class ReadOptions<T>
    {
        private readonly List<Expression<Func<T, object>>> _includePredicatesInternal = new List<Expression<Func<T, object>>>();
        private bool _readOnly;

        public IEnumerable<Expression<Func<T, object>>> IncludePredicates { get { return _includePredicatesInternal; } }
        public bool ReadOnly { get { return _readOnly; } }


        public ReadOptions<T> WithIncludePredicate(Expression<Func<T, object>> predicate)
        {
            if (predicate == null) { throw new ArgumentNullException("predicate"); }

            _includePredicatesInternal.Add(predicate);
            return this;
        }

        public ReadOptions<T> AsReadOnly()
        {
            _readOnly = true;
            return this;
        }
    }
}
