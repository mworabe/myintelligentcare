"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("rolesAvailabilityVsDemand", {
                url: "/role-availability-vs-demand",
                params: { jobTitleId: null, jobTitleName: null },
                templateUrl: "Client/app/controllers/rolesAvailabilityVsDemand/rolesAvailabilityVsDemandV2.html",
                controller: "RolesAvailabilityVsDemandCtrl as RolesAvailabilityVsDemandCtrl",
                data: {
                    requireLogin: true
                }
            });
    });