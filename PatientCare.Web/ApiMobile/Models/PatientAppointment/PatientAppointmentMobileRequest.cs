﻿using PatientCare.Core.Enums;
using System;

namespace PatientCare.Web.ApiMobile.Models.PatientAppointment
{
    public class PatientAppointmentMobileRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string AdditionalDetail { get; set; }

        public string AppointmentType { get; set; }
    }
}