﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.DashboardSetups
{
    [DataContract]
    public class DashboardSetupResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string SetupJson { get; set; }
    }
}