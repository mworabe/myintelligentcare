﻿using EDZoutstaffingportal.Core.Services.Mailing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDZoutstaffingportal.EmailNotificationSender
{
    public class Sender : ISender
    {
        private readonly ITaskNewProjectedEndDateNotificationService _service;

        public Sender(ITaskNewProjectedEndDateNotificationService service)
        {
            _service = service;
        }

        public void Send()
        {
            _service.SendEmailNotifications();
        }
    }
}
