﻿using System;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Certificate.
    /// </summary>
    public class ResourceCertificate : Entity
    {
        public long ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        public long CertificateId { get; set; }
        public virtual Certificate Certificate { get; set; }
        
        public DateTime? ExpirationDate { get; set; }
        
        public long? CertificateStateId { get; set; }        
        public virtual State CertificateState { get; set; }

        public DateTime? IssueDate { get; set; }

        public string Name
        {
            get
            {
                return Certificate != null? Certificate.Name : string.Empty;
            }
        }
    }
}
