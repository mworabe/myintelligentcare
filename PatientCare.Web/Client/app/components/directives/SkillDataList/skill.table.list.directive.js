﻿"use strict";
var app = angular.module("EDZoutstaffingPortalApp");
app.directive("skillTableList", ["uuid2", "$uibModal", "Modal", "toaster",
    function (uuid2, $uibModal, modal, toaster) {
        return {
            templateUrl: "Client/app/components/directives/SkillDataList/SkillTableListView.html",
            restrict: "E",
            replace: false,
            scope: {
                skillList: "=",
                model: "=ngModel",
                option: "=ngOption",
                viewMode: "=?ngViewMode",
                resumeMode: "=?ngIsResume",
                addButtonText: "="
            },
            link: function (scope) {

                scope.angucomleteStorePrefix = scope.angucomleteStorePrefix || "___storeFieldSelectValue";
                scope.option = scope.option || {};
                scope.viewMode = (typeof scope.viewMode === "undefined" || scope.viewMode === null) ? false : scope.viewMode;

                scope.option.transformModelPrePush = scope.option.transformModelPrePush || angular.noop;
                scope.addNew = function () {
                    if (!scope.model)
                        scope.model = [];
                    var item = {};
                    scope.option.transformModelPrePush(item);
                    scope.model.push(item);
                };

                scope.addNewFromPopUp = function () {
                    var taskFilter = {
                        fromDate: this.$parent.parentModel.startDate,
                        toDate: this.$parent.parentModel.dueDate
                    };
                    scope.editTaskModal = $uibModal.open({
                        animation: true,
                        backdrop: "static",
                        templateUrl: "Client/app/controllers/rolesAvailabilityVsDemand/rolesAvailabilityVsDemandPopUp.html",
                        controller: "RolesAvailabilityVsDemandPopUpCtrl as RolesAvailabilityVsDemandCtrl",
                        resolve: {
                            data: function () {
                                return {
                                    taskFilter: taskFilter
                                }
                            }
                        }
                    });
                    scope.editTaskModal.result.then(function (parameters) {

                        if (scope.savePopupCallback)
                            scope.savePopupCallback(parameters);
                    });
                }
                scope.dateFormat = function (value, format) {
                    if (!value)
                        return undefined;
                    if (angular.isString(value)) {
                        return moment(value).format(format || "MM/DD/YYYY");
                    }
                    return value.format(format || "MM/DD/YYYY");
                }

                scope.delete = function (field, item) {
                    var index = scope.model.indexOf(item);
                    if (index >= 0)
                        scope.model.splice(index, 1);
                }

                scope.angucompleteSelect = function (select) {
                    var field = this.$parent.$parent.field;
                    var fieldName = this.$parent.$parent.field.fieldName;
                    var item = this.$parent.$parent.$parent.item;
                    if (select && select.originalObject && select.originalObject.id > 0) {
                        item[fieldName] = select.originalObject.id;
                        item[scope.angucomleteStorePrefix + fieldName] = select.originalObject;
                        if (field.angucompleteSelector) {
                            field.angucompleteSelector(item, select.originalObject);
                        }
                    } else item[fieldName] = null;
                }
                scope.deleteItem = function (item, arr) {
                    var languageItemIndex = arr.indexOf(item);
                    arr.splice(languageItemIndex, 1);
                }
                scope.generateId = uuid2.newguid;
                scope.isFunction = angular.isFunction;

            }
        };
    }]);