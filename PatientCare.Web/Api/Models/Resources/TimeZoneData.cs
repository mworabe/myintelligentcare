﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Resources
{
    [DataContract]
    public class TimeZoneData
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string name { get; set; }

        public TimeZoneData()
        {

        }
        public TimeZoneData(String name)
        {
            this.id = name;
            this.name = name;
        }
        public TimeZoneData(String id, String name)
        {
            this.id = id;
            this.name = name;
        }

        public static List<TimeZoneData> GetTimeZoneData()
        {
            List<TimeZoneData> timezone = new List<TimeZoneData>();
            timezone.Add(new TimeZoneData("Atlantic Standard Time (AST)"));
            timezone.Add(new TimeZoneData("Eastern Daylight Time (EDT)"));
            timezone.Add(new TimeZoneData("Eastern Standard Time (EST)"));
            timezone.Add(new TimeZoneData("Central Daylight Time (CDT)"));
            timezone.Add(new TimeZoneData("Central Standard Time (CST)"));
            timezone.Add(new TimeZoneData("Mountain Daylight Time (MDT)"));
            timezone.Add(new TimeZoneData("Mountain Standard Time (MST)"));
            timezone.Add(new TimeZoneData("Pacific Daylight Time (PDT)"));
            timezone.Add(new TimeZoneData("Pacific Standard Time (PST)"));
            timezone.Add(new TimeZoneData("Alaska Daylight Time (AKDT)"));
            timezone.Add(new TimeZoneData("Alaska Standard Time (AKST)"));
            timezone.Add(new TimeZoneData("Hawaii-Aleutian Daylight Time (HDT)"));
            timezone.Add(new TimeZoneData("Hawaii-Aleutian Standard Time (HST)"));
            timezone.Add(new TimeZoneData("Samoa Standard Time (SST)"));
            timezone.Add(new TimeZoneData("Chamorro Standard Time (ChST)"));
            return timezone;
        }
    }
}