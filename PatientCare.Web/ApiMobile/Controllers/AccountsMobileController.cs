﻿using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Web.Models;
using PatientCare.Web.Services.WebApi;
using Microsoft.AspNet.Identity;
using System.Runtime.Serialization;
using PatientCare.Data.EntityFramework.Identity;

namespace PatientCare.Web.Controllers
{
    /// <summary>
    /// This Controller is used to handle Accounts Mobile operations.
    /// </summary>
    //[Authorize(Roles = (ApplicationUser.AdminRole + "," + ApplicationUser.HumanResourceRole))]
    [Authorize(Roles = ApplicationUser.PatientRole)]
    [ExceptionHandling]
    public class AccountsMobileController : AuthBaseApiController
    {
        /// <summary>
        /// Method Description : Using for Change Password. 
        /// </summary>
        /// <param name="model">Change Password Mobile Binding Model</param>
        /// <returns>Response Class</returns>
        [HttpPost]
        [Route("api/mobile/v1/change-password")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordMobileBindingModel model)
        {
            Response resultResponse = new Response();
            if (!ModelState.IsValid)
            {
                foreach(var message in ModelState.Values)
                {
                    foreach(var errorMessage in message.Errors)
                    {
                        resultResponse.Status = "False";
                        resultResponse.Message = errorMessage.ErrorMessage;
                        return Ok(resultResponse);
                    }
                }
            }
            var result = await this.AppUserManager.ChangePasswordAsync(model.UserId, model.OldPassword, model.NewPassword);

            if(result.Succeeded == true)
            {
                resultResponse.Status = "True";
                resultResponse.Message = "Password Change Successfully.";
            }
            else
            {
                resultResponse.Status = "False";
                resultResponse.Message = "Incorrect password.";
            }
            return Ok(resultResponse);
        }

        /// <summary>
        /// Description : Response Class.  
        /// </summary>
        public class Response
        {
            [DataMember]
            public string Status { get; set; }

            [DataMember]
            public object Message { get; set; }
        }
    }
}