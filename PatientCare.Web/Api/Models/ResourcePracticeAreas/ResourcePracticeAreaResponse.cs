﻿using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourcePracticeAreas
{
    [DataContract]
    public class ResourcePracticeAreaResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public PicklistResponse Resource { get; set; }

        [DataMember]
        public long? PracticeAreaId { get; set; }
        [DataMember]
        public PicklistResponse PracticeArea { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}