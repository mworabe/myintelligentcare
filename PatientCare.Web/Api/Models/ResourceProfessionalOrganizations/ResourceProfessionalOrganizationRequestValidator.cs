﻿using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceProfessionalOrganizations
{
    public class ResourceProfessionalOrganizationRequestValidator : AbstractValidator<ResourceProfessionalOrganizationRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IResourceRepository _resourceRepository;
        public ResourceProfessionalOrganizationRequestValidator(IDataContextScopeFactory dbContextScopeFactory, IResourceRepository resourceRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _resourceRepository = resourceRepository;
        }
    }
}