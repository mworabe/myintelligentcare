﻿using System.Threading.Tasks;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Services
{
    public interface IExerciseService
    {
        #region Exercise

        Task<bool> UpdateExerciseAsync(Exercise exercise);
        Task<bool> CreateExerciseAsync(Exercise exercise);
        Task<Exercise> ReadExerciseAsync(long id);
        //Task<bool> DeleteExerciseAsync(long id);
        Task<bool> DeleteExerciseAsync(Exercise exercise);

        #endregion

        #region Master Template

        Task<bool> UpdateMasterTemplateAsync(MasterTemplate masterTemplate);
        Task<bool> CreateMasterTemplateAsync(MasterTemplate masterTemplate);
        Task<MasterTemplate> ReadMasterTemplateAsync(long id);
        //Task<bool> DeleteMasterTemplateAsync(long id);
        Task<bool> DeleteMasterTemplateAsync(MasterTemplate masterTemplate);

        #endregion
    }
}
