﻿angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("referralEdit", {
                url: "/referral/edit/{referralId}",
                templateUrl: "Client/app/controllers/referral/referralEdit.html",
                controller: "ReferralPhysicianEditController as ReferralPhysicianEditController",
                params: {},
                data: {
                    security: {
                        authenticated: true
                    }
                },
                resolve: {
                    intialData: ["$q", "$stateParams", "ReferralPhysicianService", "SystemFunctions", "patientCaseService",
                        function loadCaseInitialData($q, $stateParams, referralPhysicianService, systemFunctions, patientCaseService) {
                            function loadReferralData() {
                                var defer = $q.defer();
                                if (!$stateParams.referralId) {
                                    defer.resolve();
                                } else {
                                    referralPhysicianService.get({ id: $stateParams.referralId }).$promise.then(function (success) {
                                        defer.resolve(success);
                                    }, function (error) {
                                        defer.resolve(error);
                                    });
                                }
                                return defer.promise;
                            }
                            return $q.all([loadReferralData()])
                                .then(function (resolutions) {
                                    systemFunctions.scrollToTopZero();
                                    return resolutions;
                                });
                        }]
                }
            }).state("referralList", {
                url: "/referral",
                templateUrl: "Client/app/controllers/referral/referral-list.html",
                controller: "ReferralPhysicianListController as ReferralPhysicianListController",
                params: {},
                data: {
                    security: {
                        authenticated: true
                    }
                },
            })
    });
