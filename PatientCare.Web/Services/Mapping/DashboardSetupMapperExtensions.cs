﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.DashboardSetups;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class DashboardSetupMapperExtensions
    {
        public static EmitMapperMapperProvider DashboardSetupRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<DashboardSetup, DashboardSetupResponse>(new ExtendedMapConfig<DashboardSetup, DashboardSetupResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<DashboardSetup[], DashboardSetupResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<DashboardSetupRequest, DashboardSetup>(new ExtendedMapConfig<DashboardSetupRequest, DashboardSetup>()));

            return mapperProvider;
        }
    }
}