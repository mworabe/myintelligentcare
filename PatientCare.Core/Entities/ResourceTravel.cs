﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Travel.
    /// </summary>
    public class ResourceTravel: Entity
    {
        public long ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        public VisaOrPermitEnum? VisaOrPermit { get; set; }
        public long? CountryId { get; set; }
        public virtual Country Country { get; set; }
        public int? TimeRemainingInDays { get; set; }
        public string SpecialConsiderations { get; set; }
    }
}
