"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("NavbarCtrl", [
        "$rootScope",
        "$scope",
        "$state",
        "Auth",
        "PermissionWorker",
        "Resource",
        "toaster",
        "Upload",
        'panels',
        'SignalrClient',
        'TwilioChatFactory',
        'SoundFactory',
        'VirgilFactory',
        NavbarCtrlFunc]);

function NavbarCtrlFunc($rootScope, $scope, $state, auth, permissionWorker, resourceService, toaster, uploader, panels, SignalrClient, TwilioChatFactory, SoundFactory, VirgilFactory) {


    //var currentUser = auth.getCurrentUser();
    var currentUser = null;

    $scope.openMenu = function () {
        panels.open("rightMenu");
    }
    $scope.getUserName = function () {
        return (auth.getCurrentUser().ResourceName) ? (auth.getCurrentUser().ResourceLastName.substring(0, 1) + ', ' + auth.getCurrentUser().ResourceFirstName) : auth.getCurrentUser().unique_name;
    }

    $scope.menu = [];
    $scope.elasticSearch = {};
    $scope.elasticSearch.searchPhrase = "";
    $scope.notificationList = [];
    $scope.communicationNotifList = [];



    $scope.onNotifClick = function (item, index) {
        if (item.NotifType == 'Appointment') {
            $state.go("schedules");
        } else if (item.NotifType == 'PainRatings') {
            $state.go("patientsEdit", { id: item.DataObject });
        }
        $scope.notificationList.splice(index, 1);
    }

    $scope.$on('onPainRatingAlert', function (event, item) {
        if (item.DataObject) {
            $scope.notificationList.push(item);
            $scope.$apply();
        }
    });

    $scope.onMessageItemClick = function (result, index) {
        $state.go("communication.chat.chat-box", { sessionObject: result.dataObject.session }, { reload: "communication.chat.chat-box" });
        $scope.communicationNotifList.splice(index, 1);
    }

    function isSessionInList(sid) {
        if ($scope.communicationNotifList) {
            for (var i = 0; i < $scope.communicationNotifList.length; i++) {
                var obj = $scope.communicationNotifList[i];
                if (obj.dataObject.session.channelsId == sid) {
                    return i;
                }
            }
        }

        return -1;
    }

    $rootScope.$on("onNewMessage", function (event, dataObject) {
        if (dataObject && dataObject.session && dataObject.message) {
            if (dataObject.session && dataObject.session) {
                SoundFactory.playNotifSound();
                TwilioChatFactory.setCurrentUser(dataObject.session);
                //if (isSessionInList(dataObject.session.channelsId) == -1) {
                //    $scope.communicationNotifList.push({
                //        imgUrl: dataObject.session.otherUser.photoFileNameUrl,
                //        title: dataObject.session.otherUser.lastName + " " + dataObject.session.otherUser.firstName,
                //        subTitle: "New message",
                //        dataObject: dataObject
                //    });
                //}

                $scope.communicationNotifList.push({
                    imgUrl: dataObject.session.otherUser.photoFileNameUrl,
                    title: dataObject.session.otherUser.lastName + " " + dataObject.session.otherUser.firstName,
                    subTitle: "New message",
                    dataObject: dataObject
                });
            }
        }

    });

    $scope.$on('onNewAppointment', function (event, item) {

        if (currentUser && currentUser.role && currentUser.role != "Admin") {
            var isToShowNotif = false;
            if (currentUser.role == "Therapist" && currentUser.ResourceId && currentUser.ResourceId == item.DataObject.ClinicianId) {
                isToShowNotif = true;
            }
            //FacilityId
            //currentUser.clinicId = response.data.clinicId;
            //currentUser.clinicLocationId = response.data.clinicLocationId;
            if (currentUser.role == "Front Office" && currentUser.clinicId && currentUser.clinicId == item.DataObject.FacilityId) {
                isToShowNotif = true;
            }

            if (item.DataObject && isToShowNotif) {
                SoundFactory.playNotifSound();
                var startDate = moment.utc(item.DataObject.StartDate).local();
                var endDate = moment.utc(item.DataObject.EndDate).local();
                item.subTitle = "Appointment scheduled  for Patient <b>" +
                item.DataObject.PatientName + "</b> On <b>" + moment(startDate).format("MMMM DD YYYY HH:mm") + "</b>";
                $scope.notificationList.push(item);
                $scope.$apply();
            }
        }

        /*if (currentUser &&
            currentUser.role &&
            currentUser.role != "Admin" &&
            currentUser.ResourceId &&
            currentUser.ResourceId == item.DataObject.ClinicianId ||

           (currentUser.role &&
            currentUser.role != "Admin" &&
            currentUser.ClinicLocationId &&
            currentUser.ClinicLocationId == item.DataObject.ClinicLocationId)) {
            SoundFactory.playNotifSound();

            if (item && $rootScope.currentState && $rootScope.currentState.name && $rootScope.currentState.name != "schedules") {
                if (item.DataObject) {
                    var startDate = moment.utc(item.DataObject.StartDate).local();
                    var endDate = moment.utc(item.DataObject.EndDate).local();
                    item.subTitle = "Appointment scheduled  for Patient <b>" +
                        item.DataObject.PatientName + "</b> On <b>" + moment(startDate).format("MMMM DD YYYY HH:mm") + "</b>";
                    $scope.notificationList.push(item);
                    $scope.$apply();
                }
            }
        }*/
    });

    function removeNotifs(type) {
        if ($scope.notificationList && $scope.notificationList.length > 0) {
            var i = $scope.notificationList.length;
            while (i--) {
                var obj = $scope.notificationList[i];
                if (obj.NotifType === type) {
                    $scope.notificationList.splice(i, 1);
                }
            }
        }
    }

    $scope.$on('onClearNotif', function (event, item) {
        removeNotifs(item);
    });


    $scope.elasticSearchButtonClick = function () {
        if ($scope.elasticSearch.searchPhrase === "")
            return;
        $("#elastic_search_menu").parent("li").removeClass('open');
        $state.go('elastic-search', { filter: $scope.elasticSearch.searchPhrase });
        $scope.elasticSearch.searchPhrase = "";
    }


    function initializeOnLogin() {
        currentUser = auth.getCurrentUser();
        TwilioChatFactory.login();
        VirgilFactory.init();
        SignalrClient.connect();

        
        
    }
    function onLogout() {

        TwilioChatFactory.shutDown();
        VirgilFactory.shutDown();
        SignalrClient.shutDown();
        currentUser = null;
        $scope.notificationList = [];
    }

    if (auth.isAuthenticated()) {
        initializeOnLogin();
    }

    function refreshMenu() {
        $scope.menu = [{
            'title': "home",
            "iconClass": "icon-patients",
            'state': "home",
            associatedState: ['home_care_managment', ],
        }, {
            'title': "schedule",
            "iconClass": "icon-patients",
            //'state': "sample-schedules",
            'state': "schedules",
            "name": "Schedule",  // Name field used for access rights
            associatedState: [],
        }, {
            'title': "patients",
            "iconClass": "icon-patients",
            'state': "patients",
            "name": "Patients",
            associatedState: ["patients", "patientsEdit", "patientsEdit.patient-case-add", "patientCaseEdit", ],
        }, {
            'title': "resources",
            "iconClass": "icon-resource",
            'state': "resource",
            "name": "Resources",
            associatedState: ["resource", "resourceEdit", "rolesAvailabilityVsDemand", "filters",
                "assignment", "assignmentEdit"],
        }, {
            'title': "referral",
            "iconClass": "icon-referral",
            'state': "referralList",
            "name": "Referral",
            associatedState: [],
        }, {
            'title': "library",
            "iconClass": "icon-patients",
            'state': "",
            "name": "Library",
            children: [{
                'title': "Master Templates",
                'state': "master-templates",
                "iconClass": "",
                "name": "Library",//(Right access name)
                "role": ""
            },
            {
                'title': "Favorites",
                'state': "favorite-templates",
                "iconClass": "",
                "name": "Library",//(Right access name)
                "role": ""
            }
            ],
            associatedState: ["master-templates", "favorites"],
        }, {
            'title': "Communication",
            "iconClass": "icon-patients",
            "name": "Communications",
            "specialType": true,
            associatedState: [],
        }, {
            'title': "billing",
            "iconClass": "icon-patients",
            'state': "billing",
            "name": "Billing",
            associatedState: [],
        }, {
            'title': "dashboard",
            "iconClass": "icon-patients",
            'state': "dashboard",
            "name": "Dashboards",
            associatedState: [],
        }, ];
    }
    refreshMenu();



    $rootScope.$on("edz:success-login", function () {
        refreshMenu();
        initializeOnLogin();
    });

    $rootScope.$on("edz:success-logout", onLogout);

    $scope.ExportImportIsExpand = false;
    $scope.uploadResoruces = function (file, $invalidFile) {
        if ($invalidFile && $invalidFile.length > 0) {
            toaster.pop("error", "", "Max File Size 5MB");
        }
        if (!file)
            return;
        resourceService.uploadResources(uploader, file, function (response) {
            if (response.data) {
                toaster.pop("success", "", response.data);
            } else {
                toaster.pop("success", "", "Resource import finished");
            }
            if (resourceService.reloadCommand.reload)
                resourceService.reloadCommand.reload();
        });
    };
    $scope.exportResources = function () {
        window.open("api/Resources/export-all/", "_blank", "");
    };
    $scope.isAuthenticated = auth.isAuthenticated;
    $scope.isCollapsed = true;
    $scope.homePage = function () {
        var homePage = permissionWorker.getHomePageState();
        if (homePage.hasParams) {
            $state.go(homePage.state, homePage.params);
        } else {
            $state.go(homePage.state);
        }
    };
    $scope.isAdmin = auth.isAdmin;
    $scope.logout = function () {
        auth.logout();
        $scope.currentUser = auth.getCurrentUser();
    };
    $scope.currentUser = auth.getCurrentUser();
    $scope.isActive = function (state, associateState) {
        associateState = associateState || [];
        return state === $state.current.name || _.includes(associateState, $state.current.name);
    };

    $scope.isVisibleParentMenu = function (parentItem) {
        if (parentItem && parentItem.children) {
            var result = false;
            _.forEach(parentItem.children, function (child) {
                result = result && (parentItem.name === undefined || permissionWorker.checkPermissionPageRead(parentItem.name));
            })
            return result;
        }
        return false;
    }


    $scope.isVisibleMainMenu = function (menuItem, parentItem) {
        /// TODO repait this in future 
        if (auth.isAdmin()) {
            if (menuItem.name === "Calendar") { return false; }
            else { /*parentItem.visible = true;*/ return true; }
        }

        if (menuItem.state == 'dashboard') {
            menuItem.visible = true;
            return true;
        }

        if (menuItem.state === "report") {
            return permissionWorker.checkReportsPermission();
        }

        if (menuItem.children && menuItem.children.length > 0) {
            var res = false;
            _.forEach(menuItem.children, function (child) {
                res = (res || (child.name === undefined || permissionWorker.checkPermissionPageRead(child.name)))
            });
            return res;
        }

        var result = (menuItem.name === undefined || permissionWorker.checkPermissionPageRead(menuItem.name));

        return result;
    };

    $scope.isGroupVisible = function (group) {
        return _.some(group.items, "visible");
    }
    $scope.isVisible = function (menuItem) {
        if (auth.isAdmin()) {
            menuItem.visible = true;
            return;
        }
        menuItem.visible = permissionWorker.checkPermissionPickList(menuItem.name);
    };

    $rootScope.$on("update-menu", function () {
        _.forEach($scope.menuGroups, function (item) {
            _.forEach(item.items, $scope.isVisible);
        });
    });

    _.forEach($scope.menuGroups, function (item) {
        _.forEach(item.items, $scope.isVisible);
    });



}
