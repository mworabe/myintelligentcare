﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ExerciseActivityMappingExtensions
    {
        public static DbModelBuilder MapExerciseActivity(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ExerciseActivity>().ToTable("exercise_activities").HasKey(i => i.Id);

            modelBuilder.Entity<ExerciseActivity>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ExerciseActivity>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ExerciseActivity>()
                 .Property(p => p.Name)
                 .HasColumnName("name")
                 .IsOptional();

            modelBuilder.Entity<ExerciseActivity>()
                .HasMany<Exercise_ExerciseActivity>(p => p.exerciseactivities)
                .WithOptional()
                .HasForeignKey(p => p.activity_Id);

            return modelBuilder;
        }
    }
}
