﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('performanceAnalytics', function ($compile) {
    return {
        scope: {
            config: "=config",
            //loadingBarFlag: "=isLoading",
            patientId: "=",
            caseId: "="
        },
        templateUrl: "Client/app/controllers/patient-cases/performance-analytics/performance-analytics.html",
        controller: ['$scope',
                    'PerformanceAnylyticService',
                    'toaster',
                    '$timeout',
        function ($scope, PerformanceAnylyticService, toaster, $timeout) {

            $scope.dailyExerciseData = {
                series: ['Daily Exercise'],
                data: [[]],
                lables: [],
            }

            $scope.painAnalysis = {
                series: ['Pain'],
                data: [[]],
                lables: [],
            }


           

            $scope.dataList = [];

            function processData() {
                if ($scope.dataList) {
                    var painAnalysisData = [];
                    for (var i = 0; i < $scope.dataList.length; i++) {
                        var obj = $scope.dataList[i];
                        if (obj.dates) {
                            var dates = moment(obj.dates);
                            var formatedDate = dates.format("MM/DD");
                            $scope.painAnalysis.lables.push(formatedDate);
                            $scope.painAnalysis.data[0].push(obj.painRating);

                            $scope.dailyExerciseData.lables.push(formatedDate);
                            $scope.dailyExerciseData.data[0].push(obj.totalTime);

                            
                        }
                    }
                }
            }


            function loadData() {
                
                var req = {
                    caseid: $scope.caseId,
                    patientId: $scope.patientId,
                }
                
                var date = new Date();
                req.from = moment(date).weekday(1).toDate();
                req.to = moment(date).weekday(7).toDate();
                //req.from = moment(date).startOf("year").toDate();
                //req.to = moment(date).endOf("year").toDate();

                $scope.isLoading = PerformanceAnylyticService.getData(req).$promise.then(
                                   function (response) {
                                       $scope.dataList = response;
                                       processData();
                                   },
                                   function (error) {
                                   });

            }
            loadData();

        }]
    }
});



