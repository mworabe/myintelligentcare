namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedTimeoffBenefitsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.resource_timeoff_benefits",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(),
                    timeoff_type_id = c.Long(),
                    total_balance = c.Decimal(precision: 18, scale: 2),
                    balance = c.Decimal(precision: 18, scale: 2),
                    accrued_current = c.Decimal(precision: 18, scale: 2),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id)
                .ForeignKey("dbo.leave_types", t => t.timeoff_type_id)
                .ForeignKey("dbo.resources", t => t.timeoff_type_id)
                .Index(t => t.resource_id)
                .Index(t => t.timeoff_type_id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.resource_timeoff_benefits", "timeoff_type_id", "dbo.resources");
            DropForeignKey("dbo.resource_timeoff_benefits", "timeoff_type_id", "dbo.leave_types");
            DropForeignKey("dbo.resource_timeoff_benefits", "resource_id", "dbo.resources");
            DropIndex("dbo.resource_timeoff_benefits", new[] { "timeoff_type_id" });
            DropIndex("dbo.resource_timeoff_benefits", new[] { "resource_id" });
            DropTable("dbo.resource_timeoff_benefits");
        }
    }
}
