﻿using PatientCare.Core.Enums;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.ExerciseMedias
{
    [DataContract]
    public class ExerciseMediaMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        //[DataMember]
        //public string OriginalName { get; set; }

        [DataMember]
        public MediaType? MediaType { get; set; }

        [DataMember]
        public string MediaURL { get; set; }


        [DataMember]
        public string ThumbURL { get; set; }

        [DataMember]
        public string ThumbImageName { get; set; }

    }
}