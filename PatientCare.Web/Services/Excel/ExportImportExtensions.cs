﻿using System;
using PatientCare.Core.Entities;

namespace PatientCare.Web.Services.Excel
{
    internal static class ExportImportExtensions
    {
        public static string ToExportResourceName(this Resource resource)
        {
            return resource == null ? String.Empty : String.Format("{0} {1}", resource.FirstName, resource.LastName);
        }
      
        public const string NoNeedToChangeIndicator = "0";
        public const string NeedToChangeIndicator = "1";
        public const string NeedToChangeColumnName = "need_change";

        public static readonly string[] ResourceHeaders = { 
                                            "first_name",
                                            "last_name", 
                                            "job_title",
                                            "address",
                                            "city",
                                            "state_province",
                                            "resource_company",
                                            "years_employed",
                                            "grade_level",
                                            /*10*/
                                            "years_of_college",
                                            "college_degree",
                                            "time_zone_info",
                                            "location",
                                            "valid_passport",
                                            "available_to_travel",
                                            "ever_been_convicted_of_crime",
                                            "information_certified_by_employer",
                                            "ethnicity",
                                            "armed_forces",
                                            /*20*/
                                            "armed_forces_branch",
                                            "drug_test",
                                            "credit_report_ok",
                                            "proof_of_citizenship",
                                            "hourly_rate",
                                            "resource_team",
                                            "ssn",
                                            "employee",
                                            "range",
                                            "division",
                                            /*30*/
                                            "department",
                                            "photo_file_name",
                                            "cv-file-name",
                                            "works_well_alone",
                                            "works_well_in_team",
                                            "employment_type",
                                            "productivity",
                                            "rating",
                                            "motor-vehicle-report",
                                            "motor-vehicle-report-other",
                                            /*40*/
                                            "professional-reference-checks",
                                            "professional-reference-checks-other",
                                            "employment-eligibility-verification",
                                            "employment-eligibility-verification-other",
                                            "international-work-history",
                                            "international-work-history-other",
                                            "credential-verifications",
                                            "credential-verifications-other",
                                            "country",
                                            "armed_forces_country",
                                            /*50*/
                                            "form-i-9",
                                            "form-i-9-other",
                                            "form-e-verify",
                                            "form-e-verify-other",
                                            "start_date",
                                            "end_date",
                                            "gender",
                                            "martial_status",
                                            "birth_date",
                                            "phone_number",
                                            /*60*/
                                            "resource_type",
                                            "bonus_or_other_pay",
                                            "address2",
                                            "zipcode",
                                            "annual_salary",
                                            "phone_number2",
                                            "emergency_contact_name",
                                            "emergency_contact_phone",
                                            "relationship",
                                            "supervisor",
                                            /*70*/
                                            "Active"                
                                        };

    }
    public enum ImportResult
    {
        Success = 0,
        WrongFormat = 1,
        NoData = 3,
        WrongData = 4
    }

    public enum StoreResult
    {
        Success = 0,
        Error = 1
    }
}