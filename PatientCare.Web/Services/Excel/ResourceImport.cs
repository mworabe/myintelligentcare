﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Enums;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using OfficeOpenXml;

namespace PatientCare.Web.Services.Excel
{
    public static class ResourceImport
    {
        public static ImportResult Import(ExcelPackage package, List<ResourceImportDto> records)
        {
            using (package)
            {
                var workbook = package.Workbook;
                if (workbook != null)
                {
                    if (workbook.Worksheets.Count > 0)
                    {
                        var worksheet = workbook.Worksheets.First();
                        if (!CheckFileFormat(worksheet))
                        {
                            return ImportResult.WrongFormat;
                        }
                        var lastRow = GetLastRowNumber(worksheet);
                        for (var row = 2; row <= lastRow; row++)
                        {
                            if (RequireMinimumData(worksheet, row))
                            {
                                return ImportResult.WrongData;
                            }

                            records.Add(GetResourceFromRow(worksheet, row));
                        }
                        if (records.Count == 0)
                        {
                            return ImportResult.NoData;
                        }
                        return ImportResult.Success;
                    }
                }
                return ImportResult.NoData;
            }
        }

        public static async Task<Tuple<StoreResult, string>> StoreAsync(IEnumerable<ResourceImportDto> resourceDtos,
            IResourceService resourceService,
            IPicklistRepository<Division> divisionRepository,
            IPicklistRepository<Department> departmentRepository,
            IPicklistRepository<Country> countryRepository,
            IPicklistRepository<ResourceCompany> resourceCompanyRepository,
            IPicklistRepository<JobTitle> jobTitleRepository,
            IPicklistRepository<City> cityRepository,
            IPicklistRepository<State> stateRepository,
            IResourceTeamRepository resourceTeamRepository,
            IResourceRepository resourceRepository)
        {
            var divisions = divisionRepository.List(readOptions: new ReadOptions<Division>().AsReadOnly()).ToList();
            var departments = departmentRepository.List(readOptions: new ReadOptions<Department>().AsReadOnly()).ToList();
            var countries = countryRepository.List(readOptions: new ReadOptions<Country>().AsReadOnly()).ToList();
            var resourceCompanies = resourceCompanyRepository.List(readOptions: new ReadOptions<ResourceCompany>().AsReadOnly()).ToList();
            var jobTitles = jobTitleRepository.List(readOptions: new ReadOptions<JobTitle>().AsReadOnly()).ToList();
            var cities = cityRepository.List(readOptions: new ReadOptions<City>().AsReadOnly()).ToList();
            var states = stateRepository.List(readOptions: new ReadOptions<State>().AsReadOnly()).ToList();
            var teams = resourceTeamRepository.List(readOptions: new ReadOptions<ResourceTeam>().AsReadOnly()).ToList();
            var existingResources = resourceRepository.List(readOptions: new ReadOptions<Resource>().AsReadOnly()).ToList();
            var resources = new List<Resource>();
            var skipedList = new List<string>();

            Resource exsiting = new Resource();
            try
            {
                foreach (var dto in resourceDtos)
                {
                    var resource = Mapper.Map<ResourceImportDto, Resource>(dto);
                    if ((dto.Ssn != null && dto.Ssn != "") && existingResources.Any(x => x.Ssn == dto.Ssn))
                    {
                        if (existingResources.Any(x => x.Ssn == dto.Ssn))
                        {
                            return Tuple.Create(StoreResult.Error, string.Format("SSN is already exists with another resource."));
                        }
                    }

                    if (existingResources.Any(x => x.EmployeeId == dto.EmployeeId))
                    {
                        return Tuple.Create(StoreResult.Error, string.Format("Employeeid is already exists with another resource."));
                    }

                    //existingResources.FirstOrDefault(
                    //    x =>
                    //        x.Ssn.Equals(dto.Ssn, StringComparison.InvariantCultureIgnoreCase) &&
                    //        x.EmployeeId.Equals(dto.EmployeeId, StringComparison.InvariantCultureIgnoreCase));

                    if (exsiting != null && dto.NeedChangeExisting) //if (exsiting != null && !dto.NeedChangeExisting) Old Condition
                    {
                        skipedList.Add(string.Format("{0} {1}", dto.FirstName, dto.LastName));
                    }
                    resource.Id = exsiting != null ? exsiting.Id : 0;
                    var division = (dto.Division != null) ? divisions.FirstOrDefault(x => x.Name == dto.Division.Trim()) : divisions.FirstOrDefault(x => x.Name == dto.Division);
                    var department = departments.FirstOrDefault(x => x.Name == dto.Department.Trim());
                    var country = (dto.Country != null) ? countries.FirstOrDefault(x => x.Name == dto.Country.Trim()) : countries.FirstOrDefault(x => x.Name == dto.Country);
                    var resourceCompany = (dto.ResourceCompany != null) ? resourceCompanies.FirstOrDefault(x => x.Name == dto.ResourceCompany.Trim()) : resourceCompanies.FirstOrDefault(x => x.Name == dto.ResourceCompany);
                    var jobTitle = (dto.JobTitle != null) ? jobTitles.FirstOrDefault(x => x.Name == dto.JobTitle.Trim()) : jobTitles.FirstOrDefault(x => x.Name == dto.JobTitle);
                    var supervisor = (dto.Supervisor != null) ? existingResources.FirstOrDefault(x => x.Name == dto.Supervisor.Trim()) : existingResources.FirstOrDefault(x => x.Name == dto.Supervisor);
                    var city = (dto.City != null) ? cities.FirstOrDefault(x => x.Name == dto.City.Trim()) : cities.FirstOrDefault(x => x.Name == dto.City);
                    var state = (dto.StateProvince != null) ? states.FirstOrDefault(x => x.Name == dto.StateProvince.Trim()) : states.FirstOrDefault(x => x.Name == dto.StateProvince);
                    var armedForcesCountry = (dto.ArmedForcesCountry != null) ? countries.FirstOrDefault(x => x.Name == dto.ArmedForcesCountry.Trim()) : countries.FirstOrDefault(x => x.Name == dto.ArmedForcesCountry);
                    var resourceTeam = (dto.ResourceTeam != null) ? teams.FirstOrDefault(x => x.Name == dto.ResourceTeam.Trim()) : teams.FirstOrDefault(x => x.Name == dto.ResourceTeam);
                    if (
                         //division == null ||
                         department == null
                        // || country == null
                        || resourceCompany == null
                        || jobTitle == null
                        || resource.StartDate == default(DateTime)
                        //|| supervisor == null
                        )
                    {
                        continue;
                    }
                    resource.DivisionId = (division != null) ? division.Id : (long?)null;
                    resource.Division = null;
                    resource.DepartmentId = (department != null) ? department.Id : (long?)null;
                    resource.Department = null;
                    resource.CountryId = (country != null) ? country.Id : (long?)null;
                    resource.Country = null;                  
                    resource.JobTitleId = (jobTitle != null) ? jobTitle.Id : (long?)null;
                    resource.JobTitle = null;
                    resource.CityId = (city != null) ? city.Id : (long?)null;
                    resource.City = null;
                    resource.StateId = (state != null) ? state.Id : (long?)null;
                    resource.State = null;
                    resource.SupervisorId = (supervisor != null) ? supervisor.Id : (long?)null;
                    resource.Supervisor = null;
                    resource.ArmedForcesCountryId = (armedForcesCountry != null) ? armedForcesCountry.Id : (long?)null;
                    resource.ArmedForcesCountry = null;
                    resource.ResourceTeamId = (resourceTeam != null) ? resourceTeam.Id : (long?)null;
                    resource.ResourceTeam = null;

                    if (resource.EndDate == default(DateTime))
                    {
                        resource.EndDate = null;
                    }
                    if (resource.BirthDate == default(DateTime))
                    {
                        resource.BirthDate = null;
                    }
                    resources.Add(resource);
                }
            }
            // ReSharper disable once UnusedVariable
            catch
            {
                return Tuple.Create(StoreResult.Error, "");
            }
            if (await resourceService.ImportAsync(resources))
            {
                var message = string.Empty;
                if (skipedList.Any())
                {
                    message = "These resources were skipped while importing: " + string.Join(", ", skipedList);
                }
                return Tuple.Create(StoreResult.Success, message);
            }
            return Tuple.Create(StoreResult.Error, "");
        }

        private static ResourceImportDto GetResourceFromRow(ExcelWorksheet worksheet, int row)
        {
            // ReSharper disable once UseObjectOrCollectionInitializer
            var timeZoneNames = TimeZoneInfo.GetSystemTimeZones().Select(x => new { name = x.DisplayName, id = x.Id }).ToList();
            var startColumnShift = GetStartColumnShift(worksheet);
            var resourceDto = new ResourceImportDto();
            if (startColumnShift == 1)
            {
                resourceDto.NeedChangeExisting =
                    worksheet.Cells[row, 1].GetValue<string>().Equals(ExportImportExtensions.NeedToChangeIndicator);
            }
            resourceDto.FirstName = worksheet.Cells[row, 1 + startColumnShift].GetValue<string>();
            resourceDto.LastName = worksheet.Cells[row, 2 + startColumnShift].GetValue<string>();
            resourceDto.JobTitle = worksheet.Cells[row, 3 + startColumnShift].GetValue<string>();
            resourceDto.Address = worksheet.Cells[row, 4 + startColumnShift].GetValue<string>();
            resourceDto.City = worksheet.Cells[row, 5 + startColumnShift].GetValue<string>();
            resourceDto.StateProvince = worksheet.Cells[row, 6 + startColumnShift].GetValue<string>();
            resourceDto.ResourceCompany = worksheet.Cells[row, 7 + startColumnShift].GetValue<string>();
            resourceDto.YearsEmployed = worksheet.Cells[row, 8 + startColumnShift].GetValue<int>();
            resourceDto.GradeLevel = worksheet.Cells[row, 9 + startColumnShift].GetValue<string>();

            resourceDto.YearsOfCollege = worksheet.Cells[row, 10 + startColumnShift].GetValue<int>();
            resourceDto.CollegeDegree = worksheet.Cells[row, 11 + startColumnShift].GetValue<bool>();
            var timeZoneInfo = timeZoneNames.FirstOrDefault(x => x.name.Equals(worksheet.Cells[row, 12 + startColumnShift].GetValue<string>(), StringComparison.InvariantCultureIgnoreCase));
            resourceDto.TimeZoneInfo = timeZoneInfo != null ? timeZoneInfo.id : string.Empty;
            resourceDto.Location = worksheet.Cells[row, 13 + startColumnShift].GetValue<string>();
            resourceDto.ValidPassport = worksheet.Cells[row, 14 + startColumnShift].GetValue<bool>();
            resourceDto.AvailableToTravel = worksheet.Cells[row, 15 + startColumnShift].GetValue<bool>();
            resourceDto.EverBeenConvictedOfACrime = worksheet.Cells[row, 16 + startColumnShift].GetValue<bool>();
            resourceDto.InformationCertifiedByEmployer = worksheet.Cells[row, 17 + startColumnShift].GetValue<bool>();
            resourceDto.Ethnicity = worksheet.Cells[row, 18 + startColumnShift].GetValue<string>();
            resourceDto.ArmedForces = worksheet.Cells[row, 19 + startColumnShift].GetValue<bool>();

            resourceDto.ArmedForcesBranch = worksheet.Cells[row, 20 + startColumnShift].GetValue<string>();
            resourceDto.DrugTest = worksheet.Cells[row, 21 + startColumnShift].GetValue<string>();
            resourceDto.CreditReportOk = worksheet.Cells[row, 22 + startColumnShift].GetValue<string>();
            resourceDto.ProofOfCitizenship = worksheet.Cells[row, 23 + startColumnShift].GetValue<string>();
            resourceDto.HourlyRate = worksheet.Cells[row, 24 + startColumnShift].GetValue<decimal>();
            resourceDto.ResourceTeam = worksheet.Cells[row, 25 + startColumnShift].GetValue<string>();
            resourceDto.Ssn = (worksheet.Cells[row, 26 + startColumnShift].GetValue<string>() ?? string.Empty).Replace("-", "");
            resourceDto.EmployeeId = worksheet.Cells[row, 27 + startColumnShift].GetValue<string>();
            RangeTypeEnum rangeType;
            resourceDto.Range = Enum.TryParse(worksheet.Cells[row, 28 + startColumnShift].GetValue<string>(), true, out rangeType) ? rangeType : RangeTypeEnum.L;
            resourceDto.Division = worksheet.Cells[row, 29 + startColumnShift].GetValue<string>();

            resourceDto.Department = worksheet.Cells[row, 30 + startColumnShift].GetValue<string>();
            resourceDto.PhotoFileName = worksheet.Cells[row, 31 + startColumnShift].GetValue<string>();
            resourceDto.CvFileName = worksheet.Cells[row, 32 + startColumnShift].GetValue<string>();
            resourceDto.WorksWellAlone = worksheet.Cells[row, 33 + startColumnShift].GetValue<bool>();
            resourceDto.WorksWellInTeam = worksheet.Cells[row, 34 + startColumnShift].GetValue<bool>();
            EmploymentTypeEnum employmentType;
            resourceDto.EmploymentType = Enum.TryParse(worksheet.Cells[row, 35 + startColumnShift].GetValue<string>(), true, out employmentType) ? employmentType : EmploymentTypeEnum.Hourly;
            resourceDto.Productivity = worksheet.Cells[row, 36 + startColumnShift].GetValue<double>();
            resourceDto.Rating = worksheet.Cells[row, 37 + startColumnShift].GetValue<double>();
            YesNoOtherEnum yesNoOtherEnum;
            resourceDto.MotorVehicleReport = Enum.TryParse(worksheet.Cells[row, 38 + startColumnShift].GetValue<string>(), true, out yesNoOtherEnum) ? yesNoOtherEnum : YesNoOtherEnum.NA;
            resourceDto.MotorVehicleReportOther = worksheet.Cells[row, 39 + startColumnShift].GetValue<string>();

            resourceDto.ProfessionalReferenceChecks = Enum.TryParse(worksheet.Cells[row, 40 + startColumnShift].GetValue<string>(), true, out yesNoOtherEnum) ? yesNoOtherEnum : YesNoOtherEnum.NA;
            resourceDto.ProfessionalReferenceChecksOther = worksheet.Cells[row, 41 + startColumnShift].GetValue<string>();
            resourceDto.EmploymentEligibilityVerification = Enum.TryParse(worksheet.Cells[row, 42 + startColumnShift].GetValue<string>(), true, out yesNoOtherEnum) ? yesNoOtherEnum : YesNoOtherEnum.NA;
            resourceDto.EmploymentEligibilityVerificationOther = worksheet.Cells[row, 43 + startColumnShift].GetValue<string>();
            resourceDto.InternationalWorkHistory = Enum.TryParse(worksheet.Cells[row, 44 + startColumnShift].GetValue<string>(), true, out yesNoOtherEnum) ? yesNoOtherEnum : YesNoOtherEnum.NA;
            resourceDto.InternationalWorkHistoryOther = worksheet.Cells[row, 45 + startColumnShift].GetValue<string>();
            resourceDto.CredentialVerifications = Enum.TryParse(worksheet.Cells[row, 46 + startColumnShift].GetValue<string>(), true, out yesNoOtherEnum) ? yesNoOtherEnum : YesNoOtherEnum.NA;
            resourceDto.CredentialVerificationsOther = worksheet.Cells[row, 47 + startColumnShift].GetValue<string>();
            resourceDto.Country = worksheet.Cells[row, 48 + startColumnShift].GetValue<string>();
            resourceDto.ArmedForcesCountry = worksheet.Cells[row, 49 + startColumnShift].GetValue<string>();

            resourceDto.FormI9 = Enum.TryParse(worksheet.Cells[row, 50 + startColumnShift].GetValue<string>(), true, out yesNoOtherEnum) ? yesNoOtherEnum : YesNoOtherEnum.NA;
            resourceDto.FormI9Other = worksheet.Cells[row, 51 + startColumnShift].GetValue<string>();
            resourceDto.FormEVerify = Enum.TryParse(worksheet.Cells[row, 52 + startColumnShift].GetValue<string>(), true, out yesNoOtherEnum) ? yesNoOtherEnum : YesNoOtherEnum.NA;
            resourceDto.FormEVerifyOther = worksheet.Cells[row, 53 + startColumnShift].GetValue<string>();
            resourceDto.StartDate = worksheet.Cells[row, 54 + startColumnShift].GetValue<DateTime>();
            resourceDto.EndDate = worksheet.Cells[row, 55 + startColumnShift].GetValue<DateTime>();
            GenderEnum gender;
            resourceDto.Gender = Enum.TryParse(worksheet.Cells[row, 56 + startColumnShift].GetValue<string>(), true, out gender) ? gender : GenderEnum.Male;
            MartialStatusEnum martialStatus;
            resourceDto.MartialStatus = Enum.TryParse(worksheet.Cells[row, 57 + startColumnShift].GetValue<string>(), true, out martialStatus) ? martialStatus : MartialStatusEnum.Single;
            resourceDto.BirthDate = worksheet.Cells[row, 58 + startColumnShift].GetValue<DateTime>();
            resourceDto.PhoneNumber = worksheet.Cells[row, 59 + startColumnShift].GetValue<string>();
            ResourceTypeEnum resourceType;
            resourceDto.ResourceType = Enum.TryParse(worksheet.Cells[row, 60 + startColumnShift].GetValue<string>(), true, out resourceType) ? resourceType : ResourceTypeEnum.Internal;

            resourceDto.BonusOrOtherPay = worksheet.Cells[row, 61 + startColumnShift].GetValue<decimal>();
            resourceDto.Address2 = worksheet.Cells[row, 62 + startColumnShift].GetValue<string>();
            resourceDto.ZipCode = worksheet.Cells[row, 63 + startColumnShift].GetValue<string>();
            resourceDto.Salary = worksheet.Cells[row, 64 + startColumnShift].GetValue<decimal>();
            resourceDto.PhoneNumber2 = worksheet.Cells[row, 65 + startColumnShift].GetValue<string>();
            resourceDto.EmergencyContactName = worksheet.Cells[row, 66 + startColumnShift].GetValue<string>();
            resourceDto.EmergencyContactPhoneNumber = worksheet.Cells[row, 67 + startColumnShift].GetValue<string>();

            resourceDto.Relationship = worksheet.Cells[row, 68 + startColumnShift].GetValue<RelationshipEnum>();
            resourceDto.Supervisor = worksheet.Cells[row, 69 + startColumnShift].GetValue<string>();

            resourceDto.Active = worksheet.Cells[row, 70 + startColumnShift].GetValue<bool>();

            return resourceDto;
        }

        private static bool RequireMinimumData(ExcelWorksheet worksheet, int row)
        {
            try
            {
                var startColumnShift = GetStartColumnShift(worksheet);

                return worksheet.Cells[row, 1 + startColumnShift].Value == null        // First name
                       || worksheet.Cells[row, 2 + startColumnShift].Value == null     // Last name
                                                                                       //   || worksheet.Cells[row, 48 + startColumnShift].Value == null    // Country
                       || worksheet.Cells[row, 27 + startColumnShift].Value == null    // EmployeeId
                       || worksheet.Cells[row, 3 + startColumnShift].Value == null     // JobTitle
                       || worksheet.Cells[row, 54 + startColumnShift].Value == null    // StartDate
                                                                                       // || worksheet.Cells[row, 26 + startColumnShift].Value == null    // Ssn
                       || worksheet.Cells[row, 7 + startColumnShift].Value == null     // ResourceCompany
                                                                                       //  || worksheet.Cells[row, 35 + startColumnShift].Value == null    // EmploymentType
                       || worksheet.Cells[row, 60 + startColumnShift].Value == null    // ResourceType
                                                                                       //   || worksheet.Cells[row, 12 + startColumnShift].Value == null    // TimeZoneInfo
                                                                                       //     || worksheet.Cells[row, 29 + startColumnShift].Value == null    // Division
                       || worksheet.Cells[row, 30 + startColumnShift].Value == null;   // Department
            }
            // ReSharper disable once UnusedVariable
            catch
            {
                return false;
            }
        }

        private static int GetStartColumnShift(ExcelWorksheet worksheet)
        {
            var firstCell = worksheet.Cells[1, 1].Value ?? string.Empty;
            return firstCell.Equals("first_name") ? 0 : 1;
        }

        private static bool CheckFileFormat(ExcelWorksheet worksheet)
        {
            var startColumnShift = GetStartColumnShift(worksheet);
            using (var headers = worksheet.Cells[1, 1 + startColumnShift, 1, worksheet.Dimension.End.Column + startColumnShift])
            {
                return ExportImportExtensions.ResourceHeaders.Skip(startColumnShift).All(x => headers.Any(y => y.Value.Equals(x)));
            }
        }

        private static int GetLastRowNumber(ExcelWorksheet sheet)
        {
            var lastRow = sheet.Dimension.End.Row;
            while (lastRow >= 1)
            {
                var cell = sheet.Cells[lastRow, 1];
                if (cell.Value != null)
                {
                    break;
                }
                lastRow--;
            }
            return lastRow;
        }

        private const int LastColumnNumber = 67;

    }

}