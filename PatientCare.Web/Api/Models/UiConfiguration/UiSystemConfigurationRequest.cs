﻿
using PatientCare.Web.Api.Models.DropDownConfigs;

namespace PatientCare.Web.Api.Models.UiConfiguration
{
    public class UiSystemConfigurationRequest
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string KeyType { get; set; }

        public DropDownConfigRequest[] ComplxityDataList { get; set; }
        public DropDownConfigRequest[] SeverityDataList { get; set; }
    }
}