﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("edzTabBody", [function () {
            return {
                templateUrl: "Client/app/components/directives/tabs/tabBody/tabBody.html",
                restrict: "E",
                replace: false,
                controllerAs: "tabBodyCtrl",
                transclude: true,
                require: "^edzTabBodies",
                scope: {
                    tabId: "@edzTabId",
                    contentClass: "@edzTabContentClass"
                },
                link: function ($scope, iElement, iAttrs, controller) {
                    $scope.contentClass = $scope.contentClass === undefined ? "tab-inner" : $scope.contentClass;
                    $scope.tabBodyCheck = function() {
                        return $scope.tabId === controller.currentTab;
                    }
                }
            };
        }
    ]);