﻿"use strict";
var app = angular.module("EDZoutstaffingPortalApp");

app.directive("projectDynamicField", ["ConfigLayout", "$filter", function (configLayoutService, $filter) {
    return {
        templateUrl: "Client/app/controllers/Elastic-Search/project-dynamic-field-view.html",
        restrict: "E",
        replace: false,
        scope: {
            projectFields: '=ngProjectFields',
            highLightSearchedText: "=ngHighlightFunc",
        },
        link: function (scope) {
            scope.projectLimitData = 4;
            scope.isProjectRowExpanded = false;
            scope.matchingData = scope.projectFields;

            var resourceFieldList = configLayoutService.getProjectFields();
            var resourceTabList = configLayoutService.getProjectTab();
            /*  Project Configuration does not have task tab */
            resourceTabList.push({ id: "task", name: "Task" });

            scope.isKeyVisible = function (key, value, tabId) {
                if (!value)
                    return false;
                else if ((key === 'id' || key === 'name' || key === 'description') && tabId === "projectDetails")
                    return false;
                else
                    return true;
            }

            scope.expandButtonClick = function () {
                scope.isProjectRowExpanded = !scope.isProjectRowExpanded;
                (scope.isProjectRowExpanded) ? scope.projectLimitData = scope.matchingData.length : scope.projectLimitData = 4;
                event.stopPropagation();
            }

            function getProjectFieldsByTab(tabId) {
                if (tabId === "personalInformation")
                    return resourceFieldList;
                else if (tabId === "task")
                    return [{
                        id: "taskName",
                        name: "Name",
                    }, {
                        id: "description",
                        name: "Description"
                    }];
                else if (tabId === "risks")
                    return [{
                        name: "Description",
                        id: "description",
                    }];
                else if (tabId === "actionItems")
                    return [{
                        name: "Description",
                        id: "description",
                    }];
                else if (tabId === "issues")
                    return [{
                        name: "Description",
                        id: "description",
                    }];
                else if (tabId === "decisions")
                    return [{
                        name: "Description",
                        id: "description",
                    }];
                else if (tabId === "assumptions")
                    return [{
                        name: "Description",
                        id: "description",
                    }];
                else if (tabId === "defects")
                    return [{
                        name: "Description",
                        id: "description",
                    }];
            }

            scope.getResourcecKeyLabel = function (tabId, key) {
                var resourcecFields = resourceFieldList;
                if (tabId && (tabId !== "projectDetails")) {
                    resourcecFields = getProjectFieldsByTab(tabId)
                }
                var field = _.find(resourcecFields, function (_field) { return _field.id === key });
                var tab = _.find(resourceTabList, function (_tab) { return _tab.id === tabId });
                var returnStr = "";
                if (tab && (tabId !== "projectDetails"))
                    returnStr += $filter('stringCamelCase')(tab.name) + " / ";

                if (field && field.name) {
                    returnStr += field.name;
                } else
                    returnStr += key;
                return returnStr;
            }

            scope.elipssesText = function (value, fieldsCount, fieldId) {
                if (scope.isProjectRowExpanded)
                    return value;
                else {
                    var returnString = (value.length > 50 && fieldsCount > scope.projectLimitData) ? value.substring(0, 50) + "..." : value;
                    return returnString;
                }
            }
            scope.getContainerClass = function () {
                var length = scope.matchingData.length - 1;
                if (length >= 4) {
                    return 'col-md-3';
                } else if (length >= 2) {
                    return 'col-md-6';
                } else {
                    return 'col-md-12';
                }
            }
        }
    }
}
]);