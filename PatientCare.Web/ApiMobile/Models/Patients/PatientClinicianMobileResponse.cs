﻿using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.Patients
{
    [DataContract]
    public class PatientClinicianMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}