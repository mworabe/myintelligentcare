﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.Diagnosiss;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class DiagnosisMapperExtensions
    {
        public static EmitMapperMapperProvider DiagnosisRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Diagnosis, DiagnosisResponse>
                    (new ExtendedMapConfig<Diagnosis, DiagnosisResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Diagnosis[], DiagnosisResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Diagnosis, DiagnosisAutoCompleteResponse>
                    (new ExtendedMapConfig<Diagnosis, DiagnosisAutoCompleteResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Diagnosis[], DiagnosisAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<DiagnosisRequest, Diagnosis>
                    (new ExtendedMapConfig<DiagnosisRequest, Diagnosis>()));

            return mapperProvider;
        }
    }
}