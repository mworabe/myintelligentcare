﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("uiGridTable", [
        "NgTableParams", function (ngTableParams) {
            return {
                templateUrl: "Client/app/components/directives/uiGridTable/uiGridTable-New.html",
                restrict: "E",
                replace: false,
                scope: {
                    _get: "=ngGet",
                    _getOption: "=ngOption",
                    _delete: "=?ngDelete",
                    _update: "=?ngUpdate",
                    _save: "=?ngSave",
                    _headerTemplate: "@?ngHeaderTemplate",
                    _onDataGet: "=?ngOnDataGet",
                    _changeHeaderParams: "=?changeHeaderParams",
                    _onResponceTransform: "=?ngOnResponceTransform",
                    _onRequestTransform: "=?ngOnRequestTransform",
                    _onCommand: "=?ngTableCommand",
                    _data: "=?ngData",
                    _isServerData: "=?ngIsServerData",
                    _bulkEditCallback: "=?bulkEditCallback"
                },
                bindToController: true,
                controllerAs: "uiGridTableCtrl",

                controller: ["Modal", "toaster", "$scope", "$templateRequest", "$compile",
                    function (modal, toaster, $scope, $templateRequest, $compile) {
                        var self = this;
                        self._isServerData = (self._isServerData === undefined || self._isServerData == null) ? true : self._isServerData;
                        self._bulkEditCallback = this._bulkEditCallback || angular.noop;
                        self._onRequestTransform = this._onRequestTransform || angular.noop;
                        self._changeHeaderParams = this._changeHeaderParams || angular.noop;
                        self.prevSortField = undefined;
                        self.prevSortOrder = undefined;

                        this._onDataGet = this._onDataGet || angular.noop;
                        this._onResponceTransform = this._onResponceTransform || angular.noop;
                        var originalData = [];

                        var existNewFiled = false;

                        self.colorPickerConfigs = {
                            position: "bottom"
                        };

                        self.selectForBulkEdit = false;
                        self.bulkEditTextButton = self.selectForBulkEdit == true ? "Open Bulk Editor" : "Bulk Edit";

                        function bulkEdit() {
                            self.selectForBulkEdit = !self.selectForBulkEdit;
                            self.bulkEditTextButton = self.selectForBulkEdit == true ? "Open Bulk Editor" : "Bulk Edit";

                            if (self.selectForBulkEdit === false) {
                                var selected = [];
                                angular.forEach(self.tableParams.data, function (value, key) {
                                    if (value.isSelect)
                                        this.push(value);
                                }, selected);

                                self._bulkEditCallback(selected);

                                angular.forEach(self.tableParams.data, function (value, key) {
                                    value.isSelect = false;
                                }, selected);
                            }
                        }

                        function resetRow(row, rowForm) {
                            row.isEditing = false;
                            rowForm.$setPristine();
                            self.trackedTable.untrack(row);
                            return _.find(originalData, { 'id': row.id });
                        }

                        function cancel(row, rowForm) {
                            if (row.id > 0 || (row.id.length === 36)) {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(row, originalRow);
                            } else {
                                _.remove(self.tableParams.data, { 'id': 0 });
                                existNewFiled = false;
                            }
                        }

                        function del(row) {
                            var rowPick = self.option.rowDeleteName || "name";
                            var message = self.option.formatDelete ? self.option.formatDelete(row) : row[rowPick];
                            modal.confirm.delete(function () {
                                self._delete({ id: row.id }, function () {
                                    _.remove(self.tableParams.settings().data, function (item) {
                                        return row === item;
                                    });
                                    self.totalCount--;
                                    self.tableParams.reload().then(function (data) {
                                        if (data.length === 0 && self.tableParams.total() > 0) {
                                            self.tableParams.page(self.tableParams.page() - 1);
                                            self.tableParams.reload();
                                        }
                                    });
                                    toaster.pop("success", "", "Delete successful");
                                });
                            })("Warning", message);
                        }

                        function save(row, rowForm) {
                            /*if (self.isFinanceCofig) {
                                row.dropDownType = self.option.dropdowntype;
                                row.displayName = (row.displayName) ? row.displayName : row.name;
                                row.value = row.name;
                            }*/
                            self._onRequestTransform(row);
                            if (!row.id || row.id === 0) {
                                self._save(row, function (data) {
                                    row.id = data.id;
                                    originalData.unshift(data);
                                    var originalRow = resetRow(row, rowForm);
                                    angular.extend(originalRow, row);
                                    toaster.pop("success", "", "Saved successful");
                                    self.totalCount++;
                                    existNewFiled = false;
                                }, function (response) {
                                    toaster.pop("error", "", response.data.message);
                                });
                            } else {
                                self._update({ id: row.id }, row, function () {
                                    var originalRow = resetRow(row, rowForm);
                                    angular.extend(originalRow, row);
                                    toaster.pop("success", "", "Update  successful");
                                }, function (response) {
                                    toaster.pop("error", "", response.data.message);
                                });
                            }
                        }

                        function reload() {
                            self.tableParams.reload();
                        }

                        // 
                        $scope.$on("onFinanceConfigurationTabChanged", function (event, args) {
                            self.tableParams.reload();
                        });
                        $scope.$on("onConfigDataTabChange", function (event, args) {
                            self.tableParams.reload();
                        });


                        this.dateFormat = function (date) {
                            if (!date)
                                return "";
                            return moment(date).format("MM/DD/YYYY");
                        }


                        function addNew() {
                            if (existNewFiled)
                                return;
                            existNewFiled = true;
                            self.tableParams.data.unshift({ id: 0, isEditing: true });
                        }
                        function angucompleteSelect(select) {
                            //VERY VERY VERY bad code
                            var fieldName = this.inputName;
                            var item = this.$parent.$parent.$parent.$parent.row;
                            if (select && select.originalObject && select.originalObject.id > 0) {
                                item[fieldName] = select.originalObject.id;
                            } else {
                                item[fieldName] = null;
                            }
                        }

                        function getUiList(col) {
                            if (angular.isFunction(col.list)) {
                                return col.list();
                            }
                            return col.list;
                        }
                        function getTemplate(col) {
                            var templateURL = undefined;
                            if (angular.isFunction(col.templateUrl)) {
                                templateURL = col.templateUrl();
                            } else if (typeof col.templateUrl == 'string') {
                                templateURL = col.templateUrl;
                            }
                            return templateURL;
                            console.info(this);
                            //if (templateURL)
                            //    $templateRequest(templateURL, true).then(function (fileContent) {

                            //        $compile(el.contents())(scope);
                            //    });
                        }

                        function checkColIsVisible(col, row) {
                            if (!angular.isDefined(col.isVisible)) {
                                return true;
                            }
                            if (angular.isFunction(col.isVisible)) {
                                return col.isVisible(row);
                            }
                            return col.isVisible;

                        }

                        function disabledAddButton() {
                            if (!self.option.disabledAddButton) {
                                return false;
                            }
                            if (angular.isFunction(self.option.disabledAddButton)) {
                                return self.option.disabledAddButton();
                            }
                            return self.option.disabledAddButton;
                        }
                        function disabledDeleteButton() {
                            var isDeleteButtonDisabled = false;
                            if (!self.option.disabledDeleteButton) {
                                isDeleteButtonDisabled = true;
                            } else if (angular.isFunction(self.option.disabledDeleteButton)) {
                                isDeleteButtonDisabled = self.option.disabledDeleteButton();
                            } else
                                isDeleteButtonDisabled = self.option.disabledDeleteButton;
                            return isDeleteButtonDisabled;
                        }

                        function disabledViewButton() {
                            if (!self.option.disabledViewButton) {
                                return true;
                            }
                            if (angular.isFunction(self.option.disabledViewButton)) {
                                return self.option.disabledViewButton();
                            }
                            return self.option.disabledViewButton;
                        }

                        function disableUpdateButton() {
                            var isDisabled = false;
                            if (!self.option.disabledEditButton) {
                                isDisabled = true;
                            } else if (angular.isFunction(self.option.disabledEditButton)) {
                                isDisabled = self.option.disabledEditButton();
                            } else
                                isDisabled = self.option.disabledEditButton;
                            //console.info(isDisabled);
                            return isDisabled;
                        }

                        function rowEdit(row) {
                            row.isEditing = true;
                        }

                        function setCommand() {
                            self._onCommand = self._onCommand || {};
                            self._onCommand.reload = reload;
                        }

                        function buildGetData(params) {
                            var data = {
                                pageIndex: params.page(),
                                pageSize: params.count()
                            }
                            if (self.option.searchField) {
                                data.searchField = self.option.searchField;
                            }
                            if (self.option.dropdowntype) {
                                data.dropdowntype = self.option.dropdowntype;
                            }

                            var sorting = params.sorting();
                            if ((sorting !== self.prevSortField) || data.sortOrder !== self.prevSortOrder) {
                                self.prevSortField = sorting;
                                data.pageIndex = 1;
                                //self.tableParams.page(1);
                            }

                            var sortingKeys = Object.keys(sorting);
                            if (sortingKeys.length > 0) {
                                var first = sortingKeys[0];
                                data.sortField = first;
                                data.sortOrder = sorting[data.sortField] === "asc" ? "Ascending" : "Descending";
                            }
                            var filter = self.filters;
                            var filterKeys = Object.keys(filter);
                            _.forEach(filterKeys, function (key) {
                                data[key] = filter[key];
                            });
                            data.searchPhrase = self.search.oldValue;

                            self._changeHeaderParams(data);

                            return data;
                        }

                        var options = {};
                        if (self._isServerData) {
                            options.getData = function ($defer, params) {

                                var data = buildGetData(params);

                                // ajax request to api
                                //govno code need change
                                self._onResponceTransform(data);
                                self.loadTable = self._get(data);

                                self.loadTable.$promise.then(function (response) {
                                    self._onDataGet(response);
                                    originalData = angular.copy(response.data);
                                    self.totalCount = response.itemsCount;
                                    params.total(response.itemsCount);
                                    $defer.resolve(response.data);
                                });
                            }
                        } else {
                            options.data = self._data;
                        }

                        $scope.$on('edz:clear-grid-list', function () {
                            console.info("Grid List Cleared");
                            options.data = [];
                            self.tableParams.data = [];
                            self.tableParams.total(self.tableParams.data.length);
                            self.tableParams.reload();
                        });

                        self.option = this._getOption || {};
                        if (self.option.reset)
                            self.option.reset();
                        self.pageSize = self.option.pageSize || 20;
                        self.add = self.option.onAdd_Click || addNew;
                        self.rowEdit = self.option.onEdit_Click || rowEdit;
                        self.rowView = self.option.onEdit_Click || angular.noop;
                        self.cols = self.option.cols();

                        /*  Resource Drill Down Functionality   */
                        self.tableName = self.option.tableName || "";
                        self.fieldName = self.option.fieldName || "";
                        self.fieldValue = self.option.fieldValue || "";

                        options.counts = [];
                        options.paginationMaxBlocks = 5;
                        options.paginationMinBlocks = 2;
                        self.tableParams = new ngTableParams({
                            paginationMaxBlocks: 5,
                            page: 1,
                            count: self.pageSize
                        }, options);

                        self.filters = {};

                        self.search = {
                            olValue: undefined,
                            newValue: undefined
                        }
                        /*
                        $scope.$watch(function () {
                            return self.search.newValue;
                        }, function (value) {
                            if (self.search.oldValue === value)
                                return;
                            if (self._isServerData) {
                                self.search.oldValue = value;
                                self.tableParams.page(1);
                                self.tableParams.reload();
                            } else {
                                if (self.option.searchField) {
                                    var data = {};
                                    data[self.option.searchField] = value;
                                    self.tableParams.filter(data);
                                }
                            }
                        });
                        */
                        self.pageSearchButtonClick = function () {
                            var value = self.search.newValue;
                            if (self.search.oldValue === value)
                                return;
                            if (self._isServerData) {
                                self.search.oldValue = value;
                                self.tableParams.page(1);
                                self.tableParams.reload();
                            } else {
                                if (self.option.searchField) {
                                    var data = {};
                                    data[self.option.searchField] = value;
                                    self.tableParams.filter(data);
                                }
                            }
                        }

                        self.cancel = cancel;
                        self.del = self.option.onDeleteClick || del;
                        self.save = save;
                        self.disabledAddButton = disabledAddButton;
                        self.disabledDeleteButton = disabledDeleteButton;
                        self.disabledViewButton = disabledViewButton;
                        self.disableUpdateButton = disableUpdateButton;
                        self.angucompleteSelect = angucompleteSelect;
                        self.bulkEdit = bulkEdit;
                        self.getUIList = getUiList;
                        self.checkColIsVisible = checkColIsVisible;
                        setCommand();


                       

                        //self.onMoreBtnClick = function (event) {
                        //    var x = event.x;
                        //    var y = event.y;
                        //    var offsetX = event.clientX;
                        //    var offsetY = event.clienty;
               
                            

                        //    //$(this).find('.uib-dropdown-menu ').css('top', $(this).offset().top);
                        //    //$(this).find('.uib-dropdown-menu ').css('left', $(this).offset().left);

                        //}
                    }

                ]
            };
        }
    ]);