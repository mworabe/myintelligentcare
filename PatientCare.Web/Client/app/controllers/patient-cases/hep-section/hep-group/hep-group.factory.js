"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("HepGroupFactory", ["$rootScope", "$uibModal", function ($rootScope, $modal) {

        function showGroupDialog(config) {
            return $modal.open({
                templateUrl: "Client/app/controllers/patient-cases/hep-section/hep-group/group-dialog.html",
                controller: "GroupCntrl",
                size: "large-modal",
                keyboard: false,
                resolve: {
                    dataItem: function () {
                        return config;
                    }
                }
            });
        }

        return {
            showGroupDialog: function (config) {
                return showGroupDialog(config)
            }
        }


    }
    ]);