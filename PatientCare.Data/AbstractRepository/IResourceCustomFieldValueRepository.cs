﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceCustomFieldValueRepository : IRepository<ResourceCustomFieldValue>
    {
    }
}