﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class CustomFieldPropertyMapppingExtensions
    {

        public static DbModelBuilder MapCustomFieldProperty(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<CustomFieldProperty>().ToTable("custom_field_properties").HasKey(i => i.Id);

            modelBuilder.Entity<CustomFieldProperty>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<CustomFieldProperty>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<CustomFieldProperty>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255);

            modelBuilder.Entity<CustomFieldProperty>()
                .Property(p => p.Value)
                .HasColumnName("value")
                .IsOptional()
                .IsUnicode()
                .HasMaxLength(1028);

            modelBuilder.Entity<CustomFieldProperty>()
                .HasRequired<CustomField>(p => p.CustomField)
                .WithMany()
                .HasForeignKey(p => p.CustomFieldId);

            return modelBuilder;
        }
        
    }
}
