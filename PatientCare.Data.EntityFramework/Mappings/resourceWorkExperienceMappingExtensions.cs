﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceWorkExperienceMappingExtension
    {
        public static DbModelBuilder MapResourceWorkExperience(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceWorkExperience>().ToTable("resource_work_experiences").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceWorkExperience>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceWorkExperience>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceWorkExperience>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<ResourceWorkExperience>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceWorkExperience>()
                .HasMany<ResourceWorkAssignment>(p => p.ResourceWorkAssignments)
                .WithOptional()
                .HasForeignKey(t => t.ResourceWorkExperienceId);
            
            modelBuilder.Entity<ResourceWorkExperience>()
                .Property(p => p.JobTitleId)
                .HasColumnName("job_title_id")
                .IsOptional();

            modelBuilder.Entity<ResourceWorkExperience>()
                .HasOptional<JobTitle>(p => p.WorkExperienceJobTitle)
                .WithMany()
                .HasForeignKey(p => p.JobTitleId); 
            
            modelBuilder.Entity<ResourceWorkExperience>()
                .Property(p => p.StartDate)
                .HasColumnName("start_date")
                .IsOptional();

            modelBuilder.Entity<ResourceWorkExperience>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

            return modelBuilder;
        }
    }
}
