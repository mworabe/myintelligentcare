"use strict";
(function edzNumericCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzNumericCustomField", ["edzBaseCustomField", edzNumericCustomField]);

    ///////////////////////////////////////////////////////
    function edzNumericCustomField(baseCustomField) {
        function NumericCustomField() { }

        NumericCustomField.prototype = Object.create(baseCustomField);
        NumericCustomField.prototype.optionsTemplate = "<edz-numeric-custom-field-options></edz-numeric-custom-field-options>";
        NumericCustomField.prototype.optionsTemplateEdit = "<edz-numeric-custom-field-edit-options></edz-numeric-custom-field-edit-options>";
        return NumericCustomField;
    }
})();