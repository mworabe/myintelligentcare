﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ClinicLocations
{
    [DataContract]
    public class ClinicLocationAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string LocationPrefix { get; set; }

        [DataMember]
        public long? ClinicId { get; set; }

        [DataMember]
        public ClinicLocationAutoCompleteResponse Clinic { get; set; }

        [DataMember]
        public bool? IsDeleted { get; set; }
    }
}