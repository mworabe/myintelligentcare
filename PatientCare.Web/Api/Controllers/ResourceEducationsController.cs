﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ResourceEducations;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Education operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceEducationsController : EdzBaseApiController<ResourceEducation, ResourceEducationRequest, ResourceEducationResponse>
    {
        /// <summary>
        /// Description : In this class we pass  Resource Education IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource Education Repository</param>
        public ResourceEducationsController(IResourceEducationRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.Resource);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }


        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceEducations/" + id, UriKind.Relative);
        }
    }
}