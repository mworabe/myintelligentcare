﻿using System;

namespace PatientCare.Web.Api.Models.ResourceSkills
{
    public class ResourceSkillRequest
    {
        public long Id { get; set; }

        public long ResourceId { get; set; }

        public long SkillId { get; set; }

        public int YearsOfExpirience { get; set; }

        public double Proficiency { get; set; }

        public DateTime? LastUpdated { get; set; }

        public string Description { get; set; }
    }
}