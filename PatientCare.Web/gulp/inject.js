// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright � 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')();

var wiredep = require('wiredep').stream;
var _ = require('lodash');

gulp.task('inject', ['scripts', 'styles'], function () {
    var injectStyles = gulp.src([
      path.join(conf.paths.tmp, '/serve/app/**/*.css'),
      path.join('!' + conf.paths.tmp, '/serve/app/vendor.css')
    ], { read: false });

    var injectStylesAssets = gulp.src([
      path.join(conf.paths.src, '/assets/**/*.css')
    ], { read: false });

    var injectScripts = gulp.src([
      path.join(conf.paths.src, '/app/**/*.module.js'),
      path.join(conf.paths.src, '/app/**/*.js'),
      path.join('!' + conf.paths.src, '/app/**/*.spec.js'),
      path.join('!' + conf.paths.src, '/app/**/*.mock.js')
    ])
    .pipe($.angularFilesort()).on('error', conf.errorHandler('AngularFilesort'));


    var injectScriptsAssets = gulp.src([
      path.join(conf.paths.src, '/assets/**/*.js')
    ])
    .pipe($.angularFilesort()).on('error', conf.errorHandler('AngularFilesort'));

    var injectOptions = {
        ignorePath: [conf.paths.src, path.join(conf.paths.tmp, '/serve')],
        addRootSlash: false
    };
    var injectOptionsAssets = {
        ignorePath: [conf.paths.src, path.join(conf.paths.tmp, '/serve')],
        addRootSlash: false,
        name: "assets"
    };

    return gulp.src(path.join(conf.paths.src, '/*.html'))
      .pipe($.inject(injectStyles, injectOptions))
      .pipe($.inject(injectScripts, injectOptions))
      .pipe($.inject(injectStylesAssets, injectOptionsAssets))
      .pipe($.inject(injectScriptsAssets, injectOptionsAssets))
      .pipe(wiredep(_.extend({}, conf.wiredep)))
      .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve')));
});
