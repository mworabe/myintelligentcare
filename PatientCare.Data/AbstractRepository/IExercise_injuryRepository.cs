﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IExercise_injuryRepository : IRepository<Exercise_injury>
    {
    }
}
