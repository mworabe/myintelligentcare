﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IMasterTemplateExerciseRepository : IRepository<MasterTemplateExercise>
    {
    }
}
