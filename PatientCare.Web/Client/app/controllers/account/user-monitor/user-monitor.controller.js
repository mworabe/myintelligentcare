﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .service("UserMonitorController", [
         "Auth", "$timeout", "$state", "$rootScope", "Idle", "$uibModal", "Keepalive",
        userMonitorControllerFunc]);

function userMonitorControllerFunc(auth, $timeout, $state, $rootScope, idleService, $uibModal, KeepaliveService) {

    var self = this;
    self.isMoniterStarted = false;
    self.refreshToken = undefined;
    var localStoredObj = undefined;
    var autoRefreshActiveUserToken = undefined;
    var refreshTokenTimeoutSecs = undefined;

    /*******
        All New Functionality for Auto-Logoff 
    */

    var idleStartModalInstance = undefined;
    function closeOpenModal() {
        if (idleStartModalInstance)
            idleStartModalInstance.close();
    }

    /***
        To show warning popup for idle user.
    */
    function showModal() {
        idleStartModalInstance = $uibModal.open({
            templateUrl: 'Client/app/components/modal/templates/timeout-modal-template.html',
            windowClass: 'modal-danger'
        });
    }

    /***
        Canceling all the threads related to user idle watch
    */
    function destoryIdle() {
        idleService.unwatch();
        $timeout.cancel(autoRefreshActiveUserToken);
    }
    /**
        Warn User Event, we are using it just for logs.
    */
    $rootScope.$on('IdleWarn', function (e, countdown) {
        console.info("CountDown", countdown);
    });

    /**
        Show warning popup that you are going to kikout.
    */
    $rootScope.$on("IdleStart", function () {
        closeOpenModal();
        showModal();
        $timeout.cancel(autoRefreshActiveUserToken);
    });

    /**
        Hide warning popup that you are kiked out.
        user has performed any event.
    */
    $rootScope.$on("IdleEnd", function () {
        closeOpenModal();
        reloadSystemAuthToken("IDLE END");
        autoRefreshActiveUserToken = $timeout(function () {
            reloadSystemAuthToken("IDLE END - init Timeout");
        }, refreshTokenTimeoutSecs);
        $(document).context.title = "IRMS";
    });

    /**
        Kikout User
        User is unable to perform any event in warning time period
    */
    $rootScope.$on("IdleTimeout", function () {
        closeOpenModal();
        destoryIdle();
        $(document).context.title = "IRMS";
        auth.logout(false);
    });
    /*
    $rootScope.$on('Keepalive', function () {
        // do something to keep the user's session alive
        console.info("Keeping Alive", new Date());
        reloadSystemAuthToken("Calling From KeepAlive")
    });
    */
    /**
    @timeoutTime: Time in Minutes,
    */
    function initUserIdleTimeOutFunctionality(timeoutTime) {
        localStoredObj = angular.fromJson(localStorage.getItem("TIME_OUT_KEY"));
        if (!localStoredObj) localStoredObj = {}, localStoredObj.expirationTime = 180;
        if (!timeoutTime) timeoutTime = localStoredObj.expirationTime || 180;
        timeoutTime = (timeoutTime * 60) - 30;
        idleService.setIdle(parseInt(timeoutTime));
        idleService.setTimeout(15); // Set Value for what time you want to warn User
        idleService.watch();
        console.info("Setup Idle Timeout to " + idleService.getIdle() + " at ", moment(new Date()).format("LLL"));
        refreshTokenTimeoutSecs = ((timeoutTime + 15) * 1000);
        KeepaliveService.setInterval((timeoutTime));
        console.info("Setting Interval Time", timeoutTime - 45);
        autoRefreshActiveUserToken = $timeout(function () {
            reloadSystemAuthToken("Init User Idle");
        }, refreshTokenTimeoutSecs);
    }

    /**
        Refreshing System Token and resetting it into tool.
    */
    function reloadSystemAuthToken(str) {
        console.info("Coming From : " + str, new Date());
        $timeout.cancel(autoRefreshActiveUserToken);
        auth.refreshUserToken(localStoredObj.refreshToken).then(function (res) {
            self.isMoniterStarted = false;
            var obj = {
                expirationTime: parseInt(res.data.sessionExpirationTimeInMinit),
                refreshTokenExpTime: res.data.RefreshTokenExpirationTimeInMinit,
                refreshToken: res.data.refresh_token,
            };
            localStoredObj.refreshToken = res.data.refresh_token;
            localStorage.setItem("TIME_OUT_KEY", angular.toJson(obj));
            if (res.data.access_token)
                localStorage.setItem("irms_access_token", res.data.access_token);
            autoRefreshActiveUserToken = $timeout(function () {
                reloadSystemAuthToken("reloadSystemAuthToken - Timeout");
            }, refreshTokenTimeoutSecs);
        }).catch(function (res) {
            if (auth.isAuthenticated())
                auth.logout(false);
        });
    }

    /**
        To init usermonitor and start watching.
    */
    self.initUserIdleTimeOutFunctionality = initUserIdleTimeOutFunctionality;
    /**
        to destory active monitor.
    */
    self.destoryIdle = destoryIdle;
}