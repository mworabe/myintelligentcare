﻿using System;
using System.Web.Http.Validation;
using Autofac;
using PatientCare.Web.Services.WebApi.Validation;
using FluentValidation;
using FluentValidation.WebApi;

namespace PatientCare.Web.Services.IoC
{
    internal static class ValidatorRegistrationExtensions
    {
        public static ContainerBuilder RegisterValidators(this ContainerBuilder containerBuilder)
        {
            if (containerBuilder == null) { throw new ArgumentNullException("containerBuilder"); }

            containerBuilder.RegisterAssemblyTypes(typeof(IrmsApplication).Assembly)
                .Where(t => typeof(IValidator).IsAssignableFrom(t))
                .SingleInstance()
                .AsImplementedInterfaces();

            containerBuilder.RegisterType<FluentValidationModelValidatorProvider>().As<ModelValidatorProvider>();

            containerBuilder.RegisterType<AutofacValidatorFactory>().As<IValidatorFactory>().SingleInstance();

            return containerBuilder;
        }
    }
}