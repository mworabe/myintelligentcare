namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInHepTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.hep_exercise_frequency", name: "HepDetail_Id", newName: "hep_detail_id");
            RenameIndex(table: "dbo.hep_exercise_frequency", name: "IX_HepDetail_Id", newName: "IX_hep_detail_id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.hep_exercise_frequency", name: "IX_hep_detail_id", newName: "IX_HepDetail_Id");
            RenameColumn(table: "dbo.hep_exercise_frequency", name: "hep_detail_id", newName: "HepDetail_Id");
        }
    }
}
