﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Resources
{
    public class ResourceListWithSupervisorRequet
    { 
        public long? ResourceId { get; set; }

        public string ResourceName { get; set; }
        
        public long? SupervisorId { get; set; }
        
        public string SupervisorName { get; set; }
    }
}