﻿using System.Collections.Generic;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.RoleSetup
{
    public class RoleSetupResponse
    {
        public RoleSetupResponse()
        {
            AccessRights = new AccessRights();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public bool? IsActive { get; set; }

        public string IsActiveYesNo { get; set; }

        public PicklistResponse DashboardSetup { get; set; }

        public AccessRights AccessRights { get; set; }
        
        public bool? IsDeleted { get; set; }
    }
}