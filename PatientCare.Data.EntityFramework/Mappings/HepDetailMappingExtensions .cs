﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class HepDetailMappingExtensions
    {
        public static DbModelBuilder MapHepDetail(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<HepDetail>().ToTable("hep_detail")
                .HasKey(i => i.Id);

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.ExerciseId)
                .HasColumnName("exercise_id")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .HasOptional<Exercise>(p => p.Exercise)
                .WithMany()
                .HasForeignKey(p => p.ExerciseId);

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.GroupId)
                .HasColumnName("group_id")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .HasOptional<HepGroup>(p => p.Group)
                .WithMany()
                .HasForeignKey(p => p.GroupId);

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.HepId)
                .HasColumnName("hep_id")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .HasOptional<HepMaster>(p => p.Hep)
                .WithMany()
                .HasForeignKey(p => p.HepId);

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.Sets)
                .HasColumnName("sets")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.Reps)
                .HasColumnName("reps")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.Time)
                .HasColumnName("time")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.TimeUnit)
                .HasColumnName("time_unit")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.Weight)
                .HasColumnName("weight")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.WeightUnit)
                .HasColumnName("weight_unit")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.Holds)
                .HasColumnName("holds")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.HoldsUnit)
                .HasColumnName("holds_unit")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.ExerciseOrder)
                .HasColumnName("exercise_order")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .HasMany<HepFrequency>(p => p.Frequencies)
                .WithOptional()
                .HasForeignKey(item => item.HepDetailId);

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.Comments)
                .HasColumnName("comment")
                .IsOptional();

            modelBuilder.Entity<HepDetail>()
                .Property(p => p.IsActiveFrequency)
                .HasColumnName("is_active_frequency")
                .IsOptional();

            return modelBuilder;
        }
    }
}
