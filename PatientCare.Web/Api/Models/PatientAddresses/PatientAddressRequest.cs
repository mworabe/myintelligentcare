﻿namespace PatientCare.Web.Api.Models.PatientAddresses
{
    public class PatientAddressRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public string Address { get; set; }

        public bool? AddressIsPrimary { get; set; }

        public string ZipCode { get; set; }

        public string ZipCodeTemp { get; set; }

        public long? CountryId { get; set; }

        public long? StateId { get; set; }

        public long? CityId { get; set; }

        public int? Order { get; set; }
    }
}