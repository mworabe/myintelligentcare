﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using PatientCare.Web.Api.Models;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Web.Api.Models.PostalCodes;
using System.Collections.Generic;
using System.Text;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Zip Code operations.
    /// </summary>
    [Authorize]
    public sealed class ZipCodeController : ApiBaseController
    {
        /// <summary>
        /// Description : Zip Code IPostalCodeRepository.
        /// </summary>
        private readonly IPostalCodeRepository _postalCodeRepository;
        /// <summary>
        /// Description : Zip Code IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<PostalCode> _readOptions = new ReadOptions<PostalCode>();

        /// <summary>
        /// Description : Initializes a new instance of the Zip Code Class. 
        /// </summary>
        /// <param name="postalCodeRepository">IPostal Code Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public ZipCodeController
            (
                IPostalCodeRepository postalCodeRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _postalCodeRepository = postalCodeRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/postalCode/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Postal Code.      
        /// </summary>
        /// <param name="request">Postal Code AutoComplete Request Model</param>
        /// <returns>Code Response Model</returns>
        [HttpGet]
        [Route("api/zip-code/autocomplete")]
        [SwaggerResponse(200, "zipCode", typeof(List<CodeResponse>))]
        public async Task<IHttpActionResult> AutoCompleteActivityProgram([FromUri]PostalCodeAutoCompleteRequest request = null)
        {
            List<CodeResponse> postalCodeList = new List<CodeResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                StringBuilder sb = new StringBuilder();
                sb.Append("select top 20 cast(id as bigint) as Id,zip_code as Name from postal_codes ");
                sb.Append("\r\n");
                sb.AppendFormat("where 1=1 ");
                sb.Append("\r\n");
                if (request.Search != null && request.Search != "")
                {
                    sb.AppendFormat("and (CAST(CHARINDEX(LOWER('{0}'), LOWER(zip_code)) AS int)) = 1", request.Search);
                    sb.Append("\r\n");
                }
                if (request.CountryId != null)
                {
                    sb.AppendFormat("	and country_id = {0} ", request.CountryId);
                    sb.Append("\r\n");
                }
                if (request.StateId != null)
                {
                    sb.AppendFormat("	and state_id = {0} ", request.StateId);
                    sb.Append("\r\n");
                }
                if (request.CityId != null)
                {
                    sb.AppendFormat("	and city_id = {0} ", request.CityId);
                    sb.Append("\r\n");
                }
                sb.Append(" order by zip_code asc ");
                sb.Append("\r\n");
                postalCodeList = await ctx.Database.SqlQuery<CodeResponse>(sb.ToString()).ToListAsync();
            }

            return Ok(postalCodeList.ToArray());
        }
    }
}
