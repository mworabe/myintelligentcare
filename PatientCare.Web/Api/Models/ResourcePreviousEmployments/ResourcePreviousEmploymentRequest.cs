﻿using System;
using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.ResourcePreviousEmployments
{
    public class ResourcePreviousEmploymentRequest
    {
        public long Id { get; set; }

        public long ResourceId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string TitleOfPosition { get; set; }

        public string DutiesOfPosition { get; set; }

        public string ReasonForLeaving { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? NumberOfSupervised { get; set; }

        public decimal? Salary { get; set; }

        public SalaryPerTypeEnum? SalaryPer { get; set; }

        public int? HoursPerWeek { get; set; }
    }
}