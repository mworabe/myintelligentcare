﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceProfessionalOrganizationRepository : IRepository<ResourceProfessionalOrganization>
    {
    }
}
