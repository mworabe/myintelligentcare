﻿using PatientCare.Core.Entities.Abstract;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Exercise Rating.
    /// </summary>
    public class ExerciseRating : Entity
    {
        public long? ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        public long? HepId { get; set; }

        public long? HepDetailId { get; set; }
        //public virtual HepDetail HepDetail { get; set; }

        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public long? PatientCaseId { get; set; }
        public virtual PatientCase PatientCase { get; set; }

        public int? Rating { get; set; }

        public DateTime? RatingDate { get; set; }
    }
}
