﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PatientCare.Core.Mapping
{
    public static class Mapper
    {
        private static IMapperProvider Current { get; set; }

        public static void SetMapperProvider(IMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");
            Current = mapperProvider;
        }

        public static TD Map<TS, TD>(TS source)
        {
            return Current.GetMapper<TS, TD>().Map(source);
        }

        public static IEnumerable<TD> MapCollection<TS, TD>(IEnumerable<TS> source)
        {
            var mapper = Current.GetMapper<TS, TD>();
            return source.Select(s => mapper.Map(s));
        }
    }
}
