﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Authorization Required Enum.
    /// </summary>
    public enum AuthorizeationRequiredEnum
    {
        Yes = 1,
        No = 2
    }
}
