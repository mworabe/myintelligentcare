﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PatientMobileProfileMappingExtensions
    {
        public static DbModelBuilder MapPatientMobileProfile(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<PatientMobileProfile>().ToTable("patient_mobile_profiles").HasKey(i => i.Id);

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<PatientMobileProfile>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.Language)
                .HasColumnName("language")
                .IsOptional();

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.City)
                .HasColumnName("city")
                .IsOptional();

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.State)
                .HasColumnName("state")
                .IsOptional();

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.Country)
                .HasColumnName("country")
                .IsOptional();

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.FullName)
                .HasColumnName("full_name")
                .IsOptional();

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.Email)
                .HasColumnName("email")
                .IsOptional();

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.Phone)
                .HasColumnName("phone")
                .IsOptional();

            modelBuilder.Entity<PatientMobileProfile>()
                .Property(p => p.Photo)
                .HasColumnName("photo")
                .IsOptional();

            modelBuilder.Entity<PatientMobileProfile>()
                .HasMany<PatientGoal>(p => p.Goals)
                .WithOptional()
                .HasForeignKey(item => item.PatientMobileProfileId);

            return modelBuilder;
        }
    }
}
