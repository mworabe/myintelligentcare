﻿using System;
using EmitMapper;
using PatientCare.Services.Mapping;
using PatientCare.Core.Entities;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.PatientCases;

namespace PatientCare.Web.Services.Mapping
{
    public static class PatientCaseMapperExtensions
    {
        public static EmitMapperMapperProvider PatientCaseRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientCase, PatientCaseResponse>
                    (new ExtendedMapConfig<PatientCase, PatientCaseResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientCase[], PatientCaseResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientCase, PatientCaseListResponse>
                    (new ExtendedMapConfig<PatientCase, PatientCaseListResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientCase[], PatientCaseListResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientCase, PicklistResponse>
                    (new ExtendedMapConfig<PatientCase, PicklistResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientCase, PatientCaseAutoCompleteResponse>
                    (new ExtendedMapConfig<PatientCase, PatientCaseAutoCompleteResponse>()));
            

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientCase, PicklistDefaultResponse>
                    (new ExtendedMapConfig<PatientCase, PicklistDefaultResponse>()));

            mapperProvider.RegisterMapper(() =>
              ObjectMapperManager.DefaultInstance.GetMapper<PatientCaseRequest, PatientCase>(
                  new ExtendedMapConfig<PatientCaseRequest, PatientCase>()));

            return mapperProvider;
        }
    }
}