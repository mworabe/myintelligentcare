﻿using PatientCare.Core.Entities.Abstract;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Leave Type.
    /// </summary>
    public class LeaveType : NamedEntity
    {
        public string IsPaid { get; set; }

        public int TotalLeaves { get; set; }

        public string Color { get; set; }
    }
}
