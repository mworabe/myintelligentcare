﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngPhone", [
        "$filter", "$locale", function ($filter, $locale) {
            return {
                require: "ngModel",
                scope: {
                    ngRequired: "=?ngRequired",
                    phoneValidation: "=?ngPhoneValidation"
                },
                link: function (scope, element, attrs, ngModel) {

                    if (attrs.ngPhone === "false") return;

                    function clearValue(value) {
                        value = String(value).replace(/\D/g, "");
                        return value;
                    }

                    function reformatViewValue() {
                        var formatters = ngModel.$formatters,
                            idx = formatters.length;

                        var viewValue = ngModel.$$rawModelValue;
                        while (idx--) {
                            viewValue = formatters[idx](viewValue);
                        }

                        ngModel.$setViewValue(viewValue);
                        ngModel.$render();
                    }

                    ngModel.$parsers.push(function (viewValue) {
                        var cVal = clearValue(viewValue);

                        return cVal;
                    });

                    element.on("blur", function () {
                        ngModel.$commitViewValue();
                        reformatViewValue();
                    });

                    ngModel.$validators.customError = function (cVal) {
                        if (!scope.ngRequired && (isNaN(cVal) || !cVal)) {
                            return true;
                        }
                        if (typeof scope.phoneValidation !== "undefined" && scope.phoneValidation !== "false") {
                            return (cVal && cVal.length === 10);
                        }
                        return true;
                    };

                    ngModel.$formatters.unshift(function (value) {
                        if (isNaN(value) || !value)
                            return "";

                        if (value.length > 10)
                            value = value.substring(0, 10);
                        if (value.length > 6) {
                            value = ["(", value.slice(0, 3), ") ", value.slice(3, 6), "-", value.slice(6)].join('');
                        }

                        return value;
                    });

                    scope.$on("phoneRedraw", function () {
                        ngModel.$commitViewValue();
                        reformatViewValue();
                    });

                    element.on("focus", function () {
                        ngModel.$setViewValue(ngModel.$$rawModelValue || "");
                        ngModel.$render();
                    });
                }
            };
        }
    ]);
/* Whenever change formating of Phone field, need to change if on ResourceEdit.controller.getSummaryFieldValue()    */
angular.module("EDZoutstaffingPortalApp")
    .filter("formatphonenumber", function () {
        return function (value) {
            if ((value == null || value == ''))
                return;
            if (value.length > 10)
                value = value.substring(0, 10);
            if (value.length > 6)
                value = ["(", value.slice(0, 3), ") ", value.slice(3, 6), "-", value.slice(6)].join('');
            return value;
        }
    });