﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("TermsConditionCntrl", ["$scope", "$state", "Auth", "TermsAndConditionsService", "PermissionWorker",
        function ($scope, $state, auth, termsAndConditionsService, permissionWorker) {
            var self = this;
            self.onUserActionPerform = function (isAggre) {
                var currentUser = auth.getCurrentUser();

                if (!currentUser)
                    auth.logout();
                else {
                    var reqModel = {};
                    reqModel.id = currentUser.nameid;
                    reqModel.isNDA = isAggre;
                    termsAndConditionsService.update({ id: reqModel.id }, reqModel, function (success) {
                        if (!isAggre)
                            auth.logout();
                        else {
                            var homePage = permissionWorker.getHomePageState();
                            auth.setNDA("True");
                            if (homePage.hasParams) {
                                $state.go(homePage.state, homePage.params);
                            } else {
                                $state.go(homePage.state);
                            }
                        }
                    }, function (error) {
                        auth.logout();
                    });
                }
            }
        }]);