﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.ResourceScoreFilters;
using Microsoft.AspNet.Identity;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resources Score Filters operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceScoreFiltersController : EdzBaseApiController<ResourceScoreFilter, ResourceScoreFilterRequest, ResourceScoreFilterResponse>
    {
        private readonly IPicklistRepository<ResourceScoreFilter> _resourceScoreFilter;

        /// <summary>
        /// Description : In this class we pass  Resource Score Filters IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="picklistRepository">Resource Score Filter Entity</param>
        public ResourceScoreFiltersController(IPicklistRepository<ResourceScoreFilter> picklistRepository)
            : base(picklistRepository)
        {
            OnEntityUpdating = OnEntityCreating = SetUserId;
            SearchExpression = GetSearchColumnExpressions(User.Identity.GetUserId() ?? string.Empty);
            GetStatusCodeErrorFunc = GetStatusCodeError;
            _resourceScoreFilter = picklistRepository;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }
        
        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceScoreFilters/" + id, UriKind.Relative);
        }

        private void SetUserId(ResourceScoreFilter filter)
        {
            filter.UserId = User.Identity.GetUserId() ?? string.Empty;
        }

        private static Expression<Func<ResourceScoreFilter, bool>> GetSearchColumnExpressions(string userId)
        {
            return filter => filter.UserId == userId;
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Resource Score.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <param name="filterType">Filter Type Value</param>
        /// <returns>Resource Score Filter Response Model</returns>
        [HttpGet]
        [Route("api/ResourceScoreFilters/autocomplete")]
        public IHttpActionResult Autocomplete([FromUri]string search, string filterType = null)
        {
            ListRequest request = new ListRequest
            {
                PageIndex = 1,
                PageSize = 10,
                SearchField = "name",
                SortField = "name",
                SearchPhrase = search,
            };
            var statusCodeResult = GetStatusCodeErrorFunc(Crud.Read);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (string.IsNullOrEmpty(request.SortField))
            {
                request.SortField = "name";
            }
            if (string.IsNullOrEmpty(request.SearchField) && string.IsNullOrEmpty(filterType))
            {
                request.SearchField = "name"; // default field for searching 
            }

           
            var query = PagingExtensions<ResourceScoreFilter>.CreatePagedQuery(request, SearchExpression);
            if (filterType != null || filterType != "")
            {
                query.AddFilter(x=>x.FilterType == filterType);
            }
            var page = EntityRepository.PagedList(query);
            var dtos = Mapper.Map<ResourceScoreFilter[], ResourceScoreFilterResponse[]>(page.Entities.ToArray());
            var response = new ListResponse<ResourceScoreFilterResponse>(dtos, page.TotalCount);
            return Ok(response.Data);
        }
    }
}