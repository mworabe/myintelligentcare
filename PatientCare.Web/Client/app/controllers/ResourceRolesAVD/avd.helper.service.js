﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service('resourceAVDHelper', ["$injector", resourceAVDHelperFunc]);
function resourceAVDHelperFunc($injector) {
    var helper = {};

    helper.getDefaultWorkingHours = function getDefaultWorkingHours(timeRange) {
        if (timeRange === "Year") return 2080;
        if (timeRange === "Month") return 168;
        return 8;
    }

    helper.checkNumberIsInfinity = function checkNumberIsInfinity(number) {
        return number === Number.POSITIVE_INFINITY || number === Number.NEGATIVE_INFINITY;
    }

    helper.checkMedium = function checkMedium(percentage) {
        if (helper.checkNumberIsInfinity(percentage))
            return false;
        return percentage >= 80 && percentage < 100;

    }

    helper.checkCritical = function checkCritical(percentage) {
        if (helper.checkNumberIsInfinity(percentage))
            return true;
        return percentage >= 100;
    }

    return helper;
}