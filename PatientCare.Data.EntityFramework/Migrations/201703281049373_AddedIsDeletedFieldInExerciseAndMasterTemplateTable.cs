namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsDeletedFieldInExerciseAndMasterTemplateTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.exercises", "is_deleted", c => c.Boolean());
            AddColumn("dbo.master_templates", "is_deleted", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.master_templates", "is_deleted");
            DropColumn("dbo.exercises", "is_deleted");
        }
    }
}
