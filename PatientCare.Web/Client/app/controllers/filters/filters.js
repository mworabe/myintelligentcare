"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("filters", {
                url: "/resource/filters",
                templateUrl: "Client/app/controllers/filters/filtersV2.html",
                controller: "FiltersCtrl as FiltersCtrl",
                data: {
                    security: {
                        authenticated: true
                    }
                },
                params: { filterOptions: {} }
            });
    });