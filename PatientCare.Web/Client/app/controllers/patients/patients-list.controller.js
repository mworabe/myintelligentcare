﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("PatientCtrl", [
        "patientService", "$uibModal", "toaster", "$stateParams", "NgTableParams", "$scope", "Modal", "$state", "CONS_BUILD_FOR", "MediaPreview",
        function (patientService, $uibModal, toaster, $stateParams, ngTableParams, $scope, modal, $state, CONS_BUILD_FOR, MediaPreview) {
            var self = this;
            this.resourceReload = patientService.reloadCommand;
            this.get = patientService.query;
            this.cols = patientService.getTableOption;
            this.cols.hideBulkEdit = true;
            this.delete = patientService.deletePatient;


            $scope.$on('onPainRatingAlert', function (event, item) {
                if (item.DataObject) {
                    $state.go("patients", {}, { reload: true });
                }
            });


            


        }
    ]);