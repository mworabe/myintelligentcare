﻿using System.Threading.Tasks;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Services
{
    public interface IReferralService
    {

        Task<bool> UpdateReferralPhysicianAsync(ReferralPhysician referralPhysician);

        Task<bool> CreateReferralPhysicianAsync(ReferralPhysician referralPhysician);

        Task<ReferralPhysician> ReadReferralPhysicianAsync(long id);

        Task<string> DeleteReferralPhysicianAsync(ReferralPhysician physician);
    }
}
