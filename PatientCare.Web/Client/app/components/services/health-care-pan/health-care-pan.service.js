﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("HepService", ["$resource", function ($resource) {
        var resource = $resource("api/HepMasters/:patientId/:controller/:id", null,
            {
                'update': { method: "PUT", url: "api/HepMasters/:id" },
                'query': { method: "GET", params: {}, isArray: false },
                'getCaseHep': {
                    method: "GET",
                    url: "api/HepMasters/:caseId/case",
                    params: {},
                    isArray: false
                },

                saveHep: {
                    method: "POST",
                    url: "api/HepMasters/group",
                    isArray: false
                },
                sendHEPMail: {
                    method: "GET",
                    url: "api/HepMasters/:caseId/:patientId/Email/export_hep",
                    params: {},
                    isArray: false
                }


            });
        return resource;
    }]);