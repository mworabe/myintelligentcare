﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.HepMasters
{
    [DataContract]
    public class HepDetailMobileResponse
    {
        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public DateTime? updatedDate { get; set; }
    }
}