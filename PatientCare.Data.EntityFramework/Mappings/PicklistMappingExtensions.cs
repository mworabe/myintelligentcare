﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using PatientCare.Core.Entities;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PicklistMappingExtensions
    {
        public static DbModelBuilder MapPicklistInfo<T>(this DbModelBuilder modelBuilder, string tableName, string indexName) where T : NamedEntity
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<T>().ToTable(tableName).HasKey(i => i.Id);

            modelBuilder.Entity<T>().Property(p => p.Name)
                .HasColumnName("name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute(indexName) { IsUnique = true }));

            modelBuilder.Entity<T>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<T>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            return modelBuilder;
        }
    }
}
