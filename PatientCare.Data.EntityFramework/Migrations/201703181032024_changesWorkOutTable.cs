namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changesWorkOutTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.user_workout_analytics", newName: "patient_workout_analytics");
            RenameColumn(table: "dbo.patient_workout_analytics", name: "startdate", newName: "start_date");
            RenameColumn(table: "dbo.patient_workout_analytics", name: "enddate", newName: "end_date");
            RenameColumn(table: "dbo.patient_workout_analytics", name: "pre_exercises_painrating", newName: "pre_exercises_pain_rating");           
        }
        
        public override void Down()
        {          
            RenameColumn(table: "dbo.patient_workout_analytics", name: "pre_exercises_pain_rating", newName: "pre_exercises_painrating");
            RenameColumn(table: "dbo.patient_workout_analytics", name: "end_date", newName: "enddate");
            RenameColumn(table: "dbo.patient_workout_analytics", name: "start_date", newName: "startdate");
            RenameTable(name: "dbo.patient_workout_analytics", newName: "user_workout_analytics");
        }
    }
}
