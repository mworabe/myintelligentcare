﻿using System;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ResourceSkills;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Skills operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceSkillsController : EdzBaseApiController<ResourceSkill, ResourceSkillRequest, ResourceSkillResponse>
    {
        /// <summary>
        /// Description : In this class we pass  Resource Skills IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource Skill Repository</param>
        public ResourceSkillsController(IResourceSkillRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.Skill)
                .WithIncludePredicate(item => item.Resource);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }


        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceSkills/" + id, UriKind.Relative);
        }
    }
}