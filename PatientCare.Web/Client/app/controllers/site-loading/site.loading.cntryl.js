angular.module('EDZoutstaffingPortalApp')
    .controller('siteLoadingCntrl',
    ['$scope', 'userTimeZoneFactory', '$q', 'Auth',
        function ($scope, userTimeZoneFactory, $q, auth) {
            $scope.isSiteLoaded = true

            function loadTimeZone() {
                var defer = $q.defer();
                userTimeZoneFactory.getTimeZone(defer);
                return defer.promise;
            }

            function loadConfigLayout() {
                var defer = $q.defer();
                userTimeZoneFactory.getResourceDefaultConfigs(defer);
                return defer.promise;
            }


            loadTimeZone();
            if (auth.isAuthenticated()) {
                loadConfigLayout();
            }

        }]);