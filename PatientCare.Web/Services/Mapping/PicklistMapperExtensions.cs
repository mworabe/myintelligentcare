﻿using System;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.Picklist;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class PicklistMapperExtensions
    {
        public static EmitMapperMapperProvider PicklistRegisterMappings<T>(
            this EmitMapperMapperProvider mapperProvider) where T : Entity
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() => ObjectMapperManager.DefaultInstance.GetMapper<T, PicklistResponse>
                (new ExtendedMapConfig<T, PicklistResponse>()));

            mapperProvider.RegisterMapper(() => ObjectMapperManager.DefaultInstance.GetMapper<T, PicklistDefaultResponse>
                (new ExtendedMapConfig<T, PicklistDefaultResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<T[], PicklistResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<T[], PicklistDefaultResponse[]>());

            mapperProvider.RegisterMapper(() => ObjectMapperManager.DefaultInstance.GetMapper<PicklistRequest, T>
                (new ExtendedMapConfig<PicklistRequest, T>()));

            return mapperProvider;
        }
    }
}