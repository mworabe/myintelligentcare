namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveConsraintInExerciseRatingTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.exercise_ratings", "hep_detail_id", "dbo.hep_detail");
            DropIndex("dbo.exercise_ratings", new[] { "hep_detail_id" });
            AddColumn("dbo.exercise_ratings", "hep_id", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.exercise_ratings", "hep_id");
            CreateIndex("dbo.exercise_ratings", "hep_detail_id");
            AddForeignKey("dbo.exercise_ratings", "hep_detail_id", "dbo.hep_detail", "Id");
        }
    }
}
