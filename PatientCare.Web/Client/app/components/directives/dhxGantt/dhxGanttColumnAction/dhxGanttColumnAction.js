﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ganttColumnAction", [
        function () {
            return {
                restrict: "E",
                terminal: true,
                replace: true,
                scope: {
                    _item: "=ngItem",
                    _itemId: "@ngItemId",
                    _itemName: "@ngItemName",
                    _buttons: "=ngButtons",
                    _itemData: "@ngItemData",
                    showSelectBulkEdit: "@showSelectBulkEdit",
                    checkboxModel: "=checkboxModel",
                    ngSelectedItems: "=ngSelectedItems"
                },
                templateUrl: "Client/app/components/directives/dhxGantt/dhxGanttColumnAction/dhxGanttColumnAction.html",
                link: function ($scope, $element) {

                    //var self = this;
                    //self.ngSelectedItems = $scope.ngSelectedItems;
                    if ($scope.ngSelectedItems == null || $scope.ngSelectedItems.length == 0)
                        $scope.checkboxModel = undefined;

                    $scope.chkChange = function (id) {
                        if (this.$parent.checkboxModel) {
                            this.$parent.checkboxModel = false;
                            if ($scope.ngSelectedItems.indexOf(id) >= 0)
                                $scope.ngSelectedItems.splice($scope.ngSelectedItems.indexOf(id), 1);
                        }
                        else {
                            this.$parent.checkboxModel = true;
                            $scope.ngSelectedItems.push(id);
                        }
                    }
                }
            };
        }
    ]);