﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PatientCare.Web.Api.Models.Reports
{
    [DataContract]
    public class ResourcesReportView
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Resourcename { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string EmployeeId { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public string Division { get; set; }
        [DataMember]
        public string Department { get; set; }
        [DataMember]
        public DateTime? StartDate { get; set; }
        [DataMember]
        public long? SupervisorId { get; set; }
        [DataMember]
        public string Supervisor { get; set; }
    }


}