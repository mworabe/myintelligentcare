"use strict";

angular.module("EDZoutstaffingPortalApp")
    .constant("PROJECT_VALUES", {
        STATUSES: [
            {
                name: "Not Approved",
                value: "NotApproved"
            },
            {
                name: "Approved",
                value: "Approved"
            },
            {
                name: "In Progress",
                value: "InProgress"
            },
            {
                name: "Completed",
                value: "Completed"
            },
            {
                name: "On Hold",
                value: "OnHold"
            }
        ],
        PRIORITIES: [
             { name: "1 - Critical", value: "Critical" },
            { name: "2 - High", value: "High" },
            { name: "3 - Medium", value: "Medium" },
            { name: "4 - Low", value: "Low" },
             { name: "5 - Mandatory", value: "Mandatory" },
        ],
        HEALTH: [
            "Green",
            "Yellow",
            "Red"
        ],
        BILLING_FREQUENCY: [
            "Weekly",
            "Monthly",
            "Quarterly",
            "Annually"
        ],
        GANTT_TYPE: {
            FS: "0",
            SS: "1",
            FF: "2",
            SF: "3"
        },
        GANTT_DATE_FORMAT: {
            YEAR: "%Y",
            MONTH: "%m/%Y",
            DAY: "%m/%d/%Y"
        },
        DATE_SCALE: [
            "day",
            "week",
            "month",
            "year"
        ]

    })
    .constant("SystemConfigurationKeys", {
        systemConfigs: {
            project: {
                ganttColumnConfig: "PROJECT_GANTT_CONFIG",
                ganttScaleConfig: "PROJECT_GANTT_SCALE_CONFIG"
            },
            portfolio: {
                settings: "PORTFOLIO_SETTINGS",
            },
            task: {
                ganttColumnConfig: "TASK_GANTT_CONFIG",
                ganttScaleConfig: "TASK_GANTT_SCALE_CONFIG"
            }
        }
    })
    .constant("SystemFunctions", {
        setKeyToLocalStorage: function (key, value) {
            localStorage.setItem(key, JSON.stringify(value));
        },
        getKeyFromLocalStorage: function (key) {
            var configs = undefined;
            var storedValue = localStorage.getItem(key);
            if (angular.isString(configs)) {
                configs = JSON.parse(storedValue) || {};
                return configs;
            }
            if (configs) {
                return configs;
            }
            configs = JSON.parse(storedValue);
            return configs;
        },
        deleteKeyFromLocalStorage: function (key) {
            localStorage.removeItem(key);
        },
        calculateEstimatedTimeFromFlatTaskList: function (taskList) {
            var estimatedTime = 0;
            _.forEach(taskList, function (task) {
                _.forEach(task.resources, function (resource) {
                    estimatedTime += resource.estimateTime | 0;
                });
            });
            return estimatedTime;
        },
        calculateTimeSpentFromFlatTaskList: function (taskList) {
            var estimatedTime = 0;
            _.forEach(taskList, function (task) {
                _.forEach(task.timeEntries, function (timeEntry) {
                    estimatedTime += timeEntry.timeSpent;
                });
            });
            return estimatedTime;
        },
        convertHourstoTimeString: function (hours) {

            var day = 8;
            var week = 40;
            var month = 160;
            var year = 160 * 12;
            var _year = parseInt(hours / year);
            hours = hours % year;

            var _month = parseInt(hours / month);
            hours = hours % month;

            var _week = parseInt(hours / week);
            hours = hours % week;

            var _day = parseInt(hours / day);
            hours = hours % day;

            var _hours = hours;

            return "" + ((_year > 0) ? _year + "y " : "") + ((_month > 0) ? _month + "m " : "") + ((_week > 0) ? _week + "w " : "") + ((_day > 0) ? _day + "d " : "") + ((_hours > 0) ? _hours + "h " : "");

        },
        scrollToTopZero: function () {
            $("html, body, .modal, .modal-content, .modal-dialog").animate({ scrollTop: 0 }, 400, 'swing');
        },
        getWeekDay: function (dayNumber) {
            var weekDays = new Array(7);
            weekDays[0] = "Sunday";
            weekDays[1] = "Monday";
            weekDays[2] = "Tuesday";
            weekDays[3] = "Wednesday";
            weekDays[4] = "Thursday";
            weekDays[5] = "Friday";
            weekDays[6] = "Saturday";

            return weekDays[dayNumber];
        },
        isNumberInteger: function (number) {
            return Number(number) % 1 === 0;
        },
        historyBackButtonClick: function (rootScope, state, scope) {
            scope.ignoreRedirect = true;
            var _state = (rootScope.appStateManager.name && rootScope.appStateManager.name.length > 0) ? rootScope.appStateManager.name : "home";
            var fromParams = rootScope.appStateManager.fromParams;
            var objParameters = {};
            for (var key in fromParams) {
                objParameters[key] = fromParams[key];
            }
            if (fromParams)
                state.go(_state, objParameters);
            else
                state.go(_state);
        },

        generatePaginationPageArray: function (response, pageIndex, pageSize) {
            var maxPage, minPage, numPages, pages, maxBlocks, minBlocks, maxPivotPages;
            maxBlocks = maxBlocks && maxBlocks < 6 ? 6 : maxBlocks;
            var totalItems = response.itemsCount;
            var currentPage = pageIndex;
            minBlocks = 1, maxBlocks = 5;

            if (!pageSize)
                pageSize = 20;

            pages = [];
            numPages = Math.ceil(totalItems / pageSize);

            if (numPages > 1) {
                pages.push({
                    type: "prev",
                    number: Math.max(1, currentPage - 1),
                    active: currentPage > 1,
                });
                pages.push({
                    type: 'first',
                    number: 1,
                    active: currentPage > 1,
                    current: currentPage === 1
                });
                maxPivotPages = Math.round((maxBlocks - minBlocks) / 2);
                minPage = Math.max(2, currentPage - maxPivotPages);
                maxPage = Math.min(numPages - 1, currentPage + maxPivotPages * 2 - (currentPage - minPage));
                minPage = Math.max(2, minPage - (maxPivotPages * 2 - (maxPage - minPage)));

                var i = minPage;
                while (i <= maxPage) {
                    if ((i === minPage && i !== 2) || (i === maxPage && i !== numPages - 1)) {
                        pages.push({
                            type: 'more',
                            active: false
                        });
                    } else {
                        pages.push({
                            type: 'page',
                            number: i,
                            active: currentPage !== i,
                            current: currentPage === i
                        });
                    }
                    i++;
                }
                pages.push({
                    type: 'last',
                    number: numPages,
                    active: currentPage !== numPages,
                    current: currentPage === numPages
                });
                pages.push({
                    type: 'next',
                    number: Math.min(numPages, currentPage + 1),
                    active: currentPage < numPages
                });

            }
            return pages;
        },
        getSystemFormatedDate: function formatDate(value, format) {
            if (value && format)
                return moment(value).format(format);
            if (value)
                return moment(value).format("MM/DD/YYYY");
            return "";
        },

    });

angular.module('EDZoutstaffingPortalApp')
  .filter('stringCamelCase', function () {
      return function (input) {
          input = input || '';
          return input.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
      };
  })