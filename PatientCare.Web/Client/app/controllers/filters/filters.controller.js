"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("FiltersCtrl",
    ["NgTableParams", "$q", "$rootScope", "$scope", "$stateParams", "$uibModal", "Skill", "Certificate", "Language", "Resource", "Country",
        "uuid2", "toaster", "ResourceScore", "ResourceScoreFilter", "ResourceCompany",
        "Division", "Department", "Modal", "$state", "CreatorSearchFunction", "JobTitle", "$window", "ConfigLayout",
        "AdvancedFilterService", "DropdownConfigsFunction", "PermissionWorker", "$timeout", "$filter", "$translate", "SystemFunctions",
    function (ngTableParams, $q, $rootScope, $scope, $stateParams, $uibModal, skill, certificate, language, resource, country,
        uuid2, toaster, resourceScore, resourceScoreFilter, company,
        division, department, modal, $state, creatorSearchFunction, jobTitleService, $window, configLayoutFactory,
        advancedFilterService, dropdownConfigsFunction, permissionWorker, $timeout, $filter, $translate, systemFunctionsService) {


        var self = this;
        var usedFieldsInQuery = [];

        self.tableCommand = {};
        self.filterRun = false;
        self.data = [];
        self.items = [];
        self.toolTipMessage = "Specify minimal and maximal required experiences or proficiency";
        self.defaultTargetValue = 10;
        self.currentActiveFilter = "normalFilter";
        self.prevWhereStatement = "";
        self.filterOptions = {};

        self.queryParams = {};
        self.queryParams.pageIndex = 1;
        self.queryParams.pageSize = 20;
        self.pagination = {};

        var ngTableOptions = {};
        self.tableParams = new ngTableParams({
            paginationMaxBlocks: 5,
            page: 1,
            count: self.queryParams.pageSize
        }, ngTableOptions);


        self.fieldList = advancedFilterService.getAdvancedFilterFileds({ fields: "resources" });;
        self.filterQueryModel = "";

        self.search = {
            olValue: undefined,
            newValue: undefined
        }

        self.resourceFilterType = [
            {
                id: "normalFilter",
                label: $filter('translate')("Simple Filter"),
                type: "",
                visible: true
            }, {
                id: "advanceFilter",
                label: $filter('translate')("Advanced Filter"),
                type: "",
                visible: true
            }, {
                id: "sqlQuery",
                label: $filter('translate')("SQL"),
                type: "",
                visible: permissionWorker.checkPermissionToSQLQueryFilter()
            }
        ];

        var excludedFieldsFromQuery = [
            {
                id: "avatar",
                name: "Avatar",
                container: "personalInfo",
                position: "1",
            },
        ];

        function exculedFieldsFromSQLQueryList() {
            self.fieldList.splice(self.fieldList.indexOf({ id: "avatar" }), 1);
        }
        exculedFieldsFromSQLQueryList();

        self.clearFilterModel = function () {
            self.filterQueryModel = "";
        }

        // Build Query
        self.addFeildToQuery = function (field) {
            self.filterQueryModel = (self.filterQueryModel.length > 0) ? self.filterQueryModel + " AND " + field.fieldName + " = \'\'" : self.filterQueryModel + field.fieldName + " = \'\'";
            usedFieldsInQuery.push(field);
        }

        self.showSqlFilterHelp = function () {
            var showSQLFilterHelpModal = modal.confirm.templateDialog(function (data) {
            }, "Client/app/controllers/filters/templates/sqlQueryHelpTemplate.html");
            showSQLFilterHelpModal({ name: self.filterName });
        }

        function preChangeSQLQuery() {
            return self.filterQueryModel.replace(/'/g, "''");
        }

        self.onSQLFilterButtonClick = function () {
            self.advancedFilterServerCall(preChangeSQLQuery(), 1);
        }
        self.showFieldsContainer = false;
        self.showFieldsContainerToggle = function () {
            self.showFieldsContainer = !self.showFieldsContainer;
        }

        self.setActiveFilterMode = function (activeFilterId) {
            self.currentActiveFilter = activeFilterId;
            clearFilters();
            self.data = [];
            self.pagination = {};
            self.queryParams.pageIndex = 1;
        }
        self.isFilterActive = function (filterId) {
            return (filterId === self.currentActiveFilter);
        }

        function reject() {
            self.callSubmit = false;
            self.isLoadingData = false;
        }

        self.needToShowImportance = needToShowImportance;
        self.needToShowMandatory = needToShowMandatory;


        self.transformRequestFunctions = {
            singularTransform: function (response, model) {
                var fieldName = model.fieldType.fieldName;
                if (!response[fieldName])
                    response[fieldName] = {};
                response[fieldName].item = model.item;
                response[fieldName].importance = model.importance;
                response[fieldName].mandatory = model.mandatory;
                response[fieldName].startRange = model.startRange;
                response[fieldName].endRange = model.endRange;
            },

            skillRequestTransform: function (response, model) {
                if (!response[model.fieldType.fieldName]) {
                    response[model.fieldType.fieldName] = [];
                }
                var data = {};
                data.item = {
                    skillId: model.skillId,
                    yearsOfExpirience: (model.endRange && model.endRange.yearsOfExpirience) || self.defaultTargetValue
                };
                data.importance = model.importance;
                data.mandatory = model.mandatory;
                if (model.startRange) {
                    data.startRange = {
                        skillId: model.skillId,
                        yearsOfExpirience: model.startRange.yearsOfExpirience
                    };
                }

                if (model.endRange) {
                    data.endRange = {
                        skillId: model.skillId,
                        yearsOfExpirience: model.endRange.yearsOfExpirience
                    };
                }
                response[model.fieldType.fieldName].push(data);
            },

            jobTitleRequestTransform: function (response, model) {

                var fieldName = model.fieldType.fieldName;
                if (!response[fieldName])
                    response[fieldName] = {};
                response[fieldName].item = model.jobTitleName;
                response[fieldName].importance = model.importance;
                response[fieldName].mandatory = model.mandatory;
                response[fieldName].startRange = model.startRange;
                response[fieldName].endRange = model.endRange;
            },

            spokenLanguageRequestTransform: function (response, model) {
                if (!response[model.fieldType.fieldName]) {
                    response[model.fieldType.fieldName] = [];
                }
                var data = {};
                data.item = {
                    languageId: model.languageId,
                    speakingProficiency: (model.endRange && model.endRange.proficiency) || self.defaultTargetValue
                };
                data.importance = model.importance;
                data.mandatory = model.mandatory;
                if (model.startRange) {
                    data.startRange = {
                        languageId: model.languageId,
                        speakingProficiency: model.startRange.proficiency
                    };
                }
                if (model.endRange) {
                    data.endRange = {
                        languageId: model.languageId,
                        speakingProficiency: model.endRange.proficiency
                    };
                }
                response[model.fieldType.fieldName].push(data);
            },

            writtenLanguageRequestTranform: function (response, model) {
                if (!response[model.fieldType.fieldName]) {
                    response[model.fieldType.fieldName] = [];
                }
                var data = {};
                data.item = {
                    languageId: model.languageId,
                    writingProficiency: (model.endRange && model.endRange.proficiency) || self.defaultTargetValue
                };
                data.importance = model.importance;
                data.mandatory = model.mandatory;
                if (model.startRange) {
                    data.startRange = {
                        languageId: model.languageId,
                        writingProficiency: model.startRange.proficiency
                    };
                }
                if (model.endRange) {
                    data.endRange = {
                        languageId: model.languageId,
                        writingProficiency: model.endRange.proficiency
                    };
                }
                response[model.fieldType.fieldName].push(data);
            },

            certificateRequestTransform: function (response, model) {
                if (!response[model.fieldType.fieldName]) {
                    response[model.fieldType.fieldName] = [];
                }
                var data = {};
                data.item = model.certificateId;
                data.importance = model.importance;
                data.mandatory = model.mandatory;
                response[model.fieldType.fieldName].push(data);
            },

            workWellRequestTransform: function (response, model) {
                if (!response[model.fieldType.fieldName]) {
                    response[model.fieldType.fieldName] = {};
                }

                response[model.fieldType.fieldName].item = model.item;
                response[model.fieldType.fieldName].importance = model.importance;
                response[model.fieldType.fieldName].mandatory = model.mandatory;
            },

            availabilityVsDemandTransform: function (response, model) {
                if (!response[model.fieldType.fieldName]) {
                    response[model.fieldType.fieldName] = [];
                }

                var data = {};
                data.item = {
                    dateFrom: model.item.availabilityVsDemandDateStart,
                    dateTo: model.item.availabilityVsDemandDateEnd,
                    minimalAvailability: model.item.minimalAvailabilityVsDemandDate
                };
                data.importance = model.importance;
                data.mandatory = model.mandatory;
                response[model.fieldType.fieldName].push(data);
            }
        };

        self.filterTypes = [
            {
                name: $filter('translate')("Job Title"),
                onlyOne: true,
                fieldName: "jobTitle",
                transformRequestType: "jobTitleRequestTransform"
            }, {
                name: $filter('translate')("Skill"),
                onlyOne: false,
                fieldName: "skills",
                transformRequestType: "skillRequestTransform"
            }, {
                name: $filter('translate')("Spoken Language"),
                onlyOne: false,
                fieldName: "spokenLanguages",
                transformRequestType: "spokenLanguageRequestTransform"
            }, {
                name: $filter('translate')("Written Language"),
                onlyOne: false,
                fieldName: "writtenLanguages",
                transformRequestType: "writtenLanguageRequestTranform"
            }, {
                name: $filter('translate')("Certificate"),
                onlyOne: false,
                fieldName: "certificates",
                transformRequestType: "certificateRequestTransform"
            }, {
                name: $filter('translate')("Education"),
                onlyOne: true,
                fieldName: "education",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Timezone"),
                onlyOne: true,
                fieldName: "timeZoneInfo",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Country"),
                onlyOne: true,
                fieldName: "country",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Rating"),
                onlyOne: true,
                fieldName: "rating",
                transformRequestType: "singularTransform"
            },
            //{
            //    name: "Works Well In Team",
            //    onlyOne: true,
            //    fieldName: "worksWellInTeam",
            //    transformRequestType: "workWellRequestTransform"
            //},
            //{
            //    name: "Works Well Alone",
            //    onlyOne: true,
            //    fieldName: "worksWellAlone",
            //    transformRequestType: "workWellRequestTransform"
            //},
            {
                name: $filter('translate')("Productivity"),
                onlyOne: true,
                fieldName: "productivity",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Company"),
                onlyOne: true,
                fieldName: "company",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Division"),
                onlyOne: true,
                fieldName: "division",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Department"),
                onlyOne: true,
                fieldName: "department",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Range"),
                onlyOne: true,
                fieldName: "rangeType",
                importance: false,
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Hourly Rate"),
                onlyOne: true,
                fieldName: "hourlyRate",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Grade Level"),
                onlyOne: true,
                fieldName: "gradeLevel",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Resource Type"),
                onlyOne: true,
                fieldName: "resourceType",
                transformRequestType: "singularTransform"
            }, {
                name: $filter('translate')("Resource is Available"),
                onlyOne: true,
                fieldName: "availabilityVsDemand",
                transformRequestType: "availabilityVsDemandTransform"
            }, {
                name: $filter('translate')("Location"),
                onlyOne: true,
                fieldName: "location",
                transformRequestType: "singularTransform"
            }
        ];


        function onTypeSelect(item, model) {
            var keys = Object.keys(model);
            for (var i = 0; i < keys.length; i++) {
                var propertyName = keys[i];
                if (propertyName === "fieldType" || propertyName === "$$hashKey" || propertyName === "unique")
                    continue;
                delete model[keys[i]];
            }
        }

        function filterDisabled(item) {
            if (!item.onlyOne)
                return false;
            var x = _.find(self.items, { 'fieldType': item });
            return typeof x !== "undefined";
        }

        function addCriteria() {
            self.isVisibleType = true;
        }

        function addFilter() {
            if (!self.fieldType)
                return;
            self.isVisibleType = false;
            var newItem = {
                unique: self.generateId(),
                startRange: null,
                endRange: null,
                fieldType: self.fieldType,
                importance: 0,
                mandatory: true
            };
            self.fieldType = null;
            self.items.push(newItem);
            //saveFilterLocally();
        }

        function needToShowMandatory(filter) {
            var disabledFilters = [
                /*"country",
                "rangeType",
                "company",
                "division",
                "department",
                "hourlyRate",
                "gradeLevel"*/
            ];

            return disabledFilters.indexOf(filter.fieldType.fieldName) === -1;
        }

        function needToShowImportance(filter) {
            var disabledFilters = [
                /*"country",
                "rangeType",
                "company",
                "division",
                "department",
                //"hourlyRate",
                "gradeLevel"*/
            ];
            return disabledFilters.indexOf(filter.fieldType.fieldName) === -1;
        }

        function clearFilters() {
            self.isVisibleType = false;
            self.items = [];
            $scope.$broadcast('edz:clear-filter');
            $scope.$broadcast('edz:clear-grid-list');
        }

        function saveFilters() {
            if (self.loadedFilterId > 0) {
                var saveFiltersModal = modal.confirm.templateDialog(function (data) {
                    var filter = {};
                    filter.id = self.loadedFilterId;
                    filter.name = data.name;
                    filter.filterType = "normalFilter";
                    filter.jsonFilter = angular.toJson(self.items);
                    self.isLoadingData = resourceScoreFilter.update({ id: self.loadedFilterId }, filter, function () {
                        toaster.pop("success", "", "Filter updated successfully.");
                    }, function () { });
                }, "Client/app/controllers/filters/saveFiltersModal.html");
                saveFiltersModal({ name: self.filterName });
            } else {
                var saveFiltersModal = modal.confirm.templateDialog(function (data) {
                    var filter = {};
                    filter.name = data.name;
                    filter.filterType = "normalFilter";
                    filter.jsonFilter = angular.toJson(self.items);
                    self.isLoadingData = resourceScoreFilter.save(filter, function () {
                        toaster.pop("success", "", "save succesfull");
                    });
                }, "Client/app/controllers/filters/saveFiltersModal.html");
                saveFiltersModal({});
            }
        }

        /*      Angu-AutoComplete Functions     */
        function checkAndReturnValueInAngucomplete(value) {
            if (value && value.originalObject) {
                return value.originalObject;
            }
            return null;
        }

        function checkAndReturnValueIdInAngucomplete(value) {
            if (value && value.originalObject) {
                return value.originalObject.id;
            }
            return null;
        }
        function checkAndReturnValueNameInAngucomplete(value) {
            if (value && value.originalObject) {
                return value.originalObject.name;
            }
            return null;
        }
        function getAssoociateItemInAngucomplete(self) {
            return self.$parent.$parent.item;
        }
        /*      Angu-AutoComplete Functions   -------------------  */


        this.checkAndReturnValueInAngucomplete = checkAndReturnValueInAngucomplete;

        function applyFilters(data) {
            var filter = checkAndReturnValueInAngucomplete(data.selectedFilter);
            if (filter != null) {
                self.loadedFilterId = (filter.id) ? filter.id : 0;
                self.filterName = filter.name;
                self.items = angular.fromJson(filter.jsonFilter);
            }
            // filter data on saved filter load
            if (self.items && self.items.length > 0)
                onFilterClick(filterForm);
        }

        function loadFilters() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "Client/app/controllers/filters/loadFiltersModal.html",
                controller: "LoadFiltersCtrl as LoadFiltersCtrl",
                resolve: {
                    data: function () {
                        return {
                            searchResourceScoreFilters: self.searchResourceScoreFilters,
                            filterType: self.currentActiveFilter,
                        }
                    }
                }
            });
            modalInstance.result.then(applyFilters);
        }


        function applySQLQueryFilters(data) {
            var filterQueryModel = angular.fromJson(data.selectedFilter.originalObject.jsonFilter);
            var testing = checkAndReturnValueInAngucomplete(data.selectedFilter);
            self.filterQueryModel = filterQueryModel.sqlQuery;
            self.onSQLFilterButtonClick();
        }


        self.loadSQLFilters = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "Client/app/controllers/filters/loadFiltersModal.html",
                controller: "LoadFiltersCtrl as LoadFiltersCtrl",
                resolve: {
                    data: function () {
                        return {
                            searchResourceScoreFilters: self.searchResourceScoreFilters,
                            filterType: "sqlQuery",
                        }
                    }
                }
            });
            modalInstance.result.then(applySQLQueryFilters);
        }

        self.saveSqlQueryFilter = function () {
            var obj = {};
            obj.sqlQuery = self.filterQueryModel;

            var saveSQLQueryFiltersModal = modal.confirm.templateDialog(function (data) {
                var filter = {};
                filter.name = data.name;
                filter.filterType = "sqlQuery";
                filter.jsonFilter = angular.toJson(obj);
                self.isLoadingData = resourceScoreFilter.save(filter, function () {
                    toaster.pop("success", "", "save succesfull");
                });
            }, "Client/app/controllers/filters/saveFiltersModal.html");
            saveSQLQueryFiltersModal({});
        }


        // name need for saving/loading filters

        function isHideImportanceField(fieldType) {
            return fieldType.name !== "Range";
        }

        function angucompleteSelectSkill(value) {
            var currItem = getAssoociateItemInAngucomplete(this);
            var id = checkAndReturnValueIdInAngucomplete(value);
            var name = checkAndReturnValueNameInAngucomplete(value);
            currItem.skillId = id;
            currItem.skillName = name;
        }
        function angucompleteSelectJobTitle(value) {
            var currItem = getAssoociateItemInAngucomplete(this);
            var id = checkAndReturnValueIdInAngucomplete(value);
            var name = checkAndReturnValueNameInAngucomplete(value);
            currItem.jobTitleId = id;
            currItem.jobTitleName = name;
        }

        function deleteItem(item) {
            var index = self.items.indexOf(item);
            if (index > -1)
                self.items.splice(index, 1);
        }

        function uiSelectClick() {
            self.popoverIsOpen1 = false;
            self.popoverIsOpen2 = false;
        }


        function angucompleteSelectLanguage(value) {
            var currItem = getAssoociateItemInAngucomplete(this);
            var id = checkAndReturnValueIdInAngucomplete(value);
            var name = checkAndReturnValueNameInAngucomplete(value);
            currItem.languageId = id;
            currItem.languageName = name;
        }
        function angucompleteSelectCertificate(value) {
            var currItem = getAssoociateItemInAngucomplete(this);
            var id = checkAndReturnValueIdInAngucomplete(value);
            var name = checkAndReturnValueNameInAngucomplete(value);
            currItem.certificateId = id;
            currItem.certificateName = name;
        }

        function angucompleteSelectItem(value) {
            var currItem = getAssoociateItemInAngucomplete(this);
            var id = checkAndReturnValueIdInAngucomplete(value);
            var name = checkAndReturnValueNameInAngucomplete(value);
            currItem.item = id;
            currItem.itemName = name;
        }

        function accordionChange() {
            self.accordionOpen = !self.accordionOpen;
        }

        function cancel() {
            $window.sessionStorage.setItem("localFilterData", JSON.stringify([]));
            isLocalFilterStored();
            $state.go("resource");
        }

        function isLocalFilterStored() {
            if ($window.sessionStorage && $window.sessionStorage.localFilterData && $window.sessionStorage.localFilterData.length > 1 && self.items.length <= 0) {
                self.items = JSON.parse($window.sessionStorage.localFilterData);
                $window.sessionStorage.removeItem("localFilterData");
            }
        }

        function saveFilterLocally() {
            isLocalFilterStored();
            $window.sessionStorage.setItem("localFilterData", JSON.stringify(self.items));
        }


        function transformArrayToRequestModel() {
            var request = {};
            _.forEach(self.items, function (item) {
                if (item && item.fieldType && item.fieldType.transformRequestType)
                    self.transformRequestFunctions[item.fieldType.transformRequestType](request, item);
            });
            return request;
        }


        isLocalFilterStored();

        var request;

        self.isLoadingData = false;

        function generateResourceFilterPaginationPageArray(response, pageIndex, pageSize, totalItemCount) {
            var maxPage, minPage, numPages, pages, maxBlocks, minBlocks, maxPivotPages;
            maxBlocks = maxBlocks && maxBlocks < 6 ? 6 : maxBlocks;
            var totalItems = totalItemCount;
            var currentPage = pageIndex;
            minBlocks = 1, maxBlocks = 5;

            if (!pageSize)
                pageSize = 20;

            pages = [];
            numPages = Math.ceil(totalItems / pageSize);

            if (numPages > 1) {
                pages.push({
                    type: "prev",
                    number: Math.max(1, currentPage - 1),
                    active: currentPage > 1,
                });
                pages.push({
                    type: 'first',
                    number: 1,
                    active: currentPage > 1,
                    current: currentPage === 1
                });
                maxPivotPages = Math.round((maxBlocks - minBlocks) / 2);
                minPage = Math.max(2, currentPage - maxPivotPages);
                maxPage = Math.min(numPages - 1, currentPage + maxPivotPages * 2 - (currentPage - minPage));
                minPage = Math.max(2, minPage - (maxPivotPages * 2 - (maxPage - minPage)));

                var i = minPage;
                while (i <= maxPage) {
                    if ((i === minPage && i !== 2) || (i === maxPage && i !== numPages - 1)) {
                        pages.push({
                            type: 'more',
                            active: false
                        });
                    } else {
                        pages.push({
                            type: 'page',
                            number: i,
                            active: currentPage !== i,
                            current: currentPage === i
                        });
                    }
                    i++;
                }
                pages.push({
                    type: 'last',
                    number: numPages,
                    active: currentPage !== numPages,
                    current: currentPage === numPages
                });
                pages.push({
                    type: 'next',
                    number: Math.min(numPages, currentPage + 1),
                    active: currentPage < numPages
                });

            }
            return pages;
        };


        /*
        * Main Method to Filter Resource Data.
        */
        function onFilterClick(form) { // Noraml Filter
            self.isLoadingData = true;
            if (form.$invalid) {
                return;
            }
            if (self.callSubmit) {
                return;
            }
            self.callSubmit = true;


            request = transformArrayToRequestModel();
            checkDataBeforeRequest(request);
            self.isLoadingData = resourceScore.filter(self.queryParams, request, onSuccessfullyDataGet, reject);
        }

        function onSuccessfullyDataGet(responce) {
            self.callSubmit = false;
            self.filterRun = true;
            self.data.splice(0, self.data.length);
            //_.forEach(responce.data, function (item) {
            //    //item.id = self.generateId();
            //    self.data.push(item);
            //});

            self.data = responce.data;
            ngTableOptions.data = self.data;

            self.totalCount = responce.itemsCount;
            if (self.tableCommand.reload)
                self.tableCommand.reload();
            self.isLoadingData = false;
            // Generate Pagination for Resource Filter.
            console.info(self.data);
            self.pagination.totalPages = generateResourceFilterPaginationPageArray(self.data, self.queryParams.pageIndex, self.queryParams.pageSize, self.totalCount);
            console.info(self.pagination);
            //saveFilterLocally();
        }


        self.onPageChange = function onPageChange(page) {
            systemFunctionsService.scrollToTopZero();
            self.queryParams.pageIndex = parseInt(page.number);
            if (self.currentActiveFilter === "normalFilter")
                onFilterClick(filterForm);
            else if (self.currentActiveFilter === "advanceFilter" && self.prevWhereStatement)
                advancedFilterServerCall(self.prevWhereStatement);
            else
                advancedFilterServerCall(self.prevWhereStatement);
        }

        function advancedFilterServerCall(whereStatement, pageIndex) { // Advanced Filter
            self.callSubmit = true;
            //resourceScore
            var where = { where: whereStatement };
            self.prevWhereStatement = whereStatement;
            where.pageIndex = pageIndex || self.queryParams.pageIndex;
            where.pageSize = self.queryParams.pageSize;
            self.queryParams.pageIndex = where.pageIndex;
            self.isLoadingData = resourceScore.advancedFilter(where, function (responce) {
                var tempData = {};
                tempData.data = responce.data;
                tempData.itemsCount = responce.itemsCount;
                onSuccessfullyDataGet(tempData);
            }, reject);
        }

        function checkDataBeforeRequest(request) {
            for (var key in request) {
                if (key === 'skills') {
                    request[key].map(function (item) {
                        if (!item.startRange.yearsOfExpirience && !item.endRange.yearsOfExpirience) {
                            item.item.yearsOfExpirience = 10;
                            item.endRange.yearsOfExpirience = 10;
                        }
                        if (item.startRange.yearsOfExpirience && !item.endRange.yearsOfExpirience) {
                            item.item.yearsOfExpirience = 100;
                            item.endRange.yearsOfExpirience = 100;
                        }
                    })
                }
                if (key === 'spokenLanguages') {
                    request[key].map(function (item) {
                        if (!item.speakingProficiency && !item.endRange.speakingProficiency) {
                            item.item.speakingProficiency = 10;
                            item.endRange.speakingProficiency = 10;
                        }
                        if (item.speakingProficiency && !item.endRange.speakingProficiency) {
                            item.item.speakingProficiency = 100;
                            item.endRange.speakingProficiency = 100;
                        }
                    })
                }
                if (key === 'writtenLanguages') {
                    request[key].map(function (item) {
                        if (!item.startRange.writingProficiency && !item.endRange.writingProficiency) {
                            item.item.writingProficiency = 10;
                            item.endRange.writingProficiency = 10;
                        }
                        if (item.startRange.writingProficiency && !item.endRange.writingProficiency) {
                            item.item.writingProficiency = 100;
                            item.endRange.writingProficiency = 100;
                        }
                    })
                }
                if (key === 'hourlyRate') {
                    request[key].mandatory = true;
                }
            }
            return request;
        }

        function getTable() {
            var defer = $q.defer();
            var request = transformArrayToRequestModel();
            //realy get
            self.isLoadingData = resourceScore.filter(self.queryParams, request, function (responce) {
                _.forEach(responce, function (item) {
                    item.id = self.generateId();
                });
                defer.resolve({ data: responce, itemsCount: responce.length });
            }, reject);
            return { $promise: defer.promise };
        }

        /**
            Temporary try to make and use pagination of UI_GRID_TABLE directive.
        */
        self.onDataGet = function onDataGet(response) {
            var res = {};
            res.data = response;
            res.itemsCount = (response && response.length > 0) ? response[0].itemsCount : response.length;
            response = res;
            return res;
        }


        self.timeZonesInfo = resource.getTimeZones();
        self.importanceValues = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        self.rangeTypes = ["L", "M", "H"];
        self.resourceTypes = ["Internal", "External", "Consultant"];

        creatorSearchFunction(self, "searchSkills", skill);
        creatorSearchFunction(self, "searchCertificate", certificate);
        creatorSearchFunction(self, "searchLanguage", language);
        creatorSearchFunction(self, "searchCountry", country);
        creatorSearchFunction(self, "searchCompany", company);
        creatorSearchFunction(self, "searchDivision", division);
        creatorSearchFunction(self, "searchDepartment", department);
        creatorSearchFunction(self, "searchResourceScoreFilters", resourceScoreFilter);
        creatorSearchFunction(self, "searchJobTitle", jobTitleService);

        dropdownConfigsFunction(self, "searchResourceScoreFilters", resourceScoreFilter, "normalFilter");

        self.tableOptions = resourceScore.getTableOption;
        self.tableCols = self.tableOptions.cols();
        self.get = getTable;
        self.addCriteria = addCriteria;
        self.addFilter = addFilter;
        self.clearFilters = clearFilters;
        self.saveFilters = saveFilters;
        self.loadFilters = loadFilters;
        self.angucompleteSelectSkill = angucompleteSelectSkill;
        self.angucompleteSelectJobTitle = angucompleteSelectJobTitle;
        self.angucompleteSelectLanguage = angucompleteSelectLanguage;
        self.angucompleteSelectCertificate = angucompleteSelectCertificate;
        self.angucompleteSelectItem = angucompleteSelectItem;
        self.onTypeSelect = onTypeSelect;
        self.delete = deleteItem;
        self.cancel = cancel;
        self.onFilterClick = onFilterClick;
        self.filterDisabled = filterDisabled;
        self.generateId = uuid2.newguid;
        self.accordionChange = accordionChange;
        self.accordionOpen = true;
        self.isHideImportanceField = isHideImportanceField;
        self.uiSelect_Click = uiSelectClick;
        self.advancedFilterServerCall = advancedFilterServerCall;

        // default call to load all data
        if ($stateParams && $stateParams.filterOptions && $stateParams.filterOptions.filterType) {
            self.filterOptions = $stateParams.filterOptions;
            if (self.filterOptions.filterType) {
                self.currentActiveFilter = self.filterOptions.filterType.id;
                if (self.currentActiveFilter === "normalFilter") {
                    var data = {};
                    data.selectedFilter = self.filterOptions.selectedFilter;
                    applyFilters(data);
                } else if (self.currentActiveFilter === "advanceFilter") {
                    $timeout(function () { $rootScope.$broadcast('edz:advanced-filter-loaded', { data: self.filterOptions }); }, 200);
                }
            }

        }
    }
    ]);