"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("edzHideGanttColumn", ["SystemConfigurationKeys", "Project", "ProjectTask",
        function (systemConfigurationKeys, projectService, ProjectTaskService) {
            return {
                restrict: "E",
                template: "<span class='glyphicon' ng-class=\"{'glyphicon-eye-open': showAllColumns, 'glyphicon-eye-close': !showAllColumns}\"></span>{{buttonName}}",
                scope: {
                    ganttType: '=type'
                },
                link: function (scope, element) {
                    scope.showAllColumns = true;
                    scope.buttonName = " Hide details";

                    var prjCols = "";
                    if (scope.ganttType == "task") {
                        prjCols = localStorage.getItem(systemConfigurationKeys.systemConfigs.task.ganttColumnConfig);
                    } else {
                        prjCols = localStorage.getItem(systemConfigurationKeys.systemConfigs.project.ganttColumnConfig);
                    }

                    scope.projectGanttCols = angular.fromJson(angular.fromJson(prjCols).value);

                    if (!(scope.projectGanttCols && scope.projectGanttCols.length > 0)) {
                        if (scope.ganttType == "task") {
                            scope.projectGanttCols = ProjectTaskService.getProjectTasksGanttFields();
                        } else {
                            scope.projectGanttCols = projectService.getProjectGanttFieldList();
                        }
                    }

                    element.bind('click', function () {
                        var configuredGanttCols = scope.projectGanttCols;
                        scope.showAllColumns = !scope.showAllColumns;
                        gantt.config.columns.map(function showOrHideColumn(column) {
                            var savedCol = _.find(configuredGanttCols, { name: column.name });
                            if (scope.showAllColumns) {
                                scope.buttonName = " Hide details";
                                if (savedCol)
                                    column.hide = (savedCol.name === "action" || savedCol.name === "name") ? false : savedCol.hide;
                            } else if (!column.showOnHiddenDetails) {
                                scope.buttonName = " Show details";
                                column.hide = true;
                            }
                        });
                        gantt.render();
                    });
                }
            };
        }
    ]);