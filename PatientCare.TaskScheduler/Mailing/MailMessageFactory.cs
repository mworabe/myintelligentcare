﻿using PatientCare.Core.Services.Mailing;
using System;
using System.Net.Mail;
using System.Configuration;

namespace PatientCare.TaskScheduler.Mailing
{
    public class MailMessageFactory : IMailMessageFactory
    {
        private string EmailTemplateFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["mailTamplatesFolder"];
            }
        }
        
        private const string FORGOT_PASSWORD_MODEL_EMAIL = @"\ForgotPassword_Template.html";
        
        public MailMessage ForgotPasswordMailMessage(string emailId, string userName, string link)
        {
            throw new NotImplementedException();
        }

        private const string MOBILE_ACCESS_MODEL_EMAIL = @"\MobileAccess_Template.html";

        public MailMessage MobileAccessMailMessage(string emailId, string patientName, string userName, string password)
        {
            throw new NotImplementedException();
        }
    }
}