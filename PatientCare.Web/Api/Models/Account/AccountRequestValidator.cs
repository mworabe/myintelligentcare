﻿using System.Linq;
using System.Web;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.Localization;
using FluentValidation;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PatientCare.Web.Api.Models.Account
{
    public class AccountRequestValidator : AbstractValidator<AccountRequest>
    {
        public AccountRequestValidator(IDataContextScopeFactory dbContextScopeFactory)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(dbContextScopeFactory.CreateReadOnly().DbContexts.Get<PatientCareDbContext>()));
            

            RuleFor(model => model.UserName)
                .NotEmpty()
                .Length(1, 255)
                .Must((request, s) => !manager.Users.Any(u => u.UserName == s && u.Id != request.Id))
                .WithMessage(ValidationMessages.UserWithSpecifiedUsernameExists);

            RuleFor(model => model.ResourceId)
                .Must((r, l) => (r.Role == ApplicationUser.FrontOfficeRole && l.HasValue) || r.Role != ApplicationUser.FrontOfficeRole)
                .WithMessage(ValidationMessages.PleaseSpecifyResourceForUserWithResourceRole)
                .Must((request, l) => !manager.Users.Any(x => x.ResourceId == l && l.HasValue && x.Id != request.Id))
                .WithMessage(ValidationMessages.ResourceCantBeChosenTwice);
        }
    }
}