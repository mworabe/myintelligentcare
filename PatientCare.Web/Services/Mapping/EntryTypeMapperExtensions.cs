﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.EntryTypes;
using EmitMapper;
using Newtonsoft.Json;
using PatientCare.Web.Services.Convertation;

namespace PatientCare.Web.Services.Mapping
{
    public static class EntryTypeMapperExtensions
    {
        public static EmitMapperMapperProvider EntryTypeRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<EntryType, EntryTypeResponse>
                    (new ExtendedMapConfig<EntryType, EntryTypeResponse>()
                    .ForMember(member => member.Days, source => string.IsNullOrEmpty(source.Days) ? null : JsonConvert.DeserializeObject<DayItem>(source.Days))
                    ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<EntryType, EntryTypeAutoCompleteResponse>
                    (new ExtendedMapConfig<EntryType, EntryTypeAutoCompleteResponse>()
                    .ForMember(member => member.Days, source => string.IsNullOrEmpty(source.Days) ? null : JsonConvert.DeserializeObject<DayItem>(source.Days))
                    ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<EntryType, EntryTypePicklistResponse>
                    (new ExtendedMapConfig<EntryType, EntryTypePicklistResponse>()
                    .ForMember(member => member.Days, source => string.IsNullOrEmpty(source.Days) ? null : JsonConvert.DeserializeObject<DayItem>(source.Days))
                    ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<EntryType[], EntryTypeResponse[]>());

            mapperProvider.RegisterMapper(() =>
              ObjectMapperManager.DefaultInstance.GetMapper<EntryType[], EntryTypePicklistResponse[]>());

            mapperProvider.RegisterMapper(() =>
             ObjectMapperManager.DefaultInstance.GetMapper<EntryType[], EntryTypeAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<EntryTypeRequest, EntryType>
                    (new ExtendedMapConfig<EntryTypeRequest, EntryType>()
                    .ForMember(x => x.Days, source => source.Days == null ? null : JsonConverterHelper.SerializeObject(source.Days))
                    ));

            return mapperProvider;
        }
    }
}