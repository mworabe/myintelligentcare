﻿using System;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Departments operations.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Departments")]
    public sealed class DepartmentsController : EdzPicklistApiController<Department>
    {
        /// <summary>
        /// Description : In this class we pass Department IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="picklistRepository">Certificate Entity</param>
        public DepartmentsController(IPicklistRepository<Department> picklistRepository) : base(picklistRepository)
        {
            GetPicklistStatusCodeErrorFunc = () => GetPicklistStatusCodeError(Picklist.Departments);
        }

        /// <summary>
        /// Description : Use for Get Picklist Location
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        protected override Uri GetPicklistLocation(long id)
        {
            return new Uri("/Departments/" + id, UriKind.Relative);
        }
    }
}