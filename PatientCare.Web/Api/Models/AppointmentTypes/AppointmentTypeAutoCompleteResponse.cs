﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.AppointmentTypes
{
    [DataContract]
    public class AppointmentTypeAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

    }
}