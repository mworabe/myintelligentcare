﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceCriminalRecordMappingExtensions
    {
        public static DbModelBuilder MapResourceCriminalRecord(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceCriminalRecord>().ToTable("resource_criminal_records").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceCriminalRecord>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceCriminalRecord>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceCriminalRecord>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsRequired();

            modelBuilder.Entity<ResourceCriminalRecord>()
                .HasRequired<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceCriminalRecord>()
                .Property(p => p.DateOfOffense)
                .HasColumnName("date_of_offense")
                .IsRequired();

            modelBuilder.Entity<ResourceCriminalRecord>()
                .Property(p => p.LocationOfOffense)
                .HasColumnName("location_of_offense")
                .IsUnicode()
                .HasMaxLength(255)
                .IsRequired();

            modelBuilder.Entity<ResourceCriminalRecord>()
                .Property(p => p.Offense)
                .HasColumnName("offense")
                .IsUnicode()
                .HasMaxLength(1023)
                .IsRequired();

            modelBuilder.Entity<ResourceCriminalRecord>()
                .Property(p => p.PenaltyOrDisposition)
                .HasColumnName("penalty_or_disposition")
                .IsOptional();

            return modelBuilder;
        }
    }
}
