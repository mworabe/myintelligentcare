﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{   
    public sealed class PicklistRepository<T> : RepositoryBase<T>, IPicklistRepository<T> 
        where T : Entity
    {
        public PicklistRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
