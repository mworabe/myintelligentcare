﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Calenders
{
    [DataContract]
    public class CalenderResponse
    {
        [DataMember]
        public string CalenderType { get; set; }

        [DataMember]
        public string EventType { get; set; }

        [DataMember]
        public long? Id { get; set; }

        [DataMember]
        public string ResourceName { get; set; }

        [DataMember]
        public string SupervisorName { get; set; }

        [DataMember]
        public string LeaveType { get; set; }

        [DataMember]
        public long? LeaveTypeId { get; set; }

        [DataMember]
        public string CommentFromSupervisor { get; set; }

        [DataMember]
        public string IsPaidType { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public long? SupervisorId { get; set; }

        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public DateTime? Start { get; set; }

        [DataMember]
        public DateTime? End { get; set; }

        [DataMember]
        public int TakenLeaves { get; set; }

        [DataMember]
        public int RemainingLeave { get; set; }

        [DataMember]
        public string ClassName { get; set; }

        [DataMember]
        public string StatusClassName { get; set; }

        [DataMember]
        public bool? AllDay { get; set; }
    }
}