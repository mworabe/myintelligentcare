﻿namespace PatientCare.Web.Api.Models.Exercises
{
    public class ExerciseAutoCompleteRequest
    {
        public string Search { get; set; }
        
        public long? RegionId { get; set; }

        public string Name { get; set; }

        public long? PositionId { get; set; }

        public long? ActivityId { get; set; }

        public long? ExerciseRegionId { get; set; }

        public string Keywords { get; set; }

        public long? set { get; set; }
        public long? reps { get; set; }
        public long? holds { get; set; }
        public long? weight { get; set; }
        public string weightUnit { get; set; }
        public string holdUnit { get; set; }
        public string frequency { get; set; }

    }
}