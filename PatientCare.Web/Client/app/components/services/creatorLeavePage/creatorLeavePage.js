﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//

"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("CreatorLeavePage", [
        function() {

            return function ($scope, getForm) {
                $scope.ignoreRedirect = false; 
                $scope.$on("$destroy", function() {
                    window.onbeforeunload = undefined;
                });
                $scope.$on("$stateChangeStart", function (event) {
                    var form = getForm();
                    if (!form.$dirty || $scope.ignoreRedirect) {
                        return;
                    }
                    if (!confirm("You have unsaved changes. Are you sure you want to leave this page?")) {
                        event.preventDefault();
                        $scope.$emit('$cancelLeavePage');
                    }

                });

                $scope.$on("$viewContentLoaded", function () {
                    window.onbeforeunload = function (event) {
                    var form = getForm();
                       if (!form.$dirty) {
                            return undefined;
                        }

                        var message = "You have unsaved changes. Are you sure you want to leave this page?";
                        if (typeof event == "undefined") {
                            event = window.event;
                        }
                        if (event) {
                            event.returnValue = message;
                        }
                        return message;
                    };
                });
            };
        }
    ]);