﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Core.Enums;
using System;

namespace PatientCare.Web.Api.Models.EntryTypes
{
    [DataContract]
    public class EntryTypePicklistResponse
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public EntryPeriodEnum? EntryPeriod { get; set; }
        [DataMember]
        public DayItem Days { get; set; }
        [DataMember]
        public long? AdvancedCreatePeriod { get; set; }
    }
}