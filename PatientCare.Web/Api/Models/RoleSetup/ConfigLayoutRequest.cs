﻿using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.RoleSetup
{
    public class ConfigLayoutRequest
    {
        public LayoutTypeEnum Type { get; set; }
        public ConfigLayout ConfigLayout { get; set; }
    }
}