﻿using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PatientCare.Data.EntityFramework.Identity
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public const string AdminRole = "Admin";
        public const string TherapistRole = "Therapist";
        public const string PatientRole = "Patient";
        public const string FrontOfficeRole = "FrontOffice";

        public const string SystemAdminUser = "c";
        public const string SystemAdminUserPassword = "champion1!";


        public long? ResourceId { get; set; }

        public virtual Resource Resource { get; set; }

        public long? DashboardSetupId { get; set; }

        public virtual DashboardSetup DashboardSetup { get; set;} 

        public long? PatientId { get; set; }

        public virtual Patient Patient { get; set; }

        public bool? IsNDA { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync
            (
                UserManager<ApplicationUser> manager, 
                string authenticationType = DefaultAuthenticationTypes.ApplicationCookie
            )
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
