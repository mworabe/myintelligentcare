﻿using PatientCare.Core.Entities;
using System;

namespace PatientCare.Web.Api.Models.LeaveRequests
{
    public class LeaveRequestsRequest
    {
        public long Id { get; set; }

        public long LeaveTypeId { get; set; }        

        public long? SupervisorId { get; set; }

        public long ResourceId { get; set; }        

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? StatusModifiedDate { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

        public string CommentFromSupervisor { get; set; }

        public string Canceled { get; set; }

    }
}