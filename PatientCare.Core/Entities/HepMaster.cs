﻿using PatientCare.Core.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Hep Master.
    /// </summary>
    public class HepMaster : Entity
    {
        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public long? OwnerId { get; set; }
        public virtual Resource Owner { get; set; }

        public long? PatientCaseId { get; set; }
        public virtual PatientCase PatientCase { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual ICollection<HepDetail> HepDetails { get; set; }

        public virtual ICollection<HepGroup> HepGroups { get; set; }

        public bool? IsSaveAsDraft { get; set; }

    }
}
