﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Sent Email.
    /// </summary>
    public class SentEmail:Entity
    {
        public long IdProject { get; set; }

        public string From { get; set; }
        public string To { get; set; }

        public string Subject { get; set; }
        public string Body { get; set; }

        public DateTime SendedAt { get; set; }
        public EmailTypeEnum EmailType { get; set; }
    }
}
