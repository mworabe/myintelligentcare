﻿'use strict';

angular.module("EDZoutstaffingPortalApp")
    .factory('TwilioChatFactory', [
        '$rootScope',
        '$timeout',
        'Auth',
        'ChatService',
        function ($rootScope, $timeout, Auth, ChatService) {
            var currentUser = Auth.getCurrentUser();
            var currentChatUser = null;
            var accessToken = null;
            var client = null;
            var accessManager = null;

            

            function login() {
                currentUser = Auth.getCurrentUser();
                ChatService.getTwilioToken({ userId: currentUser.nameid, deviceType: "" }, function (result) {
                    if (result && result.token) {
                        accessToken = result.token;
                        init();
                    }
                }, function (error) {
                    console.error("unable to load twilio token", error);
                });
            }

            function submitNewMssage(message) {
                ChatService.getSessionByChannelId({ ChannelsId: message.channel.sid }, function (result) {
                    $rootScope.$broadcast('onNewMessage', {
                        session: result,
                        message: message
                    });
                }, function (error) {
                });
            }

            function init() {
                client = new Twilio.Chat.Client(accessToken, { logLevel: '' });
                currentChatUser = client.userInfos;
                accessManager = new Twilio.AccessManager(accessToken);
                //accessManager.on('tokenUpdated', am => client.updateToken(am.token));

                accessManager.on('tokenUpdated', function (am) {
                    client.updateToken(am.token)
                });

                accessManager.on('tokenExpired', function () {
                    ChatService.getTwilioToken({ userId: currentUser.nameid, deviceType: "" }, function (result) {
                        if (result && result.token) {
                            accessToken = result.token;
                            accessManager.updateToken(accessToken);

                        }
                    }, function (error) {
                        console.error("unable to load twilio token", error);
                    });
                });
              


                /*accessManager.on('tokenExpired', () => {
                    ChatService.getTwilioToken({ userId: currentUser.nameid, deviceType: "" }, function (result) {
                        if (result && result.token) {
                            accessToken = result.token;
                            accessManager.updateToken(accessToken);

                        }
                    }, function (error) {
                        console.error("unable to load twilio token", error);
                    });
                });*/

                client.on('channelInvited', function (channel) {
                    $rootScope.$broadcast('channelInvited', channel);
                    channel.join();
                    console.log('Invited to channel ' + channel.friendlyName);
                });

                client.on('messageAdded', function (message) {
                    console.log('New Message added ' + message);
                    if (currentChatUser &&
                        currentChatUser.identity &&
                        currentChatUser.identity != message.author &&
                        $rootScope.currentChannelId != message.channel.sid) {
                        submitNewMssage(message);
                    }

                });
            }

            function shutDown() {
                if (client) {
                    client.shutdown();
                }
                currentUser = null;
                currentChatUser = null;
                accessToken = null;
                client = null;
                accessManager = null;
                console.info("Twilio shut down");
            }

            return {
                login: function () {
                    login();
                },
                shutDown: function () {
                    shutDown();
                },
                getClient: function () {
                    return client;
                },
                setCurrentUser: function (item) {
                    if (item.userDetails) {
                        for (var i = 0; i < item.userDetails.length; i++) {
                            var user = item.userDetails[i];
                            if (user.userId != currentUser.nameid) {
                                item.otherUser = user;
                                return;
                            }
                        }
                    }
                }


            }
        }]);