﻿using System;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Certificates operations.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Certificates")]
    public sealed class CertificatesController : EdzPicklistApiController<Certificate>
    {
        /// <summary>
        /// Description : In this class we pass Certificate IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="picklistRepository">Certificate Entity</param>
        public CertificatesController(IPicklistRepository<Certificate> picklistRepository)
            : base(picklistRepository)
        {
            GetPicklistStatusCodeErrorFunc = () => GetPicklistStatusCodeError(Picklist.Certificates);
        }

        /// <summary>
        /// Description : Use for Get Picklist Location
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        protected override Uri GetPicklistLocation(long id)
        {
            return new Uri("/Certificates/" + id, UriKind.Relative);
        }
    }
}