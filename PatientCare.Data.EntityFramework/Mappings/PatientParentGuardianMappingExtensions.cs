﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PatientParentGuardianMappingExtensions
    {
        public static DbModelBuilder MapPatientParentGaurdian(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<PatientParentGuardian>().ToTable("patient_parents_guardians").HasKey(i => i.Id);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<PatientParentGuardian>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.FirstName)
                .HasColumnName("first_name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.LastName)
                .HasColumnName("last_name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.RelationshiptoPatient)
                .HasColumnName("relationship_to_patient")
                .IsOptional();

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.Address)
                .HasColumnName("address")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.ZipCode)
                .HasColumnName("zipcode")
                .IsOptional()
                .HasMaxLength(10);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.CityId)
                .HasColumnName("city_id")
                .IsOptional();

            modelBuilder.Entity<PatientParentGuardian>()
                .HasOptional<City>(p => p.City)
                .WithMany()
                .HasForeignKey(p => p.CityId);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.CountryId)
                .HasColumnName("country_id")
                .IsOptional();

            modelBuilder.Entity<PatientParentGuardian>()
                .HasOptional<Country>(p => p.Country)
                .WithMany()
                .HasForeignKey(p => p.CountryId);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.StateId)
                .HasColumnName("state_id")
                .IsOptional();

            modelBuilder.Entity<PatientParentGuardian>()
                .HasOptional<State>(p => p.State)
                .WithMany()
                .HasForeignKey(p => p.StateId);

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.CellNumber)
                .HasColumnName("cell_number")
                .IsOptional();

            modelBuilder.Entity<PatientParentGuardian>()
               .Property(p => p.CellNumberIsPrimary)
               .HasColumnName("cell_number_is_primary")
               .IsOptional();

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.HomeNumber)
                .HasColumnName("home_number")
                .IsOptional();

            modelBuilder.Entity<PatientParentGuardian>()
                .Property(p => p.HomeNumberIsPrimary)
                .HasColumnName("home_number_is_primary")
                .IsOptional();

            return modelBuilder;
        }
    }
}
