"use strict";
(function baseCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .service("edzBaseCustomField", ['$compile', baseCustomField]);

    ///////////////////////////////////////////////////////
    function baseCustomField($compile) {
        return {
            name: null,
            required: null,
            optionsTemplate: null,
            optionsTemplateEdit: null,
            getHtmlForCreating: function getHtmlForCreating(scope) {
                return $compile(this.optionsTemplate)(scope);
            },
            getHtmlForEditing: function getHtmlForCreating(scope) {
                return $compile(this.optionsTemplateEdit)(scope);
            }
        }
    }
})();
