﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Range Type Enum.
    /// </summary>
    public enum RangeTypeEnum
    {
        L = 10,
        M = 20,
        H = 30
    }
}
