"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("clientCompany", {
                url: "/client-company",
                templateUrl: "Client/app/controllers/clientCompany/clientCompany.html",
                controller: "ClientCompanyCtrl as ClientCompanyCtrl",
                data: {
                    security: {
                        authenticated: true
                    }
                }
            }).state("clientCompanyEdit", {
                url: "/client-company/edit/:id",
                templateUrl: "Client/app/controllers/clientCompany/clientComapnyEdit.html",
                controller: "ClientCompanyEditCtrl as ClientCompanyEditCtrl",
                resolve: {
                    initialValues: [
                        "$q", "$stateParams", "CustomField", "ClientCompany", "RoleSetup", "ActivityService",
                        function ($q, $stateParams, customFieldService, clientCompanyService, roleSetupService, activityService) {

                            function loadClient() {
                                var defer = $q.defer();
                                if (!$stateParams.id) {
                                    defer.resolve();
                                } else {
                                    clientCompanyService.get({ id: $stateParams.id }).$promise.then(function (response) {
                                        defer.resolve(response);
                                    }, function (reason) {
                                        defer.resolve({
                                            errorCode: reason.status
                                        });
                                    });
                                }
                                return defer.promise;
                            }

                            function getProjectActivity() {
                                var defer = $q.defer();
                                activityService.activityAccessibleList({ search: "" }).$promise.then(function (responce) {
                                    defer.resolve(responce);
                                }, function (reason) {
                                    defer.resolve(reason.status);
                                });


                                return defer.promise;
                            }



                            /**
                                Back end calls are pending, do Not Use.
                            */
                            function loadClientConfigLayout() {
                                var defer = $q.defer();
                                roleSetupService.clientConfigLayout().$promise.then(function (response) {
                                    defer.resolve(response);
                                }, function (reason) {
                                    defer.resolve(reason);
                                });
                                return defer.promise;
                            }

                            function loadConfigLayout() {
                                var defer = $q.defer();
                                roleSetupService.projectConfigLayout().$promise.then(function (response) {
                                    defer.resolve(response);
                                });

                                return defer.promise;
                            }

                            function getEmptyArray() {
                                var defer = $q.defer();
                                defer.resolve([]);
                                return defer.promise;
                            }

                            //loadClientConfigLayout() -- need to make this call on back end -- url -- `client-config-layout`, `client-layout-config-for-role`, `client-layout-config-for-role`
                            /*  Sending Empty Array, for Client Field Configurations, partially implemented CLient for Default Layout.  */
                            return $q.all([loadConfigLayout(), loadClient(), []])
                                .then(function (resolutions) {
                                    return resolutions;
                                });
                        }
                    ]
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });