﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;
using System.Data.Entity.Infrastructure.Annotations;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class CountryMappingExtensions
    {
        public static DbModelBuilder MapCountry(this DbModelBuilder modelBuilder, string indexName)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<Country>().ToTable("countries")
                .HasKey(i => i.Id);

            modelBuilder.Entity<Country>().Property(p => p.Name)
                .HasColumnName("name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute(indexName) { IsUnique = true }));

            modelBuilder.Entity<Country>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<Country>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<Country>()
                .Property(p => p.IsoCode)
                .HasColumnName("iso_code")
                .IsOptional();

            modelBuilder.Entity<Country>()
                .Property(p => p.IsdCode)
                .HasColumnName("isd_code")
                .IsOptional();

            return modelBuilder;
        }
    }
}