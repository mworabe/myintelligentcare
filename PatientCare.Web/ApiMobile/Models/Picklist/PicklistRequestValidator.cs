﻿using FluentValidation;

namespace PatientCare.Web.ApiMobile.Models.Picklist
{
    public class PicklistRequestValidator : AbstractValidator<PicklistRequest>
    {
        public PicklistRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);
        }
    }
}