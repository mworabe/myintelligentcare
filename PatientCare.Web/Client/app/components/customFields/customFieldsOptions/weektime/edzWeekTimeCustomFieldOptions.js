"use strict";

(function edzWeekTimeCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzWeekTimeCustomFieldOptions", [
            edzWeekTimeCustomFieldOptions
        ]);

    function edzWeekTimeCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/weektime/edzWeekTimeCustomField.html",
            restrict: "E",
            scope: false,
            controller: ["$scope", function ($scope) {

                var weekdays = [
                    'Sun.',
                    'Mon.',
                    'Tues.',
                    'Weds.',
                    'Thurs.',
                    'Fri',
                    'Sat',
                    'Sun'
                ];

                if($scope.value) {
                    $scope.weekdaysValues = JSON.parse($scope.value);
                } else {
                    $scope.weekdaysValues = _.range(7).map(function(element, index) {
                        return {name: weekdays[index], from: null, to: null}
                    });
                }

                $scope.$watch("weekdaysValues", function(newValue) {
                    $scope.value = JSON.stringify(newValue);
                }, true);

                function activate() {
                    $scope.validation = {};
                    $scope.validation.require = $scope.require;
                    $scope.validation.regex = new RegExp("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", "i");
                }
                activate();
            }]
        };
    }
})();
