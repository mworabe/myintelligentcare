﻿using PatientCare.Web.ApiMobile.Models.PatientGoals;

namespace PatientCare.Web.ApiMobile.Models.Patients
{
    public class PatientMobileProfileMobileRequest
    {
        public long Id { get; set; }

        public string PatientFullName { get; set; }

        public string PatientLanguage { get; set; }

        public long? PatientId { get; set; }

        public PatientGoalMobileRequest[] Goals { get; set; }
    }
}