﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Core.Services.Mailing
{
    public interface IEmailSender
    {
        void Send(MailMessage message);

        void Send(IList<MailMessage> mesages);

        void SendMailAsync(MailMessage mesages);

        void SendMailAsync(IList<MailMessage> mesages);
    }
}
