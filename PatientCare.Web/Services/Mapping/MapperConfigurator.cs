﻿using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Services.Mapping;
using PatientCare.Web.Services.Mapping.Mobile;

namespace PatientCare.Web.Services.Mapping
{
    public static class MapperConfigurator
    {
        public static void Configure()
        {
            var mapperProvider = new EmitMapperMapperProvider()
                .PicklistRegisterMappings<Language>()
                .PicklistRegisterMappings<Skill>()
                .PicklistRegisterMappings<ResourceCompany>()
                .PicklistRegisterMappings<SurveyCategory>()
                .PicklistRegisterMappings<Certificate>()
                .PicklistRegisterMappings<Division>()
                .PicklistRegisterMappings<Department>()
                .PicklistRegisterMappings<Country>()
                .PicklistRegisterMappings<State>()
                .PicklistRegisterMappings<City>()
                .PicklistRegisterMappings<DashboardSetup>()
                .PicklistRegisterMappings<Resource>()
                .PicklistRegisterMappings<Clinic>()
                .PicklistRegisterMappings<ClinicLocation>();

            mapperProvider = mapperProvider
                .ResourceCriminalRecordRegisterMappings()
                .ResourceEducationRegisterMappings()
                .ResourceSpokenLanguageRegisterMappings()
                .ResourceBehaviouralDetailRegisterMappings()
                .ResourceRecognitionRegisterMappings()
                .ResourceIndustryRegisterMappings()
                .ResourcePracticeAreaRegisterMappings()
                .ResourceTravelRegisterMappings()
                .ResourceTeamRegisterMappings()
                .ResourcePreviousEmploymentRegisterMappings()
                .ResourceSurveyResultRegisterMappings()
                .ResourceCertificateRegisterMappings()
                .ResourceSkillRegisterMappings()
                .ResourceScoreRegisterMappings()
                .ResourceScoreFilterRegisterMappings()
                .ResourceWorkAssignementRegisterMappings()
                .ResourceWorkExperiencesRegisterMappings()
                .ResourceProfessionalOrganizationRegisterMappings()
                .CountriesRegisterMappings()
                .ResourceRegisterMappings()
                .ResourceInsuranceRegisterMappings()
                .ResourceTimeoffBenefitRegisterMappings();

            mapperProvider = mapperProvider
                .SurveyQuestionRegisterMappings()
                .JobTitleRegisterMappings()
                .AccountRegisterMappings()
                .RoleSetupRegisterMappings()
                .CustomFieldRegisterMappings()
                .DashboardSetupRegisterMappings();

            mapperProvider = mapperProvider
                .HolidayRegisterMappings()
                .LeaveTypeRegisterMappings()
                .EntryTypeRegisterMappings()
                .UserLoginHistoryRegisterMappings()
                .LeaveRequestRegisterMappings();

            mapperProvider = mapperProvider
                .DropDownConfigRegisterMappings()
                .UiConfigurationRegisterMappings();

            mapperProvider = mapperProvider
                .ClinicRegisterMappings()
                .ClinicLocationRegisterMappings();

            mapperProvider = mapperProvider
                .AppointmentTypeRegisterMappings()
                .ReferralPhysicianRegisterMappings()
                .PatientRegisterMappings()
                .PatientAddressRegisterMappings()
                .PatientParentGuardianRegisterMappings()
                .InjuryRegionRegisterMappings()
                .DiagnosisRegisterMappings()
                .PatientCaseRegisterMappings()
                .PatientAppointmentRegisterMappings()
                .ExercisePositionMappings()
                .ExerciseActivityMappings()
                .Exercise_InjuryMappings()
                .MasterTemplateRegisterMappings()
                .ExerciseRegisterMappings();

            mapperProvider = mapperProvider
                .HepFrequencyRegisterMappings()
                .HepDetailRegisterMappings()
                .HepGroupRegisterMappings()
                .HepMasterRegisterMappings()
                .hepPatientPlanRegisterMappings();

            #region Mobile Mapping

            mapperProvider = mapperProvider
                 .AppointmentTypeRegisterMappings()
                 .PatientMobileRegisterMappings()
                 .PatientAppointmentMobileRegisterMappings()
                 .HepFrequencyMobileRegisterMappings()
                 .HepDetailMobileRegisterMappings()
                 .HepMasterMobileRegisterMappings()
                 .PatientWorkoutAnalyticMobileRegisterMappings()
                 .PatientMobileProfileMobileRegisterMappings()
                 .ExerciseCommentMobileRegisterMappings()
                 .ExerciseRatingMobileRegisterMappings()
                 .AppFunctionalRatingMobileRegisterMappings()
                 .PatientGoalMobileRegisterMappings()
                 .ChatMobileRegisterMappings()
                 .ChatMemberMobileRegisterMappings()
                 .ChatKeyRegisterMappings();

            #endregion

            Mapper.SetMapperProvider(mapperProvider);
        }
    }
}