﻿namespace PatientCare.Core.Services.RoleSetupService
{
    public enum RestrictAccessTo
    {
        PersonalInfo = 0,
        Salary = 1,
        BackgroundChecks = 2,
        CriminalRecords = 3,
        PreviousEmployments = 4
    }
}
