﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Patients
{
    [DataContract]
    public class PatientAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string PatientNumber { get; set; }

        [DataMember]
        public string NickName { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public long? ClinicId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Clinic { get; set; }

        [DataMember]
        public long? ClinicLocationId { get; set; }

        [DataMember]
        public PicklistDefaultResponse ClinicLocation { get; set; }

        [DataMember]
        public long? CaseId { get; set; }

        [DataMember]
        public string CaseNo { get; set; }

        [DataMember]
        public PatientStatusEnum? Status { get; set; }

        [DataMember]
        public bool? IsDeleted { get; set; }

    }
}