﻿using PatientCare.Web.Api.Models.HepDetails;
using System;

namespace PatientCare.Web.Api.Models.HepMasters
{
    public class HepMasterRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public long? OwnerId { get; set; }

        public long? PatientCaseId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public HepDetailRequest[] HepDetails { get; set; }

        public bool? IsSaveAsDraft { get; set; }
    }   
}