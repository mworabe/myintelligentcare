﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

public class InMemoryMultipartStreamProvider : MultipartStreamProvider
{
    private readonly NameValueCollection _formData = new NameValueCollection();
    private readonly List<HttpContent> _fileContents = new List<HttpContent>();
    private readonly Collection<bool> _isFormData = new Collection<bool>();

    public NameValueCollection FormData
    {
        get { return this._formData; }
    }

    public List<HttpContent> Files
    {
        get { return this._fileContents; }
    }

    public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
    {
        ContentDispositionHeaderValue contentDisposition = headers.ContentDisposition;
        if (contentDisposition != null)
        {
            this._isFormData.Add(string.IsNullOrEmpty(contentDisposition.FileName));

            return new MemoryStream();
        }
        throw new InvalidOperationException(string.Format("Did not find required '{0}' header field in MIME multipart body part.", "Content-Disposition"));
    }

    public override async Task ExecutePostProcessingAsync()
    {
        for (int index = 0; index < Contents.Count; index++)
        {
            if (this._isFormData[index])
            {
                HttpContent formContent = Contents[index];

                ContentDispositionHeaderValue contentDisposition = formContent.Headers.ContentDisposition;
                string formFieldName = UnquoteToken(contentDisposition.Name) ?? string.Empty;

                string formFieldValue = await formContent.ReadAsStringAsync();
                this.FormData.Add(formFieldName, formFieldValue);
            }
            else
            {
                this._fileContents.Add(this.Contents[index]);
            }
        }
    }

    private static string UnquoteToken(string token)
    {
        if (string.IsNullOrWhiteSpace(token))
        {
            return token;
        }

        if (token.StartsWith("\"", StringComparison.Ordinal) && token.EndsWith("\"", StringComparison.Ordinal) && token.Length > 1)
        {
            return token.Substring(1, token.Length - 2);
        }

        return token;
    }
}