﻿"use strict";

(function edzAlphaCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzAlphaCustomFieldEditOptions", [
            "edzCustomFieldEditHelper",
            "CUSTOM_FIELD",
            edzAlphaCustomFieldOptions
        ]);

    function edzAlphaCustomFieldOptions(edzCustomFieldEditHelper, CUSTOM_FIELD) {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/alpha/edzAlphaCustomFieldEdit.html",
            restrict: "E",
            bindToController: true,
            controller: ["$scope" ,function ($scope) {
                var self = this;
                function loadData() {
                    self.model = {};
                    self.model.allowedAdditionalCharacters = edzCustomFieldEditHelper.
                        getOrCreatePropertyWithPush($scope.data, CUSTOM_FIELD.PROPERTY_NAME.ALPHA.ALLOWED_ADDITIONAL_CHARACTERS, $scope.id);
                    self.model.length = edzCustomFieldEditHelper.getOrCreatePropertyWithPush($scope.data, CUSTOM_FIELD.PROPERTY_NAME.GENERAL.LENGTH, $scope.id);
                    self.model.length.value = parseInt(self.model.length.value);
                    self.model.resizeField = edzCustomFieldEditHelper.getOrCreatePropertyWithPush($scope.data, CUSTOM_FIELD.PROPERTY_NAME.ALPHA.RESIZE_FIELD, $scope.id);
                    self.model.resizeField.value = Boolean(self.model.resizeField.value === "True");
                }

                function activate() {
                    loadData();
                }

                activate();
            }],
            controllerAs: "edzAlphaCustomFieldEditCtrl"
        };
    }
})();
