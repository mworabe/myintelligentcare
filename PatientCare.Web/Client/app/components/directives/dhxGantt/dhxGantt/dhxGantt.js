﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

window.first = true;
angular.module("EDZoutstaffingPortalApp")
    .directive("dhxGantt", ["$compile", "$timeout",
        function ($compile, $timeout) {
            return {
                restrict: "A",
                scope: {
                    data: "=ngData",
                    scopeCreater: "=ngScopeCreater",
                    onAfterTaskDrag: "=?onAfterTaskDrag"

                },
                transclude: true,
                template: "<div ng-transclude></div>",
                link: function ($scope, $element) {
                    $scope.onAfterTaskDrag = $scope.onAfterTaskDrag || angular.noop;
                    //watch data collection, reload on changes
                    
                    $scope.$watch(function () {
                        if ($scope && $scope.data && $scope.data.data) {
                            return $scope.data.data.length
                        }
                        return 0;
                    }, function (collection) {
                        gantt.clearAll();
                        if (collection) {
                            gantt.parse($scope.data, "json");
                        }
                    }, true);

                    //size of gantt
                    function updateScope(id) {
                        var div = gantt.getTaskRowNode(id);
                        if (div) {
                            var scope = $scope.scopeCreater();
                            var element = angular.element(div);
                            var linkFn = $compile(element.contents());
                            linkFn(scope);
                        }
                    }

                    function updateScopes() {
                        _.forEach(gantt.getTaskByTime(), function (item) {
                            if (!item.id)
                                return;
                            updateScope(item.id);
                        });
                    }

                    function onDataRenderHandler() {
                        updateScopes();
                    }
                    function onBeforeLightboxHandler() {
                        return false;
                    }

                    function onAfterTaskDragHandler(id, mode, e) {
                        $scope.$apply(function () {
                            updateScope(id);
                        });

                        var task = gantt.getTask(id);
                        $scope.onAfterTaskDrag(task, mode);
                        return true;
                    }

                    function onLinkDblClick() {
                        return false;
                    }


                    var events = [];
                    var firstLoad = true;
                    function onGanttScroll(x, y) {
                        if ((x !== 0 || y !== 0) && firstLoad) {
                            gantt.scrollTo(0, 0);
                        }
                    }

                    $timeout(function () {
                        firstLoad = false;
                    }, 5000);

                    events.push(gantt.attachEvent("onGanttScroll", onGanttScroll));

                    events.push(gantt.attachEvent("onBeforeLightbox", onBeforeLightboxHandler));

                    events.push(gantt.attachEvent("onLinkDblClick", onLinkDblClick));

                    events.push(gantt.attachEvent("onDataRender", onDataRenderHandler));

                    events.push(gantt.attachEvent("onAfterTaskDrag", onAfterTaskDragHandler));

                    function ganttClear() {
                        for (var i = 0; i < events.length; i++)
                            gantt.detachEvent(events[i]);
                        events = [];
                        gantt.config.columns = [];
                        gantt.clearAll();

                    }

                    $scope.$on("gantt:clear", function () {
                        ganttClear();
                    });

                    gantt.config.select_task = false;
                    gantt.config.editable_property = false;

                    gantt.init($element[0]);
                }
            };
        }
    ]);