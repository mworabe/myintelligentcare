"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("FavoriteTemplatesCntrl", ["$scope", 'FavoriteTemplateService',
        function ($scope,  FavoriteTemplateService) {
            var self = this;
            this.get = FavoriteTemplateService.query;
            this.cols = FavoriteTemplateService.getTableOption;
            this.delete = FavoriteTemplateService.delete;
            this.cols.hideBulkEdit = true;

        }
    ]);