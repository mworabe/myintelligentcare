﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Salary Type Enum.
    /// </summary>
    public enum SalaryPerTypeEnum
    {
        Hour = 0,
        Week = 1,
        Month = 2,
        Year = 3
    }
}