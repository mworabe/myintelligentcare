﻿namespace PatientCare.Web.Api.Models.Patients
{
    public class PatientAutoCompleteRequest
    {
        public string Search { get; set; }
        
        public long? ClinicId { get; set; }
    }   
}