﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Mapping;
using PatientCare.Data.AbstractRepository;
using PatientCare.Utilities;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Resources.area.Resources;
using Swashbuckle.Swagger.Annotations;

namespace PatientCare.Web.Api.Controllers.Abstract
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-28-2017.
    /// Title : EdzPicklist Api Controller.
    /// </summary>
    public abstract class EdzPicklistApiController<T> : ApiBaseController where T : NamedEntity
    {
        /// <summary>
        /// Description : EdzPicklist Api IPicklistRepository.
        /// </summary>
        protected readonly IPicklistRepository<T> PicklistRepository;

        /// <summary>
        /// 
        /// </summary>
        protected Func<StatusCodeResult> GetPicklistStatusCodeErrorFunc { get; set; }

        /// <summary>
        /// Description : EdzPicklist Api Class Constructor. 
        /// </summary>
        /// <param name="picklistRepository">IPicklist Repository</param>
        protected EdzPicklistApiController(IPicklistRepository<T> picklistRepository)
        {
            PicklistRepository = picklistRepository;
        }

        /// <summary>
        /// Method Description : Using for Get Items.     
        /// </summary>
        /// <param name="request">List Request Model</param>
        /// <returns>Picklist Response</returns>
        [ResponseType(typeof(IList<PicklistResponse>))]
        [HttpGet]
        public IHttpActionResult GetItems([FromUri]ListRequest request)
        {
            request.SortField = "name";
            request.SearchField = "name"; // hard-coded field for searching in picklists
            var query = PagingExtensions<T>.CreatePagedQuery(request);
            var page = PicklistRepository.PagedList(query);
            var dtos = Mapper.Map<T[], PicklistResponse[]>(page.Entities.ToArray());
            var response = new ListResponse<PicklistResponse>(dtos, page.TotalCount);
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Get Items by Id.     
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Picklist Response Model</returns>
        [HttpGet]
        [ResponseType(typeof(PicklistResponse))]
        public IHttpActionResult GetItem(long id)
        {
            var item = PicklistRepository.Read(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<T, PicklistResponse>(item));
        }

        /// <summary>
        /// Method Description : Using for Update Items by Id. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="itemVm">Picklist Request Model</param>
        /// <returns>Picklist Response Model</returns>
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult EditItem(long id, [FromBody] PicklistRequest itemVm)
        {
            var statusCodeResult = GetPicklistStatusCodeErrorFunc();
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (PicklistRepository.Exists(FilterQuery.Create<T>().AddFilter(p => p.Name == itemVm.Name && p.Id != id)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<T>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var item = PicklistRepository.Read(id);
            item.Name = itemVm.Name;
            PicklistRepository.Update(item,
                new UpdateOptions<T>()
                .WithNotModified(p => p.Created));

            return Ok(Mapper.Map<T, PicklistResponse>(item));
        }

        /// <summary>
        /// Method Description : Using for Create Items. 
        /// </summary>
        /// <param name="itemVm">Picklist Request Model</param>
        /// <returns>Picklist Response Model</returns>
        [HttpPost]
        [ResponseType(typeof(PicklistResponse))]
        [SwaggerResponse(201, "Created", typeof(PicklistResponse))]
        public IHttpActionResult CreateItem([FromBody]PicklistRequest itemVm)
        {
            var statusCodeResult = GetPicklistStatusCodeErrorFunc();
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (PicklistRepository.Exists(FilterQuery.Create<T>().AddFilter(p => p.Name == itemVm.Name)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<T>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var item = Mapper.Map<PicklistRequest, T>(itemVm);
            PicklistRepository.Create(item,UpdateOptions.Create<T>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return Created(GetPicklistLocation(item.Id), Mapper.Map<T, PicklistResponse>(item));
        }

        /// <summary>
        /// Method Description : Using for Delete Items by Id.     
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Picklist Response Model</returns>
        [HttpDelete]
        [ResponseType(typeof(PicklistResponse))]
        public IHttpActionResult DeleteItem(long id)
        {
            var statusCodeResult = GetPicklistStatusCodeErrorFunc();
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            var item = PicklistRepository.Read(id);
            if (item == null)
            {
                return NotFound();
            }
            try
            {
                PicklistRepository.Delete(item);
            }
            catch (AggregateException ex)
            {
                ModelState.AddModelError("", string.Join(", ", ex.InnerExceptions.Select(x => x.Message)));
            }

            return Ok(item);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Items.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <param name="dropdowntype">Dropdown Type Value</param>
        /// <returns>Picklist Response Model</returns>
        [HttpGet]
        [Route("autocomplete")]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search, string dropdowntype = null)
        {
            var filter = new FilterQuery<T>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
            }

            var autocompleteResult = await PicklistRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<T[], PicklistResponse[]>(autocompleteResult.ToArray());

            if (dropdowntype == "country")
            {
                List<PicklistResponse> listData = result.ToList();
                var item = listData.Where(x => x.Name.StartsWith("United States")).FirstOrDefault();
                if (item != null)
                {
                    var oldIndex = listData.IndexOf(item);
                    listData.RemoveAt(oldIndex);
                    listData.Insert(0, item);
                    return Ok(listData);
                }
            }

            return Ok(result);
        }

        protected abstract Uri GetPicklistLocation(long id);
    }
}