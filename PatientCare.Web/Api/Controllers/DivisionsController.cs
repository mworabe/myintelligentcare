﻿using System;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Division operations.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Divisions")]
    public sealed class DivisionsController : EdzPicklistApiController<Division>
    {
        /// <summary>
        /// Description : In this class we pass Division IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="picklistRepository">Certificate Entity</param>
        public DivisionsController(IPicklistRepository<Division> picklistRepository) : base(picklistRepository)
        {
            GetPicklistStatusCodeErrorFunc = () => GetPicklistStatusCodeError(Picklist.Divistions);
        }

        /// <summary>
        /// Description : Use for Get Picklist Location
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        protected override Uri GetPicklistLocation(long id)
        {
            return new Uri("/Divisions/" + id, UriKind.Relative);
        }
    }
}