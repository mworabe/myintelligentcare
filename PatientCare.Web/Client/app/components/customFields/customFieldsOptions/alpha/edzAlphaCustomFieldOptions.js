"use strict";

(function edzAlphaCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzAlphaCustomFieldOptions", [
            "CUSTOM_FIELD",
            "edzCustomFieldEditHelper",
            edzAlphaCustomFieldOptions
        ]);

    function edzAlphaCustomFieldOptions(CUSTOM_FIELD, edzCustomFieldEditHelper) {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/alpha/edzAlphaCustomField.html",
            restrict: "E",
            scope: false,
            controller: ["$scope", function ($scope) {
                var option = $scope.option;
                var allowedAdditionalCharacters = edzCustomFieldEditHelper.getPropertyByName(option, CUSTOM_FIELD.PROPERTY_NAME.ALPHA.ALLOWED_ADDITIONAL_CHARACTERS) || {};
                var length = edzCustomFieldEditHelper.getPropertyByName(option, CUSTOM_FIELD.PROPERTY_NAME.GENERAL.LENGTH) || {};

                var resizeField = edzCustomFieldEditHelper.getPropertyByName(option, CUSTOM_FIELD.PROPERTY_NAME.ALPHA.RESIZE_FIELD) || {};
                $scope.fieldStyle = resizeField.value === "True" ? {
                    width: length.value * 10 + 12 + 'px'
                } : {};

                $scope.validation = {};
                $scope.validation.length = length.value;
                $scope.validation.allowedAdditionalCharacters = allowedAdditionalCharacters.value;
                $scope.validation.require = $scope.require;
                
                var additionalCharacters = $scope.validation.allowedAdditionalCharacters ? $scope.validation.allowedAdditionalCharacters : "";
                additionalCharacters = additionalCharacters.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");;
                $scope.validation.regex = new RegExp("^[a-zA-Z]*[A-Za-z " + additionalCharacters + "]*$", "i");

                function loadData() {
                    
                }

                function activate() {
                    loadData();
                }

                activate();
            }]
        };
    }
})();
