﻿using PatientCare.Core.Enums;
using System;

namespace PatientCare.Web.Api.Models.ResourceProfessionalOrganizations
{
    public class ResourceProfessionalOrganizationRequest
    {
        public long Id { get; set; }

        public long ResourceId { get; set; }
        
        public string Organization { get; set; }

        public string CommitteeMembership { get; set; }

        public string CommitteeTitle { get; set; }

        public long? CommitteeMembershipId { get; set; }

        public long? CommitteeTitleId { get; set; }
    }
}