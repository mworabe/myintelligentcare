namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterGetAllExerciseSPChanges : DbMigration
    {
        public override void Up()
        {
            //AlterStoredProcedure(
            //"dbo.sp_get_all_exercises",
            //p => new
            //{
            //    keywords = p.String(defaultValueSql: null),
            //    regionId = p.String(defaultValueSql: null),
            //    positionId = p.String(defaultValueSql: null),
            //    activityId = p.String(defaultValueSql: null),
            //    pageIndex = p.Int(defaultValueSql: null),
            //    pageSize = p.Int(defaultValueSql: null)
            //},
            //body:
            //    @"select 
            //        CAST(exe.Id AS BIGINT) as ExerciseId,CAST(exe.region_id AS  BIGINT) AS InjuryRegionId,inreg.name as InjuryRegionName,
	           //     CAST(exe.position_id AS  BIGINT) AS PositionId,exepos.name as ExercisePositionName,
            //        CAST(exe.activity_id AS  BIGINT) AS ActivityId,exeact.name as ExerciseActivityName ,
	           //     exe.keywords as KeyWords,exe.comments as Comments,exe.Name as Name,
            //        CAST(exemedia.Id AS BIGINT) as ExerciseMediaId,exemedia.original_name as OriginalName,exemedia.name as MediasName
	 
            //        from exercises as exe 
	           //     left join exercise_medias exemedia on exe.Id = exemedia.exercise_id
	           //     left join injury_regions inreg on exe.region_id = inreg.Id
	           //     left join exercise_positions exepos on exe.position_id = exepos.id
	           //     left join exercise_activities exeact on exe.activity_id =	exeact.id
	           //     where ((exe.keywords like ('%'+@keywords+'%')) or (exe.Name like ('%'+@keywords+'%')))
	           //      and (@regionId ='' or Charindex(','+cast(exe.region_id as varchar(max))+',', @regionId) > 0) 
	           //      and (@activityId ='' or Charindex(','+cast(exe.activity_id as varchar(max))+',', @activityId) > 0) 
	           //      and (@positionId ='' or Charindex(','+cast(exe.position_id as varchar(max))+',', @positionId) > 0) 
	           //     order by exe.Id asc
	           //     OFFSET(@pageIndex * @pageSize) ROWS FETCH NEXT @pageSize ROWS ONLY "
            //           );
        }
        
        public override void Down()
        {

        }
    }
}
