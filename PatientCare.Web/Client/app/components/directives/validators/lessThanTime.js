﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngLessThanTime", [
        function () {
            var link = function ($scope, $element, $attrs, ctrl) {

                var validate = function (viewValue) {
                    var comparisonModel = $attrs.ngLessThanTime;

                    if (!viewValue || !comparisonModel) {
                        ctrl.$setValidity('lessThan', true);
                    } else {
                        ctrl.$setValidity('lessThan', Date.parse('01/01/2011 ' + viewValue + ':00') > Date.parse('01/01/2011 ' + comparisonModel + ':00'));
                    }
                    return viewValue;
                };

                ctrl.$parsers.unshift(validate);
                ctrl.$formatters.push(validate);

            };

            return {
                require: 'ngModel',
                restrict: 'A',
                link: link
            };

        }
    ]);