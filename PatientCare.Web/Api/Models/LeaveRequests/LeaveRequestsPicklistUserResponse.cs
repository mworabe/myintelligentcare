﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;
using System;
using PatientCare.Core.Entities;

namespace PatientCare.Web.Api.Models.LeaveRequests
{
    [DataContract]
    public class LeaveRequestsPicklistUserResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? LeaveTypeId { get; set; }

        [DataMember]
        public long? SupervisorId { get; set; }

        [DataMember]
        public string SupervisorName
        {
            get { return (Supervisor != null ? Supervisor.Name : ""); }
            set { SupervisorName = value; }
        }

        [DataMember]
        public long? ResourceId { get; set; }
        [DataMember]
        public string ResourceName
        {
            get { return (Resource != null ? Resource.Name : ""); }
            set { ResourceName = value; }
        }


        [DataMember]
        public virtual LeaveType LeaveType { get; set; }

        [DataMember]
        public string IsPaidType
        {
            get { return LeaveType.IsPaid; }
            set { IsPaidType = value; }
        }

        [DataMember]
        public virtual PicklistResponse Supervisor { get; set; }

        [DataMember]
        public virtual PicklistResponse Resource { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public DateTime? StatusModifiedDate { get; set; }

        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string CommentFromSupervisor { get; set; }

        [DataMember]
        public string Canceled { get; set; }
    }
}