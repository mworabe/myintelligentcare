﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("FinancialPlan", ["$resource", "$state", function ($resource, $state) {
      var resource = $resource("api/FinancialPlan/:id/:versionId/:controller",
        null, //parameters default
        {
            'update': { method: "PUT" },
            'query': { method: "GET", params: {}, isArray: false },
            'financialPlanByProject': { method: "GET", params: { controller: "financial-plan-by-project" }, isArray: false },
            'financialPlan': { method: "GET", params: { controller: "financial-plan" }, isArray: false },
            'financeVersions': { method: "GET", params: { controller: "Versions" }, isArray: false },
            'generateInvoice': { method: "POST", params: { controller: "generate-invoice" }, isArray: false },
            'invoiceFilters': { method: "GET", params: { controller: "invoice-filters" }, isArray: false }
        });

      resource.importSrc = "api/FinancialPlan/import";
      resource.import = function (uploader, file, projectId, cb) {
          cb = cb || angular.noop;
          var url = this.importSrc + "?id=" + projectId;
          uploader.upload({
              url: url,
              data: { file: file }
          }).then(cb);
      };

      resource.getTableOption = {
          hideActionButtons: true,
          hideBulkEdit: true,
          hideAddButton: true,
          hideGridRightOptions: true,
          disabledAddButton: true,
          cols: function () {
              return [
                  { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                  { field: "userName", title: "User Name", sortable: "userName", show: true, dataType: "text", required: true },
                  {
                      field: "versionId",
                      title: "Version",
                      sortable: "versionId",
                      show: true,
                      dataType: "text",
                      required: true,
                      dataTypeView: "href",
                      buildUrl: function (row) {
                          return $state.href("finances", { financePlanId: row.financialPlanId, versionId: row.versionId });
                      }
                  },
                  { field: "created", title: "Created", sortable: "created", show: true, dataType: "text", required: true }
              ];
          }
      };
      return resource;
  }]);