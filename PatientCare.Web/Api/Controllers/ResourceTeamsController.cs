﻿using System;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ResourceTeams;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Team operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceTeamsController : EdzBaseApiController<ResourceTeam, ResourceTeamRequest, ResourceTeamResponse>
    {
        /// <summary>
        /// Description : In this class we pass  Resource Team IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource Team Repository</param>
        public ResourceTeamsController(IResourceTeamRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.ResourceCompany)
                .WithIncludePredicate(item => item.Resources);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }

        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceTeams/" + id, UriKind.Relative);
        }
    }
}