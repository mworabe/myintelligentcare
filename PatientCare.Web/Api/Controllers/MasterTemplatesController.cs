﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.MasterTemplates;
using System.Linq.Expressions;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Web.Services.FileStorage;
using System.Collections.Generic;
using PatientCare.Data.EntityFramework;
using PatientCare.Data.AbstractDataContext;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Master Templates operations.
    /// </summary>
    [Authorize]
    public sealed class MasterTemplatesController : ApiBaseController
    {
        /// <summary>
        /// Description : Master Templates IExerciseService.
        /// </summary>
        private readonly IExerciseService _exerciseService;
        /// <summary>
        /// Description : Master Templates IMasterTemplateRepository.
        /// </summary>
        private readonly IMasterTemplateRepository _masterTemplateRepository;
        /// <summary>
        /// Description : Master Templates IMasterTemplateExerciseRepository.
        /// </summary>
        private readonly IMasterTemplateExerciseRepository _masterTemplateExerciseRepository;
        /// <summary>
        /// Description : Master Templates IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<MasterTemplate> _readOptions = new ReadOptions<MasterTemplate>();

        /// <summary>
        /// Description : Initializes a new instance of the Master Templates Class. 
        /// </summary>
        /// <param name="exerciseService">IExercise Service</param>
        /// <param name="masterTemplateRepository">IMaster Template Repository</param>
        /// <param name="masterTemplateExerciseRepository">IMaster Template Exercise Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public MasterTemplatesController
            (
                IExerciseService exerciseService,
                IMasterTemplateRepository masterTemplateRepository,
                IMasterTemplateExerciseRepository masterTemplateExerciseRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _exerciseService = exerciseService;
            _masterTemplateRepository = masterTemplateRepository;
            _masterTemplateExerciseRepository = masterTemplateExerciseRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/masterTemplates/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Master Templates.
        /// </summary>
        /// <param name="request">Master Templates List Request Model</param>
        /// <returns>Master Templates Response Model</returns>
        [HttpGet]
        [Route("api/masterTemplates/list")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<MasterTemplateListResponse>))]
        public IHttpActionResult GetAllMasterTemplate([FromUri]MasterTemplateListRequest request)
        {
            var query = PagingExtensions<MasterTemplate>.CreatePagedQuery
                      (request,
                          (string.IsNullOrWhiteSpace(request.SearchField)
                              && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                  ?
                             GetSearchColumnExpressions(request.SearchPhrase)
                            : GetAutocompleteSearchExpressions(request.SearchPhrase)
                           )
                           );

            query.AddFilter(x => x.IsMaster == true);
            // here we not take deleted master template in list screen
            query.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);

            var readOptions = new ReadOptions<MasterTemplate>().AsReadOnly();
            var page = _masterTemplateRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<MasterTemplate[], MasterTemplateResponse[]>(page.Entities.ToArray());
            var response = new ListResponse<MasterTemplateResponse>(dtos, page.TotalCount);

            int count = 0;

            foreach (var exercise in response.Data)
            {
                foreach (var file in exercise.TemplateExercises)
                {
                    count++;
                }
                exercise.TemplateExercisesCount = count;
                count = 0;
            }
           

            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Master Templates.   
        /// </summary>
        /// <param name="MasterTemplateRequest">Master Templates Request Model</param>
        /// <returns>Master Templates Response Model</returns>
        [HttpPost]
        [Route("api/masterTemplates")]
        [SwaggerResponse(201, "Created", typeof(MasterTemplateResponse))]
        public async Task<IHttpActionResult> CreateMasterTemplate([FromBody]MasterTemplateRequest MasterTemplateRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                ModelState.AddModelError("Master Template", "your session is expired. please login again");
                return BadRequest(ModelState);
            }
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user == null && user.Resource == null)
            {
                ModelState.AddModelError("Master Template", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }

            var MasterTemplate = Mapper.Map<MasterTemplateRequest, MasterTemplate>(MasterTemplateRequest);

            MasterTemplate.IsFavorite = false;
            MasterTemplate.IsMaster = true;
            MasterTemplate.IsPublic = false;
            MasterTemplate.OwnerId = user.ResourceId;

            await _exerciseService.CreateMasterTemplateAsync(MasterTemplate);

            return Created(GetEntityLocation(MasterTemplate.Id), Mapper.Map<MasterTemplate, MasterTemplateResponse>(MasterTemplate));
        }

        /// <summary>
        /// Method Description : Using for Update Master Templates.   
        /// </summary>
        /// <param name="MasterTemplateRequest">Master Templates Request Model</param>
        /// <returns>Master Templates Response Model</returns>
        [HttpPut]
        [Route("api/masterTemplates/{Id}")]
        public async Task<IHttpActionResult> EditMasterTemplate(long Id, [FromBody] MasterTemplateRequest MasterTemplateRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                ModelState.AddModelError("Master Template", "your session is expired. please login again");
                return BadRequest(ModelState);
            }
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            if (user == null && user.Resource == null)
            {
                ModelState.AddModelError("Master Template", "Logged in user is not associated with Resource, contact admin to assign resource");
                return BadRequest(ModelState);
            }

            var MasterTemplate = Mapper.Map<MasterTemplateRequest, MasterTemplate>(MasterTemplateRequest);
            MasterTemplate.Id = Id;
            MasterTemplate.IsFavorite = false;
            MasterTemplate.IsMaster = true;
            MasterTemplate.IsPublic = false;
            MasterTemplate.OwnerId = user.ResourceId;

            await _exerciseService.UpdateMasterTemplateAsync(MasterTemplate);

            return Ok(Mapper.Map<MasterTemplate, MasterTemplateResponse>(MasterTemplate));
        }

        /// <summary>
        /// Method Description : Using for Delete Master Templates by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Master Templates Response Model</returns>
        [HttpDelete]
        [Route("api/masterTemplates/{Id}")]
        public async Task<IHttpActionResult> DeleteMasterTemplateItem(long id)
        {
            var masterTemplate = _masterTemplateRepository.Read(id);
            if (masterTemplate == null)
                return NotFound();

            await _exerciseService.DeleteMasterTemplateAsync(masterTemplate);

            return Ok(new MasterTemplateResponse { Id = masterTemplate.Id });

            //var statusCodeResult = GetStatusCodeError(Crud.Delete, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            //var item = _masterTemplateRepository.Read(id);
            //if (item == null)
            //{
            //    return NotFound();
            //}

            //var errorMessage = await _exerciseService.DeleteMasterTemplateAsync(item);
            //if (string.IsNullOrEmpty(errorMessage))
            //{
            //    return Ok(item);
            //}

            //ModelState.AddModelError("Resource", errorMessage);
            //return BadRequest(ModelState);


        }

        /// <summary>
        /// Method Description : Using for Get Master Templates by Id.    
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>Master Template Response Model</returns>
        [HttpGet]
        [Route("api/masterTemplates/{Id}")]
        public async Task<IHttpActionResult> GetMasterTemplate(long Id)
        {
            var MasterTemplate = await _exerciseService.ReadMasterTemplateAsync(Id);
            if (MasterTemplate == null)
                return NotFound();

            var viewModel = Mapper.Map<MasterTemplate, MasterTemplateResponse>(MasterTemplate);

            foreach (var file in viewModel.TemplateExercises)
            {
                foreach (var exercise in file.Exercise.Medias)
                {
                    exercise.MediaURL = FileUploadHelpers.GetStorageMediaUrlPhoto(exercise.Name, GetExerciseMediaS3Path(exercise.ExerciseId.Value));
                    exercise.MediaOriginalName = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                    List<FileRequest> fileAll = new List<FileRequest>();
                    FileRequest tempFile = new FileRequest();

                    if (StorageService.UseAmazonStorage == true)
                        tempFile.S3BucketUrl = FileUploadHelpers.GetStorageUrl(exercise.Name, GetExerciseMediaS3Path(exercise.ExerciseId.Value));
                    else
                        tempFile.S3BucketUrl = StorageService.GetProjectMediaDownload + exercise.Id;

                    tempFile.S3BucketName = exercise.Name;
                    tempFile.Name = GetOriginalFileNameWithoutBucketGuid(exercise.Name);

                    fileAll.Add(tempFile);
                    exercise.Files = fileAll.ToArray();
                }
            }

            return Ok(viewModel);
        }

        private string GetExerciseMediaS3Path(long exerciseId)
        {
            return string.Format(FileUploadHelpers.ExerciseMediaFilesTemplate, exerciseId);
        }

        private static Expression<Func<MasterTemplate, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return MasterTemplate => MasterTemplate.Name.Contains(searchPhrase);
        }

        private static Expression<Func<MasterTemplate, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
                return null;

            return MasterTemplate => MasterTemplate.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Master Templates.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Master Templates Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/masterTemplates/autocomplete")]
        [ResponseType(typeof(MasterTemplateAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<MasterTemplate>();
            if (!string.IsNullOrWhiteSpace(search))
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));

            filter.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);
            var autocompleteResult = await _masterTemplateRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<MasterTemplate[], MasterTemplateAutoCompleteResponse[]>(autocompleteResult.ToArray());
          
            return Ok(result);
        }

        /// <summary>
        /// Method Description : Using for Get All Master Templates Detail.       
        /// </summary>
        /// <param name="request">Master Template List Request Model</param>
        /// <returns>Master Template List Detail Response Model</returns>
        [HttpGet]
        [Route("api/masterTemplates")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<MasterTemplateListDetailResponse>))]
        public async Task<IHttpActionResult> GetAllMasterTemplateDetail([FromUri]MasterTemplateListRequest request)
        {
            request.PageIndex = request.PageIndex - 1;

            List<MasterTemplateListDetailResponse> referralList = new List<MasterTemplateListDetailResponse>();
            var response = new ListResponse<MasterTemplateListDetailResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                //For Data
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_mastertemplate_detail_list @responseType='Data',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@sortField = '" + request.SortField + "',@sortOrder = '" + request.SortOrder + "',@searchPhrase = '" + request.SearchPhrase + "'";
                var referralDataList = await ctx.Database.SqlQuery<MasterTemplateListDetailResponse>(query).ToListAsync();
                response.Data = referralDataList.ToArray();

                //For Item Count                
                string itemCountQuery = "EXEC sp_mastertemplate_detail_list @responseType='DataCount',@pageIndex = '" + request.PageIndex + "',@pageSize = '" + request.PageSize + "',@sortField = '" + request.SortField + "',@sortOrder = '" + request.SortOrder + "',@searchPhrase = '" + request.SearchPhrase + "'";
                response.ItemsCount = await ctx.Database.SqlQuery<long>(itemCountQuery).FirstOrDefaultAsync();

                return Ok(response);
            }
        }



    }
}
