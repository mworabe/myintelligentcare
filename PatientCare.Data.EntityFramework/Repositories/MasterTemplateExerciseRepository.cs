﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class MasterTemplateExerciseRepository : RepositoryBase<MasterTemplateExercise>, IMasterTemplateExerciseRepository  
    {
        public MasterTemplateExerciseRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
