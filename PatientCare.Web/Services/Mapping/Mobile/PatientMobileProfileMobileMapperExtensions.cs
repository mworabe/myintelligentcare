﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.Patients;
using PatientCare.Web.ApiMobile.Models.PatientGoals;
using EmitMapper.MappingConfiguration;
using System.Linq;
using PatientCare.Web.Services.FileStorage;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class PatientMobileProfileMobileMapperExtensions
    {
        public static EmitMapperMapperProvider PatientMobileProfileMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientMobileProfile, PatientMobileProfileMobileResponse>
                   (new ExtendedMapConfig<PatientMobileProfile, PatientMobileProfileMobileResponse>()
                        .ForMember(x => x.PatientPhotoNameUrl, y => FileUploadHelpers.GetStorageUrlPhoto(y.Photo, FileUploadHelpers.PatientFolderTemplate))
                    ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientMobileProfile[], PatientMobileProfileMobileResponse[]>());

            var goalsMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<PatientGoalMobileRequest, PatientGoal>(
                    new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientMobileProfileMobileRequest, PatientMobileProfile>(
                        new ExtendedMapConfig<PatientMobileProfileMobileRequest, PatientMobileProfile>()
                            .ForMember(x => x.Goals, y => y.Goals != null ? y.Goals.Select(i => goalsMapper.Map(i)).ToList() : null)
                    ));

            return mapperProvider;
        }
    }
}