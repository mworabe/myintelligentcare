﻿using System;
using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Work Experience.
    /// </summary>
    public class ResourceWorkExperience : Entity
    {
        public long ResourceId { get; set; }

        public virtual Resource Resource { get; set; }

        public long? JobTitleId { get; set; }
        public virtual JobTitle WorkExperienceJobTitle { get; set; }

        //public string Position { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }        

        public virtual ICollection<ResourceWorkAssignment> ResourceWorkAssignments { get; set; }
        
    }
}
