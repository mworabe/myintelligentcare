﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceProfessionalOrganizations;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceProfessionalOrganizationMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceProfessionalOrganizationRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");
            
            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceProfessionalOrganization, ResourceProfessionalOrganizationResponse>
                (new ExtendedMapConfig<ResourceProfessionalOrganization, ResourceProfessionalOrganizationResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceProfessionalOrganization[], ResourceProfessionalOrganizationResponse[]>());
            
            mapperProvider.RegisterMapper(() =>
             ObjectMapperManager.DefaultInstance.GetMapper<ResourceProfessionalOrganizationRequest, ResourceProfessionalOrganization>
                (new ExtendedMapConfig<ResourceProfessionalOrganizationRequest, ResourceProfessionalOrganization>()));
                        
            return mapperProvider;
        }
    }
}