﻿using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.HepPatientPlan
{
    [DataContract]
    public class HepPatientPlanResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public long? HepId { get; set; }

        [DataMember]
        public string HepPlan { get; set; }
      
    }
}