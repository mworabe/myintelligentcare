﻿using System;
using System.Web.Http;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Web.Services.Chat;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using PatientCare.Web.ApiMobile.Models.ChatSessions;
using PatientCare.Core.Mapping;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using System.Collections.Generic;
using PatientCare.Data.EntityFramework;
using System.Data.Entity;
using System.Linq;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.FileStorage;
using System.Runtime.Serialization;
using System.Configuration;
using PatientCare.Data.EntityFramework.Identity;
using Swashbuckle.Swagger.Annotations;
using PatientCare.Web.Api.Models;

namespace PatientCare.Web.ApiMobile.Controllers
{
    /// <summary>
    /// This Controller is used to handle Chat operations.
    /// </summary>
    //[Authorize(Roles = ApplicationUser.PatientRole)]
    public sealed class ChatController : ApiBaseController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly static string chatUrl = ConfigurationManager.AppSettings["urlChat"];

        /// <summary>
        /// Description : Chat IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : Chat IChatService.
        /// </summary>
        private readonly IChatService _chatService;
        /// <summary>
        /// Description : Chat ChatAuthHelper.
        /// </summary>
        private ChatAuthHelper chatHelper;
        /// <summary>
        /// Description : Chat IResourceRepository.
        /// </summary>
        private readonly IResourceRepository _resourceRepository;
        /// <summary>
        /// Description : Chat IPatientRepository.
        /// </summary>
        private readonly IPatientRepository _patientRepository;

        /// <summary>
        /// Description : Initializes a new instance of the Chat Class.     
        /// </summary>
        /// <param name="chatService">IChat Service</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        /// <param name="resourceRepository">IResource Repository</param>
        /// <param name="patientRepository">IPatient Repository</param>
        public ChatController(
              IChatService chatService,
              IDataContextScopeFactory dataContextScopeFactory,
              IResourceRepository resourceRepository,
              IPatientRepository patientRepository
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            chatHelper = new ChatAuthHelper();
            _chatService = chatService;
            _resourceRepository = resourceRepository;
            _patientRepository = patientRepository;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/chat/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Generate Chat Token. 
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="deviceType">Device Type</param>
        /// <returns>Result</returns>
        [HttpGet]
        [Route("api/mobile/v1/auth/twilio-token")]
        public async Task<IHttpActionResult> ChatTokenGenerate([FromUri] string userId, [FromUri] string deviceType)
        {
            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri("http://52.89.81.249/mic-chat/");
                client.BaseAddress = new Uri(chatUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                object result = string.Empty;
                HttpResponseMessage response = await client.GetAsync("api/mobile/v1/auth/twilio-token?userId=" + userId + "&deviceType=" + deviceType + "");
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<object>();
                }
                return Ok(result);
            }
        }

        //[HttpGet]
        //[Route("api/mobile/v1/auth/virgil-card")]
        //public async Task<IHttpActionResult> VirgilCardGenerate([FromUri] string userId)
        //{
        //    using (var client = new HttpClient())
        //    {
        //        //client.BaseAddress = new Uri("http://52.89.81.249/mic-chat/");
        //        client.BaseAddress = new Uri(chatUrl);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        object result = string.Empty;
        //        HttpResponseMessage response = await client.GetAsync("api/mobile/v1/auth/virgil-card?userId=" + userId + "");
        //        if (response.IsSuccessStatusCode)
        //        {
        //            result = await response.Content.ReadAsAsync<object>();
        //        }
        //        return Ok(result);
        //    }
        //}

        /// <summary>
        /// Method Description : Using for Get User Virgil Card Detail. 
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>Result</returns>
        [HttpGet]
        [Route("api/mobile/v1/auth/user-virgil-card")]
        public async Task<IHttpActionResult> userVirgilCard([FromUri] string userId)
        {
            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri("http://52.89.81.249/mic-chat/");
                client.BaseAddress = new Uri(chatUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                object result = string.Empty;
                HttpResponseMessage response = await client.GetAsync("api/mobile/v1/auth/user-virgil-card?userId=" + userId + "");
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<object>();
                }
                return Ok(result);
            }
        }

        //[HttpGet]
        //[Route("api/mobile/v1/auth/virgil-token")]
        //public IHttpActionResult getVirgilAccesToken()
        //{
        //    return null;
        //}

        /// <summary>
        /// Method Description : Using for Create / Update Chat Session Data. 
        /// </summary>
        /// <param name="chatSession">Chat Session Request Model</param>
        /// <returns>Chat Session Response Model</returns>
        [HttpPost]
        [Route("api/mobile/v1/auth/chat-session")]
        public async Task<IHttpActionResult> Chat([FromBody] ChatSessionRequest chatSession)
        {
            bool isUpdate = chatSession.Id > 0 ? true : false;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var chat = Mapper.Map<ChatSessionRequest, ChatSession>(chatSession);
            await _chatService.InsertUpdateChatAsync(chat);

            var chatsessions = await _chatService.ReadChatAsync(chat.Id);

            ChatSessionResponse chatResponse = new ChatSessionResponse();
            chatResponse.ChannelName = chatsessions.ChannelName;
            chatResponse.ChannelsId = chatsessions.ChannelsId;
            chatResponse.ChannelUniqueName = chatsessions.ChannelUniqueName;
            chatResponse.Created = chatsessions.Created;
            chatResponse.CreatedByUserId = chatsessions.CreatedByUserId;
            chatResponse.FromUserId = chatsessions.FromUserId;
            chatResponse.Id = chatsessions.Id;
            chatResponse.LastMessage = chatsessions.LastMessage;
            chatResponse.Modified = chatsessions.Modified;
            chatResponse.ToUserId = chatsessions.ToUserId;
            chatResponse.Type = chatsessions.Type;

            List<ChatUserDetailResponse> userDetail = new List<ChatUserDetailResponse>();
            foreach (var chatUser in chatsessions.ChatMembers)
            {
                string usersId = chatUser.UserId.ToString();
                var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == usersId);
                if (user != null)
                {
                    var resourceId = (user.ResourceId != null) ? user.ResourceId : null;
                    var patientId = (user.PatientId != null) ? user.PatientId : null;
                    if (resourceId != null && patientId == null)
                    {
                        userPrivateKey userKeys = new userPrivateKey();
                        using (var dbContextScope = _dataContextScopeFactory.Create())
                        {
                            var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                            string query = "EXEC sp_user_private_key @user_id = '" + usersId + "'";
                            userKeys = ctx.Database.SqlQuery<userPrivateKey>(query).FirstOrDefault();
                        }

                        ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                        var item = _resourceRepository.Read(resourceId.Value);
                        charUserList.FirstName = item.FirstName;
                        charUserList.Id = item.Id;
                        charUserList.LastName = item.LastName;
                        charUserList.MiddleName = item.MiddleName;
                        charUserList.PhotoFileName = item.PhotoFileName;
                        //charUserList.Role = "Therapist";
                        //charUserList.UserId = usersId;
                        //charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                        //userDetail.Add(charUserList);
                        if (userKeys != null)
                        {
                            charUserList.Role = (userKeys.Role != null) ? userKeys.Role : " ";
                            charUserList.PrivateKey = (userKeys.PrivateKey != null) ? userKeys.PrivateKey : string.Empty;
                        }
                        charUserList.UserId = usersId;
                        charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                        userDetail.Add(charUserList);
                    }
                    else if (resourceId == null && patientId != null)
                    {
                        userPrivateKey userKeys = new userPrivateKey();
                        using (var dbContextScope = _dataContextScopeFactory.Create())
                        {
                            var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                            string query = "EXEC sp_user_private_key @user_id = '" + usersId + "'";
                            userKeys = ctx.Database.SqlQuery<userPrivateKey>(query).FirstOrDefault();
                        }

                        ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                        var item = _patientRepository.Read(patientId.Value);
                        charUserList.FirstName = item.FirstName;
                        charUserList.Id = item.Id;
                        charUserList.LastName = item.LastName;
                        charUserList.MiddleName = item.MiddleName;
                        charUserList.PhotoFileName = item.PhotoFileName;
                        //charUserList.Role = "Patient";
                        //charUserList.UserId = usersId;
                        //charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                        //userDetail.Add(charUserList);
                        if (userKeys != null)
                        {
                            charUserList.Role = (userKeys.Role != null) ? userKeys.Role : " ";
                            charUserList.PrivateKey = (userKeys.PrivateKey != null) ? userKeys.PrivateKey : string.Empty;
                        }
                        charUserList.UserId = usersId;
                        charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                        userDetail.Add(charUserList);
                    }
                }
                chatResponse.UserDetails = userDetail.ToArray();
            }
            return Ok(chatResponse);
            //return Ok(Mapper.Map<ChatSession, ChatSessionResponse>(chatsessions));
        }

        /// <summary>
        /// Method Description : Using for Get Chat Session Data by From Usre Id and To User Id.  
        /// </summary>
        /// <param name="fromUserId">From User Id</param>
        /// <param name="toUserId">To User Id</param>
        /// <returns>Chat Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/auth/user-chat-session")]
        public async Task<IHttpActionResult> ChatDetail([FromUri] string fromUserId, [FromUri] string toUserId)
        {
            List<ChatSessionResponse> chatsession = new List<ChatSessionResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                //string query = "EXEC sp_ChatSession @from_user_id = '" + request.FromUserId + "',@to_user_id = '" + request.ToUserId +  "'";
                string query = "EXEC sp_ChatSession @from_user_id = '" + fromUserId + "',@to_user_id = '" + toUserId + "'";
                chatsession = await ctx.Database.SqlQuery<ChatSessionResponse>(query).ToListAsync();
                //return Ok(chatsession.ToArray());
            }

            List<ChatUserDetailResponse> userDetail = new List<ChatUserDetailResponse>();


            List<string> userId = new List<string>();
            userId.Add(fromUserId);
            userId.Add(toUserId);

            for (int i = 0; i < userId.Count; i++)
            {
                string usersId = userId[i].ToString();
                var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == usersId);
                var resourceId = user.ResourceId;
                var patientId = user.PatientId;
                if (resourceId != null && patientId == null)
                {
                    ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                    var item = _resourceRepository.Read(resourceId.Value);
                    charUserList.FirstName = item.FirstName;
                    charUserList.Id = item.Id;
                    charUserList.LastName = item.LastName;
                    charUserList.MiddleName = item.MiddleName;
                    charUserList.PhotoFileName = item.PhotoFileName;
                    charUserList.Role = "Therapist";
                    charUserList.UserId = usersId;
                    charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    userDetail.Add(charUserList);
                }
                else if (resourceId == null && patientId != null)
                {
                    ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                    var item = _patientRepository.Read(patientId.Value);
                    charUserList.FirstName = item.FirstName;
                    charUserList.Id = item.Id;
                    charUserList.LastName = item.LastName;
                    charUserList.MiddleName = item.MiddleName;
                    charUserList.PhotoFileName = item.PhotoFileName;
                    charUserList.Role = "Patient";
                    charUserList.UserId = usersId;
                    charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    userDetail.Add(charUserList);
                }
            }

            ChatResponse chatResponse = new ChatResponse();
            chatResponse.ChatDetails = chatsession.ToArray();
            chatResponse.UserDetails = userDetail.ToArray();

            return Ok(chatResponse);
        }

        /// <summary>
        /// Method Description : Using for Get Chat Session Data by From Usre Id and To User Id. 
        /// </summary>
        /// <param name="fromUserId">From User Id</param>
        /// <param name="toUserId">To User Id</param>
        /// <returns>Chat Session Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/auth/chat-session-member")]
        public async Task<IHttpActionResult> userChatDetail([FromUri] string fromUserId, [FromUri] string toUserId)
        {
            List<string> userId = new List<string>();
            userId.Add(fromUserId);
            userId.Add(toUserId);

            ChatSessionResponse chatResponse = new ChatSessionResponse();

            List<ChatSessionResponse> chatsession = new List<ChatSessionResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                //string query = "EXEC sp_ChatSession @from_user_id = '" + request.FromUserId + "',@to_user_id = '" + request.ToUserId +  "'";
                string query = "EXEC sp_ChatSession @from_user_id = '" + fromUserId + "',@to_user_id = '" + toUserId + "'";
                chatsession = await ctx.Database.SqlQuery<ChatSessionResponse>(query).ToListAsync();
                //return Ok(chatsession.ToArray());
            }

            foreach (var chatDetails in chatsession)
            {
                chatResponse.ChannelName = chatDetails.ChannelName;
                chatResponse.ChannelsId = chatDetails.ChannelsId;
                chatResponse.ChannelUniqueName = chatDetails.ChannelUniqueName;
                chatResponse.Created = chatDetails.Created;
                chatResponse.CreatedByUserId = chatDetails.CreatedByUserId;
                chatResponse.FromUserId = chatDetails.FromUserId;
                chatResponse.Id = chatDetails.Id;
                chatResponse.LastMessage = chatDetails.LastMessage;
                chatResponse.Modified = chatDetails.Modified;
                chatResponse.ToUserId = chatDetails.ToUserId;
                chatResponse.Type = chatDetails.Type;

                List<ChatUserDetailResponse> userDetail = new List<ChatUserDetailResponse>();
                for (int i = 0; i < userId.Count; i++)
                {
                    string usersId = userId[i].ToString();
                    var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == usersId);
                    var resourceId = user.ResourceId;
                    var patientId = user.PatientId;
                    if (resourceId != null && patientId == null)
                    {
                        ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                        var item = _resourceRepository.Read(resourceId.Value);
                        charUserList.FirstName = item.FirstName;
                        charUserList.Id = item.Id;
                        charUserList.LastName = item.LastName;
                        charUserList.MiddleName = item.MiddleName;
                        charUserList.PhotoFileName = item.PhotoFileName;
                        charUserList.Role = "Therapist";
                        charUserList.UserId = usersId;
                        charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                        userDetail.Add(charUserList);
                    }
                    else if (resourceId == null && patientId != null)
                    {
                        ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                        var item = _patientRepository.Read(patientId.Value);
                        charUserList.FirstName = item.FirstName;
                        charUserList.Id = item.Id;
                        charUserList.LastName = item.LastName;
                        charUserList.MiddleName = item.MiddleName;
                        charUserList.PhotoFileName = item.PhotoFileName;
                        charUserList.Role = "Patient";
                        charUserList.UserId = usersId;
                        charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                        userDetail.Add(charUserList);
                    }
                }

                chatResponse.UserDetails = userDetail.ToArray();
            }

            return Ok(chatResponse);
        }

        /// <summary>
        /// Method Description : Using for Get Chat Session Data by Channels Id. 
        /// </summary>
        /// <param name="ChannelsId">Channel Id</param>
        /// <returns>Chat Session Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/auth/chat-session-detail")]
        public async Task<IHttpActionResult> chatSessionDetail([FromUri] string ChannelsId)
        {
            ChatSessionResponse chatResponse = new ChatSessionResponse();

            List<ChatSessionDetailsResponse> chatsession = new List<ChatSessionDetailsResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_Chat_Session_Details @channel_id = '" + ChannelsId + "'";
                chatsession = await ctx.Database.SqlQuery<ChatSessionDetailsResponse>(query).ToListAsync();
            }

            //chatResponse.ChannelName = chatsession[0].ChannelName;
            //chatResponse.ChannelsId = chatsession[0].ChannelsId;
            //chatResponse.ChannelUniqueName = chatsession[0].ChannelUniqueName;
            //chatResponse.Created = chatsession[0].Created;
            //chatResponse.CreatedByUserId = chatsession[0].CreatedByUserId;
            //chatResponse.FromUserId = chatsession[0].FromUserId;
            //chatResponse.Id = chatsession[0].Id;
            //chatResponse.LastMessage = chatsession[0].LastMessage;
            //chatResponse.Modified = chatsession[0].Modified;
            //chatResponse.ToUserId = chatsession[0].ToUserId;
            //chatResponse.Type = chatsession[0].Type;

            List<ChatUserDetailResponse> userDetail = new List<ChatUserDetailResponse>();
            foreach (var chatDetails in chatsession)
            {
                chatResponse.ChannelName = chatDetails.ChannelName;
                chatResponse.ChannelsId = chatDetails.ChannelsId;
                chatResponse.ChannelUniqueName = chatDetails.ChannelUniqueName;
                chatResponse.Created = chatDetails.Created;
                chatResponse.CreatedByUserId = chatDetails.CreatedByUserId;
                chatResponse.FromUserId = chatDetails.FromUserId;
                chatResponse.Id = chatDetails.Id;
                chatResponse.LastMessage = chatDetails.LastMessage;
                chatResponse.Modified = chatDetails.Modified;
                chatResponse.ToUserId = chatDetails.ToUserId;
                chatResponse.Type = chatDetails.Type;

                string usersId = chatDetails.UserId;
                var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == usersId);
                var resourceId = user.ResourceId;
                var patientId = user.PatientId;
                if (resourceId != null && patientId == null)
                {
                    userPrivateKey userKeys = new userPrivateKey();
                    using (var dbContextScope = _dataContextScopeFactory.Create())
                    {
                        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                        string query = "EXEC sp_user_private_key @user_id = '" + usersId + "'";
                        userKeys = ctx.Database.SqlQuery<userPrivateKey>(query).FirstOrDefault();
                    }
                    ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                    var item = _resourceRepository.Read(resourceId.Value);
                    charUserList.FirstName = (item.FirstName != null) ? item.FirstName : string.Empty;
                    charUserList.Id = item.Id;
                    charUserList.LastName = (item.LastName != null) ? item.LastName : string.Empty;
                    charUserList.MiddleName = (item.MiddleName != null) ? item.MiddleName : string.Empty;
                    charUserList.PhotoFileName = (item.PhotoFileName != null) ? item.PhotoFileName : string.Empty;
                    //charUserList.Role = "Therapist";
                    if (userKeys != null)
                    {
                        charUserList.Role = (userKeys.Role != null) ? userKeys.Role : " ";
                        charUserList.PrivateKey = (userKeys.PrivateKey != null) ? userKeys.PrivateKey : string.Empty;
                    }
                    charUserList.UserId = usersId;
                    charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    userDetail.Add(charUserList);
                }
                else if (resourceId == null && patientId != null)
                {
                    userPrivateKey userKeys = new userPrivateKey();
                    using (var dbContextScope = _dataContextScopeFactory.Create())
                    {
                        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                        string query = "EXEC sp_user_private_key @user_id = '" + usersId + "'";
                        userKeys = ctx.Database.SqlQuery<userPrivateKey>(query).FirstOrDefault();
                    }

                    ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                    var item = _patientRepository.Read(patientId.Value);
                    charUserList.FirstName = (item.FirstName != null) ? item.FirstName : string.Empty;
                    charUserList.Id = item.Id;
                    charUserList.LastName = (item.LastName != null) ? item.LastName : string.Empty;
                    charUserList.MiddleName = (item.MiddleName != null) ? item.MiddleName : string.Empty;
                    charUserList.PhotoFileName = (item.PhotoFileName != null) ? item.PhotoFileName : string.Empty;
                    //charUserList.Role = "Patient";
                    if (userKeys != null)
                    {
                        charUserList.Role = (userKeys.Role != null) ? userKeys.Role : string.Empty;
                        charUserList.PrivateKey = (userKeys.PrivateKey != null) ? userKeys.PrivateKey : string.Empty;
                    }
                    charUserList.UserId = usersId;
                    charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    userDetail.Add(charUserList);
                }
                chatResponse.UserDetails = userDetail.ToArray();
            }

            return Ok(chatResponse);
        }

        /// <summary>
        /// Method Description : Using for Get Chat Session Data by User Id. 
        /// </summary>
        /// <param name="UserId">User Id</param>
        /// <returns>Response Entity</returns>
        [HttpGet]
        [Route("api/mobile/v1/auth/chat-session-list")]
        public async Task<IHttpActionResult> chatSessionDetailList([FromUri] string UserId)
        {
            Reponse response = new Reponse();

            ChatSessionResponse chatResponse = new ChatSessionResponse();

            List<ChatSessionResponse> chatsession = new List<ChatSessionResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_Chat_Session_List_UserId @user_id = '" + UserId + "'";
                chatsession = await ctx.Database.SqlQuery<ChatSessionResponse>(query).ToListAsync();
            }

            List<ChatUserDetailResponse> userDetail = new List<ChatUserDetailResponse>();
            foreach (var chatDetails in chatsession.ToList())
            {
                string channel_id = chatDetails.ChannelsId;
                List<userList> userlist = new List<userList>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    string query = "EXEC sp_Chat_User_List_ChannelId  @user_id = '" + UserId + "', @channel_id = '" + channel_id + "'";
                    userlist = await ctx.Database.SqlQuery<userList>(query).ToListAsync();
                }

                if (userlist.Count == 2)
                {
                    foreach (var usersList in userlist.ToList())
                    {
                        string usersId = usersList.UserId;
                        var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == usersId);
                        if (user != null)
                        {
                            var resourceId = user.ResourceId;
                            var patientId = user.PatientId;
                            if (resourceId != null && patientId == null)
                            {
                                userPrivateKey userKeys = new userPrivateKey();
                                using (var dbContextScope = _dataContextScopeFactory.Create())
                                {
                                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                                    string query = "EXEC sp_user_private_key @user_id = '" + usersId + "'";
                                    userKeys = ctx.Database.SqlQuery<userPrivateKey>(query).FirstOrDefault();
                                }

                                ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                                var item = _resourceRepository.Read(resourceId.Value);
                                charUserList.FirstName = item.FirstName;
                                charUserList.Id = item.Id;
                                charUserList.LastName = item.LastName;
                                charUserList.MiddleName = item.MiddleName;
                                charUserList.PhotoFileName = item.PhotoFileName;
                                //charUserList.Role = "Therapist";
                                if (userKeys != null)
                                {
                                    charUserList.Role = (userKeys.Role != null) ? userKeys.Role : " ";
                                    charUserList.PrivateKey = (userKeys.PrivateKey != null) ? userKeys.PrivateKey : string.Empty;
                                }
                                charUserList.UserId = usersId;
                                charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                                userDetail.Add(charUserList);
                            }
                            else if (resourceId == null && patientId != null)
                            {
                                userPrivateKey userKeys = new userPrivateKey();
                                using (var dbContextScope = _dataContextScopeFactory.Create())
                                {
                                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                                    string query = "EXEC sp_user_private_key @user_id = '" + usersId + "'";
                                    userKeys = ctx.Database.SqlQuery<userPrivateKey>(query).FirstOrDefault();
                                }

                                ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                                var item = _patientRepository.Read(patientId.Value);
                                charUserList.FirstName = item.FirstName;
                                charUserList.Id = item.Id;
                                charUserList.LastName = item.LastName;
                                charUserList.MiddleName = item.MiddleName;
                                charUserList.PhotoFileName = item.PhotoFileName;
                                //charUserList.Role = "Patient";
                                if (userKeys != null)
                                {
                                    charUserList.Role = (userKeys.Role != null) ? userKeys.Role : " ";
                                    charUserList.PrivateKey = (userKeys.PrivateKey != null) ? userKeys.PrivateKey : string.Empty;
                                }
                                charUserList.UserId = usersId;
                                charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                                userDetail.Add(charUserList);
                            }
                        }
                    }
                    chatDetails.UserDetails = userDetail.ToArray();
                    userDetail.Clear();
                    //response.ChatDetails = chatsession.ToArray();
                }
                else
                {
                    chatsession.Remove(chatDetails);
                }
            }
            response.ChatDetails = chatsession.ToArray();
            foreach (var r in response.ChatDetails)
            {
                var a = r.UserDetails;
                int count = a.Count();
                if(count < 2)
                {
                    chatsession.Remove(r);
                }
            }
            response.ChatDetails = chatsession.ToArray();
            return Ok(response);
        }

        /// <summary>
        /// Description : Response Class
        /// </summary>
        public class Reponse
        {
            [DataMember]
            public ChatSessionResponse[] ChatDetails { get; set; }
        }

        /// <summary>
        /// Description : User Private Key Class
        /// </summary>
        public class userPrivateKey
        {
            [DataMember]
            public long Id { get; set; }

            [DataMember]
            public string UserId { get; set; }

            [DataMember]
            public string PrivateKey { get; set; }

            [DataMember]
            public string Role { get; set; }
        }

        /// <summary>
        /// Description : User List Class
        /// </summary>
        public class userList
        {
            [DataMember]
            public string UserId { get; set; }
        }

        /// <summary>
        /// Description : Chat Chennel List Class
        /// </summary>
        public class chatChennelList
        {
            [DataMember]
            public long Id { get; set; }

            [DataMember]
            public string ChannelsId { get; set; }

        }

        /// <summary>
        /// Method Description : Using for Get Chat Detail Member Data by From Usre Id and To User Id. 
        /// </summary>
        /// <param name="fromUserId">From User Id</param>
        /// <param name="toUserId">To User Id</param>
        /// <returns>Reponse Entity</returns>
        [HttpGet]
        [Route("api/mobile/v1/auth/chat-session")]
        public async Task<IHttpActionResult> chatDetailMember([FromUri] string fromUserId, [FromUri] string toUserId)
        {
            List<string> userId = new List<string>();

            Reponse response = new Reponse();

            List<ChatSessionResponse> chatresp = new List<ChatSessionResponse>();

            //ChatSessionResponse chatResponse = new ChatSessionResponse();

            List<chatChennelList> chatsession = new List<chatChennelList>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_Chat_Member_channel @from_user_id = '" + fromUserId + "',@to_user_id = '" + toUserId + "'";
                chatsession = await ctx.Database.SqlQuery<chatChennelList>(query).ToListAsync();
            }

            foreach (var chatDetails in chatsession)
            {
                ChatSessionResponse chatResponse = new ChatSessionResponse();
                var existsInList = userId.Contains(chatDetails.ChannelsId);
                if (existsInList == false)
                {
                    userId.Add(chatDetails.ChannelsId);


                    for (int i = 0; i < userId.Count; i++)
                    {
                        List<ChatSessionDetailsResponse> chatDetailsession = new List<ChatSessionDetailsResponse>();
                        using (var dbContextScope = _dataContextScopeFactory.Create())
                        {
                            var chanelid = userId[i].ToString();
                            var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                            string query = "EXEC sp_Chat_Session_Details @channel_id = '" + chanelid + "'";
                            chatDetailsession = await ctx.Database.SqlQuery<ChatSessionDetailsResponse>(query).ToListAsync();
                        }
                        List<ChatUserDetailResponse> userDetail = new List<ChatUserDetailResponse>();
                        foreach (var chatdetail in chatDetailsession)
                        {
                            chatResponse.ChannelName = chatdetail.ChannelName;
                            chatResponse.ChannelsId = chatdetail.ChannelsId;
                            chatResponse.ChannelUniqueName = chatdetail.ChannelUniqueName;
                            chatResponse.Created = chatdetail.Created;
                            chatResponse.CreatedByUserId = chatdetail.CreatedByUserId;
                            chatResponse.FromUserId = chatdetail.FromUserId;
                            chatResponse.Id = chatdetail.Id;
                            chatResponse.LastMessage = chatdetail.LastMessage;
                            chatResponse.Modified = chatdetail.Modified;
                            chatResponse.ToUserId = chatdetail.ToUserId;
                            chatResponse.Type = chatdetail.Type;

                            string usersId = chatdetail.UserId;

                            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == usersId);
                            var resourceId = user.ResourceId;
                            var patientId = user.PatientId;
                            if (resourceId != null && patientId == null)
                            {
                                userPrivateKey userKeys = new userPrivateKey();
                                using (var dbContextScope = _dataContextScopeFactory.Create())
                                {
                                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                                    string query = "EXEC sp_user_private_key @user_id = '" + usersId + "'";
                                    userKeys = ctx.Database.SqlQuery<userPrivateKey>(query).FirstOrDefault();
                                }

                                ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                                var item = _resourceRepository.Read(resourceId.Value);
                                charUserList.FirstName = (item.FirstName != null) ? item.FirstName : string.Empty;
                                charUserList.Id = item.Id;
                                charUserList.LastName = (item.LastName != null) ? item.LastName : string.Empty;
                                charUserList.MiddleName = (item.MiddleName != null) ? item.MiddleName : string.Empty;
                                charUserList.PhotoFileName = (item.PhotoFileName != null) ? item.PhotoFileName : string.Empty;
                                //charUserList.Role = "Therapist";
                                if (userKeys != null)
                                {
                                    charUserList.Role = (userKeys.Role != null) ? userKeys.Role : " ";
                                    charUserList.PrivateKey = (userKeys.PrivateKey != null) ? userKeys.PrivateKey : string.Empty;
                                }
                                charUserList.UserId = usersId;
                                charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                                userDetail.Add(charUserList);
                            }
                            else if (resourceId == null && patientId != null)
                            {
                                userPrivateKey userKeys = new userPrivateKey();
                                using (var dbContextScope = _dataContextScopeFactory.Create())
                                {
                                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                                    string query = "EXEC sp_user_private_key @user_id = '" + usersId + "'";
                                    userKeys = ctx.Database.SqlQuery<userPrivateKey>(query).FirstOrDefault();
                                }

                                ChatUserDetailResponse charUserList = new ChatUserDetailResponse();
                                var item = _patientRepository.Read(patientId.Value);
                                charUserList.FirstName = (item.FirstName != null) ? item.FirstName : string.Empty;
                                charUserList.Id = item.Id;
                                charUserList.LastName = (item.LastName != null) ? item.LastName : string.Empty;
                                charUserList.MiddleName = (item.MiddleName != null) ? item.MiddleName : string.Empty;
                                charUserList.PhotoFileName = (item.PhotoFileName != null) ? item.PhotoFileName : string.Empty;
                                //charUserList.Role = "Patient";
                                if (userKeys != null)
                                {
                                    charUserList.Role = (userKeys.Role != null) ? userKeys.Role : string.Empty;
                                    charUserList.PrivateKey = (userKeys.PrivateKey != null) ? userKeys.PrivateKey : string.Empty;
                                }
                                charUserList.UserId = usersId;
                                charUserList.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                                userDetail.Add(charUserList);
                            }
                            chatResponse.UserDetails = userDetail.ToArray();

                        }

                    }
                    var exists = chatresp.Contains(chatResponse);
                    if (exists == false)
                    {
                        chatresp.Add(chatResponse);
                    }
                }
                //var exists = chatresp.Contains(chatResponse);
                //if (exists == false)
                //{
                //    chatresp.Add(chatResponse);
                //}
                //chatresp.Add(chatResponse);
            }
            response.ChatDetails = chatresp.ToArray();
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create/Update Chat Chat Key.  
        /// </summary>
        /// <param name="chatKey">Chat Key Request Model</param>
        /// <returns>Chat Key Response Model</returns>
        [HttpPost]
        [Route("api/mobile/v1/auth/chat-key")]
        public async Task<IHttpActionResult> ChatKey([FromBody] ChatKeyRequest chatKey)
        {
            bool isUpdate = chatKey.Id > 0 ? true : false;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var chat = Mapper.Map<ChatKeyRequest, ChatKey>(chatKey);
            await _chatService.InsertUpdateChatKeyAsync(chat);

            var chatkeys = await _chatService.ReadChatKeyAsync(chat.Id);

            //return Ok(chatkeys);

            return Ok(Mapper.Map<ChatKey, ChatKeyResponse>(chatkeys));

        }

        /// <summary>
        /// Method Description : Using for Get Chat Key.   
        /// </summary>
        /// <param name="fromUserId">From User Id</param>
        /// <param name="toUserId">To User Id</param>
        /// <returns>Chat Key Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/auth/chat-user_key")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ChatKeyResponse>))]
        public async Task<IHttpActionResult> chatKeyDetail([FromUri] string fromUserId, [FromUri] string toUserId)
        {
            try
            {
                List<ChatKeyResponse> chatkeys = new List<ChatKeyResponse>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    string query = "EXEC sp_chat_keys @from_user_id = '" + fromUserId + "',@to_user_id = '" + toUserId + "'";
                    chatkeys = await ctx.Database.SqlQuery<ChatKeyResponse>(query).ToListAsync();
                    return Ok(chatkeys);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Ok(ex.Message);
            }
        }
    }
}
