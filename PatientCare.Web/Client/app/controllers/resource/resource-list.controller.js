﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ResourceCtrl", [
        "Resource", "$uibModal", "toaster", "$stateParams", "NgTableParams", "$scope", "Modal", "$state", "CONS_BUILD_FOR",
        function (resourceService, $uibModal, toaster, $stateParams, ngTableParams, $scope, modal, $state, CONS_BUILD_FOR) {

            var self = this;
            var originalData = [];

            this._onResponceTransform = function (response) {
                var checked = this._getOption.getResourceCheckboxInactive();
                response.showInactive = checked;
            }

            this.resourceReload = resourceService.reloadCommand;
            this.get = resourceService.query;
            this.cols = resourceService.getTableOption;
            this.cols.hideBulkEdit = true;
            this.delete = resourceService.delete;

            this._changeHeaderParams = function (data) {
                if ($stateParams && $stateParams.tableName != '') {
                    data.tableName = $stateParams.tableName;
                }
                if ($stateParams && $stateParams.fieldName != '') {
                    data.fieldName = $stateParams.fieldName;
                }
                if ($stateParams && $stateParams.fieldValue != '') {
                    data.fieldValue = $stateParams.fieldValue;
                }
                if ($stateParams && $stateParams.activityId != null) {
                    data.activityTypeId = $stateParams.activityId;
                }
                if ($stateParams && $stateParams.projectId != null) {
                    data.projectId = $stateParams.projectId;
                }
            }

            this.bulkEditCallback = function (resources) {
                if (!resources || resources.length == 0) {
                    toaster.info("", "Please select at least one resource");
                    return;
                }
                self.editResourceBulkModal = $uibModal.open({
                    animation: true,
                    backdrop: "static",
                    templateUrl: "Client/app/controllers/resource/templates/resourceBulkEditV2.html",
                    controller: "ResourceBulkEditCtrl as ResourceBulkEditCtrl",
                    size: 'lg',
                    resolve: {
                        customFieldsResource: [
                            "$q", "$stateParams", "CustomField", "RoleSetup", "Resource",
                            function loadResourceInitialData($q, $stateParams, customFieldService, roleSetupService, resourceService) {
                                function loadResource() {
                                    var defer = $q.defer();
                                    if (!$stateParams.id) {
                                        defer.resolve();
                                    } else {
                                        resourceService.get({ id: $stateParams.id }).$promise.then(function (response) {
                                            defer.resolve(response);
                                        }, function (reason) {
                                            defer.resolve({
                                                errorCode: reason.status
                                            });
                                        });
                                    }
                                    return defer.promise;

                                }

                                function loadTimeZone() {

                                    var defer = $q.defer();
                                    resourceService.getTimeZones().$promise.then(function (response) {
                                        defer.resolve(response);
                                    });
                                    return defer.promise;
                                }

                                function loadCustomFields() {
                                    var defer = $q.defer();
                                    if ($stateParams.id) {
                                        defer.resolve();
                                    } else {
                                        customFieldService.customFieldValuesByResource().$promise.then(function (response) {
                                            defer.resolve(response);
                                        });
                                    }
                                    return defer.promise;
                                }

                                function loadConfigLayout() {
                                    var defer = $q.defer();
                                    roleSetupService.resourceConfigLayout().$promise.then(function (response) {
                                        defer.resolve(response);
                                    });

                                    return defer.promise;
                                }

                                return $q.all([loadCustomFields(), loadConfigLayout(), loadResource(), loadTimeZone()])
                                    .then(function (resolutions) {
                                        return resolutions;
                                    });
                            }
                        ], selectedResources: { resources: resources }
                    },
                    data: {
                        security: {
                            authenticated: true
                        }
                    }
                });
                self.editResourceBulkModal.result.then(function (parameters) {
                    console.log("modal exit")
                });
            }
        }
    ]);