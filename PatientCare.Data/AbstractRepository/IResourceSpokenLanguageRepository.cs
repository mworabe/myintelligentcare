﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceSpokenLanguageRepository : IRepository<ResourceSpokenLanguage>
    {
    }
}
