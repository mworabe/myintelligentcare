namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCOlumninHepGroupTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hep_groups", "hep_id", c => c.Long());
            CreateIndex("dbo.hep_groups", "hep_id");
            AddForeignKey("dbo.hep_groups", "hep_id", "dbo.hep_master", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.hep_groups", "hep_id", "dbo.hep_master");
            DropIndex("dbo.hep_groups", new[] { "hep_id" });
            DropColumn("dbo.hep_groups", "hep_id");
        }
    }
}
