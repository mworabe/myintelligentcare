namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CHnageColumnNameInPatientAndCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "is_deleted", c => c.Boolean());
            AddColumn("dbo.patient_cases", "is_deleted", c => c.Boolean());
            DropColumn("dbo.patients", "id_deleted");
            DropColumn("dbo.patient_cases", "id_deleted");
        }
        
        public override void Down()
        {
            AddColumn("dbo.patient_cases", "id_deleted", c => c.Boolean());
            AddColumn("dbo.patients", "id_deleted", c => c.Boolean());
            DropColumn("dbo.patient_cases", "is_deleted");
            DropColumn("dbo.patients", "is_deleted");
        }
    }
}
