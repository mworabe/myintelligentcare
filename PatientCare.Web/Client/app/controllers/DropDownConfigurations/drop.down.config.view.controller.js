﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("DropDownConfigurationViewController",
    ["$q", "$scope", "$stateParams", "$state", "$translate", "$filter", "Auth", "PermissionWorker", "DynamicDropDownConfigService", "Certificate", "Country",
     "Department", "Division", "JobTitle", "Language", "ResourceCompany", "Skill",
     "LeaveTypeService", "HolidaysServices", "entryTypeService", "CreatorSearchFunction", "ClinicService", "LocationService", 'AppointmentTypeService',
     "DiagnosisService", "InjuryRegionService", "ExercisePositionService", "ExerciseActivitieService",
     DropDownConfigView]);

function DropDownConfigView($q, $scope, $stateParams, $state, $translate, $filter, auth, permissionWorkerService, dynamicDropDownConfigService,
        certificateService, countryService, departmentService, divisionService, jobTitleService, languageService, resourceCompanyService, skillService,
        leaveTypeService, holidaysServices, entryTypeService, creatorSearchFunction, clinicService, locationService, appointmentTypeService,
        diagnosisService, injuryRegionService, exercisePositionService, exerciseActivitieService) {

    var self = this;
    self.isLoadingData = false;
    self.currentView = $stateParams.currentView || "general";
    self.currentTab = $stateParams.currentTab || "clinics";

    creatorSearchFunction(self, "searchTimeEntryPeriod", entryTypeService);
    creatorSearchFunction(self, "searchClinics", clinicService);

    self.mainViewList = [
        {
            id: "general",
            label: "General",
            name: "General",
            hasPermissionFlag: "GeneralConfigurationData",
            visible: true,
            tabList: [
                {
                    id: "clinics",
                    container: 'clinics',
                    name: "Clinics",
                    service: {
                        get: clinicService.query,
                        save: clinicService.save,
                        update: clinicService.update,
                        delete: clinicService.delete,
                        requestTransformCallback: function (row) {
                            row.dropDownType = "clinics";
                            row.displayName = (row.displayName) ? row.displayName : row.name;
                            row.value = row.name;
                        }
                    },
                    tableConfigurations: getClinicConfigurations("clinics", "Clinics", "GeneralConfigurationData"),
                    required: true,
                    visible: true,
                }, {
                    id: "locations",
                    container: 'locations',
                    name: "Locations",
                    service: {
                        get: locationService.query,
                        save: locationService.save,
                        update: locationService.update,
                        delete: locationService.delete,
                        requestTransformCallback: function (row) {
                            row.dropDownType = "locations";
                            row.displayName = (row.displayName) ? row.displayName : row.name;
                            row.value = row.name;
                        }
                    },
                    tableConfigurations: getLocationConfigurations("locations", "Locations", "GeneralConfigurationData"),
                    required: true,
                    visible: true,
                }, ]
        },
        {
            id: "resources",
            label: "Resource",
            name: "Resources",
            hasPermissionFlag: "ResourceConfigurationData",
            visible: true,
            tabList: [
               {
                   id: "certificates",
                   name: "Certificates",
                   container: 'certificates',
                   service: {
                       get: certificateService.query,
                       delete: certificateService.delete,
                       update: certificateService.update,
                       save: certificateService.save,
                       requestTransformCallback: function (row) {
                           row.dropDownType = "certificates";
                           row.displayName = (row.displayName) ? row.displayName : row.name;
                           row.value = row.name;
                       }
                   },
                   tableConfigurations: defaultTabConfigurations("certificates", "Certificates", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "jobTitle",
                   name: "Job Title",
                   container: 'jobTitle',
                   service: {
                       get: jobTitleService.query,
                       delete: jobTitleService.delete,
                       update: jobTitleService.update,
                       save: jobTitleService.save,
                       requestTransformCallback: function (row) {
                           row.dropDownType = "jobTitle";
                           row.displayName = (row.displayName) ? row.displayName : row.name;
                           row.value = row.name;
                       }
                   },
                   tableConfigurations: getResourceJobTitleConfigurations("jobTitle", "Job Title", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "skill",
                   name: "Skill",
                   container: 'skill',
                   service: {
                       get: skillService.query,
                       delete: skillService.delete,
                       update: skillService.update,
                       save: skillService.save,
                       requestTransformCallback: function (row) {
                           row.dropDownType = "skill";
                           row.displayName = (row.displayName) ? row.displayName : row.name;
                           row.value = row.name;
                       }
                   },
                   tableConfigurations: defaultTabConfigurations("skill", "Skill", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "languages",
                   name: "Languages",
                   container: 'languages',
                   service: {
                       get: languageService.query,
                       delete: languageService.delete,
                       update: languageService.update,
                       save: languageService.save,
                       requestTransformCallback: function (row) {
                           row.dropDownType = "languages";
                           row.displayName = (row.displayName) ? row.displayName : row.name;
                           row.value = row.name;
                       }
                   },
                   tableConfigurations: defaultTabConfigurations("languages", "Languages", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "businessUnits",
                   name: "Business Units",
                   container: 'businessUnits',
                   service: defaultTabServices("businessUnits"),
                   tableConfigurations: defaultTabConfigurations("businessUnits", "Business Units", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "resourceDesignation",
                   name: "Resource Designation",
                   container: 'resourceDesignation',
                   service: defaultTabServices("resourceDesignation"),
                   tableConfigurations: defaultTabConfigurations("resourceDesignation", "Resource Designation", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "corporateGoals",
                   name: "Corporate Goals",
                   container: 'corporateGoals',
                   service: defaultTabServices("corporateGoals"),
                   tableConfigurations: getCorporateGoalsConfigurations("corporateGoals", "Corporate Goals", "IntakeConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "practiceArea",
                   name: "Practice Area",
                   container: 'practiceArea',
                   service: defaultTabServices("practiceArea"),
                   tableConfigurations: defaultTabConfigurations("practiceArea", "Practice Area", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "resourceCompany",
                   name: "Resource Company",
                   container: 'resourceCompany',
                   service: {
                       get: resourceCompanyService.query,
                       delete: resourceCompanyService.delete,
                       update: resourceCompanyService.update,
                       save: resourceCompanyService.save,
                       requestTransformCallback: function (row) {
                           row.dropDownType = "resourceCompany";
                           row.displayName = (row.displayName) ? row.displayName : row.name;
                           row.value = row.name;
                       }
                   },
                   tableConfigurations: defaultTabConfigurations("resourceCompany", "Resource Company", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "departments",
                   name: "Departments",
                   container: 'departments',
                   service: {
                       get: departmentService.query,
                       delete: departmentService.delete,
                       update: departmentService.update,
                       save: departmentService.save,
                       requestTransformCallback: function (row) {
                           row.dropDownType = "departments";
                           row.displayName = (row.displayName) ? row.displayName : row.name;
                           row.value = row.name;
                       }
                   },
                   tableConfigurations: defaultTabConfigurations("departments", "Departments", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "division",
                   name: "Divisions",
                   container: 'division',
                   service: {
                       get: divisionService.query,
                       delete: divisionService.delete,
                       update: divisionService.update,
                       save: divisionService.save,
                       requestTransformCallback: function (row) {
                           row.dropDownType = "division";
                           row.displayName = (row.displayName) ? row.displayName : row.name;
                           row.value = row.name;
                       }
                   },
                   tableConfigurations: defaultTabConfigurations("division", "Divisions", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               }, {
                   id: "industry",
                   name: "Industry",
                   container: 'industry',
                   service: defaultTabServices("industry"),
                   tableConfigurations: getIndustryConfigurations("industry", "Industry", "ResourceConfigurationData"),
                   required: true,
                   visible: true,
               },
            ],
        },
        {
            id: "patientConfigurations",
            label: "Patient",
            name: "Patient",
            hasPermissionFlag: "PatientConfigurationData",//TODO need to implmennt in backend
            visible: true,
            tabList: [
                {
                    id: "appointmentType",
                    container: 'appointmentType',
                    name: "Appointment Type",
                    service: {
                        get: appointmentTypeService.getAll,
                        save: appointmentTypeService.save,
                        update: appointmentTypeService.update,
                        delete: appointmentTypeService.delete,
                        requestTransformCallback: function (row) {
                            row.dropDownType = "appointmentType";
                            row.displayName = (row.displayName) ? row.displayName : row.name;
                            row.value = row.name;
                        }
                    },
                    tableConfigurations: getAppointmentTypeConfig("appointmentType", "Appointment Type", "PatientConfigurationData"),
                    required: true,
                    visible: true,
                }, {
                    id: "injuryRegion",
                    container: 'injuryRegion',
                    name: "Injury Region",
                    service: {
                        get: injuryRegionService.getAll,
                        save: injuryRegionService.save,
                        update: injuryRegionService.update,
                        delete: injuryRegionService.delete,
                        requestTransformCallback: function (row) {
                            row.dropDownType = "injuryRegion";
                            row.displayName = (row.displayName) ? row.displayName : row.name;
                            row.value = row.name;
                        }
                    },
                    tableConfigurations: defaultTabConfigurations("injuryRegion", "Injury Region", "PatientConfigurationData"),
                    required: true,
                    visible: true,
                }, {
                    id: "diagnosis",
                    container: 'diagnosis',
                    name: "Diagnosis",
                    service: {
                        get: diagnosisService.getAll,
                        save: diagnosisService.save,
                        update: diagnosisService.update,
                        delete: diagnosisService.delete,
                        requestTransformCallback: function (row) {
                            row.dropDownType = "diagnosis";
                            row.displayName = (row.displayName) ? row.displayName : row.name;
                            row.value = row.name;
                        }
                    },
                    tableConfigurations: defaultTabConfigurations("diagnosis", "Diagnosis", "PatientConfigurationData"),
                    required: true,
                    visible: true,
                },
            ]
        },
        {
            id: "exerciseConfigurations",
            label: "Exercise",
            name: "Exercise",
            hasPermissionFlag: "ExerciseConfigurationData",//TODO need to implmennt in backend
            visible: true,
            tabList: [
                //{
                //    id: "exercisePosition",
                //    container: 'exercisePosition',
                //    name: "Exercise Position",
                //    service: {
                //        get: exercisePositionService.getAll,
                //        save: exercisePositionService.save,
                //        update: exercisePositionService.update,
                //        delete: exercisePositionService.delete,
                //        requestTransformCallback: function (row) {
                //            row.dropDownType = "exercisePosition";
                //            row.displayName = (row.displayName) ? row.displayName : row.name;
                //            row.value = row.name;
                //        }
                //    },
                //    tableConfigurations: defaultTabConfigurations("exercisePosition", "Exercise Position", "PatientConfigurationData"),
                //    required: true,
                //    visible: true,
                //},
                {
                    id: "exerciseActivitie",
                    container: 'exerciseActivitie',
                    name: "Exercise Activity",
                    service: {
                        get: exerciseActivitieService.getAll,
                        save: exerciseActivitieService.save,
                        update: exerciseActivitieService.update,
                        delete: exerciseActivitieService.delete,
                        requestTransformCallback: function (row) {
                            row.dropDownType = "exerciseActivitie";
                            row.displayName = (row.displayName) ? row.displayName : row.name;
                            row.value = row.name;
                        }
                    },
                    tableConfigurations: defaultTabConfigurations("exerciseActivitie", "Exercise Activity", "PatientConfigurationData"),
                    required: true,
                    visible: true,
                }, {
                    id: "exerciseRegion",
                    name: "Exercise Region",
                    container: 'exerciseRegion',
                    service: defaultTabServices("exerciseRegion"),
                    tableConfigurations: defaultTabConfigurations("exerciseRegion", "Exercise Region", "PatientConfigurationData"),
                    required: true,
                    visible: true,
                },
            ]
        },

    ];

    function defaultTabServices(dropdownType) {
        return {
            get: dynamicDropDownConfigService.query,
            save: dynamicDropDownConfigService.save,
            update: dynamicDropDownConfigService.update,
            delete: dynamicDropDownConfigService.delete,
            requestTransformCallback: function (row) {
                row.dropDownType = dropdownType;
                row.displayName = (row.displayName) ? row.displayName : row.name;
                row.value = row.name;
            }
        }
    }

    function defaultTabConfigurations(dropDownType, pageHeading, hasPermissionFlag) {
        return {
            pageName: "" + pageHeading,
            hideBulkEdit: true,
            dropdowntype: dropDownType,
            disabledAddButton: disabledAddButton(hasPermissionFlag),
            disabledDeleteButton: disabledDeleteButton(hasPermissionFlag, dropDownType),
            disabledEditButton: disabledEditButton(hasPermissionFlag),
            cols: function () {
                return [
                    {
                        field: "name",
                        title: "Name",
                        sortable: "name",
                        show: true,
                        dataType: "text"
                    },
                    {
                        field: "action",
                        title: ".",
                        class: "blank-cell",
                        dataType: "command"
                    }
                ];
            }
        };
    }

    function disabledAddButton(permissionFlag) {
        if ("ResourceConfigurationData" == permissionFlag)
            return !permissionWorkerService.canResourceConfigDataInsert();
        if ("ProjectConfigurationData" == permissionFlag)
            return !permissionWorkerService.canProjectConfigDataInsert();
        if ("FinanceConfigurationData" == permissionFlag)
            return !permissionWorkerService.canFinanceConfigDataInsert();
        //if ("IntakeConfigurationData" == permissionFlag)
        //    return !permissionWorkerService.canIntakeConfigDataInsert();
        if ("GeneralConfigurationData" == permissionFlag)
            return !permissionWorkerService.canGeneralConfigDataInsert();
        return false;
    }
    function disabledDeleteButton(permissionFlag) {
        if ("ResourceConfigurationData" == permissionFlag) {
            return function () {
                return permissionWorkerService.canResourceConfigDataDelete();
            }
        }
        if ("ProjectConfigurationData" == permissionFlag)
            return function () {
                return permissionWorkerService.canProjectConfigDataDelete();
            }
        if ("FinanceConfigurationData" == permissionFlag)
            return function () {
                return permissionWorkerService.canFinanceConfigDataDelete();
            }
        if ("IntakeConfigurationData" == permissionFlag)
            return function () {
                return permissionWorkerService.canIntakeConfigDataDelete();
            }
        if ("GeneralConfigurationData" == permissionFlag)
            return function () {
                return permissionWorkerService.canGeneralConfigDataDelete();
            }
        return false;
    }
    function disabledEditButton(permissionFlag) {
        if ("ResourceConfigurationData" == permissionFlag)
            return function () {
                return permissionWorkerService.canResourceConfigDataUpdate();
            }
        if ("ProjectConfigurationData" == permissionFlag)
            return function () {
                return permissionWorkerService.canProjectConfigDataUpdate();
            }
        if ("FinanceConfigurationData" == permissionFlag)
            return function () {
                return permissionWorkerService.canFinanceConfigDataUpdate();
            }
        if ("IntakeConfigurationData" == permissionFlag)
            return function () {
                return permissionWorkerService.canIntakeConfigDataUpdate();
            }
        if ("GeneralConfigurationData" == permissionFlag)
            return function () {
                return permissionWorkerService.canGeneralConfigDataUpdate();
            }
        return false;
    }

    function getIndustryConfigurations(dropDownType, pageHeading, hasPermissionFlag) {
        return {
            pageName: "" + pageHeading,
            hideBulkEdit: true,
            dropdowntype: dropDownType,
            disabledAddButton: disabledAddButton(hasPermissionFlag),
            disabledDeleteButton: disabledDeleteButton(hasPermissionFlag),
            disabledEditButton: disabledEditButton(hasPermissionFlag),
            cols: function () {
                return [
                    {
                        field: "name",
                        title: "Industry Name",
                        sortable: "name",
                        show: true,
                        dataType: "text"
                    }, {
                        field: "configField1",
                        title: "Industry Code",
                        sortable: "configField1",
                        show: true,
                        dataType: "text"
                    }, {
                        field: "action",
                        title: ".",
                        class: "blank-cell",
                        dataType: "command"
                    }
                ];
            }
        };
    }

    function getResourceJobTitleConfigurations(dropDownType, pageHeading, hasPermissionFlag) {
        return {
            pageName: "" + pageHeading,
            hideBulkEdit: true,
            dropdowntype: dropDownType,
            disabledAddButton: disabledAddButton(hasPermissionFlag),
            disabledDeleteButton: disabledDeleteButton(hasPermissionFlag),
            disabledEditButton: disabledEditButton(hasPermissionFlag),
            cols: function () {
                return [
                    {
                        field: "name",
                        title: "Name",
                        sortable: "name",
                        show: true,
                        dataType: "text",
                        customViewText: function (row) { return (row.name.length > 30) ? row.name.substring(0, 30) + "..." : row.name }
                    }, {
                        field: "holidayEligible",
                        title: "Holiday Eligible",
                        sortable: "holidayEligible",
                        show: true,
                        dataType: "boolean",
                        dataTypeView: "boolean",
                        customViewText: function (row) { return (row.holidayEligible) ? '<i class="fa fa-check"></i>' : (row.holidayEligible == false || row.holidayEligible == 'false') ? '<i class="fa fa-times"></i>' : "" }
                    }, {
                        field: "hoursEdit",
                        title: "40 Hours Edit",
                        sortable: "hoursEdit",
                        show: true,
                        dataType: "boolean",
                        customViewText: function (row) { return (row.hoursEdit) ? '<i class="fa fa-check"></i>' : (row.hoursEdit == false || row.hoursEdit == 'false') ? '<i class="fa fa-times"></i>' : "" }
                    }, {
                        field: "priorityOrder",
                        title: "Range",
                        sortable: "priorityOrder",
                        required: true,
                        show: true,
                        dataType: "ui-select",
                        list: ["High", "Medium", "Low"],
                        customViewText: function (row) { return row.priorityOrder; }

                    }, {
                        field: "rate",
                        title: "Rate",
                        sortable: "rate",
                        show: true,
                        dataType: "number", dataTypeView: 'currency',
                    }, {
                        field: "entryType",
                        title: "Time Entry Period",
                        required: true,
                        show: true,
                        dataType: "angucomplete-alt",
                        search: self.searchTimeEntryPeriod,
                        customViewText: function (row) { return (row.entryType) ? row.entryType.name : ""; },
                        angucompleteSelect: function (selected) {
                            var item = this.$parent.$parent.$parent.$parent.$parent.$parent.row; //this.$parent.$parent.$parent.item;
                            if (item) {
                                item.entryTypeId = (selected && selected.originalObject && selected.originalObject.id > 0) ? selected.originalObject.id : null;
                                item.entryType = (selected && selected.originalObject) ? selected.originalObject : null;
                            }
                        },
                    },
                    {
                        field: "action",
                        title: ".",
                        class: "blank-cell",
                        dataType: "command"
                    }
                ];
            }
        };
    }

    function getCorporateGoalsConfigurations(dropDownType, pageHeading, hasPermissionFlag) {
        return {
            pageName: "" + pageHeading,
            hideBulkEdit: true,
            dropdowntype: dropDownType,
            disabledAddButton: disabledAddButton(hasPermissionFlag),
            disabledDeleteButton: disabledDeleteButton(hasPermissionFlag),
            disabledEditButton: disabledEditButton(hasPermissionFlag),
            cols: function () {
                return [
                    {
                        field: "name",
                        title: "Name",
                        sortable: "name",
                        show: true,
                        dataType: "text"
                    }, {
                        field: "configField1",
                        title: "Year",
                        sortable: "configField1",
                        show: true,
                        dataType: "text"
                        //dataType: "date-time"
                    },
                    {
                        field: "action",
                        title: ".",
                        class: "blank-cell",
                        dataType: "command"
                    }
                ];
            }
        };
    }

    function getClinicConfigurations(dropDownType, pageHeading, hasPermissionFlag) {
        return {
            pageName: "" + pageHeading,
            hideBulkEdit: true,
            dropdowntype: dropDownType,
            disabledAddButton: disabledAddButton(hasPermissionFlag),
            disabledDeleteButton: disabledDeleteButton(hasPermissionFlag, dropDownType),
            disabledEditButton: disabledEditButton(hasPermissionFlag),
            cols: function () {
                return [{
                    field: "clinicPrefix",
                    title: "Prefix",
                    sortable: "clinicPrefix",
                    show: true,
                    required: true,
                    dataType: "text"
                }, {
                    field: "name",
                    title: "Clinic",
                    sortable: "name",
                    show: true,
                    required: true,
                    dataType: "text"
                }, {
                    field: "address",
                    title: "Address",
                    sortable: "address",
                    show: true,
                    dataType: "text"
                }, {
                    field: "phone",
                    title: "Phone",
                    sortable: "phone",
                    show: true,
                    dataType: "phone",
                    dataTypeView: "phone",
                }, {
                    field: "email",
                    title: "Email",
                    sortable: "email",
                    show: true,
                    dataType: "email"
                }, {
                    field: "action",
                    title: ".",
                    class: "blank-cell",
                    dataType: "command"
                }];
            }
        };
    }


    function getAppointmentTypeConfig(dropDownType, pageHeading, hasPermissionFlag) {
        return {
            pageName: "" + pageHeading,
            hideBulkEdit: true,

            disabledAddButton: disabledAddButton(hasPermissionFlag),
            disabledDeleteButton: disabledDeleteButton(hasPermissionFlag, dropDownType),
            disabledEditButton: disabledEditButton(hasPermissionFlag),
            cols: function () {
                return [{
                    field: "name",
                    title: "Name",
                    sortable: "name",
                    show: true,
                    required: true,
                    dataType: "text"
                }, {
                    field: "color",
                    title: "Color",
                    sortable: "color",
                    show: true,
                    required: true,
                    dataType: "color-ui-select",
                    list: ["#FF9900", "#FF7111", "#FF7453", "#00FFFF", "#0198E1", "#03A89E", "#FF5229", "#0AC92B", "#FF3385",
                        "#37FDFC", "#1C86EE", "#FF9999", "#22136C", "#E36FAC", "#2E0854", "#68CCFF", "#36BFCA", "#852AD0", "#9E09CD",
                        "#99FF33", "#5CDF21", "#0066CC", "#3122F6", "#000066", "#666699", "#7F7F7F", "#333300", "#080808"]
                }, {
                    field: "action",
                    title: ".",
                    class: "blank-cell",
                    dataType: "command"
                }, ];
            }
        };
    }

    function getLocationConfigurations(dropDownType, pageHeading, hasPermissionFlag) {
        return {
            pageName: "" + pageHeading,
            hideBulkEdit: true,
            dropdowntype: dropDownType,
            disabledAddButton: disabledAddButton(hasPermissionFlag),
            disabledDeleteButton: disabledDeleteButton(hasPermissionFlag, dropDownType),
            disabledEditButton: disabledEditButton(hasPermissionFlag),
            cols: function () {
                return [{
                    field: "locationPrefix",
                    title: "Prefix",
                    sortable: "locationPrefix",
                    show: true,
                    required: true,
                    dataType: "text"
                }, {
                    field: "name",
                    title: "Location",
                    sortable: "name",
                    show: true,
                    required: true,
                    dataType: "text"
                }, {
                    field: "clinicId",
                    title: "Clinic",
                    sortable: "clinicId",
                    show: true,
                    dataType: "angucomplete-alt",
                    search: self.searchClinics,
                    customViewText: function (row) { return (row.clinic) ? row.clinic.name : ""; },
                    angucompleteSelect: function (selected) {
                        var item = this.$parent.$parent.$parent.$parent.$parent.$parent.row;
                        if (item) {
                            item.clinicId = (selected && selected.originalObject && selected.originalObject.id > 0) ? selected.originalObject.id : null;
                            item.clinic = (selected && selected.originalObject) ? selected.originalObject : null;
                        }
                    },
                }, {
                    field: "address",
                    title: "Address",
                    sortable: "address",
                    show: true,
                    dataType: "text"
                }, {
                    field: "email",
                    title: "Email",
                    sortable: "email",
                    show: true,
                    dataType: "email"
                }, {
                    field: "phone",
                    title: "Phone",
                    sortable: "phone",
                    show: true,
                    dataType: "phone",
                    dataTypeView: "phone",
                }, {
                    field: "action",
                    title: ".",
                    class: "blank-cell",
                    dataType: "command"
                }, ];
            }
        };
    }

    function onViewChangeSelectTab(view) {
        if (view.tabList && view.tabList.length > 0)
            self.currentTab = view.tabList[0].id;
    }

    self.isViewActive = function (view) {
        return self.currentView == view.id;
    }
    self.setActiveView = function (view) {
        onViewChangeSelectTab(view);
        self.currentView = view.id;
        self.activeView = view;
    }

    self.activeView = (self.currentView) ? _.find(self.mainViewList, { id: self.currentView }) : self.mainViewList[0];
    self.activeTab = (self.activeView.tabList.length > 0) ? self.activeView.tabList[0] : [];

    /*  For User Permissions    */
    $scope.isAuthenticated = auth.isAuthenticated;
    $scope.isAdmin = auth.isAdmin;
    $scope.currentUser = auth.getCurrentUser();

    $scope.isVisibleMainMenu = function (viewItem) {
        /// TODO repait this in future 
        if (auth.isAdmin())
            return true;
        return (viewItem.hasPermissionFlag === undefined || permissionWorkerService.checkPermissionPageRead(viewItem.hasPermissionFlag));
    };
    self.invoiceConfigs_Click = function () {
        $state.go("invoiceConfig");
    }

    function generateTabClick(tab) {
        self[tab.id + "_Click"] = function () {
            self.currentTab = tab.id;
        };
    }
    self.setActiveTab = function setActiveTab(tab) {
        self.activeTab = tab;
    }

    function generateTableConfigurationsForTabs() {
        _.forEach(self.mainViewList, function (view) {
            _.forEach(view.tabList, function (tab) {
                var tableCols = function () { // Static, for all this fields are same.
                    return [
                        {
                            field: "name",
                            title: "Name",
                            sortable: "name",
                            show: true,
                            dataType: "text"
                        },
                        {
                            field: "action",
                            title: ".",
                            class: "blank-cell",
                            dataType: "command"
                        }
                    ];
                }
                var tableConfigurations = {};
                tableConfigurations.pageName = tab.name;
                tableConfigurations.hideBulkEdit = true;
                tableConfigurations.dropdowntype = tab.id;
                tableConfigurations.cols = tableCols;
                self[tab.id + "_TableOptions"] = tableConfigurations;
            });
        })
    }

}