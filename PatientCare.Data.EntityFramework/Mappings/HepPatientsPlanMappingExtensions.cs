﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class HepPatientsPlanMappingExtensions
    {
        public static DbModelBuilder MapHepPatientsPlan(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<HepPatientsPlan>().ToTable("hep_patient_plan").HasKey(i => i.Id);

            modelBuilder.Entity<HepPatientsPlan>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<HepPatientsPlan>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<HepPatientsPlan>()
                .Property(p => p.HepPlan)
                .HasColumnName("hep_plan")
                .IsOptional();

            modelBuilder.Entity<HepPatientsPlan>()
                .Property(p => p.HepId)
                .HasColumnName("hep_id")
                .IsOptional();

            modelBuilder.Entity<HepPatientsPlan>()
                .HasOptional<HepMaster>(p => p.Hep)
                .WithMany()
                .HasForeignKey(p => p.HepId);

            modelBuilder.Entity<HepPatientsPlan>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<HepPatientsPlan>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            return modelBuilder;
        }
    }
}
