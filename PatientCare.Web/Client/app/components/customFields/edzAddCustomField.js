"use strict";

(function edzAddCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzAddCustomField", [
            "edzAddCustomFieldFactory",
            "CustomField",
            "$stateParams",
            "$state",
            "EDZ_CUSTOM_FIELD_TYPES",
            edzAddCustomField
        ]);

    function edzAddCustomField(edzAddCustomFieldFactory, customFieldService, $stateParams, $state, EDZ_CUSTOM_FIELD_TYPES) {
        return {
            templateUrl: "Client/app/components/customFields/edzAddCustomFieldV2.html",
            restrict: "E",
            scope: true,
            link: function edzAddCustomFieldLink(scope, element, attributes, controller) {
                scope.htmlElement = element.querySelectorAll(".additionalOptionCustomFieldContainer");
            },
            controller: [
                "$scope", "toaster", function edzAddCustomFieldCtrl($scope, toaster) {
                    var modelId = $stateParams.id;
                    var vm = this;
                    vm.model = {};
                    activate();
                    vm.onChangeFieldType = changeFieldType;
                    vm.onSave_Click = onSaveClick;

                    function callback(responce) {
                        if (modelId && modelId > 0) {
                            $state.go($state.current, {}, { reload: true });
                        } else {
                            $state.go($state.current, { id: responce.id }, { reload: true });
                        }
                    }

                    function reject() {
                        vm.callSubmit = false;
                    }

                    function changeFieldType($item) {
                        var setNewElementHtml = _.flowRight(setFieldHtml, _.curry(getCustomFieldEditHtml)($scope), function () { return vm.customField; });

                        vm.customField = edzAddCustomFieldFactory($item.name);

                        setNewElementHtml();

                        function getCustomFieldEditHtml(scope, field) {
                            var scopeNew = scope.$new();
                            if (vm.loadType && vm.loadType === vm.model.type) {
                                vm.model.customFieldProperties = [];
                                _.forEach(vm.loadProperties, function (item) {
                                    vm.model.customFieldProperties.push(item);
                                });
                            } else {
                                vm.model.customFieldProperties = [];
                            }
                            scopeNew.data = vm.model.customFieldProperties;
                            scopeNew.id = parseInt(modelId || 0);
                            return field.getHtmlForEditing(scopeNew);
                        }

                        function setFieldHtml(htmlToSet) {
                            $scope.htmlElement.empty();
                            $scope.htmlElement.append(htmlToSet);
                        }
                    }

                    function onSaveClick(form) {
                        if (form.$invalid) {
                            return;
                        }

                        if (modelId && modelId > 0) {
                            customFieldService.update({ id: modelId }, vm.model, function (responce) {
                                callback(responce);
                                toaster.pop("success", "", "update succesfull");
                            }, reject);
                        } else {

                            customFieldService.save(vm.model, function (responce) {
                                callback(responce);
                                toaster.pop("success", "", "save succesfull");
                            }, reject);
                        }
                        vm.callSubmit = true;
                    }

                    function initialData() {
                        if (modelId) {
                            modelId = parseInt(modelId);
                            vm.loadPage = customFieldService.get({ id: modelId }, function (responce) {
                                vm.model = responce;
                                vm.loadType = responce.type;
                                vm.loadProperties = responce.customFieldProperties;
                                var field = _.find(EDZ_CUSTOM_FIELD_TYPES, { name: responce.type });
                                if (field)
                                    changeFieldType(field);
                            });
                        }
                    }


                    //////////////////////////////////////////////////////////////////////
                    function activate() {
                        initialData();
                        vm.customFieldType = EDZ_CUSTOM_FIELD_TYPES.TYPE_SELECT.name;
                        vm.customFieldTypeEnum = _.values(EDZ_CUSTOM_FIELD_TYPES);
                    }
                }
            ],
            controllerAs: "edzAddCustomFieldCtrl"
        };
    }
})();