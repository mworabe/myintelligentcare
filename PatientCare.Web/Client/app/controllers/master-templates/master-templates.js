"use strict";
angular.module("EDZoutstaffingPortalApp")
    .config(function($stateProvider) {
        $stateProvider
            .state("master-templates", {
                url: "/master-templates/:tab",
                templateUrl: "Client/app/controllers/master-templates/master-templates.html",
                controller: "MasterTemplatesCntrl as MasterCtrl",
                data: {
                    security: {
                        authenticated: true
                    }
                }
            }).state("master-templates-add", {
                url: "/master-templates-add",
                templateUrl: "Client/app/controllers/master-templates/master-templates-add-edit.html",
                controller: "MasterTemplatesAddEditCntrl as MasterAddEditCtrl",
                resolve: {
                    dataObject: function () { return {};}
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            }).state("master-templates-edit", {
                url: "/master-templates-edit/:id",
                templateUrl: "Client/app/controllers/master-templates/master-templates-add-edit.html",
                controller: "MasterTemplatesAddEditCntrl as MasterAddEditCtrl",
                resolve: {
                    dataObject: ['MasterTemplateService', '$stateParams', '$q', function (MasterTemplateService, $stateParams, $q) {
                        function loadTemplate() {
                            var defer = $q.defer();

                            if ($stateParams.id) {
                                MasterTemplateService.get({ id: $stateParams.id }).$promise.then(function (response) {
                                    defer.resolve(response);
                                }, function (reason) {
                                    defer.resolve({
                                        errorCode: reason.status
                                    });
                                });
                            } else {
                                defer.resolve(response);
                            }
                            
                            
                            return defer.promise;
                        }

                        return $q.all([loadTemplate()])
                            .then(function (resolutions) {
                                return resolutions;
                            });
                    }]
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });