namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedInjuryndDiagnosisTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.diagnosis",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        name = c.String(),
                        created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
 
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.injury_regions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        name = c.String(),
                        created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),

                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.injury_regions");
            DropTable("dbo.diagnosis");
        }
    }
}
