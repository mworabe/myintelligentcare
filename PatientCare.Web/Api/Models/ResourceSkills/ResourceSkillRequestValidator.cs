﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceSkills
{
    public class ResourceSkillRequestValidator : AbstractValidator<ResourceSkillRequest>
    {
        //private readonly IDataContextScopeFactory _dbContextScopeFactory;
        //private readonly IResourceRepository _resourceRepository;
        //private readonly IPicklistRepository<Skill> _skillRepository;

        public ResourceSkillRequestValidator(
            //IDataContextScopeFactory dbContextScopeFactory, IResourceRepository resourceRepository, IPicklistRepository<Skill> skillRepository
            )
        {
            //_dbContextScopeFactory = dbContextScopeFactory;
            //_resourceRepository = resourceRepository;
            //_skillRepository = skillRepository;

            //RuleFor(model => model.YearsOfExpirience)
            //    .InclusiveBetween(0, 100);

            //RuleFor(model => model.Proficiency)
            //    .InclusiveBetween(0, 100);

            //RuleFor(model => model.ResourceId)
            //    .NotEmpty()
            //    .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _resourceRepository, l))
            //    .WithMessage("The resource with provided Id doesn't exists");

            //RuleFor(model => model.SkillId)
            //    .NotEmpty()
            //    .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _skillRepository, l))
            //    .WithMessage("The skill with provided Id doesn't exists");

        }
    }
}