﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.HepFrequencies;

namespace PatientCare.Web.Services.Mapping
{
    public static class HepFrequencyMapperExtensions
    {
        public static EmitMapperMapperProvider HepFrequencyRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepFrequency, HepFrequencyResponse>
                    (new ExtendedMapConfig<HepFrequency, HepFrequencyResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepFrequency[], HepFrequencyResponse[]>());
            
            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepFrequencyRequest, HepFrequency>
                    (new ExtendedMapConfig<HepFrequencyRequest, HepFrequency>()));
            
            return mapperProvider;
        }
    }
}