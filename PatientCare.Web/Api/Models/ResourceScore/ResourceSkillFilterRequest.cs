﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientCare.Web.Api.Models.ResourceScore
{
    public class ResourceSkillFilterRequest
    {
        public long SkillId { get; set; }

        public int YearsOfExpirience { get; set; }
    }
}