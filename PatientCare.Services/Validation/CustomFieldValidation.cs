﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using PatientCare.Core.Entities;
using PatientCare.Core.Enums;
// ReSharper disable InconsistentNaming

namespace PatientCare.Services.Validation
{
    public static class CustomFieldValidation
    {
        private const string ALPHA_LENGTH = "LENGTH";
        private const string ALPHA_ALLOWED_ADDITIONAL_CHARACTERS = "ALLOWED_ADDITIONAL_CHARACTERS";
        private const string ALPHANUMERIC_LENGTH = "LENGTH";
        private const string ALPHANUMERIC_ALLOWED_ADDITIONAL_CHARACTERS = "ALLOWED_ADDITIONAL_CHARACTERS";
        private const string NUMERIC_FROM = "FROM";
        private const string NUMERIC_TO = "TO";

        private static bool CheckLength(CustomField customField, string value, string  propertyName)
        {
            var fieldLength = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == propertyName);
            if (fieldLength != null && !string.IsNullOrWhiteSpace(fieldLength.Value))
            {
                int length;
                int.TryParse(fieldLength.Value, out length);
                return length >= value.Length;
            }

            return true;
        }

        private static string EscapeAdditionalCharacter(CustomField customField,  string propertyName)
        {
            var allowedSymbol = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == propertyName);
            if (allowedSymbol == null || string.IsNullOrWhiteSpace(allowedSymbol.Value))
                return null;

            var regexEscape = Regex.Escape(allowedSymbol.Value).Replace("]", @"\]");
            return regexEscape;

        }


        private static bool CompareAlpha(CustomField customField, CustomField customField2)
        {
            var fieldLength1 = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == ALPHA_LENGTH) ?? new CustomFieldProperty();
            var fieldLength2 = customField2.CustomFieldProperties.FirstOrDefault(x => x.Name == ALPHA_LENGTH) ?? new CustomFieldProperty();
            if (fieldLength1.Value != fieldLength2.Value)
                return false;

            var allowedSymbol1 = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == ALPHA_ALLOWED_ADDITIONAL_CHARACTERS) ?? new CustomFieldProperty();
            var allowedSymbol2 = customField2.CustomFieldProperties.FirstOrDefault(x => x.Name == ALPHA_ALLOWED_ADDITIONAL_CHARACTERS) ?? new CustomFieldProperty();
            return allowedSymbol1.Value == allowedSymbol2.Value;
        }

        private static string Alpha(CustomField customField, string value)
        {
            if (!CheckLength(customField, value, ALPHA_LENGTH))
            {
                return "to  long";
            }

            var regexEscape = EscapeAdditionalCharacter(customField, ALPHA_ALLOWED_ADDITIONAL_CHARACTERS);
            if (regexEscape == null)
                return null;
            var regex = "^[a-zA-Z]*[A-Za-z " + regexEscape + "]*$";

            return Regex.IsMatch(value, regex, RegexOptions.Singleline) ? null : "incorrect format";
        }


        private static bool CompareAlphanumeric(CustomField customField, CustomField customField2)
        {
            var fieldLength1 = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == ALPHANUMERIC_LENGTH) ?? new CustomFieldProperty();
            var fieldLength2 = customField2.CustomFieldProperties.FirstOrDefault(x => x.Name == ALPHANUMERIC_LENGTH) ?? new CustomFieldProperty();
            if (fieldLength1.Value != fieldLength2.Value)
                return false;

            var allowedSymbol1 = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == ALPHANUMERIC_ALLOWED_ADDITIONAL_CHARACTERS) ?? new CustomFieldProperty();
            var allowedSymbol2 = customField2.CustomFieldProperties.FirstOrDefault(x => x.Name == ALPHANUMERIC_ALLOWED_ADDITIONAL_CHARACTERS) ?? new CustomFieldProperty();
            return allowedSymbol1.Value == allowedSymbol2.Value;
        }
        private static string Alphanumeric(CustomField customField, string value)
        {
            if (!CheckLength(customField, value, ALPHANUMERIC_LENGTH))
            {
                return "to  long";
            }

            var regexEscape = EscapeAdditionalCharacter(customField, ALPHANUMERIC_ALLOWED_ADDITIONAL_CHARACTERS);
            var regex = "^^[a-zA-Z0-9]*[A-Za-z0-9 " + regexEscape + "]*$";

            return Regex.IsMatch(value, regex, RegexOptions.Singleline) ? null : "incorrect format";
        }
        private static string Date(string value)
        {
            DateTime date;
            return DateTime.TryParse(value, out date)? null : "incorect date";
        }
        private static bool CompareNumeric(CustomField customField, CustomField customField2)
        {
            var fromProperty1 = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == NUMERIC_FROM) ?? new CustomFieldProperty();
            var toProperty1 = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == NUMERIC_TO) ?? new CustomFieldProperty();


            var fromProperty2 = customField2.CustomFieldProperties.FirstOrDefault(x => x.Name == NUMERIC_FROM) ?? new CustomFieldProperty();
            var toProperty2 = customField2.CustomFieldProperties.FirstOrDefault(x => x.Name == NUMERIC_TO) ?? new CustomFieldProperty();

            return fromProperty1.Value == fromProperty2.Value && toProperty1.Value == toProperty2.Value;
        }

        private static string ValidateTimeCustomField(string value)
        {
            const string pattern = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";
            return Regex.IsMatch(value, pattern) ? null : "incorrect format for time custom field. Please specify time with HH:MM format.";
        }

        private static string Numeric(CustomField customField, string value)
        {
            long from, to;
            var fromProperty = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == NUMERIC_FROM);
            var toProperty = customField.CustomFieldProperties.FirstOrDefault(x => x.Name == NUMERIC_TO);
            long val;
            if (!long.TryParse(value, out val))
            {
                return "incorect format";
            }

            if (fromProperty == null || string.IsNullOrWhiteSpace(fromProperty.Value))
                from = long.MinValue;
            else
            {
                if (!long.TryParse(fromProperty.Value, out from))
                {
                    from = long.MinValue;
                }

            }
            if (toProperty == null || string.IsNullOrWhiteSpace(toProperty.Value))
                to = long.MaxValue;
            else
            {
                if (!long.TryParse(toProperty.Value, out to))
                {
                    to = long.MinValue;
                }
            }
            if (!(val >= from))
            {
                return "min value";
            }
            return !(val <= to) ? "max value" : null;
        }

        private static bool IgnoreRequired(CustomFieldTypeEnum type)
        {
            return type == CustomFieldTypeEnum.Checkbox;
        }

        public static string Validation(CustomField customField, string value)
        {
            /***********Sergio please have a look or Den, please have a look*********/
            value = value ?? string.Empty;

            if (customField.Require && string.IsNullOrWhiteSpace(value) && !IgnoreRequired(customField.Type)) {
                return "Field required";
            }

            if (string.IsNullOrWhiteSpace(value) && !customField.Require) {
                return null;
            }

            switch (customField.Type)
            {
                case CustomFieldTypeEnum.Alpha:
                    return Alpha(customField, value);
                case CustomFieldTypeEnum.Alphanumeric:
                    return Alphanumeric(customField, value);
                case CustomFieldTypeEnum.Bigtext:
                    return null;
                case CustomFieldTypeEnum.Select:
                    return null;
                case CustomFieldTypeEnum.Checkbox:
                    return null;
                case CustomFieldTypeEnum.File:
                    return null;
                case CustomFieldTypeEnum.Date:
                    return Date(value);
                case CustomFieldTypeEnum.Numeric:
                    return Numeric(customField, value);
                case CustomFieldTypeEnum.Time:
                    return ValidateTimeCustomField(value);
                    case CustomFieldTypeEnum.WeekTime:
                    return null;
                default:
                    return "undefined filed type";
            }

        }

        public static bool CompareValidationField(CustomField customField, CustomField customField2)
        {
            if (customField.Require != customField2.Require)
                return false;
            switch (customField.Type)
            {
                case CustomFieldTypeEnum.Alpha:
                    return CompareAlpha(customField, customField2);
                case CustomFieldTypeEnum.Alphanumeric:
                    return CompareAlphanumeric(customField, customField2);
                case CustomFieldTypeEnum.Bigtext:
                    return true;
                case CustomFieldTypeEnum.Select:
                    return true;
                case CustomFieldTypeEnum.Checkbox:
                    return true;
                case CustomFieldTypeEnum.File:
                    return true;
                case CustomFieldTypeEnum.Date:
                    return true;
                case CustomFieldTypeEnum.Time:
                    return true;
                case CustomFieldTypeEnum.Numeric:
                    return CompareNumeric(customField, customField2);
                default:
                    return false;
            }

        }
    }
}