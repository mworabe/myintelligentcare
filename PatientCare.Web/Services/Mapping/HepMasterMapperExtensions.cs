﻿using System;
using EmitMapper;
using PatientCare.Services.Mapping;
using System.Linq;
using PatientCare.Core.Entities;
using EmitMapper.MappingConfiguration;
using PatientCare.Web.Api.Models.HepMasters;
using PatientCare.Web.Api.Models.HepDetails;
using PatientCare.Web.Api.Models.HepGroups;

namespace PatientCare.Web.Services.Mapping
{
    public static class HepMasterMapperExtensions
    {
        public static EmitMapperMapperProvider HepMasterRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMaster, HepMasterResponse>
                    (new ExtendedMapConfig<HepMaster, HepMasterResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMaster[], HepMasterResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMaster, HepMasterListResponse>
                    (new ExtendedMapConfig<HepMaster, HepMasterListResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMaster[], HepMasterListResponse[]>());

            var hepGroupMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<HepGroupRequest, HepGroup>
                    (new DefaultMapConfig());

            var hepDetailMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<HepDetailRequest, HepDetail>
                    (new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMasterWithGroupRequest, HepMaster>(
                    new ExtendedMapConfig<HepMasterWithGroupRequest, HepMaster>()
                    .ForMember(x => x.HepDetails, y => y.HepDetails != null ? y.HepDetails.Select(i => hepDetailMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.HepGroups, y => y.HepGroups != null ? y.HepGroups.Select(i => hepGroupMapper.Map(i)).ToList() : null)
                ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepMasterRequest, HepMaster>
                    (new ExtendedMapConfig<HepMasterRequest, HepMaster>()
                        .ForMember(x => x.HepDetails, y => y.HepDetails != null ? y.HepDetails.Select(i => hepDetailMapper.Map(i)).ToList() : null)
                    ));

            return mapperProvider;
        }
    }
}