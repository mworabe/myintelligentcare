﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('hepList', function ($compile) {
    return {
        scope: {
            config: "=config",
            //loadingBarFlag: "=isLoading",
            patientId: "=",
            caseId: "="
        },
        templateUrl: "Client/app/controllers/patient-cases/hep-section/hep-list.html",
        controller: ['$scope',
                    '$rootScope',
                    'HepService',
                    'toaster',
                    'HepGroupFactory',
                    '$timeout',
                    'ConstantFacory',
                    'Modal',
                    '$filter',
        function ($scope, $rootScope, HepService, toaster, HepGroupFactory, $timeout, ConstantFacory, modal, $filter) {

            $scope.dataModel = {};
            $scope.dataItemList = [];
            $scope.myFilteringdata = {};
            $scope.counter = 0;
            $scope.myFirst = {};

            $scope.timeUnit = ConstantFacory.getTimeUnit();
            $scope.weightUnit = ConstantFacory.getWeightUnit();
            $scope.frequency = ConstantFacory.getFrequencies();
            $scope.resistancess = ConstantFacory.getResistances();
            $scope.editingExercises = function () {
                _.forEach($scope.dataItemList, function (item) {
                    item.sets = $scope.myFilteringdata.set;
                    item.reps = $scope.myFilteringdata.rep;
                    item.time = $scope.myFilteringdata.time;
                    item.timeUnit = $scope.myFilteringdata.timeUnit;
                    item.weight = $scope.myFilteringdata.weight;
                    item.weightUnit = $scope.myFilteringdata.weightUnits;
                    item.holds = $scope.myFilteringdata.holds;
                    item.holdsUnit = $scope.myFilteringdata.holdingsUnit;
                    item.comments = $scope.myFilteringdata.comment;
                    item.resistance = $scope.myFilteringdata.resistancesss;
                    item.frequency = $scope.myFilteringdata.frequencyy;

                    if (_.isEqual(item.frequency, '1x Day')) {
                        _.forEach(item.frequencies, function (ff) {
                            ff.exerciseFrequency = 1;
                        });
                    }
                    else if (_.isEqual(item.frequency, '2x Day')) {
                        _.forEach(item.frequencies, function (ff) {
                            ff.exerciseFrequency = 2;
                        });
                    }
                    else if (_.isEqual(item.frequency, '3x Day')) {
                        _.forEach(item.frequencies, function (ff) {
                            ff.exerciseFrequency = 3;
                        });
                    }
                    else if (_.isEqual(item.frequency, '4x Day')) {
                        _.forEach(item.frequencies, function (ff) {
                            ff.exerciseFrequency = 4;
                        });
                    }
                    else if (_.isEqual(item.frequency, '5x Day')) {
                        _.forEach(item.frequencies, function (ff) {
                            ff.exerciseFrequency = 5;
                        });
                    }
                    else if (_.isEqual(item.frequency, 'Other')) {

                        item.frequencies[0].exerciseFrequency = $scope.myFirst.sunday;
                        item.frequencies[1].exerciseFrequency = $scope.myFirst.monday;
                        item.frequencies[2].exerciseFrequency = $scope.myFirst.tuesday;
                        item.frequencies[3].exerciseFrequency = $scope.myFirst.wednesday;
                        item.frequencies[4].exerciseFrequency = $scope.myFirst.thursday;
                        item.frequencies[5].exerciseFrequency = $scope.myFirst.friday;
                        item.frequencies[6].exerciseFrequency = $scope.myFirst.saturday;
                       
                    }
                });

            }


            $scope.myFre = false;

            $scope.myChange = function (a) {
                if (_.isEqual(a, 'Other')) {
                    $scope.myFre = true;
                }
                else {
                    $scope.myFre = false;
                }
            }


            $scope.sortList=function(){
                if ($scope.dataItemList) {
                    $scope.dataItemList = $filter('orderBy')($scope.dataItemList, 'exerciseOrder');
                }
            }

            $scope.arrageItems = function () {
                if ($scope.dataItemList) {
                    for (var i = 0; i < $scope.dataItemList.length; i++) {
                        $scope.dataItemList[i].exerciseOrder = i;
                    }
                }
            }

            $scope.sortGroupItems = function (item) {
                if (item.hepDetails) {
                    item.hepDetails=$filter('orderBy')(item.hepDetails, 'exerciseOrder');
                }
            }

            $scope.arrageGroupItems = function (item) {
                if (item.hepDetails) {
                    for (var i = 0; i < item.hepDetails.length; i++) {
                        item.hepDetails[i].exerciseOrder = i;
                    }
                }
            }

            function processResponse(response) {
                $scope.dataItemList = [];
                if (response.hepDetails) {
                    angular.forEach(response.hepDetails, function (hepDetail) {
                        hepDetail.type = "Normal";
                        $scope.dataItemList.push(hepDetail);
                    });
                }
                if (response.hepGroups) {
                    angular.forEach(response.hepGroups, function (hepDetail) {
                        hepDetail.type = "Group";
                        $scope.sortGroupItems(hepDetail);
                        $scope.arrageGroupItems(hepDetail);
                        $scope.dataItemList.push(hepDetail);
                    });
                }
                $scope.dataModel = response;

                if ($scope.dataModel && $scope.dataModel.id) {
                    $rootScope.$broadcast('MIC:ON-IS-HEP-AVAILABLE', true);
                }
                $scope.sortList();
                $scope.arrageItems();
            }

            $scope.loadData = function () {
                $scope.isLoading = HepService.getCaseHep({ caseId: $scope.caseId }).$promise.then(
                                    function (response) { processResponse(response); },
                                    function (error) { });
            }
            $scope.loadData();
            $scope.onCancel = function () { }

            $scope.emailHEP = function () {
                $scope.isLoading = HepService.sendHEPMail({ caseId: $scope.caseId, patientId: $scope.patientId }, {}).$promise.then(
                                    function (response) {
                                        toaster.pop("success", "", "Email sent");
                                    },
                                    function (error) { });
            }

            function validate(isDraft) {
                if (!$scope.dataModel.startDate) {
                    toaster.pop("error", "", "Please select start date.");
                    return false;
                }
                if (!$scope.dataModel.endDate) {
                    toaster.pop("error", "", "Please select end date.");
                    return false;
                }
                if ($scope.dataModel.startDate > $scope.dataModel.endDate) {
                    toaster.pop("error", "", "Start date must be less than end date. ");
                    return false;
                }
                if ($scope.dataModel.endDate < $scope.dataModel.startDate) {
                    toaster.pop("error", "", "End date must be greater than start date. ");
                    return false;
                }
                return true;
            }

            function getRequest(isDraft) {
                $scope.dataModel.isSaveAsDraft = isDraft;
                $scope.dataModel.hepDetails = getItemsBasedOnTypes("Normal");
                $scope.dataModel.hepGroups = getItemsBasedOnTypes("Group");

                if ($scope.dataModel.hepDetails && $scope.dataModel.hepDetails.length > 0) {
                    angular.forEach($scope.dataModel.hepDetails, function (item) {
                        item.exercise = null;
                    })
                }

                if ($scope.dataModel.hepGroups && $scope.dataModel.hepGroups.length > 0) {
                    angular.forEach($scope.dataModel.hepGroups, function (groupItem) {
                        angular.forEach(groupItem.hepDetails, function (item) {
                            item.exercise = null;
                        })
                    })
                }
                return $scope.dataModel;
            }

            $scope.$on('MIC:On-case-save-Update-HEP', function (event, args) {
                if ($scope.dataModel && $scope.dataModel.id) {
                    $scope.onSave(args);
                } else {
                    toaster.pop("error", "", "Please create HEP plan.");
                }

                
            });

            $scope.$on('MIC:ON-HEP-SEND-MAIL', function (event, args) {
                if ($scope.dataModel && $scope.dataModel.id) {
                    $scope.emailHEP();
                } else {
                    toaster.pop("error", "", "Please create HEP plan.");
                }
            });

            $scope.onSave = function (isDraft) {
                if (validate(isDraft)) {
                    $scope.isLoading = HepService.saveHep(getRequest(isDraft)).$promise.then(function (result) {
                        if (result) {
                            $scope.loadData();
                            if (isDraft == true) {
                                toaster.pop("success", "", "HEP saved Successful.");
                            }
                            else
                            {
                                toaster.pop("success", "", "HEP Successfully Submitted to Patient.");
                            }
                        }
                    }, function (error) {
                    });
                }
            }
            $scope.varmy = 0;
            $scope.myAddition = {};
            $scope.onAddExdercise = function () {
                $scope.myAddition = {                 
                    tempId: 1,
                    sets: 211,
                    reps: 1,
                    time: 1,
                    timeUnit: "Sec",
                    holds: 1,
                    holdsUnit: "Sec",
                    weight: 10,
                    weightUnit: "lbs",
                    isActiveFrequency: false,
                    resistance: "Red",
                    frequency: "1x Day",
                    frequencies: null,
                    comments: " I am",
                    name: "Blank"
                };
                $scope.myAddition.type = "Normal";
                $scope.dataItemList.push($scope.myAddition);
                $scope.myAddition = {};
                $scope.myAddition.type = '';
            }

            $scope.getDayModel = function (item, id) {
                if (item.frequencies && id) {
                    for (var i = 0; i < item.frequencies.length; i++) {
                        if (item.frequencies[i].holdsUnit == id) {
                            return item.frequencies[i];
                        }
                    }
                }
                return null;
            }


            $scope.moveUp = function (index) {
                if (index > 0) {
                    var newIndex = index - 1;
                    var currentObj = $scope.dataItemList[index];
                    $scope.dataItemList[index] = $scope.dataItemList[newIndex];
                    $scope.dataItemList[newIndex] = currentObj;
                    $scope.dataItemList[index].exerciseOrder = index;
                    $scope.dataItemList[newIndex].exerciseOrder = newIndex;
                    $scope.sortList();
                }
            }

            $scope.moveDown = function (index) {
                if (index < ($scope.dataItemList.length - 1)) {
                    var newIndex = index + 1;
                    var currentObj = $scope.dataItemList[index];
                    $scope.dataItemList[index] = $scope.dataItemList[newIndex];
                    $scope.dataItemList[newIndex] = currentObj;
                    $scope.dataItemList[index].exerciseOrder = index;
                    $scope.dataItemList[newIndex].exerciseOrder = newIndex;
                    $scope.sortList();

                }
            }

            $scope.moveUp1 = function (item) {
                debugger;
                if (item.exerciseOrder >= 0) {
                    var currentIndex = item.exerciseOrder;

                    var maxIndex = -1;
                    var prevObj=null

                    for (var i = 0; i < $scope.dataItemList.length; i++) {
                        var obj = $scope.dataItemList[i];
                        if (obj.exerciseOrder < item.exerciseOrder && maxIndex < obj.exerciseOrder) {
                            prevObj = obj;
                            maxIndex=obj.exerciseOrder;
                        }
                    }
                    if (prevObj) {
                        item.exerciseOrder = prevObj.exerciseOrder
                        prevObj.exerciseOrder = currentIndex;
                    }
                }
            }
            $scope.moveDown1 = function(item) {
                
                var maxNo = -1;
                if ($scope.dataItemList) {
                    for (var i = 0; i < $scope.dataItemList.length; i++) {
                        if (maxNo <= $scope.dataItemList[i].exerciseOrder) {
                            maxNo = $scope.dataItemList[i].exerciseOrder; 
                        }
                    }
                }

                if (item.exerciseOrder < maxNo) {
                    var currentIndex = item.exerciseOrder;
                    var newIndex = -1;
                    for (var i = 0; i < $scope.dataItemList.length; i++) {
                        var obj = $scope.dataItemList[i];
                        if (obj.exerciseOrder > item.exerciseOrder) {
                            newIndex = obj.exerciseOrder;
                            obj.exerciseOrder = currentIndex;
                            break;
                        }
                    }
                    if (newIndex == -1) {
                        newIndex = maxNo;
                    }
                    item.exerciseOrder = newIndex;
                }
            }

            function findInList(item, type) {
                for (var i = 0; i < $scope.dataItemList.length > 0; i++) {
                    var obj = $scope.dataItemList[i];
                    if (item.tempId && item.type == type && item.tempId == obj.tempId) {
                        return i;
                    } else if (item.id && item.type == type && item.id == obj.id) {
                        return i;
                    }
                }
                return -1;
            }
            function getItemsBasedOnTypes(type) {
                var list = [];
                for (var i = 0; i < $scope.dataItemList.length > 0; i++) {
                    var obj = $scope.dataItemList[i];
                    if (obj.type == type) {
                        list.push(obj);
                    }
                }
                return list;
            }

            function processGroupSelection(groupItem) {
                if (groupItem.hepDetails) {
                    angular.forEach(groupItem.hepDetails, function (hepItem) {
                        var index = findInList(hepItem, "Normal");//changed
                        if (index > -1) {
                            $scope.dataItemList.splice(index, 1);
                        }
                    });
                    return;
                }
            }
            function getNewGroupSeq1() {
                var maxOrder = 0
                for (var i = 0; i < $scope.dataItemList.length > 0; i++) {
                    var obj = $scope.dataItemList[i];
                    if (maxOrder < obj.exerciseOrder) {
                        maxOrder = obj.exerciseOrder;
                    }
                }
                return (maxOrder + 1);
            }

            function getNewGroupSeq() {
                var maxOrder = 0
                if ($scope.dataItemList) {
                    maxOrder = ($scope.dataItemList.length-1);
                }
                return (maxOrder + 1);
            }

      

                //*******************************************************

            $scope.onCreateGroup = function () {

                var normalItems = angular.copy(getItemsBasedOnTypes("Normal"));
                var groupItem = { type: "Group" };
                var config = {
                    title: "Create Group",
                    groupItem: groupItem,
                    normalItems: normalItems,

                };
                HepGroupFactory.showGroupDialog(config).result.then(function (result) {
                    if (result) {
                        var grouItem = result.groupItem;
                        processGroupSelection(grouItem);
                        $scope.arrageItems();
                        grouItem.exerciseOrder = getNewGroupSeq();
                        $scope.arrageGroupItems(grouItem);
                        $scope.sortGroupItems(grouItem);

                        grouItem.type = "Group";
                        grouItem.tempId = ConstantFacory.guid();
                        $scope.dataItemList.push(grouItem);
                        $scope.sortList();
                    }
                });
            }

            $scope.editGroup = function (groupItem) {
                var normalItems = angular.copy(getItemsBasedOnTypes("Normal"));
                var config = {
                    title: "Edit Group",
                    groupItem: groupItem,
                    normalItems: normalItems,
                };

                HepGroupFactory.showGroupDialog(config).result.then(function (result) {
                    if (result) {
                        var grouItem = result.groupItem;
                        var notSelectedItems = result.notSelectedItems;
                        processGroupSelection(grouItem);
                        $scope.arrageItems();
                        var index = findInList(grouItem, "Group");

                        $scope.arrageGroupItems(grouItem);
                        $scope.sortGroupItems(grouItem);

                        if (index > -1) {
                            $scope.dataItemList[index] = grouItem;
                        } else {
                            $scope.dataItemList.push(grouItem);
                        }
                        if (notSelectedItems && notSelectedItems.length > 0) {
                            angular.forEach(notSelectedItems, function (item) {
                                var index = findInList(item, "Normal");
                                if (index == -1) {
                                    item.exerciseOrder = getNewGroupSeq();
                                    item.type = "Normal";
                                    $scope.dataItemList.push(item);
                                }
                            });
                        }
                        $timeout(function () {
                            var copy = angular.copy($scope.dataItemList);
                            $scope.dataItemList = [];
                            $scope.dataItemList = copy;
                        }, 1000);
                    }
                });
            }

            $scope.unGroupItem = function (dataObj) {
                dataObj.exerciseOrder = getNewGroupSeq();
                dataObj.type = "Normal";
                $scope.dataItemList.push(dataObj);
                $scope.sortList();
            }

            $scope.deleteNormalItem = function (item) {
                modal.confirm.delete(function () {
                    var index = findInList(item, "Normal");
                    if (index > -1) {
                        $scope.dataItemList.splice(index, 1);
                        $scope.arrageItems();
                        $scope.sortList();
                    }
                })("Warning", "Item");
            }

            $scope.deleteGroup = function (item) {
                var index = findInList(item, "Group");
                if (index > -1) {
                    $scope.dataItemList.splice(index, 1);
                    $scope.arrageItems();
                    $scope.sortList();
                }               
            }
        }]
    }
});



