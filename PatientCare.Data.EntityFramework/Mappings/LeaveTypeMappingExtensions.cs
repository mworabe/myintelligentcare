﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class LeaveTypeMappingExtensions
    {
        public static DbModelBuilder MapLeaveType(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<LeaveType>().ToTable("leave_types").HasKey(i => i.Id);

            modelBuilder.Entity<LeaveType>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<LeaveType>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();
            
            modelBuilder.Entity<LeaveType>()
               .Property(p => p.TotalLeaves)
               .HasColumnName("total_leaves")
               .IsOptional();
          
            modelBuilder.Entity<LeaveType>()
              .Property(p => p.Color)
              .HasColumnName("color")             
              .HasMaxLength(20)
              .IsOptional();

            modelBuilder.Entity<LeaveType>()
              .Property(p => p.IsPaid)
              .HasColumnName("is_paid")
              .IsOptional();

            return modelBuilder;
        }
    }
}
