namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateCHatSessionSP : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
         "dbo.sp_ChatSession",
         p => new
         {
             from_user_id = p.String(defaultValueSql: null),
             to_user_id = p.String(defaultValueSql: null)
         },
         body:
              @"select 
	           Id,channel_id as ChannelsId,channel_unique_name as ChannelUniqueName,
               channel_name as ChannelName,from_user_id as FromUserId,to_user_id as ToUserId,
	           created_by_user_id as CreatedByUserId,last_message as LastMessage ,
	           type as Type ,created as Created,modified as Modified 
               from chat_Sessions 
	           where (from_user_id = @from_user_id and to_user_id = @to_user_id)
	           or (from_user_id = @to_user_id and to_user_id = @from_user_id)"
             );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_ChatSession");
        }
    }
}
