namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeInColumnPatient : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.patients", "relationship_to_contact", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.patients", "relationship_to_contact", c => c.String());
        }
    }
}
