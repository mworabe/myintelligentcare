﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Patient Status Enum.
    /// </summary>
    public enum PatientStatusEnum
    {
        InActive = 0,
        Active = 1
        
    }
}
