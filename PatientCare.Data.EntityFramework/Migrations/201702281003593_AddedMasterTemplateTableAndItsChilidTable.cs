namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMasterTemplateTableAndItsChilidTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.master_template_exercises",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        exercise_id = c.Long(),
                        master_template_id = c.Long(),
                        created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.exercises", t => t.exercise_id)
                .ForeignKey("dbo.master_templates", t => t.master_template_id)
                .Index(t => t.exercise_id)
                .Index(t => t.master_template_id);
            
            CreateTable(
                "dbo.master_templates",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        owner_id = c.Long(),
                        is_master = c.Boolean(),
                        is_favorite = c.Boolean(),
                        is_public = c.Boolean(),
                        name = c.String(),
                        created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.owner_id)
                .Index(t => t.owner_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.master_template_exercises", "master_template_id", "dbo.master_templates");
            DropForeignKey("dbo.master_templates", "owner_id", "dbo.resources");
            DropForeignKey("dbo.master_template_exercises", "exercise_id", "dbo.exercises");
            DropIndex("dbo.master_templates", new[] { "owner_id" });
            DropIndex("dbo.master_template_exercises", new[] { "master_template_id" });
            DropIndex("dbo.master_template_exercises", new[] { "exercise_id" });
            DropTable("dbo.master_templates");
            DropTable("dbo.master_template_exercises");
        }
    }
}
