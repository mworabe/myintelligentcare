namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedClinicAndClinicLocationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.clinic_locations",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(),
                    clinic_id = c.Long(),
                    contact_person = c.String(),
                    address = c.String(),
                    email = c.String(),
                    phone = c.String(),
                    location_prefix = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.clinics", t => t.clinic_id)
                .Index(t => t.clinic_id);

            CreateTable(
                "dbo.clinics",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(),
                    contact_person = c.String(),
                    address = c.String(),
                    email = c.String(),
                    phone = c.String(),
                    clinic_prefix = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.clinic_locations", "clinic_id", "dbo.clinics");
            DropIndex("dbo.clinic_locations", new[] { "clinic_id" });
            DropTable("dbo.clinics");
            DropTable("dbo.clinic_locations");
        }
    }
}
