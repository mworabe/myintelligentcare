﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("AdvancedFilterService", ["$resource", function ($resource) {
    var res = $resource("api/ResourceScore/:controller", null, {
        'getAdvancedFilterFileds': { method: "GET", params: { controller: "AdvanceFilter"}, isArray: true },
    });

    return res;
}]);