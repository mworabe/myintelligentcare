﻿using System;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ResourceSurveyResults;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Survey Results operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceSurveyResultsController : EdzBaseApiController<ResourceSurveyResult, ResourceSurveyResultRequest, ResourceSurveyResultResponse>
    {
        /// <summary>
        /// Description : In this class we pass  Resource Certificate IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource Survey Results Repository</param>
        public ResourceSurveyResultsController(IResourceSurveyResultRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.Resource);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }

        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceSurveyResults/" + id, UriKind.Relative);
        }
    }
}