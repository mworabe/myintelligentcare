﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceCertificateRepository : RepositoryBase<ResourceCertificate>, IResourceCertificateRepository
    {
        public ResourceCertificateRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
