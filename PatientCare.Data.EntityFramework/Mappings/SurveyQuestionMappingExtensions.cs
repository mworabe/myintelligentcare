﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class SurveyQuestionMappingExtensions
    {
        public static DbModelBuilder MapSurveyQuestion(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<SurveyQuestion>().ToTable("survey_questions").HasKey(i => i.Id);

            modelBuilder.Entity<SurveyQuestion>().Property(p => p.Text)
                .HasColumnName("text")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255);

            modelBuilder.Entity<SurveyQuestion>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<SurveyQuestion>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();


            modelBuilder.Entity<SurveyQuestion>()
                .Property(p => p.CategoryId)
                .HasColumnName("category_id")
                .IsRequired();

            modelBuilder.Entity<SurveyQuestion>()
                .Property(p => p.Weight)
                .HasColumnName("weight")
                .IsRequired();

            modelBuilder.Entity<SurveyQuestion>()
                .HasRequired<SurveyCategory>(p => p.Category)
                .WithMany()
                .HasForeignKey(p => p.CategoryId);

            return modelBuilder;
        }
    }
}
