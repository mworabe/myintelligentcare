"use strict";
angular.module("EDZoutstaffingPortalApp")
    .config(function($stateProvider) {
        $stateProvider
            .state("favorite-templates", {
                url: "/favorite-templates/:tab",
                templateUrl: "Client/app/controllers/favorite-templates/favorite-templates.html",
                controller: "FavoriteTemplatesCntrl as FavoriteCtrl",
                data: {
                    security: {
                        authenticated: true
                    }
                }
            }).state("favorite-templates-add", {
                url: "/favorite-templates-add",
                templateUrl: "Client/app/controllers/favorite-templates/favorite-templates-add-edit.html",
                controller: "FavoriteTemplatesAddEditCntrl as FavoriteAddEditCtrl",
                resolve: {
                    dataObject: function () { return {};}
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            }).state("favorite-templates-edit", {
                url: "/favorite-templates-edit/:id",
                templateUrl: "Client/app/controllers/favorite-templates/favorite-templates-add-edit.html",
                controller: "FavoriteTemplatesAddEditCntrl as FavoriteAddEditCtrl",
                resolve: {
                    dataObject: ['FavoriteTemplateService', '$stateParams', '$q', function (FavoriteTemplateService, $stateParams, $q) {
                        function loadTemplate() {
                            var defer = $q.defer();

                            if ($stateParams.id) {
                                FavoriteTemplateService.get({ id: $stateParams.id }).$promise.then(function (response) {
                                    defer.resolve(response);
                                }, function (reason) {
                                    defer.resolve({
                                        errorCode: reason.status
                                    });
                                });
                            } else {
                                defer.resolve(response);
                            }
                            
                            
                            return defer.promise;
                        }

                        return $q.all([loadTemplate()])
                            .then(function (resolutions) {
                                return resolutions;
                            });
                    }]
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });