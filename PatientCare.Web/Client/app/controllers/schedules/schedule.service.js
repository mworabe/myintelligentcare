﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("ScheduleService", ["$resource", function ($resource) {
    var resource = $resource("api/scheduler/:controller/:id", null,
        {
            'query': { method: "GET", params: {}, isArray: false },
            'update': { method: "PUT", params: {}, isArray: false },
            'loadScheduleData': { method: "GET", params: {}, isArray: true, url: "api/scheduler/" },
        });

    return resource;
}]);
