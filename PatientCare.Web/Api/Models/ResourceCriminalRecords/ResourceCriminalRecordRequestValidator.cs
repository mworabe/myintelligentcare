﻿using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceCriminalRecords
{
    public class ResourceCriminalRecordRequestValidator : AbstractValidator<ResourceCriminalRecordRequest>
    {
        public ResourceCriminalRecordRequestValidator()
        {
            RuleFor(model => model.LocationOfOffense)
                .NotEmpty();

            RuleFor(model => model.Offense)
                .NotEmpty();

            RuleFor(model => model.DateOfOffense)
                .NotEmpty();

            RuleFor(model => model.PenaltyOrDisposition)
                .NotEmpty();
        }
    }
}