"use strict";

(function edzTimeCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzTimeCustomFieldOptions", [
            edzTimeCustomFieldOptions
        ]);

    function edzTimeCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/time/edzTimeCustomField.html",
            restrict: "E",
            scope: false,
            controller: ["$scope", function ($scope) {

                function activate() {
                    $scope.validation = {};
                    $scope.validation.require = $scope.require;
                    $scope.validation.regex = new RegExp("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", "i");
                }
                activate();
            }]
        };
    }
})();
