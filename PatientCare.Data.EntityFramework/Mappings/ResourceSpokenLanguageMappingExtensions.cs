﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceSpokenLanguageMappingExtensions
    {
        public static DbModelBuilder MapResourceSpokenLanguage(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceSpokenLanguage>().ToTable("resource_spoken_languages").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceSpokenLanguage>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceSpokenLanguage>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceSpokenLanguage>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsRequired();

            modelBuilder.Entity<ResourceSpokenLanguage>()
                .HasRequired<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceSpokenLanguage>()
                .Property(p => p.LanguageId)
                .HasColumnName("language_id")
                .IsRequired();

            modelBuilder.Entity<ResourceSpokenLanguage>()
                .HasRequired<Language>(p => p.Language)
                .WithMany()
                .HasForeignKey(p => p.LanguageId);

            modelBuilder.Entity<ResourceSpokenLanguage>()
                .Property(p => p.YearsOfExpirience)
                .HasColumnName("years_of_expirience")
                .IsRequired();

            modelBuilder.Entity<ResourceSpokenLanguage>()
                .Property(p => p.SpeakingProficiency)
                .HasColumnName("speaking_proficiency")
                .IsOptional();

            modelBuilder.Entity<ResourceSpokenLanguage>()
                .Property(p => p.WritingProficiency)
                .HasColumnName("writing_proficiency")
                .IsOptional();

            return modelBuilder;
        }
    }
}
