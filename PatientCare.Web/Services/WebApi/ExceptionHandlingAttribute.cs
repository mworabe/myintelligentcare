﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using PatientCare.Web.Api.Models;

namespace PatientCare.Web.Services.WebApi
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public sealed class ExceptionHandlingAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var ex = context.Exception as AggregateException;
            var modelState = new Dictionary<string, IList<string>> { { "model", new List<string>() } };
            if (ex != null)
            {
                foreach (var e in ex.InnerExceptions)
                {
                    modelState["model"].Add(e.Message);
                }
            }
            else
            {
                modelState["model"].Add(context.Exception.Message);
            }
            var errorResponse = new ErrorResponse
            {
                Message = "The request is invalid.",
                ModelState = modelState
            };
            context.Response = context.Request.CreateResponse(HttpStatusCode.BadRequest, errorResponse);
        }
    }
}