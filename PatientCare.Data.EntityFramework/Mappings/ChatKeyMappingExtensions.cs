﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ChatKeyMappingExtensions
    {
        public static DbModelBuilder MapChatKey(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ChatKey>().ToTable("chat_user_key").HasKey(i => i.Id);

            modelBuilder.Entity<ChatKey>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ChatKey>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ChatKey>()
                .Property(p => p.UserId)
                .HasColumnName("user_id")
                .IsOptional();

            modelBuilder.Entity<ChatKey>()
                .Property(p => p.PrivateKey)
                .HasColumnName("private_key")
                .IsOptional();

            return modelBuilder;
        }
    }
}
