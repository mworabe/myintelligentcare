﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("resourceRolesAVDService", ["$resource",
        function ($resource) {
            var resource = $resource("api/ResourceDemand/:id/:controller",
                null, {
                    'getRolesAVD': { method: "GET", params: { controller: "GetJobTitleAvd" }, isArray: false },
                    'getResourceAVD': { method: "GET", params: { controller: "GetResourceAvdByJobTitle" }, isArray: false },
                });
            return resource;
        }
    ]);