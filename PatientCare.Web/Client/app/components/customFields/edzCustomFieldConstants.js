﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .constant("CUSTOM_FIELD", {
        PROPERTY_NAME: {
            ALPHANUMERIC : {
                ALLOWED_ADDITIONAL_CHARACTERS: "ALLOWED_ADDITIONAL_CHARACTERS",
                LENGTH: "LENGTH",
                RESIZE_FIELD: "RESIZE_FIELD"
            },
            ALPHA: {
                ALLOWED_ADDITIONAL_CHARACTERS: "ALLOWED_ADDITIONAL_CHARACTERS",
                LENGTH: "LENGTH",
                RESIZE_FIELD: "RESIZE_FIELD"
            },
            NUMERIC : {
                FROM: "FROM",
                TO: "TO",
                NUMBER_OF_DECIMALS: "NUMBER_OF_DECIMALS"
            },
            SELECT : {
                "SELECT" : "SELECT"
            },
            GENERAL: {
                LENGTH: "LENGTH",
                NUMBER_OF_DECIMALS: "NUMBER_OF_DECIMALS"
            }
        }

    });