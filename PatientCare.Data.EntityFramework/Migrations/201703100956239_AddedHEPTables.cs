namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedHEPTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.hep_detail",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        exercise_id = c.Long(),
                        group_id = c.Long(),
                        hep_id = c.Long(),
                        sets = c.Int(nullable: false),
                        reps = c.Int(nullable: false),
                        time = c.Int(nullable: false),
                        time_unit = c.Int(nullable: false),
                        weight = c.String(nullable: false),
                        holds = c.Int(nullable: false),
                        holds_unit = c.Int(nullable: false),
                        exercise_order = c.Int(nullable: false),
                        created = c.DateTime(nullable: false),
                        modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.exercises", t => t.exercise_id)
                .ForeignKey("dbo.hep_groups", t => t.group_id)
                .ForeignKey("dbo.hep_master", t => t.hep_id)
                .Index(t => t.exercise_id)
                .Index(t => t.group_id)
                .Index(t => t.hep_id);
            
            CreateTable(
                "dbo.hep_master",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        patient_id = c.Long(),
                        owner_id = c.Long(),
                        patient_case_id = c.Long(),
                        start_date = c.DateTime(),
                        end_date = c.DateTime(),
                        created = c.DateTime(nullable: false),
                        modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.owner_id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.patient_cases", t => t.patient_case_id)
                .Index(t => t.patient_id)
                .Index(t => t.owner_id)
                .Index(t => t.patient_case_id);
            
            CreateTable(
                "dbo.hep_exercise_frequency",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        hep_id = c.Long(),
                        hep_detail_id = c.Long(),
                        holds_unit = c.Int(nullable: false),
                        exercise_frequency = c.Int(nullable: false),
                        created = c.DateTime(nullable: false),
                        modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.hep_master", t => t.hep_id)
                .ForeignKey("dbo.hep_detail", t => t.hep_detail_id)
                .ForeignKey("dbo.hep_detail", t => t.hep_id)
                .Index(t => t.hep_id)
                .Index(t => t.hep_detail_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.hep_exercise_frequency", "hep_id", "dbo.hep_detail");
            DropForeignKey("dbo.hep_exercise_frequency", "hep_detail_id", "dbo.hep_detail");
            DropForeignKey("dbo.hep_exercise_frequency", "hep_id", "dbo.hep_master");
            DropForeignKey("dbo.hep_detail", "hep_id", "dbo.hep_master");
            DropForeignKey("dbo.hep_master", "patient_case_id", "dbo.patient_cases");
            DropForeignKey("dbo.hep_master", "patient_id", "dbo.patients");
            DropForeignKey("dbo.hep_master", "owner_id", "dbo.resources");
            DropForeignKey("dbo.hep_detail", "group_id", "dbo.hep_groups");
            DropForeignKey("dbo.hep_detail", "exercise_id", "dbo.exercises");
            DropIndex("dbo.hep_exercise_frequency", new[] { "hep_detail_id" });
            DropIndex("dbo.hep_exercise_frequency", new[] { "hep_id" });
            DropIndex("dbo.hep_master", new[] { "patient_case_id" });
            DropIndex("dbo.hep_master", new[] { "owner_id" });
            DropIndex("dbo.hep_master", new[] { "patient_id" });
            DropIndex("dbo.hep_detail", new[] { "hep_id" });
            DropIndex("dbo.hep_detail", new[] { "group_id" });
            DropIndex("dbo.hep_detail", new[] { "exercise_id" });
            DropTable("dbo.hep_exercise_frequency");
            DropTable("dbo.hep_master");
            DropTable("dbo.hep_detail");
        }
    }
}
