﻿using PatientCare.Core.Entities;
using PatientCare.Core.Services.Mailing;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Services.Mailing
{
    public class EmailService : IEmailService
    {
        private readonly ISentEmailRepository _sentEmailRepository;
        public EmailService(ISentEmailRepository sentEmailRepository)
        {
            _sentEmailRepository = sentEmailRepository;
        }        
    }
}
