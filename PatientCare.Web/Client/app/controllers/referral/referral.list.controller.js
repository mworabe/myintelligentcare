﻿(function RefrralListCtrlIIFE() {
    "use strict";
    angular.module("EDZoutstaffingPortalApp")
        .controller("ReferralPhysicianListController",
            ["ReferralPhysicianService", ReferralListControllerFunc]);

    function ReferralListControllerFunc(referralPhysicianService) {
        var self = this;

        self.get = referralPhysicianService.query;
        self.save = referralPhysicianService.save;
        self.update = referralPhysicianService.update;
        self.delete = referralPhysicianService.delete;
        self.tableOptions = referralPhysicianService.getTableOption;

    }
})();
