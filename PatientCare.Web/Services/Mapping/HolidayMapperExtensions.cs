﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.Holidays;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class HolidayMapperExtensions
    {
        public static EmitMapperMapperProvider HolidayRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Holiday, HolidayResponse>
                    (new ExtendedMapConfig<Holiday, HolidayResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Holiday[], HolidayResponse[]>());

            mapperProvider.RegisterMapper(() =>
             ObjectMapperManager.DefaultInstance.GetMapper<Holiday[], HolidayPicklistResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HolidayRequest, Holiday>
                    (new ExtendedMapConfig<HolidayRequest, Holiday>()));
            
            return mapperProvider;
        }
    }
}