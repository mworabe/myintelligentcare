﻿using System;
using System.Web.Http.Results;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ResourceWorkAssignment;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Work Assignment operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceWorkAssignmentController : EdzBaseApiController<ResourceWorkAssignment, ResourceWorkAssignmentRequest, ResourceWorkAssignmentResponse>
    {
        /// <summary>
        /// Description : In this class we pass Resource Work Assignment IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource Work Assignment Repository</param>
        public ResourceWorkAssignmentController(IResourceWorkAssignmentRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.ResourceWorkExperienceId);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }


        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceWorkAssignment/" + id, UriKind.Relative);
        }
    }
}