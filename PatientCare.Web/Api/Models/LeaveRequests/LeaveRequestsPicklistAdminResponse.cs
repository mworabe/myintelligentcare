﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;
using System;

namespace PatientCare.Web.Api.Models.LeaveRequests
{
    [DataContract]
    public class LeaveRequestsPicklistAdminResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? LeaveTypeId { get; set; }

        [DataMember]
        public long? SupervisorId { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public virtual PicklistResponse LeaveType { get; set; }

        [DataMember]
        public virtual PicklistResponse Supervisor { get; set; }

        [DataMember]
        public virtual PicklistResponse Resource { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public DateTime? StatusModifiedDate { get; set; }

        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string CommentFromSupervisor { get; set; }

        [DataMember]
        public string Canceled { get; set; }
    }
}