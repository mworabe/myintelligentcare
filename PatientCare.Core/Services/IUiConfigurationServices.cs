﻿using System.Threading.Tasks;
using PatientCare.Core.Entities;
using System.Collections.Generic;
using System;

namespace PatientCare.Core.Services
{
    public interface IUiConfigurationServices
    {
        Task<bool> UpdateUiConfigurationAsync(UiConfiguration uiconfiguration);

        Task<bool> CreateUiConfigurationAsync(UiConfiguration uiconfiguration);

        Task<bool> DeleteUiConfigurationAsync(long id);
    }
}
