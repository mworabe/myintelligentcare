﻿using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Master Template.
    /// </summary>
    public class MasterTemplate : NamedEntity
    {
        public long? OwnerId { get; set; }
        public virtual Resource Owner { get; set; }

        public bool? IsMaster { get; set; }

        public bool? IsFavorite { get; set; }

        public bool? IsPublic { get; set; }

        public bool? IsDeleted { get; set; }

        public virtual ICollection<MasterTemplateExercise> TemplateExercises { get; set; }
    }
}
