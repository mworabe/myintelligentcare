﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Utilities;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : Entity
    {
        protected readonly IAmbientDataContextLocator DataContextLocator;

        protected RepositoryBase(IAmbientDataContextLocator dataContextLocator)
        {
            if (dataContextLocator == null) { throw new ArgumentNullException("dataContextLocator"); }
            DataContextLocator = dataContextLocator;
        }

        protected virtual PatientCareDbContext CurrentContext
        {
            get { return DataContextLocator.Get<PatientCareDbContext>(); }
        }

        protected virtual DbSet<T> EntitySet
        {
            get { return CurrentContext.Set<T>(); }
        }

        protected static Expression<Func<T, bool>> GetSearchByIdExpressionFilter(long id)
        {
            var entityParameter = Expression.Variable(typeof(T), "targetConcreteEntity");
            //targetConcreteEntity.ID
            var accessPropertyExpression = Expression.PropertyOrField(entityParameter, ReflectionHelper.GetMemberName<T>(t => t.Id));
            //targetConcreteEntity.Id == id
            var filterExpression = Expression.Equal(accessPropertyExpression, Expression.Constant(id));
            return Expression.Lambda<Func<T, bool>>(filterExpression, entityParameter);
        }

        protected virtual IQueryable<T> CreateReadQuery(ReadOptions<T> readOptions = null)
        {
            var queryable = EntitySet.AsQueryable();
            if (readOptions != null)
            {
                queryable = queryable.AttachIncludePredicates(readOptions.IncludePredicates);
                if (readOptions.ReadOnly)
                {
                    queryable = queryable.AsNoTracking();
                }
            }
            return queryable;
        }

        public virtual T Read(long id, ReadOptions<T> readOptions = null)
        {
            return CreateReadQuery(readOptions).FirstOrDefault(GetSearchByIdExpressionFilter(id));
        }

        public virtual Task<T> ReadAsync(long id, ReadOptions<T> readOptions = null)
        {
            return CreateReadQuery(readOptions).FirstOrDefaultAsync(GetSearchByIdExpressionFilter(id));
        }

        public virtual void Delete(T entity)
        {
            if (entity == null) { throw new ArgumentNullException("entity"); }
            EntitySet.Remove(entity);
            CurrentContext.SaveChanges();
        }

        public virtual Task DeleteAsync(T entity)
        {
            if (entity == null) { throw new ArgumentNullException("entity"); }
            EntitySet.Remove(entity);
            return CurrentContext.SaveChangesAsync();
        }

        public PagedResult<T> PagedList(PagedQuery<T> query, ReadOptions<T> readOptions = null)
        {
            var entities = CreatePagedQuery(query, readOptions).ToArray();
            var totalCount = PagedCount(query);

            return new PagedResult<T>
            {
                Entities = entities,
                TotalCount = totalCount
            };
        }

        public async Task<PagedResult<T>> PagedListAsync(PagedQuery<T> query)
        {
            var entities = await CreatePagedQuery(query).ToArrayAsync();
            var totalCount = await PagedCountAsync(query);

            return new PagedResult<T>
            {
                Entities = entities,
                TotalCount = totalCount
            };
        }

        protected virtual IQueryable<T> CreatePagedQuery(PagedQuery<T> query, ReadOptions<T> readOptions = null)
        {
            var entityQuery = EntitySet.AsQueryable();
            if (query != null)
            {
                if (readOptions != null)
                {
                    entityQuery = entityQuery.AttachIncludePredicates(readOptions.IncludePredicates);
                    if (readOptions.ReadOnly)
                    {
                       entityQuery = entityQuery.AsNoTracking();
                    }
                }
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions)
                    .ApplyOrderExpressions(query.OrderByExpressions, query.SortOrder);

                entityQuery = entityQuery.Skip(query.ItemsToSkip);
                entityQuery = entityQuery.Take(query.ItemsToGet);
            }
            return entityQuery;
        }

        protected long PagedCount(PagedQuery<T> query)
        {
            var entityQuery = EntitySet.AsQueryable();
            if (query != null)
            {
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions);
            }
            return entityQuery.LongCount();
        }

        protected Task<long> PagedCountAsync(PagedQuery<T> query)
        {
            var entityQuery = EntitySet.AsQueryable();
            if (query != null)
            {
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions);
            }
            return entityQuery.LongCountAsync();
        }

        public void AsModified(T entity, Expression<Func<T, object>> propertyExpression = null)
        {
            if (entity == null) { throw new ArgumentNullException("entity"); }
            if (propertyExpression != null)
            {
                CurrentContext.Entry(entity).Property(propertyExpression).IsModified = true;
            }
            else
            {
                CurrentContext.Entry(entity).State = EntityState.Modified;
            }
        }

        public void AsNotModified(T entity, Expression<Func<T, object>> propertyExpression = null)
        {
            if (entity == null) { throw new ArgumentNullException("entity"); }
            if (propertyExpression != null)
            {
                CurrentContext.Entry(entity).Property(propertyExpression).IsModified = false;
            }
            else
            {
                CurrentContext.Entry(entity).State = EntityState.Unchanged;
            }
        }


        public Task<T> FirstOrDefaultAsync(FilterQuery<T> query, ReadOptions<T> readOptions = null)
        {
            var entityQuery = CreateReadQuery(readOptions);
            if (query != null)
            {
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions);
            }
            return entityQuery.FirstOrDefaultAsync();
        }

        public Task<T> LastOrDefaultAsync(FilterQuery<T> query, ReadOptions<T> readOptions = null)
        {
            var entityQuery = CreateReadQuery(readOptions);
            if (query != null)
            {
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions);
            }
            return entityQuery.OrderByDescending(x => x.Id).FirstOrDefaultAsync();
        }

        public bool Exists(FilterQuery<T> query)
        {
            var entityQuery = EntitySet.AsQueryable();
            if (query != null)
            {
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions);
            }
            return entityQuery.Any();
        }

        public Task<bool> ExistsAsync(FilterQuery<T> query)
        {
            var entityQuery = EntitySet.AsQueryable();
            if (query != null)
            {
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions);
            }
            return entityQuery.AnyAsync();
        }

        protected void ApplyUpdateOptions(T entity, UpdateOptions<T> options)
        {
            if (options != null)
            {
                foreach (var notModifiedProperty in options.NotModifiedProperties)
                {
                    AsNotModified(entity, notModifiedProperty);
                }
            }
        }

        public virtual void Update(T entity, UpdateOptions<T> options = null)
        {
            if (entity == null) { throw new ArgumentNullException("entity"); }
            entity.Modified = DateTime.UtcNow;
            EntitySet.Attach(entity);
            CurrentContext.Entry(entity).State = EntityState.Modified;
            ApplyUpdateOptions(entity, options);
            CurrentContext.SaveChanges();
        }

        public virtual Task UpdateAsync(T entity, UpdateOptions<T> options = null)
        {
            if (entity == null) { throw new ArgumentNullException("entity"); }
            entity.Modified = DateTime.UtcNow;
            EntitySet.Add(entity);
            CurrentContext.Entry(entity).State = EntityState.Modified;
            ApplyUpdateOptions(entity, options);
            return CurrentContext.SaveChangesAsync();
        }

        public virtual void Create(T entity, UpdateOptions<T> options = null)
        {
            if (entity == null) { throw new ArgumentNullException("entity"); }
            entity.Modified = DateTime.UtcNow;
            EntitySet.Add(entity);
            ApplyUpdateOptions(entity, options);
            CurrentContext.SaveChanges();
        }

        public virtual Task CreateAsync(T entity, UpdateOptions<T> options = null)
        {
            if (entity == null) { throw new ArgumentNullException("entity"); }
            entity.Modified = DateTime.UtcNow;
            EntitySet.Add(entity);
            ApplyUpdateOptions(entity, options);
            return CurrentContext.SaveChangesAsync();
        }

        public PatientCareDbContext getsCurrentContext()
        {
            return CurrentContext;
        }

        public async Task<IEnumerable<T>> ListAsync(FilterQuery<T> filter = null, ReadOptions<T> readOptions = null)
        {
            var query = CreateReadQuery(readOptions);
            if (filter != null)
            {
                query = query.ApplyFilters(filter.FilterExpressions);
            }
            return await query.ToArrayAsync();
        }


        public async Task<IEnumerable<T>> AutocompleteAsync(FilterQuery<T> filter = null, ReadOptions<T> readOptions = null)
        {
            var query = CreateReadQuery(readOptions);
            if (filter != null)
            {
                query = query.ApplyFilters(filter.FilterExpressions);                
                query = query.Take(20); // for multi select division
            }
            return await query.ToArrayAsync();
        }

        public IEnumerable<T> List(FilterQuery<T> filter = null, ReadOptions<T> readOptions = null)
        {
            var query = CreateReadQuery(readOptions);
            if (filter != null)
            {
                query = query.ApplyFilters(filter.FilterExpressions);
            }
            return query.ToArray();
        }
    }
}
