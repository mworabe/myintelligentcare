﻿using System;
using System.Data.Entity;
using PatientCare.Data.EntityFramework.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ApplicationRoleMappingExtensions
    {
        public static DbModelBuilder MapApplicationRole(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<IdentityRole>().ToTable("AspNetRoles");

            var entityTypeConfiguration =
                modelBuilder.Entity<ApplicationRole>().ToTable("AspNetRoles");

            entityTypeConfiguration.Property(r => r.Name)
                .IsRequired();

            entityTypeConfiguration.Property(r => r.IsActive)
                .HasColumnName("is_active")
                .IsOptional();

            entityTypeConfiguration.Property(r => r.Description)
                .HasColumnName("description")
                .IsOptional();

            entityTypeConfiguration.Property(r => r.JsonSetup)
                .HasColumnName("json_setup");

            entityTypeConfiguration.Property(r => r.DashboardSetupId)
                .HasColumnName("dashboard_setup_id")
                .IsOptional();

            entityTypeConfiguration
                .HasOptional(r => r.DashboardSetup)
                .WithMany()
                .HasForeignKey(r => r.DashboardSetupId);

            entityTypeConfiguration
                .HasMany(p => p.RoleConfigureLayouts)
                .WithOptional()
                .HasForeignKey(x => x.RoleId);

            return modelBuilder;
        }
    }
}
