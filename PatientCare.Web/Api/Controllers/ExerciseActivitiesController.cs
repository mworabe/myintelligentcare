﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.ExerciseActivities;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle  Exercise Activities operations.
    /// </summary>
    [Authorize]
    public sealed class ExerciseActivitiesController : ApiBaseController
    {
        /// <summary>
        /// Description : Exercise Activities Field IConfigurationDataService.
        /// </summary>
        private readonly IConfigurationDataService _configurationDataService;

        /// <summary>
        /// Description : Exercise Activities Field IExerciseActivityRepository.
        /// </summary>
        private readonly IExerciseActivityRepository _exerciseActivityRepository;

        /// <summary>
        /// Description : Exercise Activities Field IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<ExerciseActivity> _readOptions = new ReadOptions<ExerciseActivity>();

        /// <summary>
        /// Description : Initializes a new instance of the Exercise Activities Class.
        /// </summary>
        /// <param name="configurationDataService">IConfiguration Data Service</param>
        /// <param name="exerciseActivityRepository">IExercise Activity Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public ExerciseActivitiesController
            (
                IConfigurationDataService configurationDataService,
                IExerciseActivityRepository exerciseActivityRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _configurationDataService = configurationDataService;
            _exerciseActivityRepository = exerciseActivityRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/ExerciseActivities/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Exercise Activity.    
        /// </summary>
        /// <param name="request">Exercise Activity List Request Model</param>
        /// <returns>Exercise Activity Response Model</returns>
        [HttpGet]
        [Route("api/ExerciseActivities")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ExerciseActivityResponse>))]
        public IHttpActionResult GetAllExerciseActivity([FromUri]ExerciseActivityListRequest request)
        {
            var query = PagingExtensions<ExerciseActivity>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            var readOptions = new ReadOptions<ExerciseActivity>().AsReadOnly();
            var page = _exerciseActivityRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<ExerciseActivity[], ExerciseActivityResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<ExerciseActivityResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Exercise Activity.   
        /// </summary>
        /// <param name="exerciseActivityRequest">Exercise Activity Request Model</param>
        /// <returns>Exercise Activity Response Model</returns>
        [HttpPost]
        [Route("api/ExerciseActivities")]
        [SwaggerResponse(201, "Created", typeof(ExerciseActivityResponse))]
        public async Task<IHttpActionResult> CreateExerciseActivity([FromBody]ExerciseActivityRequest exerciseActivityRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_exerciseActivityRepository.Exists(FilterQuery.Create<ExerciseActivity>().AddFilter(p => p.Name == exerciseActivityRequest.Name)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<ExerciseActivity>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var ExerciseActivity = Mapper.Map<ExerciseActivityRequest, ExerciseActivity>(exerciseActivityRequest);
            await _configurationDataService.CreateExerciseActivityAsync(ExerciseActivity);

            return Created(GetEntityLocation(ExerciseActivity.Id), Mapper.Map<ExerciseActivity, ExerciseActivityResponse>(ExerciseActivity));
        }

        /// <summary>
        /// Method Description : Using for Update Exercise Activity.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="exerciseActivityRequest">Exercise Activity Request Model</param>
        /// <returns>Exercise Activity Response Model</returns>
        [HttpPut]
        [Route("api/ExerciseActivities/{id}")]
        public async Task<IHttpActionResult> EditExerciseActivity(long id, [FromBody] ExerciseActivityRequest exerciseActivityRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_exerciseActivityRepository.Exists(FilterQuery.Create<ExerciseActivity>().AddFilter(p => p.Name == exerciseActivityRequest.Name && p.Id != exerciseActivityRequest.Id)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<ExerciseActivity>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var ExerciseActivity = Mapper.Map<ExerciseActivityRequest, ExerciseActivity>(exerciseActivityRequest);
            ExerciseActivity.Id = id;

            await _configurationDataService.UpdateExerciseActivityAsync(ExerciseActivity);

            return Ok(Mapper.Map<ExerciseActivity, ExerciseActivityResponse>(ExerciseActivity));
        }

        /// <summary>
        /// Method Description : Using for Delete Exercise Activity by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Exercise Activity Response Model</returns>
        [HttpDelete]
        [Route("api/ExerciseActivities/{id}")]
        public async Task<IHttpActionResult> DeleteExerciseActivityItem(long id)
        {
            var success = await _configurationDataService.DeleteExerciseActivityAsync(id);
            if (!success)
            {
                return NotFound();
            }

            return Ok(new EntryType { Id = id });
        }

        /// <summary>
        /// Method Description : Using for Get Exercise Activity by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Exercise Activity Response Model</returns>
        [HttpGet]
        [Route("api/ExerciseActivities/{id}")]
        public async Task<IHttpActionResult> GetExerciseActivity(long id)
        {
            var ExerciseActivity = await _configurationDataService.ReadExerciseActivityAsync(id);
            if (ExerciseActivity == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<ExerciseActivity, ExerciseActivityResponse>(ExerciseActivity);
            return Ok(viewModel);
        }

        private static Expression<Func<ExerciseActivity, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return ExerciseActivity => ExerciseActivity.Name.Contains(searchPhrase);
        }

        private static Expression<Func<ExerciseActivity, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return ExerciseActivity => ExerciseActivity.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Exercise Activity.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Exercise Activity Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/ExerciseActivities/autocomplete")]
        [ResponseType(typeof(ExerciseActivityAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<ExerciseActivity>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
            }

            var autocompleteResult = await _exerciseActivityRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<ExerciseActivity[], ExerciseActivityAutoCompleteResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }
    }
}
