﻿using System.Threading.Tasks;
using PatientCare.Core.Entities;
using System.Collections.Generic;

namespace PatientCare.Core.Services
{
    public interface ITimeManagementService
    {
        //#region Holiday Methods
        //Task<bool> UpdateHolidayAsync(Holiday holiday);
        //Task<bool> CreateHolidayAsync(Holiday holiday);
        //Task<Holiday> ReadHolidayAsync(long id);
        //List<Holiday> ReadHolidayMonthlyAsync(long month, long year);
        //List<Holiday> ReadAllHolidayAsync();
        //Task<bool> DeleteHolidayAsync(long id);

        //#endregion

        //#region Leave Type Methods
        //Task<bool> UpdateLeaveTypeAsync(LeaveType leaveType);
        //Task<bool> CreateLeaveTypeAsync(LeaveType leaveType);
        //Task<LeaveType> ReadLeaveTypeAsync(long id);
        //List<LeaveType> SearcLeaveTypeAsync(string search);
        //Task<bool> DeleteLeaveTypeAsync(long id);
        //#endregion

        //#region Leave Request Methods
        //Task<bool> SendLeaveRequestAsync(LeaveRequest leaveRequest);

        //Task<LeaveRequest> ReadLeaveRequestByIdAsync(long id);

        //Task<bool> ApproveLeaveRequestAsync(LeaveRequest approveLeaveRequest);

        //string GetStringForLeaveRequestList(long resourceid, bool IsSupervisor);

        //Task<bool> UpdateLeaveRequestAsync(LeaveRequest leaveRequest);

        //#endregion

        #region UserLoginhistory Methods

        //Task<bool> UpdateUserLoginHistoryAsync(UserLoginHistory userLoginHistory);

        Task<bool> CreateuserLoginHistoryAsync(UserLoginHistory userLoginHistory);

        #endregion

        //#region Entry Type Methods
        //Task<bool> UpdateEntryTypeAsync(EntryType entryType);
        //Task<bool> CreateEntryTypeAsync(EntryType entryType);
        //Task<EntryType> ReadEntryTypeAsync(long id);
        //List<EntryType> SearcEntryTypeAsync(string search);
        //Task<bool> DeleteEntryTypeAsync(long id);
        //#endregion
       
    }
}
