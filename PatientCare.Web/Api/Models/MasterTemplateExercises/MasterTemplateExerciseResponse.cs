﻿using PatientCare.Web.Api.Models.Exercises;
using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.MasterTemplateExercises
{
    [DataContract]
    public class MasterTemplateExerciseResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ExerciseId { get; set; }

        [DataMember]
        public ExerciseResponse Exercise { get; set; }

        [DataMember]
        public long? MasterTemplateId { get; set; }

        [DataMember]
        public int? ExerciseOrder { get; set; }

    }
}