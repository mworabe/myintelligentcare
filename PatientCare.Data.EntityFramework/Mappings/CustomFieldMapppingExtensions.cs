﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class CustomFieldMapppingExtensions
    {
        public static DbModelBuilder MapCustomField(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.MapPicklistInfo<CustomField>("custom_fields", "ix_custom_field_name_unique");
            modelBuilder.Entity<CustomField>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<CustomField>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<CustomField>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255);

            modelBuilder.Entity<CustomField>()
                .Property(p => p.Description)
                .HasColumnName("description")
                .IsOptional()
                .IsUnicode()
                .HasMaxLength(256);

            modelBuilder.Entity<CustomField>()
                .Property(p => p.DefaultValue)
                .HasColumnName("default_value")
                .IsUnicode()
                .IsOptional();

            modelBuilder.Entity<CustomField>()
                .Property(p => p.Require)
                .HasColumnName("require")
                .IsRequired();

            modelBuilder.Entity<CustomField>()
                .Property(p => p.ResourceAvailable)
                .HasColumnName("resource_available")
                .IsOptional();
            
            modelBuilder.Entity<CustomField>()
                .Property(p => p.Type)
                .HasColumnName("type")
                .IsRequired();

            modelBuilder.Entity<CustomField>()
                .HasMany<ResourceCustomFieldValue>(p => p.ResourceCustomFieldValues)
                .WithOptional()
                .HasForeignKey(item => item.CustomFieldId);

            modelBuilder.Entity<CustomField>()
                .HasMany<CustomFieldProperty>(p => p.CustomFieldProperties)
                .WithOptional()
                .HasForeignKey(item => item.CustomFieldId);

            return modelBuilder;
        }
    }
}