﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Exercises;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.HepFrequencies
{
    [DataContract]
    public class HepFrequencyResponse
    {

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? HepId { get; set; }

        //[DataMember]
        //public ExerciseResponse Hep { get; set; }

        [DataMember]
        public long? HepDetailId { get; set; }

        [DataMember]
        public int? Day { get; set; }

        [DataMember]
        public int? ExerciseFrequency { get; set; }

        [DataMember]
        public bool? IsActive { get; set; }

    }
}