﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceEducationRepository : IRepository<ResourceEducation>
    {
    }
}
