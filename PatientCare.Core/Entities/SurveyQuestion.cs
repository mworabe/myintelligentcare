﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Survey Question.
    /// </summary>
    public class SurveyQuestion : Entity
    {
        public string Text { get; set; }
        public long CategoryId { get; set; }
        public int Weight { get; set; }
        public virtual SurveyCategory Category { get; set; }
    }
}
