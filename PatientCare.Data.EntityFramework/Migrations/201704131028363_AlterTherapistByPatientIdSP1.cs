namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterTherapistByPatientIdSP1 : DbMigration
    {
        public override void Up()
        {
            AlterStoredProcedure(
          "dbo.sp_therapist_by_patientId",
          p => new
          {
              Type = p.String(defaultValueSql: null),
              DataCount = p.Int(defaultValueSql: null),
              Search = p.String(defaultValueSql: null),
              PatientId = p.Long(defaultValueSql: null)
          },
          body:
            @"declare @cliicId bigint,
		               @clinicLocation bigint;
	            select @cliicId = clinic_id , @clinicLocation = clinic_location_id  from patients where Id = @PatientId

	            if (@Type = 'all')
	            begin
		            select Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email from resources 
		            where Id in (
		            select Distinct  users.ResourceId from AspNetUsers users
		            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
		            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist') 
		            and clinic_id = @cliicId
	            end
	            else if (@Type = 'withSearch')
	            begin
		            select TOP (@DataCount) Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email from resources 
		            where Id in (
		            select Distinct  users.ResourceId from AspNetUsers users
		            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
		            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist')
		            and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
		            and clinic_id = @cliicId

	            end"
                    );
        }
        
        public override void Down()
        {
            AlterStoredProcedure(
          "dbo.sp_therapist_by_patientId",
          p => new
          {
              Type = p.String(defaultValueSql: null),
              DataCount = p.Int(defaultValueSql: null),
              Search = p.String(defaultValueSql: null),
              PatientId = p.Long(defaultValueSql: null)
          },
          body:
            @"declare @cliicId bigint,
		               @clinicLocation bigint;
	            select @cliicId = clinic_id , @clinicLocation = clinic_location_id  from patients where Id = @PatientId

	            if (@Type = 'all')
	            begin
		            select Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email from resources 
		            where Id in (
		            select Distinct  users.ResourceId from AspNetUsers users
		            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
		            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist') 
		            and clinic_id = @cliicId and clinic_location_id = @clinicLocation
	            end
	            else if (@Type = 'withSearch')
	            begin
		            select TOP (@DataCount) Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email from resources 
		            where Id in (
		            select Distinct  users.ResourceId from AspNetUsers users
		            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
		            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name ='Therapist')
		            and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
		            and clinic_id = @cliicId and clinic_location_id = @clinicLocation

	            end"
                    );
        }
    }
}
