﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace TwilioIpMessaging
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            //config.Routes.MapHttpRoute(
            //    name: "API TokenEndpoint",
            //    routeTemplate: "services/newtoken/{grantType}",
            //    defaults: new { controller = "Chat" },
            //    constraints: null);
        }

    }
}
