﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.ResourcePracticeAreas;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourcePracticeAreaMapperExtensions
    {
        public static EmitMapperMapperProvider ResourcePracticeAreaRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePracticeArea,ResourcePracticeAreaResponse>
                    (new ExtendedMapConfig<ResourcePracticeArea,ResourcePracticeAreaResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePracticeArea[],ResourcePracticeAreaResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePracticeAreaResponse, ResourcePracticeArea>
                    (new ExtendedMapConfig<ResourcePracticeAreaResponse, ResourcePracticeArea>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePracticeAreaRequest,ResourcePracticeArea>
                    (new ExtendedMapConfig<ResourcePracticeAreaRequest,ResourcePracticeArea>()));

            return mapperProvider;
        }
    }
}