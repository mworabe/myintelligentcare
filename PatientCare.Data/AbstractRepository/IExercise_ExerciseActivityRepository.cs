﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IExercise_ExerciseActivityRepository : IRepository<Exercise_ExerciseActivity>
    {
    }
}
