﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.ResourceBehaviouralDetails;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceBehaviouralDetailMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceBehaviouralDetailRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceBehaviouralDetail, ResourceBehaviouralDetailResponse>
                    (new ExtendedMapConfig<ResourceBehaviouralDetail, ResourceBehaviouralDetailResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceBehaviouralDetail[], ResourceBehaviouralDetailResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceBehaviouralDetailRequest, ResourceBehaviouralDetail>
                    (new ExtendedMapConfig<ResourceBehaviouralDetailRequest, ResourceBehaviouralDetail>()));

            return mapperProvider;
        }
    }
}