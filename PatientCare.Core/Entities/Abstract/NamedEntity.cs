﻿using System.Diagnostics;

namespace PatientCare.Core.Entities.Abstract
{
    /// <summary>
    /// This class is use as Abstract class .
    /// </summary>
    [DebuggerDisplay("Id = {Id}, Name = {Name}")]
    public abstract class NamedEntity : Entity
    {
        public string Name { get; set; }
    }
}
