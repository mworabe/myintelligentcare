﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("LoginHistoryCtrl", [
       "$scope", "LoginHistory", 'GridHelper',
        function ($scope, loginHistoryService, GridHelper) {
            var loadData = function (params) {
               
                var callParams = GridHelper.getCallParams(params);
                return loginHistoryService.query(callParams)
                    .$promise.then(function (data) {
                        params.total(data.itemsCount);
                        return data.data;
                    });
            }
            $scope.tp = GridHelper.getTableParams(loadData);
        }
    ]);