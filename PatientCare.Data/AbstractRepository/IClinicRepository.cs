﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IClinicRepository : IRepository<Clinic>
    {
    }
}
