﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Employment Type Enum.
    /// </summary>
    public enum EmploymentTypeEnum
    {
        Hourly = 0,
        Salary = 1
    }
}
