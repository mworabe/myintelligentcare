﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ExercisePositions
{
    [DataContract]
    public class ExercisePositionAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}