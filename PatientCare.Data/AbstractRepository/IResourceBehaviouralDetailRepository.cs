﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceBehaviouralDetailRepository : IRepository<ResourceBehaviouralDetail>
    {
    }
}
