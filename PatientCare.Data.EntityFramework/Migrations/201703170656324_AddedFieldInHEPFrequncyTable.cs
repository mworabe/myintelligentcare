namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldInHEPFrequncyTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hep_exercise_frequency", "is_active", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.hep_exercise_frequency", "is_active");
        }
    }
}
