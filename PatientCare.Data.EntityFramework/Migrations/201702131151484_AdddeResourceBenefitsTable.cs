namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdddeResourceBenefitsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.resource_insurances",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        resource_id = c.Long(),
                        insurance_type_id = c.Long(),
                        participating = c.String(),
                        employee_cost = c.String(),
                        employer_contribution = c.String(),
                        created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.drop_down_configs", t => t.resource_id)
                .ForeignKey("dbo.resources", t => t.resource_id)
                .ForeignKey("dbo.resources", t => t.insurance_type_id)
                .Index(t => t.resource_id)
                .Index(t => t.insurance_type_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.resource_insurances", "insurance_type_id", "dbo.resources");
            DropForeignKey("dbo.resource_insurances", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_insurances", "resource_id", "dbo.drop_down_configs");
            DropIndex("dbo.resource_insurances", new[] { "insurance_type_id" });
            DropIndex("dbo.resource_insurances", new[] { "resource_id" });
            DropTable("dbo.resource_insurances");
        }
    }
}
