﻿using System.Runtime.Serialization;
using PatientCare.Core.Entities;
using System.Collections.Generic;

namespace PatientCare.Web.Api.Models.Resources
{
    [DataContract]
    public class ResourceHierarchicalResponse
    {
        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public string ResourceName  { get; set; }

        [DataMember]
        public string ResourceFullName { get; set; }


        [DataMember]
        public string JobTitle { get; set; }

        [DataMember]
        public IEnumerable<ResourceHierarchicalResponse> Children { get; set; }
    }
}