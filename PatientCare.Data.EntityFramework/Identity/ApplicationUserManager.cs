﻿using System.Data.Entity;
using System.Linq;
using PatientCare.Data.AbstractRepository;
using Microsoft.AspNet.Identity;

namespace PatientCare.Data.EntityFramework.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        protected virtual IQueryable<ApplicationUser> CreatePagedQuery(PagedQuery<ApplicationUser> query, ReadOptions<ApplicationUser> readOptions = null)
        {
            var entityQuery = Users.AsQueryable();
            if (query != null)
            {
                if (readOptions != null)
                {
                    entityQuery = entityQuery.AttachIncludePredicates(readOptions.IncludePredicates);
                    if (readOptions.ReadOnly)
                    {
                        entityQuery = entityQuery.AsNoTracking();
                    }
                }
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions)
                    .ApplyOrderExpressions(query.OrderByExpressions, query.SortOrder);

                entityQuery = entityQuery.Skip(query.ItemsToSkip);
                entityQuery = entityQuery.Take(query.ItemsToGet);
            }
            return entityQuery;
        }
        protected long PagedCount(PagedQuery<ApplicationUser> query)
        {
            var entityQuery = Users.AsQueryable();
            if (query != null)
            {
                entityQuery = entityQuery.ApplyFilters(query.FilterExpressions);
            }
            return entityQuery.LongCount();
        }


        public PagedResult<ApplicationUser> PagedList(PagedQuery<ApplicationUser> query, ReadOptions<ApplicationUser> readOptions = null)
        {
            var entities = CreatePagedQuery(query, readOptions).ToArray();
            var totalCount = PagedCount(query);

            return new PagedResult<ApplicationUser>
            {
                Entities = entities,
                TotalCount = totalCount
            };
        }
    }
}
