﻿using PatientCare.Web.ApiMobile.Models.PatientGoals;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.Patients
{
    [DataContract]
    public class PatientMobileProfileMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public long? PatientId { get; set; }
        [DataMember]
        public string PatientFullName { get; set; }
        [DataMember]
        public string PatientPhone { get; set; }
        [DataMember]
        public string PatientEmail { get; set; }
        [DataMember]
        public string PatientPhoto { get; set; }
        [DataMember]
        public string PatientPhotoNameUrl { get; set; }
        [DataMember]
        public string PatientNumber { get; set; }
        
        [DataMember]
        public string PatientLanguage { get; set; }
        [DataMember]
        public string PatientCountry { get; set; }
        [DataMember]
        public string PatientState { get; set; }
        [DataMember]
        public string PatientCity { get; set; }
        
        [DataMember]
        public long? PatientCaseId { get; set; }
        [DataMember]
        public string PatientCaseNo { get; set; }
        [DataMember]
        public long? PatientInjuryRegionId { get; set; }
        [DataMember]
        public string PatientInjuryRegion { get; set; }

        [DataMember]
        public PatientClinicianMobileResponse[] Clinicians { get; set; }

        [DataMember]
        public PatientGoalMobileResponse[] Goals { get; set; }
    }
}