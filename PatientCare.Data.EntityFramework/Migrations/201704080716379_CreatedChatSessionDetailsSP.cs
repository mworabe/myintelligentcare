namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreatedChatSessionDetailsSP : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
               "dbo.sp_Chat_Session_Details",
               p => new
               {
                   channel_id = p.String(defaultValueSql: null)
               },
               body:
                    @"select 
		                chats.Id,chats.channel_id as ChannelsId,chats.channel_unique_name as ChannelUniqueName,
		                chats.channel_name as ChannelName,chats.from_user_id as FromUserId,chats.to_user_id as ToUserId,
		                chats.created_by_user_id as CreatedByUserId,chats.last_message as LastMessage ,
		                chats.type as Type ,chats.created as Created,chats.modified as Modified, chatmem.Id as ChatMemberId,chatmem.user_id as UserId
		                from chat_Sessions chats
		                left join chat_members chatmem on chats.Id = chatmem.chat_session_id
	                 where chats.channel_id = @channel_id"
                   );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_Chat_Session_Details");
        }
    }
}
