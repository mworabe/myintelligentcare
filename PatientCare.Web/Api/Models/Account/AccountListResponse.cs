﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Account
{
    [DataContract]
    public class AccountListResponse : ListResponse<AccountResponse>
    {
        public AccountListResponse(AccountResponse[] data, long itemsCount, string[] roles)
            : base(data, itemsCount)
        {
            Roles = roles;
        }

        [DataMember]
        public string[] Roles { get; set; }
    }
}