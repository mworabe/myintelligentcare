﻿(function ResourceEditCtrlIIFE() {
    "use strict";
    angular.module("EDZoutstaffingPortalApp")
        .controller("PatientEditCtrl",
            ["$scope", "$stateParams", "patientService", "$state", "$q",
                "toaster", "Upload", "Country", "CreatorSearchFunction", "uuid2", "DateTimeHelper", "Modal",
                "CreatorLeavePage", "PermissionWorker", "customFieldsResource", "$rootScope", "ConfigLayout", "StateService",
                "CityService", "DynamicDropDownConfigService", "DropdownConfigsFunction", "$timeout", "SystemFunctions",
                "$filter", "$translate", "$uibModal", "Language", "ReferralPhysicianService", "ClinicService", "patientCaseService", "Resource",
                "LocationService", "ClinicLocationService", '$injector', '$uibModalInstance', 'ImageCropFactory', "ClinicianService","ZipCodeService",
                PatientEditCtrlFunc]
        );
    function PatientEditCtrlFunc($scope, $stateParams, patientService, $state, $q,
                              toaster, uploader, country, creatorSearchFunction, uuid2, dateTimeHelper, modal,
                              creatorLeavePage, permissionWorker, initialValues, $rootScope, configLayoutFactory, StateService,
                              CityService, dynamicDropDownConfigService, dropdownConfigsFunction, $timeout, systemFunctionsService,
                              $filter, $translate, $uibModal, languageService, referralPhysicianService, clinicService, patientCaseService, resourceService,
                              locationService, clinicLocationService, $injector, $uibModalInstance, ImageCropFactory, ClinicianService, ZipCodeService) {

        var self = this;
        console.info(!(initialValues[2] && initialValues[2].errorCode === 404));
        self.isPatientLoadedCorrectly = !(initialValues[2] && initialValues[2].errorCode === 404);

        if (!self.isPatientLoadedCorrectly) {
            toaster.pop("error", "", "We are unable to serve you at this moment, please try again later.");
            return;
        }

        self.resourceConfig = initialValues[1] || {};
        self.resourceTabs = _.unionBy(initialValues[1].tabs, configLayoutFactory.getPatientTab(), "id");

        /** Fix for IRMS5 only, Education Date Field as Text Field.*/
        if (initialValues[2] && self.IS_BUILD_FOR_STINSON) {
            var tempModel = initialValues[2];
            var educations = tempModel.educations;
            var tempCertificate = tempModel.certificates;

            _.forEach(educations, function (edu) {
                if (edu.dateObtained) edu.dateObtained = edu.dateObtained.substring(0, 4);// moment(edu.dateObtained).format("YYYY");
            })
            _.forEach(tempCertificate, function (edu) {
                if (edu.issueDate) edu.issueDate = edu.issueDate.substring(0, 4);// moment(edu.issueDate).format("YYYY");
            })
        }


        var tempResourceModel = angular.copy(initialValues[2]);
        var modelId = $stateParams.id || undefined;
        self.isNewPatient = (modelId && modelId > 0);
        var counterFilesPromise = 0;
        var audioService = {};
        var salaryCoeff = 2080;
        var params = {};
        var MIN_YEARS = 15;
        var MAX_YEARS = 100;
        //var $uibModalInstance;
        //if (!modelId) {
        //    $uibModalInstance = $injector.get("$uibModalInstance");
        //}


        //tabs options
        self.other_Condition = isToShowOtherTab;
        self.summary_Condition = showSummaryTab;
        //self.backgroundCheck_Condition = permissionWorker.restrictBackgroundChecks;
        //self.criminalRecords_Condition = permissionWorker.restrictCriminalRecords;
        self.previousEmployments_Condition = permissionWorker.restrictPreviousEmployments;
        self.workingDetails_Click = calculateYearsOfExperience;
        self.resourceHref = $state.href("resource");
        self.leftMenuMaxHeight = (window.innerHeight - 30);
        self.patientHep_Click = function () {
            $state.go("patientCaseEdit", { patientId: modelId, caseId: self.model.caseId, currentTab: "hep" });
        }


        self.model = {

        };

        


        self.model.range = "M";
        self.autocompleteMinLength = 0;
        self.availabilityArray = [];
        self.availableVsDemandData = [];
        self.availableVsDemandHeaders = [];
        self.availableList = [];
        self.projectId = modelId;
        self.model.active = true;
        self.currentTab = $stateParams.currentTab || "patientDetails";
        //self.currentTab = "summary";
        self.availabilityModelShow = { timeRange: "Day" };
        self.callSubmit = false;
        self.companyName = "";
        self.organizationalChartData = [];
        self.availableVsDemandData = $stateParams.availabilityData || [];
        self.availableVsDemandHeaders = $stateParams.availabilityHeaders || [];


        
        

        $scope.resumeFieldList = {};

        function getValueInAnguAutocomplete(selected) {
            return (selected && selected.originalObject) ? selected.originalObject.id : null;
        }

        self.replaceSpecialChars = function (string) {
            if (!string || !string.length)
                return string;
            var replaceString = string.replace(/\n/g, "<br>");
            return replaceString;
        }

        function showSummaryTab() {
            return !(self.model.id && self.model.id > 0)
        }

        function isToShowOtherTab() {
            if (self.otherContainer) {
                if (self.otherContainer.container1 && self.otherContainer.container1.length > 0)
                    return false;
                else if (self.otherContainer.container2 && self.otherContainer.container2.length > 0)
                    return false;
                else if (self.otherContainer.container3 && self.otherContainer.container3.length > 0)
                    return false;
                else if (self.otherContainer.container4 && self.otherContainer.container4.length > 0)
                    return false;
                else if (self.otherContainer.container5 && self.otherContainer.container5.length > 0)
                    return false;
                else if (self.otherContainer.container6 && self.otherContainer.container6.length > 0)
                    return false;
                else
                    return true;
            }
            return true;
        }

        self.getPatientCaseList = patientCaseService.getPatientCases;
        self.getPatientCaseTableOptions = patientCaseService.getTableOption;
        self.getPatientCaseReloadCommand = patientCaseService.reloadCommand;
        self.onPatientCaseDelete = patientCaseService.onPatientCaseDelete;
        self._onPatientCasesResponseTransform = function _onPatientCasesResponseTransform(data) {
            data.patientId = self.model.id;
        }
        self.onAddNewCaseButtonClick = function onAddNewCaseButtonClick() {
            $state.go('patientsEdit.patient-case-add', { patientId: self.model.id });
        }

        self.birthDate_max = moment().subtract(MIN_YEARS, "year");
        self.birthDate_min = moment().subtract(MAX_YEARS, "year");



        self.dateFormat = function (date, formate) {
            if (!date)
                return "";
            if (!formate)
                formate = "MM/DD/YYYY"
            return moment(date).format(formate);
        }

        self.resumeSmallDateFormat = function (date) {
            if (!date)
                return "";
            return moment(date).format("MM/YYYY");
        }

        self.resumeCertificateDateFormat = function (date) {
            if (!date)
                return "";
            return moment(date).format("YYYY");
        }

        self.getPreviousEmploymentCompany_and_TitleValue = function getPreviousEmploymentCompany_and_TitleValue(companyName, titleOfPosition) {
            return (companyName) ? ((titleOfPosition) ? companyName + " / " + titleOfPosition : companyName) : "";
        }

        function getMonthsDiffrence(d1, d2) {
            var months;
            var d1Y = d1.getFullYear();
            var d2Y = d2.getFullYear();
            var d1M = d1.getMonth();
            var d2M = d2.getMonth();

            return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
        }

        self.getResourceFullName = function () {
            if (self.model) {
                if ((self.model.lastName && self.model.lastName.length > 0) && (self.model.firstName && self.model.firstName.length > 0)) {
                    return self.model.lastName + " " + self.model.firstName + " / ";
                }
                return "";
            }
        }

        self.getResourceFullNameForSummary = function () {
            if (self.model) {
                if ((self.model.lastName && self.model.lastName.length > 0) && (self.model.firstName && self.model.firstName.length > 0)) {
                    return self.model.lastName + ", " + self.model.firstName + " / ";
                }
                return "";
            }
        }

        function calculateYearsOfExperience() {
            if (self.model && self.model.startDate) {
                var totalMonths = getMonthsDiffrence(new Date(self.model.startDate), new Date());
                var years = parseInt(totalMonths / 12);
                var months = ((totalMonths / 12) - years) * 12;
                self.model.yearsEmployed = parseInt(years);
                self.model.monthsEmployed = parseInt(months);
            }
        }

        self.getEmployedFromString = function () {
            if (self.model && self.model.startDate) {
                var totalMonths = getMonthsDiffrence(new Date(self.model.startDate), new Date());
                var years = parseInt(totalMonths / 12);
                var months = ((totalMonths / 12) - years) * 12;
                return "Years Employed : " + ((years > 0) ? "<strong>" + parseInt(years) + "</strong> Years " : "") + ((months > 0) ? "<strong>" + parseInt(months) + "</strong> Months " : "");
            }
        }

        function resourceCompanySelected(selected) {
            self.model.resourceCompanyId = getValueInAnguAutocomplete(selected);
            self.model.resourceCompany = (selected && selected.originalObject) ? selected.originalObject.name : null;
        }

        function businessUnitSelected(selected) {
            self.model.businessUnitId = getValueInAnguAutocomplete(selected);
        }

        function IndustrySelected(selected) {
            self.model.industryId = getValueInAnguAutocomplete(selected);
        }

        function resourceJobTitleSelected(selected) {
            self.model.jobTitleId = getValueInAnguAutocomplete(selected);
        }

        function resourceCountrySelected(selected) {
            self.model.countryId = getValueInAnguAutocomplete(selected);
        }

        function resourceStateSelected(selected) {
            self.model.stateId = getValueInAnguAutocomplete(selected);
        }
        self.zipCode_Enum = [];
        function resourceCitySelected(selected) {
            self.model.cityId = getValueInAnguAutocomplete(selected);
            self.zipCode_Enum = (selected && selected.originalObject && selected.originalObject.postalCodeLists) ? selected.originalObject.postalCodeLists : [];

            if (selected && selected.title) { // manually Call city changed
                if (self.zipCode_Enum && self.zipCode_Enum.length > 0) {
                    //self.model.zipCode = self.zipCode_Enum[0].zipCode;
                    $timeout(function () {
                        $scope.$broadcast('angucomplete-alt:changeInput', 'zipCode', self.model.zipCode);
                    }, 1000);
                } else
                    self.model.zipCode = null;
            }
        }
        self.clinicLocation_Enum = [];


        function getAutoCompleteValueId(selected) {
            return (selected && selected.originalObject) ? selected.originalObject.id : null;
        }

        //zipCode

        self.zip1_InputChanged = function (str) {
            self.model.zip1 = $("#zip1_value").val();
            updateGeoNamesForZip1Code();
        }
        self.zip1_Selected = function (selected) {
            self.model.zip1 = (selected && selected.originalObject) ? selected.originalObject.name : null;
            updateGeoNamesForZip1Code();
        }
        function updateGeoNamesForZip1Code() {
            if (self.model.zip1 && self.prevZipCode1 != self.model.zip1) {
                self.prevZipCode1= angular.copy(self.model.zip1);
                resourceService.getGeoNamesForZipCode({ zipCode: self.model.zip1 }, function (success) {
                    
                    if (success && success.city && success.state && success.country) {
                        $timeout(function updateAutoCompleteValue() {
                            self.model.city1 = success.city;
                            self.model.state1 = success.state;
                            self.model.country1 = success.country;
                            

                            $scope.$broadcast('angucomplete-alt:changeInput', 'city1', success.city);
                            $scope.$broadcast('angucomplete-alt:changeInput', 'state1', success.state);
                            $scope.$broadcast('angucomplete-alt:changeInput', 'country1', success.country);

                        }, 100);
                    } else {
                        console.info("Zip Cleared from GeoNames");
                        $scope.$broadcast('angucomplete-alt:clearInput', 'zip1');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'city1');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'state1');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'country1');
                        toaster.pop("error", "", "Entered Postal code 1 is not valid, Re-Enter it or Choose one from list.");
                    }
                }, function (error) { });
            }
        }




        self.zip2_InputChanged = function (str) {
            self.model.zip2 = $("#zip2_value").val();
            updateGeoNamesForZip2Code();
        }
        self.zip2_Selected = function (selected) {
            self.model.zip2 = (selected && selected.originalObject) ? selected.originalObject.name : null;
            updateGeoNamesForZip2Code();
        }


        function updateGeoNamesForZip2Code() {
            if (self.model.zip2 && self.prevZipCode2 != self.model.zip2) {
                self.prevZipCode2 = angular.copy(self.model.zip2);
                resourceService.getGeoNamesForZipCode({ zipCode: self.model.zip2 }, function (success) {
                    if (success && success.city && success.state && success.country) {
                        $timeout(function updateAutoCompleteValue() {
                            self.model.city2 = success.city;
                            self.model.state2 = success.state;
                            self.model.country2 = success.country;
                            

                            $scope.$broadcast('angucomplete-alt:changeInput', 'city2', success.city);
                            $scope.$broadcast('angucomplete-alt:changeInput', 'state2', success.state);
                            $scope.$broadcast('angucomplete-alt:changeInput', 'country2', success.country);

                        }, 100);
                    } else {
                        console.info("Zip Cleared from GeoNames");
                        $scope.$broadcast('angucomplete-alt:clearInput', 'zip2');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'city2');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'state2');
                        $scope.$broadcast('angucomplete-alt:clearInput', 'country2');
                        toaster.pop("error", "", "Entered Postal code 2 is not valid, Re-Enter it or Choose one from list.");
                    }
                }, function (error) { });
            }
        }


        function referralPhysicianSelected(selected) {
            self.model.referralPhysicianId = getAutoCompleteValueId(selected);
        }
        function clinicianSelected(selected) {
            self.model.clinicianId = getAutoCompleteValueId(selected);
            
            
        }
        function clinicSelected(selected) {
            self.model.clinicId = getAutoCompleteValueId(selected);
            creatorSearchFunction(self, "clinicLocation_Search", clinicLocationService, "", { clinicId: self.model.clinicId });
        }
        function clinicLocationSelected(selected) {
            self.model.clinicLocationId = getAutoCompleteValueId(selected);
        }

        function resourceArmedForcesCountrySelected(selected) {
            self.model.armedForcesCountryId = getValueInAnguAutocomplete(selected);
        }

        function resourceSupervisorSelected(selected) {
            self.model.supervisorId = getValueInAnguAutocomplete(selected);
        }

        function certificateStateSelected(selected) {
            self.model.certificateStateId = getValueInAnguAutocomplete(selected);
        }
        function patientLanguageSelected(selected) {
            self.model.languageId = getValueInAnguAutocomplete(selected);
        }

        function city1Selected(selected) {
            self.model.city1Id = getValueInAnguAutocomplete(selected);
        }
        function state1Selected(selected) {
            //if (!self.model.address1) self.model.address1 = {};
            self.model.state1Id = getValueInAnguAutocomplete(selected);
        }
        function country1Selected(selected) {
            //if (!self.model.address1) self.model.address1 = {};
            self.model.country1Id = getValueInAnguAutocomplete(selected);
        }

        function city2Selected(selected) {
            //if (!self.model.address2) self.model.address2 = {};
            self.model.city2Id = getValueInAnguAutocomplete(selected);
        }
        function state2Selected(selected) {
            //if (!self.model.address2) self.model.address2 = {};
            self.model.state2Id = getValueInAnguAutocomplete(selected);
        }
        function country2Selected(selected) {
            //if (!self.model.address2) self.model.address2 = {};
            self.model.country2Id = getValueInAnguAutocomplete(selected);
        }

        self.cellNumberIsPrimary_OnChange = function (isChecked) {
            self.model.homeNumberIsPrimary = !isChecked
        }

        self.homeNumberIsPrimary_OnChange = function (isChecked) {
            self.model.cellNumberIsPrimary = !isChecked
        }

        self.address1IsPrimary_OnChange = function (isChecked) {
            self.model.address2IsPrimary = !isChecked
        }

        self.address2IsPrimary_OnChange = function (isChecked) {
            self.model.address1IsPrimary = !isChecked
        }


        self.city1_Selected = city1Selected;
        self.city2_Selected = city2Selected;
        self.state1_Selected = state1Selected;
        self.state2_Selected = state2Selected;
        self.country1_Selected = country1Selected;
        self.country2_Selected = country2Selected;

        


        self.referralPhysician_Selected = referralPhysicianSelected;
        self.clinician_Selected = clinicianSelected;
        self.clinic_Selected = clinicSelected;
        self.clinicLocation_Selected = clinicLocationSelected;
        self.language_Selected = patientLanguageSelected;
        self.resourceCompany_Selected = resourceCompanySelected;
        self.businessUnit_Selected = businessUnitSelected;
        self.industry_Selected = IndustrySelected;
        self.jobTitle_Selected = resourceJobTitleSelected;
        self.country_Selected = resourceCountrySelected;
        self.state_Selected = resourceStateSelected;
        self.city_Selected = resourceCitySelected;
        self.armedForcesCountry_Selected = resourceArmedForcesCountrySelected;
        self.supervisor_Selected = resourceSupervisorSelected;
        self.certificateState_Selected = certificateStateSelected;

        //todo

        self.uploadFileAvatarFunc = function (file, $invalidFile) {
            if ($invalidFile && $invalidFile.length > 0) {
                toaster.pop("error", "", "Max File Size 5MB");
            }
            if (!file)
                return;
            modal.confirm.templateDialog(function (data) {
                var mimeString = data.output.split(',')[0].split(':')[1].split(';')[0];
                self.uploadFileAvatar = base64ToBlob(data.output, mimeString);
                self.uploadFileAvatar.name = data.image.name;
            }, "Client/app/controllers/resource/templates/cropAvatar.html")({ image: file, output: null });

        }


        self.deleteAvatar = function () {
            self.model.photoFileName = "";
            self.uploadFileAvatar = null;
            self.model.photoFileNameUrl = "";
        }
        self.deleteCV = function () {
            self.model.cvFileNameUrl = "";
            self.model.cvFileName = "";
            self.uploadFileCV = null;

        };

        self.visibleDeleteCvButton = function () {
            return self.model.cvFileNameUrl || self.model.cvFileName || self.uploadFileCV;

        };

        self.onCancel_Click = function () {
            $scope.ignoreRedirect = true;
            $state.go($state.current, { currentTab: self.currentTab, id: modelId }, { reload: true });
            $timeout(function () { toaster.pop("success", "", "Resource Restored Successful."); }, 2000);
        };

        self.onCancelButtonClick = function onCancelButtonClick() {
            systemFunctionsService.historyBackButtonClick($rootScope, $state, $scope);
            $scope.ignoreRedirect = true;
            if ($state.current.name === "patients.patients-add")
                $state.go("patients", {}, { reload: true });
            else if ($state.current.name === "schedules.patients-add")
                $state.go("schedules", {}, { reload: true });

            //window.history.back();
        }

        $scope.hideErrorTab = function () {
            $scope.formErrorList = null;
        }

        /**
            Form Validation and Header Error Mechanism Functions.
        */

        self.getAllResourceFields = function () {
            var allFieldsArray = [];
            if (configLayoutFactory.getPatientFields().length > 0) {
                allFieldsArray = allFieldsArray.concat(configLayoutFactory.getPatientFields())
            }
            return allFieldsArray;
        }

        function getTabName(tabId) {
            var tabList = configLayoutFactory.getPatientTab();
            var tabName = "";
            _.forEach(tabList, function (tab) {
                if (tab.container == tabId) {
                    tabName = tab.name;
                }
            });
            return tabName;
        }

        function getFieldByName(fieldId, tabName) {

            var resourceAllFields = self.getAllResourceFields();
            if (!tabName) {
                for (var fieldIndex = 0; fieldIndex < resourceAllFields.length; fieldIndex++) {
                    var field = resourceAllFields[fieldIndex];

                    if (field.id == fieldId || field.fieldName == fieldId) {
                        return field;
                    }
                }
            } else {

                for (var fieldIndex = 0; fieldIndex < resourceAllFields.length; fieldIndex++) {
                    var field = resourceAllFields[fieldIndex];

                    if ((field.id == fieldId || field.fieldName == fieldId) && field.container == tabName) {
                        return field;
                    }
                }
            }
        }

        function prepareResourceFormErrorList(form) {
            var errorList = {};
            _.forIn(form.$error, function (arr) {
                var arry = arr;
                _.forEach(arry, function (item) {

                    var currentTabName = null;


                    // Patch for Skill, Education like tabs where field name is different than model name and added _uniq value.
                    if (item.$options.tabValidationName) {
                        currentTabName = item.$options.tabValidationName;
                    }
                    var fieldName = item.$name;
                    if (fieldName === "rating_date") {
                        errorList["Performance Reviews"] = ["Rating Date"];
                        return;
                    }

                    if (fieldName.indexOf("_") > 0 && fieldName != "rating_date")
                        fieldName = fieldName.substring(0, fieldName.indexOf("_"));



                    var actualField = getFieldByName(fieldName, currentTabName);




                    if (!actualField && item.$options && item.$options.tabValidationName) {
                        //"travel Details"
                        currentTabName = item.$options.tabValidationName;
                    }
                    if (!actualField || !actualField.container) {
                        //return;
                    }
                    if (actualField) {
                        if (item.$options.tabValidationName) {
                            currentTabName = getTabName(item.$options.tabValidationName);
                        } else {
                            currentTabName = getTabName(actualField.container);
                        }
                    }

                    if (actualField) {
                        var fieldArray = errorList[currentTabName];
                        var isFieldExist = _.find(fieldArray, function (field) { return (field === actualField.name || field === actualField.label) });

                        if (actualField.container !== "") {
                            if (!fieldArray) {
                                fieldArray = [];
                                errorList[currentTabName] = fieldArray;
                            }
                            if (!isFieldExist)
                                fieldArray.push((actualField.name !== '' && actualField.name !== "undefined" && actualField.name) ? actualField.name : actualField.label);
                        }
                    } else {
                        //_.find(self.otherContainer, )
                        var tab = _.find(configLayoutFactory.getPatientTab(), { id: currentTabName });

                        if (tab && tab.name) {
                            var field = getFieldByName(fieldName);
                            if (!errorList[tab.name] && typeof errorList[tab.name] !== 'Array')
                                errorList[tab.name] = [];
                            if (field && field.name)
                                errorList[tab.name].push(field.name);
                        } else {

                        }
                    }
                });
            });
            $scope.formErrorList = errorList;
        }


        self.onSave_Click = function (form) {
            var newId = 0;
            var tempYearsEmployed;

            systemFunctionsService.scrollToTopZero();

            function successful() {
                $scope.ignoreRedirect = true;
                self.callSubmit = false;
                console.info($state);
                if (modelId && modelId > 0) {
                    toaster.pop("success", "", "Patient updated successful");

                    modelId = self.model.id;
                    $state.go($state.current, {
                        currentTab: self.currentTab,
                        id: modelId,
                    }, { reload: true });
                } else {
                    toaster.pop("success", "", "Patient Added successful");
                    $state.go("patients", {}, { reload: true });
                    $scope.close();
                }
            }

            function openTabInErrorModel() {
                _.forIn(form.$error, function (arr) {
                    _.forEach(arr, function (item) {
                        self[item.$options.accordionOpenFieldName] = true;
                    });
                });
            }


            $scope.formErrorList = null;
            $scope.tempAvailability = {};

            //if (form.$invalid || form.$error) {
            if (!form.$valid) {
                openTabInErrorModel();
                prepareResourceFormErrorList(form);
                return;
            }

            //self.loadCorrect = true;
            self.callSubmit = true;
            if (counterFilesPromise !== 0) {
                toaster.pop("warning", "", "waiting file upload");
                return;
            }
            preSendChangeModel();
            var defer = $q.defer();

            if (modelId && modelId > 0) {
                self.loadCorrect = patientService.update({ id: modelId }, self.model, function (responce) {
                    self.model = responce;
                    defer.resolve(responce);
                }, function (error) { reject(); });
            } else {
                self.loadCorrect = patientService.save(self.model, function (responce) {
                    self.model = responce;
                    defer.resolve(responce);
                }, function (error) { reject(); });
            }

            defer.promise.then(function (responce) {
                newId = responce.id;
                var deferUploadAvatar = $q.defer();

                if (self.uploadFileAvatar) {
                    patientService.uploadAvatar(uploader, self.uploadFileAvatar, responce.id, function () {
                        deferUploadAvatar.resolve();
                    }, reject);
                } else deferUploadAvatar.resolve();

                self.loadCorrect = $q.all([deferUploadAvatar.promise]).then(function () {
                    successful();
                });

            });
        }; // Save Func ****

        self.stopRecording = function () {
            audioService.recordRTC.stopRecording(function (url) {
                audioService.recordingEndedCallback(url);
                stopStream();
            });
        }

        self.canUpdateOrInsertButtonDisabled = canUpdateOrInsertButtonDisabled;
        self.runRecord = function () {
            captureAudio(commonConfig);

            audioService.mediaCapturedCallback = function () {
                audioService.recordRTC = RecordRTC(audioService.stream, {
                    type: 'audio',
                    bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                    sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                    leftChannel: params.leftChannel || false,
                    disableLogs: params.disableLogs || false,
                    recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
                });

                audioService.recordingEndedCallback = function () {
                    if (!$scope.$$phase)
                        $scope.$apply(function () {
                            self.audioSrc = audioService.recordRTC.toURL();
                        });
                };

                audioService.recordRTC.startRecording();
            };
        };

        function setFormScope(form) {
            $scope.__form = form;
        }

        function getForm() {
            return $scope.__form.patientEditForm;
        }

        function getRestrictedFieldList(fieldName) {
            var fieldList = [];
            if (fieldName === "Salary") {
                fieldList.push({ id: "salary" });
                fieldList.push({ id: "bonusOrOtherPay" });
                return fieldList;
            } else if (fieldName === "Personal Info") {
                fieldList.push({ id: "martialStatus" });
                fieldList.push({ id: "ssn" });
                fieldList.push({ id: "lgbt" });
                fieldList.push({ id: "ethnicity" });
                fieldList.push({ id: "gender" });
                fieldList.push({ id: "emergencyContactName" });
                fieldList.push({ id: "emergencyContactPhoneNumber" });
                return fieldList;
            } else if (fieldName === "Background Checks") {
                fieldList.push({ id: "armedForcesCountry" });
                fieldList.push({ id: "armedForcesBranch" });
                fieldList.push({ id: "armedForces" });
                fieldList.push({ id: "drugTest" });
                fieldList.push({ id: "creditReportOk" });
                fieldList.push({ id: "motorVehicleReport" });
                fieldList.push({ id: "motorVehicleReportOther" });
                fieldList.push({ id: "professionalReferenceChecks" });
                fieldList.push({ id: "professionalReferenceChecksOther" });
                fieldList.push({ id: "credentialVerifications" });
                fieldList.push({ id: "credentialVerificationsOther" });
                fieldList.push({ id: "employmentEligibilityVerification" });
                fieldList.push({ id: "employmentEligibilityVerificationOther" });
                fieldList.push({ id: "formI9" });
                fieldList.push({ id: "formI9Other" });
                fieldList.push({ id: "formEVerify" });
                fieldList.push({ id: "formEVerifyOther" });
                fieldList.push({ id: "internationalWorkHistory" });
                fieldList.push({ id: "internationalWorkHistoryOther" });
                return fieldList;
            }
        }

        function hideFieldForNewPateint() {
            var hiddenFieldsForNewPateint = [{ id: "status" }, { id: "patientId" }, { id: "overview" }];
            _.forEach(self.generalPoropertyList, function (formField) {
                var field = _.find(hiddenFieldsForNewPateint, { id: formField.id });
                if (field && field.id) {
                    formField.visible = false;
                }
            });
        }

        function hideFieldForNewPateintV2(containers) {
            if (!containers)
                return;
            _.forEach(configLayoutFactory.getPatientsHiddenFieldsForNewModel(), function (fieldToHide) {
                var field = undefined;
                field = _.find(containers.container1, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container2, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container3, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container4, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container5, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }
                field = _.find(containers.container6, fieldToHide);
                if (field && field.id == fieldToHide.id) {
                    field.visible = false;
                    return;
                }

            });
        }

        function parceLayoutModel() {
            self.tabs = _.unionBy(initialValues[1].tabs, configLayoutFactory.getPatientTab(), "id");
            initialValues[1].containers = initialValues[1].containers || configLayoutFactory.getDefaultPatientContainer();
            self.patientDetails = _.find(initialValues[1].containers, { id: "patientDetails" });

            configLayoutFactory.addRequiredCustomField(initialValues[1].containers, self.model.customFieldValues, self.otherContainer);
            self.generalPoropertyList = configLayoutFactory.getPatientFields();

            // hide some fields if New Patient
            //hideFieldForNewPateint();
            if (!modelId)
                hideFieldForNewPateintV2(self.patientDetails);

            self.__ResourceEditCtrl = "ResourceEditCtrl";
        }

        self.isTabVisible = function isTabVisible(tabId) {
            var tab = _.find(self.tabs, { id: tabId });
            return (tab && tab.visible)
        }

        self.getTabDisplayName = function (tabId) {
            if (self.tabs && self.tabs.length > 0) {
                var tab = _.find(self.tabs, { id: tabId });
                if (tab && tab.name)
                    return $filter('translate')(tab.name);
            }
        }

        if (modelId) {
            modelId = parseInt(modelId);
            self.model = initialValues[2];
            self.titleText = self.model.name;
            if (!tempResourceModel)
                tempResourceModel = angular.copy(self.model);

            self.model.medicareCapOT = 500;
            self.model.remainingYTD = 1000;
            self.model.medicareCapPT_SLP = 2000;
            self.model.remainingYTD_PT_SLP = 1000;

        } else {
            self.model.customFieldValues = initialValues[0];
            self.titleText = "New Resource";
        }
        parceLayoutModel();

        self.patientHep_Condition = function () {
            return !(self.model.caseId > 0);
        }

        self.prevBirthDate = angular.copy(self.model.birthDate);

        $scope.$watch(function () { return self.model.birthDate }, function () {
            self.prevBirthDate = self.model.birthDate;
            if (self.model.birthDate) {
                self.model.age = moment().diff(self.model.birthDate, 'years');
            }
        })



        self.restrictPersonalInfo = permissionWorker.restrictPersonalInfo;
        self.restrictSalary = permissionWorker.restrictSalary;
        self.restrictBackgroundChecks = permissionWorker.restrictBackgroundChecks;
        self.restrictCriminalRecords = permissionWorker.restrictCriminalRecords;
        self.restrictPreviousEmployments = permissionWorker.restrictPreviousEmployments;

        creatorSearchFunction(self, "searchCountry", country);

        creatorSearchFunction(self, "country_Search", country);
        creatorSearchFunction(self, "state_Search", StateService);
        creatorSearchFunction(self, "city_Search", CityService);

        creatorSearchFunction(self, "country1_Search", country);
        creatorSearchFunction(self, "state1_Search", StateService);
        creatorSearchFunction(self, "city1_Search", CityService);
        creatorSearchFunction(self, "zip1_Search", ZipCodeService);

        creatorSearchFunction(self, "country2_Search", country);
        creatorSearchFunction(self, "state2_Search", StateService);
        creatorSearchFunction(self, "city2_Search", CityService);
        creatorSearchFunction(self, "zip2_Search", ZipCodeService);



        creatorSearchFunction(self, "language_Search", languageService);
        creatorSearchFunction(self, "clinic_Search", clinicService);
        creatorSearchFunction(self, "referralPhysician_Search", referralPhysicianService);
        //

        creatorSearchFunction(self, "clinician_Search", ClinicianService);
        //creatorSearchFunction(self, "clinician_Search", resourceService);
        //creatorSearchFunction(self, "assignedClinician_Search", resourceService, "", { assignedClinicianId: self.model.clinicianId });
        creatorSearchFunction(self, "clinicLocation_Search", clinicLocationService);



        // For Dynamic Dropdowns
        //dropdownConfigsFunction(self, "country_Search", country, "country");

        generateList();
        listModelInitialization();

        self.timeZoneInfo_Enum = initialValues[3];



        ////////////////////////////////////////////////////////////////////

        function canUpdateOrInsertButtonDisabled() {
            if (modelId > 0) {
                return permissionWorker.canPatientUpdate();
            }

            return permissionWorker.canPatientInsert();
        }   

        function intallFirefoxScreenCapturingExtension() {
            InstallTrigger.install({
                'Foo': {
                    // URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                    URL: 'https://addons.mozilla.org/firefox/downloads/file/355418/enable_screen_capturing_in_firefox-1.0.006-fx.xpi?src=cb-dl-hotness',
                    toString: function () {
                        return this.URL;
                    }
                }
            });
        }

        function stopStream() {
            if (audioService.stream && audioService.stream.stop) {
                audioService.stream.getTracks()[0].stop();
                audioService.stream = null;
            }
        }

        function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
            navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
        }

        function captureAudio(config) {
            captureUserMedia({ audio: true }, function (audioStream) {

                config.onMediaCaptured(audioStream);

                audioStream.onended = function () {
                    config.onMediaStopped();
                };
            }, function (error) {
                config.onMediaCapturingFailed(error);
            });
        }

        function employmentTypeIsSalary() {
            return self.model.employmentType === "Salary";
        }

        function getBlobFileToRecord() {
            var blob = recordRTC.getBlob();
            blob.name = "record.wav";
        }

        function reject() {
            self.callSubmit = false;
        }

        function generateList() {
            self.prefix_Enum = ["Ms", "Miss", "Mrs", "Mr", "Dr", "Atty", "Prof", "Hon", ];
            self.gender_Enum = ["Male", "Female"];
            self.prefferedContactMethod_Enum = [{ title: "Email", value: "Email" }, { title: "Phone", value: "Phone" }];
            self.relationshipToPatient_Enum = self.relationshipToContact_Enum = [
                {
                    title: "Daughter",
                    value: "Daughter"
                }, {
                    title: "Friend",
                    value: "Friend"
                }, {
                    title: "Parent",
                    value: "Parent"
                }, {
                    title: "Partner",
                    value: "Partner"
                }, {
                    title: "Son",
                    value: "Son"
                }, {
                    title: "Sibling",
                    value: "Sibling"
                }, {
                    title: "Spouse",
                    value: "Spouse"
                }, {
                    title: "Legal Gardian",
                    value: "LegalGardian"
                }
            ];
            self.maritialStatus_Enum = ["Single", "Married"];
            self.status_Enum = ["Active", "Inactive"];

            self.range = ["L", "M", "H"];
            self.suffix_Enum = ["Sr.", "Jr.", "M.D.", "Ph.D."];
            self.visaOrWorkPermitEnum = ["Visa", "Permit"];
            self.salaryPer = ["Hour", "Week", "Month", "Year"];
            self.resourceType_Enum = ["Internal", "External", "Consultant"];
            self.employmentType_Enum = [
            {
                title: "Exempt",
                value: "Salary"
            },
            {
                title: "Non-Exempt",
                value: "Hourly"
            }];
            self.yesNoOtherEnum = ["NA", "Yes", "No", "Other"];
            self.passFailENUM = ["Pass", "Fail"];
            self.ethnicity_Enum = [
                "Native American or Alaska Native",
                "Caucasian",
                "Hispanic",
                "Black or African American",
                "Asian",
                "Native Hawaiian or Pacific Islander",
                "Other"
            ];
            self.availabilityTimeRange = dateTimeHelper.dateTypeEnum();
        }

        function setResourceIdInList(item) {
            if (modelId)
                item.resourceId = modelId;
        }

        self.getNA = function (fieldName, propertyName) {
            if (propertyName) {
                return (self.model[fieldName] && self.model[fieldName][propertyName]) || "-";
            }
            return self.model[fieldName] || "-";
        }

        self.getSummaryFieldValue = function (field) {
            var value = "";
            if (typeof self.model[field.id] === "object" && self.model[field.id] && self.model[field.id].hasOwnProperty("name")) {
                value = self.model[field.id].name;
            } else {
                value = self.model[field.id];
            }
            if (!value) {
                return value;
            }

            var actualField = _.find(self.generalPoropertyList, { id: field.id });
            if (actualField && actualField.type) {
                switch (actualField.type) {
                    case "DATE_TIME":
                        value = self.dateFormat(new Date(value));
                        break;
                    case "PHONE":
                        if (value && value.length > 10)
                            value = value.substring(0, 10);

                        if (value && value.length > 6)
                            value = ["(", value.slice(0, 3), ") ", value.slice(3, 6), "-", value.slice(6)].join('');
                        break;
                    case "CHECKBOX":
                        if (field.id == "active") {
                            if (value == true || value == "true") {
                                value = "Yes";
                            } else {
                                value = "No";
                            }
                        } else {
                            if (value == true || value == "true") {
                                value = "True";
                            } else {
                                value = "False";
                            }
                        }
                        break;
                    default:
                        value = value;
                        break;
                }
            }
            return value;
        }

        self.getSliderClass = function (fieldName) {
            var value = self.model[fieldName];
            if (!value)
                return "0%";
            return (value * 10) + "%";
        }
        self.getPatientFullName = function getPatientFullName() {
            var fullName = "";
            if ((self.model && self.model.prefix)) {
                fullName += self.model.prefix + " ";
            }
            fullName += self.model.name;
            return fullName;
        }

        function preSendChangeModel() {
            if (self.model.zipCode === '' && self.model.zipCode.length == 0) {
                self.model.zipCode = null;
            }
            calculateYearsOfExperience();

            self.model.patientAddress1 = new Object;

            self.model.patientAddress1.address = self.model.add1;
            self.model.patientAddress1.patientId = self.model.id;
            self.model.patientAddress1.cityId = self.model.city1Id;
            self.model.patientAddress1.stateId = self.model.state1Id;
            self.model.patientAddress1.countryId = self.model.country1Id;
            self.model.patientAddress1.zipCode = self.model.zip1;
            self.model.patientAddress1.addressIsPrimary = self.model.address1IsPrimary;




            self.model.patientAddress2 = {};
            self.model.patientAddress2.address = self.model.add2;
            self.model.patientAddress2.patientId = self.model.id;
            self.model.patientAddress2.cityId = self.model.city2Id;
            self.model.patientAddress2.stateId = self.model.state2Id;
            self.model.patientAddress2.countryId = self.model.country2Id;
            self.model.patientAddress2.zipCode = self.model.zip2;
            self.model.patientAddress2.addressIsPrimary = self.model.address2IsPrimary;


        }


        function base64ToBlob(base64Data, contentType) {
            contentType = contentType || '';
            var sliceSize = 1024;
            var byteCharacters = atob(base64Data.split(',')[1]);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            return new Blob(byteArrays, { type: contentType });
        }

        self.setFormScope = setFormScope;
        creatorLeavePage($scope, getForm);

        $rootScope.$on("custom-file-upload-start", function () {
            counterFilesPromise++;
        });
        $rootScope.$on("custom-file-upload-end", function () {
            counterFilesPromise--;
        });

        function listModelInitialization() {

            self.address1ViewModel = {
                // transformModelPrePush: setPatientId,
                fields: [
                     {
                         label: "address",
                         fieldName: "address",
                         container: "patientAddress",
                         dataType: "text",
                         classContainer: "col-md-3",
                         required: true
                     }, {
                         label: "City",
                         fieldName: "city",
                         container: "patientAddress",
                         dataType: "angucomplete-alt",
                         classContainer: "col-md-3",
                         required: true,
                         initValue: "city",
                         search: self.searchSkills,
                     }, {
                         label: "state",
                         fieldName: "state",
                         container: "patientAddress",
                         dataType: "angucomplete-alt",
                         classContainer: "col-md-3",
                         required: true,
                         initValue: "state",
                         search: self.searchSkills,
                     }, {
                         label: "country",
                         fieldName: "country",
                         container: "patientAddress",
                         dataType: "angucomplete-alt",
                         classContainer: "col-md-3",
                         required: true,
                         initValue: "country",
                         search: self.searchSkills,
                     },
                ],
            };

            self.patientCasesViewModel = {
                fields: [
                {
                    fieldName: "caseId",
                    label: "Case ID",
                    dataType: "date-time",
                    inputClass: "from-date",
                    classContainer: "col-md-3"
                },
                {
                    fieldName: "maxAvailabilityPercentage",
                    label: "Availability %",
                    dataType: "number",
                    inputClass: "availability",
                    classContainer: "col-md-3"
                },
                {
                    fieldName: "created",
                    label: "Post Date",
                    dataType: "date-time",
                    inputClass: "created",
                    classContainer: "col-md-3"
                },
                {
                    fieldName: "changedBy",
                    label: "Changed By",
                    dataType: "text",
                    inputClass: "changed-by",
                    classContainer: "col-md-3"
                }
                ]
            };
        }

        self.saveMobileAccess = function () {
            if (self.model.patientUserInfo.userName === '' && self.model.patientUserInfo.userName.length == 0) {
                toaster.pop("error", "", "Username is required");
                return;
            }
            else if (self.model.patientUserInfo.password === '' && self.model.patientUserInfo.password.length == 0) {
                toaster.pop("error", "", "Password is required");
                return;
            }
            var obj = {
                id: self.model.patientUserInfo.id,
                patientId: self.model.id,
                username: self.model.patientUserInfo.userName,
                password: self.model.patientUserInfo.password
            }

            self.loadCorrect = patientService.update({ id: modelId, controller: "mobileAccess" }, obj, function (result) {
                toaster.pop("success", "", "Mobile Access is Created Successfully.");
            }, function (error) {
                toaster.pop("error", "", "Unable to create Mobile access, please try again later.");
            });
        }



        self.onAddPatientCancelClick = function () {
            self.onCancelButtonClick();
            $scope.close();
        }

        $scope.close = function () {
            $uibModalInstance.dismiss('');
        }

    }

})();
