namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changesInPatientMobileProfileTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.patient_mobile_profiles", "city_id", "dbo.Cities");
            DropForeignKey("dbo.patient_mobile_profiles", "country_id", "dbo.countries");
            DropForeignKey("dbo.patient_mobile_profiles", "language_id", "dbo.languages");
            DropForeignKey("dbo.patient_mobile_profiles", "state_id", "dbo.States");
            DropIndex("dbo.patient_mobile_profiles", new[] { "language_id" });
            DropIndex("dbo.patient_mobile_profiles", new[] { "city_id" });
            DropIndex("dbo.patient_mobile_profiles", new[] { "state_id" });
            DropIndex("dbo.patient_mobile_profiles", new[] { "country_id" });
            AddColumn("dbo.patient_mobile_profiles", "language", c => c.String());
            AddColumn("dbo.patient_mobile_profiles", "city", c => c.String());
            AddColumn("dbo.patient_mobile_profiles", "state", c => c.String());
            AddColumn("dbo.patient_mobile_profiles", "country", c => c.String());
            DropColumn("dbo.patient_mobile_profiles", "language_id");
            DropColumn("dbo.patient_mobile_profiles", "city_id");
            DropColumn("dbo.patient_mobile_profiles", "state_id");
            DropColumn("dbo.patient_mobile_profiles", "country_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.patient_mobile_profiles", "country_id", c => c.Long());
            AddColumn("dbo.patient_mobile_profiles", "state_id", c => c.Long());
            AddColumn("dbo.patient_mobile_profiles", "city_id", c => c.Long());
            AddColumn("dbo.patient_mobile_profiles", "language_id", c => c.Long());
            DropColumn("dbo.patient_mobile_profiles", "country");
            DropColumn("dbo.patient_mobile_profiles", "state");
            DropColumn("dbo.patient_mobile_profiles", "city");
            DropColumn("dbo.patient_mobile_profiles", "language");
            CreateIndex("dbo.patient_mobile_profiles", "country_id");
            CreateIndex("dbo.patient_mobile_profiles", "state_id");
            CreateIndex("dbo.patient_mobile_profiles", "city_id");
            CreateIndex("dbo.patient_mobile_profiles", "language_id");
            AddForeignKey("dbo.patient_mobile_profiles", "state_id", "dbo.States", "Id");
            AddForeignKey("dbo.patient_mobile_profiles", "language_id", "dbo.languages", "Id");
            AddForeignKey("dbo.patient_mobile_profiles", "country_id", "dbo.countries", "Id");
            AddForeignKey("dbo.patient_mobile_profiles", "city_id", "dbo.Cities", "Id");
        }
    }
}
