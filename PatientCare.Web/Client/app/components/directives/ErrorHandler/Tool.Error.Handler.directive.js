﻿"use strict";
var app = angular.module("EDZoutstaffingPortalApp");
app.directive("appFormErrorHandler", [
    "uuid2",
    "toaster",
    AppFormErrorHandlerFunc
]);

function AppFormErrorHandlerFunc(uuid2, toaster) {
    return {
        templateUrl: "Client/app/components/directives/ErrorHandler/App-Form-Error-Handler-View.html",
        restrict: "E",
        replace: false,
        scope: {
            _form: "=ngForm",
            _controller: "=ngCtrl",
            _module: "=ngModule",

        },
        link: function (scope) {
            var self = this;
            toaster.pop("warning", "", "App Form Error Handler InIt.");
        }
    };
}