﻿namespace PatientCare.Core.Services.RoleSetupService
{
    public enum Section
    {
        None = 0,
        Schedule = 1,
        Patients = 2,
        Insurance = 3,
        Library = 4,
        Resources = 5,
        Referral = 6,
        Communications = 7,
        Billing = 8,
        Dashboards = 9
    }
}
