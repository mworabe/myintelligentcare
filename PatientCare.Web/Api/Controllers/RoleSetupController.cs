﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PatientCare.Core.Entities;
using PatientCare.Core.Enums;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.DashboardSetups;
using PatientCare.Web.Api.Models.Resources;
using PatientCare.Web.Api.Models.RoleSetup;
using PatientCare.Web.Controllers;
using PatientCare.Web.Services.Convertation;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Role Setup operations.
    /// </summary>
    [Authorize(Roles = ApplicationUser.AdminRole)]
    //for the Role authorization testing and debugging
    //[Authorize(Roles = ApplicationUser.AdminRole + "," + ApplicationUser.AdministratorRole)]
    public class RoleSetupController : AuthBaseApiController
    {
        /// <summary>
        /// Description : Role Setup IRoleConfigureLayoutRepository.
        /// </summary>
        private readonly IRoleConfigureLayoutRepository _configureLayoutRepository;
        /// <summary>
        /// Description : Role Setup IPicklistRepository DashboardSetup.
        /// </summary>
        private readonly IPicklistRepository<DashboardSetup> _dashboardSetupRepository;

        /// <summary>
        /// Description : Initializes a new instance of the Role Setup Class.     
        /// </summary>
        /// <param name="configureLayoutRepository">IRoleConfigure Layout Repository</param>
        /// <param name="dashboardSetupRepository">IDashboard Setup Repository</param>
        public RoleSetupController
            (
                IRoleConfigureLayoutRepository configureLayoutRepository,
                IPicklistRepository<DashboardSetup> dashboardSetupRepository
            )
        {
            _configureLayoutRepository = configureLayoutRepository;
            _dashboardSetupRepository = dashboardSetupRepository;
        }

        private static Expression<Func<ApplicationRole, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return role => role.Name.Contains(searchPhrase)
                     || role.Description.Contains(searchPhrase);
        }

        private static Uri GetEntityLocation(string id)
        {
            return new Uri("/RoleSetup/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Role Setup.  
        /// </summary>
        /// <param name="request">List Request</param>
        /// <returns>Role Setup Response Model</returns>
        [HttpGet]
        [ResponseType(typeof(IList<RoleSetupResponse>))]
        public IHttpActionResult GetRoleSetup([FromUri]ListRequest request)
        {
            if (request.SortField == "isActiveYesNo")
            {
                request.SortField = "isActive";
            }

            if (string.IsNullOrEmpty(request.SortField))
            {
                request.SortField = "name";
            }
            var query = PagingExtensions<ApplicationRole>.CreatePagedQuery
                        (request,
                                string.IsNullOrWhiteSpace(request.SearchPhrase) ?
                                 null : GetSearchColumnExpressions(request.SearchPhrase)
                        );


            //exclude Admin role
            //dashboard Related modification for this reason comment the code and add new code
            //query.AddFilter(x => x.Name != ApplicationUser.AdminRole);
            //start here
            var userId = User.Identity.GetUserId();
            var user = AppUserManager.Users.FirstOrDefault(x => x.Id == userId);
            if (user == null)
            {
                return NotFound();
            }
            var roleName = AppUserManager.GetRoles(userId).FirstOrDefault();

            if (roleName != ApplicationUser.AdminRole)
            {
                //exclude Admin role
                query.AddFilter(x => x.Name != ApplicationUser.AdminRole);
            }
            //end here

            //Hide Patient Role
            query.AddFilter(x => x.Name != ApplicationUser.PatientRole);

            var readOptions = new ReadOptions<ApplicationRole>().AsReadOnly();

            var page = AppRoleManager.PagedList(query, readOptions);

            var pageData = page.Entities.ToArray();

            //need replace to picklist
            var dtos = Mapper.Map<ApplicationRole[], RoleSetupResponse[]>(page.Entities.ToArray());
            if (dtos.Count() > 0)
            {
                foreach (var item in dtos.Where(x => x.Name == ApplicationUser.AdminRole || x.Name == ApplicationUser.TherapistRole || x.Name == ApplicationUser.FrontOfficeRole))
                {
                    item.IsDeleted = false;
                }
            }
            var response = new ListResponse<RoleSetupResponse>(dtos, page.TotalCount);
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Get Role Setup by Id.  
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Role Setup Response Model</returns>
        [HttpGet]
        [ResponseType(typeof(RoleSetupResponse))]
        public async Task<IHttpActionResult> GetRoleSetup(string id)
        {
            var role = await AppRoleManager.FindByIdAsync(id);

            //dashboard Related modification fo rthis reason comment the code and add new code
            //if (role == null || role.Name == ApplicationUser.AdminRole)
            //{
            //    return NotFound();
            //}
            //start here
            var userId = User.Identity.GetUserId();
            var user = AppUserManager.Users.FirstOrDefault(x => x.Id == userId);
            if (user == null)
            {
                return NotFound();
            }
            var roleName = AppUserManager.GetRoles(userId).FirstOrDefault();

            if (roleName != ApplicationUser.AdminRole)
            {
                return NotFound();
            }
            //end here

            var dtos = Mapper.Map<ApplicationRole, RoleSetupResponse>(role);
            dtos.AccessRights = ((dtos.AccessRights == null) ? new AccessRights() : dtos.AccessRights);
            dtos.AccessRights.MainHomePage = ((dtos.AccessRights == null) || (dtos.AccessRights.MainHomePage == null)) ? new HomePageDynamic() : dtos.AccessRights.MainHomePage;
            return Ok(dtos);
        }

        /// <summary>
        /// Method Description : Using for Create New Role Setup.   
        /// </summary>
        /// <param name="roleSetupRequest">Role Setup Request Model</param>
        /// <returns>Role Setup Response Model</returns>
        [HttpPost]
        [SwaggerResponse(201, "Created", typeof(ResourceResponse))]
        public async Task<IHttpActionResult> CreateRoleSetup([FromBody]RoleSetupRequest roleSetupRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var role = Mapper.Map<RoleSetupRequest, ApplicationRole>(roleSetupRequest);
            var result = await AppRoleManager.CreateAsync(role);
            return !result.Succeeded ? GetErrorResult(result) : Created(GetEntityLocation(role.Id), Mapper.Map<ApplicationRole, RoleSetupResponse>(role));
        }

        /// <summary>
        /// Method Description : Using for Update Role Setup.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="roleSetupRequest">Role Setup Request Model</param>
        /// <returns>Role Setup Response Model</returns>
        [HttpPut]
        public async Task<IHttpActionResult> EditRoleSetup(string id, [FromBody] RoleSetupRequest roleSetupRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var role = await AppRoleManager.FindByIdAsync(id);

            if (role.Name == ApplicationUser.AdminRole && roleSetupRequest.Name != ApplicationUser.AdminRole)
            {
                ModelState.AddModelError("Role", "You can not modify name of 'Admin' role.");
                return BadRequest(ModelState);
            }

            role.IsActive = roleSetupRequest.IsActive;
            role.Description = roleSetupRequest.Description;
            role.Name = roleSetupRequest.Name;
            role.DashboardSetupId = roleSetupRequest.DashboardSetupId;

            role.JsonSetup = roleSetupRequest.AccessRights == null ? null : JsonConverterHelper.SerializeObject(roleSetupRequest.AccessRights);

            role.IsActive = roleSetupRequest.IsActive;

            var result = await AppRoleManager.UpdateAsync(role);
            return !result.Succeeded ? GetErrorResult(result) : Ok(Mapper.Map<ApplicationRole, RoleSetupResponse>(role));
        }

        /// <summary>
        /// Method Description : Using for Change Default Layout.   
        /// </summary>
        /// <param name="request">Config Layout Request Model</param>
        /// <returns>Role Setup Response Model</returns>
        [HttpPost]
        [Route("api/RoleSetup/change-default-layout")]
        public async Task<IHttpActionResult> ChangeDefaultLayout([FromBody] ConfigLayoutRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var query = new FilterQuery<RoleConfigureLayout>().AddFilter(f => f.RoleId == null).AddFilter(f => f.Type == request.Type);

            var configureLayout = await _configureLayoutRepository.FirstOrDefaultAsync(query) ?? new RoleConfigureLayout();

            configureLayout.RoleId = null;
            configureLayout.Type = request.Type;
            configureLayout.Configure = JsonConverterHelper.SerializeObject(request.ConfigLayout);
            if (configureLayout.Id == 0)
            {
                await _configureLayoutRepository.CreateAsync(configureLayout);
            }
            else
            {
                await _configureLayoutRepository.UpdateAsync(configureLayout);
            }

            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Change Role Layout.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="request">Config Layout Request Model</param>
        [HttpPost]
        [Route("api/RoleSetup/{id}/change-role-layout")]
        public async Task<IHttpActionResult> ChangeRoleLayout(string id, [FromBody] ConfigLayoutRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var query = new FilterQuery<RoleConfigureLayout>().AddFilter(f => f.RoleId == id).AddFilter(f => f.Type == request.Type);

            var configureLayout = await _configureLayoutRepository.FirstOrDefaultAsync(query) ?? new RoleConfigureLayout();

            configureLayout.RoleId = id;
            configureLayout.Type = request.Type;
            configureLayout.Configure = JsonConverterHelper.SerializeObject(request.ConfigLayout);
            if (configureLayout.Id == 0)
                await _configureLayoutRepository.CreateAsync(configureLayout);
            else
                await _configureLayoutRepository.UpdateAsync(configureLayout);

            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Get Default Resource Layout.  
        /// </summary>
        [HttpGet]
        [Route("api/RoleSetup/resource-layout-config-default")]
        public async Task<IHttpActionResult> ResourceLayoutDefault()
        {
            var configureLayout = await _configureLayoutRepository.FirstOrDefaultAsync(new FilterQuery<RoleConfigureLayout>()
                .AddFilter(x => x.RoleId == null).AddFilter(x => x.Type == LayoutTypeEnum.Resource));

            var converter = configureLayout == null ? new ConfigLayout() : JsonConvert.DeserializeObject<ConfigLayout>(configureLayout.Configure);

            return Ok(converter);
        }

        /// <summary>
        /// Method Description : Using for Get Resource Layout For Role by Id.  
        /// </summary>
        /// <param name="id">Id</param>
        [HttpGet]
        [Route("api/RoleSetup/{id}/resource-layout-config-for-role")]
        public async Task<IHttpActionResult> ResourceLayoutForRole(string id)
        {
            var configureLayout = await _configureLayoutRepository.FirstOrDefaultAsync(new FilterQuery<RoleConfigureLayout>()
                .AddFilter(x => x.RoleId == id).AddFilter(x => x.Type == LayoutTypeEnum.Resource));

            var converter = configureLayout == null ? new ConfigLayout() : JsonConvert.DeserializeObject<ConfigLayout>(configureLayout.Configure);

            return Ok(converter);
        }

        /// <summary>
        /// Method Description : Using for Get Resource Layout.  
        /// </summary>
        [HttpGet]
        [Route("api/RoleSetup/resource-config-layout")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ResourceRoleLayout()
        {
            //var claims = ((ClaimsIdentity) User.Identity).Claims.ToList();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleNameClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);

            if (roleNameClaim == null)
            {
                return Unauthorized();
            }

            var roleName = roleNameClaim.Value;
            var role = await AppRoleManager.FindByNameAsync(roleName);

            if (role == null)
            {
                return Unauthorized();
            }

            var configureLayout = await _configureLayoutRepository.ListAsync(new FilterQuery<RoleConfigureLayout>()
                .AddFilter(x => x.RoleId == role.Id || x.RoleId == null).AddFilter(x => x.Type == LayoutTypeEnum.Resource));

            var layout = configureLayout.FirstOrDefault(x => x.RoleId == role.Id) ?? configureLayout.FirstOrDefault(x => x.RoleId == null);

            var converter = layout == null ? new ConfigLayout() : JsonConvert.DeserializeObject<ConfigLayout>(layout.Configure);

            return Ok(converter);
        }

        /// <summary>
        /// Method Description : Using for Delete Role by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteRole(string id)
        {
            var existedRole = await AppRoleManager.FindByIdAsync(id);
            if (existedRole == null
                || existedRole.Name == ApplicationUser.AdminRole
                || existedRole.Name == ApplicationUser.FrontOfficeRole
                || existedRole.Name == ApplicationUser.TherapistRole)
            {
                return NotFound();
            }
            await AppRoleManager.DeleteAsync(existedRole);
            return Ok();
        }

        /// <summary>
        /// Method Description : Using for Get Crud List.   
        /// </summary>
        [HttpGet]
        [Route("api/RoleSetup/crud-list")]
        public IHttpActionResult GetCrudList()
        {
            return Ok(GetEnumListFrom(typeof(Crud)));
        }

        /// <summary>
        /// Method Description : Using for Get Section List.   
        /// </summary>
        [HttpGet]
        [Route("api/RoleSetup/section-list")]
        public IHttpActionResult GetSectionList()
        {
            return Ok(GetEnumListFrom(typeof(Section)));
        }

        /// <summary>
        /// Method Description : Using for Get Home Page List.   
        /// </summary>
        [HttpGet]
        [Route("api/RoleSetup/homepage-list")]
        public IHttpActionResult GetHomePageList()
        {
            return Ok(GetEnumListFrom(typeof(HomePage)));
        }

        /// <summary>
        /// Method Description : Using for Get Pick List.   
        /// </summary>
        [HttpGet]
        [Route("api/RoleSetup/picklist-list")]
        public IHttpActionResult GetPicklistList()
        {
            return Ok(GetEnumListFrom(typeof(Picklist)));
        }

        /// <summary>
        /// Method Description : Using for Get Restrict Access To List.   
        /// </summary>
        [HttpGet]
        [Route("api/RoleSetup/restrict-access-to-list")]
        public IHttpActionResult GetRestrictAccessToList()
        {
            return Ok(GetEnumListFrom(typeof(RestrictAccessTo)));
        }

        /// <summary>
        /// Method Description : Using for Get Role Setup by User.   
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>Role Setup Response Model</returns>
        [HttpGet]
        [ResponseType(typeof(RoleSetupResponse))]
        [Route("api/RoleSetup/ByUser/{userId}")]
        public IHttpActionResult GetRoleSetupByUser(string userId)
        {
            var roleName = AppUserManager.GetRoles(userId).FirstOrDefault();

            if (string.IsNullOrEmpty(roleName))
            {
                return NotFound();
            }

            var existedRole = AppRoleManager.FindByName(roleName);

            if (existedRole == null)
            {
                return NotFound();
            }

            var roleSetupResponse = new RoleSetupResponse
            {
                IsActive = existedRole.IsActive ?? true,
                IsActiveYesNo = (existedRole.IsActive ?? true) ? "Yes" : "No",
                Description = existedRole.Description,
                AccessRights = string.IsNullOrEmpty(existedRole.JsonSetup) ? null : JsonConvert.DeserializeObject<AccessRights>(existedRole.JsonSetup)
            };

            return Ok(roleSetupResponse);
        }

        /// <summary>
        /// Method Description : Using for Get Dashboards List.   
        /// </summary>
        /// <param name="request">List Request</param>
        /// <returns>Dashboard Setup Response Model</returns> 
        [ResponseType(typeof(IList<DashboardSetupResponse>))]
        [HttpGet]
        [Route("api/RoleSetup/dashboard-list")]
        public IHttpActionResult GetdDashboards([FromUri]ListRequest request)
        {
            request.SortField = "name";
            request.SearchField = "name"; // hard-coded field for searching in picklists
            var query = PagingExtensions<DashboardSetup>.CreatePagedQuery(request);
            var page = _dashboardSetupRepository.PagedList(query);
            var dtos = Mapper.Map<DashboardSetup[], DashboardSetupResponse[]>(page.Entities.ToArray());
            var response = new ListResponse<DashboardSetupResponse>(dtos, page.TotalCount);
            return Ok(response);
        }

        private static IList<object> GetEnumListFrom(Type enumType)
        {
            var enumVals = new List<object>();
            foreach (var item in Enum.GetValues(enumType))
            {
                enumVals.Add(new
                {
                    id = (int)item,
                    name = item.ToString()
                });
            }
            return enumVals;
        }
    }
}