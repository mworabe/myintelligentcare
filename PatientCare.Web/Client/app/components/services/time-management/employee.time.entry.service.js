﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("employeeEntryTypeService", ["$resource", function ($resource) {
      var resource = $resource("api/employee-entry-type/:id/:controller", null,
          {
              'update': { method: "PUT" },
              'query': { method: "GET", params: {}, isArray: false },
          });

      return resource;
  }]);