﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("AddAppoinmentCtrl", ["patientService", "$uibModal", "toaster", "$scope",
        "$uibModalInstance", "data", "DYNAMIC_FORM_CONSTROLLER", "CreatorSearchFunction", "ClinicService", "AppointmentTypeService", 'AppointmentService', 'ClinicianService', 'AppointmentFactory', 'LocationService', 'patientCaseService','ClinicLocationService',
        function (patientService, $uibModal, toaster, $scope,
            $uibModalInstance, data, DYNAMIC_FORM_CONSTROLLER, creatorSearchFunction, clinicService, appointmentTypeService, AppointmentService, ClinicianService, AppointmentFactory, LocationService, patientCaseService, ClinicLocationService) {

            console.info("Appointment Model");
            console.info(data);

            var self = this;
            self.model = data.appointmentModel || {};


            if (data.clinician) {
                self.model.clinician = data.clinician
                self.model.clinicianId = data.clinician.id;
            }

            
            
            self.initDate = function (date) {
                self.model.date = date;
                self.model.startTime = date;
                self.model.endTime = moment(date).add(30, 'm');
            }

            if (data && data.scheduleDate) {
                self.initDate(data.scheduleDate)
            } else if (self.model && self.model.startDate) {
                self.initDate(new Date(self.model.startDate))
            } else {
                self.initDate(new Date())
            }

            self.autocompleteMinLength = 0;

            self.modalUniqId = data.modalId;
            self.isModalMinimized = false;


            creatorSearchFunction(self, "patientName_Search", patientService);
            creatorSearchFunction(self, "clinician_Search", ClinicianService);
            creatorSearchFunction(self, "appointmentType_Search", appointmentTypeService);
            creatorSearchFunction(self, "facility_Search", clinicService);
            creatorSearchFunction(self, "clinicLocation_Search", LocationService);

            self.modalTitle = "Appoinment Detail"
            self.onModalCloseButtonClick = function onModalCloseButtonClick() {

                $uibModalInstance.dismiss();
            }

            if (!self.modalUniqId)
                self.onModalCloseButtonClick();
              self.generalPoropertyList = self.formFields = [{
                id: "patientName",
                name: "Patient Name",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
            }, {
                id: "date",
                name: "Date ",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                columClass: "col-md-6",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
            }, {
                id: "startTime",
                name: "Start Time ",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                columClass: "col-md-3",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TIME,
            }, {
                id: "endTime",
                name: "End Time ",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                columClass: "col-md-3",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TIME,
            }, {
                id: "clinician",
                name: "Clinician",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
            }, {
                id: "appointmentType",
                name: "Appointment Type",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
            }, {
                id: "facility",
                name: "Clinic Name",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
            }, {
                id: "clinicLocation",
                name: "Clinic Location",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: true,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
            }, {
                id: "additionalDetail",
                name: "Additional Detail",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                rows: 5,
                required: false,
                visible: true,
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXTAREA,
            }, {
                id: "recurrence",
                name: "Recurrence",
                container: "details",
                position: "1",
                pattern: ".{0,255}",
                required: false,
                visible: false,
                specialName: "title",
                specialValue: "value",
                type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
            }, ];

            self.recurrence_Enum = [{
                title: "Does Not Repeat", value: "noRep"
            }, {
                title: "Daily", value: "daily"
            }, {
                title: "Week Day (Mon - Fri) ", value: "weekDay"
            }, {
                title: "Weekly", value: "weekly"
            }, {
                title: "Monthly", value: "monthly",
            }, {
                title: "Yearly", value: "yearly"
            }, ];

            function validateForm(form) {

            }

            function preSaveChanges() {
                var request = self.model;
                var date = angular.copy(self.model.date);
                request.startDate = moment(date)
                                .hour(moment(self.model.startTime).hours())
                                .minutes(moment(self.model.startTime).minutes())
                                .utc();

                request.endDate = moment(date)
                    .hour(moment(self.model.endTime).hours())
                    .minutes(moment(self.model.endTime).minutes())
                    .utc();

                request.AppointmentStatus = AppointmentFactory.getStatusFromText("Confirmed").id;
                return request;
            }


            self.onFormSubmit = function onFormSubmit(form) {
                if (form.$invalid) {
                    toaster.pop("error", "", "Invalid Data, Please Check Error Summary and Fix them.");
                    return;
                }
                validateForm(form);
                var model = preSaveChanges();
                if (model.id && model.id > 0)
                    AppointmentService.update({ id: model.id }, model, function (result) {
                        if (result && result.id > 0) {
                            toaster.pop("success", "", "Appointment Updated Successfully.");
                            $uibModalInstance.close(result);
                        }
                    }, function (error) { });
                else
                    AppointmentService.save(model, function (result) {
                        if (result && result.id > 0) {
                            toaster.pop("success", "", "Appointment added Successfully.");
                            $uibModalInstance.close(result);
                        }
                    }, function (error) { });

            }

            self.injuryRegionList;
            self.cliniciansServices;
            self.clinicsServices;
            self.clinicLocations;
            self.patientss;
            self.patientName_Selected = function (selected) {
                self.model.patientId = (selected && selected.originalObject) ? selected.originalObject.id : null;
                self.model.CaseId = '';  
                patientCaseService.alll({ search: self.model.patientId }, function (response) {
                    self.cassss = response;
                    _.forEach(self.cassss, function (item) {
                        if (item.caseStatus == 'Inprogress')
                        {
                            self.model.CaseId = item.id;

                        }
                    });
                });                                  
            }

            self.appointmentType_Selected = function (selected) {
                self.model.appointmentTypeId = (selected && selected.originalObject) ? selected.originalObject.id : null;
            }

            self.facility_Selected = function (selected) {
                self.model.facilityId = (selected && selected.originalObject) ? selected.originalObject.id : null;               
                //self.model.clinicianId = '';
                //self.model.clinicLocationId = '';
                ClinicLocationService.alll({ search: self.model.facilityId }, function (response) { self.clinicLocations = response; });               
            }

            self.clinicLocations_Selected = function ()
            {

               ClinicianService.alll({ search: self.model.clinicLocationId }, function (response) { self.cliniciansServices = response; });
            }

            if (self.model.facilityId) {
                ClinicianService.alll({ search: self.model.clinicLocationId }, function (response) { self.clinicLocations = response; });
            }
            if (self.model.clinicLocationId) {
                ClinicianService.alll({ search: self.model.clinicLocationId }, function (response) { self.cliniciansServices = response; });
            }
        }
    ]);
