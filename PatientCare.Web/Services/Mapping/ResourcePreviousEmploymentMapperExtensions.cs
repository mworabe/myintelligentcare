﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourcePreviousEmployments;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourcePreviousEmploymentMapperExtensions
    {
        public static EmitMapperMapperProvider ResourcePreviousEmploymentRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePreviousEmployment, ResourcePreviousEmploymentResponse>(new ExtendedMapConfig<ResourcePreviousEmployment, ResourcePreviousEmploymentResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePreviousEmployment[], ResourcePreviousEmploymentResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePreviousEmploymentRequest, ResourcePreviousEmployment>(new ExtendedMapConfig<ResourcePreviousEmploymentRequest, ResourcePreviousEmployment>()));

            return mapperProvider;
        }
    }
}