﻿namespace PatientCare.Web.Api.Models.MasterTemplateExercises
{
    public class MasterTemplateExerciseRequest
    {
        public long Id { get; set; }

        public long? ExerciseId { get; set; }

        public long? MasterTemplateId { get; set; }

        public int? ExerciseOrder { get; set; }

    }   
}