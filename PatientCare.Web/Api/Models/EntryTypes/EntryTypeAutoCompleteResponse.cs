﻿using PatientCare.Core.Enums;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.EntryTypes
{
    [DataContract]
    public class EntryTypeAutoCompleteResponse 
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public EntryPeriodEnum? EntryPeriod { get; set; }

        [DataMember]
        public DayItem Days { get; set; }

        [DataMember]
        public long? AdvancedCreatePeriod { get; set; }

    }
}