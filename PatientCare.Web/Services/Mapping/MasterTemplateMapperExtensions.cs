﻿using System;
using EmitMapper;
using PatientCare.Services.Mapping;
using System.Linq;
using PatientCare.Core.Entities;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.MasterTemplates;
using PatientCare.Web.Api.Models.MasterTemplateExercises;
using EmitMapper.MappingConfiguration;

namespace PatientCare.Web.Services.Mapping
{
    public static class MasterTemplateMapperExtensions
    {
        public static EmitMapperMapperProvider MasterTemplateRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<MasterTemplate, MasterTemplateResponse>
                    (new ExtendedMapConfig<MasterTemplate, MasterTemplateResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<MasterTemplate[], MasterTemplateResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<MasterTemplate, MasterTemplateListResponse>
                    (new ExtendedMapConfig<MasterTemplate, MasterTemplateListResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<MasterTemplate[], MasterTemplateListResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<MasterTemplate, PicklistResponse>
                    (new ExtendedMapConfig<MasterTemplate, PicklistResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<MasterTemplate, PicklistDefaultResponse>
                    (new ExtendedMapConfig<MasterTemplate, PicklistDefaultResponse>()));

            var masterTemplateExerciseMapper =
                           ObjectMapperManager.DefaultInstance.GetMapper<MasterTemplateExerciseRequest, MasterTemplateExercise>(
                               new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<MasterTemplateRequest, MasterTemplate>(
                    new ExtendedMapConfig<MasterTemplateRequest, MasterTemplate>()
                    .ForMember(x => x.TemplateExercises, y => y.TemplateExercises != null ? y.TemplateExercises.Select(i => masterTemplateExerciseMapper.Map(i)).ToList() : null)
                  ));

            //mapperProvider.RegisterMapper(() =>
            //  ObjectMapperManager.DefaultInstance.GetMapper<MasterTemplateRequest, MasterTemplate>(
            //      new ExtendedMapConfig<MasterTemplateRequest, MasterTemplate>()));

            return mapperProvider;
        }
    }
}