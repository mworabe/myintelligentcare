﻿using System;
using PatientCare.Core.Mapping;
using EmitMapper;

namespace PatientCare.Services.Mapping
{
    public class EmitMapperMapper<TS,TD> : IMapper<TS,TD>
    {
        private readonly ObjectsMapper<TS, TD> _objectsMapper;

        public EmitMapperMapper(ObjectsMapper<TS, TD> objectsMapper)
        {
            if (objectsMapper == null) throw new ArgumentNullException("objectsMapper");
            _objectsMapper = objectsMapper;
        }

        public TD Map(TS source)
        {
            return _objectsMapper.Map(source);
        }
    }
}
