﻿using PatientCare.Web.ApiMobile.Models.HepDetails;

namespace PatientCare.Web.ApiMobile.Models.HepGroups
{
    public class HepGroupMobileRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long? PatientId { get; set; }

        public long? PatientCaseId { get; set; }

        public long? HepId { get; set; }

        public int? ExerciseOrder { get; set; }

        public HepDetailsMobileRequest[] HepDetails { get; set; }
    }   
}