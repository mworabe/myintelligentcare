﻿"use strict";

angular.module('EDZoutstaffingPortalApp')
    .directive('clock', function () {
        return {
            template: "<span>{{currentTime|date:formate}}</span>",
            restrict: 'EA',
            scope: {
                formate: '@',
            },
            controller: ['$scope', '$timeout', function ($scope, $timeout) {
                $scope.formate = "hh:mm a";
                $scope.currentTime = new Date();
                var updateTime = function () {
                    $scope.currentTime = new Date();
                    $timeout(updateTime, 1000);
                }
                $timeout(updateTime, 1000);

            }]

        }

    });



