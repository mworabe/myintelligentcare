﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ClinicLocationRepository : RepositoryBase<ClinicLocation>, IClinicLocationRepository
    {
        public ClinicLocationRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
