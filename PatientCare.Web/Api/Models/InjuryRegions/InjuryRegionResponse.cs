﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.InjuryRegions
{
    [DataContract]
    public class InjuryRegionResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}