﻿using System;
using System.Collections.Generic;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Services.ResourceScoreCalculation
{
    public interface IResourceScoreService
    {
        IList<Tuple<Resource, double, long>> Calculate(ScoreCalculationFilter filter, int pageIndex, int pageSize);
    }
}
