﻿using PatientCare.Core.Enums;
using System;

namespace PatientCare.Web.Api.Models.PatientAppointment
{
    public class PatientAppointmentRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public long? CaseId { get; set; }        

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long? ClinicLocationId { get; set; }

        public long? AppointmentTypeId { get; set; }

        public long? ClinicianId { get; set; }

        public long? FacilityId { get; set; }

        public string AdditionalDetail { get; set; }

        public AppointmentStatusEnum? AppointmentStatus { get; set; }

        public bool? IsMobile { get; set; }

        public bool? IsReschedule { get; set; }

    }
}