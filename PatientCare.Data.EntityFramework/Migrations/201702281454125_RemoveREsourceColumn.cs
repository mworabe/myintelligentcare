namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveREsourceColumn : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.resources", "resource_company_id", "dbo.resource_companies");
            DropIndex("dbo.resources", new[] { "resource_company_id" });
            DropColumn("dbo.resources", "resource_company_id");
            DropColumn("dbo.resources", "resource_type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.resources", "resource_type", c => c.Byte());
            AddColumn("dbo.resources", "resource_company_id", c => c.Long());
            CreateIndex("dbo.resources", "resource_company_id");
            AddForeignKey("dbo.resources", "resource_company_id", "dbo.resource_companies", "Id");
        }
    }
}
