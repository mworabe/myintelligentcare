﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.PatientAddresses;

namespace PatientCare.Web.Services.Mapping
{
    public static class PatientAddressMapperExtensions
    {
        public static EmitMapperMapperProvider PatientAddressRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAddress, PatientAddressResponse>
                    (new ExtendedMapConfig<PatientAddress,PatientAddressResponse>())); 

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAddress[], PatientAddressResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientAddressRequest, PatientAddress>(new ExtendedMapConfig<PatientAddressRequest, PatientAddress>()));

            return mapperProvider;
        }
    }
}