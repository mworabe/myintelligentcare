﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.EntryTypes;
using Swashbuckle.Swagger.Annotations;
using PatientCare.Web.Api.Models;
using System.Linq.Expressions;
using System.Linq;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using Newtonsoft.Json;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using System.Text;

namespace PatientCare.Web.Api.Controllers
{
    [Authorize]
    public sealed class EntryTypeController : ApiBaseController
    {

        private readonly ITimeManagementService _timeManagementService;
        private readonly IEntryTypeRepository _entryTypeRepository;
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<EntryType> _readOptions = new ReadOptions<EntryType>();
        public EntryTypeController
            (
                ITimeManagementService timeManagementService,
                IEntryTypeRepository entryTypeRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _timeManagementService = timeManagementService;
            _entryTypeRepository = entryTypeRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/entryTypes/" + id, UriKind.Relative);
        }

        [HttpPost]
        [Route("api/entry-type")]
        [SwaggerResponse(201, "Created", typeof(EntryTypeResponse))]
        public async Task<IHttpActionResult> CreateEntryType([FromBody]EntryTypeRequest entryTypeRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_entryTypeRepository.Exists(FilterQuery.Create<EntryType>().AddFilter(p => p.Name == entryTypeRequest.Name)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<EntryType>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var entryType = Mapper.Map<EntryTypeRequest, EntryType>(entryTypeRequest);
            await _timeManagementService.CreateEntryTypeAsync(entryType);

            return Created(GetEntityLocation(entryType.Id), Mapper.Map<EntryType, EntryTypeResponse>(entryType));
        }

        [HttpPut]
        [Route("api/entry-type/{id}")]
        public async Task<IHttpActionResult> EditEntryType(long id, [FromBody] EntryTypeRequest entryTypeRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_entryTypeRepository.Exists(FilterQuery.Create<EntryType>().AddFilter(p => p.Name == entryTypeRequest.Name && p.Id != id)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<EntryType>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var entryType = Mapper.Map<EntryTypeRequest, EntryType>(entryTypeRequest);
            entryType.Id = id;

            await _timeManagementService.UpdateEntryTypeAsync(entryType);

            return Ok(Mapper.Map<EntryType, EntryTypeResponse>(entryType));
        }

        [HttpPut]
        [Route("api/entry-type/create-timesheet/{id}")]
        public async Task<IHttpActionResult> CreateTimesheetByEntryTypeId(long id)
        {
            var entryType = await _entryTypeRepository.ReadAsync(id, null);
            if (entryType == null)
                return NotFound();

            #region Day Json Convert
            DayItem d = (entryType.Days == null && entryType.Days == "") ? new DayItem() : JsonConvert.DeserializeObject<DayItem>(entryType.Days);
            string sun = (d != null) ? ((d.SUN == true) ? "1" : "NULL") : "NULL";
            string mon = (d != null) ? ((d.MON == true) ? "2" : "NULL") : "NULL";
            string tue = (d != null) ? ((d.TUE == true) ? "3" : "NULL") : "NULL";
            string wed = (d != null) ? ((d.WED == true) ? "4" : "NULL") : "NULL";
            string thu = (d != null) ? ((d.THU == true) ? "5" : "NULL") : "NULL";
            string fri = (d != null) ? ((d.FRI == true) ? "6" : "NULL") : "NULL";
            string sat = (d != null) ? ((d.SAT == true) ? "7" : "NULL") : "NULL";
            #endregion
            
            string query = "declare @resturnData as int;EXEC @resturnData = sp_ins_time_sheet_template @entryTypeId = " + entryType.Id + ",@sun = " + sun + ",@mon = " + mon + ",@tue = " + tue + ",@wed = " + wed + ",@thu = " + thu + ",@fri = " + fri + ",@sat = " + sat + "; select @resturnData;";
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                int result;
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                result = ctx.Database.SqlQuery<int>(query).FirstOrDefault();

                if (result != 0)
                {
                    ModelState.AddModelError("Time Sheet Period", "Timesheets are not created for " + entryType.Name.ToString());
                    return BadRequest(ModelState);
                }
            }
            return Ok("Timesheets are created for " + entryType.Name.ToString());
        }

        [HttpDelete]
        [Route("api/entry-type/{id}")]
        public async Task<IHttpActionResult> DeleteEntryTypeItem(long id)
        {
            var success = await _timeManagementService.DeleteEntryTypeAsync(id);
            if (!success)
            {
                return NotFound();
            }

            return Ok(new EntryType { Id = id });
        }

        [HttpGet]
        [Route("api/entry-type/{id}")]
        public async Task<IHttpActionResult> GetEntryType(long id)
        {
            var entryType = await _timeManagementService.ReadEntryTypeAsync(id);
            if (entryType == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<EntryType, EntryTypeResponse>(entryType);
            return Ok(viewModel);
        }

        [HttpGet]
        [Route("api/entry-type")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<EntryTypeResponse>))]
        public IHttpActionResult GetAllEntryType([FromUri]EntryTypeListRequest request)
        {
            var query = PagingExtensions<EntryType>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            var readOptions = new ReadOptions<EntryType>().AsReadOnly();
            var page = _entryTypeRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<EntryType[], EntryTypeResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<EntryTypeResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        private static Expression<Func<EntryType, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return entryType => entryType.Name.Contains(searchPhrase);
        }

        private static Expression<Func<EntryType, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return entryType => entryType.Name.Contains(searchPhrase);
        }


        [HttpGet]
        [Route("api/entry-type/autocomplete")]
        [ResponseType(typeof(EntryTypeAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<EntryType>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
            }

            var autocompleteResult = await _entryTypeRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<EntryType[], EntryTypeAutoCompleteResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }
    }
}
