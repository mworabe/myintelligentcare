﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.Countries;

namespace PatientCare.Web.Services.Mapping
{
    public static class CountriesMapperExtensions
    {
        public static EmitMapperMapperProvider CountriesRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Country, CountryResponse>
                    (new ExtendedMapConfig<Country, CountryResponse>()));
            
            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Country[], CountryResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<CountryRequest, Country>(
                    new ExtendedMapConfig<CountryRequest, Country>()));

            return mapperProvider;
        }
    }
}