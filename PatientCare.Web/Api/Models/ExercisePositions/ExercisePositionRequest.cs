﻿namespace PatientCare.Web.Api.Models.ExercisePositions
{
    public class ExercisePositionRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }   
}