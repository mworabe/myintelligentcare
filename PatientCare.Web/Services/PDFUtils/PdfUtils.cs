﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using PatientCare.Core.Services.Mailing;
using System.Configuration;
using System.Net;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace PatientCare.Web.Services.PDFUtils
{
    public class PdfUtils
    {
        public static byte[] genratePDF(string content, Rectangle pageSize)
        {
            Document document = new Document(pageSize);
            TextReader sr = new StringReader(content);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                return bytes;
            }
        }
    }
}
