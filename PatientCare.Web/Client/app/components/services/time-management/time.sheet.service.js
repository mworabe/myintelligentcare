﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("timeSheetService", ["$resource", function ($resource) {
      var resource = $resource("api/timeSheetPeriod/:controller/:id", null,
          {
              'update': { method: "PUT" },
              'query': { method: "GET", params: { controller: "resource" }, isArray: false },
              'getEntity': { method: "GET", params: { controller: "resource" }, isArray: false },
          });

      return resource;
  }]);