﻿using System;
using System.Runtime.Serialization;
using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.ResourcePreviousEmployments
{
    [DataContract]
    public class ResourcePreviousEmploymentResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string TitleOfPosition { get; set; }
        
        [DataMember]
        public string DutiesOfPosition { get; set; }

        [DataMember]
        public string ReasonForLeaving { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public int? NumberOfSupervised { get; set; }

        [DataMember]
        public decimal? Salary { get; set; }

        [DataMember]
        public SalaryPerTypeEnum? SalaryPer { get; set; }

        [DataMember]
        public int? HoursPerWeek { get; set; }

        [DataMember]
        public long ResourceId { get; set; }
    }
}