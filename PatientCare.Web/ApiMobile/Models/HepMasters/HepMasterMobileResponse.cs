﻿using PatientCare.Web.Api.Models.HepDetails;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.ApiMobile.Models.HepDetails;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.HepMasters
{
    [DataContract]
    public class HepMasterMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Patient { get; set; }

        [DataMember]
        public long? OwnerId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Owner { get; set; }

        [DataMember]
        public long? PatientCaseId { get; set; }

        [DataMember]
        public PicklistDefaultResponse PatientCase { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public DateTime? Modified { get; set; }

        [DataMember]
        public HepDetailsMobileResponse[] HepDetails { get; set; }

        [DataMember]
        public bool? IsSaveAsDraft { get; set; }

    }
}