// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright � 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

var path = require("path");
var gulp = require("gulp");
var conf = require("./conf");

var browserSync = require("browser-sync");

function isOnlyChange(event) {
    return event.type === "changed";
}

gulp.task("watch", ["inject"], function() {

    gulp.watch([path.join(conf.paths.src, "/*.html"), "bower.json"], ["inject"]);

    gulp.watch([
        path.join(conf.paths.src, "/app/**/*.css"),
        path.join(conf.paths.src, "/app/**/*.less")
    ], function(event) {
        if (isOnlyChange(event)) {
            browserSync.reload(event.path);
        } else {
            gulp.start("inject");
        }
    });

    gulp.watch(path.join(conf.paths.src, "/app/**/*.js"), function(event) {
        if (isOnlyChange(event)) {
            gulp.start("scripts");
        } else {
            gulp.start("inject");
        }
    });

    gulp.watch(path.join(conf.paths.src, "/app/**/*.html"), function(event) {
        browserSync.reload(event.path);
    });
});