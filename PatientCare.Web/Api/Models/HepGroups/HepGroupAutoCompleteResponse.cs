﻿using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.HepGroups
{
    [DataContract]
    public class HepGroupAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Patient { get; set; }

        [DataMember]
        public long? PatientCaseId { get; set; }

        [DataMember]
        public PicklistDefaultResponse PatientCase { get; set; }

        [DataMember]
        public int? ExerciseOrder { get; set; }
    }
}