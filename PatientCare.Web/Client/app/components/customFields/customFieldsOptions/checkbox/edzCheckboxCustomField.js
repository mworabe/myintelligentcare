"use strict";
(function edzCheckboxCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzCheckboxCustomField", ["edzBaseCustomField", edzCheckboxCustomField]);

    ///////////////////////////////////////////////////////
    function edzCheckboxCustomField(baseCustomField) {
        function CheckboxCustomField() { }

        CheckboxCustomField.prototype = Object.create(baseCustomField);
        CheckboxCustomField.prototype.optionsTemplate = "<edz-checkbox-custom-field-options></edz-checkbox-custom-field-options>";
        CheckboxCustomField.prototype.optionsTemplateEdit = "<edz-checkbox-custom-field-edit-options></edz-checkbox-custom-field-edit-options>";
        return CheckboxCustomField;
    }
})();