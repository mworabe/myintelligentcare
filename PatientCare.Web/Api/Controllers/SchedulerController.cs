﻿using System;
using System.Web.Http;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Schedulers;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Scheduler operations.
    /// </summary>
    [Authorize]
    public sealed class SchedulerController : ApiBaseController
    {
        /// <summary>
        /// Description : Scheduler IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        /// Description : Initializes a new instance of the Scheduler Class.     
        /// </summary>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public SchedulerController
            (
               IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("scheduler/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Scheduler.
        /// </summary>
        /// <param name="request">Scheduler List Request Model</param>
        /// <returns>Scheduler List Response Model</returns>
        [HttpGet]
        [Route("api/scheduler")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<SchedulerListResponse>))]
        public async Task<IHttpActionResult> GetAllPatientAppointment([FromUri]SchedulerListRequest request)
        {
            if (request.Clinician != null)
                request.Clinician = ',' + request.Clinician + ',';
            if (request.Clinic != null)
                request.Clinic = ',' + request.Clinic + ',';
            if (request.Status != null)
                request.Status = ',' + request.Status + ',';

            List<SchedulerListResponse> appointments = new List<SchedulerListResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_get_appointment_for_schedular @Status = '" + request.Status + "',@Clinic = '" + request.Clinic + "',@clinician = '" + request.Clinician + "',@startDate = '" + request.From.Value.ToString("MM/dd/yyyy") + "',@endDate = '" + request.To.Value.ToString("MM/dd/yyyy") + "'";
                appointments = await ctx.Database.SqlQuery<SchedulerListResponse>(query).ToListAsync();
                return Ok(appointments.ToArray());
            }
        }
    }
}
