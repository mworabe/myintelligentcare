﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("RoleSetup", [
        "$resource", "$state", function ($resource, $state) {
            var resource = $resource("api/RoleSetup/:id/:controller",
                null, //parameters default
                {
                    'update': { method: "PUT" },
                    'delete': { method: "DELETE" },
                    'changeLayout': { method: "POST", params: { controller: "change-role-layout" }, isArray: false },
                    'changeDefaultLayout': { method: "POST", params: { controller: "change-default-layout" }, isArray: false },
                    'query': { method: "GET", params: {}, isArray: false },
                    'resourceConfigLayout': { method: "GET", params: { controller: "resource-config-layout" }, isArray: false },
                    'projectConfigLayout': { method: "GET", params: { controller: "project-config-layout" }, isArray: false },
                    'projectTaskConfigLayout': { method: "GET", params: { controller: "project-task-config-layout" }, isArray: false },
                    'intakeConfigLayout': { method: "GET", params: { controller: "intake-config-layout" }, isArray: false },
                    'clientConfigLayout': { method: "GET", params: { controller: "client-config-layout" }, isArray: false },

                    'resourceLayoutConfigForRole': { method: "GET", params: { controller: "resource-layout-config-for-role" }, isArray: false },
                    'projectLayoutConfigForRole': { method: "GET", params: { controller: "project-layout-config-for-role" }, isArray: false },
                    'projectTaskLayoutConfigForRole': { method: "GET", params: { controller: "project-task-layout-config-for-role" }, isArray: false },
                    'intakeLayoutConfigForRole': { method: "GET", params: { controller: "intake-layout-config-for-role" }, isArray: false },
                    'ClientLayoutConfigForRole': { method: "GET", params: { controller: "client-layout-config-for-role" }, isArray: false },

                    'getdDashboards': { method: "GET", params: { controller: "dashboard-list" }, isArray: false },
                    'resourceLayoutDefault': { method: "GET", params: { controller: "resource-layout-config-default" }, isArray: false },
                    'projectLayoutDefault': { method: "GET", params: { controller: "project-layout-config-default" }, isArray: false },
                    'projectTaskLayoutDefault': { method: "GET", params: { controller: "project-task-layout-config-default" }, isArray: false },
                    'intakeLayoutDefault': { method: "GET", params: { controller: "intake-layout-config-default" }, isArray: false },
                    'clientLayoutDefault': { method: "GET", params: { controller: "client-layout-config-for-role" }, isArray: false },

                });

            resource.getTableOption = {
                pageName: "Roles",
                hideBulkEdit: true,
                cols: function () {
                    return [
                        { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                        { field: "name", title: "Name", sortable: "name", show: true, dataType: "text", required: true },
                        { field: "description", title: "Description", sortable: "description", show: true, dataType: "text", required: true },
                        { field: "isActiveYesNo", title: "Active", sortable: "isActiveYesNo", show: true, dataType: "text", required: true },
                        {
                            field: "action",
                            title: ".",
                            dataType: "command"
                        },
                    ];
                },
                onAdd_Click: function () {
                    $state.go("roleEdit");
                },
                onEdit_Click: function (row) {
                    $state.go("roleEdit", { id: row.id });
                },
                headerButtons: [
                    {
                        text: "Default Layout",
                        click: function () {
                            $state.go("configLayout");
                        },
                        iconClass: "layout"
                    }
                ],
                menuButtons: [
                    {
                        click: function (row) {
                            $state.go("configLayout", { id: row.id });
                        },
                        iconClass: "layout"
                    }
                ]
            };
            return resource;
        }
    ]);