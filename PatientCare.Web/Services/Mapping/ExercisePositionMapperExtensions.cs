﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ExercisePositions;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ExercisePositionMapperExtensions
    {
        public static EmitMapperMapperProvider ExercisePositionMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExercisePosition, ExercisePositionResponse>
                    (new ExtendedMapConfig<ExercisePosition, ExercisePositionResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExercisePosition[], ExercisePositionResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExercisePosition, ExercisePositionAutoCompleteResponse>
                    (new ExtendedMapConfig<ExercisePosition, ExercisePositionAutoCompleteResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExercisePosition[], ExercisePositionAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExercisePositionRequest, ExercisePosition>
                    (new ExtendedMapConfig<ExercisePositionRequest, ExercisePosition>()));

            return mapperProvider;
        }
    }
}