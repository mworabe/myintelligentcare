﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Hep Frequency.
    /// </summary>
    public class HepFrequency : Entity
    {
        public long? HepId { get; set; }
        public virtual HepMaster Hep { get; set; }

        public long? HepDetailId { get; set; }
        public virtual HepDetail HepDetail { get; set; }

        public WeekDaysEnum? Day { get; set; }

        public int? ExerciseFrequency { get; set; }

        public bool? IsActive { get; set; }
    }
}
