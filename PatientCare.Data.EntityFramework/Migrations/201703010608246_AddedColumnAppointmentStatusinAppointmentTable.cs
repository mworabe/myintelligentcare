namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedColumnAppointmentStatusinAppointmentTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patient_appointments", "appointment_status", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.patient_appointments", "appointment_status");
        }
    }
}
