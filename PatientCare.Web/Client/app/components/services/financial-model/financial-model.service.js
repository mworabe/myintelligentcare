﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("FinancialModelService", ["$resource", function ($resource) {
      var resource = $resource("api/FinancialPlanningProfitability/:id",
        null, {
            'create': { method: "POST" },
            'update': { method: "PUT", },
            'get': { method: 'GET', url: "api/FinancialPlanningProfitability/:id/financial-planning-profitability" },
            'query': { method: "GET" },
            'sharedModelList': { method: "GET", url: 'api/FinancialPlanningProfitability/FPPMSharedModelList' },
            'shareModel': { method: "PUT", url: 'api/FPPApprovalRequest' },
            'changeStatus': { method: 'PUT', url: 'api/FPPApproveReject' }

        });
      return resource;
  }]);