﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ResourceRoleAVDController", ["resourceRolesAVDService", "$scope", "$state", "DateTimeHelper", "toaster",
        "resourceAVDHelper", "NgTableParams", "SystemFunctions",
        ResourceRoleAVDControllerFunc]);

function ResourceRoleAVDControllerFunc(resourceRolesAVDService, $scope, $state, dateTimeHelper, toaster,
    resourceAVDHelper, ngTableParams, systemFunctionsService) {
    var self = this;

    self.jobTitleId = $state.params.roleId || 0;
    console.info(self.jobTitleId);
    self.isDetailedMode = false;
    self.isFormSubmitted = false;
    self.data = [];
    self.totalCount = undefined;
    self.selectedJobTitleId = undefined;
    self.timeRangeEnum = dateTimeHelper.dateTypeEnum();

    self.tableRequest = {};
    self.tableParams = {};
    self.pagination = {};
    self.queryParams = {};

    self.jobTitleModel = {};

    self.queryParams.pageIndex = 1;
    self.queryParams.pageSize = 20;

    self.tableParams = new ngTableParams({ count: 100 }, {
        counts: [],
        getData: function () {
            return [];
        }
    });

    self.tableRequest.start = moment().startOf('month');
    self.tableRequest.end = self.tableRequest.start.clone().add(1, "years").endOf('month');// moment().endOf('month');
    ////.subtract(1, "days");// moment().endOf('month');
    self.tableRequest.timeRange = "Month";
    self.columnMode = "Month";
    self.viewInnerWidth = parseInt(window.innerWidth);
    self.isInlineFormVisible = self.viewInnerWidth <= 1100;

    function processRolesServerData(data) {
        var sortType = (self.queryParams.sortType) ? self.queryParams.sortType : "asc"
        var groupByData = _.groupBy(data, "jobtitleid");
        return _.sortBy(groupByData, "", sortType);
    }

    function processResourceServerData(data) {
        var groupByData = _.groupBy(data, "resourceId");
        return _.orderBy(groupByData, ["resourceName"], ["asc"]);
    }


    function setColumnMode(serverDataElement) {
        if (serverDataElement.dDay) {
            self.columnMode = "Day";
        } else if (serverDataElement.dMonth) {
            self.columnMode = "Month";
        } else if (serverDataElement.dYear) {
            self.columnMode = "Year";
        }
    }

    function onServerErrorCallback(response) {
        self.isFormSubmitted = false;
        var msg = "Unable to process your request, please try again later.";
        if (response.data && response.data.messageDetail)
            msg = response.data.messageDetail;
        toaster.pop("error", "", msg);
    }
    self.calculateTotalResourceDemandPercentage = function calculateTotalResourceDemandPercentage(col) {
        return ((self.getResourceRedValue(col) * 100) / col.totalHours).toFixed(2);
    }
    self.getResourceRedValue = function getResourceRedValue(col) {
        return col.totalTimeOffHours + col.holidayHours + col.reservedAvailability + col.demandFromTasks;
    }

    function onServerSuccessCallbackRoles(response) {
        self.isFormSubmitted = false;
        self.data = processRolesServerData(response.data);
        self.pagination = systemFunctionsService.generatePaginationPageArray(response, self.queryParams.pageIndex, self.queryParams.pageSize);
        self.totalCount = response.itemsCount;
        setColumnMode(response.data[0]);
    }

    function onServerSuccessCallbackResources(response) {
        self.isFormSubmitted = false;
        self.data = processResourceServerData(response.data);
        self.pagination = systemFunctionsService.generatePaginationPageArray(response, self.queryParams.pageIndex, self.queryParams.pageSize);
        self.totalCount = response.itemsCount;
        setColumnMode(response.data[0]);
    }
    self.onJobTitleNameClick = function onJobTitleNameClick(row) {
        console.info(row);
        self.jobTitleModel.jobTitleId = row.jobtitleid;
        self.jobTitleModel.jobTitleName = row.jobTitleName;
        self.jobTitleModel.isDetailedMode = self.isDetailedMode;
        self.data = [];
        reloadDataFromServer();
        //$state.go("resourceAvd", { roleId: row.jobtitleid, jobTitle: row.jobTitleName, isDetailedMode: self.isDetailedMode });
    }


    self.onBackButtonClick = function onBackButtonClick() {
        self.jobTitleModel = {};
        self.data = [];
        self.queryParams.pageIndex = 1;
        self.isDetailedMode = false;
        reloadDataFromServer();
    }

    function reloadDataFromServer() {
        self.queryParams.pageIndex = 1;
        initializedResourceAVD(resourceAVDForm);
    }

    /**
        Main Call to read Data and Initialize View.
        @_form == resourceAVDForm
    */
    function initializedResourceAVD(_form) {
        if (!_form) {
            return;
        }
        if (_form.$invalid) {
            toaster.pop("error", "", "Availability Filter Data is not Valid, please check missing required data.");
            return;
        }

        systemFunctionsService.scrollToTopZero();

        self.isFormSubmitted = true;
        if (!_form.$submitted)
            _form.$submitted = true;

        var tempModel = {};

        tempModel.start = self.tableRequest.start.format("LL");
        tempModel.end = self.tableRequest.end.format("LL");
        tempModel.timeRange = self.tableRequest.timeRange;
        tempModel.pageIndex = self.queryParams.pageIndex;
        tempModel.pageSize = self.queryParams.pageSize;
        tempModel.sortType = self.queryParams.sortType;


        if (self.jobTitleModel && !self.jobTitleModel.jobTitleId > 0) {
            self.isPageBusy = resourceRolesAVDService.getRolesAVD(tempModel, onServerSuccessCallbackRoles, onServerErrorCallback);
        } else {
            tempModel.jobTitleId = self.jobTitleModel.jobTitleId;
            self.isPageBusy = resourceRolesAVDService.getResourceAVD(tempModel, onServerSuccessCallbackResources, onServerErrorCallback);
        }
    }

    self.calculateTotalRoleDemandPercentage = function calculateTotalRoleDemandPercentage(resourcesCount, totalDemandResource) {
        return ((totalDemandResource * 100) / resourcesCount).toFixed(2);
    }

    self.onPageChange = function (page) {
        systemFunctionsService.scrollToTopZero();
        self.queryParams.pageIndex = parseInt(page.number);
        initializedResourceAVD(resourceAVDForm);
    }

    /**
        Roles Utilization Functions
    */
    self.getRoleUtilization = function getRoleUtilization(col) {
        var demandResource = parseFloat(col.totalDemandResource);
        var availableResource = parseFloat(col.totalAvailableResources);
        var roleUtilization = ((demandResource * 100) / (demandResource + availableResource));
        return (isNaN(roleUtilization) || !roleUtilization) ? 0 : parseFloat(roleUtilization).toFixed(2);
    }

    self.isCriticalClass = function isCriticalClass(col) {
        return resourceAVDHelper.checkCritical(parseFloat(self.getRoleUtilization(col)));
    }
    self.isMediumClass = function isMediumClass(col) {
        return resourceAVDHelper.checkMedium(parseFloat(self.getRoleUtilization(col)));
    }


    self.getResourceUtilization = function getResourceUtilization(col) {
        var demandResource = parseFloat(col.demandFromTasks);
        var availableResource = parseFloat(col.totalAvailableHours);
        var roleUtilization = ((demandResource * 100) / (demandResource + availableResource));
        return (isNaN(roleUtilization) || !roleUtilization) ? 0 : parseFloat(roleUtilization).toFixed(2);
    }
    self.isCriticalClassForResource = function isCriticalClass(col) {
        return resourceAVDHelper.checkCritical(parseFloat(self.getResourceUtilization(col)));
    }
    self.isMediumClassForResource = function isMediumClass(col) {
        return resourceAVDHelper.checkMedium(parseFloat(self.getResourceUtilization(col)));
    }

    /****/
    self.onModeChangeButtonClick = function onModeChangeButtonClick() {
        self.isDetailedMode = !self.isDetailedMode;
    }

    self.onAllResourceButtonClick = function onAllResourceButtonClick() {
        $state.go("resource");
    }

    self.onResourceNameClick = function onResourceNameClick(row) {
        console.info(row);
        $state.go("resourceEdit", { id: row.resourceId, currentTab: "availability" });
    }

    self.getEllipsesName = function getEllipsesName(name, chars) {
        if (name)
            return (name.length > chars) ? name.substring(0, chars) + "..." : name;
        return name;
    }

    self.onSortButtonClick = function onSortButtonClick() {
        if (!self.queryParams.sortType)
            self.queryParams.sortType = "asc";
        else
            self.queryParams.sortType = self.queryParams.sortType === "asc" ? "desc" : "asc";
        initializedResourceAVD(resourceAVDForm);
    }


    self.submitAVDForm = initializedResourceAVD;

    // Default Call.
    initializedResourceAVD(resourceAVDForm);
}