﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.SurveyQuestions
{
    [DataContract]
    public class SurveyQuestionResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public string Modified { get; set; }

        [DataMember]
        public string Created { get; set; }

        [DataMember]
        public PicklistResponse Category { get; set; }
    }
}