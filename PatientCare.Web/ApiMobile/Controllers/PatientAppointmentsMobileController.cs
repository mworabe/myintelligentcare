﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.ApiMobile.Models.PatientAppointment;
using System.Web.Http.Description;
using PatientCare.Web.Api.Models.AppointmentTypes;
using PatientCare.Core.Enums;
using System.Linq.Expressions;
using PatientCare.Web.Hubs;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.ApiMobile.Models;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Web.ApiMobile.Controllers
{
    /// <summary>
    /// This Controller is used to handle Patient Appointments Mobile operations.
    /// </summary>
    //[Authorize(Roles = ApplicationUser.PatientRole)]
    public sealed class PatientAppointmentsMobileController : ApiBaseController
    {

        /// <summary>
        /// Description : Patient Appointments IPatientService.
        /// </summary>
        private readonly IPatientService _patientService;
        /// <summary>
        /// Description : Patient Appointments IPatientAppointmentRepository.
        /// </summary>
        private readonly IPatientAppointmentRepository _patientAppointmentRepository;
        /// <summary>
        /// Description : Patient Appointments IAppointmentTypeRepository.
        /// </summary>
        private readonly IAppointmentTypeRepository _appointmentTypeRepository;
        /// <summary>
        /// Description : Patient Appointments IPatientCaseRepository.
        /// </summary>
        private readonly IPatientCaseRepository _patientCaseRepository;
        /// <summary>
        /// Description : Patient Appointments IPatientRepository.
        /// </summary>
        private readonly IPatientRepository _patientRepository;
        /// <summary>
        /// Description : Patient Appointments IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<PatientAppointment> _readOptions = new ReadOptions<PatientAppointment>();

        /// <summary>
        /// Description : Initializes a new instance of the Patient Appointments Mobile Class. 
        /// </summary>
        /// <param name="patientService">IPatient Service</param>
        /// <param name="patientAppointmentRepository">IPatientAppointment Repository</param>
        /// <param name="appointmentTypeRepository">IAppointmentType Repository</param>
        /// <param name="patientCaseRepository">IPatientCase Repository</param>
        /// <param name="patientRepository">IPatient Repository</param>
        /// <param name="dataContextScopeFactory">IDataContext Scope Factory</param>
        public PatientAppointmentsMobileController
            (
                IPatientService patientService,
                IPatientAppointmentRepository patientAppointmentRepository,
                IAppointmentTypeRepository appointmentTypeRepository,
                IPatientCaseRepository patientCaseRepository,
                IPatientRepository patientRepository,
                 IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _patientService = patientService;
            _patientAppointmentRepository = patientAppointmentRepository;
            _appointmentTypeRepository = appointmentTypeRepository;
            _patientCaseRepository = patientCaseRepository;
            _patientRepository = patientRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("patientappointments/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Patient Appointment by Id.  
        /// </summary>
        /// <param name="Id">Id</param>
        /// <param name="request">Patient Appointment Mobile List Request Model</param>
        /// <returns>Patient Appointment Mobile Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/patientAppointments/{id}/appointment")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<PatientAppointmentMobileListResponse>))]
        public IHttpActionResult GetAllPatientAppointment(long Id, [FromUri]PatientAppointmentMobileListRequest request)
        {
            if (Id == 0)
                return NotFound();

            request.PageIndex = 1;
            request.PageSize = 999;

            //request.PageIndex = request.PageIndex;
            //request.PageSize = request.PageSize;

            //var query = PagingExtensions<PatientAppointment>.CreatePagedQuery
            //              (request,
            //                  (string.IsNullOrWhiteSpace(request.SearchField)
            //                      && !string.IsNullOrWhiteSpace(request.SearchPhrase)
            //                          ?
            //                     GetSearchColumnExpressions(request.SearchPhrase)
            //                    : GetAutocompleteSearchExpressions(request.SearchPhrase)
            //                   ));

            var query = PagingExtensions<PatientAppointment>.CreatePagedQuery(request);
            query.AddFilter(x => x.PatientId == Id);
            query.AddOrderBy(x => x.Id);
            query.SortOrder = SortOrder.Descending;

            var readOptions = new ReadOptions<PatientAppointment>().AsReadOnly()
              .WithIncludePredicate(item => item.AppointmentType);

            var page = _patientAppointmentRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<PatientAppointment[], PatientAppointmentMobileResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<PatientAppointmentMobileResponse>(dtos, page.TotalCount);
            return Ok(response);
        }

        private static Expression<Func<PatientAppointment, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return s => s.PatientName.Name.Contains(searchPhrase);
        }

        private static Expression<Func<PatientAppointment, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return s => s.PatientName.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Create New Patient Appointment.   
        /// </summary>
        /// <param name="patientAppointmentRequest">Patient Appointment Request Model</param>
        /// <returns>Patient Appointment Response Model</returns>
        [HttpPost]
        [Route("api/mobile/v1/patientAppointments/{id}")]
        [SwaggerResponse(201, "Created", typeof(PatientAppointmentMobileResponse))]
        public async Task<IHttpActionResult> CreatePatientAppointment(long id, [FromBody]PatientAppointmentMobileRequest patientAppointmentRequest)
        {
            MobileErrorResponse res = new MobileErrorResponse();
            try
            {
                if (patientAppointmentRequest.StartDate < DateTime.UtcNow)                
                    throw new Exception("You cannot request appointment for past time.");
                
                if (id == 0)
                    throw new Exception("Cannot create appointment, please contact our office.");

                // here from mobile You get two values only PtVisit and DryNeedle you need rectify it which id you have to set in appointmemtId 
                    long? appointmentTypeId = null;
                if (patientAppointmentRequest.AppointmentType != null)
                {
                    if (patientAppointmentRequest.AppointmentType == "Dry Needle")
                    {
                        using (var dbContextScope = _dataContextScopeFactory.Create())
                        {
                            var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                            var dbSet = ctx.Set<AppointmentType>();
                            var getAppointmentTypeId = dbSet.AsNoTracking().Where(x => x.Name == "Dry Needle").FirstOrDefault();
                            //_appointmentTypeRepository.List(new FilterQuery<AppointmentType>().AddFilter(x => x.Name == "Dry Needle")).FirstOrDefault();
                            if (getAppointmentTypeId != null)
                                appointmentTypeId = getAppointmentTypeId.Id;
                        }   
                    }
                    else
                        appointmentTypeId = null;
                }
                else
                    appointmentTypeId = null;

                patientAppointmentRequest.PatientId = id;

                var patientAppointment = Mapper.Map<PatientAppointmentMobileRequest, PatientAppointment>(patientAppointmentRequest);
                patientAppointment.AppointmentTypeId = appointmentTypeId;
                patientAppointment.AppointmentType = null;
                patientAppointment.IsMobile = true;
                patientAppointment.IsReschedule = false;
                patientAppointment.AppointmentStatus = AppointmentStatusEnum.Request;
                
                var currentCaseDetail = _patientCaseRepository.List(new FilterQuery<PatientCase>().AddFilter(x => x.PatientId == patientAppointment.PatientId && x.CaseStatus == CaseStatusEnum.Inprogress && (x.IsDeleted == false || x.IsDeleted == null))).FirstOrDefault();
                if (currentCaseDetail != null)
                {
                    patientAppointment.CaseId = currentCaseDetail.Id;
                    patientAppointment.ClinicLocationId = currentCaseDetail.ClinicLocationId;
                    patientAppointment.ClinicianId = currentCaseDetail.AssignedPhysicianId;
                    patientAppointment.FacilityId = currentCaseDetail.ClinicId;
                }
                else
                {
                    var PatientDetail = _patientRepository.List(new FilterQuery<Patient>().AddFilter(x => x.Id == patientAppointment.PatientId && (x.IsDeleted == false || x.IsDeleted == null))).FirstOrDefault();
                    patientAppointment.ClinicLocationId = PatientDetail.ClinicLocationId;
                    patientAppointment.ClinicianId = PatientDetail.ClinicianId;
                    patientAppointment.FacilityId = PatientDetail.ClinicId;
                }
                await _patientService.CreatePatientApointmentAsync(patientAppointment);

                var notification = new NotificationHelper(_dataContextScopeFactory);
                await notification.pushAppointmentUpdate("New Appointment Created.", patientAppointment.Id);

                res.Type = ErrorTypeEnum.Success;
                res.Message = "Thank you for your request. A front office staff member will review your request and respond at their earliest availability. Please note that End Times can vary based on the medical condition of each patient.";
            }
            catch (Exception ex)
            {
                res.Type = ErrorTypeEnum.Error;
                res.Message = ex.Message.ToString();
            }
            return Ok(res);
        }

        /// <summary>
        /// Method Description : Using for Update Patient Appointment.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="patientAppointmentRequest">Patient Appointment Request Model</param>
        /// <returns>Mobile Error Response Model</returns>
        [HttpPut]
        [Route("api/mobile/v1/rescheduleAppointments/{id}")]
        public async Task<IHttpActionResult> RescheduleAppointment(long id, [FromBody] PatientAppointmentMobileRequest patientAppointmentRequest)
        {
            MobileErrorResponse res = new MobileErrorResponse();
            try
            {
                if (patientAppointmentRequest.StartDate < DateTime.UtcNow)
                    throw new Exception("You cannot request appointment for past time.");

                if (id == 0)
                    throw new Exception("Cannot reschedule appointment, please contact our office.");

                var patientAppointment = await _patientService.ReadPatientApointmentAsync(id);

                if (patientAppointment == null)
                    throw new Exception("Cannot reschedule appointment, please contact our office.");

                if (patientAppointment.AppointmentStatus == AppointmentStatusEnum.Confirmed)
                {
                    patientAppointment.AppointmentStatus = AppointmentStatusEnum.Rescheduled;
                }

                patientAppointment.Id = patientAppointmentRequest.Id;
                patientAppointment.StartDate = patientAppointmentRequest.StartDate;
                patientAppointment.EndDate = patientAppointmentRequest.EndDate;
                patientAppointment.AdditionalDetail = (patientAppointmentRequest.AdditionalDetail != null) ? patientAppointmentRequest.AdditionalDetail : patientAppointment.AdditionalDetail;
                patientAppointment.ClinicianId = patientAppointment.ClinicianId;
                patientAppointment.IsReschedule = true;
                patientAppointment.IsMobile = true;

                await _patientService.UpdatePatientApointmentAsync(patientAppointment);

                var notification = new NotificationHelper(_dataContextScopeFactory);
                await notification.pushAppointmentUpdate("Appointment Modified.", patientAppointment.Id);

                res.Type = ErrorTypeEnum.Success;
                res.Message = "Thank you for your reschedule request. A front office staff member will review your reschedule request and respond at their earliest availability. Please note that End Times can vary based on the medical condition of each patient.";
            }
            catch (Exception ex)
            {
                res.Type = ErrorTypeEnum.Error;
                res.Message = ex.Message.ToString();
            }
            return Ok(res);
        }

        /// <summary>
        /// Method Description : Using for Delete Patient Appointment by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Mobile Error Response Model</returns>
        [HttpDelete]
        [Route("api/mobile/v1/patientAppointments/{id}")]
        public async Task<IHttpActionResult> DeletePatientsAppointmentItem(long id)
        {
            MobileErrorResponse res = new MobileErrorResponse();
            try
            {
                if (id == 0)
                    throw new Exception("Cannot delete appointment, please contact our office.");

                var success = await _patientService.DeletePatientApointmentAsync(id);
                if (!success)
                    throw new Exception("Cannot delete appointment, please contact our office.");

                var notification = new NotificationHelper(_dataContextScopeFactory);
                await notification.pushAppointmentUpdate("Appointment Deleted.", id);

                res.Type = ErrorTypeEnum.Success;
                res.Message = "Appointment is deleted";
            }
            catch (Exception ex)
            {
                res.Type = ErrorTypeEnum.Error;
                res.Message = ex.Message.ToString();
            }
            return Ok(res);
        }

        /// <summary>
        /// Method Description : Using for Get Patients Appointment by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patients Appointment Mobile Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/patientAppointments/{id}")]
        public async Task<IHttpActionResult> GetPatientsAppointment(long id)
        {
            var patientAppointment = await _patientService.ReadPatientApointmentAsync(id);
            if (patientAppointment == null)
                return NotFound();

            var viewModel = Mapper.Map<PatientAppointment, PatientAppointmentMobileResponse>(patientAppointment);
            return Ok(viewModel);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Patient Appointment.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Patient Appointment Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/AppointmentTypes/autocomplete")]
        [ResponseType(typeof(AppointmentTypeAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<AppointmentType>();
            if (!string.IsNullOrWhiteSpace(search))
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));

            var autocompleteResult = await _appointmentTypeRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<AppointmentType[], AppointmentTypeAutoCompleteResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }
    }
}
