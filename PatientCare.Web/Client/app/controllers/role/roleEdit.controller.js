﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("RoleEditCtrl", [
        "RoleSetup", "$state", "$stateParams", "$q", "$translate", "$filter",
        function (roleSetup, $state, $stateParams, $q, $translate, $filter) {
            var self = this;
            var modelId = $stateParams.id;
            self.callSubmit = false;

            this.titleText = modelId ? "Edit Role" : "New Role";

            function updateModelAfterTake() {
                function fillList() {
                    self.model.accessRights.sections = [];
                    _.forEach(self.sectionList, function (item) {
                        self.model.accessRights.sections.push({
                            item: item
                        });
                    });
                    self.model.accessRights.configurationSections = [];
                    _.forEach(self.tempConfigurationSectionList, function (item) {
                        self.model.accessRights.configurationSections.push({
                            item: item
                        });
                    });
                }

                if (!self.model)
                    self.model = {};
                if (!self.model.id)
                    self.model.isActive = true;

                if (!self.model.accessRights) {
                    self.model.accessRights = {};
                    fillList();
                    return;
                } else {
                    if (!self.model.accessRights.sections) {
                        fillList();
                        return;
                    }
                    if (!self.model.accessRights.configurationSections) {
                        fillList();
                        return;
                    }
                }

                _.forEach(self.sectionList, function (item) {
                    var index = _.findIndex(self.model.accessRights.sections, function (section) {
                        return section.item === item;
                    });
                    if (index === -1) {
                        self.model.accessRights.sections.push({
                            item: item
                        });
                    }
                });

                _.forEach(self.configurationSectionList, function (item) {
                    var index = _.findIndex(self.model.accessRights.configurationSections, function (section) {
                        return section.item === item;
                    });
                    if (index === -1) {
                        self.model.accessRights.configurationSections.push({
                            item: item
                        });
                    }
                });
            }

            function loadInitalData() {

                if (modelId) {
                    self.loadPage = roleSetup.get({ id: modelId }, function (responce) {
                        self.model = responce;
                        updateModelAfterTake();
                    });
                } else {
                    updateModelAfterTake();
                }
            }

            function isCheckedCRUDItem(section, crudOperation) {
                return _.findIndex(section.actions, function (crud) {
                    return crud === crudOperation;
                }) !== -1;
            }

            function isCheckboxCrud_Click(section, crudOperation, checked) {
                if (!section.actions)
                    section.actions = [];
                if (checked) {
                    section.actions.push(crudOperation);
                } else {

                    _.remove(section.actions, function (item) {
                        return item === crudOperation;
                    });
                }

            }

            self.getSectionName = function (sectionId) {
                var obj = _.find(self.tempSectionList, { value: sectionId });
                return obj.name || sectionId;
            }
            self.getConfigurationSectionName = function (sectionId) {
                var obj = _.find(self.tempConfigurationSectionList, { value: sectionId.value || sectionId });
                return obj.name || sectionId;
            }

            self.getActivitySectionName = function (sectionId) {
                var obj = _.find(self.tempSectionList, { value: sectionId });
                return obj.name || sectionId;
            }

            function initializeHomePageList() {
                var activityList = angular.fromJson(localStorage.getItem("PROJECT_MENU_ITEMS"));
                self.homePageList = [];
                self.homePageList.push({ value: "HumanResourceHome", name: "Human Resource Home", stateParams: undefined });
                self.homePageList.push({ value: "home_care_managment", name: "Care Management", stateParams: undefined });
                self.homePageList.push({ value: "ResourceHome", name: "Resource Home", stateParams: undefined });
                self.homePageList.push({ value: "Calendar", name: "Calendar", stateParams: undefined });
                self.homePageList.push({ value: "Dashboard", name: "Dashboard", stateParams: undefined });
                _.forEach(activityList, function (activity) {
                    self.homePageList.push({ value: "Projects", name: activity.name, stateParams: { id: activity.id } });
                });
                self.homePageList.push({ value: "Resources", name: "Resources", stateParams: undefined });
            }


            function initialEnum() {
                initializeHomePageList();
                self.dashboardList = [];

                self.CRUD_List = [
                    "Create",
                    "Read",
                    "Update",
                    "Delete"
                ];

                self.PicklistsList = [
                    { value: "Certificates", name: "Certificates" },
                    { value: "Countries", name: "Countries" },
                    { value: "Departments", name: "Departments" },
                    { value: "Divistions", name: "Divisions" },
                    { value: "Languages", name: "Languages" },
                    { value: "JobTitles", name: "Job Titles" },
                    { value: "ResourceCompanies", name: "Resource Companies" },
                    { value: "Skills", name: "Skills" },
                    { value: "SurveyCategories", name: "Survey Categories" }
                ];

                self.sectionList = [
                    "Schedule",
                    "Patients",
                    "Insurance",
                    "Library",
                    "Resources",
                    "Referral",
                    "Communications",
                    "Billing",
                    "Dashboards",
                ];

                self.configurationSectionList = [
                    "ResourceConfigurationData",
                    "PatientConfigurationData",
                    "GeneralConfigurationData",
                ];
                self.tempSectionList = [
                    { value: "Schedule", name: "Schedule" },
                    { value: "Patients", name: "Patients" },
                    { value: "Insurance", name: "Insurance" },
                    { value: "Library", name: "Library" },
                    { value: "Resources", name: "Resources" },
                    { value: "Referral", name: "Referral" },
                    { value: "Communications", name: "Communications" },
                    { value: "Billing", name: "Billing" },
                    { value: "Dashboards", name: "Dashboards" },
                ];
                self.tempConfigurationSectionList = [
                    { value: "ResourceConfigurationData", name: "Resource Configuration Data" },
                    { value: "PatientConfigurationData", name: "Patient Configuration Data" },
                    { value: "GeneralConfigurationData", name: "General Configuration Data" },
                ];

                self.restrictAccessToList = [
                    { value: "PersonalInfo", name: "Personal Info" },
                    { value: "Salary", name: "Salary" },
                    { value: "BackgroundChecks", name: "Background Checks" },
                    { value: "CriminalRecords", name: "Criminal Records" },
                    { value: "PreviousEmployments", name: "Previous Employments" }
                ];
            }

            function reject() {
                self.callSubmit = false;
            }

            function activate() {

                initialEnum();
                loadInitalData();
            }

            function onCancelClick() {
                $state.go("adminConfigData", { currentTab: "roles" });
            }

            function preSaveModelChanges() {
                /*
                    Udate MODEL is neccesory.
                */
            }

            function onSaveClick(form, callback) {
                callback = callback || function (responce) {
                    $state.go("adminConfigData", { currentTab: "roles" });
                };
                if (self.callSubmit)
                    return;

                if (form.$invalid) {
                    return;
                }

                preSaveModelChanges();

                if (modelId) {
                    roleSetup.update({ id: modelId }, self.model, function (responce) {
                        callback(responce);
                    }, reject);
                } else {
                    roleSetup.save(self.model, function (responce) {
                        callback(responce);
                    }, reject);
                }
                self.callSubmit = true;

            };

            function searchDashboard(search) {
                var defer = $q.defer();
                roleSetup.getdDashboards({
                    pageIndex: 1,
                    pageSize: 10,
                    searchField: "name",
                    searchPhrase: search
                }).$promise.then(function (responce) {
                    defer.resolve(responce.data || []);
                });
                return defer.promise;
            }

            function getValueInAnguAutocomplete(selected) {
                return (selected && selected.originalObject) ? selected.originalObject.id : null;
            }

            function dashboardSelected(selected) {
                self.model.dashboardSetupId = getValueInAnguAutocomplete(selected);
            }

            activate();
            this.onSave_Click = onSaveClick;
            this.onCancel_Click = onCancelClick;
            this.isCheckedCRUDItem = isCheckedCRUDItem;
            this.isCheckboxCrud_Click = isCheckboxCrud_Click;
            this.searchDashboard = searchDashboard;
            this.dashboardSelected = dashboardSelected;
        }
    ]);