﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ClinicLocationMappingExtensions
    {
        public static DbModelBuilder MapClinicLocation(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ClinicLocation>().ToTable("clinic_locations")
                .HasKey(i => i.Id);

            modelBuilder.Entity<Clinic>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ClinicLocation>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ClinicLocation>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            modelBuilder.Entity<ClinicLocation>()
                .Property(p => p.ContactPerson)
                .HasColumnName("contact_person")
                .IsOptional();
            
            modelBuilder.Entity<ClinicLocation>()
                .Property(p => p.Address)
                .HasColumnName("address")
                .IsOptional();

            modelBuilder.Entity<ClinicLocation>()
                .Property(p => p.Email)
                .HasColumnName("email")
                .IsOptional();

            modelBuilder.Entity<ClinicLocation>()
                .Property(p => p.Phone)
                .HasColumnName("phone")
                .IsOptional();

            modelBuilder.Entity<ClinicLocation>()
                .Property(p => p.LocationPrefix)
                .HasColumnName("location_prefix")
                .IsOptional();

            modelBuilder.Entity<ClinicLocation>()
                .Property(p => p.ClinicId)
                .HasColumnName("clinic_id")
                .IsOptional();

            modelBuilder.Entity<ClinicLocation>()
                .HasOptional<Clinic>(p => p.Clinic)
                .WithMany()
                .HasForeignKey(p => p.ClinicId);

            modelBuilder.Entity<ClinicLocation>()
                .Property(p => p.IsDeleted)
                .HasColumnName("is_deleted")
                .IsOptional();

            return modelBuilder;
        }
    }
}
