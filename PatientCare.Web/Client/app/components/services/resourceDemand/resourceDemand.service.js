﻿'use strict';

angular.module('EDZoutstaffingPortalApp')
  .service('ResourceDemand', ['$resource', "$filter", "$translate", function ($resource, $filter, $translate) {
      var resource = $resource('api/ResourceDemand/:id/:controller',
        null, //parameters default
        {
            'update': { method: "PUT" },
            'getResourceCapacity': { method: "GET", isArray: false, params: { controller: "GetResourceCapacity" } },
            'buildCapacityReport': { method: "GET", isArray: true, params: { controller: "BuildCapacityReport" } },
            'getResourceDemandList': { method: "GET", isArray: false, params: { controller: "GetResourceDemandList" } }
        });

      resource.reloadCommand = {};

      resource.getTableOption = {
          pageName: "Demand",
          hideTitle: true,
          pageSize: 10,
          cols: function () {
              return [
                  {
                      field: "demandFrom",
                      title: "Start Date",
                      customViewText: function (row) {
                          return $filter("date")(row.demandFrom, "MM/dd/yyyy");
                      }
                  },
                  {
                      field: "demandPercentage",
                      title: "Demand %",
                      dataType: "number"
                  },
                  {
                      field: "demandTo",
                      title: "End Date",
                      customViewText: function (row) {
                          return $filter("date")(row.demandTo, "MM/dd/yyyy");
                      }
                  },/*
                  {
                      field: "created",
                      title: "Post Date",
                      customViewText: function (row) {
                          return $filter("date")(row.created, "MM/dd/yyyy HH:MM:ss");
                      }
                  },*/
                  {
                      field: "changedBy",
                      title: $filter('translate')("Project") + " / Task",
                      dataType: "text"
                  }
              ];
          }
      };

      return resource;
  }]);
