﻿using PatientCare.Web.ApiMobile.Models.ChatMembers;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.ChatSessions
{
    [DataContract]
    public class ChatSessionResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string ChannelsId { get; set; }

        [DataMember]
        public string ChannelUniqueName { get; set; }

        [DataMember]
        public string ChannelName { get; set; }

        [DataMember]
        public string FromUserId { get; set; }

        [DataMember]
        public string ToUserId { get; set; }

        [DataMember]
        public string CreatedByUserId { get; set; }

        [DataMember]
        public string LastMessage { get; set; }

        [DataMember]
        public int? Type { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public DateTime Modified { get; set; }

        [DataMember]
        public ChatUserDetailResponse[] UserDetails { get; set; }

        //[DataMember]
        //public ChatMemberResponse[] ChatMembers { get; set; }
    }
}