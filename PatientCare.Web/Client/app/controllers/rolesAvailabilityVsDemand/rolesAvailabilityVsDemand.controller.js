"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("RolesAvailabilityVsDemandCtrl", [
        "NgTableParams", "ResourceDemand", "DateTimeHelper", "uuid2", "$state", "AvaialabilityCalculationService", "$stateParams", "$timeout", "toaster", "$scope",
function (ngTableParams, resourceDemand, dateTimeHelper, uuid2, $state, avaialabilityCalculationService, $stateParams, $timeout, toaster, $scope) {

    var self = this;
    console.info($stateParams);


    $scope.popOverHtml = "<table>"+
                        "<tr><td>Target Cases:</td><td>12</td></tr>" +
                        "<tr><td>Achieved:</td><td>13</td></tr>" +
                        "<tr><td><b>Difference:</b></td><td><b>1</b></td></tr>" +
                        "</table>"

    //$('[data-toggle="popover"]').popover();






    function callThisFunction() {
        if ($stateParams && $stateParams.jobTitleId) {
            self.jobTitleSelectId = $stateParams.jobTitleId;
        }
    }

    var dateNow = moment().startOf('month');
    var newDate = dateNow.clone().add(1, "years").subtract(1, "days");
    var changeTypeTable;
    var resourceResponce = [];

    self.onAvailabilityImageClick = function onAvailabilityImageClick() {
        $state.go("resourceCaseList", {});
    }

    self.allResourceButtonClick = function () {
        $state.go("resource");
    }

    function getFormatUtilization(percentage) {
        if (percentage === Number.POSITIVE_INFINITY || percentage === Number.NEGATIVE_INFINITY)
            return "-";
        return percentage.toFixed(0) + "%";
    }

    function checkNumberIsInfinity(number) {
        return number === Number.POSITIVE_INFINITY || number === Number.NEGATIVE_INFINITY;
    }

    function checkMedium(percentage) {
        if (checkNumberIsInfinity(percentage))
            return false;
        return percentage >= 80 && percentage < 100;

    }

    function checkCritical(percentage) {
        if (checkNumberIsInfinity(percentage))
            return true;
        return percentage >= 100;
    }

    function getClassesFromImage(percentage) {
        var value = ((percentage / 10) | 0);
        value = value > 10 ? 10 : value;
        return "donut" + value;
    }

    function getDefaultWorkingHours() {
        if (self.tableRequest.timeRange === "Month") return 168;
        if (self.tableRequest.timeRange === "Year") return 2080;
        return 8;
    }

    function initializeAvailabilityItem(jobTitleId, jobTitleName, resourceId, resourceName, infoByDates) {
        return {
            jobTitleId: jobTitleId,
            jobTitleName: jobTitleName,
            resourceId: resourceId,
            resourceName: resourceName,
            totalHours: 0,
            totalDemandHours: 0,
            totalAvailableHours: 0,
            utilization: 0,
            infoByDates: infoByDates || [],
            resources: [],
            id: uuid2.newguid(),
        }
    }


    /**
        Main call after Server data get. calculates columns and prepares its data.
        @rowData: Server response 
        @result: returns poreccesed data for roles availability vs demand for role and resource.
    */
    function transformResponseByJobTitle(rowData) {
        var result = [];
        self.headers = [];
        var jobTitleGroup = _.groupBy(rowData, "jobTitleId");
        var defaultWorkingHours = getDefaultWorkingHours();
        var pushHeader = "1";

        _.forEach(jobTitleGroup, function (jobTitle, index) {
            var item = jobTitle[0];

            var jobTitleArrayItem = _.find(result, { jobTitleId: jobTitle.jobTitleId })
            if (!jobTitleArrayItem) {
                jobTitleArrayItem = initializeAvailabilityItem(item.jobTitleId, item.jobTitleName, item.resourceId, item.resourceName);
                result.push(jobTitleArrayItem);
            }

            var infoByDatesSelect = [];
            _.forEach(jobTitle, function (jobTitleItem) {
                _.forEach(jobTitleItem.infoByDates, function (infoByte) {
                    infoByte.resourceId = jobTitleItem.resourceId;
                    infoByte.resourceName = jobTitleItem.resourceName;
                    infoByDatesSelect.push(infoByte);
                });
            });
            var groupByInfoByDates = _.groupBy(infoByDatesSelect, "startDate");
            var numberOfResourcesByRole = jobTitle.length;
            var jobTitleObj = {};

            _.forEach(groupByInfoByDates, function (values, key) {
                if (pushHeader == "1") {
                    self.headers.push({
                        name: dateTimeHelper.getColumnName(moment(values[0].startDate), self.tableRequest.timeRange),
                        id: uuid2.newguid()
                    });
                }

                _.forEach(values, function (resourceItem) {
                    var col = {
                        available: resourceItem.available.toFixed(1),
                        demand: resourceItem.demand.toFixed(1),
                        hoursCount: resourceItem.hoursCount,
                        totalHoursCount: resourceItem.totalHoursCount,
                        reservedAvailabilityPercentage: resourceItem.reservedAvailabilityPercentage,
                        scheduledTimeOff: resourceItem.scheduledTimeOff,
                        lessHoliday: resourceItem.lessHoliday,
                        id: uuid2.newguid()
                    };

                    var resourceAvailabilityData = avaialabilityCalculationService.getAvailabilityData(col);
                    resourceAvailabilityData.startDate = key;
                    resourceAvailabilityData.totalDemandResources = (resourceAvailabilityData.totalDemandHours / resourceAvailabilityData.totalHoursCount).toFixed(2);
                    resourceAvailabilityData.totalAvailableResources = (resourceAvailabilityData.availableWrokHours / resourceAvailabilityData.totalHoursCount).toFixed(2);

                    resourceAvailabilityData.isCriticalClass = checkCritical(resourceAvailabilityData.utilization);
                    resourceAvailabilityData.isMediumClass = checkMedium(resourceAvailabilityData.utilization);

                    var periodKey = dateTimeHelper.getColumnName(moment(resourceItem.startDate), self.tableRequest.timeRange);


                    var currentPeriodItem = _.find(jobTitleArrayItem.infoByDates, { periodKey: periodKey })

                    if (!currentPeriodItem) {
                        currentPeriodItem = {};
                        currentPeriodItem.totalHours = 0;
                        currentPeriodItem.totalDemandHours = 0;
                        currentPeriodItem.totalAvailableHours = 0;
                        currentPeriodItem.utilization = 0;

                        currentPeriodItem.startDate = uuid2.newguid();
                        resourceItem.periodKey = periodKey;
                        currentPeriodItem.periodKey = periodKey;
                        jobTitleArrayItem.infoByDates.push(currentPeriodItem);

                    }


                    var resource = _.find(jobTitleArrayItem.resources, { resourceId: resourceItem.resourceId });

                    if (!resource) {
                        resource = initializeAvailabilityItem(item.jobTitleId, item.jobTitleName, resourceItem.resourceId, resourceItem.resourceName);
                        jobTitleArrayItem.resources.push(resource);
                    }

                    currentPeriodItem.totalHours += resourceAvailabilityData.totalHoursCount;
                    currentPeriodItem.totalDemandHours += resourceAvailabilityData.totalDemandHours;
                    currentPeriodItem.totalAvailableHours += resourceAvailabilityData.availableWrokHours;
                    currentPeriodItem.utilization = (parseFloat(currentPeriodItem.utilization) + parseFloat((resourceAvailabilityData.utilization / numberOfResourcesByRole))).toFixed(2);
                    //m.utilization+=parseFloat((n.utilization/p).toFixed(2))
                    //m.utilization = (parseFloat(m.utilization) + parseFloat((n.utilization / p))).toFixed(2);
                    currentPeriodItem.isCriticalClass = checkCritical(currentPeriodItem.utilization);
                    currentPeriodItem.isMediumClass = checkMedium(currentPeriodItem.utilization);

                    currentPeriodItem.totalDemandResources = ((currentPeriodItem.totalDemandHours * numberOfResourcesByRole) / currentPeriodItem.totalHours).toFixed(2);
                    currentPeriodItem.totalAvailableResources = ((currentPeriodItem.totalAvailableHours * numberOfResourcesByRole) / currentPeriodItem.totalHours).toFixed(2);
                    currentPeriodItem.totalResources = numberOfResourcesByRole

                    currentPeriodItem.totalDemandResourcesPercentage = ((currentPeriodItem.totalDemandHours * 100) / currentPeriodItem.totalHours).toFixed(2);
                    currentPeriodItem.totalAvailableResourcesPercentage = ((currentPeriodItem.totalAvailableHours * 100) / currentPeriodItem.totalHours).toFixed(2);

                    resource.infoByDates.push(resourceAvailabilityData);

                    var startDate = key;
                });
            });
            pushHeader = "0";
        });

        result = _.orderBy(result, ["jobTitleName"], ["asc"]);

        return result;
    }


    function setAvailabilityArray(itemName) {

        var sortType = self.sortType;
        self.totalCount = self[itemName].length;

        var parameterSort = self.jobTitleSelectId > 0 ? "resourceName" : "jobTitleName";

        /*  There are issues in Sorting need to check   ComeHere*/
        if (sortType === "asc") {
            self[itemName] = _.sortBy(self[itemName], function (item) {
                return item[parameterSort];
            });
        }
        if (sortType === "desc") {
            self[itemName] = _.sortBy(self[itemName], function (item) {
                return item[parameterSort].charCodeAt() * -1;
            });
        }

        // prepared original data for View.
        self.data = self[itemName];
    }

    function setAvailability() {

        if ($stateParams.jobTitleId > 0 || self.jobTitleSelectId > 0) {
            self.data = self.jobTitles;
        }

        if (self.jobTitleSelectId > 0) {
            var jobTitle = _.filter(self.data, { "jobTitleId": self.jobTitleSelectId });
            if (jobTitle.length > 0 && jobTitle[0].resources) {
                self.resourceResponceSelect = jobTitle[0].resources;
                setAvailabilityArray("resourceResponceSelect");
            } else {
                self.jobTitleSelectId = undefined;
                self.jobTitleName = undefined;
                $timeout(function () { toaster.pop("error", "", "Selected Role does not have any resources."); }, 200);
                setAvailabilityArray("jobTitles");
            }
        } else {
            setAvailabilityArray("jobTitles");
        }
    }
    function getEndDate(startDate, endDate) {
        var endDateFormated = new Date(endDate) //endDate.format("LL");
        var start = new Date(startDate);
        var end = new Date(endDate);
        var maxLimitDate = new Date(start.getFullYear() + 3, start.getMonth(), start.getDate());
        if (end > maxLimitDate) {
            self.tableRequest.end = moment(maxLimitDate);
            return moment(maxLimitDate).format("LL");
        }
        return moment(endDateFormated).format("LL");
    }

    function reClaculateAvailabilityDemandData(needReloadDataFromServer) {
        //self.headers = []; // re-initialized headers for duplication

        if (self.tableRequest.timeRange === "Day") {
            var start = new Date(self.tableRequest.start);
            var nextMonth = new Date(start.getFullYear(), start.getMonth() + 1, start.getDate());
            self.tableRequest.end = moment(nextMonth);
        }

        var model = angular.copy(self.tableRequest);
        model.start = model.start.format("LL");
        model.end = getEndDate(model.start, model.end);
        if ($stateParams.jobTitleId > 0 || self.jobTitleSelectId > 0)
            model.selectedRoleId = $stateParams.jobTitleId || self.jobTitleSelectId;

        if (needReloadDataFromServer) {
            self.loadPage = resourceDemand.buildCapacityReport(model).$promise
            .then(function (response) {
                resourceResponce = response;
                if (response.length === 0) {
                    return;
                }
                // setting job-Titles
                self.jobTitles = transformResponseByJobTitle(response);
                setAvailability();
            });
        } else {
            setAvailability();
        }
    }


    function getDataFromServier(form) {
        if (form.$invalid)
            return;
        //updateModel(true);
        //self.jobTitleSelectId = null;
        //self.jobTitleName = null;
        reClaculateAvailabilityDemandData(true);

    }

    function sort() {
        if (!self.sortType) {
            self.sortType = "asc";
        } else {
            self.sortType = self.sortType === "asc" ? "desc" : "asc";
        }
        //updateModel(false);
        reClaculateAvailabilityDemandData(false);
    }

    function onJobTitleClick(jobTitleId, jobTitleName, needToLoadDataFromServer) {
        changeTypeTable = true;
        self.jobTitleSelectId = jobTitleId;
        self.jobTitleName = jobTitleName;
        //updateModel(false);
        reClaculateAvailabilityDemandData((needToLoadDataFromServer) ? needToLoadDataFromServer : false);
    }


    function onResourceClick(resourceId) {
        $state.go("resourceEdit", { id: resourceId, currentTab: 'availability' });
    }

    function backClick() {
        changeTypeTable = true;

        self.jobTitleSelectId = null;
        self.jobTitleName = null;
        //updateModel(false);
        reClaculateAvailabilityDemandData(false);
    }


    this.tableParams = new ngTableParams({
        count: 100
    },
        {
            counts: [],
            getData: function () {
                return [];
            }

        });

    // Restore to default model state.
    function initialParameter() {
        self.isDetailedMode = false;
        self.headers = [];
        self.timeRangeEnum = dateTimeHelper.dateTypeEnum();
        self.minDate = moment();
        self.jobTitleSelectId = null;
        self.jobTitleName = null;
        self.data = [];
        self.tableRequest = {
            timeRange: "Month",
            start: dateNow,
            end: newDate
        }
    }


    // initial startups.
    function actviate() {
        initialParameter();
        //updateModel(true);

        if ($stateParams.jobTitleId && $stateParams.jobTitleName)
            onJobTitleClick($stateParams.jobTitleId, $stateParams.jobTitleName, true);
        else
            reClaculateAvailabilityDemandData(true);
    }

    this.calculateWightsGren = calculateWightsGren;
    this.calculateWightsRed = calculateWightsRed;

    function calculateWightsGren(col) {
        var WightsGren = (col.available / col.hoursCount) * 100;
        return WightsGren.toFixed(2);
    }
    function calculateWightsRed(col) {
        var WightsRed = (col.demand / col.hoursCount) * 100;
        return WightsRed.toFixed(2);
    }


    /* pagination*/
    function pageChanged() { };
    this.totalItems = 64;
    this.maxSize = 5;
    this.numPages = 5;


    // actviate();

    self.checkCritical = checkCritical;
    self.checkMedium = checkMedium;
    self.back_Click = backClick;
    self.onJobTitle_Click = onJobTitleClick;
    self.sort = sort;
    self.getDataFromServier = getDataFromServier;
    self.getClassesFromImage = getClassesFromImage;
    self.onResource_Click = onResourceClick;
    self.pageChanged = pageChanged;
}
    ]);