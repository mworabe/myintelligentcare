﻿using EDZoutstaffingportal.Core.Entities;
using EDZoutstaffingportal.Core.Services;
using EDZoutstaffingportal.Data.AbstractDataContext;
using EDZoutstaffingportal.Data.AbstractRepository;
using EDZoutstaffingportal.Data.EntityFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDZoutstaffingportal.Services
{
    public class AppUtility : IAppUtility
    {
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        private readonly IResourceService _resourceService;
        private readonly IProjectTaskResourceRepository _projectTaskResourceRepository;
        private readonly IProjectTaskTimeEntryRepository _projectTaskTimeEntryRepository;


        public AppUtility(
              IDataContextScopeFactory dataContextScopeFactory,
              IProjectTaskResourceRepository projectTaskResourceRepository,
              IProjectTaskTimeEntryRepository projectTaskTimeEntryRepository
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _projectTaskResourceRepository = projectTaskResourceRepository;
            _projectTaskTimeEntryRepository = projectTaskTimeEntryRepository;
        }

        public decimal? CalculateTaskEstimateHours(long taskId)
        {
            decimal? result = _projectTaskResourceRepository.List(new FilterQuery<ProjectTaskResource>().AddFilter(x => x.ProjectTaskId == taskId && x.EstimateTime != null)).Sum(x => x.EstimateTime);
            return result;
        }

        public decimal? CalculateTaskTimespent(long taskId)
        {
            decimal? result = _projectTaskTimeEntryRepository.List(new FilterQuery<ProjectTaskTimeEntry>().AddFilter(x => x.ProjectTaskId == taskId )).Sum(x => x.TimeSpent);
            return result;
        }
    }
}
