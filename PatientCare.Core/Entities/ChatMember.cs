﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Chat Member.
    /// </summary>
    public class ChatMember : Entity
    {
        #region Chat Members 

        public long? ChatSessionId { get; set; }
        public virtual ChatSession ChatSession { get; set; }

        public string UserId { get; set; }

        #endregion

    }
}
