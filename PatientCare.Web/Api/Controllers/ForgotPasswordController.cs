﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Services;
using Microsoft.AspNet.Identity;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.Forgotpassword;
using Swashbuckle.Swagger.Annotations;
using PatientCare.Core.Services.Mailing;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Forgot Password operations.
    /// </summary>
    //[Authorize]
    public sealed class ForgotPasswordController : ApiBaseController
    {
        /// <summary>
        /// Description : Forgot Password ITimeManagementService.
        /// </summary>
        private readonly ITimeManagementService _timeManagementService;
        /// <summary>
        /// Description : Forgot Password IEmailSender.
        /// </summary>
        private readonly IEmailSender _emailSender;
        /// <summary>
        /// Description : Forgot Password IMailMessageFactory.
        /// </summary>
        private readonly IMailMessageFactory _messageFactory;

        /// <summary>
        /// Description : Initializes a new instance of the Forgot Password Class.       
        /// </summary>
        /// <param name="timeManagementService"></param>
        /// <param name="emailSender"></param>
        /// <param name="messageFactory"></param>
        public ForgotPasswordController(
                ITimeManagementService timeManagementService,
                IEmailSender emailSender,
                IMailMessageFactory messageFactory
         )
        {
            _timeManagementService = timeManagementService;
            _emailSender = emailSender;
            _messageFactory = messageFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/ForgotPassword/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Create New Forgot Password.   
        /// </summary>
        /// <param name="forgotPasswordRequest">Forgot Password Request Model</param>
        /// <returns>Forgot password Response Model</returns>
        [HttpPost]
        [SwaggerResponse(201, "Created", typeof(ForgotpasswordResponse))]
        [Route("api/ForgotPassword")]
        public IHttpActionResult ForgotPasswordReqeust([FromBody]ForgotpasswordRequest forgotPasswordRequest)
        {
            ForgotpasswordResponse fpr = new ForgotpasswordResponse();
            if (forgotPasswordRequest.Username == null)
            {
                fpr.ResponseType = "error";
                fpr.ResponseMessage = "Username should not be blank.";
                return Ok(fpr);
            }
            var userId = AppUserManager.FindByName(forgotPasswordRequest.Username);
            if (userId == null)
            {
                fpr.ResponseType = "error";
                fpr.ResponseMessage = "Username is not exists in system.";
                return Ok(fpr);
            }
            if (userId.Resource == null)
            {
                fpr.ResponseType = "error";
                fpr.ResponseMessage = "Username doesn't have any resource.";
                return Ok(fpr);
            }
            if (userId.Resource != null && userId.Resource.Email == null)
            {
                fpr.ResponseType = "error";
                fpr.ResponseMessage = "Email not found with this username.";
                return Ok(fpr);
            }

            var request = System.Web.HttpContext.Current.Request;
            string[] hostingInstanceArray = request.Url.Segments[1].ToString().Split('/');
            string hostingInstance = hostingInstanceArray[0].ToString();
            var address = string.Format("{0}://{1}", request.Url.Scheme, request.Url.Authority);
            //string resetLink = address + "/" + hostingInstance + "/reset-password?id=" + userId.Id;
            string resetLink = address + "/reset-password?id=" + userId.Id;

            try
            {
                var message = _messageFactory.ForgotPasswordMailMessage(userId.Resource.Email, userId.Resource.Name, resetLink);
                _emailSender.Send(message);

                fpr.ResponseType = "success";
                fpr.ResponseMessage = "Your request is accepted, please check your email.";
            }
            catch
            {
                fpr.ResponseType = "error";
                fpr.ResponseMessage = "Unable to process your request,please try again later.";
                return Ok(fpr);
            }

            return Ok(fpr);
        }

        /// <summary>
        /// Method Description : Using for check valid User Id.    
        /// </summary>
        /// <param name="userid">User Id</param>
        /// <returns>Returns True/False</returns>
        [HttpGet]
        [Route("api/ForgotPassword/validUser/{userid}")]
        public bool ValidUserId(string userid)
        {
            bool response = false;
            var userId = AppUserManager.FindById(userid);
            if (userId != null)
            {
                response = true;
            }
            return response;
        }

        /// <summary>
        /// Method Description : Using for Update Forgot Password.   
        /// </summary>
        /// <param name="forgotPasswordRequest">Forgot Password Request Model</param>
        /// <returns>Forgot password Response Model</returns>
        [HttpPut]
        [Route("api/ForgotPassword/reset/")]
        public IHttpActionResult ResetPassWord([FromBody]ForgotpasswordRequest forgotPasswordRequest)
        {
            ForgotpasswordResponse fpr = new ForgotpasswordResponse();
            if (forgotPasswordRequest.UserId == null)
            {
                fpr.ResponseType = "error";
                fpr.ResponseMessage = "Unable to process your request, please try again later.";
                return Ok(fpr);
            }

            var userId = AppUserManager.FindById(forgotPasswordRequest.UserId);
            if (userId == null)
            {
                fpr.ResponseType = "error";
                fpr.ResponseMessage = "Unable to process your request, please try again later.";
                return Ok(fpr);
            }

            IdentityResult removeResult = this.AppUserManager.RemovePassword(forgotPasswordRequest.UserId);
            IdentityResult resule = this.AppUserManager.AddPassword(forgotPasswordRequest.UserId, forgotPasswordRequest.NewPassword);

            fpr.ResponseType = "success";
            fpr.ResponseMessage = "Password is reset successfully";

            return Ok(fpr);
        }
    }
}
