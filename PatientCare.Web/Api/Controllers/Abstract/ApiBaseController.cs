﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.Services.WebApi;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using OfficeOpenXml;
using System.Data.Entity;
using PatientCare.Web.Services.FileStorage;

namespace PatientCare.Web.Api.Controllers.Abstract
{
    [ExceptionHandling]
    public abstract class ApiBaseController : ApiController
    {
        protected ApplicationUserManager AppUserManager
        {
            get
            {
                return Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        protected ApplicationRoleManager AppRoleManager
        {
            get
            {
                return Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        protected RoleAccessRights GetRoleAccessRights()
        {
            var userId = User.Identity.GetUserId();
            //if (string.IsNullOrEmpty(userId)) return _forTestingPurpose; // For testing by the swagger
            if (!AppUserManager.Users.Any(user => user.Id.Equals(userId, StringComparison.CurrentCultureIgnoreCase))) return null;
            var roleName = AppUserManager.GetRoles(userId).FirstOrDefault();
            var existedRole = AppRoleManager.FindByName(roleName);
            return new RoleAccessRights
            {
                RoleId = existedRole.Id,
                RoleName = existedRole.Name,
                IsActive = existedRole.IsActive ?? true,
                Description = existedRole.Description,
                AccessRights = string.IsNullOrEmpty(existedRole.JsonSetup) ? new AccessRights() : JsonConvert.DeserializeObject<AccessRights>(existedRole.JsonSetup)
            };
        }

        protected RoleAccessRights GetRoleAccessRightsByResourceId(string resourceId)
        {
            var userId = resourceId;// User.Identity.GetUserId();           
            //if (string.IsNullOrEmpty(userId)) return _forTestingPurpose; // For testing by the swagger
            if (!AppUserManager.Users.Any(x => x.Id.Equals(userId, StringComparison.CurrentCultureIgnoreCase))) return null;
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId.ToString());
            var roleName = AppUserManager.GetRoles(userId).FirstOrDefault();
            var existedRole = AppRoleManager.FindByName(roleName);
            return new RoleAccessRights
            {
                RoleId = existedRole.Id,
                RoleName = existedRole.Name,
                IsActive = existedRole.IsActive ?? true,
                Description = existedRole.Description,
                AccessRights = string.IsNullOrEmpty(existedRole.JsonSetup) ? new AccessRights() : JsonConvert.DeserializeObject<AccessRights>(existedRole.JsonSetup)
            };
        }

        protected string GetExerciseMediaS3Path(long exerciseId)
        {
            return string.Format(FileUploadHelpers.ExerciseMediaFilesTemplate, exerciseId);

        }

        /* private readonly RoleAccessRights _forTestingPurpose = new RoleAccessRights
         {
             Description = "Test description",
             IsActive = true,
             RoleId = "SwaggerTest",
             RoleName = "SwaggerRole",
             AccessRights = new AccessRights
             {
                 //HomePage = HomePage.HumanResourceHome,
                 MainHomePage = new HomePageDynamic(),
                 OnlyForDepartment = false,
                 OnlyForDivision = false,
                 RestrictAccess = new List<RestrictAccessTo> { RestrictAccessTo.PersonalInfo, RestrictAccessTo.CriminalRecords },
                 Sections = new List<AccessItem<Section>>
                 {
                     new AccessItem<Section>
                     {
                         Item = Section.Projects,
                         Actions = new List<Crud>
                         {
                             Crud.Create,
                             Crud.Delete,
                             Crud.Read,
                             Crud.Update
                         }
                     },
                     new AccessItem<Section>
                     {
                         Item = Section.Resources,
                         Actions = new List<Crud>
                         {
                             Crud.Create,
                             Crud.Delete,
                             Crud.Read,
                             Crud.Update
                         }
                     },
                     new AccessItem<Section>
                     {
                         Item = Section.Tasks,
                         Actions = new List<Crud>
                         {
                             Crud.Create,
                             Crud.Delete,
                             Crud.Read,
                             Crud.Update
                         }
                     },
                     new AccessItem<Section>
                     {
                         Item = Section.TimeEntries,
                         Actions = new List<Crud>
                         {
                             Crud.Create,
                             Crud.Delete,
                             Crud.Read,
                             Crud.Update
                         }
                     },
                     new AccessItem<Section>
                     {
                         Item = Section.Finances,
                         Actions = new List<Crud>
                         {
                             Crud.Create,
                             Crud.Delete,
                             Crud.Read,
                             Crud.Update
                         }
                     }
                 },
                 Picklists = new List<Picklist> { Picklist.Certificates, Picklist.Countries, Picklist.Departments }
             }
         };*/

        protected static async Task<ExcelPackage> GetExcelPackage(InMemoryMultipartStreamProvider result)
        {
            var originalFileName = result.Files.First().Headers.ContentDisposition.FileName;
            var ext = Path.GetExtension(JsonConvert.DeserializeObject(originalFileName).ToString());

            var file = result.Files[0];
            var fileStream = await file.ReadAsStreamAsync();

            var excelFile = new ExcelPackage(fileStream);
            return excelFile;
        }

        protected StatusCodeResult GetStatusCodeError(RoleAccessRights roleInfo, Crud action, Section section)
        {
            if (roleInfo == null)
            {
                return StatusCode(HttpStatusCode.Unauthorized);
            }
            return !CheckPermission(roleInfo, action, section) ? StatusCode(HttpStatusCode.Forbidden) : null;
        }

        protected StatusCodeResult GetStatusCodeError(Crud action, Section section)
        {
            var roleInfo = GetRoleAccessRights();
            if (roleInfo == null)
            {
                return StatusCode(HttpStatusCode.Unauthorized);
            }
            return !CheckPermission(roleInfo, action, section) ? StatusCode(HttpStatusCode.Forbidden) : null;
        }

        protected StatusCodeResult GetPicklistStatusCodeError(Picklist picklist)
        {
            var roleInfo = GetRoleAccessRights();
            if (roleInfo == null)
            {
                return StatusCode(HttpStatusCode.Unauthorized);
            }
            return !CheckPicklistPermission(roleInfo, picklist) ? StatusCode(HttpStatusCode.Forbidden) : null;
        }

        protected static bool IsValidEmailAddress(string emailAddress)
        {
            return new System.ComponentModel.DataAnnotations.EmailAddressAttribute().IsValid(emailAddress);
        }

        public string GetOriginalFileNameWithoutBucketGuid(string bucketName)
        {
            var fileName = Path.GetFileName(bucketName);
            return fileName != null ? fileName.Substring(1) : string.Empty;
        }

        protected enum QueryEnum
        {
            FieldBlock = 1,
            JoinBlock = 2,
            WhereBlock = 3,
            SearchBlock = 4,
            OrderBlock = 5,
            Paging = 6
        }

        // TODO Clean up this after testing LanguageMessageHandler
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            //var culture = DetermineBestCulture(controllerContext.Request);

            //Thread.CurrentThread.CurrentCulture = culture;
            //Thread.CurrentThread.CurrentUICulture = culture;

            base.Initialize(controllerContext);
        }

        private static CultureInfo DetermineBestCulture(HttpRequestMessage request)
        {
            var lang = request.GetRouteData().Values["lang"];

            if (lang == null) return CultureInfo.GetCultureInfo("en-US");
            string result;
            return CultureInfo.GetCultureInfo(LanguageDictionary.TryGetValue(lang.ToString(), out result) ? result : "en-US");
        }

        private static bool CheckPermission(RoleAccessRights roleInfo, Crud action, Section section)
        {
            if (roleInfo.RoleName == ApplicationUser.AdminRole)
            {
                return true;
            }
            return action != Crud.None && roleInfo.AccessRights.Sections.Any(x => x.Item == section && x.Actions.Any(y => y == action));
        }

        private static bool CheckPicklistPermission(RoleAccessRights roleInfo, Picklist picklist)
        {
            return roleInfo.RoleName == ApplicationUser.AdminRole || roleInfo.AccessRights.Picklists.Any(x => x == picklist);
        }

        private static readonly Dictionary<string, string> LanguageDictionary = new Dictionary<string, string>
        {
            {"en", "en-US"},
            {"de", "de-DE"}
        };
    }
}