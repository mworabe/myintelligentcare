﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("CompanyConfiguration", ["$resource", function ($resource) {
    var resource = $resource("api/CompanyConfiguration/:id/:controller",
        null, //parameters default
        {
            'update': { method: "PUT" },
            'query': { method: "GET", params: {}, isArray: false }
        });

    resource.getTableOption = {
        pageName: "Company Configuration",
        cols: function () {
            return [
                { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "companyLogo", title: "companyLogo", sortable: "name", show: true, dataType: "text", required: true },
                { field: "companyName", title: "companyName", sortable: "name", show: true, dataType: "text", required: true },
                { field: "companyAddress", title: "companyAddress", sortable: "name", show: true, dataType: "text", required: true },
                { field: "companyPhoneNumber", title: "companyPhoneNumber", sortable: "name", show: true, dataType: "text", required: true },
                { field: "companyEmail", title: "companyEmail", sortable: "name", show: true, dataType: "text", required: true },
                { field: "companyWeb", title: "companyWeb", sortable: "name", show: true, dataType: "text", required: true }
            ];
        }
    };


    resource.fileSrc = "api/CompanyConfiguration/UploadLogo";
    resource.uploadFile = function (uploader, file) {
        var url = this.fileSrc;
        return uploader.upload({
            url: url,
            data: { file: file }
        });
    };

    return resource;
}
]);