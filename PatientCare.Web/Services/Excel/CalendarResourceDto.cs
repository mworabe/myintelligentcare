﻿using System;

namespace PatientCare.Web.Services.Excel
{
    public class CalendarResourceDto
    {
        public long? ResourceId { get; set; }
        public string ResourceName { get; set; }
        public DateTime? AssignTaskStartDate { get; set; }
        public DateTime? AssignTaskEndDate { get; set; }
        public string EmployeeId { get; set; }
        public string EmailId { get; set; }
        public long? SupervisorId { get; set; }
        public string SupervisorName { get; set; }
    }
}
