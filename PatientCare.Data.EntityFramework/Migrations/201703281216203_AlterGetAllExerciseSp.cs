namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterGetAllExerciseSp : DbMigration
    {
        public override void Up()
        {
         //   AlterStoredProcedure(
         //   "dbo.sp_get_all_exercises",
         //   p => new
         //   {
         //       keywords = p.String(defaultValueSql: null),
         //       regionId = p.String(defaultValueSql: null),
         //       positionId = p.String(defaultValueSql: null),
         //       activityId = p.String(defaultValueSql: null),
         //       pageIndex = p.Int(defaultValueSql: null),
         //       pageSize = p.Int(defaultValueSql: null),
         //       responseType = p.String(defaultValueSql: null)
         //   },
         //   body:
         //    @"IF(@responseType = 'DataCount')
    	    //    BEGIN
			      //  select CAST(Count(distinct exe.Id) AS BIGINT) as ItemCount				
			      //  from exercises as exe 
			      //  where ((exe.keywords like ('%'+@keywords+'%')) or (exe.Name like ('%'+@keywords+'%')))
				     //    and (@regionId ='' or Charindex(','+cast(exe.region_id as varchar(max))+',', @regionId) > 0) 
				     //    and (@activityId ='' or Charindex(','+cast(exe.activity_id as varchar(max))+',', @activityId) > 0) 
				     //    and (@positionId ='' or Charindex(','+cast(exe.position_id as varchar(max))+',', @positionId) > 0) 
         //                and ((exe.is_deleted = 'False') or (exe.is_deleted IS NULL))
		       // END
	        //IF(@responseType = 'Data')
    	    //    BEGIN
			      //  select CAST(exe.Id AS BIGINT) as Id				
			      //  from exercises as exe 
			      //  where ((exe.keywords like ('%'+@keywords+'%')) or (exe.Name like ('%'+@keywords+'%')))
				     //    and (@regionId ='' or Charindex(','+cast(exe.region_id as varchar(max))+',', @regionId) > 0) 
				     //    and (@activityId ='' or Charindex(','+cast(exe.activity_id as varchar(max))+',', @activityId) > 0) 
				     //    and (@positionId ='' or Charindex(','+cast(exe.position_id as varchar(max))+',', @positionId) > 0) 
         //                and ((exe.is_deleted = 'False') or (exe.is_deleted IS NULL))
			      //  order by exe.Id asc
			      //  OFFSET(@pageIndex * @pageSize) ROWS FETCH NEXT @pageSize ROWS ONLY
		       // END "
         //              );
        }
        
        public override void Down()
        {
            //            AlterStoredProcedure(
            //"dbo.sp_get_all_exercises",
            //p => new
            //{
            //    keywords = p.String(defaultValueSql: null),
            //    regionId = p.String(defaultValueSql: null),
            //    positionId = p.String(defaultValueSql: null),
            //    activityId = p.String(defaultValueSql: null),
            //    pageIndex = p.Int(defaultValueSql: null),
            //    pageSize = p.Int(defaultValueSql: null),
            //    responseType = p.String(defaultValueSql: null)
            //},
            //body:
            // @"IF(@responseType = 'DataCount')
    	       //             BEGIN
			         //           select CAST(Count(distinct exe.Id) AS BIGINT) as ItemCount				
			         //           from exercises as exe 
			         //           where ((exe.keywords like ('%'+@keywords+'%')) or (exe.Name like ('%'+@keywords+'%')))
				        //             and (@regionId ='' or Charindex(','+cast(exe.region_id as varchar(max))+',', @regionId) > 0) 
				        //             and (@activityId ='' or Charindex(','+cast(exe.activity_id as varchar(max))+',', @activityId) > 0) 
				        //             and (@positionId ='' or Charindex(','+cast(exe.position_id as varchar(max))+',', @positionId) > 0) 
		          //          END
	           //         IF(@responseType = 'Data')
    	       //             BEGIN
			         //           select CAST(exe.Id AS BIGINT) as Id				
			         //           from exercises as exe 
			         //           where ((exe.keywords like ('%'+@keywords+'%')) or (exe.Name like ('%'+@keywords+'%')))
				        //             and (@regionId ='' or Charindex(','+cast(exe.region_id as varchar(max))+',', @regionId) > 0) 
				        //             and (@activityId ='' or Charindex(','+cast(exe.activity_id as varchar(max))+',', @activityId) > 0) 
				        //             and (@positionId ='' or Charindex(','+cast(exe.position_id as varchar(max))+',', @positionId) > 0) 
			         //           order by exe.Id asc
			         //           OFFSET(@pageIndex * @pageSize) ROWS FETCH NEXT @pageSize ROWS ONLY
		          //          END "
            //           );
        }
    }
}
