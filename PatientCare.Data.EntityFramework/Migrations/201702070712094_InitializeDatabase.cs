namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitializeDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.certificates",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true, name: "ix_certificates_name_unique");

            CreateTable(
                "dbo.Cities",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.countries",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true, name: "ix_countries_name_unique");

            CreateTable(
                "dbo.custom_field_properties",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    CustomFieldId = c.Long(nullable: false),
                    value = c.String(maxLength: 1028),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.custom_fields", t => t.CustomFieldId, cascadeDelete: true)
                .Index(t => t.CustomFieldId);

            CreateTable(
                "dbo.custom_fields",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_available = c.Boolean(),
                    type = c.Int(nullable: false),
                    require = c.Boolean(nullable: false),
                    default_value = c.String(),
                    description = c.String(maxLength: 256),
                    IsSearchable = c.Boolean(nullable: false),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true, name: "ix_custom_field_name_unique");

            CreateTable(
                "dbo.resource_custom_field_values",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    custom_field_id = c.Long(nullable: false),
                    resource_id = c.Long(nullable: false),
                    value = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.custom_fields", t => t.custom_field_id, cascadeDelete: true)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .Index(t => t.custom_field_id)
                .Index(t => t.resource_id);

            CreateTable(
                "dbo.resources",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    first_name = c.String(nullable: false, maxLength: 255),
                    middle_name = c.String(maxLength: 255),
                    last_name = c.String(nullable: false, maxLength: 255),
                    overview = c.String(),
                    birth_date = c.DateTime(),
                    martial_status = c.Int(),
                    gender = c.Int(),
                    lgbt = c.Int(),
                    ethnicity = c.String(maxLength: 255),
                    Prefix = c.String(),
                    Suffix = c.String(),
                    active = c.Boolean(),
                    preferred_first_name = c.String(),
                    email = c.String(),
                    address = c.String(maxLength: 255),
                    address2 = c.String(maxLength: 255),
                    address3 = c.String(maxLength: 255),
                    address4 = c.String(maxLength: 255),
                    address5 = c.String(maxLength: 255),
                    address6 = c.String(maxLength: 255),
                    city_id = c.Long(),
                    zipcode = c.String(maxLength: 10),
                    country_id = c.Long(),
                    state_id = c.Long(),
                    timekeeper_number = c.String(),
                    employee_id = c.String(nullable: false, maxLength: 255),
                    job_title_id = c.Long(nullable: false),
                    start_date = c.DateTime(),
                    ssn = c.String(maxLength: 255),
                    resource_company_id = c.Long(nullable: false),
                    end_date = c.DateTime(),
                    location = c.String(maxLength: 255),
                    office_Number = c.String(maxLength: 128),
                    extension = c.String(maxLength: 128),
                    supervisor_id = c.Long(),
                    time_zone_info = c.String(),
                    home_number = c.String(),
                    division_id = c.Long(),
                    relationship = c.Int(),
                    mobile_phone_number = c.String(maxLength: 128),
                    department_id = c.Long(),
                    emergency_contact_name = c.String(),
                    emergency_contact_phone = c.String(),
                    firm_rate = c.Decimal(precision: 18, scale: 2),
                    national_rate = c.Decimal(precision: 18, scale: 2),
                    employment_type = c.Int(),
                    salary = c.Decimal(precision: 18, scale: 2),
                    bonus_or_other_pay = c.Decimal(precision: 18, scale: 2),
                    resource_type = c.Byte(nullable: false),
                    grade_level = c.Int(),
                    years_of_college = c.Int(),
                    years_employed = c.Int(),
                    available_to_travel = c.Boolean(),
                    works_well_in_team = c.Boolean(),
                    valid_passport = c.Boolean(),
                    works_well_alone = c.Boolean(),
                    armed_forces_country_id = c.Long(),
                    armed_forces_branch = c.String(maxLength: 255),
                    armed_forces = c.Boolean(),
                    drug_test = c.String(maxLength: 1023),
                    credit_report_ok = c.String(maxLength: 1023),
                    motor_vehicle_report = c.Int(nullable: false),
                    employment_eligibility_verification = c.Int(nullable: false),
                    international_work_history = c.Int(nullable: false),
                    professional_reference_checks = c.Int(nullable: false),
                    form_i_9 = c.Int(nullable: false),
                    form_e_verify = c.Int(nullable: false),
                    finger_printing = c.Int(),
                    finger_printing_other = c.String(),
                    drug_screening = c.Int(),
                    drug_screening_other = c.String(),
                    credit_check = c.Int(),
                    credit_check_other = c.String(),
                    resource_team_id = c.Long(),
                    college_degree = c.Boolean(),
                    ever_been_convicted_of_crime = c.Boolean(),
                    information_certified_by_employer = c.Boolean(),
                    proof_of_citizenship = c.String(maxLength: 1023),
                    hourly_rate = c.Decimal(precision: 18, scale: 2),
                    range = c.Int(nullable: false),
                    photo_file_name = c.String(maxLength: 255),
                    cv_file_name = c.String(maxLength: 255),
                    productivity = c.Double(),
                    rating = c.Double(),
                    rating_date = c.DateTime(),
                    motor_vehicle_report_other = c.String(maxLength: 1023),
                    professional_reference_checks_other = c.String(maxLength: 1023),
                    employment_eligibility_verification_other = c.String(maxLength: 1023),
                    international_work_history_other = c.String(maxLength: 1023),
                    credential_verifications = c.Int(nullable: false),
                    credential_verifications_other = c.String(maxLength: 1023),
                    form_i_9_other = c.String(maxLength: 1023),
                    form_e_verify_other = c.String(maxLength: 1023),
                    business_unit_id = c.Long(nullable: false),
                    cubical_or_office_number = c.String(maxLength: 100),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.countries", t => t.armed_forces_country_id)
                .ForeignKey("dbo.drop_down_configs", t => t.business_unit_id)
                .ForeignKey("dbo.Cities", t => t.city_id)
                .ForeignKey("dbo.countries", t => t.country_id)
                .ForeignKey("dbo.departments", t => t.department_id)
                .ForeignKey("dbo.divisions", t => t.division_id)
                .ForeignKey("dbo.job_titles", t => t.job_title_id, cascadeDelete: true)
                .ForeignKey("dbo.resource_companies", t => t.resource_company_id, cascadeDelete: true)
                .ForeignKey("dbo.resource_teams", t => t.resource_team_id)
                .ForeignKey("dbo.States", t => t.state_id)
                .ForeignKey("dbo.resources", t => t.supervisor_id)
                .Index(t => t.first_name, name: "non_first_name")
                .Index(t => t.last_name, name: "non_last_name")
                .Index(t => t.gender, name: "non_gender")
                .Index(t => t.active, name: "non_Active")
                .Index(t => t.city_id)
                .Index(t => t.country_id)
                .Index(t => t.state_id)
                .Index(t => t.job_title_id)
                .Index(t => t.resource_company_id)
                .Index(t => t.location, name: "non_location")
                .Index(t => t.supervisor_id)
                .Index(t => t.division_id)
                .Index(t => t.department_id)
                .Index(t => t.years_employed, name: "non_years_employed")
                .Index(t => t.armed_forces_country_id)
                .Index(t => t.resource_team_id)
                .Index(t => t.hourly_rate, name: "non_hourly_rate")
                .Index(t => t.photo_file_name, name: "non_photo_file_name")
                .Index(t => t.rating, name: "non_rating")
                .Index(t => t.business_unit_id);

            CreateTable(
                "dbo.drop_down_configs",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    value = c.String(maxLength: 255),
                    display_name = c.String(maxLength: 255),
                    drop_down_type = c.String(maxLength: 255),
                    config_field_1 = c.String(),
                    config_field_2 = c.String(),
                    config_field_3 = c.String(),
                    config_field_4 = c.String(),
                    config_field_5 = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.resource_certificates",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(nullable: false),
                    certificate_id = c.Long(nullable: false),
                    expirationDate = c.DateTime(),
                    certificate_state_id = c.Long(),
                    issue_date = c.DateTime(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.certificates", t => t.certificate_id, cascadeDelete: true)
                .ForeignKey("dbo.States", t => t.certificate_state_id)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .Index(t => t.resource_id)
                .Index(t => t.certificate_id)
                .Index(t => t.certificate_state_id);

            CreateTable(
                "dbo.States",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.resource_criminal_records",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(nullable: false),
                    date_of_offense = c.DateTime(nullable: false),
                    location_of_offense = c.String(nullable: false, maxLength: 255),
                    offense = c.String(nullable: false, maxLength: 1023),
                    penalty_or_disposition = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .Index(t => t.resource_id);

            CreateTable(
                "dbo.departments",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true, name: "ix_departments_name_unique");

            CreateTable(
                "dbo.divisions",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true, name: "ix_divisions_name_unique");

            CreateTable(
                "dbo.resource_educations",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(nullable: false),
                    subject = c.String(maxLength: 255),
                    name_of_school = c.String(maxLength: 255),
                    date_obtained = c.DateTime(),
                    start_date = c.DateTime(),
                    end_date = c.DateTime(),
                    resource_designation_id = c.Long(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .ForeignKey("dbo.drop_down_configs", t => t.resource_designation_id)
                .Index(t => t.resource_id)
                .Index(t => t.resource_designation_id);

            CreateTable(
                "dbo.resource_industries",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(),
                    industry_id = c.Long(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.drop_down_configs", t => t.industry_id)
                .ForeignKey("dbo.resources", t => t.resource_id)
                .Index(t => t.resource_id)
                .Index(t => t.industry_id);

            CreateTable(
                "dbo.job_titles",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    rate = c.Decimal(precision: 18, scale: 2),
                    priority_order = c.Int(),
                    holiday_eligible = c.Boolean(),
                    hours_edit = c.Boolean(),
                    entry_type_id = c.Long(),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.entry_types", t => t.entry_type_id)
                .Index(t => t.entry_type_id)
                .Index(t => t.name, unique: true, name: "ix_job_title_name_unique");

            CreateTable(
                "dbo.entry_types",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    entry_period = c.Int(),
                    days = c.String(),
                    start_date = c.DateTime(),
                    end_date = c.DateTime(),
                    advanced_create_period = c.Long(),
                    last_entry_date_created = c.DateTime(),
                    name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.resource_previous_employments",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(),
                    address = c.String(),
                    title_of_position = c.String(),
                    duties_of_position = c.String(),
                    reason_for_leaving = c.String(),
                    start_date = c.DateTime(),
                    end_date = c.DateTime(),
                    number_of_supervised = c.Int(),
                    salary = c.Decimal(precision: 18, scale: 2),
                    salary_per = c.Int(),
                    hours_per_week = c.Int(),
                    name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .Index(t => t.resource_id);

            CreateTable(
                "dbo.resource_behavioural_details",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(),
                    behaviour = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id)
                .Index(t => t.resource_id);

            CreateTable(
                "dbo.resource_companies",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true, name: "ix_resource_companies_name_unique");

            CreateTable(
                "dbo.resource_practice_areas",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(),
                    practice_area_id = c.Long(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.drop_down_configs", t => t.practice_area_id)
                .ForeignKey("dbo.resources", t => t.resource_id)
                .Index(t => t.resource_id)
                .Index(t => t.practice_area_id);

            CreateTable(
                "dbo.resource_professional_organizations",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(),
                    organization = c.String(),
                    committee_membership_id = c.Long(),
                    committee_title_id = c.Long(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.drop_down_configs", t => t.committee_membership_id)
                .ForeignKey("dbo.drop_down_configs", t => t.committee_title_id)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .Index(t => t.resource_id)
                .Index(t => t.committee_membership_id)
                .Index(t => t.committee_title_id);

            CreateTable(
                "dbo.resource_recognitions",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(),
                    recognition = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id)
                .Index(t => t.resource_id);

            CreateTable(
                "dbo.resource_teams",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_company_id = c.Long(nullable: false),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resource_companies", t => t.resource_company_id, cascadeDelete: true)
                .Index(t => t.resource_company_id)
                .Index(t => t.name, unique: true, name: "ix_resource_team_name_unique");

            CreateTable(
                "dbo.resource_work_experiences",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(),
                    job_title_id = c.Long(),
                    start_date = c.DateTime(),
                    end_date = c.DateTime(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id)
                .ForeignKey("dbo.job_titles", t => t.job_title_id)
                .Index(t => t.resource_id)
                .Index(t => t.job_title_id);

            CreateTable(
                "dbo.resource_work_assignments",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    assignment_description = c.String(),
                    StartDate = c.DateTime(),
                    end_date = c.DateTime(),
                    resource_work_experience_id = c.Long(),
                    assignment_industry_id = c.Long(),
                    of_note = c.Boolean(),
                    judicial_clerk_ship = c.Boolean(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.drop_down_configs", t => t.assignment_industry_id)
                .ForeignKey("dbo.resource_work_experiences", t => t.resource_work_experience_id)
                .Index(t => t.resource_work_experience_id)
                .Index(t => t.assignment_industry_id);

            CreateTable(
                "dbo.resource_skills",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(nullable: false),
                    skill_id = c.Long(nullable: false),
                    years_of_expirience = c.Int(nullable: false),
                    proficiency = c.Double(),
                    last_updated = c.DateTime(),
                    description = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .ForeignKey("dbo.skills", t => t.skill_id, cascadeDelete: true)
                .Index(t => t.resource_id)
                .Index(t => t.skill_id);

            CreateTable(
                "dbo.skills",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true, name: "ix_skills_name_unique");

            CreateTable(
                "dbo.resource_spoken_languages",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(nullable: false),
                    language_id = c.Long(nullable: false),
                    years_of_expirience = c.Int(nullable: false),
                    speaking_proficiency = c.Int(),
                    writing_proficiency = c.Int(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.languages", t => t.language_id, cascadeDelete: true)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .Index(t => t.resource_id)
                .Index(t => t.language_id);

            CreateTable(
                "dbo.languages",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true, name: "ix_languages_name_unique");

            CreateTable(
                "dbo.resource_survey_results",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(nullable: false),
                    score = c.Int(nullable: false),
                    survey_date = c.DateTime(nullable: false),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .Index(t => t.resource_id);

            CreateTable(
                "dbo.resource_travels",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(nullable: false),
                    visa_or_permit = c.Int(nullable: false),
                    country_id = c.Long(nullable: false),
                    time_remaining_in_days = c.Int(nullable: false),
                    special_considerations = c.String(maxLength: 1023),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.countries", t => t.country_id, cascadeDelete: true)
                .ForeignKey("dbo.resources", t => t.resource_id, cascadeDelete: true)
                .Index(t => t.resource_id)
                .Index(t => t.country_id);

            CreateTable(
                "dbo.dashboard_setups",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    setupjson = c.String(name: "setup-json"),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.holidays",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    location = c.Long(),
                    holiday_date = c.DateTime(),
                    description = c.String(maxLength: 255),
                    name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.leave_requests",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    leave_type_id = c.Long(),
                    supervisor_id = c.Long(),
                    resource_id = c.Long(),
                    start_date = c.DateTime(),
                    end_date = c.DateTime(),
                    status_modified_date = c.DateTime(),
                    reason = c.String(maxLength: 255),
                    status = c.String(maxLength: 255),
                    comment_from_supervisor = c.String(maxLength: 255),
                    total_timeOff_days = c.Long(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.leave_types", t => t.leave_type_id)
                .ForeignKey("dbo.resources", t => t.resource_id)
                .ForeignKey("dbo.resources", t => t.supervisor_id)
                .Index(t => t.leave_type_id)
                .Index(t => t.supervisor_id)
                .Index(t => t.resource_id);

            CreateTable(
                "dbo.leave_types",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    is_paid = c.String(),
                    total_leaves = c.Int(),
                    color = c.String(maxLength: 20),
                    Name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.postal_codes",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    city_id = c.Long(),
                    zip_code = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.city_id)
                .Index(t => t.city_id);

            CreateTable(
                "dbo.resource_score_filter",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    json_filter = c.String(nullable: false),
                    user_id = c.String(nullable: false, maxLength: 255),
                    filter_type = c.String(),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.ApplicationRoleConfigureLayouts",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    type = c.Int(nullable: false),
                    configure = c.String(nullable: false),
                    role_id = c.String(maxLength: 128),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetRoles", t => t.role_id)
                .Index(t => new { t.type, t.role_id }, unique: true, name: "ix_role_configure_layout_unique");

            CreateTable(
                "dbo.AspNetRoles",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 128),
                    Name = c.String(nullable: false, maxLength: 256),
                    json_setup = c.String(),
                    description = c.String(),
                    is_active = c.Boolean(),
                    dashboard_setup_id = c.Long(),
                    Discriminator = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dashboard_setups", t => t.dashboard_setup_id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex")
                .Index(t => t.dashboard_setup_id);

            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                {
                    UserId = c.String(nullable: false, maxLength: 128),
                    RoleId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            CreateTable(
                "dbo.survey_categories",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    name = c.String(nullable: false, maxLength: 255),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.name, unique: true, name: "ix_survey_categories_name_unique");

            CreateTable(
                "dbo.survey_questions",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    text = c.String(nullable: false, maxLength: 255),
                    category_id = c.Long(nullable: false),
                    weight = c.Int(nullable: false),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.survey_categories", t => t.category_id, cascadeDelete: true)
                .Index(t => t.category_id);

            CreateTable(
                "dbo.trial_period",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    trial_period_date = c.DateTime(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.ui_configurations",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    resource_id = c.Long(),
                    key = c.String(maxLength: 255),
                    value = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resources", t => t.resource_id)
                .Index(t => t.resource_id);

            CreateTable(
                "dbo.user_login_history",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    user_id = c.String(),
                    action = c.String(maxLength: 255),
                    log_date_time = c.DateTime(defaultValueSql: "GETUTCDATE()"),
                    ip_address = c.String(maxLength: 255),
                    session_key = c.String(maxLength: 255),
                    user_name = c.String(maxLength: 1024),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.AspNetUsers",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 128),
                    ResourceId = c.Long(),
                    DashboardSetupId = c.Long(),
                    Email = c.String(maxLength: 256),
                    EmailConfirmed = c.Boolean(nullable: false),
                    PasswordHash = c.String(),
                    SecurityStamp = c.String(),
                    PhoneNumber = c.String(),
                    PhoneNumberConfirmed = c.Boolean(nullable: false),
                    TwoFactorEnabled = c.Boolean(nullable: false),
                    LockoutEndDateUtc = c.DateTime(),
                    LockoutEnabled = c.Boolean(nullable: false),
                    AccessFailedCount = c.Int(nullable: false),
                    UserName = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dashboard_setups", t => t.DashboardSetupId)
                .ForeignKey("dbo.resources", t => t.ResourceId)
                .Index(t => t.ResourceId)
                .Index(t => t.DashboardSetupId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");

            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    UserId = c.String(nullable: false, maxLength: 128),
                    ClaimType = c.String(),
                    ClaimValue = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                {
                    LoginProvider = c.String(nullable: false, maxLength: 128),
                    ProviderKey = c.String(nullable: false, maxLength: 128),
                    UserId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.sentemail",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    IdProject = c.Long(nullable: false),
                    From = c.String(),
                    To = c.String(),
                    Subject = c.String(),
                    Body = c.String(),
                    SendedAt = c.DateTime(),
                    EmailType = c.Int(nullable: false),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "ResourceId", "dbo.resources");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "DashboardSetupId", "dbo.dashboard_setups");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ui_configurations", "resource_id", "dbo.resources");
            DropForeignKey("dbo.survey_questions", "category_id", "dbo.survey_categories");
            DropForeignKey("dbo.ApplicationRoleConfigureLayouts", "role_id", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetRoles", "dashboard_setup_id", "dbo.dashboard_setups");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.postal_codes", "city_id", "dbo.Cities");
            DropForeignKey("dbo.leave_requests", "supervisor_id", "dbo.resources");
            DropForeignKey("dbo.leave_requests", "resource_id", "dbo.resources");
            DropForeignKey("dbo.leave_requests", "leave_type_id", "dbo.leave_types");
            DropForeignKey("dbo.resource_travels", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_travels", "country_id", "dbo.countries");
            DropForeignKey("dbo.resource_survey_results", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resources", "supervisor_id", "dbo.resources");
            DropForeignKey("dbo.resources", "state_id", "dbo.States");
            DropForeignKey("dbo.resource_spoken_languages", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_spoken_languages", "language_id", "dbo.languages");
            DropForeignKey("dbo.resource_skills", "skill_id", "dbo.skills");
            DropForeignKey("dbo.resource_skills", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_work_experiences", "job_title_id", "dbo.job_titles");
            DropForeignKey("dbo.resource_work_assignments", "resource_work_experience_id", "dbo.resource_work_experiences");
            DropForeignKey("dbo.resource_work_assignments", "assignment_industry_id", "dbo.drop_down_configs");
            DropForeignKey("dbo.resource_work_experiences", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resources", "resource_team_id", "dbo.resource_teams");
            DropForeignKey("dbo.resource_teams", "resource_company_id", "dbo.resource_companies");
            DropForeignKey("dbo.resource_recognitions", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_professional_organizations", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_professional_organizations", "committee_title_id", "dbo.drop_down_configs");
            DropForeignKey("dbo.resource_professional_organizations", "committee_membership_id", "dbo.drop_down_configs");
            DropForeignKey("dbo.resource_practice_areas", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_practice_areas", "practice_area_id", "dbo.drop_down_configs");
            DropForeignKey("dbo.resources", "resource_company_id", "dbo.resource_companies");
            DropForeignKey("dbo.resource_behavioural_details", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_previous_employments", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resources", "job_title_id", "dbo.job_titles");
            DropForeignKey("dbo.job_titles", "entry_type_id", "dbo.entry_types");
            DropForeignKey("dbo.resource_industries", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_industries", "industry_id", "dbo.drop_down_configs");
            DropForeignKey("dbo.resource_educations", "resource_designation_id", "dbo.drop_down_configs");
            DropForeignKey("dbo.resource_educations", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resources", "division_id", "dbo.divisions");
            DropForeignKey("dbo.resources", "department_id", "dbo.departments");
            DropForeignKey("dbo.resource_custom_field_values", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_criminal_records", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resources", "country_id", "dbo.countries");
            DropForeignKey("dbo.resources", "city_id", "dbo.Cities");
            DropForeignKey("dbo.resource_certificates", "resource_id", "dbo.resources");
            DropForeignKey("dbo.resource_certificates", "certificate_state_id", "dbo.States");
            DropForeignKey("dbo.resource_certificates", "certificate_id", "dbo.certificates");
            DropForeignKey("dbo.resources", "business_unit_id", "dbo.drop_down_configs");
            DropForeignKey("dbo.resources", "armed_forces_country_id", "dbo.countries");
            DropForeignKey("dbo.resource_custom_field_values", "custom_field_id", "dbo.custom_fields");
            DropForeignKey("dbo.custom_field_properties", "CustomFieldId", "dbo.custom_fields");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "DashboardSetupId" });
            DropIndex("dbo.AspNetUsers", new[] { "ResourceId" });
            DropIndex("dbo.ui_configurations", new[] { "resource_id" });
            DropIndex("dbo.survey_questions", new[] { "category_id" });
            DropIndex("dbo.survey_categories", "ix_survey_categories_name_unique");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", new[] { "dashboard_setup_id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.ApplicationRoleConfigureLayouts", "ix_role_configure_layout_unique");
            DropIndex("dbo.postal_codes", new[] { "city_id" });
            DropIndex("dbo.leave_requests", new[] { "resource_id" });
            DropIndex("dbo.leave_requests", new[] { "supervisor_id" });
            DropIndex("dbo.leave_requests", new[] { "leave_type_id" });
            DropIndex("dbo.resource_travels", new[] { "country_id" });
            DropIndex("dbo.resource_travels", new[] { "resource_id" });
            DropIndex("dbo.resource_survey_results", new[] { "resource_id" });
            DropIndex("dbo.languages", "ix_languages_name_unique");
            DropIndex("dbo.resource_spoken_languages", new[] { "language_id" });
            DropIndex("dbo.resource_spoken_languages", new[] { "resource_id" });
            DropIndex("dbo.skills", "ix_skills_name_unique");
            DropIndex("dbo.resource_skills", new[] { "skill_id" });
            DropIndex("dbo.resource_skills", new[] { "resource_id" });
            DropIndex("dbo.resource_work_assignments", new[] { "assignment_industry_id" });
            DropIndex("dbo.resource_work_assignments", new[] { "resource_work_experience_id" });
            DropIndex("dbo.resource_work_experiences", new[] { "job_title_id" });
            DropIndex("dbo.resource_work_experiences", new[] { "resource_id" });
            DropIndex("dbo.resource_teams", "ix_resource_team_name_unique");
            DropIndex("dbo.resource_teams", new[] { "resource_company_id" });
            DropIndex("dbo.resource_recognitions", new[] { "resource_id" });
            DropIndex("dbo.resource_professional_organizations", new[] { "committee_title_id" });
            DropIndex("dbo.resource_professional_organizations", new[] { "committee_membership_id" });
            DropIndex("dbo.resource_professional_organizations", new[] { "resource_id" });
            DropIndex("dbo.resource_practice_areas", new[] { "practice_area_id" });
            DropIndex("dbo.resource_practice_areas", new[] { "resource_id" });
            DropIndex("dbo.resource_companies", "ix_resource_companies_name_unique");
            DropIndex("dbo.resource_behavioural_details", new[] { "resource_id" });
            DropIndex("dbo.resource_previous_employments", new[] { "resource_id" });
            DropIndex("dbo.job_titles", "ix_job_title_name_unique");
            DropIndex("dbo.job_titles", new[] { "entry_type_id" });
            DropIndex("dbo.resource_industries", new[] { "industry_id" });
            DropIndex("dbo.resource_industries", new[] { "resource_id" });
            DropIndex("dbo.resource_educations", new[] { "resource_designation_id" });
            DropIndex("dbo.resource_educations", new[] { "resource_id" });
            DropIndex("dbo.divisions", "ix_divisions_name_unique");
            DropIndex("dbo.departments", "ix_departments_name_unique");
            DropIndex("dbo.resource_criminal_records", new[] { "resource_id" });
            DropIndex("dbo.resource_certificates", new[] { "certificate_state_id" });
            DropIndex("dbo.resource_certificates", new[] { "certificate_id" });
            DropIndex("dbo.resource_certificates", new[] { "resource_id" });
            DropIndex("dbo.resources", new[] { "business_unit_id" });
            DropIndex("dbo.resources", "non_rating");
            DropIndex("dbo.resources", "non_photo_file_name");
            DropIndex("dbo.resources", "non_hourly_rate");
            DropIndex("dbo.resources", new[] { "resource_team_id" });
            DropIndex("dbo.resources", new[] { "armed_forces_country_id" });
            DropIndex("dbo.resources", "non_years_employed");
            DropIndex("dbo.resources", new[] { "department_id" });
            DropIndex("dbo.resources", new[] { "division_id" });
            DropIndex("dbo.resources", new[] { "supervisor_id" });
            DropIndex("dbo.resources", "non_location");
            DropIndex("dbo.resources", new[] { "resource_company_id" });
            DropIndex("dbo.resources", new[] { "job_title_id" });
            DropIndex("dbo.resources", new[] { "state_id" });
            DropIndex("dbo.resources", new[] { "country_id" });
            DropIndex("dbo.resources", new[] { "city_id" });
            DropIndex("dbo.resources", "non_Active");
            DropIndex("dbo.resources", "non_gender");
            DropIndex("dbo.resources", "non_last_name");
            DropIndex("dbo.resources", "non_first_name");
            DropIndex("dbo.resource_custom_field_values", new[] { "resource_id" });
            DropIndex("dbo.resource_custom_field_values", new[] { "custom_field_id" });
            DropIndex("dbo.custom_fields", "ix_custom_field_name_unique");
            DropIndex("dbo.custom_field_properties", new[] { "CustomFieldId" });
            DropIndex("dbo.countries", "ix_countries_name_unique");
            DropIndex("dbo.certificates", "ix_certificates_name_unique");
            DropTable("dbo.sentemail");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.user_login_history");
            DropTable("dbo.ui_configurations");
            DropTable("dbo.trial_period");
            DropTable("dbo.survey_questions");
            DropTable("dbo.survey_categories");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.ApplicationRoleConfigureLayouts");
            DropTable("dbo.resource_score_filter");
            DropTable("dbo.postal_codes");
            DropTable("dbo.leave_types");
            DropTable("dbo.leave_requests");
            DropTable("dbo.holidays");
            DropTable("dbo.dashboard_setups");
            DropTable("dbo.resource_travels");
            DropTable("dbo.resource_survey_results");
            DropTable("dbo.languages");
            DropTable("dbo.resource_spoken_languages");
            DropTable("dbo.skills");
            DropTable("dbo.resource_skills");
            DropTable("dbo.resource_work_assignments");
            DropTable("dbo.resource_work_experiences");
            DropTable("dbo.resource_teams");
            DropTable("dbo.resource_recognitions");
            DropTable("dbo.resource_professional_organizations");
            DropTable("dbo.resource_practice_areas");
            DropTable("dbo.resource_companies");
            DropTable("dbo.resource_behavioural_details");
            DropTable("dbo.resource_previous_employments");
            DropTable("dbo.entry_types");
            DropTable("dbo.job_titles");
            DropTable("dbo.resource_industries");
            DropTable("dbo.resource_educations");
            DropTable("dbo.divisions");
            DropTable("dbo.departments");
            DropTable("dbo.resource_criminal_records");
            DropTable("dbo.States");
            DropTable("dbo.resource_certificates");
            DropTable("dbo.drop_down_configs");
            DropTable("dbo.resources");
            DropTable("dbo.resource_custom_field_values");
            DropTable("dbo.custom_fields");
            DropTable("dbo.custom_field_properties");
            DropTable("dbo.countries");
            DropTable("dbo.Cities");
            DropTable("dbo.certificates");
        }
    }
}
