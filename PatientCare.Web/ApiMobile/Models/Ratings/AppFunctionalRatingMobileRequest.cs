﻿namespace PatientCare.Web.ApiMobile.Models.Ratings
{
    public class AppFunctionalRatingMobileRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public long? PatientCaseId { get; set; }

        public int? FunctionalRating { get; set; }

        public string Comment { get; set; }
    }
}