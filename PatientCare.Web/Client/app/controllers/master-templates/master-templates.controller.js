"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("MasterTemplatesCntrl", ["$scope", 'ExerciseService', 'MasterTemplateService', '$stateParams', 'Modal', "$state",
        function ($scope, exerciseService, MasterTemplateService, $stateParams, modal, $state) {
            var self = this;

            self.tabs = [{
                id: "all-exercise",
                title: "ALL EXERCISES",
            }, {
                id: "templates",
                title: "TEMPLATES"
            }];

            self.sortOptions = [
                { value: "name", title: "By Name" },
                { value: "created", title: "By Date" }
            ]


            self.selectedSortTitle = "Sort By";
            self.onSortSelected = function (item) {
                self.selectedSortTitle = item.title;
                self.queryParams.sortField = item.value;
                self.loadExercise();
            }
            self.onSortOrderSelected = function () {
                if (self.queryParams.sortOrder == "Descending") {
                    self.queryParams.sortOrder = "Ascending";
                } else if (self.queryParams.sortOrder == "Ascending") {
                    self.queryParams.sortOrder = "Descending";
                }
                self.loadExercise();
            }

            self.search = {};
            function resetParams() {
                self.queryParams = {
                    pageIndex: 1,
                    pageSize: 20,
                    sortField: "name",
                    sortOrder: "Ascending",
                    searchPhrase: ""
                }
                self.selectedSortTitle = "Sort By";
            }
            resetParams();
            self.onPageSearch = function () {
                if (self.search.oldValue == self.search.newValue) {
                    return;
                } else {
                    resetParams();
                    self.queryParams.searchPhrase = self.search.newValue;
                    self.search.oldValue = self.search.newValue;
                    self.loadExercise();
                }
            }

            self.loadExercise = function () {
                self.isLoading = exerciseService.getAll(self.queryParams).$promise.then(
                    function (response) {
                        self.exerciseItem = response.data;
                        self.exerciseTotalItems = response.itemsCount;
                    },
                    function (error) {
                    });

            }


            if ($stateParams.tab) {
                self.selectedTab = self.tabs[$stateParams.tab].id;
            } else {
                self.selectedTab = self.tabs[0].id;
            }
            self.isTabSelected = function (id) {
                if (id == self.selectedTab) return true;
                return false;
            }
            self.setSelectedTab = function (id) {
                self.selectedTab = id;
            }

            
            self.exerciseTotalItems = 0;
            self.exerciseItem = [];
            self.loadExercise();


            var self = this;
            this.get = MasterTemplateService.query;
            this.cols = MasterTemplateService.getTableOption;
            this.delete = MasterTemplateService.delete;
            this.cols.hideBulkEdit = true;

            self.deleteExercise = function (item) {
                modal.confirm.delete(function () {
                    self.isLoading = exerciseService.delete({ id: item.id }).$promise.then(
                    function (response) {
                        $state.go($state.current, {}, { reload: true });
                    },
                    function (error) {

                    });
                })("Delete Exercise", "Exercise");
            }



        }
    ]);