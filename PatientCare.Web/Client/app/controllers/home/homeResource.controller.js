﻿'use strict';

angular.module('EDZoutstaffingPortalApp')
    .controller('HomeResourceCtrl', [
        "Home", 
        function (homeResource) {
            this.get = homeResource.query;
            this.cols = homeResource.getTableOption;
        }]);
