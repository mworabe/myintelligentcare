﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceCompanyRepository : RepositoryBase<ResourceCompany>
    {
        public ResourceCompanyRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
