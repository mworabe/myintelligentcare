﻿"use strict";
var EOPApp = angular.module("EDZoutstaffingPortalApp")
EOPApp.service("AppointmentService", ["$resource", "$state", "$injector", '$filter', 'AppointmentFactory', function ($resource, $state, $injector, $filter, AppointmentFactory) {


    var resource = $resource("api/patientAppointments/:id/:controller", null, {
        "query": { method: "GET", params: { controller: "list" }, isArray: false },
        "update": { method: "PUT", params: {}, isArray: false },
        "getTodaysAppointment": { method: "GET", params: { controller: "user" }, isArray: false },
        "changeStatus": {
            method: "PUT",
            url: "api/patientAppointments/:id/:appointmentStatus",
            params: {},
            isArray: false
        },
    });



    resource.getTableOption = {
        formatDelete: function (row) {
            return row.name;
        },
        //disabledAddButton: function () {
        //    return !getPermissionWorker().canResourceInsert();
        //},
        //disabledDeleteButton: function () {
        //    return getPermissionWorker().canResourceDelete();
        //},
        //disabledEditButton: function () {
        //    return getPermissionWorker().canResourceUpdate();
        //},

        cols: function () {
            return [
               {
                   field: "startDate",
                   title: "Time",
                   sortable: "startDate",
                   show: true,
                   dataType: "text",
                   dataTypeView: "custom-html",
                   templateHTML: function (row) {
                       return "<div class=''>" + moment.utc(row.startDate).local().format("hh:mm a") + "</div>";
                   },
               },
                {
                    field: "patientName",
                    title: "Patient Name",
                    sortable: "patientName",
                    show: true,
                    dataTypeView: "custom-html",
                    dataType: "text",
                    templateHTML: function (row) {
                        var patientId = (row.patientId || uuid2.newguid().substring(0, 5).replace(/-/g, '_'));
                        var photoUrl = "Client/assets/images/userIco.png";
                        if (row.photoFileNameUrl)
                            photoUrl = row.photoFileNameUrl;
                        function getPatientURL() { return $state.href("patientsEdit", { id: row.id }); };
                        return "<div class=\"custom-html-container\">" +
                                    "<img class=\"img\" src=\"" + photoUrl + "\" width=\"30\" height=\"30\" />" +
                                    "<div class=\"detail-section\">" +
                                        "<div class=\"custom-html-title\"><a href=\"" + getPatientURL() + "\">" + row.patientName + "</a></div>" +
                                        "<div class=\"custom-html-sub-title\"><span>Patient ID:</span> <span>" + (row.patientNumber || "123456878") + "</span></div>" +
                                    "</div>" +
                                "</div>";
                    },
                    customViewText: function (row) {
                        return row.name;
                    },
                    buildUrl: function (row) {
                        return $state.href("patientsEdit", { id: row.patientId });
                    }
                }, {
                    field: "caseNo",
                    title: $filter('translate')('Case ID'),
                    sortable: "caseNo",
                    show: true,
                    dataType: "text",
                    dataTypeView: "href",
                    buildUrl: function (row) {
                        return $state.href("patientCaseEdit", { patientId: row.patientId, caseId: row.caseId });
                    }
                }, {
                    field: "clinicianName",
                    title: "Assigned Clinician",
                    sortable: "clinicianName",
                    show: true,
                    dataType: "text",
                    customViewText: function (row) {
                        return (row.clinicianName) ? row.clinicianName : "";
                    },
                }, {
                    field: "injuryRegion",
                    title: "Injury Region",
                    sortable: "injuryRegionName",
                    show: true,
                    dataType: "text",
                    customViewText: function (row) {
                        return (row.injuryRegionName) ? row.injuryRegionName : "";
                    },
                }, {
                    field: "insurance",
                    title: "Insurance",
                    sortable: "insurance",
                    show: true,
                    dataType: "text",
                    customViewText: function (row) {
                        return (row.insurance) ? row.insurance : "";
                    },
                }, {
                    field: "totalVisit",
                    title: "Visit To Date",
                    sortable: "totalVisit",
                    show: true,
                    dataType: "text",
                    customViewText: function (row) {
                        return (row.totalVisit) ? row.totalVisit : "";
                    },
                }, {
                    field: "status",
                    title: "Status",
                    sortable: "status",
                    show: true,
                    dataType: "text",
                    dataTypeView: "iconStatus",
                    getBoxClass: function (row) {
                        var status = AppointmentFactory.getStatusFromText(row.appointmentStatus);
                        if (status) {
                            return status.boxClass;
                        }
                        return "status-box-blue"
                    },
                    getIconClass: function (row) {
                        var status = AppointmentFactory.getStatusFromText(row.appointmentStatus);
                        if (status) {
                            return status.icon;
                        }
                        return "status-box-green"
                    },

                }, {
                    field: "relatedTo",
                    title: "Related To",
                    sortable: "relatedTo",
                    show: true,
                    dataType: "text",
                    customViewText: function (row) {
                        return (row.relatedTo) ? row.relatedTo : "";
                    },
                }, {
                    field: "action",
                    title: "Action",
                    class: "",
                    dataType: "command"
                }
            ];
        },
        hideAddButton: true,
        showDropDownAddButton: false,
        dropDownAddButtonOptions: [
            {
                title: "New Patient",
                state: "patientsEdit",
                onOptionClick: function () {
                    $state.go("patientsEdit");
                }
            }, {
                title: "New appointment",
                state: "add-appointment",
                onOptionClick: function () {
                    AppointmentFactory.newAppointment({}, function (result) {

                    });
                }
            }, {
                title: "New Refral Source",
                state: "add-refral-source",
                onOptionClick: function () {
                    $state.go("add-refral-source");
                }
            },
        ],
        onAdd_Click: function () {
            $state.go("patientsEdit");
        },
        onEdit_Click: function (row) {
            $state.go("patientsEdit", { id: row.id });
        },
        pageName: "Today's Appointment",
        menuButtons: [
            {
                click: function (row) {
                    resource.setInactive({ id: row.id }, {}, function () {
                        resource.reloadCommand.reload();
                    });
                },

                iconClass: "icon icon-user-times"
            }
        ],
        headerButtons: [],
        headerLeftItems: []
    };





    return resource;
}]);
