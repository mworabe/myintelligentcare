﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("CbaProject", [
        "$resource", "$state", function($resource, $state) {
            var resource = $resource("api/CbaProjects/:controller/:id/:versionId",
                null, //parameters default
                {
                    'update': { method: "PUT" },
                    'query': { method: "GET", params: {}, isArray: false },
                    'cbaProjectByVersion': { method: "GET", params: {}, isArray: false },
                    'projectVersions': { method: "GET", params: { controller: "Versions" }, isArray: false }
                });

            resource.importSrc = "api/CbaProjects/import";
            resource.import = function (uploader, file, projectId, cb) {
                cb = cb || angular.noop;
                var url = this.importSrc + "?id=" + projectId;
                uploader.upload({
                    url: url,
                    data: { file: file }
                }).then(cb);
            };

            resource.getTableOption = {
                hideActionButtons: true,
                cols: function() {
                    return [
                        { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                        { field: "userName", title: "User Name", sortable: "userName", show: true, dataType: "text", required: true },
                        {
                            field: "versionId",
                            title: "Version",
                            sortable: "versionId",
                            show: true,
                            dataType: "text",
                            required: true,
                            dataTypeView: "href",
                            buildUrl: function(row) {
                                return $state.href("cba", { projectId: row.projectId, versionId: row.versionId });
                            }
                        },
                        { field: "created", title: "Created", sortable: "created", show: true, dataType: "text", required: true },
                    ];
                }
            };

            return resource;
        }
    ]);