namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreatedSPofGetSingleAppointmentPUSH : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
             "dbo.sp_get_single_appointment_signalR",
             p => new
             {
                 appointmentId = p.Long(defaultValueSql: null)
             },
             body:
                 @"SELECT  CAST(pa.Id AS  BIGINT) AS AppointmentId,CAST(res.Id AS  BIGINT) AS ClinicianId,(res.last_name +' '+res.first_name) AS ClinicianName,
    	CAST(p.Id AS  BIGINT) AS PatientId,(p.last_name +' '+ p.first_name) AS PatientName,P.patient_number AS PatientNumber,
    	p.photo_file_name AS PhotoFileName,p.birth_DATE AS BirthDATE,p.cell_number AS CellNumber,p.email AS Email,P.patient_number, 
    	CAST(pa.case_id AS  BIGINT) AS CaseId,pcase.case_no AS CaseNo,pcase.case_status AS CaseStatus,
    	CAST(pcase.injury_region_id AS  BIGINT) AS InjuryRegionId,injury.name AS InjuryRegionName,
    	CAST(pcase.primay_diagnosis_id  AS  BIGINT)  AS PrimayDiagnosisId,diag.name AS PrimayDiagnosisName,pcase.return_to AS ReturnTo,
    	CAST(pcase.refering_physician_id AS  BIGINT)  AS ReferingPhysicianId,(refphy.last_name +' '+ refphy.first_name) AS ReferingPhysicianName,		
    	CAST(pa.appointment_type_id AS  BIGINT)  AS AppointmentTypeId,aptype.Name AS AppointmentTypeName,aptype.color AS AppointmentTypeColor,	
    	CAST(pa.facility_id AS  BIGINT)  AS FacilityId,pa.clinic_location_id AS ClinicLocationId,	
    	pa.start_DATE AS StartDATE,pa.end_DATE AS EndDATE,appointment_status AS AppointmentStatus,
    pa.is_mobile as IsMobile,pa.is_reschedule as IsReschedule 
    FROM patient_appointments pa
    	LEFT OUTER JOIN patients p ON pa.patient_id = p.Id
    	LEFT OUTER JOIN patient_cases pcase ON pa.case_id = pcase.Id
    	LEFT OUTER JOIN resources res ON pa.clinician_id = res.Id
    	LEFT OUTER JOIN appointment_types aptype ON pa.appointment_type_id = aptype.Id
    	LEFT OUTER JOIN injury_regions AS injury ON pcase.injury_region_id = injury.Id
    	LEFT OUTER JOIN referral_physicians AS refphy ON pcase.refering_physician_id = refphy.Id
    	LEFT OUTER JOIN diagnosis AS diag ON pcase.primay_diagnosis_id = diag.Id
    WHERE pa.Id = @appointmentId"
           );
        }
        
        public override void Down()
        {
        }
    }
}
