namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedPatientParentGuardianTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.patients", name: "cell_number_is_primary", newName: "home_number_is_primary");
            CreateTable(
                "dbo.patient_parents_guardian",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    patient_id = c.Long(),
                    first_name = c.String(nullable: false, maxLength: 255),
                    last_name = c.String(nullable: false, maxLength: 255),
                    relationship_to_patient = c.Int(),
                    address = c.String(maxLength: 255),
                    city_id = c.Long(),
                    state_id = c.Long(),
                    country_id = c.Long(),
                    zipcode = c.String(maxLength: 10),
                    cell_number = c.String(),
                    home_number_is_primary = c.Boolean(),
                    home_number = c.String(),
                    HomeNumberIsPrimary = c.Boolean(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.city_id)
                .ForeignKey("dbo.countries", t => t.country_id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.States", t => t.state_id)
                .Index(t => t.patient_id)
                .Index(t => t.city_id)
                .Index(t => t.state_id)
                .Index(t => t.country_id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.patient_parents_guardian", "state_id", "dbo.States");
            DropForeignKey("dbo.patient_parents_guardian", "patient_id", "dbo.patients");
            DropForeignKey("dbo.patient_parents_guardian", "country_id", "dbo.countries");
            DropForeignKey("dbo.patient_parents_guardian", "city_id", "dbo.Cities");
            DropIndex("dbo.patient_parents_guardian", new[] { "country_id" });
            DropIndex("dbo.patient_parents_guardian", new[] { "state_id" });
            DropIndex("dbo.patient_parents_guardian", new[] { "city_id" });
            DropIndex("dbo.patient_parents_guardian", new[] { "patient_id" });
            DropTable("dbo.patient_parents_guardian");
            RenameColumn(table: "dbo.patients", name: "home_number_is_primary", newName: "cell_number_is_primary");
        }
    }
}
