﻿"use strict";

(function edzWeekTimeCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzWeekTimeCustomFieldEditOptions", [
            edzWeekTimeCustomFieldOptions
        ]);

    function edzWeekTimeCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/weektime/edzWeekTimeCustomFieldEdit.html",
            restrict: "E",
            bindToController: {
                model: "=model"
            },
            controller: ["$scope", function edzAddCustomFieldCtrl($scope) {
            }],
            controllerAs: "edzWeekTimeCustomFieldEditOptionsCtrl"
        };
    }
})();
