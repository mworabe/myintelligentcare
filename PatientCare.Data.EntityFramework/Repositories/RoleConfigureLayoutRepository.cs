﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class RoleConfigureLayoutRepository : RepositoryBase<RoleConfigureLayout>,
        IRoleConfigureLayoutRepository
    {

        public RoleConfigureLayoutRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
            
        }
    }
}
