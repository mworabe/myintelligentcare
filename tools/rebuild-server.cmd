@echo off

call "%~dp0run-msbuild" "..\EDZoutstaffingportal.sln" Rebuild %~1 || goto :error

goto :done

:error
echo Server build failed with error %errorlevel%.

:done
exit /b %errorlevel%
