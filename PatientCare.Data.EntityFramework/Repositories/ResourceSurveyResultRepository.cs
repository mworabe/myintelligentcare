﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceSurveyResultRepository : RepositoryBase<ResourceSurveyResult>, IResourceSurveyResultRepository
    {
        public ResourceSurveyResultRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
