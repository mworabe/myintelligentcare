﻿namespace PatientCare.Web.Api.Models
{
    public class FileResponce
    {
        public string S3Bucketurl { get; set; }
        public bool IsDelete { get; set; }
        public string Name { get; set; }
    }
}