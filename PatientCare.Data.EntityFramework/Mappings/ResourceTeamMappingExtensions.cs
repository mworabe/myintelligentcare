﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceTeamMappingExtensions
    {
        public static DbModelBuilder MapResourceTeam(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.MapPicklistInfo<ResourceTeam>("resource_teams", "ix_resource_team_name_unique");

            modelBuilder.Entity<ResourceTeam>()
                .Property(p => p.ResourceCompanyId)
                .HasColumnName("resource_company_id")
                .IsRequired();

            modelBuilder.Entity<ResourceTeam>()
                .HasRequired<ResourceCompany>(p => p.ResourceCompany)
                .WithMany()
                .HasForeignKey(p => p.ResourceCompanyId);

            modelBuilder.Entity<ResourceTeam>()
                .HasMany<Resource>(p => p.Resources)
                .WithOptional()
                .HasForeignKey(item => item.ResourceTeamId);

            return modelBuilder;
        }
    }
}
