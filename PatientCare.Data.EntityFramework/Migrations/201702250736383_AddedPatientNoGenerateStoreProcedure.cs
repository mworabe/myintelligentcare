namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddedPatientNoGenerateStoreProcedure : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
               "dbo.sp_patient_no_generate",
               p => new
               {
                   patientid = p.Long()
               },
               body:
                   @"Declare @newPatientNo nvarchar(max);
                     Declare @clinicPrefix nvarchar(max);
                     declare @locationPrefix nvarchar(max);

                     select @clinicPrefix = c.clinic_prefix from patients p left Outer Join clinics c on p.clinic_id = c.Id where p.Id = @patientid
                     select @locationPrefix = cl.location_prefix from patients p left Outer Join clinic_locations cl on p.clinic_location_id = cl.Id where p.Id = @patientid

                     SET @newPatientNo = @clinicPrefix +'-'+ @locationPrefix + '-' + Convert(nvarchar(max), @patientid)

                     update patients set patient_number = @newPatientNo where Id = @patientid
                    "
           );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_patient_no_generate");
        }
    }
}
