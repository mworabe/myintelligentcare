﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PatientCare.Web.Api.Models.Holidays
{
    public class HolidayListRequest : ListRequest
    {
        //[DataMember(Name = "holidate")]        
        //public DateTime? HolidayDate { get; set; }

        [DataMember(Name = "filter")]
        public string filter { get; set; }
    }
}