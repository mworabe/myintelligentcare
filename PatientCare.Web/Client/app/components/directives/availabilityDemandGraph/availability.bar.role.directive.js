﻿"use strict";
var app = angular.module("EDZoutstaffingPortalApp");

app.directive("availabilityBarRoleField", [function () {
    return {
        templateUrl: "Client/app/components/directives/availabilityDemandGraph/availability-bar-role-view.html",
        restrict: "E",
        replace: false,
        scope: {
            column: '=ngColumn'
        },
        link: function (scope) {

            scope.availabilityData = scope.column;
        }
    }
}
]);