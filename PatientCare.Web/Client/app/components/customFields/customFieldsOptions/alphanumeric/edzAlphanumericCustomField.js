"use strict";
(function edzAlphanumericCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzAlphanumericCustomField", ["edzBaseCustomField", testCustomField]);

    ///////////////////////////////////////////////////////
    function testCustomField(baseCustomField) {
        function AlphanumericCustomField() { }

        AlphanumericCustomField.prototype = Object.create(baseCustomField);
        AlphanumericCustomField.prototype.optionsTemplate = "<edz-alphanumeric-custom-field-options></edz-alphanumeric-custom-field-options>";
        AlphanumericCustomField.prototype.optionsTemplateEdit = "<edz-alphanumeric-custom-field-edit-options></edz-alphanumeric-custom-field-edit-options>";
        return AlphanumericCustomField;
    }
})();