﻿using FluentValidation;
using PatientCare.Web.Api.Models.ExerciseActivities;

namespace PatientCare.Web.Api.Models.DropDownConfigs
{
    public class ExerciseActivityRequestValidator : AbstractValidator<ExerciseActivityRequest>
    {
        public ExerciseActivityRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);            
        }
    }
}