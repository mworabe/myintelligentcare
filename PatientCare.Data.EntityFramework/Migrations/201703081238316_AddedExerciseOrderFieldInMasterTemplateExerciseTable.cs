namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedExerciseOrderFieldInMasterTemplateExerciseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.master_template_exercises", "exercise_order", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.master_template_exercises", "exercise_order");
        }
    }
}
