﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceScore
{
    [DataContract]
    public class AdvanceSearchFilterResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Company { get; set; }

        [DataMember]
        public string JobTitle { get; set; }

        [DataMember]
        public string Department { get; set; }

        [DataMember]
        public string Division { get; set; }

        [DataMember]
        public double Score { get; set; }

        [DataMember]
        public string PhotoFileNameUrl { get; set; }

        [DataMember]
        public double? Productivity { get; set; }
    }
}