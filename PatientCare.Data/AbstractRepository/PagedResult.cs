﻿namespace PatientCare.Data.AbstractRepository
{
    public class PagedResult<T>
    {
        public T[] Entities { get; set; }
        public long TotalCount { get; set; }
    }
}
