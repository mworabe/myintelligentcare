﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services.ResourceScoreCalculation;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ResourceScore;
using PatientCare.Web.Services.FileStorage;
using Thinktecture.IdentityModel.Extensions;
using PatientCare.Data.AbstractDataContext;
using System.Threading.Tasks;
using PatientCare.Data.EntityFramework;
using System.Data.Entity;
using PatientCare.Web.Api.Models.AdvanceSearches;
using Microsoft.AspNet.Identity;
using System.Text;
using PatientCare.Web.Api.Models;
using Newtonsoft.Json;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resources Score operations.
    /// </summary>
    [Authorize]
    public class ResourceScoreController : ApiBaseController
    {
        /// <summary>
        /// Description : Resources Score IResourceScoreService.
        /// </summary>
        private readonly IResourceScoreService _resourceScoreService;
        /// <summary>
        /// Description : Resources Score IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : Resources Score IResourceRepository.
        /// </summary>
        private readonly IResourceRepository _resourceRepository;

        /// <summary>
        /// Description : Initializes a new instance of the Resources Score Class.    
        /// </summary>
        /// <param name="resourceScoreService">IResource Score Service</param>
        /// <param name="resourceRepository">IResource Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public ResourceScoreController(IResourceScoreService resourceScoreService, IResourceRepository resourceRepository, IDataContextScopeFactory dataContextScopeFactory)
        {
            _resourceScoreService = resourceScoreService;
            _resourceRepository = resourceRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        /// <summary>
        /// Method Description : Calculate Resource Scores. 
        /// </summary>
        /// <param name="filterRequest">Score Calculation Filter Request Model</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="pageSize">Page Size</param>
        /// <returns>Score Calculation Filter Response Model</returns>
        [HttpPost]
        public IHttpActionResult CalculateResourceScores([FromBody] ScoreCalculationFilterRequest filterRequest, int? pageIndex = null, int? pageSize = null)
        {
            var roleInfo = GetRoleAccessRights();
            var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }

            pageSize = (pageSize != null) ? pageSize.Value : 20;
            pageIndex = ((pageIndex != null) ? pageIndex.Value : 1);

            var filter = Mapper.Map<ScoreCalculationFilterRequest, ScoreCalculationFilter>(filterRequest);

            FixMandatory(filterRequest);

            var resources = ApplyRestriction(roleInfo, _resourceScoreService.Calculate(filter, pageIndex.Value, pageSize.Value));

            var result = resources.Select(x => new ScoreCalculationFilterResponse
            {
                Id = x.Item1.Id,
                Name = x.Item1.Name,
                Department = (x.Item1.Department != null ? string.Join(", ", x.Item1.Department.Name) : string.Empty),
                JobTitle = (x.Item1.JobTitle != null ? string.Join(", ", x.Item1.JobTitle.Name) : string.Empty),
                Division = (x.Item1.Division != null ? string.Join(", ", x.Item1.Division.Name) : string.Empty),
                Score = Math.Round(x.Item2, 0),
                PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(x.Item1.PhotoFileName, string.Empty),
                Productivity = x.Item1.Productivity,
            })
            .OrderByDescending(o => o.Score).ToList();


            var response = new ListResponse<ScoreCalculationFilterResponse>();
            response.Data = result.ToArray();
            response.ItemsCount = resources.Select(x => x.Item3).FirstOrDefault();
            return Ok(response);
        }

        private IEnumerable<Tuple<Resource, double, long>> ApplyRestriction(RoleAccessRights roleInfo,
            IEnumerable<Tuple<Resource, double, long>> list)
        {
            if (roleInfo.AccessRights.IsForDepartmentOnly ||
                roleInfo.AccessRights.IsForDivisionOnly)
            {
                long resourceId;
                var claimsIdentity = (ClaimsIdentity)User.Identity;
                var userResourceId = claimsIdentity.Claims.GetValue("ResourceId");

                if (long.TryParse(userResourceId, out resourceId))
                {
                    var resourceUser = _resourceRepository.Read(resourceId, ReadOptions.Create<Resource>().AsReadOnly());

                    if (roleInfo.AccessRights.IsForDepartmentOnly && resourceUser.DepartmentId != null)
                    {
                        list = list.Where(x => x.Item1.DepartmentId == resourceUser.DepartmentId);
                    }

                    if (roleInfo.AccessRights.IsForDivisionOnly && resourceUser.DivisionId != null)
                    {
                        list = list.Where(x => x.Item1.DivisionId == resourceUser.DivisionId);
                    }
                }
            }
            return list;
        }

        private static void FixMandatory(ScoreCalculationFilterRequest filterRequest)
        {
            if (filterRequest.Country != null) filterRequest.Country.Mandatory = true;
            if (filterRequest.RangeType != null) filterRequest.RangeType.Mandatory = true;
            if (filterRequest.Company != null) filterRequest.Company.Mandatory = true;
            if (filterRequest.Division != null) filterRequest.Division.Mandatory = true;
            if (filterRequest.Department != null) filterRequest.Department.Mandatory = true;
            if (filterRequest.HourlyRate != null) filterRequest.HourlyRate.Mandatory = true;
            if (filterRequest.GradeLevel != null) filterRequest.GradeLevel.Mandatory = true;
        }

        /// <summary>
        /// Method Description : Using for Get Advance Filter Fields.  
        /// </summary>
        /// <param name="fields">Fields</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/ResourceScore/advanceFilter")]
        public IHttpActionResult GetAdvanceFilterFields(string fields)
        {
            List<FieldsListResponse> responceList = FieldsListResponse.GetResourceFilterFields(fields);

            return Ok(responceList);
        }

        /// <summary>
        /// Method Description : Using for Get Global Search.  
        /// </summary>
        /// <param name="filter">Filter Value</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="searchType">Search Type</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/search")]
        public IHttpActionResult GetGlobalSearchData(string filter = null, int? pageIndex = null, int? pageSize = null, SearchType? searchType = null)
        {
            filter = (filter != null) ? filter.Replace("'", "''") : filter;

            GlobalSearchResponse response = new GlobalSearchResponse();
            response.SearchType = searchType;
            response.pageIndex = pageIndex;

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                try
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                    if (searchType == SearchType.Resource || searchType == SearchType.All)
                    {
                        string resourceSearch = "EXEC sp_resource_global_search @pageIndex = " + (pageIndex - 1) * pageSize + ",@pageSize = " + pageSize + ",@searchTextContain = N'" + filter.Trim().ToLower() + "'";
                        var query = ctx.Database.SqlQuery<GlobalResoourceSearchData>(resourceSearch).ToList();
                        var lst = new List<GlobalResoourceSearchData>();
                        lst = query.ToList();
                        if (lst != null && lst.Count > 0)
                        {
                            foreach (var item in lst)
                            {
                                item.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.photoFileName, "");
                                if (item.DataString != null && item.DataString != "")
                                {
                                    string convertToJson = RemoveFakeCharInString(item.DataString);
                                    JsonDataModel[] list = JsonConvert.DeserializeObject<JsonDataModel[]>(convertToJson);
                                    item.Data = list.ToArray();
                                }
                            }
                            response.Resource = new ListResponse<GlobalResoourceSearchData>(lst.ToArray(), lst.FirstOrDefault().ItemsCount);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Global Search", ex.Message.ToString());
                    return BadRequest(ModelState);
                }
            }
            return Ok(response);
        }

        private string RemoveFakeCharInString(string fakeString)
        {
            StringBuilder sb = new StringBuilder(fakeString);
            sb.Replace(",,", ",");
            sb.Replace("},]", "}]");
            sb.Replace("\n", " ");
            sb.Replace("\t", " ");
            //sb.Replace(@"\\", @"\\\\");
            return sb.ToString();
        }

        /// <summary>
        /// Method Description : Using for Get Advanced Filter Data.
        /// </summary>
        /// <param name="advanceSearchFilterRequest">Advance Search Filter Request Model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ResourceScore/advanced-filter")]
        public IHttpActionResult GetAdvancedFilterData([FromBody] AdvanceSearchFilterRequest advanceSearchFilterRequest)
        {
            var roleInfo = GetRoleAccessRights();
            var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }

            var lst = new ListResponse<AdvanceSearchFilterResponse>();

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                try
                {
                    if (advanceSearchFilterRequest.Where != null || advanceSearchFilterRequest.Where != "")
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}", advanceSearchFilterRequest.Where.Trim().ToString());
                        sb.Append("\r\n");

                        if (roleInfo.AccessRights.IsForDepartmentOnly || roleInfo.AccessRights.IsForDivisionOnly)
                        {
                            var userId = User.Identity.GetUserId();
                            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);

                            if (user != null && user.Resource != null)
                            {
                                if (roleInfo.AccessRights.IsForDepartmentOnly)
                                {
                                    sb.AppendFormat(" and r.Department_id = {0}", user.Resource.DepartmentId);
                                }
                                if (roleInfo.AccessRights.IsForDivisionOnly)
                                {
                                    sb.AppendFormat(" and r.division_id = {0}", user.Resource.DivisionId);
                                    //sb.Append("\r\n");
                                    //sb.Append("( select distinct resource_id from resource_divisions ");
                                    //sb.Append("\r\n");
                                    //sb.AppendFormat("  where division_id in(select distinct division_id from resource_divisions where resource_id = {0})", user.ResourceId);
                                    //sb.Append("\r\n");
                                    //sb.Append(")");
                                    //sb.Append("\r\n");
                                }
                            }
                        }

                        var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                        string s = "exec  Sp_Resources_Advance_Filter @critaria ='" + sb.ToString().Trim() + "'";

                        var query = ctx.Database.SqlQuery<AdvanceSearchFilterResponse>(s).ToList();
                        List<AdvanceSearchFilterResponse> data = query.ToList();

                        lst.ItemsCount = data.Count;
                        lst.Data = data.Skip((advanceSearchFilterRequest.PageIndex - 1) * advanceSearchFilterRequest.PageSize).Take(advanceSearchFilterRequest.PageSize).ToArray();
                        foreach (var item in lst.Data)
                        {
                            item.PhotoFileNameUrl = FileUploadHelpers.GetStorageUrl(item.PhotoFileNameUrl, "");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Advance Search Filter", ex.Message.ToString());
                    return BadRequest(ModelState);
                }
            }
            return Ok(lst);
        }
    }
}
