﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PatientGoalMappingExtensions
    {
        public static DbModelBuilder MapPatientGoal(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<PatientGoal>().ToTable("patient_goals").HasKey(i => i.Id);

            modelBuilder.Entity<PatientGoal>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<PatientGoal>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<PatientGoal>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<PatientGoal>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<PatientGoal>()
                .Property(p => p.PatientCaseId)
                .HasColumnName("patient_case_id")
                .IsOptional();

            modelBuilder.Entity<PatientGoal>()
                .HasOptional<PatientCase>(p => p.PatientCase)
                .WithMany()
                .HasForeignKey(p => p.PatientCaseId);

            modelBuilder.Entity<PatientGoal>()
                .Property(p => p.GoalType)
                .HasColumnName("goal_type")
                .IsOptional();

            modelBuilder.Entity<PatientGoal>()
                .Property(p => p.GoalName)
                .HasColumnName("goal_name")
                .IsOptional();

            modelBuilder.Entity<PatientGoal>()
                .Property(p => p.PatientMobileProfileId)
                .HasColumnName("patient_mobile_profile_id")
                .IsOptional();

            modelBuilder.Entity<PatientGoal>()
                .HasOptional<PatientMobileProfile>(p => p.PatientMobileProfile)
                .WithMany()
                .HasForeignKey(p => p.PatientMobileProfileId);

            return modelBuilder;
        }
    }
}
