﻿using System;

namespace PatientCare.Web.Api.Models.ResourceSurveyResults
{
    public class ResourceSurveyResultRequest
    {
        public long ResourceId { get; set; }

        public string Name { get; set; }

        public int Score { get; set; }

        public DateTime SurveyDate { get; set; }
    }
}