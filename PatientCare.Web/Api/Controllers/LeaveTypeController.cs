﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.LeaveTypes;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using PatientCare.Web.Api.Models;
using System.Linq.Expressions;
using System.Linq;

namespace PatientCare.Web.Api.Controllers
{
    [Authorize]
    public sealed class LeaveTypeController : ApiBaseController
    {

        private readonly ITimeManagementService _timeManagementService;
        private readonly ILeaveTypeRepository _leaveTypeRepository;
        ReadOptions<LeaveType> _readOptions = new ReadOptions<LeaveType>();
        public LeaveTypeController(ITimeManagementService timeManagementService,
                                ILeaveTypeRepository leaveTypeRepository)
        {
            _timeManagementService = timeManagementService;
            _leaveTypeRepository = leaveTypeRepository;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/LeaveTypes/" + id, UriKind.Relative);
        }

        [HttpPost]
        [SwaggerResponse(201, "Created", typeof(LeaveTypeResponse))]
        public async Task<IHttpActionResult> CreateLeaveType([FromBody]LeaveTypeRequest leaveTypeRequest)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Create, Section.TimeOff);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var leaveType = Mapper.Map<LeaveTypeRequest, LeaveType>(leaveTypeRequest);
            await _timeManagementService.CreateLeaveTypeAsync(leaveType);

            return Created(GetEntityLocation(leaveType.Id), Mapper.Map<LeaveType, LeaveTypeResponse>(leaveType));
        }

        [HttpPut]
        public async Task<IHttpActionResult> EditLeaveType(long id, [FromBody] LeaveTypeRequest leaveTypeRequest)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Update, Section.TimeOff);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var leaveType = Mapper.Map<LeaveTypeRequest, LeaveType>(leaveTypeRequest);
            leaveType.Id = id;

            await _timeManagementService.UpdateLeaveTypeAsync(leaveType);

            return Ok(Mapper.Map<LeaveType, LeaveTypeResponse>(leaveType));
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteLeaveTypeItem(long id)
        {
            var success = await _timeManagementService.DeleteLeaveTypeAsync(id);
            if (!success)
            {
                return NotFound();
            }

            return Ok(new LeaveType { Id = id });
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetLeaveType(long id)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}
            var leaveType = await _timeManagementService.ReadLeaveTypeAsync(id);
            if (leaveType == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<LeaveType, LeaveTypeResponse>(leaveType);
            return Ok(viewModel);
        }

        [HttpGet]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<LeaveTypeResponse>))]
        public IHttpActionResult GetAllLeaveType([FromUri]LeaveTypeListRequest request)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.TimeOff);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            var query = PagingExtensions<LeaveType>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            var readOptions = new ReadOptions<LeaveType>().AsReadOnly();
            var page = _leaveTypeRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<LeaveType[], LeaveTypePicklistResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<LeaveTypePicklistResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        private static Expression<Func<LeaveType, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return leaveType => leaveType.Name.Contains(searchPhrase)
                || leaveType.Color.Contains(searchPhrase)
                 || leaveType.IsPaid.Contains(searchPhrase);
        }

        private static Expression<Func<LeaveType, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return leaveType => leaveType.Name.Contains(searchPhrase);
        }

        [HttpGet]
        [SwaggerResponse(200, "SearchLeaveType", typeof(ListResponse<LeaveTypeAutoCompleteResponse>))]
        public IHttpActionResult SearcLeaveType([FromUri]string search)
        {
            var page = _timeManagementService.SearcLeaveTypeAsync(search);
            var dtos = Mapper.Map<LeaveType[], LeaveTypeAutoCompleteResponse[]>(page.ToArray());

            var response = new List<LeaveTypeAutoCompleteResponse>(dtos);

            return Ok(response);
        }
    }
}
