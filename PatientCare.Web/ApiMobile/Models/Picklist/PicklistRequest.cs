﻿using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.Picklist
{
    [DataContract]
    public class PicklistRequest
    {
        [DataMember]
        public string Name { get; set; }        
    }
}