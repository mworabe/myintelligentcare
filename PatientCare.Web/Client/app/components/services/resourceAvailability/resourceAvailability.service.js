﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
'use strict';

angular.module('EDZoutstaffingPortalApp')
  .service('ResourceAvailability', ['$resource', "$filter", function ($resource, $filter) {
      var resource = $resource('api/ResourceAvailabilities/:id/:controller',
        null, //parameters default
        {
            'update': { method: "PUT" },
            'getResourceTable': { method: "GET", isArray: true, params: { controller: "GetResourceTable" } },
            'buildCapacityReport': { method: "GET", isArray: true, params: { controller: "BuildCapacityReport" } },
            'getResourceAvailabilityList': { method: "GET", isArray: false, params: { controller: "GetResourceAvailabilityList" } }
        });

      resource.reloadCommand = {};

      resource.getTableOption = {
          pageName: "Availabilities",
          hideTitle: true,
          pageSize: 10,
          cols: function () {
              return [
                  {
                      field: "availabilityFrom",
                      title: "Start Date",
                      customViewText: function (row) {
                          return $filter("date")(row.availabilityFrom, "MM/dd/yyyy");
                      }
                  },
                  {
                      field: "maxAvailabilityPercentage",
                      title: "Availability %",
                      dataType: "number"
                  },
                  {
                      field: "availabilityTo",
                      title: "End Date",
                      customViewText: function (row) {
                          return $filter("date")(row.availabilityTo, "MM/dd/yyyy");
                      }
                  },
                  {
                      field: "changedBy",
                      title: "Changed By",
                      dataType: "text"
                  }
              ];
          }
      };

      return resource;
  }]);
