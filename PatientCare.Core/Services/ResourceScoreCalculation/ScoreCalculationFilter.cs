using System.Collections.Generic;
using PatientCare.Core.Entities;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Services.ResourceScoreCalculation
{
    public class ScoreCalculationFilter
    {
        public ScoreCalculationFilter()
        {
            Skills = new List<ScoreFilterItem<ResourceSkill>>();
            SpokenLanguages = new List<ScoreFilterItem<ResourceSpokenLanguage>>();
            WrittenLanguages = new List<ScoreFilterItem<ResourceSpokenLanguage>>();
            Certificates = new List<ScoreFilterItem<long>>();
            AvailabilityVsDemand = new List<ScoreFilterItem<AvailabilityVsDemandFilter>>();
        }
        public IList<ScoreFilterItem<ResourceSkill>> Skills { get; set; }

        public IList<ScoreFilterItem<ResourceSpokenLanguage>> SpokenLanguages { get; set; }

        public IList<ScoreFilterItem<ResourceSpokenLanguage>> WrittenLanguages { get; set; }

        public IList<ScoreFilterItem<long>> Certificates { get; set; }

        public ScoreFilterItem<string> TimeZoneInfo { get; set; }

        public ScoreFilterItem<long?> Country { get; set; }

        public ScoreFilterItem<bool> WorksWellInTeam { get; set; }

        public ScoreFilterItem<bool> WorksWellAlone { get; set; }

        public ScoreFilterItem<double> Rating { get; set; }

        public ScoreFilterItem<double> Productivity { get; set; }

        public ScoreFilterItem<string> Education { get; set; }

        public ScoreFilterItem<string> JobTitle { get; set; }

        public ScoreFilterItem<long?> Company { get; set; }

        public ScoreFilterItem<long?> Division { get; set; }

        public ScoreFilterItem<long?> Department { get; set; }

        public ScoreFilterItem<RangeTypeEnum?> RangeType { get; set; }

        public ScoreFilterItem<ResourceTypeEnum?> ResourceType { get; set; }

        public ScoreFilterItem<decimal?> HourlyRate { get; set; }

        public ScoreFilterItem<int?> GradeLevel { get; set; }

        public ScoreFilterItem<string> Location { get; set; }

        public IList<ScoreFilterItem<AvailabilityVsDemandFilter>> AvailabilityVsDemand { get; set; }
    }
}