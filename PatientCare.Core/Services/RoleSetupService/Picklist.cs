﻿namespace PatientCare.Core.Services.RoleSetupService
{
    public enum Picklist
    {
        Certificates = 0,
        Countries = 1,
        Departments = 2,
        Divistions = 3,
        Languages = 4,
        JobTitles = 5,
        ResourceCompanies = 6,
        Skills = 7,
        SurveyCategories = 8,
        States = 9,
        Cities = 10
    }
}
