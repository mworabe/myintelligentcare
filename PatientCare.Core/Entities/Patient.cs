﻿using System;
using System.Collections.Generic;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Patient.
    /// </summary>
    public class Patient : Entity
    {
        #region Patient Info Division

        public string Prefix { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string NickName { get; set; }

        public DateTime? BirthDate { get; set; }

        public bool? IsMinor { get; set; }

        public string SSN { get; set; }

        public GenderEnum? Gender { get; set; }

        public string Email { get; set; }

        public ContactMethodEnum? PrefferedContactMethod { get; set; }

        public string CellNumber { get; set; }

        public bool? CellNumberIsPrimary { get; set; }

        public string HomeNumber { get; set; }

        public bool? HomeNumberIsPrimary { get; set; }

        public long? LanguageId { get; set; }

        public virtual Language Language { get; set; }

        public string SpecialAccomodations { get; set; }

        public MartialStatusEnum? MaritialStatus { get; set; }

        public string EmergencyContact { get; set; }

        public RelationshipEnum? RelationshipToContact { get; set; }

        public string EmergencyPhone { get; set; }

        public string EmployerName { get; set; }

        public string EmployerPhone { get; set; }

        public long? ReferralPhysicianId { get; set; }

        public virtual ReferralPhysician ReferralPhysician { get; set; }

        public long? ClinicId { get; set; }

        public virtual Clinic Clinic { get; set; }

        public long? ClinicLocationId { get; set; }

        public virtual ClinicLocation ClinicLocation { get; set; }

        public string PatientNumber { get; set; }

        public long? ClinicianId { get; set; }
        public virtual Resource Clinician { get; set; }

        public bool? IsMedicarePatient { get; set; }

        public bool? HasMobileAccess { get; set; }

        public string PhotoFileName { get; set; }

        public PatientStatusEnum? Status { get; set; }

        public string WebPTPatientId { get; set; }

        public bool? IsDeleted { get; set; }

        #endregion

        // Need to show Patient entity as Picklist        
        public string Name { get { return string.Concat(LastName ?? string.Empty, " ", FirstName ?? string.Empty); } }

        public virtual ICollection<PatientAddress> Addresses { get; set; }

        public virtual ICollection<PatientParentGuardian> ParentGaurdians { get; set; }        
    }
}
