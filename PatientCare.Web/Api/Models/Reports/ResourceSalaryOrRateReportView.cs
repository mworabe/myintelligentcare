﻿using PatientCare.Core.Enums;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Reports
{
    [DataContract]
    public class ResourceSalaryOrRateReportView
    {
        [DataMember]
        public string EmployeeId { get; set; }
        [DataMember]
        public string ResourceName { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public string Department { get; set; }
        [DataMember]
        public string Division { get; set; }
        [DataMember]
        public EmploymentTypeEnum? EmploymentType { get; set; }
        [DataMember]
        public decimal? Salary { get; set; }
        [DataMember]
        public decimal? BonusOrOtherPay { get; set; }
        [DataMember]
        public decimal? Rate { get; set; }
    }
}