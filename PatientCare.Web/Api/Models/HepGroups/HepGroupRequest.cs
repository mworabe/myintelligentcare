﻿using PatientCare.Web.Api.Models.HepDetails;

namespace PatientCare.Web.Api.Models.HepGroups
{
    public class HepGroupRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long? PatientId { get; set; }

        public long? PatientCaseId { get; set; }

        public int? ExerciseOrder { get; set; }

        public long? HepId { get; set; }

        public HepDetailRequest [] HepDetails { get; set; }
    }   
}