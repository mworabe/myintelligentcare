﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceTravels
{
    public class ResourceTravelRequestValidator : AbstractValidator<ResourceTravelRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IResourceRepository _resourceRepository;
        private readonly IPicklistRepository<Country> _countryRepository;

        public ResourceTravelRequestValidator(IDataContextScopeFactory dbContextScopeFactory, IResourceRepository resourceRepository, IPicklistRepository<Country> countryRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _resourceRepository = resourceRepository;
            _countryRepository = countryRepository;

            RuleFor(model => model.CountryId)
                .NotEmpty()
                .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _countryRepository, l))
                .WithMessage("The country with provided Id doesn't exists");

            RuleFor(model => model.TimeRemainingInDays)
                .NotEmpty()
                .GreaterThanOrEqualTo(1);
            
            RuleFor(model => model.SpecialConsiderations)
                .Length(0, 1023);

            RuleFor(model => model.VisaOrPermit)
                .NotNull();

            //RuleFor(model => model.ResourceId)
            //    .NotEmpty()
            //    .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _resourceRepository, l))
            //    .WithMessage("The resource with provided Id doesn't exists");

        }
    }
}