﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourcePreviousEmploymentMappingExtensions
    {
        public static DbModelBuilder MapResourcePreviousEmployment(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourcePreviousEmployment>().ToTable("resource_previous_employments").HasKey(i => i.Id);

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.Address)
                .HasColumnName("address")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.TitleOfPosition)
                .HasColumnName("title_of_position")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.DutiesOfPosition)
                .HasColumnName("duties_of_position")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.ReasonForLeaving)
                .HasColumnName("reason_for_leaving")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.StartDate)
                .HasColumnName("start_date")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.NumberOfSupervised)
                .HasColumnName("number_of_supervised")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.Salary)
                .HasColumnName("salary")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.SalaryPer)
                .HasColumnName("salary_per")
                .IsOptional();

            modelBuilder.Entity<ResourcePreviousEmployment>()
                .Property(p => p.HoursPerWeek)
                .HasColumnName("hours_per_week")
                .IsOptional();

            return modelBuilder;
        }
    }
}
