﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IUserLoginHistoryRepository : IRepository<UserLoginHistory>
    {
    }
}
