﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PatientCare.Data.AbstractRepository
{
    public static class FilterQuery
    {
        public static FilterQuery<T> Create<T>()
        {
            return new FilterQuery<T>();
        }
    }

    public class FilterQuery<T>
    {
        protected readonly List<Expression<Func<T, bool>>> FilterExpressionsInternal = new List<Expression<Func<T, bool>>>();

        public Expression<Func<T, bool>>[] FilterExpressions { get { return FilterExpressionsInternal.ToArray(); } }

        public FilterQuery<T> AddFilter(Expression<Func<T, bool>> expression)
        {
            if (expression == null) { throw new ArgumentNullException("expression"); }
            FilterExpressionsInternal.Add(expression);
            return this;
        }

        public FilterQuery<T> AddFilters(IEnumerable<Expression<Func<T, bool>>> expressions)
        {
            if (expressions == null) { throw new ArgumentNullException("expressions"); }
            FilterExpressionsInternal.AddRange(expressions);
            return this;
        }

        public FilterQuery<T> AddFilterIf(bool condition, Expression<Func<T, bool>> expression)
        {
            if (condition)
            {
                return AddFilter(expression);
            }
            return this;
        }

        public FilterQuery<T> AddFiltersIf(bool condition, IEnumerable<Expression<Func<T, bool>>> expressions)
        {
            if (condition)
            {
                return AddFilters(expressions);
            }
            return this;
        }

        public FilterQuery<T> AddFilterIf(bool condition, Expression<Func<T, bool>> expressionIfTrue, Expression<Func<T, bool>> expressionIfFalse)
        {
            return AddFilter(condition ? expressionIfTrue : expressionIfFalse);
        }

        public FilterQuery<T> AddFiltersIf(bool condition, IEnumerable<Expression<Func<T, bool>>> expressionsIfTrue, IEnumerable<Expression<Func<T, bool>>> expressionsIfFalse)
        {
            return AddFilters(condition ? expressionsIfTrue : expressionsIfFalse);
        }
    }
}
