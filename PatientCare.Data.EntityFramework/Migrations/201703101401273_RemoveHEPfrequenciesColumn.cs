namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveHEPfrequenciesColumn : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.hep_exercise_frequency", "hep_id", "dbo.hep_master");
            DropForeignKey("dbo.hep_exercise_frequency", "hep_detail_id", "dbo.hep_detail");
            DropIndex("dbo.hep_exercise_frequency", new[] { "hep_detail_id" });
            RenameColumn(table: "dbo.hep_exercise_frequency", name: "hep_id", newName: "HepDetail_Id");
            RenameIndex(table: "dbo.hep_exercise_frequency", name: "IX_hep_id", newName: "IX_HepDetail_Id");
            DropColumn("dbo.hep_exercise_frequency", "hep_detail_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.hep_exercise_frequency", "hep_detail_id", c => c.Long());
            RenameIndex(table: "dbo.hep_exercise_frequency", name: "IX_HepDetail_Id", newName: "IX_hep_id");
            RenameColumn(table: "dbo.hep_exercise_frequency", name: "HepDetail_Id", newName: "hep_id");
            CreateIndex("dbo.hep_exercise_frequency", "hep_detail_id");
            AddForeignKey("dbo.hep_exercise_frequency", "hep_detail_id", "dbo.hep_detail", "Id");
            AddForeignKey("dbo.hep_exercise_frequency", "hep_id", "dbo.hep_master", "Id");
        }
    }
}
