﻿'use strict';

angular.module("EDZoutstaffingPortalApp")
    .factory('SignalrClient', ['$rootScope', 'backendHubProxy', '$timeout', 'SoundFactory', "$location", "APP_CONFIG",
        function ($rootScope, backendHubProxy, $timeout, SoundFactory, $location, APP_CONFIG) {
            var connection;
            var proxy;

            
            var url = APP_CONFIG.signalrUrl;


            function initialize() {
                connection = $.hubConnection(url);
                proxy = connection.createHubProxy('appointmentBroadCasterHub');
                proxy.on("broadcastAppointmentUpdate", function (data) {
                    $rootScope.$broadcast('onNewAppointment', data);

                });
                proxy.on("broadCastPainRating", function (data) {
                    SoundFactory.playNotifSound();
                    $rootScope.$broadcast('onPainRatingAlert', data);

                });
                connection.start().done(function () {
                    console.info("On Connection Established");
                });

            }
            function disconnect() {
                if (connection) {
                    connection.stop();
                }
            }

            return {
                connect: function () {
                    initialize();
                },
                shutDown: function () {
                    disconnect();
                }
            }
        }]);