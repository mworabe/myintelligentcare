﻿"use strict";
var EOPApp = angular.module("EDZoutstaffingPortalApp")
EOPApp.service("ExerciseService", ["$resource", "$state", "$injector", 'Upload', function ($resource, $state, $injector, Upload) {
    var permissionWorker = null;
    function getPermissionWorker() {
        if (!permissionWorker)
            permissionWorker = $injector.get("PermissionWorker");
        return permissionWorker;
    }
    var resource = $resource("api/Exercises/:id/:controller", null, {
        "query": { method: "GET", params: {}, isArray: false },
        "update": { method: "put", params: {}, isArray: false },
        "getAll": { method: "GET", params: {}, isArray: false },
        "getFilterExercise": {
            method: "GET",
            url: "api/GetAllExercises",
            isArray: false
        },

    });
    resource.uploadMedia = function (file) {
        var url = "api/Exercises/UploadFile"
        return Upload.upload({
            url: url,
            data: { file: file }
        });
    };

    return resource;
}]);
