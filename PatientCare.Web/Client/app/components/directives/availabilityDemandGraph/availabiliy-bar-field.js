﻿"use strict";
var app = angular.module("EDZoutstaffingPortalApp");

app.directive("availabilityBarField", [function () {
    return {
        templateUrl: "Client/app/components/directives/availabilityDemandGraph/availability-bar-field.html",
        restrict: "E",
        replace: false,
        scope: {
            column: '=ngColumn',
        },
        link: function (scope) {
            scope.fixValueTo = scope.fixedTo || 2;
            scope.availabilityData = scope.column;
        }
    }
}
]);