﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.ChatSessions;
using EmitMapper.MappingConfiguration;
using PatientCare.Web.ApiMobile.Models.ChatMembers;
using System.Linq;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class ChatMobileMapperExtensions
    {
        public static EmitMapperMapperProvider ChatMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ChatSession, ChatSessionResponse>
                    (new ExtendedMapConfig<ChatSession, ChatSessionResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ChatSession[], ChatSessionResponse[]>());

            var memberMapper =
                            ObjectMapperManager.DefaultInstance.GetMapper<ChatMemberRequest, ChatMember>(
                            new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ChatSessionRequest, ChatSession>(
                    new ExtendedMapConfig<ChatSessionRequest, ChatSession>()
                    .ForMember(x => x.ChatMembers, y => y.ChatMembers != null ? y.ChatMembers.Select(i => memberMapper.Map(i)).ToList() : null)
                  ));

            //mapperProvider.RegisterMapper(() =>
            //    ObjectMapperManager.DefaultInstance.GetMapper<ChatSessionRequest, ChatSession>
            //        (new ExtendedMapConfig<ChatSessionRequest, ChatSession>()));

            return mapperProvider;
        }
    }
}