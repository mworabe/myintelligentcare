﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.ResourceTravels;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceTravelMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceTravelRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTravel, ResourceTravelResponse>(new ExtendedMapConfig<ResourceTravel, ResourceTravelResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTravel[], ResourceTravelResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTravelRequest, ResourceTravel>(new ExtendedMapConfig<ResourceTravelRequest, ResourceTravel>()));


            mapperProvider.RegisterMapper(() =>
                    ObjectMapperManager.DefaultInstance.GetMapper<ResourceTravel[], PicklistResponse[]>());
            return mapperProvider;
        }
    }
}