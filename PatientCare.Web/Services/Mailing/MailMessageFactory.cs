﻿using PatientCare.Core.Services.Mailing;
using System.Net.Mail;
using System.IO;

namespace PatientCare.Web.Services.Mailing
{
    public class MailMessageFactory : IMailMessageFactory
    {
        public MailMessage ForgotPasswordMailMessage(string emailId, string userName, string link)
        {
            var mail = new MailMessage();
            StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Templates/ForgotPassword_Template.html"));
            string readFile = reader.ReadToEnd();
            string StrContent = "";
            StrContent = readFile;

            StrContent = StrContent.Replace("[UserName]", userName);
            StrContent = StrContent.Replace("[ResetLink]", link);

            mail.Subject = "MIC Password Reset";
            mail.Body = StrContent.ToString();
            mail.IsBodyHtml = true;
            mail.To.Add(emailId);
            reader.Close();
            return mail;
        }

        public MailMessage MobileAccessMailMessage(string emailId, string patientName, string userName,string password)
        {
            var mail = new MailMessage();
            StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Templates/MobileAccess_Template.html"));
            string readFile = reader.ReadToEnd();
            string StrContent = "";
            StrContent = readFile;
            StrContent = StrContent.Replace("[PatientName]", patientName);
            StrContent = StrContent.Replace("[UserName]", userName);
            StrContent = StrContent.Replace("[Password]", password);

            mail.Subject = "MIC Password Reset";
            mail.Body = StrContent.ToString();
            mail.IsBodyHtml = true;
            mail.To.Add(emailId);
            reader.Close();
            return mail;
        }
    }
}