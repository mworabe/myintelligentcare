﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PatientAppointment
{
    [DataContract]
    public class NotificationResponse
    {

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public object DataObject { get; set; }

        [DataMember]
        public string NotifType { get; set; }

    }
}