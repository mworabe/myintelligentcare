﻿namespace PatientCare.Core.Services.Dashboards
{
    public class ProjectTypeViewModel
    {
        public long Id { get; set; }
        public int Count { get; set; }
        public string status { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }

        public string ProjectHealthStatus { get; set; }
    }
}
