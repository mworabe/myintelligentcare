"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("ImageCropFactory", ["$rootScope", "$uibModal", function ($rootScope, $modal) {
        
        function cropImage(config) {
            return $modal.open({
                templateUrl: "Client/app/components/image-crop/img-crop-dialog.html",
                controller: "ImageCropCntrl",
                size: "lg",
                keyboard: false,
                resolve: {
                    dataItem: function () {
                        return config;
                    }
                }
            });
        }

        return {
            cropImage: function (config) {
                return cropImage(config)
            }
        }


    }
    ]);