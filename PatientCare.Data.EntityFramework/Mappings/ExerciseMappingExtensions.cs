﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ExerciseMappingExtensions
    {
        public static DbModelBuilder MapExercise(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<Exercise>().ToTable("exercises")
                .HasKey(i => i.Id);

            modelBuilder.Entity<Exercise>()
                    .Property(p => p.Created)
                    .HasColumnName("created")
                    .IsRequired()
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<Exercise>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<Exercise>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            modelBuilder.Entity<Exercise>()
                    .Property(p => p.set)
                    .HasColumnName("set")
                    .IsOptional();
            modelBuilder.Entity<Exercise>()
               .Property(p => p.reps)
               .HasColumnName("reps")
               .IsOptional();
            modelBuilder.Entity<Exercise>()
               .Property(p => p.holds)
               .HasColumnName("holds")
               .IsOptional();

            modelBuilder.Entity<Exercise>()
               .Property(p => p.weight)
               .HasColumnName("weight")
               .IsOptional();

            modelBuilder.Entity<Exercise>()
               .Property(p => p.weightUnit)
               .HasColumnName("weightUnit")
               .IsOptional();

            modelBuilder.Entity<Exercise>()
              .Property(p => p.holdUnit)
              .HasColumnName("holdUnit")
              .IsOptional();

            modelBuilder.Entity<Exercise>()
              .Property(p => p.frequency)
              .HasColumnName("frequency")
              .IsOptional();


            modelBuilder.Entity<Exercise>()
                .Property(p => p.Keywords)
                .HasColumnName("keywords")
                .IsOptional();

            modelBuilder.Entity<Exercise>()
                .Property(p => p.Comments)
                .HasColumnName("comments")
                .IsOptional();

            modelBuilder.Entity<Exercise>()
                .HasMany<ExerciseMedia>(p => p.Medias)
                .WithOptional()
                .HasForeignKey(item => item.ExerciseId);

            modelBuilder.Entity<Exercise>()
                .HasMany<Exercise_ExerciseActivity>(item => item.exercises)
                .WithOptional()
                .HasForeignKey(item => item.exercise_Id);

            modelBuilder.Entity<Exercise>()
                .HasMany<Exercise_injury>(item => item.injuries)
                .WithOptional()
                .HasForeignKey(item => item.injury_Id);

            modelBuilder.Entity<Exercise>()
                .Property(p => p.IsDeleted)
                .HasColumnName("is_deleted")
                .IsOptional();
            return modelBuilder;
        }
    }
}
