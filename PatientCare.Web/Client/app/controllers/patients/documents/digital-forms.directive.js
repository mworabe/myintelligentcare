﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('digitalForms', function ($compile) {
    return {
        scope: {
            config: "=config",
            //loadingBarFlag: "=isLoading",
            patientId: "=",
            caseId: "="
        },
        templateUrl: "Client/app/controllers/patients/documents/digital-forms.html",
        controller: ['$scope',
                    "MailDialogFactory",
        function ($scope, MailDialogFactory) {

            

            $scope.tabs = [{
                id: "ConsentForm",
                title: "Consent Form & Cancelation Policy",
            }, {
                id: "PHI",
                title: "PHI"
            }];

            $scope.selectedTab = $scope.tabs[0].id;

            $scope.isTabSelected = function (id) {
                if (id == $scope.selectedTab) return true;
                return false;
            }
            $scope.setSelectedTab = function (id) {
                $scope.selectedTab = id;
            }

            $scope.sendMail = function () {
                var mail = {
                    subject: "",
                    body: ""
                }
                
                
                if ($scope.selectedTab == "ConsentForm") {
                    mail.subject = "Concent Form";
                    mail.body ="Hello Matt,\n\nHere is your concent form to fill for your case - ABC-03-29-1. Please click on below link and fill the required details and submit the form.\nhttps://abc.myintelligentcare.com/patient/12202/concent_form.html\n\n";
                    mail.body+="For any queries contact us on : number from mobile app appointmetn page\n\n";
                    mail.body += "James Smith\n";
                    mail.body+="Therapist\n";
                    
                } else {
                    mail.subject = "PHI Form ";
                    mail.body = "Hello Matt,\n\nHere is your concent form to fill  for your case - ABC-03-29-1. Please click on below link and fill the required details and submit the form.\nhttps://abc.myintelligentcare.com/patient/12202/concent_form.html\n\n";
                    mail.body += "For any queries contact us on : number from mobile app appointmetn page\n\n";
                    mail.body += "James Smith\n";
                    mail.body += "Therapist\n";
                    
                }
                MailDialogFactory.showMailDialog(mail);
            }
           
        }]
    }
});



