
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("terms", {
                url: "/terms-condition",
                templateUrl: "Client/app/controllers/terms-condition/terms-condition.html",
                controller: "TermsConditionCntrl as TermsConditionCntrl",
                data: {
                    requireLogin: true
                }

            });
    });