﻿using System;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Criminal Record.
    /// </summary>
    public class ResourceCriminalRecord: Entity
    {
        public long ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        public DateTime DateOfOffense { get; set; }
        public string LocationOfOffense { get; set; }
        public string Offense { get; set; }
        public string PenaltyOrDisposition { get; set; }
    }
}
