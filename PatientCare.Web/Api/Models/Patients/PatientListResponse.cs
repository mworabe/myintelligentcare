﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.ApiMobile.Models.PatientWorkoutAnalytics;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Patients
{
    [DataContract]
    public class PatientListResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Prefix { get; set; }

        [DataMember]
        public string PatientNumber { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public long? ClinicId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Clinic { get; set; }

        [DataMember]
        public long? ClinicianId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Clinician { get; set; }

        [DataMember]
        public string PhotoFileNameUrl { get; set; }

        [DataMember]
        public long? CaseId { get; set; }

        [DataMember]
        public string CaseNo { get; set; }

        [DataMember]
        public CaseStatusEnum? CaseStatus { get; set; }

        [DataMember]
        public long? InjuryRegionId { get; set; }

        [DataMember]
        public PicklistDefaultResponse InjuryRegion { get; set; }

        [DataMember]
        public RelatedCauseEnum? RelatedCause { get; set; }

        [DataMember]
        public DateTime? LastVisitDate { get; set; }

        [DataMember]
        public PatientStatusEnum? Status { get; set; }

        [DataMember]
        public bool? HasMobileAccess { get; set; }

        [DataMember]
        public bool HasHep { get; set; }

        [DataMember]
        public long? HepId { get; set; }

        [DataMember]
        public PatientWorkoutAnalyticResponse PatientWorkAnalytic { get; set; }

        [DataMember]
        public int? PreExercisesPainRating
        {
            get { return (PatientWorkAnalytic != null ? PatientWorkAnalytic.PreExercisesPainRating : 0); }
            set { PreExercisesPainRating = value; }
        }

        [DataMember]
        public bool? IsDeleted { get; set; }
    }
}