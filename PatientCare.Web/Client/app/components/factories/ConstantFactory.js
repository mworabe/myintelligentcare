﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("ConstantFacory", ["$rootScope", function ($rootScope) {

        var timeUnit = [
               { id: 1, title: "Sec" },
               { id: 2, title: "Min" }           
        ];

        var weightUnit = [
             { id: 1, title: "kilos" },
             { id: 2, title: "lbs" }
  
        ];

        var frequencyUnit = [
            { id: 1, title: "1x Day" },
            { id: 2, title: "2x Day" },
            { id: 3, title: "3x Day" },
            { id: 4, title: "4x Day" },
            { id: 5, title: "5x Day" },
            { id: 6, title: "NA" },
        ];

        var frequenciesUnit = [
            { id: 1, title: "1x Day" },
            { id: 2, title: "2x Day" },
            { id: 3, title: "3x Day" },
            { id: 4, title: "4x Day" },
            { id: 5, title: "5x Day" },
            { id: 6, title: "NA" },
            { id: 7, title: "Other" }
        ];

        var resistances = [
            { id: 1, title: "Yellow" },
            { id: 2, title: "Red" },
            { id: 3, title: "Green" },
            { id: 4, title: "Blue" },
            { id: 5, title: "Black" },
        ];



        return {
            getResistances: function () {
                return resistances;

            },
            getTimeUnit: function () {
                return timeUnit;
            },
            getFrequency: function () {
                return frequencyUnit;
            },
            getWeightUnit: function () {
                return weightUnit;
            },
            getFrequencies: function () {
                return frequenciesUnit;
            },
            guid: function () {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                      .toString(16)
                      .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                  s4() + '-' + s4() + s4() + s4();
            }
        }


    }]);