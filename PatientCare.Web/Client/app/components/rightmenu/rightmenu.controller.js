﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("RightMenuCntrl", [
        "$scope", "$state", 'panels', "$rootScope", "Auth", "config", "PermissionWorker", "Resource", "toaster", "Upload",
        function ($scope, $state, panels, $rootScope, auth, config, permissionWorker, resourceService, toaster, uploader) {

            $scope.menuGroups = [
                {
                    name: "Administrative",
                    icon: "Administrative",
                    items: [
                        {
                            'title': "Roles",
                            'state': "role",
                            "name": "Roles",
                            'icon': 'right-submenu-role',
                            onlyAdmin: true,
                            associatedState: ["role"]
                        }, {
                            'title': "Users",
                            'state': "userList",
                            "name": "Users",
                            'icon': 'right-submenu-userList',
                            associatedState: ["userList"]
                        }, {

                            'title': "Client Companies",
                            'state': "clientCompany",
                            "name": "ClientCompanies",
                            'icon': 'right-submenu-clientCompany',
                            associatedState: ["clientCompany"]
                        }, {
                            'title': "Login History",
                            'state': "login-history",
                            "name": "LoginHistory",
                            'icon': 'login-history',
                            associatedState: ["login-history"]
                        }
                    /*    , {
                            'title': "Dashboards",
                            'state': "dashboard-list",
                            "name": "Dashboards",
                            'icon': 'icon-dashboard',
                            associatedState: ["login-history"]
                        }, */
                    ]
                }, /*{
                    title: "Custom Fields",
                    state: "customField",
                    icon: "CustomFields",
                    name: "Custom Field",
                    associatedState: ["customField"]
                }, */{
                    title: "Configuration Data",
                    state: "configurationData",
                    icon: "config-data",
                    name: "Configuration Data",
                    associatedState: ["configurationData"]
                }
             /*   , {
                    icon: "Survey",
                    name: "survey",
                    items: [
                         {
                             'title': "Surveys",
                             'state': "survey",
                             "name": "Survey",
                             'icon': 'Survey',
                             associatedState: ["surveyCategory"]
                         }, {
                             'title': "Survey Categories",
                             'state': "surveyCategory",
                             "name": "SurveyCategories",
                             'icon': 'right-submenu-surveyCategory',
                             associatedState: ["surveyCategory"]
                         },
                    ]
                } */
                , {
                    icon: "change-passwords",
                    state: "changePassword",
                    name: "Change Password",
                    items: []
                }
              /*  , {
                    icon: "importExport",
                    name: "Import/Export",
                    items: [{
                        'title': "Import Resources",
                        'state': "invoiceConfig",
                        "name": "invoiceConfig",
                        'icon': 'config-data',
                        associatedState: ["invoiceConfig"]
                    }]
                } */
            ];

            $scope.closeRightPanel = function () {
                panels.close();
            }

            $scope.logout = function () {
                console.info("LOGging Out");
                $scope.closeRightPanel();
                $scope.currentUser = undefined;
                auth.logout();
            };

            $scope.exportResources = function () {
                window.open("api/Resources/export-all/", "_blank", "");
            };

            $scope.uploadResoruces = function (file, $invalidFile) {
                if ($invalidFile && $invalidFile.length > 0) {
                    toaster.pop("error", "", "Max File Size 5MB");
                }
                if (!file)
                    return;
                resourceService.uploadResources(uploader, file, function (response) {
                    if (response.data) {
                        toaster.pop("success", "", response.data);
                    } else {
                        toaster.pop("success", "", "Import is finished");
                    }
                    if (resourceService.reloadCommand.reload)
                        resourceService.reloadCommand.reload();
                });
            };

            $scope.isAuthenticated = auth.isAuthenticated;
            $scope.isAdmin = auth.isAdmin;
            $scope.currentUser = auth.getCurrentUser();
            $scope.isActive = function (state, associateState) {
                associateState = associateState || [];
                return state === $state.current.name || _.includes(associateState, $state.current.name);
            };
            $scope.isVisibleMainMenu = function (menuItem) {
                /// TODO repait this in future 
                if (auth.isAdmin()) {
                    if (menuItem.name === "Calendar" || menuItem.name === "TimeOff") return false;
                    return true;
                }
                return (menuItem.name === undefined || permissionWorker.checkPermissionPageRead(menuItem.name));
            };
            $scope.isGroupVisible = function (group) {

                return _.some(group.items, "visible");
            }
            $scope.isVisible = function (menuItem) {
                if (auth.isAdmin()) {
                    menuItem.visible = true;
                    return true;
                }
                menuItem.visible = permissionWorker.checkPermissionPickList(menuItem.name);
                return permissionWorker.checkPermissionPickList(menuItem.name);
            };

            $scope.isItemVisible = function (name) {
                if (auth.isAdmin()) {
                    return true;
                }
                return permissionWorker.checkPermissionPickList(name);
            };

            $scope.isCustomConfigDataVisible = function () {
                if (auth.isAdmin())
                    return true;
                return (
                    permissionWorker.checkPermissionPageRead('ResourceConfigurationData') ||
                    permissionWorker.checkPermissionPageRead('ProjectConfigurationData') ||
                    permissionWorker.checkPermissionPageRead('FinanceConfigurationData') ||
                    permissionWorker.checkPermissionPageRead('IntakeConfigurationData') ||
                    permissionWorker.checkPermissionPageRead('GeneralConfigurationData')
                    );
            }
            $scope.isExportVisible = function () {
                return permissionWorker.canResourceRead();
            }

            $scope.isImportVisible = function () {
                return permissionWorker.canResourceInsert();
            }


            $rootScope.$on("update-menu", function () {
                _.forEach($scope.menuGroups, function (item) {
                    _.forEach(item.items, $scope.isVisible);
                });
            });

            _.forEach($scope.menuGroups, function (item) {
                _.forEach(item.items, $scope.isVisible);
            });
        }
    ]);