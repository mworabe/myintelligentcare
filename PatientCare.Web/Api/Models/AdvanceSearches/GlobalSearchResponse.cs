﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceScore
{
    [DataContract]
    public class GlobalSearchResponse
    {
        [DataMember]
        public SearchType? SearchType { get; set; }

        [DataMember]
        public ListResponse<GlobalResoourceSearchData> Resource { get; set; }
        
        [DataMember]
        public int? pageIndex { get; set; }

    }

    [DataContract]
    public class GlobalResoourceSearchData
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long ItemsCount { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public JsonDataModel[] Data { get; set; }

        [DataMember]
        public string DataString { get; set; }

        [DataMember]
        public string YearsOfExperience { get; set; }

        [DataMember]
        public string photoFileName { get; set; }

        [DataMember]
        public string location { get; set; }

        [DataMember]
        public string Education { get; set; }

        [DataMember]
        public string Industry { get; set; }

        [DataMember]
        public string PhotoFileNameUrl { get; set; }
    }

    [DataContract]
    public class JsonDataModel
    {
        [DataMember]
        public string field { get; set; }
        [DataMember]
        public string tabId { get; set; }
        [DataMember]
        public string value { get; set; }
    }

    [DataContract]
    public class StakeHolderResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long ProjectId { get; set; }
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public long StakeHolderRoleId { get; set; }

        [DataMember]
        public string StakeHolderRoleName { get; set; }

        [DataMember]
        public string TimekeeperNumber { get; set; }
    }
}