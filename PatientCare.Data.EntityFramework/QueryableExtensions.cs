﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace PatientCare.Data.EntityFramework
{
    internal static class QueryableExtensions
    {
        public static IQueryable<T> ApplyFilters<T>(this IQueryable<T> entities, IEnumerable<Expression<Func<T, bool>>> filterExpressions)
        {
            if (entities == null) { throw new ArgumentNullException("entities"); }
            if (filterExpressions != null)
            {
                return filterExpressions.Aggregate(entities, (current, filter) => current.Where(filter));
            }
            return entities;
        }

        public static IQueryable<T> ApplyOrderExpressions<T>(this IQueryable<T> entities, IList<Expression<Func<T, object>>> orderByExpressions, SortOrder order = SortOrder.Ascending)
        {
            if (entities == null) { throw new ArgumentNullException("entities"); }
            if (orderByExpressions != null)
            {
                for (var i = 0; i < orderByExpressions.Count; i++)
                {
                    entities = i == 0
                        ? entities.ObjectSort(orderByExpressions[i], order)
                        : ((IOrderedQueryable<T>)entities).ThenObjectSort(orderByExpressions[i], order);
                }
            }
            return entities;
        }

        public static IQueryable<T> AttachIncludePredicates<T>(this IQueryable<T> entities, IEnumerable<Expression<Func<T, object>>> includePredicates)
        {
            if (entities == null) { throw new ArgumentNullException("entities"); }
            if (includePredicates != null)
            {
                entities = includePredicates.Aggregate(entities, (current, includePredicate) => current.Include(includePredicate));
            }
            return entities;
        }

        public static IOrderedQueryable<T> ObjectSort<T>(this IQueryable<T> entities, Expression<Func<T, object>> expression, SortOrder order = SortOrder.Ascending)
        {
            var unaryExpression = expression.Body as UnaryExpression;
            if (unaryExpression != null)
            {
                var propertyExpression = (MemberExpression)unaryExpression.Operand;
                var parameters = expression.Parameters;

                if (propertyExpression.Type == typeof(string))
                {
                    var newExpression = Expression.Lambda<Func<T, string>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(DateTime))
                {
                    var newExpression = Expression.Lambda<Func<T, DateTime>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(bool))
                {
                    var newExpression = Expression.Lambda<Func<T, bool>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(int))
                {
                    var newExpression = Expression.Lambda<Func<T, int>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(int?))
                {
                    var newExpression = Expression.Lambda<Func<T, int?>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(long))
                {
                    var newExpression = Expression.Lambda<Func<T, long>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(decimal))
                {
                    var newExpression = Expression.Lambda<Func<T, decimal>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(decimal?))
                {
                    var newExpression = Expression.Lambda<Func<T, decimal?>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }

                // temporary solution. Maybe need to replace all ifs with this code
                if (propertyExpression.Type.IsEnum || Nullable.GetUnderlyingType(propertyExpression.Type) != null)
                {
                    ParameterExpression param = Expression.Parameter(typeof (T), string.Empty);
                    MemberExpression property = Expression.PropertyOrField(param, propertyExpression.Member.Name);
                    LambdaExpression sort = Expression.Lambda(property, param);
                    MethodCallExpression call = Expression.Call(typeof(Queryable), 
                        "OrderBy" + (order == SortOrder.Descending ? "Descending" : string.Empty),
                        new[] { typeof(T), property.Type },
                        entities.Expression,
                        Expression.Quote(sort));
                    return (IOrderedQueryable<T>) entities.Provider.CreateQuery<T>(call);
                }

                throw new NotSupportedException("Object type resolution not implemented for this type");
            }
            return order == SortOrder.Ascending ? entities.OrderBy(expression) : entities.OrderByDescending(expression);
        }

        public static IOrderedQueryable<T> ThenObjectSort<T>(this IOrderedQueryable<T> entities, Expression<Func<T, object>> expression, SortOrder order = SortOrder.Ascending)
        {
            var unaryExpression = expression.Body as UnaryExpression;
            if (unaryExpression != null)
            {
                var propertyExpression = (MemberExpression)unaryExpression.Operand;
                var parameters = expression.Parameters;

                if (propertyExpression.Type == typeof(DateTime))
                {
                    var newExpression = Expression.Lambda<Func<T, DateTime>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.ThenBy(newExpression) : entities.ThenByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(bool))
                {
                    var newExpression = Expression.Lambda<Func<T, bool>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.ThenBy(newExpression) : entities.ThenByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(int))
                {
                    var newExpression = Expression.Lambda<Func<T, int>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.ThenBy(newExpression) : entities.ThenByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(int?))
                {
                    var newExpression = Expression.Lambda<Func<T, int?>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.ThenBy(newExpression) : entities.ThenByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(long))
                {
                    var newExpression = Expression.Lambda<Func<T, long>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.ThenBy(newExpression) : entities.ThenByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(decimal))
                {
                    var newExpression = Expression.Lambda<Func<T, decimal>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.ThenBy(newExpression) : entities.ThenByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(decimal?))
                {
                    var newExpression = Expression.Lambda<Func<T, decimal?>>(propertyExpression, parameters);
                    return order == SortOrder.Ascending ? entities.ThenBy(newExpression) : entities.ThenByDescending(newExpression);
                }

                // temporary solution. Maybe need to replace all ifs with this code
                if (propertyExpression.Type.IsEnum)
                {
                    ParameterExpression param = Expression.Parameter(typeof(T), string.Empty);
                    MemberExpression property = Expression.PropertyOrField(param, propertyExpression.Member.Name);
                    LambdaExpression sort = Expression.Lambda(property, param);
                    MethodCallExpression call = Expression.Call(typeof(Queryable),
                        "OrderBy" + (order == SortOrder.Descending ? "Descending" : string.Empty),
                        new[] { typeof(T), property.Type },
                        entities.Expression,
                        Expression.Quote(sort));
                    return (IOrderedQueryable<T>)entities.Provider.CreateQuery<T>(call);
                }

                throw new NotSupportedException("Object type resolution not implemented for this type");
            }
            return order == SortOrder.Ascending ? entities.ThenBy(expression) : entities.ThenByDescending(expression);
        }
    }
}
