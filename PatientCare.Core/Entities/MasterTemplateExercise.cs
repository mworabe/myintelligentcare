﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Master Template Exercise.
    /// </summary>
    public class MasterTemplateExercise : Entity
    {
        public long? ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }
        
        public long? MasterTemplateId { get; set; }
        public virtual MasterTemplate MasterTemplate { get; set; }

        public int? ExerciseOrder { get; set; }
    }
}
