﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.PatientAddresses;
using PatientCare.Web.Api.Models.PatientParentGuardians;
using System;

namespace PatientCare.Web.Api.Models.Patients
{
    public class PatientRequest
    {
        public long Id { get; set; }

        public string Prefix { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string NickName { get; set; }

        public DateTime? BirthDate { get; set; }

        public bool? IsMinor { get; set; }

        public string SSN { get; set; }

        public GenderEnum? Gender { get; set; }

        public string Email { get; set; }

        public ContactMethodEnum? PrefferedContactMethod { get; set; }

        public string CellNumber { get; set; }

        public bool? CellNumberIsPrimary { get; set; }

        public string HomeNumber { get; set; }

        public bool? HomeNumberIsPrimary { get; set; }

        public long? LanguageId { get; set; }        

        public string SpecialAccomodations { get; set; }

        public MartialStatusEnum? MaritialStatus { get; set; }

        public string EmergencyContact { get; set; }

        public RelationshipEnum? RelationshipToContact { get; set; }

        public string EmergencyPhone { get; set; }

        public string EmployerName { get; set; }

        public string EmployerPhone { get; set; }

        public long? ReferralPhysicianId { get; set; }

        public long? ClinicId { get; set; }

        public long? ClinicLocationId { get; set; }

        public PatientAddressRequest[] Addresses { get; set; }

        public PatientParentGuardianRequest[] ParentGaurdians { get; set; }

        public PatientAddressRequest PatientAddress1 { get; set; }

        public PatientAddressRequest PatientAddress2 { get; set; }

        public PatientParentGuardianRequest ParentGaurdian { get; set; }

        public string PatientNumber { get; set; }

        public long? ClinicianId { get; set; }

        public bool? IsMedicarePatient { get; set; }        

        public PatientStatusEnum? Status { get; set; }

        public string MobileAccessUserId { get; set; }

        public string MobileAccessUserName { get; set; }

        public string MobileAccessPassword { get; set; }        
        
        public string PhotoFileName { get; set; }

        public string WebPTPatientId { get; set; }

        public bool? IsDeleted { get; set; }
    }
}