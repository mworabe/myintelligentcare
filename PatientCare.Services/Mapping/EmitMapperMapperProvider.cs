﻿using System;
using System.Collections.Generic;
using PatientCare.Core.Mapping;
using EmitMapper;

namespace PatientCare.Services.Mapping
{
    public class EmitMapperMapperProvider : IMapperProvider
    {
        private static readonly Dictionary<Tuple<Type,Type>, object> MappersCache = new Dictionary<Tuple<Type, Type>, object>();

        public IMapper<TS, TD> GetMapper<TS, TD>()
        {
            var key = new Tuple<Type, Type>(typeof(TS), typeof(TD));
            if (!MappersCache.ContainsKey(key))
            {
                throw new InvalidOperationException("Could not find mapper for types '" + typeof(TS).FullName + "' => '" + typeof(TD).FullName + "'");
            }
            return (IMapper<TS, TD>)MappersCache[key];
        }

        public EmitMapperMapperProvider RegisterMapper<TS,TD>(Func<ObjectsMapper<TS,TD>> mapperFactory)
        {
            if (mapperFactory == null) throw new ArgumentNullException("mapperFactory");
            var key = new Tuple<Type, Type>(typeof (TS), typeof (TD));
            var mapper = new EmitMapperMapper<TS, TD>(mapperFactory());
            MappersCache[key] = mapper;
            return this;
        }
    }
}
