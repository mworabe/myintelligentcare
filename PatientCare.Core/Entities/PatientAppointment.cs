﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Patient Appointment.
    /// </summary>
    public class PatientAppointment : Entity
    {
        public long? PatientId { get; set; }
        public virtual Patient PatientName { get; set; }

        public long? CaseId { get; set; }
        public virtual PatientCase Case { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long? ClinicLocationId { get; set; }
        public virtual ClinicLocation ClinicLocation { get; set; }

        public long? AppointmentTypeId { get; set; }
        public virtual AppointmentType AppointmentType { get; set; }

        public long? ClinicianId { get; set; }
        public virtual Resource Clinician { get; set; }

        public long? FacilityId { get; set; }
        public virtual Clinic Facility { get; set; }

        public string AdditionalDetail { get; set; }

        public AppointmentStatusEnum? AppointmentStatus { get; set; }

        public bool? IsMobile { get; set; }

        public bool? IsReschedule { get; set; }

        public AppointmentTypeMobileEnum? AppointmentTypeMobile { get; set; }
    }
}
