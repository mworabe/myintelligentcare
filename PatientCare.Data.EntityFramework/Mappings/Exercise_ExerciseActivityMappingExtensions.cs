﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class Exercise_ExerciseActivityMappingExtensions
    {
        public static DbModelBuilder MapExercise_ExcerciseActivity(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<Exercise_ExerciseActivity>().ToTable("exercise_exerciseactivity")
                .HasKey(i => i.Id);

            modelBuilder.Entity<Exercise_ExerciseActivity>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<Exercise_ExerciseActivity>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<Exercise_ExerciseActivity>()
                            .Property(p => p.activity_Id)
                            .HasColumnName("activity_Id")
                            .IsOptional();

            modelBuilder.Entity<Exercise_ExerciseActivity>()
                           .Property(p => p.exercise_Id)
                           .HasColumnName("exercise_Id")
                           .IsOptional();

            modelBuilder.Entity<Exercise_ExerciseActivity>()
                .HasOptional<Exercise>(p => p.exercise)
                .WithMany()
                .HasForeignKey(p => p.exercise_Id);

            modelBuilder.Entity<Exercise_ExerciseActivity>()
                .HasOptional<ExerciseActivity>(p => p.activity)
                .WithMany()
                .HasForeignKey(p => p.activity_Id);

            return modelBuilder;
        }
    }
}
