﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface ICustomFieldRepository : IRepository<CustomField>
    {
    }
}