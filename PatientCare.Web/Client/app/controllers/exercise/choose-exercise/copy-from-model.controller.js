﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("CopyFromCntrl", [
		"$scope",
		'toaster',
        'dataItem',
        '$uibModalInstance',
        'MasterTemplateService',
        'FavoriteTemplateService',
		function ($scope, toaster, dataItem, $uibModalInstance, MasterTemplateService, FavoriteTemplateService) {

		    var from = dataItem.from;
		    $scope.dataItem = [];


		    self.pageSize = 2000;
		    self.exercisePage = 1;
		    self.totalItems = 0;
		    


		    $scope.loadData = function () {
		        if (from == "Master") {
		            $scope.isLoading = MasterTemplateService.getFullDetailList({ pageIndex: self.exercisePage, pageSize: self.pageSize }).$promise.then(
                        function (response) {
                            $scope.dataItem = response.data;
                        },
                        function (error) {

                        });
		        } else if (from == 'Favorite') {
		            $scope.isLoading = FavoriteTemplateService.getFullDetailList({ pageIndex: self.exercisePage, pageSize: self.pageSize }).$promise.then(
                        function (response) {
                            $scope.dataItem = response.data;
                        },
                        function (error) {

                        });
		        }
		    }

		    if (from == "Master") {
		        $scope.title = "Master Templates"
		    } else if (from == 'Favorite') {
		        $scope.title = "Favorite  Templates"
		    }

		    $scope.loadData();

			
		    $scope.onItemSelected = function (item) {
		        if (item) {
		            $uibModalInstance.close(item);
		        }
		    }

			$scope.close = function () {
				$uibModalInstance.dismiss('');
			}

		}
    ]);