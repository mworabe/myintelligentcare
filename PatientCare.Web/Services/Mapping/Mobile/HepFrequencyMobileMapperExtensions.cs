﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.HepFrequencies;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class HepFrequencyMobileMapperExtensions
    {
        public static EmitMapperMapperProvider HepFrequencyMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepFrequency, HepFrequencyMobileResponse>
                    (new ExtendedMapConfig<HepFrequency, HepFrequencyMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepFrequency[], HepFrequencyMobileResponse[]>());
            
            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepFrequencyMobileRequest, HepFrequency>
                    (new ExtendedMapConfig<HepFrequencyMobileRequest, HepFrequency>()));
            
            return mapperProvider;
        }
    }
}