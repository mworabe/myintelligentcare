namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTableNameofPatentsCases : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.patients_cases", newName: "patient_cases");
            AlterColumn("dbo.patient_cases", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.patient_cases", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.patient_cases", newName: "patients_cases");
            AlterColumn("dbo.patients_cases", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.patients_cases", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
    }
}
