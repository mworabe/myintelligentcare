namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedColumninHEPDetailTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hep_detail", "weight_unit", c => c.String());
            AlterColumn("dbo.hep_detail", "weight", c => c.Decimal(precision: 18, scale: 2));

            AlterColumn("dbo.user_workout_analytics", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.user_workout_analytics", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.hep_detail", "weight", c => c.String());
            DropColumn("dbo.hep_detail", "weight_unit");

            AlterColumn("dbo.user_workout_analytics", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.user_workout_analytics", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
    }
}
