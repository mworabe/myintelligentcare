﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Yes No Other Enum.
    /// </summary>
    public enum YesNoOtherEnum
    {
        NA = 0,
        Yes = 1,
        No = 2,
        Other = 3
    }
}
