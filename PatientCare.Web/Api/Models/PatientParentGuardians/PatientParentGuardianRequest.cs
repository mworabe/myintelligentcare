﻿using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.PatientParentGuardians
{
    public class PatientParentGuardianRequest
    {
        public long Id { get; set; }
        
        public long? PatientId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public RelationshipEnum? RelationshiptoPatient { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public string ZipCodeTemp { get; set; }

        public long? CountryId { get; set; }

        public long? StateId { get; set; }

        public long? CityId { get; set; }

        public string CellNumber { get; set; }

        public bool? CellNumberIsPrimary { get; set; }

        public string HomeNumber { get; set; }

        public bool? HomeNumberIsPrimary { get; set; }

    }
}