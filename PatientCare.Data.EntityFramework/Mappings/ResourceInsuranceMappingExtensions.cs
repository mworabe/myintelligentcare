﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceInsuranceMappingExtensions
    {
        public static DbModelBuilder MapResourceInsurance(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceInsurance>().ToTable("resource_insurances").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceInsurance>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceInsurance>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceInsurance>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<ResourceInsurance>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceInsurance>()
              .Property(p => p.InsuranceTypeId)
              .HasColumnName("insurance_type_id")
              .IsOptional();

            modelBuilder.Entity<ResourceInsurance>()
                .HasOptional<DropDownConfig>(p => p.InsuranceType)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceInsurance>()
                .Property(p => p.Participating)
                .HasColumnName("participating")
                .IsOptional();

            modelBuilder.Entity<ResourceInsurance>()
              .Property(p => p.EmployeeCost)
              .HasColumnName("employee_cost")
              .IsOptional();

            modelBuilder.Entity<ResourceInsurance>()
              .Property(p => p.EmployerContribution)
              .HasColumnName("employer_contribution")
              .IsOptional();


            return modelBuilder;
        }
    }
}
