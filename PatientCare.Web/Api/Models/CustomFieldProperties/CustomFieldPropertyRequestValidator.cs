﻿using System;
using PatientCare.Core.Enums;
using FluentValidation;

namespace PatientCare.Web.Api.Models.CustomFieldProperties
{
    public class CustomFieldPropertyRequestValidator : AbstractValidator<CustomFieldPropertyRequest>
    {
        public CustomFieldPropertyRequestValidator()
        {
        }
    }
}