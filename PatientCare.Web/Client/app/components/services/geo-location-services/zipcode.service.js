﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("ZipCodeService", ["$resource", function ($resource) {
      var resource = $resource("api/zip-code/:controller",
        null, //parameters default
            {
                'update': { method: "PUT" },
                'query': { method: "GET", params: {}, isArray: false }
            });

      resource.getTableOption = {
          pageName: "City",
          cols: function () {
              return [
                  { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                  { field: "name", title: "Postal Code", sortable: "name", show: true, dataType: "text", required: true },
                  { field: "action", title: "More", dataType: "command" },
              ];
          }
      };
      return resource;
  }]);
