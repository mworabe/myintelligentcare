namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changesHEpdetailTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.hep_detail", "hep_id", "dbo.hep_master");
            DropIndex("dbo.hep_detail", new[] { "hep_id" });
            AlterColumn("dbo.hep_detail", "sets", c => c.Int());
            AlterColumn("dbo.hep_detail", "reps", c => c.Int());
            AlterColumn("dbo.hep_detail", "time", c => c.Int());
            AlterColumn("dbo.hep_detail", "time_unit", c => c.Int());
            AlterColumn("dbo.hep_detail", "weight", c => c.String());
            AlterColumn("dbo.hep_detail", "holds", c => c.Int());
            AlterColumn("dbo.hep_detail", "holds_unit", c => c.Int());
            AlterColumn("dbo.hep_detail", "exercise_order", c => c.Int());
            AlterColumn("dbo.hep_exercise_frequency", "holds_unit", c => c.Int());
            AlterColumn("dbo.hep_exercise_frequency", "exercise_frequency", c => c.Int());
            DropColumn("dbo.hep_detail", "hep_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.hep_detail", "hep_id", c => c.Long());
            AlterColumn("dbo.hep_exercise_frequency", "exercise_frequency", c => c.Int(nullable: false));
            AlterColumn("dbo.hep_exercise_frequency", "holds_unit", c => c.Int(nullable: false));
            AlterColumn("dbo.hep_detail", "exercise_order", c => c.Int(nullable: false));
            AlterColumn("dbo.hep_detail", "holds_unit", c => c.Int(nullable: false));
            AlterColumn("dbo.hep_detail", "holds", c => c.Int(nullable: false));
            AlterColumn("dbo.hep_detail", "weight", c => c.String(nullable: false));
            AlterColumn("dbo.hep_detail", "time_unit", c => c.Int(nullable: false));
            AlterColumn("dbo.hep_detail", "time", c => c.Int(nullable: false));
            AlterColumn("dbo.hep_detail", "reps", c => c.Int(nullable: false));
            AlterColumn("dbo.hep_detail", "sets", c => c.Int(nullable: false));
            CreateIndex("dbo.hep_detail", "hep_id");
            AddForeignKey("dbo.hep_detail", "hep_id", "dbo.hep_master", "Id");
        }
    }
}
