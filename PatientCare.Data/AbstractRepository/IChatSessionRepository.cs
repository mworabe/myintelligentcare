﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IPatientRepository : IRepository<Patient>
    {
    }
}
