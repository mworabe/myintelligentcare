namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using PatientCare.Data.EntityFramework.Identity;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPatientIdInUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "PatientId", c => c.Long());
            CreateIndex("dbo.AspNetUsers", "PatientId");
            AddForeignKey("dbo.AspNetUsers", "PatientId", "dbo.patients", "Id");

            Sql(string.Format(
                                @"
                                INSERT INTO dbo.AspNetRoles(Id,Name,Discriminator)
                                VALUES('f8acb07d-0997-4abc-8a47-11e951b53f26','{0}','ApplicationRole');                                                              
                                ",
                                 ApplicationUser.PatientRole
                                 ));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "PatientId", "dbo.patients");
            DropIndex("dbo.AspNetUsers", new[] { "PatientId" });
            DropColumn("dbo.AspNetUsers", "PatientId");

            Sql(@"
                    DELETE
                    FROM dbo.AspNetRoles
                    WHERE Id in('f8acb07d-0997-4abc-8a47-11e951b53f26')
                ");
        }
    }
}
