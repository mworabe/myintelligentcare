﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Account;
using PatientCare.Web.Api.Models.PatientAddresses;
using PatientCare.Web.Api.Models.PatientParentGuardians;
using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Patients
{
    [DataContract]
    public class PatientResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Prefix { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string NickName { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public bool? IsMinor { get; set; }

        [DataMember]
        public string SSN { get; set; }

        [DataMember]
        public GenderEnum? Gender { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public ContactMethodEnum? PrefferedContactMethod { get; set; }

        [DataMember]
        public string CellNumber { get; set; }

        [DataMember]
        public bool? CellNumberIsPrimary { get; set; }

        [DataMember]
        public string HomeNumber { get; set; }

        [DataMember]
        public bool? HomeNumberIsPrimary { get; set; }

        [DataMember]
        public long? LanguageId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Language { get; set; }

        [DataMember]
        public string SpecialAccomodations { get; set; }

        [DataMember]
        public MartialStatusEnum? MaritialStatus { get; set; }

        [DataMember]
        public string EmergencyContact { get; set; }

        [DataMember]
        public RelationshipEnum? RelationshipToContact { get; set; }

        [DataMember]
        public string EmergencyPhone { get; set; }

        [DataMember]
        public string EmployerName { get; set; }

        [DataMember]
        public string EmployerPhone { get; set; }

        [DataMember]
        public long? ReferralPhysicianId { get; set; }

        [DataMember]
        public PicklistDefaultResponse ReferralPhysician { get; set; }

        [DataMember]
        public long? ClinicId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Clinic { get; set; }

        [DataMember]
        public long? ClinicLocationId { get; set; }

        [DataMember]
        public PicklistDefaultResponse ClinicLocation { get; set; }

        [DataMember]
        public PatientAddressResponse PatientAddress1 { get; set; }

        [DataMember]
        public PatientAddressResponse PatientAddress2 { get; set; }

        [DataMember]
        public PatientParentGuardianResponse ParentGaurdian { get; set; }

        [DataMember]
        public string PatientNumber { get; set; }

        [DataMember]
        public long? ClinicianId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Clinician { get; set; }

        [DataMember]
        public bool? IsMedicarePatient { get; set; }

        [DataMember]
        public bool? HasMobileAccess { get; set; }

        [DataMember]
        public string PhotoFileName { get; set; }

        [DataMember]
        public string PhotoFileNameUrl { get; set; }

        [DataMember]
        public PatientStatusEnum? Status { get; set; }

        [DataMember]
        public string MobileAccessUserId { get; set; }

        [DataMember]
        public string MobileAccessUserName { get; set; }

        [DataMember]
        public string MobileAccessPassword { get; set; }

        [DataMember]
        public string WebPTPatientId { get; set; }

        [DataMember]
        public long? CaseId { get; set; }

        [DataMember]
        public bool? IsDeleted { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public AccountPatientResponse PatientUserInfo { get; set; }

    }
}