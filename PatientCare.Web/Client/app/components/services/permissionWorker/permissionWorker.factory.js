﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("PermissionWorker", ["Auth",
        function (auth) {

            var SCHEDULER_NAME = "Schedule";
            var PATIENT_NAME = "Patients";
            var INSURANCE_NAME = "Insurance";
            var LIBRARY_NAME = "Library";
            var RESOURCE_NAME = "Resources";
            var REFERRAL_NAME = "Referral";
            var COMMUNICATION_NAME = "Communications";
            var BILLING_NAME = "Billing";
            var DASHBOARD_NAME = "Dashboards";


            var RESOURCE_CONFIG_DATA = "ResourceConfigurationData";
            var GENERAL_CONFIG_DATA = "GeneralConfigurationData";



            var homePages = {
                "Dashboard": "dashboard",
                "Calendar": "calendar",
                "HumanResourceHome": "home_human_resource",
                "ResourceHome": "home_resource",
                "Resources": "resource",
                "home_care_managment": "home_care_managment",
            }

            var restrictAccess = [
                "PersonalInfo",
                "Salary",
                "BackgroundChecks",
                "CriminalRecords",
                "PreviousEmployments",
            ];


            function getHomePageState() {
                var rules = auth.getRules();
                var returnObj = {};
                returnObj.hasParams = false;
                returnObj.state = "home_human_resource";
                if (!rules) {
                    return returnObj;
                } else if (rules.mainHomePage && rules.mainHomePage.value != "Projects") {
                    returnObj.state = homePages[rules.mainHomePage.value] || "home_human_resource";
                    return returnObj;
                } else if (rules.mainHomePage && rules.mainHomePage.value) {
                    var obj = {};
                    returnObj.hasParams = true;
                    var userHomePage = rules.mainHomePage;
                    returnObj.state = homePages[userHomePage.value];
                    returnObj.params = userHomePage.stateParams;
                    return returnObj;
                }
                return returnObj;
            }


            /*
             * For SQL Filter on Resource Filter to check either a Resource having access to SQL Filter or not.
             * Returns true if logged in user having permission to access SQL Filter on Resource Filter else False.
             * @reutrn: Boolean
             */
            function checkPermissionToSQLQueryFilter() {
                var rules = auth.getRules();
                if (!rules.accessSQLQuery || rules.accessSQLQuery === null || rules.accessSQLQuery === false) {
                    return false;
                } else if (rules.accessSQLQuery)
                    return rules.accessSQLQuery
                return false;
            }

            function checkReportPermission() {
                var rules = auth.getRules();
                if (!rules.accessViewReports || rules.accessViewReports === null || rules.accessViewReports === false) {
                    return false;
                } else if (rules.accessViewReports)
                    return rules.accessViewReports;
                return false;
            }

            function checkPermissionPageRead(pageName) {
                if (auth.isAdmin())
                    return true;
                return checkPagePermission(pageName, "Read");
            }

            function checkPermissionPickList(name) {
                var rules = auth.getRules();
                if (!rules)
                    return false;
                return _.includes(rules.picklists, name);
            }

            function checkPermissionPageUpdate(pageName) {
                if (auth.isAdmin())
                    return true;
                return checkPagePermission(pageName, "Update");
            }

            function checkPermissionPageInsert(pageName) {
                if (auth.isAdmin())
                    return true;
                return checkPagePermission(pageName, "Create");
            }
            function checkPermissionPageDelete(pageName) {
                if (auth.isAdmin())
                    return true;
                return checkPagePermission(pageName, "Delete");
            }

            function restrictByName(name) {
                if (auth.isAdmin())
                    return false;
                var rules = auth.getRules();
                if (!rules)
                    return false;

                if (!rules.restrictAccess)
                    return false;

                return _.includes(rules.restrictAccess, name);
            }

            /**
                Rights Checking Function.
            */
            function checkPagePermission(page, access) {
                if (auth.isAdmin())
                    return true;
                var rules = auth.getRules();
                if (!rules)
                    return false;
                var section = _.find(rules.sections, { "item": page });
                if (!section)
                    return false;
                return _.includes(section.actions, access);
            }

            return {
                checkPermissionPageRead: checkPermissionPageRead,
                checkPermissionPageUpdate: checkPermissionPageUpdate,
                checkPermissionPageInsert: checkPermissionPageInsert,
                checkPermissionPageDelete: checkPermissionPageDelete,
                checkPermissionPickList: checkPermissionPickList,
                getHomePageState: getHomePageState,
                restrictByName: restrictByName,
                checkPermissionToSQLQueryFilter: checkPermissionToSQLQueryFilter,
                checkReportsPermission: checkReportPermission,

                // Configuration Data Rules ----------------------
                // Resource Config Data
                canResourceConfigDataRead: function () {
                    return checkPermissionPageRead(RESOURCE_CONFIG_DATA);
                },
                canResourceConfigDataUpdate: function () {
                    return checkPermissionPageUpdate(RESOURCE_CONFIG_DATA);
                },
                canResourceConfigDataInsert: function () {
                    return checkPermissionPageInsert(RESOURCE_CONFIG_DATA);
                },
                canResourceConfigDataDelete: function () {
                    return checkPermissionPageDelete(RESOURCE_CONFIG_DATA);
                },

                // General Config Data
                canGeneralConfigDataRead: function () {
                    return checkPermissionPageRead(GENERAL_CONFIG_DATA);
                },
                canGeneralConfigDataUpdate: function () {
                    return checkPermissionPageUpdate(GENERAL_CONFIG_DATA);
                },
                canGeneralConfigDataInsert: function () {
                    return checkPermissionPageInsert(GENERAL_CONFIG_DATA);
                },
                canGeneralConfigDataDelete: function () {
                    return checkPermissionPageDelete(GENERAL_CONFIG_DATA);
                },
                // Configuration Data Rules ----------------------

                //Scheduler
                canSchedulerRead: function () {
                    return checkPermissionPageRead(SCHEDULER_NAME);
                },
                canSchedulerUpdate: function () {
                    return checkPermissionPageUpdate(SCHEDULER_NAME);
                },
                canSchedulerInsert: function () {
                    return checkPermissionPageInsert(SCHEDULER_NAME);
                },
                canSchedulerDelete: function () {
                    return checkPermissionPageDelete(SCHEDULER_NAME);
                },

                //Patient
                canPatientRead: function () {
                    return checkPermissionPageRead(PATIENT_NAME);
                },
                canPatientUpdate: function () {
                    return checkPermissionPageUpdate(PATIENT_NAME);
                },
                canPatientInsert: function () {
                    return checkPermissionPageInsert(PATIENT_NAME);
                },
                canPatientDelete: function () {
                    return checkPermissionPageDelete(PATIENT_NAME);
                },

                //Insurance
                canInsuranceRead: function () {
                    return checkPermissionPageRead(INSURANCE_NAME);
                },
                canInsuranceUpdate: function () {
                    return checkPermissionPageUpdate(INSURANCE_NAME);
                },
                canInsuranceInsert: function () {
                    return checkPermissionPageInsert(INSURANCE_NAME);
                },
                canInsuranceDelete: function () {
                    return checkPermissionPageDelete(INSURANCE_NAME);
                },

                //Library
                canLibraryRead: function () {
                    return checkPermissionPageRead(LIBRARY_NAME);
                },
                canLibraryUpdate: function () {
                    return checkPermissionPageUpdate(LIBRARY_NAME);
                },
                canLibraryInsert: function () {
                    return checkPermissionPageInsert(LIBRARY_NAME);
                },
                canLibraryDelete: function () {
                    return checkPermissionPageDelete(LIBRARY_NAME);
                },

                //Resource
                canResourceRead: function () {
                    return checkPermissionPageRead(RESOURCE_NAME);
                },
                canResourceUpdate: function () {
                    return checkPermissionPageUpdate(RESOURCE_NAME);
                },
                canResourceInsert: function () {
                    return checkPermissionPageInsert(RESOURCE_NAME);
                },
                canResourceDelete: function () {
                    return checkPermissionPageDelete(RESOURCE_NAME);
                },

                //Referral
                canReferralRead: function () {
                    return checkPermissionPageRead(REFERRAL_NAME);
                },
                canReferralUpdate: function () {
                    return checkPermissionPageUpdate(REFERRAL_NAME);
                },
                canReferralInsert: function () {
                    return checkPermissionPageInsert(REFERRAL_NAME);
                },
                canReferralDelete: function () {
                    return checkPermissionPageDelete(REFERRAL_NAME);
                },

                //Communications
                canCommunicationRead: function () {
                    return checkPermissionPageRead(COMMUNICATION_NAME);
                },
                canCommunicationUpdate: function () {
                    return checkPermissionPageUpdate(COMMUNICATION_NAME);
                },
                canCommunicationInsert: function () {
                    return checkPermissionPageInsert(COMMUNICATION_NAME);
                },
                canCommunicationDelete: function () {
                    return checkPermissionPageDelete(COMMUNICATION_NAME);
                },

                //Billing
                canBillingRead: function () {
                    return checkPermissionPageRead(BILLING_NAME);
                },
                canBillingUpdate: function () {
                    return checkPermissionPageUpdate(BILLING_NAME);
                },
                canBillingInsert: function () {
                    return checkPermissionPageInsert(BILLING_NAME);
                },
                canBillingDelete: function () {
                    return checkPermissionPageDelete(BILLING_NAME);
                },

                //Dashboards
                canDashboardsRead: function () {
                    return checkPermissionPageRead(DASHBOARD_NAME);
                },
                canDashboardsUpdate: function () {
                    return checkPermissionPageUpdate(DASHBOARD_NAME);
                },
                canDashboardsInsert: function () {
                    return checkPermissionPageInsert(DASHBOARD_NAME);
                },
                canDashboardsDelete: function () {
                    return checkPermissionPageDelete(DASHBOARD_NAME);
                },

                //resttrict
                restrictPersonalInfo: function () {
                    return restrictByName("PersonalInfo");
                },
                restrictSalary: function () {
                    return restrictByName("Salary");
                },
                restrictBackgroundChecks: function () {
                    return restrictByName("BackgroundChecks");
                },
                restrictCriminalRecords: function () {
                    return restrictByName("CriminalRecords");
                },
                restrictPreviousEmployments: function () {
                    return restrictByName("PreviousEmployments");
                }
            }

        }
    ]);