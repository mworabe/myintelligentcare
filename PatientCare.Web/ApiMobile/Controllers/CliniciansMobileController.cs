﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Web.ApiMobile.Models.Picklist;
using PatientCare.Web.ApiMobile.Models.AutoCompletes;
using System.Collections.Generic;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Services.FileStorage;
using PatientCare.Data.EntityFramework.Identity;

namespace PatientCare.Web.ApiMobile.Controllers
{
    /// <summary>
    /// This Controller is used to handle Clinicians Mobile operations.
    /// </summary>
    //[Authorize(Roles = ApplicationUser.PatientRole)]
    public sealed class CliniciansMobileController : ApiBaseController
    {
        /// <summary>
        /// Description : Clinicians Mobile IResourceRepository.
        /// </summary>
        private readonly IResourceRepository _resourceRepository;
        /// <summary>
        /// Description : Clinicians Mobile IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        /// Description : Initializes a new instance of the Clinicians Mobile class.
        /// </summary>
        /// <param name="resourceRepository">IResource Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public CliniciansMobileController
            (
                IResourceRepository resourceRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _resourceRepository = resourceRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Clinicians/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Therapist List.  
        /// </summary>
        /// <param name="autoCompleteRequest">Auto Complete Request Model</param>
        /// <returns>Picklist Default Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/therapist")]            
        [ResponseType(typeof(PicklistDefaultResponse))]
        public async Task<IHttpActionResult> therapistList([FromUri]AutoCompleteRequest autoCompleteRequest = null)
        {
            var filter = new FilterQuery<Resource>();
            if (!string.IsNullOrWhiteSpace(autoCompleteRequest.Search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(autoCompleteRequest.Search.ToLower()));
            }

            var resultList = new List<Resource>();
            if (autoCompleteRequest.Type == "all")
            {
                List<PicklistDefaultResponse> appointments = new List<PicklistDefaultResponse>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    string query = "EXEC sp_therapist_by_patientId @Type = 'all',@Search = '" + autoCompleteRequest.Search + "', @PatientId = '" + autoCompleteRequest.PatientId + "', @DataCount =0";
                    appointments = await ctx.Database.SqlQuery<PicklistDefaultResponse>(query).ToListAsync();
                    foreach (var item in appointments)
                    {
                        item.PhotoFileName = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    }
                    return Ok(appointments.ToArray());
                }
            }
            else
            {
                List<PicklistDefaultResponse> appointments = new List<PicklistDefaultResponse>();
                //appointments.TherapistRole = "Therapist";
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    string query = "EXEC sp_therapist_by_patientId  @Type = 'withSearch',@Search = '" + autoCompleteRequest.Search + "', @PatientId = '" + autoCompleteRequest.PatientId + "', @DataCount = 20";
                    appointments = await ctx.Database.SqlQuery<PicklistDefaultResponse>(query).ToListAsync();
                    foreach (var item in appointments)
                    {
                        item.PhotoFileName = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    }
                    return Ok(appointments.ToArray());
                }
            }
        }

    }
}
