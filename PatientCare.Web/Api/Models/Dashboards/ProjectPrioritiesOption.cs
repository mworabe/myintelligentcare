﻿namespace PatientCare.Web.Api.Models.Dashboards
{
    public enum ProjectPrioritiesOption
    {
        ByDivision = 0, 
        ByDepartment = 1
    }
}