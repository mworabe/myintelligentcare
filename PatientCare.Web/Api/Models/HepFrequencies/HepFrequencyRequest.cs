﻿using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.HepFrequencies
{
    public class HepFrequencyRequest
    {
        public long Id { get; set; }

        public long? HepId { get; set; }

        public long? HepDetailId { get; set; }

        public int? Day { get; set; }

        public int? ExerciseFrequency { get; set; }

        public bool? IsActive { get; set; }
    }   
}