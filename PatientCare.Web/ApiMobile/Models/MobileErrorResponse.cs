﻿using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models
{
    [DataContract]
    public class MobileErrorResponse
    {
        [DataMember]
        public ErrorTypeEnum? Type { get; set; }

        [DataMember]
        public string Message { get; set; }
       
    }
}