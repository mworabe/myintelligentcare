﻿using System;
using System.Data.SqlClient;
using System.Linq.Expressions;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Web.Api.Models
{
    public static class PagingExtensions<TEntity> 
    {
        public static PagedQuery<TEntity> CreatePagedQuery(ListRequest request, Expression<Func<TEntity, bool>> searchColumnsList = null, Expression<Func<TEntity, object>> customOrderBy = null)
        {
            var pagedQuery = new PagedQuery<TEntity>
            {
                ItemsToGet = request.PageSize,
                ItemsToSkip = (request.PageIndex - 1) * request.PageSize,
                SortOrder = (request.SortOrder == null || request.SortOrder.Value == SortOrders.Ascending) ? SortOrder.Ascending : SortOrder.Descending
            };

            if (customOrderBy != null)
            {
                pagedQuery.AddOrderBy(customOrderBy);
            }
            else
            {
                var sortField = request.SortField ?? "Id";

                if (QueryHelper.PropertyExists<TEntity>(sortField) || sortField.Contains("."))
                {
                    var exp = QueryHelper.GetPropertyExpression<TEntity>(sortField);
                    pagedQuery.AddOrderBy(exp);
                }
            }
            

            if (searchColumnsList != null)
            {
               return pagedQuery.AddFilter(searchColumnsList);
            }

            var searchFilter = GetSearchExpressionFilter(request);
            if (searchFilter != null)
            {
                pagedQuery.AddFilter(searchFilter);
            }

            return pagedQuery;
        }

        private static Expression<Func<TEntity, bool>> GetSearchExpressionFilter(ListRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.SearchField) || string.IsNullOrWhiteSpace(request.SearchPhrase))
            {
                return null;
            }

            if (!QueryHelper.PropertyExists<TEntity>(request.SearchField ?? string.Empty))
            {
                return null;
            }

            var stringContainsMethodInfo = typeof(string).GetMethod("Contains");
            var entityParameter = Expression.Variable(typeof(TEntity), "targetConcreteEntity");

            var accessPropertyExpression = Expression.PropertyOrField(entityParameter, request.SearchField);
            var filterExpression = (Expression)Expression.Call(accessPropertyExpression, stringContainsMethodInfo, Expression.Constant(request.SearchPhrase));

            return Expression.Lambda<Func<TEntity, bool>>(filterExpression, entityParameter);
        }
    }
}