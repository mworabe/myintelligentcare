﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("CommitteeMembershipService", ["$resource", function ($resource) {
      var resource = $resource("api/DropDownConfigs/:id/:controller",
        null, //parameters default
            {
                'update': { method: "PUT" },
                'query': { method: "GET", params: { "dropdowntype": "committeeMembership" }, isArray: false }
            });

      resource.getTableOption = {
          pageName: "Committee Membership",
          cols: function () {
              return [
                  { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                  { field: "value", title: "Name", sortable: "name", show: true, dataType: "text", required: true },
                  { field: "displayName", title: "Display Name", sortable: "name", show: true, dataType: "text", required: true },
                  { field: "dropDownType", title: "dropDownType", sortable: "name", show: false, dataType: "text", required: true },
                  { field: "action", title: ".", dataType: "command" },
              ];
          }
      };
      return resource;
  }]);
