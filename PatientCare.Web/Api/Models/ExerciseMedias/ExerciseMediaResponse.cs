﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Services.FileStorage;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ExerciseMedias
{
    [DataContract]
    public class ExerciseMediaResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? ExerciseId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Exercise { get; set; }

        [DataMember]
        public string OriginalName { get; set; }

        [DataMember]
        public MediaType? MediaType { get; set; }

        [DataMember]
        public string MediaURL { get; set; }

        //[DataMember]
        //public bool IsLocal
        //{
        //    get { return (StorageService.UseAmazonStorage); }
        //    set { IsLocal = value; }
        //}

        [DataMember]
        public string MediaOriginalName { get; set; }

        [DataMember]
        public FileRequest[] Files { get; set; }

        [DataMember]
        public string ThumbURL { get; set; }

        [DataMember]
        public string ThumbImageName { get; set; }

    }
}