﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.InjuryRegions;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.AutoCompletes;
using System.Collections.Generic;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Services.FileStorage;
using PatientCare.Web.Api.Models.ChatWeb;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Chat Web operations.
    /// </summary>
    [Authorize]
    public sealed class ChatwebController : ApiBaseController
    {
        /// <summary>
        /// Description : Chat Web Field IResourceRepository.
        /// </summary>
        private readonly IResourceRepository _resourceRepository;


        /// <summary>
        /// Description : Chat Web Field IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        /// Description :  Initializes a new instance of the Chat Web Class. 
        /// </summary>
        /// <param name="resourceRepository"></param>
        /// <param name="dataContextScopeFactory"></param>
        public ChatwebController
            (
                IResourceRepository resourceRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _resourceRepository = resourceRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Clinicians/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Chat User List.  
        /// </summary>
        /// <param name="autoCompleteRequest">AutoComplete Request Model</param>
        /// <returns>Chat User Response Model</returns>
        [HttpGet]
        [Route("api/mobile/v1/chatuserlist")]
        [ResponseType(typeof(ChatUserResponse))]
        public async Task<IHttpActionResult> chatUserList([FromUri]AutoCompleteRequest autoCompleteRequest = null)
        {
            string userId = User.Identity.GetUserId();
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            var resourceId = user.ResourceId;

            var filter = new FilterQuery<Resource>();
            if (!string.IsNullOrWhiteSpace(autoCompleteRequest.Search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(autoCompleteRequest.Search.ToLower()));
            }

            ChatUserResponse response = new ChatUserResponse();

            var resultList = new List<Resource>();
            if (autoCompleteRequest.Type == "all")
            {
                List<ChatTherapistList> therapistlist = new List<ChatTherapistList>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    //string query = "EXEC sp_chat_userlist  @ResourceId = '" + resourceId + "',@loginUserId = '" + userId + "' ,@Role = 'Therapist', @Type = 'all',@Search = '" + autoCompleteRequest.Search + "', @DataCount =0";
                    string query = "EXEC sp_chat_userlist  @loginUserId = '" + userId + "' ,@Role = 'Therapist', @Type = 'all',@Search = '" + autoCompleteRequest.Search + "', @DataCount =0";
                    therapistlist = await ctx.Database.SqlQuery<ChatTherapistList>(query).ToListAsync();
                    foreach (var item in therapistlist)
                    {
                        item.PhotoFileName = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    }
                    //return Ok(appointments.ToArray());
                }

                List<ChatFrontofficetList> frontOfficelist = new List<ChatFrontofficetList>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    //string query = "EXEC sp_chat_userlist  @ResourceId = '" + resourceId + "',@loginUserId = '" + userId + "' ,@Role = 'FrontOffice', @Type = 'all',@Search = '" + autoCompleteRequest.Search + "', @DataCount =0";
                    string query = "EXEC sp_chat_userlist  @loginUserId = '" + userId + "' ,@Role = 'FrontOffice', @Type = 'all',@Search = '" + autoCompleteRequest.Search + "', @DataCount =0";
                    frontOfficelist = await ctx.Database.SqlQuery<ChatFrontofficetList>(query).ToListAsync();
                    foreach (var item in frontOfficelist)
                    {
                        item.PhotoFileName = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    }
                }

                List<ChatPatientList> patientlist = new List<ChatPatientList>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    //string query = "EXEC sp_chat_userlist  @ResourceId = '" + resourceId + "',@loginUserId = '" + userId + "' ,@Role = 'Patient',@Type = 'all',@Search = '" + autoCompleteRequest.Search + "', @DataCount =0";
                    string query = "EXEC sp_chat_userlist  @loginUserId = '" + userId + "' ,@Role = 'Patient',@Type = 'all',@Search = '" + autoCompleteRequest.Search + "', @DataCount =0";
                    patientlist = await ctx.Database.SqlQuery<ChatPatientList>(query).ToListAsync();
                    foreach (var item in patientlist)
                    {
                        item.PhotoFileName = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.PatientFolderTemplate);
                    }
                }
                response.ChatTherapistLists = therapistlist.ToArray();
                response.ChatFrontofficetLists = frontOfficelist.ToArray();
                response.ChatPatientLists = patientlist.ToArray();
                return Ok(response);
            }
            else
            {
                List<ChatTherapistList> therapistlist = new List<ChatTherapistList>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    //string query = "EXEC sp_chat_userlist  @ResourceId = '" + resourceId + "',@loginUserId = '" + userId + "' , @Role = 'Therapist', @Type = 'withSearch',@Search = '" + autoCompleteRequest.Search + "', @DataCount =20";
                    string query = "EXEC sp_chat_userlist  @loginUserId = '" + userId + "' , @Role = 'Therapist', @Type = 'withSearch',@Search = '" + autoCompleteRequest.Search + "', @DataCount =20";
                    therapistlist = await ctx.Database.SqlQuery<ChatTherapistList>(query).ToListAsync();
                    foreach (var item in therapistlist)
                    {
                        item.PhotoFileName = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    }
                }

                List<ChatFrontofficetList> frontOfficelist = new List<ChatFrontofficetList>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    //string query = "EXEC sp_chat_userlist  @ResourceId = '" + resourceId + "',@loginUserId = '" + userId + "' , @Role = 'FrontOffice', @Type = 'withSearch',@Search = '" + autoCompleteRequest.Search + "', @DataCount =20";
                    string query = "EXEC sp_chat_userlist  @loginUserId = '" + userId + "' , @Role = 'FrontOffice', @Type = 'withSearch',@Search = '" + autoCompleteRequest.Search + "', @DataCount =20";
                    frontOfficelist = await ctx.Database.SqlQuery<ChatFrontofficetList>(query).ToListAsync();
                    foreach (var item in frontOfficelist)
                    {
                        item.PhotoFileName = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    }
                }

                List<ChatPatientList> patientlist = new List<ChatPatientList>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    //string query = "EXEC sp_chat_userlist  @ResourceId = '" + resourceId + "',@loginUserId = '" + userId + "' ,@Role = 'Patient',@Type = 'withSearch',@Search = '" + autoCompleteRequest.Search + "', @DataCount =20";
                    string query = "EXEC sp_chat_userlist  @loginUserId = '" + userId + "' ,@Role = 'Patient',@Type = 'withSearch',@Search = '" + autoCompleteRequest.Search + "', @DataCount =20";
                    patientlist = await ctx.Database.SqlQuery<ChatPatientList>(query).ToListAsync();
                    foreach (var item in patientlist)
                    {
                        item.PhotoFileName = FileUploadHelpers.GetStorageUrl(item.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate);
                    }
                }
                response.ChatTherapistLists = therapistlist.ToArray();
                response.ChatFrontofficetLists = frontOfficelist.ToArray();
                response.ChatPatientLists = patientlist.ToArray();
                return Ok(response);
            }
        }

    }
}
