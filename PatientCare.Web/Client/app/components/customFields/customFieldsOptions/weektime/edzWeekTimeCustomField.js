"use strict";
(function edzWeekTimeCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("weekTimeCustomField", ["edzBaseCustomField", edzWeekTimeCustomField]);

    ///////////////////////////////////////////////////////
    function edzWeekTimeCustomField(baseCustomField) {
        function WeekTimeCustomField() {
        }

        WeekTimeCustomField.prototype = Object.create(baseCustomField);
        WeekTimeCustomField.prototype.optionsTemplate = "<edz-week-time-custom-field-options></edz-week-time-custom-field-options>";
        WeekTimeCustomField.prototype.optionsTemplateEdit = "<edz-week-time-custom-field-edit-options></edz-week-time-custom-field-edit-options>";
        return WeekTimeCustomField;
    }
 })();