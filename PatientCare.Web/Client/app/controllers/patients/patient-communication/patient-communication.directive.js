﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('patientCommunication', function ($compile) {
    return {
        scope: {
            config: "=config",
            //loadingBarFlag: "=isLoading",
            patientId: "=",
            caseId: "="
        },
        templateUrl: "Client/app/controllers/patients/patient-communication/patient-communication.html",
        controller: ['$scope',
        function ($scope) {

            $scope.tabs = [{
                id: "Chat",
                title: "Chat",
            }, {
                id: "Mail",
                title: "Mail"
            }];

            $scope.selectedTab = $scope.tabs[0].id;

            $scope.isTabSelected = function (id) {
                if (id == $scope.selectedTab) return true;
                return false;
            }
            $scope.setSelectedTab = function (id) {
                $scope.selectedTab = id;
            }


        }]
    }
});



