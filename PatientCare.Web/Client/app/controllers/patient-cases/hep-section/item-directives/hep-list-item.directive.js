﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('hepListItem', function ($compile) {
    return {
        scope: {
            item: "=data",
            moveUp: "&",
            moveDown: "&",
            deleteFn: "&",
            isFirst: "=",
            isLast: "=",
            ungroupItemFn: "&",
            isToShowCheck: "=",
            isDeleteButtonHide: "=",
            isToShowUnGroupBtn: "="

        },

        templateUrl: "Client/app/controllers/patient-cases/hep-section/item-directives/hep-list-item.html",
        controller: ['$scope', 'ConstantFacory', function ($scope, ConstantFacory) {

            console.info($scope.moveUpFn);

            $scope.timeUnit = ConstantFacory.getTimeUnit();
            $scope.weightUnit = ConstantFacory.getWeightUnit();
            $scope.frequenciess = ConstantFacory.getFrequencies();
            $scope.resistancess = ConstantFacory.getResistances();
            $scope.myFrequ = false;

            $scope.myF = function (b) {
                if (_.isEqual(b, 'Other')) {
                    $scope.myFrequ = true;
                }
                else if (_.isEqual(b, '2x Day')) {
                    _.forEach($scope.item.frequencies, function (ii) {
                        ii.exerciseFrequency = 2;
                    });
                    $scope.myFrequ = false;
                }
                else if (_.isEqual(b, '3x Day')) {
                    _.forEach($scope.item.frequencies, function (ii) {
                        ii.exerciseFrequency = 3;
                    });
                    $scope.myFrequ = false;
                }
                else if (_.isEqual(b, '4x Day')) {
                    _.forEach($scope.item.frequencies, function (ii) {
                        ii.exerciseFrequency = 4;
                    });
                    $scope.myFrequ = false;
                }
                else if (_.isEqual(b, '5x Day')) {
                    _.forEach($scope.item.frequencies, function (ii) {
                        ii.exerciseFrequency = 5;
                    });
                    $scope.myFrequ = false;
                }
                else {
                    $scope.myFrequ = false;
                }
            }

            $scope.myediting = true;
            $scope.toShow;
            $scope.clickMe = function (a) {
                if (!$scope.toShow && !$scope.myediting) {
                    $scope.toShow = true;
                    $scope.myediting = false;
                }
                else if ($scope.toShow && !$scope.myediting){
                    $scope.toShow = false;
                    $scope.myediting = false;
                }
                else if ($scope.toShow && $scope.myediting) {
                    $scope.toShow = true;
                    $scope.myediting = false;
                }
                else if (!$scope.toShow && $scope.myediting) {
                    $scope.toShow = true;
                    $scope.myediting = false;
                }
            }


            $scope.toShow;

            $scope.expandcontract = function () {
                if ($scope.toShow)
                {
                $scope.toShow = false;
                $scope.myediting = true;
                }
                else
                {
                    $scope.toShow = true;
                    $scope.myediting = true;
                }
            }


            $scope.getDayName = function (item) {

                if (item.day || (item.day == 0)) {
                    if (item.day == 0)
                        return "Sun"
                    if (item.day == 1)
                        return "Mon"
                    if (item.day == 2)
                        return "Tue"
                    if (item.day == 3)
                        return "Wed"
                    if (item.day == 4)
                        return "Thurs"
                    if (item.day == 5)
                        return "Fri"
                    if (item.day == 6)
                        return "Sat"
                }

                return "";
            }

        }]
    }
});



