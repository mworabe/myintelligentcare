﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Role Configure Layout.
    /// </summary>
    public class RoleConfigureLayout : Entity
    {
        public LayoutTypeEnum Type { get; set; }
        public string Configure { get; set; }

        public string RoleId { get; set; }
        
    }
}
