namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedReferalDetailListSP : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
          "dbo.sp_referral_detail_list",
          p => new
          {
              pageIndex = p.Int(defaultValueSql: null),
              pageSize = p.Int(defaultValueSql: null),
              responseType = p.String(defaultValueSql: null),
              sortField = p.String(defaultValueSql: null),
              sortOrder = p.String(defaultValueSql: null),
              searchPhrase = p.String(defaultValueSql: null)
          },
          body:
            @" IF(@responseType = 'DataCount')
                BEGIN
                select CAST(Count(distinct refPhy.Id) AS  BIGINT) AS ItemCount 
                from referral_physicians as refPhy  	      
                END
                IF(@responseType = 'Data')
                BEGIN
                select maintable.Id,maintable.Name,maintable.FirstName,maintable.LastName,maintable.Speciality,maintable.MedicalDesignation,maintable.ExtensionNumber,
                maintable.MedicalDesignationOther,maintable.PhoneNumber,maintable.Fax,maintable.Email,maintable.FacilityName,
                maintable.PostalCode,maintable.CityId,maintable.City,maintable.StateId,maintable.State,maintable.CountryId,maintable.Country,
                maintable.ReferralsToDate,maintable.LastReferralReceived from (
                select 
                refPhy.Id,(refPhy.last_name + ' ' + refPhy.first_name) as Name,refPhy.first_name as FirstName,refPhy.last_name as LastName,refPhy.speciality as Speciality
                ,refPhy.medical_designation as MedicalDesignation,refPhy.extension_number as ExtensionNumber, 
                refPhy.medical_designation_other as MedicalDesignationOther,refPhy.phone_number as PhoneNumber,refPhy.fax as Fax,refPhy.email as Email,refPhy.facility_name as FacilityName,refPhy.postal_code as PostalCode,
                city.Id as CityId,city.Name as City,states.Id as StateId,states.Name as State,countries.Id as CountryId,countries.name as Country ,
                (select count(Id) from patients where referral_physician_id = refPhy.Id) as ReferralsToDate,
                (select top 1 (created) from patients where referral_physician_id = refPhy.Id order by created desc) as LastReferralReceived
                from referral_physicians as refPhy
                left join Cities as city on refPhy.city_id = city.Id
                left join States as states on refPhy.state_id = states.Id
                left join countries as countries on refPhy.country_id = countries.Id ) as maintable
                where (( maintable.Name like ('%'+@searchPhrase+'%'))
                or (maintable.Speciality like ('%'+@searchPhrase+'%'))    
                or (maintable.MedicalDesignation like ('%'+@searchPhrase+'%'))  
                or (maintable.FacilityName like ('%'+@searchPhrase+'%'))  
                or (maintable.City like ('%'+@searchPhrase+'%'))  
                or (maintable.ReferralsToDate like ('%'+@searchPhrase+'%'))) 

                order by
                case
                when @sortOrder <> 'Ascending' then 0
                when @sortField = 'Id' then maintable.Id
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'Name' then maintable.Name
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'Speciality' then  maintable.Speciality 
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'MedicalDesignation' then maintable.MedicalDesignation
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'FacilityName' then maintable.FacilityName
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'City' then maintable.City
                end ASC,
                case
                when @sortOrder <> 'Ascending' then ''
                when @sortField = 'ReferralsToDate' then maintable.ReferralsToDate
                end ASC,
                case
                when @sortOrder <> 'Ascending' then cast(null as date)
                when @sortField = 'LastReferralReceived' then maintable.LastReferralReceived
                end ASC,
	    
                case
                when @sortOrder <> 'Descending' then 0
                when @sortField = 'Id' then maintable.Id
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'Name' then  maintable.Name  
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'Speciality' then  maintable.Speciality 
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'MedicalDesignation' then maintable.MedicalDesignation
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'FacilityName' then maintable.FacilityName
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'City' then maintable.City
                end DESC,
                case
                when @sortOrder <> 'Descending' then ''
                when @sortField = 'ReferralsToDate' then maintable.ReferralsToDate
                end DESC,
                case
                when @sortOrder <> 'Descending' then cast(null as date)
                when @sortField = 'LastReferralReceived' then maintable.LastReferralReceived
                end DESC
                OFFSET(@pageIndex * @pageSize) ROWS FETCH NEXT @pageSize ROWS ONLY
                END"
                   );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_patient_detail_list");
        }
    }
}
