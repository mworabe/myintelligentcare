﻿using System;
using System.Collections.Generic;
using PatientCare.Core.Entities;
using PatientCare.Core.Entities.Abstract;


namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Time off Benefit.
    /// </summary>
    public class ResourceTimeoffBenefit : Entity
    {
        public long? ResourceId { get; set; }
        public virtual Resource Resource { get; set; }

        public long? TimeoffTypeId { get; set; }
        public virtual LeaveType TimeoffType { get; set; }
        
        public decimal? TotalBalance { get; set; }
        public decimal? Balance { get; set; }
        public decimal? AccruedCurrent { get; set; }
    }
}
