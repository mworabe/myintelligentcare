﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceSurveyResultRepository : IRepository<ResourceSurveyResult>
    {
    }
}
