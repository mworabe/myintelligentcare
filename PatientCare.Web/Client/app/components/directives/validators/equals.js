﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngEquals", [
        function() {
            return {
                restrict: "A",
                require: "ngModel",
                link: function(scope, elem, attrs, ngModel) {
                    function validate() {
                        var val1 = ngModel.$viewValue;
                        var val2 = attrs.ngEquals;

                        ngModel.$setValidity("equals", !val1 || !val2 || val1 === val2);
                    };

                    scope.$watch(attrs.ngModel, function() {
                        validate();
                    });

                    attrs.$observe("ngEquals", function() {
                        validate();
                    });
                }
            };
        }
    ]);