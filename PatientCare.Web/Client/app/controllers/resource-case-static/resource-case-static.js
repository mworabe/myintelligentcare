﻿//resourceCaseList

"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("resourceCaseList", {
                url: "/resource/cases",
                params: {},
                templateUrl: "Client/app/controllers/resource-case-static/ResourceCaseStaticView.html",
                controller: "ResourceCaseStaticCtrl as ResourceCaseStaticCtrl",
                data: {
                    requireLogin: true
                }
            });
    });