﻿namespace PatientCare.Web.Api.Models.ExerciseActivities
{
    public class ExerciseActivityRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }   
}