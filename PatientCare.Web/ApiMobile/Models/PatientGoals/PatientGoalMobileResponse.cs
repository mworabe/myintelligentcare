﻿using PatientCare.Core.Enums;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.PatientGoals
{
    [DataContract]
    public class PatientGoalMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public long? PatientCaseId { get; set; }

        [DataMember]
        public PatientGoalTypeEnum? GoalType { get; set; }

        [DataMember]
        public string GoalName { get; set; }

        [DataMember]
        public long? PatientMobileProfileId { get; set; }
    }
}