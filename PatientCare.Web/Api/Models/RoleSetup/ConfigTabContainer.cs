﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientCare.Web.Api.Models.RoleSetup
{
    public class ConfigTabContainer
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public ConfigFieldContainer[] Container1 { get; set; }
        public ConfigFieldContainer[] Container2 { get; set; }
        public ConfigFieldContainer[] Container3 { get; set; }
        public ConfigFieldContainer[] Container4 { get; set; }
        public ConfigFieldContainer[] Container5 { get; set; }
        public ConfigFieldContainer[] Container6 { get; set; }

    }
}