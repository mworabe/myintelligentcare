﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ChatWeb
{
    [DataContract]
    public class ChatUserResponse
    {
        [DataMember]
        public ChatPatientList[] ChatPatientLists { get; set; }

        [DataMember]
        public ChatTherapistList[] ChatTherapistLists { get; set; }

        [DataMember]
        public ChatFrontofficetList[] ChatFrontofficetLists { get; set; }
    }
}