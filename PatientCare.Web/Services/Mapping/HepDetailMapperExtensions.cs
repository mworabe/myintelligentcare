﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.HepDetails;
using EmitMapper.MappingConfiguration;
using PatientCare.Web.Api.Models.HepFrequencies;
using System.Linq;

namespace PatientCare.Web.Services.Mapping
{
    public static class HepDetailMapperExtensions
    {
        public static EmitMapperMapperProvider HepDetailRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepDetail, HepDetailResponse>
                    (new ExtendedMapConfig<HepDetail, HepDetailResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepDetail[], HepDetailResponse[]>());

            var hepFrequencyMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<HepFrequencyRequest, HepFrequency>(
                  new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                 ObjectMapperManager.DefaultInstance.GetMapper<HepDetailRequest, HepDetail>
                    (new ExtendedMapConfig<HepDetailRequest, HepDetail>()
                        .ForMember(x => x.Frequencies, y => y.Frequencies != null ? y.Frequencies.Select(i => hepFrequencyMapper.Map(i)).ToList() : null)
                    ));

            return mapperProvider;
        }
    }
}