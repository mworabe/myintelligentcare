﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface ILeaveTypeRepository : IRepository<LeaveType>
    {
    }
}
