namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesminorTableRename : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.appointment_type", newName: "appointment_types");
            RenameTable(name: "dbo.exercise_activitys", newName: "exercise_activities");
            RenameTable(name: "dbo.patient_appointment", newName: "patient_appointments");
            DropColumn("dbo.exercises", "ExerciseImage");
            DropColumn("dbo.exercises", "ExerciseVideo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.exercises", "ExerciseVideo", c => c.String());
            AddColumn("dbo.exercises", "ExerciseImage", c => c.String());
            RenameTable(name: "dbo.patient_appointments", newName: "patient_appointment");
            RenameTable(name: "dbo.exercise_activities", newName: "exercise_activitys");
            RenameTable(name: "dbo.appointment_types", newName: "appointment_type");
        }
    }
}
