﻿"use strict";
var EOPApp = angular.module("EDZoutstaffingPortalApp")
EOPApp.service("DiagnosisService", ["$resource", "$state", "$injector", function ($resource, $state, $injector) {
    var permissionWorker = null;
    function getPermissionWorker() {
        if (!permissionWorker)
            permissionWorker = $injector.get("PermissionWorker");
        return permissionWorker;
    }
    var resource = $resource("api/Diagnosis/:id/:controller", null, {
        "query": { method: "GET", params: { controller: "list" }, isArray: false },
        "update": { method: "put", params: {}, isArray: false },
        "getAll": { method: "GET", params: {}, isArray: false },
    });


    resource.getTableOption = {
        pageName: "Appointment Type",
        hideBulkEdit: true,
        formatDelete: function (row) {
            return row.lastName + " " + row.firstName;
        },
        disabledAddButton: function () {
            return !getPermissionWorker().canAssignmentInsert();
        },
        disabledEditButton: function () {
            return getPermissionWorker().canAssignmentUpdate();
        },
        disabledDeleteButton: function () {
            return getPermissionWorker().canAssignmentDelete();
        },
        cols: function () {
            return [
                {
                    field: "name",
                    title: "Assignment Name",
                    sortable: "name",
                    dataType: "text",
                    show: true,
                    dataTypeView: "href",
                    buildUrl: function (row) {
                        return $state.href("assignmentEdit", { id: row.id });
                    }
                }, {
                    field: "assignedTo",
                    title: "Assigned To",
                    sortable: false,
                    show: true,
                    dataType: "text",
                    customViewText: function (row) {
                        return row.resources[0].resource.name;
                    }
                },
                {
                    field: "startDate",
                    title: "Start Date",
                    sortable: "startDate",
                    show: true,
                    dataType: "text",
                    customViewText: function (row) {
                        return moment(row.startDate).format("MM/DD/YYYY");
                    }
                }, {
                    field: "endDate",
                    title: "End Date",
                    sortable: "endDate",
                    show: true,
                    customViewText: function (row) {
                        return (row.endDate) ? moment(row.endDate).format("MM/DD/YYYY") : moment(row.dueDate).format("MM/DD/YYYY");
                    }
                },
                {
                    field: "estimateTime",
                    title: "Estimated Hours",
                    sortable: "estimateTime",
                    show: true,
                    dataType: "text",
                },
                {
                    field: "timeSpent",
                    title: "Time Spent",
                    sortable: "timeSpent",
                    show: true,
                    dataType: "text",
                },
                {
                    field: "action",
                    title: ".",
                    class: "blank-cell",
                    dataType: "command"
                }
            ];
        },
        onAdd_Click: function () {
            if (!getPermissionWorker().canAssignmentInsert()) {
                return;
            }
            $state.go("assignmentEdit");
        },
        onEdit_Click: function (row) {
            $state.go("assignmentEdit", { id: row.id });
        },
        menuButtons: [
            {
                click: function (row) {
                    resource.setInactive({ id: row.id }, {}, function () {
                        resource.reloadCommand.reload();
                    });
                },

                iconClass: "icon icon-user-times"
            }
        ],
        headerButtons: [],
        headerRightItems: []

    };


    return resource;
}]);
