﻿using PatientCare.Core.Enums;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.JobTitles
{
    public class JobTitleRequest
    {
        public string Name { get; set; }

        public decimal? Rate { get; set; }

        public PriorityOrderEnum? PriorityOrder { get; set; }

        public bool? HolidayEligible { get; set; }

        public bool? HoursEdit { get; set; }

        public long? EntryTypeId { get; set; }
    }
}