﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .constant("DYNAMIC_FORM_CONSTROLLER", {
        FIELD_TYPE: {
            TEXT: "TEXT",
            DATE_TIME: "DATE_TIME",
            TIME: "TIME",
            UI_SELECT: "UI_SELECT",
            AUTOCOMPLETE: "AUTOCOMPLETE",
            PHONE: "PHONE",
            NUMBER: "NUMBER",
            CHECKBOX: "CHECKBOX",
            TEXTAREA: "TEXTAREA",
            CURRENCY: "CURRENCY",
            CUSTOM_TEMPLATE: "CUSTOM_TEMPLATE",
            MULTI_SELECT: "MULTI_SELECT",
            AUTO_COMPLETE_TYPE_ABLE: "AUTO_COMPLETE_TYPE_ABLE",
            AUTO_COMPLETE_WITH_BUTTON: "AUTO_COMPLETE_WITH_BUTTON"
        }
    });