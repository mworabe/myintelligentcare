﻿namespace PatientCare.Web.Api.Models.DashboardSetups
{
    public class DashboardSetupRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string SetupJson { get; set; }
    }
}