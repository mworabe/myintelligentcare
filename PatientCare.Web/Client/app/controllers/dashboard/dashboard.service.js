"use strict";
angular.module("EDZoutstaffingPortalApp")
    .service("DashboardResource",
    ["$rootScope", "$http", "$q", 'DASHBOARD_CONSTANTS', "DashboardSetups",
        function ($rootScope, $http, $q, DASHBOARD_CONSTANTS, DashboardSetupService) {
            var INITIAL_MODEL = {
                title: "IRMS Dashboard",
                structure: '4-8'
            };
            var GENERAL_URL = 'api/DashboardSetups';
            var SAVE_URL = 'api/DashboardSetups/UpsertDashBoard';
            var GET_SINGLE_URL = 'api/DashboardSetups/GetDashBoard';
            var memoizedProjectData = false;
            var tempModel = {};

            return {
                getDashboard: getDashboard,
                saveDashboard: _.debounce(saveDashboard, 500),
                deleteDashboard: delete_dashboard,
                getLastInsertedDashboard: getLastInsertedDashboard,
                getDashboardList: getDashboardList
            };

            ////////////////////////////////////////////////////////////////////////////////////////
            function saveChangeProject(data) {
                $rootScope.$emit(DASHBOARD_CONSTANTS.DASHBOARD_CHANGED_EVENT);
                memoizedProjectData = data;
            }

            function getLastInsertedDashboard() {
                return tempModel;
            }

            function getDashboardList() {
                var defer = $q.defer();
                DashboardSetupService.getDashboardList({
                    pageIndex: 1,
                    pageSize: 1000,
                    sortField: 'created',
                    sortOrder: 'ascending'
                }).$promise.then(function (responce) {
                    defer.resolve(responce.data || []);
                });
                return defer.promise;
            }

            function getDashboard(dashboardId) {
                return $q(function (resolve) {
                    var id = 0;
                    if (dashboardId && dashboardId > 0)
                        id = dashboardId;
                    $http.get(GET_SINGLE_URL + "/" + id).then(function (response) {
                        var dashboard = response.data;
                        var returnedModel = INITIAL_MODEL;
                        if (dashboard.id) {
                            returnedModel = JSON.parse(dashboard.setupJson);
                            returnedModel.id = dashboard.id;
                        }
                        saveChangeProject(returnedModel);
                        resolve(dashboard);
                    });
                });
            }

            function saveDashboard(model) {
                return $q(function (resolve) {
                    saveChangeProject(model);
                    $http.post(SAVE_URL, {
                        id: model.id,
                        name: model.title,
                        setupJson: JSON.stringify(model)
                    }).then(function (res) {
                        tempModel = res.data;
                        resolve(tempModel);
                    });
                });
            }

            function delete_dashboard(model) {
                return $q(function (resolve) {
                    $http.delete(SAVE_URL + "/" + model.id).then(function (res) {
                        resolve(res);
                    });
                });
            }

        }
    ]);
