﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Spoken Language.
    /// </summary>
    public class ResourceSpokenLanguage: Entity
    {
        public long ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        public long LanguageId { get; set; }
        public virtual Language Language { get; set; }
        public int YearsOfExpirience { get; set; }
        public int SpeakingProficiency { get; set; }
        public int WritingProficiency { get; set; }
    }
}
