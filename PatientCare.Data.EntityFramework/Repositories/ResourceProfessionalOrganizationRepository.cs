﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceProfessionalOrganizationRepository : RepositoryBase<ResourceProfessionalOrganization>, IResourceProfessionalOrganizationRepository
    {
        public ResourceProfessionalOrganizationRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
