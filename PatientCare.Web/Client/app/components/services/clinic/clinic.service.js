﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("ClinicService", ["$resource", function ($resource) {
    var resource = $resource("api/clinics/:controller/:id",
        null, //parameters default
        {
            'query': { method: "GET", params: {}, isArray: false },
            'update': { method: "PUT", params: {}, isArray: false },
            'all': { method: "GET", params: {}, isArray: true, url: "api/clinics/autocomplete?search=" },
        });
    return resource;
}
]);
