﻿using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.PostalCodes;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Cities
{
    [DataContract]
    public class CityResponse
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }

        //[DataMember]
        //public CodeResponse[] PostalCodeLists { get; set; }

        [DataMember]
        public CodeResponse State { get; set; }

        [DataMember]
        public long? StateId { get; set; }
        [DataMember]
        public string StateName { get; set; }
        [DataMember]
        public string StateCode { get; set; }
    }
}