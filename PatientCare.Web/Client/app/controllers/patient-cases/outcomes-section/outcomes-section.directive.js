﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('outComeSection', function ($compile) {
    return {
        scope: {
            config: "=config",

        },
        templateUrl: "Client/app/controllers/patient-cases/outcomes-section/outcomes-section.html",
        controller: ['$scope',
                    'toaster',
                    "MailDialogFactory",
        function ($scope, toaster, MailDialogFactory) {





            $scope.tabs = [{
                id: "Scorecard",
                title: "Scorecard",
            }, {
                id: "Surveys",
                title: "Surveys"
            }, {
                id: "Results",
                title: "Results"
            }];

            $scope.selectedTab = $scope.tabs[0].id;

            $scope.isTabSelected = function (id) {
                if (id == $scope.selectedTab) return true;
                return false;
            }
            $scope.setSelectedTab = function (id) {
                $scope.selectedTab = id;
            }


            $scope.scoreCardList = [
                { title: "Patient Satisfaction", result: "Yes" },
                { title: "Progress Tracking", result: "68%" },
                { title: "Visits per Case", result: "5" },
                { title: "Outcomes Survey Results: Dizziness Handicap Inventory", result: "60/100" },
                { subTitle: "Dizziness Handicap Inventory 1", result: "80/100" },
                { subTitle: "Dizziness Handicap Inventory 2", result: "30/100" },
                { subTitle: "Dizziness Handicap Inventory 3", result: "60/100" },
                { title: "<b>Total Scorecard Score<b>", result: "<b>5</b>" },
                
            ];

            //{ subTitle: "Dizziness Handicap Inventory", result: "80/100" },
            //    { subTitle: "Lower Extremity Functional Scale", result: "60/100" },
            //    { subTitle: "Modify Owestry Low Back Pain", result: "50/100" },
            //    { subTitle: "Neck Disability Index", result: "60/100" },
            //    { subTitle: "Upper Extremity Quick Dash", result: "80/100" }


            $scope.surveyDropDownConfig = [
                { value: "Dizziness Handicap Inventory (DHI)",key:"dhi" },
                { value: "Lower Extremity Functional Scale (LEFS)", key: "LEFS" },
                { value: "Modified Qwestry Lower Back Pain", key: "qwbp" },
                { value: "Neck Disability Index (NDI)", key: "ndi" },
                { value: "Upper Extremity QuickDash", key: "ueq" },
                { value: "Upper Extremity QuickDash – SPORTS/PERFORMING ARTS MODULE ", key: "ueqSports" },
                { value: "Upper Extremity QuickDash – WORK MODULE ", key: "ueqWork" },

            ]

            $scope.currentSelectedSurvey = $scope.surveyDropDownConfig[0];
            $scope.currentSelectedSurveyResult = $scope.surveyDropDownConfig[0];

           

            $scope.resultList = [
                {
                    title: "Dizziness Handicap Inventory 1",
                    date: "05/03/2017",
                    result: "80/100",
                    type:"dhi"

                },
                {
                    title: "Dizziness Handicap Inventory 2",
                    date: "04/26/2017",
                    result: "60/100",
                    type:"dhi"
                },
                {
                    title: "Dizziness Handicap Inventory 3",
                    date: "05/03/2017",
                    result: "75/100",
                    type: "dhi"
                }
            ]

            $scope.selectedResult = "";

            $scope.onViewResult = function (item) {
                $scope.selectedResult = item
            }

            
            $scope.sendMail = function () {
                var mail = {
                    subject: "",
                    body: ""
                }

                debugger;
                if ($scope.currentSelectedSurvey.key == 'dhi') {
                    mail.subject = "Dizziness Handicap Inventory";
                    mail.body = "Hello Matt,\n\nHere is your Dizziness Handicap Inventory form to fill  for your case - ABC-03-29-1. Please click on below link and fill the required details and submit the form.\nhttps://abc.myintelligentcare.com/patient/12202/dhi_form.html\n\n";
                    mail.body += "For any queries contact us on : number from mobile app appointmetn page\n\n";
                    mail.body += "James Smith\n";
                    mail.body += "Therapist\n";
                    
                } else {
                    mail.subject = "Upper Extremity QuickDash Form";
                    mail.body = "Hello Matt,\n\nHere is your Upper Extremity QuickDash form to fill for your case - ABC-03-29-1. Please click on below link and fill the required details and submit the form.\nhttps://abc.myintelligentcare.com/patient/12202/quickdash_form.html\n\n";
                    mail.body += "For any queries contact us on : number from mobile app appointmetn page\n\n";
                    mail.body += "James Smith\n";
                    mail.body += "Therapist\n";
                    
                }
                MailDialogFactory.showMailDialog(mail);
            }


        }]
    }
});



