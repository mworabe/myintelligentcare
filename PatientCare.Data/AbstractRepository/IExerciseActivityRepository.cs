﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IExerciseActivityRepository : IRepository<ExerciseActivity>
    {
    }
}
