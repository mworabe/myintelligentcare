﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.JobTitles
{
    [DataContract]
    public class JobTitleResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public decimal? Rate { get; set; }

        [DataMember]
        public PriorityOrderEnum? PriorityOrder { get; set; }

        [DataMember]
        public bool? HolidayEligible { get; set; }

        [DataMember]
        public bool? HoursEdit { get; set; }

        [DataMember]
        public long? EntryTypeId { get; set; }
        [DataMember]
        public virtual PicklistResponse EntryType { get; set; }
    }
}