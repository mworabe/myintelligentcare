﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Job Title.
    /// </summary>
    public class JobTitle : NamedEntity
    {
        public decimal? Rate { get; set; }

        public PriorityOrderEnum? PriorityOrder { get; set; }

        public bool? HolidayEligible { get; set; }

        public bool? HoursEdit { get; set; }

        public long? EntryTypeId { get; set; }
        public virtual EntryType EntryType { get; set; }
    }
}
