﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models
{
    [DataContract(Name = "errorResponse")]
    internal class ErrorResponse
    {
        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "modelState")]
        public Dictionary<string, IList<string>> ModelState { get; set; }
    }
}