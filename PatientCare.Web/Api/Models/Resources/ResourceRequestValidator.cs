﻿using System;
using System.Linq;
using PatientCare.Core.Entities;
using PatientCare.Core.Enums;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Services.Validation;
using PatientCare.Web.Services.Validation;
using FluentValidation;
using FluentValidation.Results;

namespace PatientCare.Web.Api.Models.Resources
{
    public class ResourceRequestValidator : AbstractValidator<ResourceRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IPicklistRepository<JobTitle> _positionRepository;
        private readonly IPicklistRepository<ResourceCompany> _resourceCompanyRepository;
        private readonly IPicklistRepository<Division> _divisionRepository;
        private readonly IPicklistRepository<Department> _departmentRepository;
        private readonly IPicklistRepository<Country> _countryRepository;
        private readonly ICustomFieldRepository _customFieldRepository;

        public ResourceRequestValidator(IDataContextScopeFactory dbContextScopeFactory,
            IPicklistRepository<JobTitle> positionRepository,
            IPicklistRepository<ResourceCompany> resourceCompanyRepository,
            IPicklistRepository<Division> divisionRepository,
            IPicklistRepository<Department> departmentRepository,
            IPicklistRepository<Country> countryRepository,
            ICustomFieldRepository customFieldRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _positionRepository = positionRepository;
            _resourceCompanyRepository = resourceCompanyRepository;
            _divisionRepository = divisionRepository;
            _departmentRepository = departmentRepository;
            _countryRepository = countryRepository;
            _customFieldRepository = customFieldRepository;

            RuleFor(model => model.FirstName)
                .NotEmpty()
                .Length(1, 255);

            RuleFor(model => model.LastName)
                .NotEmpty()
                .Length(1, 255);

            RuleFor(model => model.Address)
                .Length(0, 255);

            RuleFor(model => model.Address2)
                .Length(0, 255);

            RuleFor(model => model.ZipCode)
                .Length(0, 10)
                .Matches(@"^[0-9]+$")
                .WithMessage("Zip code should contain only digits");
            
            RuleFor(model => model.Location)
                .Length(1, 255);

            RuleFor(model => model.Ethnicity)
                .Length(1, 255);

            RuleFor(model => model.ArmedForcesCountryId)
                .Must(l => l != null
                    && ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _countryRepository, l.Value))
                .WithMessage("The country with provided Id doesn't exists")
                .When(r => r.ArmedForcesCountryId != null);
            
            RuleFor(model => model.EmployeeId)
                .NotEmpty()
                .Length(1, 255);

            RuleFor(model => model.EndDate)
                .GreaterThanOrEqualTo(m => m.StartDate);

            RuleFor(model => model.Range)
                .NotNull()
                .Must(itemType => Enum.IsDefined(typeof(RangeTypeEnum), itemType));

            Custom(request =>
            {
                using (dbContextScopeFactory.CreateReadOnly())
                {
                    var customFieldValuesIds = request.CustomFieldValues.Select(x => x.CustomFieldId).ToArray();
                    var customFieldResource = _customFieldRepository.List(new FilterQuery<CustomField>()
                        .AddFilter(x => x.ResourceAvailable || customFieldValuesIds.Contains(x.Id)), new ReadOptions<CustomField>().WithIncludePredicate(x => x.CustomFieldProperties)).ToArray();
                    var customFieldValues = request.CustomFieldValues;
                    var requiredFieldsId = customFieldResource.Where(x => x.Require).Select(x => x.Id).ToList();
                    var customFieldValuesRequire =
                        customFieldValues.Where(x => requiredFieldsId.Contains(x.CustomFieldId)).ToList();

                    if (requiredFieldsId.Count != customFieldValuesRequire.Count)
                        return new ValidationFailure("CustomFieldValues", "lacks fields");

                    foreach (var resourceCustomFieldValueRequest in customFieldValues)
                    {
                        var customField =
                            customFieldResource.FirstOrDefault(x => x.Id == resourceCustomFieldValueRequest.CustomFieldId);
                        if (customField == null)
                            continue;

                        var invalidMessage = CustomFieldValidation.Validation(customField, resourceCustomFieldValueRequest.Value);
                        if (!string.IsNullOrWhiteSpace(invalidMessage))
                            return new ValidationFailure("CustomFieldValues", string.Format("field {0} {1}", customField.Name, invalidMessage));
                    }
                    return null;
                }
            });
        }
    }
}