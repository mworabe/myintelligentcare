﻿using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceEducations
{
    public class ResourceEducationRequestValidator : AbstractValidator<ResourceEducationRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IResourceRepository _resourceRepository;

        public ResourceEducationRequestValidator(IDataContextScopeFactory dbContextScopeFactory, IResourceRepository resourceRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _resourceRepository = resourceRepository;

            RuleFor(model => model.Subject)
                .NotEmpty()
                .Length(1, 255)
                .WithMessage("'Degree' must be between 1 and 255 characters.You entered more then 255 characters.");

            RuleFor(model => model.NameOfSchool)
                 .NotEmpty()
                 .Length(1, 255)
                 .WithMessage("'Name of school' must be between 1 and 255 characters.You entered more then 255 characters.");

            //RuleFor(model => model.DateObtained)
            //    .NotEmpty();

            //RuleFor(model => model.StartDate)
            //    .NotEmpty();

            //RuleFor(model => model.EndDate)
            //    .NotEmpty()
            //    .GreaterThanOrEqualTo(m => m.StartDate);

            //RuleFor(model => model.ResourceId)
            //    .NotEmpty()
            //    .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _resourceRepository, l))
            //    .WithMessage("The resource with provided Id doesn't exists");
        }
    }
}