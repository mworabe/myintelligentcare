﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("trackedTableRow", [function () {
            return {
                restrict: "A",
                priority: -1,
                require: ["^trackedTable", "ngForm"],
                controller: ['$scope', '$parse', '$attrs', '$element', function ($scope, $parse, $attrs, $element) {
                    var self = this;
                    var row = $parse($attrs.trackedTableRow)($scope);
                    //var rowFormCtrl = $element.controller("form");
                    var trackedTableCtrl = $element.controller("trackedTable");

                    function isCellDirty(cell) {
                        return trackedTableCtrl.isCellDirty(row, cell);
                    }

                    function setCellDirty(cell, isDirty) {
                        trackedTableCtrl.setCellDirty(row, cell, isDirty);
                    }

                    function setCellInvalid(cell, isInvalid) {
                        trackedTableCtrl.setCellInvalid(row, cell, isInvalid);
                    }

                    self.isCellDirty = isCellDirty;
                    self.setCellDirty = setCellDirty;
                    self.setCellInvalid = setCellInvalid;

                }]
            };
        }
    ]);