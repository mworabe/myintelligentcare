﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ExerciseCommentMappingExtensions
    {
        public static DbModelBuilder MapExerciseComment(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ExerciseComment>().ToTable("exercise_comments").HasKey(i => i.Id);

            modelBuilder.Entity<ExerciseComment>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ExerciseComment>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ExerciseComment>()
                .Property(p => p.ExerciseId)
                .HasColumnName("exercise_id")
                .IsOptional();

            modelBuilder.Entity<ExerciseComment>()
                .HasOptional<Exercise>(p => p.Exercise)
                .WithMany()
                .HasForeignKey(p => p.ExerciseId);

            modelBuilder.Entity<ExerciseComment>()
                .Property(p => p.HepId)
                .HasColumnName("hep_id")
                .IsOptional();

            modelBuilder.Entity<ExerciseComment>()
                .Property(p => p.HepDetailId)
                .HasColumnName("hep_detail_id")
                .IsOptional();

            //modelBuilder.Entity<ExerciseComment>()
            //    .HasOptional<HepDetail>(p => p.HepDetail)
            //    .WithMany()
            //    .HasForeignKey(p => p.HepDetailId);

            modelBuilder.Entity<ExerciseComment>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<ExerciseComment>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<ExerciseComment>()
                .Property(p => p.PatientCaseId)
                .HasColumnName("patient_case_id")
                .IsOptional();

            modelBuilder.Entity<ExerciseComment>()
                .HasOptional<PatientCase>(p => p.PatientCase)
                .WithMany()
                .HasForeignKey(p => p.PatientCaseId);

            modelBuilder.Entity<ExerciseComment>()
                .Property(p => p.Comment)
                .HasColumnName("comment")
                .IsOptional();
            
            modelBuilder.Entity<ExerciseComment>()
                .Property(p => p.CommentDate)
                .HasColumnName("comment_date")
                .IsOptional();
            
            return modelBuilder;
        }
    }
}
