namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldInHepGroupTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hep_groups", "exercise_order", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.hep_groups", "exercise_order");
        }
    }
}
