﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Postal Code.
    /// </summary>
    public class PostalCode : Entity
    {
        public long? CityId { get; set; }
        public virtual City City { get; set; }

        public string ZipCode { get; set; }

        public long? StateId { get; set; }

        public string StateCode { get; set; }

        public long? CountryId { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }
    }
}
