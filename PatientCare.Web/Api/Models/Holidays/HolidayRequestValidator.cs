﻿using FluentValidation;

namespace PatientCare.Web.Api.Models.Holidays
{
    public class HolidayRequestValidator : AbstractValidator<HolidayRequest>
    {
        public HolidayRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);            
        }
    }
}