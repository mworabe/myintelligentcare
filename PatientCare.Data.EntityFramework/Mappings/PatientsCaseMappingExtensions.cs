﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PatientCaseMappingExtensions
    {
        public static DbModelBuilder MapPatientCase(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<PatientCase>().ToTable("patient_cases")
                .HasKey(i => i.Id);

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<PatientCase>()
               .Property(p => p.PatientId)
               .HasColumnName("patient_id")
               .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.PatientNumber)
                .HasColumnName("patient_number")
                .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.CaseNo)
                .HasColumnName("case_no")
                .IsOptional();

            modelBuilder.Entity<PatientCase>()
               .Property(p => p.ClinicId)
               .HasColumnName("clinic_id")
               .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .HasOptional<Clinic>(p => p.Clinic)
                .WithMany()
                .HasForeignKey(p => p.ClinicId);

            modelBuilder.Entity<PatientCase>()
               .Property(p => p.ClinicLocationId)
               .HasColumnName("clinic_location_id")
               .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .HasOptional<ClinicLocation>(p => p.ClinicLocation)
                .WithMany()
                .HasForeignKey(p => p.ClinicLocationId);

            modelBuilder.Entity<PatientCase>()
               .Property(p => p.CaseStatus)
               .HasColumnName("case_status")
               .IsOptional();

            modelBuilder.Entity<PatientCase>()
               .Property(p => p.StartDate)
               .HasColumnName("start_date")
               .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.BirthDate)
                .HasColumnName("birth_date")
                .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.Age)
                .HasColumnName("age")
                .IsOptional();

            modelBuilder.Entity<PatientCase>()
             .Property(p => p.InjuryRegionId)
             .HasColumnName("injury_region_id")
             .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .HasOptional<InjuryRegion>(p => p.InjuryRegion)
                .WithMany()
                .HasForeignKey(p => p.InjuryRegionId);

           modelBuilder.Entity<PatientCase>()
                .Property(p => p.AssignedPhysicianId)
                .HasColumnName("assigned_physician_id")
                .IsOptional();

           modelBuilder.Entity<PatientCase>()
                .HasOptional<Resource>(p => p.AssignedPhysician)
                .WithMany()
                .HasForeignKey(p => p.AssignedPhysicianId);

            modelBuilder.Entity<PatientCase>()
                 .Property(p => p.ReferingPhysicianId)
                 .HasColumnName("refering_physician_id")
                 .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .HasOptional<ReferralPhysician>(p => p.ReferingPhysician)
                .WithMany()
                .HasForeignKey(p => p.ReferingPhysicianId);

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.ReturnTo)
                .HasColumnName("return_to")
                .IsOptional();

            modelBuilder.Entity<PatientCase>()
              .Property(p => p.AuthorizeationRequired)
              .HasColumnName("authorizeation_required")
              .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.AuthorizeationNumber)
                .HasColumnName("authorizeation_number")
                .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.RelatedCause)
                .HasColumnName("related_cause")
                .IsOptional();

            modelBuilder.Entity<PatientCase>()
               .Property(p => p.PrimayDiagnosisId)
               .HasColumnName("primay_diagnosis_id")
               .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .HasOptional<Diagnosis>(p => p.PrimayDiagnosis)
                .WithMany()
                .HasForeignKey(p => p.PrimayDiagnosisId);

            modelBuilder.Entity<PatientCase>()
               .Property(p => p.SecondaryDiagnosisId)
               .HasColumnName("secondary_diagnosis_id")
               .IsOptional();

            modelBuilder.Entity<PatientCase>()
                .HasOptional<Diagnosis>(p => p.SecondaryDiagnosis)
                .WithMany()
                .HasForeignKey(p => p.SecondaryDiagnosisId);

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.WebPTCaseId)
                .HasColumnName("webpt_case_id")
                .IsOptional()
                .HasMaxLength(500);

            modelBuilder.Entity<PatientCase>()
                .Property(p => p.IsDeleted)
                .HasColumnName("is_deleted")
                .IsOptional();

            return modelBuilder;
        }
    }
}
