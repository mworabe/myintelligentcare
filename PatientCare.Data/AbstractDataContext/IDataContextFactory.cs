﻿namespace PatientCare.Data.AbstractDataContext
{
    public interface IDataContextFactory
    {
        object CreateDataContext();
    }
}
