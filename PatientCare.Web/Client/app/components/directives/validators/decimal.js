// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.
//
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngDecimal", [
        function () {
            return {
                restrict: "A",
                require: "ngModel",
                link: function ($scope, $elem, $attrs, ngCtrl) {
                    $scope.$watch($attrs.ngModel, function(value) {
                        if(value) {
                            if($elem.val().indexOf('.') !== -1) {
                                if($elem.val().split('.')[1].length > 2) {
                                    ngCtrl.$setViewValue(value.toFixed(2));
                                    ngCtrl.$render();
                                }
                            }
                        }
                    });
                }
            };
        }
    ]);