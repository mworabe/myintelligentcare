﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IChatSessionRepository : IRepository<ChatSession>
    {
    }
}
