﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.ResourceIndustry;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceIndustryMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceIndustryRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceIndustry,ResourceIndustryResponse>
                    (new ExtendedMapConfig<ResourceIndustry,ResourceIndustryResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceIndustry[],ResourceIndustryResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceIndustryResponse, ResourceIndustry>
                    (new ExtendedMapConfig<ResourceIndustryResponse, ResourceIndustry>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceIndustryRequest,ResourceIndustry>
                    (new ExtendedMapConfig<ResourceIndustryRequest,ResourceIndustry>()));

            return mapperProvider;
        }
    }
}