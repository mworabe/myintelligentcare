﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class HepGroupMappingExtensions
    {
        public static DbModelBuilder MapHepGroup(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<HepGroup>().ToTable("hep_groups")
                .HasKey(i => i.Id);

            modelBuilder.Entity<HepGroup>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<HepGroup>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<HepGroup>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            modelBuilder.Entity<HepGroup>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<HepGroup>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<HepGroup>()
                .Property(p => p.PatientCaseId)
                .HasColumnName("patient_case_id")
                .IsOptional();

            modelBuilder.Entity<HepGroup>()
                .HasOptional<PatientCase>(p => p.PatientCase)
                .WithMany()
                .HasForeignKey(p => p.PatientCaseId);

            modelBuilder.Entity<HepGroup>()
                .Property(p => p.HepId)
                .HasColumnName("hep_id")
                .IsOptional();

            modelBuilder.Entity<HepGroup>()
                .HasOptional<HepMaster>(p => p.Hep)
                .WithMany()
                .HasForeignKey(p => p.HepId);

            modelBuilder.Entity<HepGroup>()
                .HasMany<HepDetail>(p => p.HepDetails)
                .WithOptional()
                .HasForeignKey(item => item.GroupId);

            modelBuilder.Entity<HepGroup>()
                .Property(p => p.ExerciseOrder)
                .HasColumnName("exercise_order")
                .IsOptional();

            return modelBuilder;
        }
    }
}
