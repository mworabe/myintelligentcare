﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class MasterTemplateRepository : RepositoryBase<MasterTemplate>, IMasterTemplateRepository  
    {
        public MasterTemplateRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
