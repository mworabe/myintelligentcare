﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IPatientWorkoutAnalyticRepository : IRepository<PatientWorkoutAnalytic>
    {
    }
}
