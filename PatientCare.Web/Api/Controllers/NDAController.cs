﻿using Microsoft.AspNet.Identity;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Core.Services.Mailing;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework.Identity;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.Account;
using PatientCare.Web.ApiMobile.Models.ChatSessions;
using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle NDA operations.
    /// </summary>
    //[Authorize]
    public sealed class NDAController : ApiBaseController
    {
        /// <summary>
        /// Description : Initializes a new instance of the NDA Class.  
        /// </summary>
        public NDAController()
        {
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/nda/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Update NDA.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="request">Account Request Model</param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/nda/{id}")]
        public async Task<IHttpActionResult> InsertUpdateUserNDA(long? id, AccountRequest request)
        {
            AccountRequest model = new AccountRequest();
            //model.Id = (!string.IsNullOrWhiteSpace(request.Id)) ? request.Id : Guid.NewGuid().ToString();
            //model.isUpdate = (!string.IsNullOrWhiteSpace(request.Id)) ? true : false;
            //model.IsNDA = (!string.IsNullOrWhiteSpace(request.IsNDA.ToString())) ? true : false;
            model.Id = request.Id;
            model.IsNDA = request.IsNDA;

            var item = Mapper.Map<AccountRequest, ApplicationUser>(model);
            string userId = request.Id;

            if (request.Id != null)
            {
                var user = await AppUserManager.FindByIdAsync(request.Id);
                SetApplicationUserModel(user, model);
                user.IsNDA = request.IsNDA;
                return Ok(await AppUserManager.UpdateAsync(user));
            }
            return Ok("Id not Null");

            //if (model.isUpdate == false)
            //{
            //    return Ok(await AppUserManager.CreateAsync(item));
            //}
            //else
            //{
            //    var user = await AppUserManager.FindByIdAsync(request.Id);
            //    SetApplicationUserModel(user, model);
            //    user.IsNDA = request.IsNDA;
            //    return Ok(await AppUserManager.UpdateAsync(user));
            //}
        }

        /// <summary>
        /// Method Description : Using for Set Model Data.  
        /// </summary>
        /// <param name="model">Application User</param>
        /// <param name="request">Account Request</param>
        public void SetApplicationUserModel(ApplicationUser model, AccountRequest request)
        {
            model.Id = request.Id;
            model.IsNDA = request.IsNDA;
        }

    }
}
