﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("ClinicLocationService", ["$resource", function ($resource) {
    var resource = $resource("api/clinicLocations/:controller/:id",
        null, //parameters default
        {
            'query': { method: "GET", params: {}, isArray: false },
            'update': { method: "PUT", params: {}, isArray: false },
            'all': { method: "GET", params: {}, isArray: true, url: "api/cliniclocations/autocomplete?search=" },
            'alll': { method: "GET", params: {}, isArray: true, url: "api/ClinicLocations/autocompletee" },
        });
    return resource;
}
]);
