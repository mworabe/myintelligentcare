@echo off

setlocal

set PATH=%ProgramFiles(x86)%\MSBuild\12.0\Bin\amd64;%systemroot%\Microsoft.NET\Framework64\v4.0.30319;%PATH%

@echo on

MSBuild.exe "%~1" /nologo /maxcpucount /consoleloggerparameters:ShowCommandLine;ShowTimestamp /target:"%~2" /property:Configuration="%~3" "%~4" "%~5" "%~6" "%~7" "%~8" "%~9"

@echo off

endlocal
