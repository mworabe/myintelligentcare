"use strict";

(function edzCheckboxCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzCheckboxCustomFieldOptions", [
            edzCheckboxCustomFieldOptions
        ]);

    function edzCheckboxCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/checkbox/edzCheckboxCustomField.html",
            restrict: "E",
            scope: false
        };
    }
})();
