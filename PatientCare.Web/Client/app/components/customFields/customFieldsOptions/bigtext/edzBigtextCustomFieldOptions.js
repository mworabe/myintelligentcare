"use strict";

(function edzBigtextCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzBigtextCustomFieldOptions", [
            edzBigtextCustomFieldOptions
        ]);

    function edzBigtextCustomFieldOptions() {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/bigtext/edzBigtextCustomField.html",
            restrict: "E",
            scope: false,
            controller: ["$scope", function ($scope) {
                $scope.validation = {};
                $scope.validation.require = $scope.require;

                function loadData() {

                }

                function activate() {
                    loadData();
                }

                activate();
            }]
        };
    }
})();
