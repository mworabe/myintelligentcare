﻿using PatientCare.Web.Api.Models.Clinics;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PatientCases
{
    [DataContract]
    public class PatientCaseListResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Prefix { get; set; }
        
        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public long? ClinicId { get; set; }

        [DataMember]
        public ClinicAutoCompleteResponse Clinic { get; set; }
    }
}