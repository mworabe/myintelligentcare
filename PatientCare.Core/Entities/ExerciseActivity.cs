﻿using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Exercise Activity.
    /// </summary>
    public class ExerciseActivity : NamedEntity
    {
        public virtual ICollection<Exercise_ExerciseActivity> exerciseactivities { get; set; }
    }
}
