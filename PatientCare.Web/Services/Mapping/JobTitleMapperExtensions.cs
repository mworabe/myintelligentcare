﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.JobTitles;
using PatientCare.Web.Api.Models.Picklist;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class JobTitleMapperExtensions
    {
        public static EmitMapperMapperProvider JobTitleRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<JobTitle, JobTitleResponse>(new ExtendedMapConfig<JobTitle, JobTitleResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<JobTitle[], JobTitleResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<JobTitleRequest, JobTitle>(new ExtendedMapConfig<JobTitleRequest, JobTitle>()));

            mapperProvider.RegisterMapper(() =>
                    ObjectMapperManager.DefaultInstance.GetMapper<JobTitle[], PicklistResponse[]>());

            mapperProvider.RegisterMapper(() =>
                    ObjectMapperManager.DefaultInstance.GetMapper<JobTitle[], JobTitlePickListResponse[]>());

            return mapperProvider;
        }
    }
}