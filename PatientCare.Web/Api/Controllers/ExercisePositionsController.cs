﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.ExercisePositions;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle  Exercise Position operations.
    /// </summary>
    [Authorize]
    public sealed class ExercisePositionsController : ApiBaseController
    {
        /// <summary>
        /// Description : Exercise Position Field IConfigurationDataService.
        /// </summary>
        private readonly IConfigurationDataService _configurationDataService;

        /// <summary>
        /// Description : Exercise Position Field IExercisePositionRepository.
        /// </summary>
        private readonly IExercisePositionRepository _exercisePositionRepository;

        /// <summary>
        /// Description : Exercise Position Field IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<ExercisePosition> _readOptions = new ReadOptions<ExercisePosition>();

        /// <summary>
        /// Description : Initializes a new instance of the Exercise Position Class.    
        /// </summary>
        /// <param name="configurationDataService">IConfiguration Data Service</param>
        /// <param name="exercisePositionRepository">IExercise Position Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public ExercisePositionsController
            (
                IConfigurationDataService configurationDataService,
                IExercisePositionRepository exercisePositionRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _configurationDataService = configurationDataService;
            _exercisePositionRepository = exercisePositionRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/ExercisePositions/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Exercise Position.    
        /// </summary>
        /// <param name="request">Exercise Position List Request Model</param>
        /// <returns>Exercise Position Response Model</returns>
        [HttpGet]
        [Route("api/ExercisePositions")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ExercisePositionResponse>))]
        public IHttpActionResult GetAllExercisePosition([FromUri]ExercisePositionListRequest request)
        {
            var query = PagingExtensions<ExercisePosition>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            var readOptions = new ReadOptions<ExercisePosition>().AsReadOnly();
            var page = _exercisePositionRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<ExercisePosition[], ExercisePositionResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<ExercisePositionResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Exercise Position.   
        /// </summary>
        /// <param name="exercisePositionRequest">Exercise Position Request Model</param>
        /// <returns>Exercise Position Response Model</returns>
        [HttpPost]
        [Route("api/ExercisePositions")]
        [SwaggerResponse(201, "Created", typeof(ExercisePositionResponse))]
        public async Task<IHttpActionResult> CreateExercisePosition([FromBody]ExercisePositionRequest exercisePositionRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_exercisePositionRepository.Exists(FilterQuery.Create<ExercisePosition>().AddFilter(p => p.Name == exercisePositionRequest.Name)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<ExercisePosition>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var ExercisePosition = Mapper.Map<ExercisePositionRequest, ExercisePosition>(exercisePositionRequest);
            await _configurationDataService.CreateExercisePositionAsync(ExercisePosition);

            return Created(GetEntityLocation(ExercisePosition.Id), Mapper.Map<ExercisePosition, ExercisePositionResponse>(ExercisePosition));
        }

        /// <summary>
        /// Method Description : Using for Update Exercise Position.   
        /// </summary>
        /// <param name="id">Exercise Position Request Model</param>
        /// <param name="exercisePositionRequest">Exercise Position Request Model</param>
        /// <returns>Exercise Position Response Model</returns>
        [HttpPut]
        [Route("api/ExercisePositions/{id}")]
        public async Task<IHttpActionResult> EditExercisePosition(long id, [FromBody] ExercisePositionRequest exercisePositionRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_exercisePositionRepository.Exists(FilterQuery.Create<ExercisePosition>().AddFilter(p => p.Name == exercisePositionRequest.Name && p.Id != exercisePositionRequest.Id)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<ExercisePosition>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var ExercisePosition = Mapper.Map<ExercisePositionRequest, ExercisePosition>(exercisePositionRequest);
            ExercisePosition.Id = id;

            await _configurationDataService.UpdateExercisePositionAsync(ExercisePosition);

            return Ok(Mapper.Map<ExercisePosition, ExercisePositionResponse>(ExercisePosition));
        }

        /// <summary>
        /// Method Description : Using for Delete Exercise Position by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Exercise Position Response Model</returns>
        [HttpDelete]
        [Route("api/ExercisePositions/{id}")]
        public async Task<IHttpActionResult> DeleteExercisePositionItem(long id)
        {
            var success = await _configurationDataService.DeleteExercisePositionAsync(id);
            if (!success)
            {
                return NotFound();
            }

            return Ok(new EntryType { Id = id });
        }

        /// <summary>
        /// Method Description : Using for Get Exercise Position by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Exercise Position Response Model</returns>
        [HttpGet]
        [Route("api/ExercisePositions/{id}")]
        public async Task<IHttpActionResult> GetExercisePosition(long id)
        {
            var ExercisePosition = await _configurationDataService.ReadExercisePositionAsync(id);
            if (ExercisePosition == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<ExercisePosition, ExercisePositionResponse>(ExercisePosition);
            return Ok(viewModel);
        }

        private static Expression<Func<ExercisePosition, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return exerciseposition => exerciseposition.Name.Contains(searchPhrase);
        }

        private static Expression<Func<ExercisePosition, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return exerciseposition => exerciseposition.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Exercise Position.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Exercise Position Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/ExercisePositions/autocomplete")]
        [ResponseType(typeof(ExercisePositionAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<ExercisePosition>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
            }

            var autocompleteResult = await _exercisePositionRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<ExercisePosition[], ExercisePositionAutoCompleteResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }
    }
}
