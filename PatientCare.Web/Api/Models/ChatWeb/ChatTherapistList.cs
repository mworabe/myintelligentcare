﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ChatWeb
{
    [DataContract]
    public class ChatTherapistList
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string PhotoFileName { get; set; }

        [DataMember]
        public string UserId { get; set; }
    }
}