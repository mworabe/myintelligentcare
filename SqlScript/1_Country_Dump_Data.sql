-- setting to null resource countries fields
update r set country_id = null, armed_forces_country_id = null
from resources r
where country_id is not null or armed_forces_country_id is not null

-- setting to null patient_addresss countries fields
update pa set country_id = null
from patient_addresss pa
where country_id is not null

-- setting to null patient_parents_guardians countries fields
update ppg set country_id = null
from patient_parents_guardians ppg
where country_id is not null

-- setting to null referral_physicians countries fields
update ref set country_id = null
from referral_physicians ref
where country_id is not null

-- setting to null resource_travels countries fields
update rt set country_id = null
from resource_travels rt
where country_id is not null

-- deleting old countries values
delete from countries

-- reset index from 1
DBCC CHECKIDENT ('[countries]', RESEED, 0);

-- adding new values to countries
SET IDENTITY_INSERT [dbo].[countries] ON 

GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (1, N'Afghanistan', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AF', N'93')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (2, N'Albania', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AL', N'355')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (3, N'Algeria', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'DZ', N'213')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (4, N'American Samoa', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AS', N'1-684')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (5, N'Andorra', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AD', N'376')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (6, N'Angola', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AO', N'244')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (7, N'Anguilla', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AI', N'1-264')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (8, N'Antarctica', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AQ', N'672')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (9, N'Antigua and Barbuda', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AG', N'1-268')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (10, N'Argentina', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AR', N'54')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (11, N'Armenia', CAST(N'2017-05-04 05:46:53.967' AS DateTime), CAST(N'2017-05-04 05:46:53.967' AS DateTime), N'AM', N'374')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (12, N'Aruba', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'AW', N'297')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (13, N'Australia', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'AU', N'61')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (14, N'Austria', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'AT', N'43')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (15, N'Azerbaijan', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'AZ', N'994')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (16, N'Bahamas', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BS', N'1-242')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (17, N'Bahrain', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BH', N'973')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (18, N'Bangladesh', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BD', N'880')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (19, N'Barbados', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BB', N'1-246')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (20, N'Belarus', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BY', N'375')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (21, N'Belgium', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BE', N'32')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (22, N'Belize', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BZ', N'501')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (23, N'Benin', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BJ', N'229')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (24, N'Bermuda', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BM', N'1-441')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (25, N'Bhutan', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BT', N'975')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (26, N'Bolivia', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BO', N'591')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (27, N'Bosnia and Herzegowina', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BA', N'387')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (28, N'Botswana', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BW', N'267')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (29, N'Bouvet Island', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BV', N'47')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (30, N'Brazil', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BR', N'55')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (31, N'British Indian Ocean Territory', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'IO', N'246')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (32, N'Brunei Darussalam', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BN', N'673')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (33, N'Bulgaria', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BG', N'359')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (34, N'Burkina Faso', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BF', N'226')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (35, N'Burundi', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'BI', N'257')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (36, N'Cambodia', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'KH', N'855')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (37, N'Cameroon', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CM', N'237')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (38, N'Canada', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CA', N'1')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (39, N'Cape Verde', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CV', N'238')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (40, N'Cayman Islands', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'KY', N'1-345')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (41, N'Central African Republic', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CF', N'236')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (42, N'Chad', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'TD', N'235')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (43, N'Chile', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CL', N'56')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (44, N'China', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CN', N'86')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (45, N'Christmas Island', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CX', N'61')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (46, N'Cocos (Keeling) Islands', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CC', N'61')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (47, N'Colombia', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CO', N'57')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (48, N'Comoros', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'KM', N'269')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (49, N'Congo Democratic Republic of', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CG', N'242')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (50, N'Cook Islands', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CK', N'682')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (51, N'Costa Rica', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CR', N'506')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (52, N'Cote D''Ivoire', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CI', N'225')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (53, N'Croatia', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'HR', N'385')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (54, N'Cuba', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CU', N'53')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (55, N'Cyprus', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CY', N'357')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (56, N'Czech Republic', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'CZ', N'420')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (57, N'Denmark', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'DK', N'45')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (58, N'Djibouti', CAST(N'2017-05-04 05:46:53.970' AS DateTime), CAST(N'2017-05-04 05:46:53.970' AS DateTime), N'DJ', N'253')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (59, N'Dominica', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'DM', N'1-767')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (60, N'Dominican Republic', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'DO', N'1-809')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (61, N'Timor-Leste', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'TL', N'670')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (62, N'Ecuador', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'EC', N'593')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (63, N'Egypt', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'EG', N'20')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (64, N'El Salvador', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'SV', N'503')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (65, N'Equatorial Guinea', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'GQ', N'240')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (66, N'Eritrea', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'ER', N'291')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (67, N'Estonia', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'EE', N'372')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (68, N'Ethiopia', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'ET', N'251')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (69, N'Falkland Islands (Malvinas)', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'FK', N'500')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (70, N'Faroe Islands', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'FO', N'298')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (71, N'Fiji', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'FJ', N'679')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (72, N'Finland', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'FI', N'358')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (73, N'France', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'FR', N'33')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (75, N'French Guiana', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'GF', N'594')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (76, N'French Polynesia', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'PF', N'689')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (77, N'French Southern Territories', CAST(N'2017-05-04 05:46:53.973' AS DateTime), CAST(N'2017-05-04 05:46:53.973' AS DateTime), N'TF', NULL)
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (78, N'Gabon', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GA', N'241')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (79, N'Gambia', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GM', N'220')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (80, N'Georgia', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GE', N'995')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (81, N'Germany', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'DE', N'49')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (82, N'Ghana', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GH', N'233')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (83, N'Gibraltar', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GI', N'350')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (84, N'Greece', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GR', N'30')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (85, N'Greenland', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GL', N'299')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (86, N'Grenada', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GD', N'1-473')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (87, N'Guadeloupe', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GP', N'590')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (88, N'Guam', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GU', N'1-671')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (89, N'Guatemala', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GT', N'502')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (90, N'Guinea', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GN', N'224')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (91, N'Guinea-bissau', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GW', N'245')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (92, N'Guyana', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'GY', N'592')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (93, N'Haiti', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'HT', N'509')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (94, N'Heard Island and McDonald Islands', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'HM', N'011')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (95, N'Honduras', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'HN', N'504')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (96, N'Hong Kong', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'HK', N'852')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (97, N'Hungary', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'HU', N'36')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (98, N'Iceland', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'IS', N'354')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (99, N'India', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'IN', N'91')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (100, N'Indonesia', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'ID', N'62')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (101, N'Iran (Islamic Republic of)', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'IR', N'98')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (102, N'Iraq', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'IQ', N'964')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (103, N'Ireland', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'IE', N'353')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (104, N'Israel', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'IL', N'972')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (105, N'Italy', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'IT', N'39')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (106, N'Jamaica', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'JM', N'1-876')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (107, N'Japan', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'JP', N'81')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (108, N'Jordan', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'JO', N'962')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (109, N'Kazakhstan', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'KZ', N'7')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (110, N'Kenya', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'KE', N'254')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (111, N'Kiribati', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'KI', N'686')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (112, N'Korea, Democratic People''s Republic of', CAST(N'2017-05-04 05:46:53.977' AS DateTime), CAST(N'2017-05-04 05:46:53.977' AS DateTime), N'KP', N'850')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (113, N'South Korea', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'KR', N'82')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (114, N'Kuwait', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'KW', N'965')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (115, N'Kyrgyzstan', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'KG', N'996')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (116, N'Lao People''s Democratic Republic', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'LA', N'856')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (117, N'Latvia', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'LV', N'371')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (118, N'Lebanon', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'LB', N'961')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (119, N'Lesotho', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'LS', N'266')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (120, N'Liberia', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'LR', N'231')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (121, N'Libya', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'LY', N'218')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (122, N'Liechtenstein', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'LI', N'423')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (123, N'Lithuania', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'LT', N'370')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (124, N'Luxembourg', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'LU', N'352')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (125, N'Macao', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MO', N'853')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (126, N'Macedonia, The Former Yugoslav Republic of', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MK', N'389')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (127, N'Madagascar', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MG', N'261')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (128, N'Malawi', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MW', N'265')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (129, N'Malaysia', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MY', N'60')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (130, N'Maldives', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MV', N'960')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (131, N'Mali', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'ML', N'223')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (132, N'Malta', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MT', N'356')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (133, N'Marshall Islands', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MH', N'692')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (134, N'Martinique', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MQ', N'596')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (135, N'Mauritania', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MR', N'222')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (136, N'Mauritius', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MU', N'230')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (137, N'Mayotte', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'YT', N'262')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (138, N'Mexico', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MX', N'52')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (139, N'Micronesia, Federated States of', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'FM', N'691')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (140, N'Moldova', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MD', N'373')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (141, N'Monaco', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MC', N'377')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (142, N'Mongolia', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MN', N'976')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (143, N'Montserrat', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MS', N'1-664')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (144, N'Morocco', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MA', N'212')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (145, N'Mozambique', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MZ', N'258')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (146, N'Myanmar', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'MM', N'95')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (147, N'Namibia', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'NA', N'264')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (148, N'Nauru', CAST(N'2017-05-04 05:46:53.980' AS DateTime), CAST(N'2017-05-04 05:46:53.980' AS DateTime), N'NR', N'674')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (149, N'Nepal', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NP', N'977')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (150, N'Netherlands', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NL', N'31')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (151, N'Netherlands Antilles', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'AN', N'599')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (152, N'New Caledonia', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NC', N'687')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (153, N'New Zealand', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NZ', N'64')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (154, N'Nicaragua', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NI', N'505')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (155, N'Niger', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NE', N'227')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (156, N'Nigeria', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NG', N'234')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (157, N'Niue', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NU', N'683')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (158, N'Norfolk Island', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NF', N'672')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (159, N'Northern Mariana Islands', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'MP', N'1-670')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (160, N'Norway', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'NO', N'47')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (161, N'Oman', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'OM', N'968')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (162, N'Pakistan', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'PK', N'92')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (163, N'Palau', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'PW', N'680')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (164, N'Panama', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'PA', N'507')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (165, N'Papua New Guinea', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'PG', N'675')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (166, N'Paraguay', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'PY', N'595')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (167, N'Peru', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'PE', N'51')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (168, N'Philippines', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'PH', N'63')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (169, N'Pitcairn', CAST(N'2017-05-04 05:46:53.983' AS DateTime), CAST(N'2017-05-04 05:46:53.983' AS DateTime), N'PN', N'64')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (170, N'Poland', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'PL', N'48')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (171, N'Portugal', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'PT', N'351')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (172, N'Puerto Rico', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'PR', N'1-787')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (173, N'Qatar', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'QA', N'974')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (174, N'Reunion', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'RE', N'262')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (175, N'Romania', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'RO', N'40')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (176, N'Russian Federation', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'RU', N'7')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (177, N'Rwanda', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'RW', N'250')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (178, N'Saint Kitts and Nevis', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'KN', N'1-869')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (179, N'Saint Lucia', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'LC', N'1-758')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (180, N'Saint Vincent and the Grenadines', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'VC', N'1-784')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (181, N'Samoa', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'WS', N'685')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (182, N'San Marino', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'SM', N'378')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (183, N'Sao Tome and Principe', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'ST', N'239')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (184, N'Saudi Arabia', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'SA', N'966')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (185, N'Senegal', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'SN', N'221')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (186, N'Seychelles', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'SC', N'248')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (187, N'Sierra Leone', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'SL', N'232')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (188, N'Singapore', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'SG', N'65')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (189, N'Slovakia (Slovak Republic)', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'SK', N'421')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (190, N'Slovenia', CAST(N'2017-05-04 05:46:53.987' AS DateTime), CAST(N'2017-05-04 05:46:53.987' AS DateTime), N'SI', N'386')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (191, N'Solomon Islands', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'SB', N'677')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (192, N'Somalia', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'SO', N'252')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (193, N'South Africa', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'ZA', N'27')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (194, N'South Georgia and the South Sandwich Islands', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'GS', N'500')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (195, N'Spain', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'ES', N'34')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (196, N'Sri Lanka', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'LK', N'94')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (197, N'Saint Helena, Ascension and Tristan da Cunha', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'SH', N'290')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (198, N'St. Pierre and Miquelon', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'PM', N'508')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (199, N'Sudan', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'SD', N'249')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (200, N'Suriname', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'SR', N'597')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (201, N'Svalbard and Jan Mayen Islands', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'SJ', N'47')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (202, N'Swaziland', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'SZ', N'268')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (203, N'Sweden', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'SE', N'46')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (204, N'Switzerland', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'CH', N'41')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (205, N'Syrian Arab Republic', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'SY', N'963')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (206, N'Taiwan', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TW', N'886')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (207, N'Tajikistan', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TJ', N'992')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (208, N'Tanzania, United Republic of', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TZ', N'255')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (209, N'Thailand', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TH', N'66')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (210, N'Togo', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TG', N'228')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (211, N'Tokelau', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TK', N'690')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (212, N'Tonga', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TO', N'676')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (213, N'Trinidad and Tobago', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TT', N'1-868')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (214, N'Tunisia', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TN', N'216')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (215, N'Turkey', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TR', N'90')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (216, N'Turkmenistan', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TM', N'993')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (217, N'Turks and Caicos Islands', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TC', N'1-649')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (218, N'Tuvalu', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'TV', N'688')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (219, N'Uganda', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'UG', N'256')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (220, N'Ukraine', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'UA', N'380')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (221, N'United Arab Emirates', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'AE', N'971')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (222, N'United Kingdom', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'GB', N'44')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (223, N'United States', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'US', N'1')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (224, N'United States Minor Outlying Islands', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'UM', N'246')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (225, N'Uruguay', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'UY', N'598')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (226, N'Uzbekistan', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'UZ', N'998')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (227, N'Vanuatu', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'VU', N'678')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (228, N'Vatican City State (Holy See)', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'VA', N'379')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (229, N'Venezuela', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'VE', N'58')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (230, N'Vietnam', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'VN', N'84')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (231, N'Virgin Islands (British)', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'VG', N'1-284')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (232, N'Virgin Islands (U.S.)', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'VI', N'1-340')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (233, N'Wallis and Futuna Islands', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'WF', N'681')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (234, N'Western Sahara', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'EH', N'212')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (235, N'Yemen', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'YE', N'967')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (236, N'Serbia', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'RS', N'381')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (238, N'Zambia', CAST(N'2017-05-04 05:46:53.990' AS DateTime), CAST(N'2017-05-04 05:46:53.990' AS DateTime), N'ZM', N'260')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (239, N'Zimbabwe', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'ZW', N'263')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (240, N'Aaland Islands', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'AX', N'358')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (241, N'Palestine', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'PS', N'970')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (242, N'Montenegro', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'ME', N'382')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (243, N'Guernsey', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'GG', N'44-1481')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (244, N'Isle of Man', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'IM', N'44-1624')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (245, N'Jersey', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'JE', N'44-1534')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (247, N'Curaçao', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'CW', N'599')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (248, N'Ivory Coast', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'CI', N'225')
GO
INSERT [dbo].[countries] ([Id], [name], [created], [modified], [iso_code], [isd_code]) VALUES (249, N'Kosovo', CAST(N'2017-05-04 05:46:53.993' AS DateTime), CAST(N'2017-05-04 05:46:53.993' AS DateTime), N'XK', N'383')
GO
SET IDENTITY_INSERT [dbo].[countries] OFF
GO
