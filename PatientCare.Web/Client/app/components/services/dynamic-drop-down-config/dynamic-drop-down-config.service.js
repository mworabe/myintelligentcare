﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("DynamicDropDownConfigService", ["$resource", function ($resource) {
    var resource = $resource("api/DropDownConfigs/:id/:controller",
        null, //parameters default
        {
            'update': { method: "PUT" },
            'query': { method: "GET", params: {}, isArray: false }
        });

    resource.getTableOption = {
        cols: function () {
            return [
                { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "name", title: "Name", sortable: "name", show: true, dataType: "text", required: true },
                { field: "action", title: "action", dataType: "command", show: true},
            ];
        },
    };
    resource.reloadCommand = {};
    return resource;
}
]);
