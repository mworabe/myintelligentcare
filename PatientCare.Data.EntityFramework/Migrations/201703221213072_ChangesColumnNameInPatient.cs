namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesColumnNameInPatient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "has_mobile_access", c => c.Boolean());
            DropColumn("dbo.patients", "is_mobile_access");
        }
        
        public override void Down()
        {
            AddColumn("dbo.patients", "is_mobile_access", c => c.Boolean());
            DropColumn("dbo.patients", "has_mobile_access");
        }
    }
}
