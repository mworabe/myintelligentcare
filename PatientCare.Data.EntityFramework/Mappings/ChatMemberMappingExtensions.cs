﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ChatMemberMappingExtensions
    {
        public static DbModelBuilder MapChatMember(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ChatMember>().ToTable("chat_members").HasKey(i => i.Id);

            modelBuilder.Entity<ChatMember>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ChatMember>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ChatMember>()
                .Property(p => p.UserId)
                .HasColumnName("user_id")
                .IsOptional();

            modelBuilder.Entity<ChatMember>()
                 .Property(p => p.ChatSessionId)
                 .HasColumnName("chat_session_id")
                 .IsOptional();

            modelBuilder.Entity<ChatMember>()
                .HasOptional<ChatSession>(p => p.ChatSession)
                .WithMany()
                .HasForeignKey(p => p.ChatSessionId);

            return modelBuilder;
        }
    }
}
