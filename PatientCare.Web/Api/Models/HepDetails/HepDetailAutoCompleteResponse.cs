﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.HepFrequencies;
using PatientCare.Web.Api.Models.MasterTemplateExercises;
using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.HepDetails
{
    [DataContract]
    public class HepDetailAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ExerciseId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Exercise { get; set; }

        [DataMember]
        public long? GroupId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Group { get; set; }

        [DataMember]
        public long? HepId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Hep { get; set; }

        [DataMember]
        public int? Sets { get; set; }

        [DataMember]
        public int? Reps { get; set; }

        [DataMember]
        public int? Time { get; set; }

        [DataMember]
        public TimeEnum? TimeUnit { get; set; }

        [DataMember]
        public decimal? Weight { get; set; }

        [DataMember]
        public string WeightUnit { get; set; }

        [DataMember]
        public int? Holds { get; set; }

        [DataMember]
        public TimeEnum? HoldsUnit { get; set; }

        [DataMember]
        public int? ExerciseOrder { get; set; }

        [DataMember]
        public bool? IsActiveFrequency { get; set; }

        [DataMember]
        public HepFrequencyResponse[] HepFrequencies { get; set; }
    }
}