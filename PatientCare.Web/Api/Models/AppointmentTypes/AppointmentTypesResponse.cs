﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.AppointmentTypes
{
    [DataContract]
    public class AppointmentTypesResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Color { get; set; }
    }
}