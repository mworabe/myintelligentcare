﻿using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Exercise.
    /// </summary>
    public class Exercise : NamedEntity
    {

        public long? set { get; set; }

        public long? reps { get; set; }

        public long? holds { get; set; }

        public long? weight { get; set; }

        public string weightUnit { get; set; }

        public int time { get; set; }

        public string timeUnit { get; set; }

        public string holdUnit { get; set; }

        public string frequency { get; set; }

        public string resistance { get; set; }

        public string Keywords { get; set; }

        public string Comments { get; set; }

        public bool? IsDeleted { get; set; }

        public virtual ICollection<ExerciseMedia> Medias { get; set; }

        public virtual ICollection<Exercise_ExerciseActivity> exercises { get; set; }

        public virtual ICollection<Exercise_injury> injuries { get; set; }
    }
}
