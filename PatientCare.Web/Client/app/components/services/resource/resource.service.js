﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("Resource", [
        "$resource", "$state", "$injector", "$uibModal", "$filter", "$translate",
        function ($resource, $state, $injector, $uibModal, $filter, $translate) {
            var permissionWorker = null;

            function getPermissionWorker() {
                if (!permissionWorker) {
                    permissionWorker = $injector.get("PermissionWorker");
                }
                return permissionWorker;
            };


            var resource = $resource("api/Resources/:id/:controller",
                null, //parameters default
                {
                    'update': { method: "PUT" },
                    'query': { method: "GET", params: {}, isArray: false },
                    'getTimeZones': { method: "GET", params: { controller: "GetTimeZones" }, isArray: true },
                    'setInactive': { method: "POST", params: { controller: "set-inactive" }, isArray: false },
                    'updateBulk': { method: "POST", params: { controller: "update-bulk" }, isArray: false },
                    'exportResume': { method: "POST", params: { controller: "export_resume" }, isArray: false },
                    'getOrganizationalHierarchy': { method: "GET", params: { controller: "GetChild" }, isArray: false },
                    'skillAutoComplete': { method: "GET", params: { controller: "skill-autocomplete" }, isArray: true },
                    'getConflictResource': { method: "GET", params: { controller: "conflict-resources" }, isArray: true },
                    "saveConflictResource": { method: "post", params: { controller: "conflict-resources" }, isArray: true },
                    'getGeoNamesForZipCode': { method: "GET", params: { controller: "get-geo-names-for-zipcode" }, isArray: false },
                });


            resource.avatarSrc = "api/Resources/UploadPhoto";
            resource.uploadAvatar = function (uploader, file, resourceId, cb) {
                cb = cb || angular.noop;
                var url = this.avatarSrc + "?id=" + resourceId;
                uploader.upload({
                    url: url,
                    data: { file: file }
                }).then(cb);
            };

            resource.cvSrc = "api/Resources/UploadCv";
            resource.uploadCV = function (uploader, file, resourceId, cb) {
                cb = cb || angular.noop;
                var url = this.cvSrc + "?id=" + resourceId;
                uploader.upload({
                    url: url,
                    data: { file: file }
                }).then(cb);
            };

            resource.fileSrc = "api/Resources/UploadFile";
            resource.uploadFile = function (uploader, file) {
                var url = this.fileSrc;
                return uploader.upload({
                    url: url,
                    data: { file: file }
                });
            };

            resource.uploadResources = function (uploader, file, cb) {
                cb = cb || angular.noop;
                var url = "api/Resources/import";
                uploader.upload({
                    url: url,
                    data: { file: file }
                }).then(cb);
            };

            resource.reloadCommand = {};

            function getNameToEntity(entity) {

                if (!entity)
                    return "";

                return entity.name;
            }

            var resourceCheckboxInactive = false;

            resource.getTableOption = {
                formatDelete: function (row) {
                    return row.lastName + " " + row.firstName;
                },
                disabledAddButton: function () {
                    return !getPermissionWorker().canResourceInsert();
                },
                disabledDeleteButton: function () {
                    return getPermissionWorker().canResourceDelete();
                },
                disabledEditButton: function () {
                    return getPermissionWorker().canResourceUpdate();
                },
                getResourceCheckboxInactive: function () {
                    return resourceCheckboxInactive;
                },
                reset: function () {
                    resourceCheckboxInactive = false;
                },
                cols: function () {
                    return [
                        {
                            field: "name",
                            title: "Name",
                            sortable: "Name",
                            image: function (row) {
                                return row.photoFileNameUrl || "Client/assets/images/userIco.png";
                            },
                            show: true,
                            dataTypeView: "href",
                            dataType: "text",
                            customViewText: function (row) {
                                return row.lastName + ", " + row.firstName;
                            },
                            buildUrl: function (row) {
                                return $state.href("resourceEdit", { id: row.id });
                            }
                        }, {
                            field: "employeeId",
                            title: $filter('translate')('Employee ID'),
                            sortable: "employeeId",
                            show: true,
                            dataType: "text"
                        }, {
                            field: "jobTitleName",
                            title: "Job Title",
                            sortable: "jobTitleName",
                            show: true,
                            dataType: "text",
                        }, {
                            field: "departmentName",
                            title: $filter('translate')('Department'),
                            sortable: "departmentName",
                            show: true,
                            dataType: "text",
                        }, {
                            field: "clinicLocationName",
                            title: "Location",
                            sortable: "clinicLocationName",
                            show: true,
                            dataType: "text"
                        }, {
                            field: "active",
                            title: "Status",
                            sortable: "active",
                            show: true,
                            dataType: "text",
                            customViewText: function (row) {
                                return (row.active) ? "Active" : "Inactive";
                            },
                        }, {
                            field: "yearsEmployed",
                            title: "Years Employed",
                            sortable: "yearsEmployed",
                            show: true,
                            dataType: "text"
                        },  /*{
                            field: "divisionName",
                            title: $filter('translate')('Division'),
                            sortable: "divisionName",
                            show: true,
                            dataType: "text",
                        }, {
                            field: "countryName",
                            title: "Country",
                            sortable: "countryName",
                            show: true,
                            dataType: "text",
                        }, /* {
                            field: "status",
                            title: "Status",
                            sortable: "active",
                            show: true,
                            dataType: "boolean",
                            dataTypeView: "boolean",
                            customViewText: function (row) {
                                return (row.active ? "Active" : "Inactive");
                            }
                        },*/ {
                            field: "action",
                            title: "Action",
                            class: "",
                            dataType: "command"
                        }
                    ];
                },

                onAdd_Click: function () {
                    $state.go("resourceEdit");
                },
                onEdit_Click: function (row) {
                    $state.go("resourceEdit", { id: row.id });
                },
                pageName: "Resources",
                menuButtons: [
                    {
                        click: function (row) {
                            resource.setInactive({ id: row.id }, {}, function () {
                                resource.reloadCommand.reload();
                            });
                        },

                        iconClass: "icon icon-user-times"
                    }
                ],
                headerButtons: [{
                    text: "Availability vs Demand",
                    click: function () {
                        $state.go("rolesAvailabilityVsDemand");
                    },
                    iconClass: "availability"
                }, ],
                headerLeftItems: []
            };
            return resource;
        }
    ]);