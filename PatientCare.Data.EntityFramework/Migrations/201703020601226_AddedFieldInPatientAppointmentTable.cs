namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldInPatientAppointmentTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patient_appointments", "is_mobile", c => c.Boolean());
            AddColumn("dbo.patient_appointments", "is_reschedule", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.patient_appointments", "is_reschedule");
            DropColumn("dbo.patient_appointments", "is_mobile");
        }
    }
}
