
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("item", {
                url: "/item",
                templateUrl: "Client/app/controllers/sample/item-list.html",
                controller: "itemCntrl",
                data: {
                    requireLogin: true
                }

            }).state("item.detail", {
                url: "/item-detail",
                templateUrl: "Client/app/controllers/sample/item-detail.html",
                controller: "itemDetailCntrl",
                data: {
                    requireLogin: true
                }

            })



    });