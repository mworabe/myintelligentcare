"use strict";

(function ngMoreThanDateTimeIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("ngMoreThanDateTime", ngMoreThanDateTime);

    function ngMoreThanDateTime() {
        return {
            restrict: "A",
            require: "ngModel",
            link: function ($scope, $element, $attrs, ngModel) {

                if (!$attrs.ngMoreThanDateTime || $attrs.moreThanDateTimeField === "false")
                    return;
                var newValue = null;

                $scope.$watch($attrs.ngMoreThanDateTime, function (newVal) {
                    if (newVal)
                        newValue = moment(newVal);
                    setValidation(ngModel.$modelValue, newValue);
                });


                function compareDate(currValue, moreValue) {
                    return moreValue.isSame(currValue, "day") || moreValue.isBefore(currValue);
                }

                function setValidation(currValue, lessValue) {
                    if (!currValue || !lessValue || (currValue.isValid && !currValue.isValid()) || (lessValue.isValid && !lessValue.isValid()))
                        return;
                    ngModel.$setValidity("moreThenDateTime", compareDate(ngModel.$modelValue, newValue));
                }
                $scope.$watch(function () {
                    return ngModel.$modelValue;
                }, function (newVal) {
                    if (newVal) {
                        setValidation(ngModel.$modelValue, newValue);
                    } else ngModel.$setValidity("moreThenDateTime", true);
                });

            }
        };
    }
})();