﻿"use strict";
var app = angular.module("EDZoutstaffingPortalApp");
app.directive("workExperience", ["uuid2", "$uibModal", "Modal", "toaster", "DropdownConfigsFunction", "DynamicDropDownConfigService", "CONS_BUILD_FOR",
    function (uuid2, $uibModal, modal, toaster, dropdownConfigsFunction, dynamicDropDownConfigService, CONS_BUILD_FOR) {
        return {
            templateUrl: "Client/app/components/directives/WorkExperienceDataList/WorkExperienceView.html",
            restrict: "E",
            replace: false,
            scope: {
                skillList: "=",
                model: "=ngModel",
                option: "=ngOption",
                viewMode: "=?ngViewMode",
                addButtonText: "=",
                resumeMode: "=?ngResumeMode",
                controllerName: "=?ngCtrl"
            },
            link: function (scope) {
                scope.IS_BUILD_FOR_STINSON = CONS_BUILD_FOR.isForStinson;

                dropdownConfigsFunction(scope, "industrySearch", dynamicDropDownConfigService, "industry");


                scope.angucomleteStorePrefix = scope.angucomleteStorePrefix || "___storeFieldSelectValue";
                scope.option = scope.option || {};
                scope.viewMode = (typeof scope.viewMode === "undefined" || scope.viewMode === null) ? false : scope.viewMode;


                scope.option.transformModelPrePush = scope.option.transformModelPrePush || angular.noop;
                scope.addNew = function () {
                    if (!scope.model)
                        scope.model = [];
                    var item = {};
                    scope.option.transformModelPrePush(item);
                    scope.model.push(item);
                };

                scope.getColspanValue = function getColspanValue() {
                    return (scope.IS_BUILD_FOR_STINSON) ? 4 : 2;
                }

                scope.angucompleteSelect = function (select) {
                    var field = this.$parent.$parent.field;
                    var fieldName = this.$parent.$parent.field.fieldName;
                    var item = this.$parent.$parent.$parent.item;
                    if (select && select.originalObject && select.originalObject.id > 0) {
                        item.jobTitleId = select.originalObject.id;
                        item[scope.angucomleteStorePrefix + fieldName] = select.originalObject;
                        if (field.angucompleteSelector) {
                            field.angucompleteSelector(item, select.originalObject);
                        }
                    } else item[fieldName] = null;
                }

                scope.onIndustrySelected = function (selected) {
                    var index = (this.inputName).split("_");
                    if (index.length > 0) {
                        var item = this.$parent.$parent.$parent.item;
                        if (item) {
                            item.resourceWorkAssignments[index[1]].assignmentIndustryId = (selected && selected.originalObject && selected.originalObject.id > 0) ? selected.originalObject.id : 0;
                            item.resourceWorkAssignments[index[1]].assignmentIndustry = (selected && selected.originalObject) ? selected.originalObject : {};
                        }
                    }
                }

                scope.addNewAssignment = function (i, index) {

                    if (scope.model && !scope.model[index].resourceWorkAssignments)
                        scope.model[index].resourceWorkAssignments = [];

                    var item = {};
                    item.resourceWorkExperienceId = i;
                    item.isAssignmentOpen = true;

                    scope.model[index].isAssignmentOpen = true;
                    $("#collapsibleRow_" + index).addClass('in');
                    scope.model[index].resourceWorkAssignments.push(item);

                };

                scope.addNewFromPopUp = function () {
                    var taskFilter = {
                        fromDate: this.$parent.parentModel.startDate,
                        toDate: this.$parent.parentModel.dueDate
                    };
                    scope.editTaskModal = $uibModal.open({
                        animation: true,
                        backdrop: "static",
                        templateUrl: "Client/app/controllers/rolesAvailabilityVsDemand/rolesAvailabilityVsDemandPopUp.html",
                        controller: "RolesAvailabilityVsDemandPopUpCtrl as RolesAvailabilityVsDemandCtrl",
                        resolve: {
                            data: function () {
                                return {
                                    taskFilter: taskFilter
                                }
                            }
                        }
                    });
                    scope.editTaskModal.result.then(function (parameters) {

                        if (scope.savePopupCallback)
                            scope.savePopupCallback(parameters);
                    });
                }
                scope.dateFormat = function (value, format) {
                    if (!value)
                        return undefined;
                    if (angular.isString(value)) {
                        return moment(value).format(format || "MM/DD/YYYY");
                    }
                    return value.format(format || "MM/DD/YYYY");
                }

                scope.delete = function (field, item) {
                    var index = scope.model.indexOf(item);
                    if (index >= 0)
                        scope.model.splice(index, 1);
                }

                scope.deleteAssignment = function (parentIndex, item, childIndex) {
                    var index = scope.model[parentIndex].resourceWorkAssignments.indexOf(item);
                    if (index >= 0)
                        scope.model[parentIndex].resourceWorkAssignments.splice(index, 1);


                    if (scope.model[parentIndex].resourceWorkAssignments.length == 0) {
                        scope.model[parentIndex].isAssignmentOpen = false;
                    }


                }

                scope.deleteItem = function (item, arr) {
                    var languageItemIndex = arr.indexOf(item);
                    arr.splice(languageItemIndex, 1);
                }
                scope.generateId = uuid2.newguid;
                scope.isFunction = angular.isFunction;

                scope.workExperiencePositionList = ["Manager", "HR", "Gen. Manager", "Team Leader"];

            }
        };
    }]);