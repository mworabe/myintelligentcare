﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Medical Designation Enum.
    /// </summary>
    public enum MedicalDesignationEnum
    {
        MD = 0,
        GP = 1,
        OT = 2,
        Psych = 3,
        ENT = 4,
        Gyn = 5,
        OBGYN = 6,
        Physio = 7,
        Other = 8
    }
}
