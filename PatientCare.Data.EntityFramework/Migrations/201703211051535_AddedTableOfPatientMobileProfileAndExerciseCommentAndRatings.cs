namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedTableOfPatientMobileProfileAndExerciseCommentAndRatings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.app_functional_ratings",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    patient_id = c.Long(),
                    patient_case_id = c.Long(),
                    functional_rating = c.Int(),
                    comment = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.patient_cases", t => t.patient_case_id)
                .Index(t => t.patient_id)
                .Index(t => t.patient_case_id);

            CreateTable(
                "dbo.exercise_comments",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    exercise_id = c.Long(),
                    hep_id = c.Long(),
                    patient_id = c.Long(),
                    patient_case_id = c.Long(),
                    comment = c.String(),
                    comment_date = c.DateTime(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.exercises", t => t.exercise_id)
                .ForeignKey("dbo.hep_master", t => t.hep_id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.patient_cases", t => t.patient_case_id)
                .Index(t => t.exercise_id)
                .Index(t => t.hep_id)
                .Index(t => t.patient_id)
                .Index(t => t.patient_case_id);

            CreateTable(
                "dbo.exercise_ratings",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    exercise_id = c.Long(),
                    hep_id = c.Long(),
                    patient_id = c.Long(),
                    patient_case_id = c.Long(),
                    rating = c.Int(),
                    rating_date = c.DateTime(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.exercises", t => t.exercise_id)
                .ForeignKey("dbo.hep_master", t => t.hep_id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.patient_cases", t => t.patient_case_id)
                .Index(t => t.exercise_id)
                .Index(t => t.hep_id)
                .Index(t => t.patient_id)
                .Index(t => t.patient_case_id);

            CreateTable(
                "dbo.patient_mobile_profiles",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    full_name = c.String(),
                    email = c.String(),
                    phone = c.String(),
                    photo = c.String(),
                    patient_id = c.Long(),
                    language_id = c.Long(),
                    city_id = c.Long(),
                    state_id = c.Long(),
                    country_id = c.Long(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.city_id)
                .ForeignKey("dbo.countries", t => t.country_id)
                .ForeignKey("dbo.languages", t => t.language_id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.States", t => t.state_id)
                .Index(t => t.patient_id)
                .Index(t => t.language_id)
                .Index(t => t.city_id)
                .Index(t => t.state_id)
                .Index(t => t.country_id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.patient_mobile_profiles", "state_id", "dbo.States");
            DropForeignKey("dbo.patient_mobile_profiles", "patient_id", "dbo.patients");
            DropForeignKey("dbo.patient_mobile_profiles", "language_id", "dbo.languages");
            DropForeignKey("dbo.patient_mobile_profiles", "country_id", "dbo.countries");
            DropForeignKey("dbo.patient_mobile_profiles", "city_id", "dbo.Cities");
            DropForeignKey("dbo.exercise_ratings", "patient_case_id", "dbo.patient_cases");
            DropForeignKey("dbo.exercise_ratings", "patient_id", "dbo.patients");
            DropForeignKey("dbo.exercise_ratings", "hep_id", "dbo.hep_master");
            DropForeignKey("dbo.exercise_ratings", "exercise_id", "dbo.exercises");
            DropForeignKey("dbo.exercise_comments", "patient_case_id", "dbo.patient_cases");
            DropForeignKey("dbo.exercise_comments", "patient_id", "dbo.patients");
            DropForeignKey("dbo.exercise_comments", "hep_id", "dbo.hep_master");
            DropForeignKey("dbo.exercise_comments", "exercise_id", "dbo.exercises");
            DropForeignKey("dbo.app_functional_ratings", "patient_case_id", "dbo.patient_cases");
            DropForeignKey("dbo.app_functional_ratings", "patient_id", "dbo.patients");
            DropIndex("dbo.patient_mobile_profiles", new[] { "country_id" });
            DropIndex("dbo.patient_mobile_profiles", new[] { "state_id" });
            DropIndex("dbo.patient_mobile_profiles", new[] { "city_id" });
            DropIndex("dbo.patient_mobile_profiles", new[] { "language_id" });
            DropIndex("dbo.patient_mobile_profiles", new[] { "patient_id" });
            DropIndex("dbo.exercise_ratings", new[] { "patient_case_id" });
            DropIndex("dbo.exercise_ratings", new[] { "patient_id" });
            DropIndex("dbo.exercise_ratings", new[] { "hep_id" });
            DropIndex("dbo.exercise_ratings", new[] { "exercise_id" });
            DropIndex("dbo.exercise_comments", new[] { "patient_case_id" });
            DropIndex("dbo.exercise_comments", new[] { "patient_id" });
            DropIndex("dbo.exercise_comments", new[] { "hep_id" });
            DropIndex("dbo.exercise_comments", new[] { "exercise_id" });
            DropIndex("dbo.app_functional_ratings", new[] { "patient_case_id" });
            DropIndex("dbo.app_functional_ratings", new[] { "patient_id" });
            DropTable("dbo.patient_mobile_profiles");
            DropTable("dbo.exercise_ratings");
            DropTable("dbo.exercise_comments");
            DropTable("dbo.app_functional_ratings");
        }
    }
}
