﻿using System;
using System.Reflection;
using Autofac;
using Quartz;
using Quartz.Impl;

namespace PatientCare.Web.Services.IoC
{
    internal static class JobRegistrationExtensions
    {
        public static ContainerBuilder RegisterJob(this ContainerBuilder containerBuilder)
        {
            if (containerBuilder == null) { throw new ArgumentNullException("containerBuilder"); }

            // Schedule
            containerBuilder.Register(x => new StdSchedulerFactory().GetScheduler()).As<IScheduler>();

            // Schedule jobs
            containerBuilder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(x => typeof(IJob).IsAssignableFrom(x));

            return containerBuilder;
        }
    }
}