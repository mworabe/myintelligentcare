"use strict";
angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("care-plan-add", {
                url: "/care-pan-add/:patientId/:caseId",
                templateUrl: "Client/app/controllers/health-care-plan/health-care-plan.html",
                controller: "CarePlanAddEditCntrl as CarePlanCntrl",
                resolve: {
                    dataObject: function () { return {}; }
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            }).state("care-plan-edit", {
                url: "/care-pan-edit/:id",
                templateUrl: "Client/app/controllers/health-care-plan/health-care-plan.html",
                controller: "CarePlanAddEditCntrl as CarePlanCntrl",
                resolve: {
                    dataObject: ['HepService', '$stateParams', '$q', function (HepService, $stateParams, $q) {
                        function loadHep() {
                            var defer = $q.defer();
                            if ($stateParams.id) {
                                HepService.get({ id: $stateParams.id }).$promise.then(function (response) {
                                    defer.resolve(response);
                                }, function (reason) {
                                    defer.resolve({
                                        errorCode: reason.status
                                    });
                                });
                            } else {
                                defer.resolve(response);
                            }
                            
                            
                            return defer.promise;
                        }

                        return $q.all([loadHep()])
                            .then(function (resolutions) {
                                return resolutions;
                            });
                    }]
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });