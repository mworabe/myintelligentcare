@echo off

rem call "%~dp0run-nunit" "..\EDZoutstaffingportal.Tests\bin\%~1\EDZoutstaffingportal.Tests.dll" || goto :error
call "C:\Program Files (x86)\NUnit 2.6.4\bin\nunit-console.exe" /xml:Results.xml ..\EDZoutstaffingportal.Tests\bin\den\EDZoutstaffingportal.Tests.dll || goto :error

goto :done

:error
echo Server test failed with error %errorlevel%.

:done
exit /b %errorlevel%
