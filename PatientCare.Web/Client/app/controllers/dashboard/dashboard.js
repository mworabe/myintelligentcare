// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright � 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("dashboard", {
                url: "/dashboard",
                templateUrl: "Client/app/controllers/dashboard/dashboard.html",
                controller: "DashboardCtrl as DashboardCtrl",
                data: {
                    requireLogin: true
                },
                resolve: {
                    dashboard: ['DashboardResource', function (DashboardResource) {
                        return DashboardResource.getDashboard();
                    }]
                }
            })
            .state("dashboard-list", {
                url: "/dashboard/list",
                templateUrl: "Client/app/controllers/dashboard/dashboard-list.html",
                controller: "DashboardListCtrl as DashboardListCtrl",
                data: {
                    requireLogin: true
                },
                resolve: {
                    dashboard: ['DashboardResource', function (DashboardResource) {
                        return DashboardResource.getDashboardList();
                    }]
                }
            })
            .state("dashboard-edit", {
                url: "/dashboard/edit/:id",
                templateUrl: "Client/app/controllers/dashboard/dashboard.html",
                controller: "DashboardCtrl as DashboardCtrl",
                data: {
                    requireLogin: true
                },
                resolve: {
                    dashboard: ['DashboardResource', '$stateParams', function (DashboardResource, $stateParams) {
                        if ($stateParams) {
                            return DashboardResource.getDashboard($stateParams.id);
                        }
                        return {};
                    }]
                }
            });
    });