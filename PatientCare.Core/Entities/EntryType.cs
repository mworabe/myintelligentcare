﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Entity Type.
    /// </summary>
    public class EntryType : NamedEntity
    {
        public EntryPeriodEnum? EntryPeriod { get; set; }

        public string Days { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long? AdvancedCreatePeriod { get; set; }

        public DateTime? LastEntryDateCreated { get; set; }
    }
}
