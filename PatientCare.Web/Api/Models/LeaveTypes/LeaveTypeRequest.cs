﻿using System;

namespace PatientCare.Web.Api.Models.LeaveTypes
{
    public class LeaveTypeRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string IsPaid { get; set; }

        public int TotalLeaves { get; set; }

        public string Color { get; set; }

    }
}