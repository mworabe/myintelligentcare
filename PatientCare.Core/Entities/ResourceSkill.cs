﻿using System;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Skill.
    /// </summary>
    public class ResourceSkill: Entity
    {
        public long ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        public long SkillId { get; set; }
        public virtual Skill Skill { get; set; }
        public int YearsOfExpirience { get; set; }
        public double Proficiency { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string Description { get; set; }
    }
}
