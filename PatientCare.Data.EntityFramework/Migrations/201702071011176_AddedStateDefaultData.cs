﻿namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedStateDefaultData : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                    -- setting to null resource states fields
                    update r set state_id = null
                    from resources r
                    where state_id is not null
                    -- deleting old states values
                    Delete from states
                    -- adding new values to states
                   SET IDENTITY_INSERT [dbo].[states] ON 
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3919, N'Alabama')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3920, N'Alaska')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3921, N'Arizona')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3922, N'Arkansas')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3923, N'Byram')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3924, N'California')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3925, N'Cokato')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3926, N'Colorado')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3927, N'Connecticut')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3928, N'Delaware')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3929, N'District of Columbia')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3930, N'Florida')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3931, N'Georgia')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3932, N'Hawaii')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3933, N'Idaho')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3934, N'Illinois')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3935, N'Indiana')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3936, N'Iowa')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3937, N'Kansas')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3938, N'Kentucky')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3939, N'Louisiana')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3940, N'Lowa')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3941, N'Maine')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3942, N'Maryland')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3943, N'Massachusetts')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3944, N'Medfield')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3945, N'Michigan')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3946, N'Minnesota')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3947, N'Mississippi')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3948, N'Missouri')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3949, N'Montana')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3950, N'Nebraska')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3951, N'Nevada')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3952, N'New Hampshire')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3953, N'New Jersey')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3954, N'New Jersy')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3955, N'New Mexico')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3956, N'New York')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3957, N'North Carolina')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3958, N'North Dakota')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3959, N'Ohio')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3960, N'Oklahoma')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3961, N'Ontario')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3962, N'Oren')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3963, N'Pennsylvania')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3964, N'Ramey')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3965, N'Rhode Island')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3966, N'South Carolina')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3967, N'South Dakota')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3968, N'Sublimity')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3969, N'Tennessee')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3970, N'Texas')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3971, N'Trimble')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3972, N'Utah')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3973, N'Vermont')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3974, N'Virginia')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3975, N'Washington')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3976, N'West Virginia')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3977, N'Wisconsin')
                        INSERT [dbo].[states] ([Id], [name]) VALUES (3978, N'Wyoming')
                        SET IDENTITY_INSERT [dbo].[states] OFF
               ");
        }

        public override void Down()
        {
        }
    }
}
