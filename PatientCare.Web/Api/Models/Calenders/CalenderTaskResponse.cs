﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Calenders
{
    [DataContract]
    public class CalenderTaskResponse
    {
        [DataMember]
        public string CalenderType { get; set; }

        [DataMember]
        public string TaskName  { get; set; }

        [DataMember]
        public string EventType { get; set; }

        [DataMember]
        public long? Id { get; set; }//TaskId

        [DataMember]
        public long? ResourceId { get; set; }
        
        [DataMember]
        public string ResourceName { get; set; }

        [DataMember]
        public long? SupervisorId { get; set; }

        [DataMember]
        public string SupervisorName { get; set; }

        [DataMember]
        public string Title { get; set; }//Task Name

        [DataMember]
        public DateTime? Start { get; set; }

        [DataMember]
        public DateTime? End { get; set; }

        [DataMember]
        public decimal? EstimentTime { get; set; }
        
        [DataMember]
        public string Priority { get; set; }

        [DataMember]
        public long? ProjectId { get; set; }

        [DataMember]
        public string  ProjectName { get; set; }

        [DataMember]
        public DateTime? ProjectStartDate { get; set; }

        [DataMember]
        public DateTime? ProjectEndDate { get; set; }

        [DataMember]
        public string ProjectStatus { get; set; }
        
        [DataMember]
        public long? ProrjectDepartmentId { get; set; }

        [DataMember]
        public string ProrjectDepartmentName { get; set; }

        [DataMember]
        public long? ProrjectDivisionId { get; set; }

        [DataMember]
        public string ProrjectDivisionName { get; set; }

        [DataMember]
        public string TaskStatus { get; set; }

        [DataMember]
        public decimal? TimeSpent { get; set; }

        [DataMember]
        public string ClassName { get; set; }

        [DataMember]
        public bool? AllDay { get; set; }
    }
}