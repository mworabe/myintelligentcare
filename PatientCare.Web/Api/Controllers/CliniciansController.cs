﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.InjuryRegions;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.AutoCompletes;
using System.Collections.Generic;
using PatientCare.Data.EntityFramework;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Clinicians operations.
    /// </summary>
    [Authorize]
    public sealed class CliniciansController : ApiBaseController
    {
        /// <summary>
        /// Description : Clinicians Field IResourceRepository.
        /// </summary>
        private readonly IResourceRepository _resourceRepository;

        /// <summary>
        /// Description : Clinicians Field IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        /// Description : Initializes a new instance of the Clinicians Class.  
        /// </summary>
        /// <param name="resourceRepository">Resource Repository</param>
        /// <param name="dataContextScopeFactory">Data Context Scope Factory</param>
        public CliniciansController
            (
                IResourceRepository resourceRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _resourceRepository = resourceRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Clinicians/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Clinician List.   
        /// </summary>
        /// <param name="autoCompleteRequest">Auto Complete Request</param>
        /// <returns>List Type Picklist Default Response Model</returns>
        [HttpGet]
        [Route("api/Clinicians/autocomplete")]
        [ResponseType(typeof(PicklistDefaultResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]AutoCompleteRequest autoCompleteRequest = null)
        {
            var filter = new FilterQuery<Resource>();
            if (!string.IsNullOrWhiteSpace(autoCompleteRequest.Search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(autoCompleteRequest.Search.ToLower()));
            }
            var resultList = new List<Resource>();
            if (autoCompleteRequest.Type == "all")
            {
                List<PicklistDefaultResponse> appointments = new List<PicklistDefaultResponse>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    string query = "EXEC sp_clinician_autocomplete @Operation = 1,@Search = '" + autoCompleteRequest.Search + "',@DataCount =0";
                    appointments = await ctx.Database.SqlQuery<PicklistDefaultResponse>(query).ToListAsync();
                    return Ok(appointments.ToArray());
                }
            }
            else
            {
                List<PicklistDefaultResponse> appointments = new List<PicklistDefaultResponse>();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    string query = "EXEC sp_clinician_autocomplete  @Operation = 2,@Search = '" + autoCompleteRequest.Search + "',@DataCount = 20";
                    appointments = await ctx.Database.SqlQuery<PicklistDefaultResponse>(query).ToListAsync();
                    return Ok(appointments.ToArray());
                }
            }
        }


        [HttpGet]
        [Route("api/Clinicians/autocompletee")]
        [ResponseType(typeof(List<PicklistDefaultResponse>))]
        public async Task<IHttpActionResult> Autocompletee([FromUri] long search)
        {
            List<PicklistDefaultResponse> dd = new List<PicklistDefaultResponse>();
            List<PicklistDefaultResponse> respons = new List<PicklistDefaultResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC SP_ClinicianByLocation @Id =" + search;
                respons = await ctx.Database.SqlQuery<PicklistDefaultResponse>(query).ToListAsync();
            }
            return Ok(respons);
        }

    }
}
