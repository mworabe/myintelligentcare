﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceSurveyResults;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceSurveyResultMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceSurveyResultRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSurveyResult, ResourceSurveyResultResponse>(new ExtendedMapConfig<ResourceSurveyResult, ResourceSurveyResultResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSurveyResult[], ResourceSurveyResultResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSurveyResultRequest, ResourceSurveyResult>(new ExtendedMapConfig<ResourceSurveyResultRequest, ResourceSurveyResult>()));

            return mapperProvider;
        }
    }
}