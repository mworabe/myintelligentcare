﻿using System;
using System.Data;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using System.Data.Entity;

/// <summary>
/// Created By : Swayam Tech Lab (Mohit Solanki).
/// Created Date : Jun-23-2017.
/// </summary>
namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle chat operations And implementing all methods which is initialize in IChatService.
    /// </summary>
    public class ChatService : IChatService
    {
        /// <summary>
        ///Description : Chat Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Chat Service Field IChatSessionRepository
        /// </summary>
        private readonly IChatSessionRepository _chatSessionRepository;

        /// <summary>
        ///Description : Chat Service Field IChatKeyRepository
        /// </summary>
        private readonly IChatKeyRepository _chatKeyRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Chat class.
        /// </summary>
        /// <param name="dataContextScopeFactory">Data Context Scope Factory</param>
        /// <param name="chatSessionRepository">Chat Session Repository</param>
        /// <param name="chatKeyRepository">Chat Key Repository</param>
        public ChatService
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IChatSessionRepository chatSessionRepository,
                IChatKeyRepository chatKeyRepository

            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _chatSessionRepository = chatSessionRepository;
            _chatKeyRepository = chatKeyRepository
;
        }

        #region Chat Methods

        /// <summary>
        /// Method Description : Using for get Chat data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>ChatSession Entity</returns>
        public async Task<ChatSession> ReadChatAsync(long id)
        {
            var readOptions = new ReadOptions<ChatSession>().AsReadOnly();

            var clinic = await _chatSessionRepository.ReadAsync(id, readOptions);

            return clinic;

        }

        /// <summary>
        /// Method Description : Using for Delete Chat data.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteChatAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var patientappointment = _chatSessionRepository.Read(id);

                if (patientappointment != null)
                {
                    _chatSessionRepository.Delete(patientappointment);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Method Description : Using for Create/Update chat session data.
        /// </summary>
        /// <param name="chatsession">ChatSession Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> InsertUpdateChatAsync(ChatSession chatsession)
        {
            if (chatsession == null) throw new ArgumentNullException("patientWorkoutAnalytics");
            if (chatsession.Id == 0)
            {
                await _chatSessionRepository.CreateAsync(chatsession, UpdateOptions.Create<ChatSession>()
                    .WithNotModified(p => p.Created)
                    .WithNotModified(p => p.Modified));
            }
            else
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    ChatSessionChildren(ctx, chatsession);
                    var originPatientWorkoutAnalytic = _chatSessionRepository.Read(chatsession.Id, new ReadOptions<ChatSession>().AsReadOnly());
                    await _chatSessionRepository.UpdateAsync(chatsession, UpdateOptions.Create<ChatSession>()
                        .WithNotModified(p => p.Created));
                }
            }

            return true;
        }

        /// <summary>
        /// Method Description : Using for Update Chat Members.
        /// </summary>
        /// <param name="ctx">DbContext</param>
        /// <param name="chatsession">Chat Session Entity</param>
        private static void ChatSessionChildren(DbContext ctx, ChatSession chatsession)
        {
            var chatMembers = chatsession.ChatMembers;
            ctx.ChildrenProcessing(chatMembers, x => x.ChatSessionId == chatsession.Id);
        }

        #endregion

        #region Chat Key

        /// <summary>
        /// Method Description : Using for Create/Update chat key data.
        /// </summary>
        /// <param name="chatKey">ChatKey Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> InsertUpdateChatKeyAsync(ChatKey chatKey)
        {
            if (chatKey == null) throw new ArgumentNullException("patientWorkoutAnalytics");
            if (chatKey.Id == 0)
            {
                await _chatKeyRepository.CreateAsync(chatKey, UpdateOptions.Create<ChatKey>()
                    .WithNotModified(p => p.Created)
                    .WithNotModified(p => p.Modified));
            }
            else
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var originchatKey = _chatKeyRepository.Read(chatKey.Id, new ReadOptions<ChatKey>().AsReadOnly());
                    await _chatKeyRepository.UpdateAsync(chatKey, UpdateOptions.Create<ChatKey>()
                        .WithNotModified(p => p.Created));
                }
            }

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Chat key by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Chatkey Entity</returns>
        public async Task<ChatKey> ReadChatKeyAsync(long id)
        {
            var readOptions = new ReadOptions<ChatKey>().AsReadOnly();

            var chat = await _chatKeyRepository.ReadAsync(id, readOptions);

            return chat;

        }

        #endregion

    }
}
