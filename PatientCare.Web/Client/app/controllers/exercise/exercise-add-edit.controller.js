"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("ExerciseAddEditCtrl", ["$scope", '$stateParams', 'CreatorSearchFunction', 'ConstantFacory','InjuryRegionService', 'DYNAMIC_FORM_CONSTROLLER', 'SystemFunctions',
        "$rootScope", '$state', 'ExerciseActivitieService', 'ExercisePositionService', 'ExerciseService', 'toaster', 'dataObject', "DropdownConfigsFunction", "DynamicDropDownConfigService",
        function ($scope, $stateParams, creatorSearchFunction, constantFactory, injuryRegionService, DYNAMIC_FORM_CONSTROLLER, systemFunctionsService,
            $rootScope, $state, exerciseActivitieService, exercisePositionService, exerciseService, toaster, dataObject, dropdownConfigsFunction, dynamicDropDownConfigService) {



            var self = this;
            self.selectedImage = null;
            self.selectedImageURL = null;
            self.selectedVideo = null;
            self.selectedVideoURL = null;
            self.tabs = [
                {
                    id: "photo",
                    title: "Photo",
                }, {
                    id: "video",
                    title: "Video"
                }
            ]
            self.selectedTab = self.tabs[0].id;
            self.myConstantTimeUnits = constantFactory.getTimeUnit();
            self.myConstantWeightUnits = constantFactory.getWeightUnit();
            self.myConstantFrequency = constantFactory.getFrequency();
            self.myResistance = constantFactory.getResistances();
            self.injuryRegionList = dataObject[0];
            self.activityList = dataObject[1];
            self.model = {
                medias: [],
                injuries:[],// [{ injury_Id: 1,  exercise_Id: null }],
                exercises: []// [{ activity_Id: 1, exercise_Id: null }, { activity_Id: 2, exercise_Id: null }]
            };
            var modelId = $stateParams.id || undefined;
            self.title = "Add Exercise";

            _.forEach(self.injuryRegionList, function (item) {
                item.isSelected = false;

            });

            _.forEach(self.activityList, function (item) {
                item.isSelected = false;

            });



            self.resistanceList = [{ name: "Red" }, { name: "Black" }, { name: "Yellow" }, { name: "Green" }, { name: "Blue" }];
            self.mySelectedResistance;
            self.myFun = function () {
                self.model.resistance = self.mySelectedResistance.name;
            }
            self.onClinicianSelected = function (item) {
                if (item.isSelected)
                {
                    self.model.exercises.push({ activity_Id: item.id, exercise_Id: null });
                }
            }
            self.onClinicianSelected1 = function (item) {
                if (item.isSelected) {
                    self.model.injuries.push({ injury_Id: item.id, exercise_Id: null });
                }
            }
            function findMedia(type) {
                if (self.model.medias) {
                    for (var i = 0; i < self.model.medias.length; i++) {
                        var obj = self.model.medias[i];
                        if (obj.mediaType === type) {
                            return i;
                        }
                    }
                }
                return -1;
            }


            if (modelId && dataObject) {
                self.model = dataObject[2];
                self.title = "Edit Exercise"
                if (self.model.medias && self.model.medias.length > 0) {
                    var imageIndex = findMedia("Image");
                    if (imageIndex > -1) {
                        self.selectedImageURL = self.model.medias[imageIndex].mediaURL;
                    }

                    var videoIndex = findMedia("Video");
                    if (videoIndex > -1) {
                        self.selectedVideoURL = self.model.medias[videoIndex].mediaURL;
                        self.selectedTab = self.tabs[1].id;
                    }
                }
                self.injuryRegionList = dataObject[1];
                self.activityList = dataObject[0];
                _.forEach(self.activityList, function (item) {

                    _.forEach(self.model.exercises, function (itm) {
                        if (_.isEqual(item.id, itm.activity_Id))
                        {
                            item.isSelected = true;

                        }
                    });

                });

                _.forEach(self.injuryRegionList, function (item) {
                   // item.isSelected = true;
                    _.forEach(self.model.injuries, function (itm) {
                        if (_.isEqual(item.id, itm.injury_Id)) {
                            item.isSelected = true;

                        }
                    });

                });
                self.model.exercises = [];
                self.model.injuries = [];
            }

            self.myExercises = [];
            self.myactivitySelections = function () {
                _.forEach(self.myExercises, function (item) {
                 self.model.exercises.push({activity_Id:item.id, exercise_Id:null});
                });
            }
            self.myRegions = [];
            self.myregionSelections = function () {
                _.forEach(self.myRegions, function (item) {                  
                        self.model.injuries.push({ injury_Id:null , exercise_Id:item.id });                                     
                });
            }

            self.isTabSelected = function (id) {
                if (id === self.selectedTab) return true;
                return false;
            }
            self.setSelectedTab = function (id) {
                self.selectedTab = id;
            }

            self.autocompleteMinLength = 0;
            self.onImageDrop = function (file) {
                if (file && file.length > 0) {
                    self.selectedImage = file[0];
                    self.selectedImageURL = null;
                }
            }

            self.onVideoDrop = function (file) {
                if (file && file.length > 0) {
                    self.selectedVideo = file[0];
                    self.selectedVideoURL = null;
                }
            }

            function validateForm(form) { }

            function preSaveChanges() {
                var request = self.model;
                return request;
            }

            function uploadImage() {
                self.isLoading = exerciseService.uploadMedia(self.selectedImage).then(
                   function (resp) {
                       if (resp && resp.data.fileName) {
                           if (!self.model.medias) {
                               self.model.medias = [];
                           }
                           var index = findMedia("Image");
                           if (index > -1) {
                               var obj = self.model.medias[index];
                               obj.originalName = self.selectedImage.name;
                               obj.name = resp.data.fileName;
                           } else {
                               self.model.medias.push({
                                   exerciseId: self.model.id,
                                   originalName: self.selectedImage.name,
                                   mediaType: "Image",
                                   name: resp.data.fileName
                               });
                           }

                           if (self.selectedVideo) {
                               uploadVideo();
                           } else {
                               saveChanges();
                           }
                       }
                   },
                   function (error) { },
                   function (prg) { });
            }

            function uploadVideo() {

                self.isLoading = exerciseService.uploadMedia(self.selectedVideo).then(
                    function (resp) {
                        if (resp && resp.data.fileName) {
                            if (!self.model.medias) {
                                self.model.medias = [];
                            }
                            var index = findMedia("Video");
                            if (index > -1) {
                                var obj = self.model.medias[index];
                                obj.originalName = self.selectedVideo.name;
                                obj.name = resp.data.fileName;
                                obj.ThumbImageName = resp.data.thumbFileName;
                            } else {
                                self.model.medias.push({
                                    exerciseId: self.model.id,
                                    originalName: self.selectedVideo.name,
                                    mediaType: "Video",
                                    name: resp.data.fileName,
                                    ThumbImageName: resp.data.thumbFileName,
                                });
                            }
                            saveChanges();
                        }
                    },
                function (error) { },
                function (prg) { });
            }

            self.onSuccess = function (result) {
                if (result && result.id > 0) {
                    toaster.pop("success", "", "Exercise Saved Successfully.");
                    $state.go("master-templates");
                }
            }


            function saveChanges() {

                if (modelId) {
                    self.isLoading = exerciseService.update({ id: modelId }, preSaveChanges(), self.onSuccess, function (error) {
                    })
                } else {
                    self.isLoading = exerciseService.save(preSaveChanges(), self.onSuccess, function (error) {
                    })
                }
            }


            self.onFormSubmit = function onFormSubmit(form) {
                if (form.$invalid) {
                    toaster.pop("error", "", "Invalid Data, Please Check Error Summary and Fix them.");
                    return;
                }
                validateForm(form);

                if (self.selectedImage) {
                    uploadImage();
                } else if (self.selectedVideo) {
                    uploadVideo();
                } else {
                    saveChanges();
                }
            }

            self.generalPoropertyList = self.formFields = [
                 {
                     id: "name",
                     name: "Name",
                     container: "details",
                     position: "1",
                     pattern: ".{0,255}",
                     required: true,
                     visible: true,
                     type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                }
            ];





            self.onCancelButtonClick = function onCancelButtonClick() {
                systemFunctionsService.historyBackButtonClick($rootScope, $state, $scope);
                window.history.back();
            }


        }
    ]);

