"use strict";

(function edzAddCustomFieldFactoryIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzAddCustomFieldFactory", ["edzAlphaCustomField", "edzAlphanumericCustomField", "edzNumericCustomField", "edzSelectCustomField", "edzDateCustomField",
            "edzCheckboxCustomField", "edzBigtextCustomField", "edzFileCustomField", "edzTimeCustomField", "weekTimeCustomField", "EDZ_CUSTOM_FIELD_TYPES",
            edzAddCustomFieldFactory
        ]);

    function edzAddCustomFieldFactory(alphaCustomField, alphanumericCustomField, numericCustomField, selectCustomField, dateCustomField, checkboxCustomField, bigtextCustomField,
        fileCustomField, timeCustomField, weekTimeCustomField, EDZ_CUSTOM_FIELD_TYPES) {
        return getNewField;

        function getNewField(fieldType) {
            switch(fieldType) {
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_SELECT.name:
                    return new selectCustomField();
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_CHECKBOX.name:
                    return new checkboxCustomField();
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_DATE.name:
                    return new dateCustomField();
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_ALPHA.name:
                    return new alphaCustomField();
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_ALPHA_NUMERIC.name:
                    return new alphanumericCustomField();
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_NUMERIC.name:
                    return new numericCustomField();
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_BIGTEXT.name:
                    return new bigtextCustomField();
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_FILE.name:
                    return new fileCustomField();
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_TIME.name:
                    return new timeCustomField();
                case EDZ_CUSTOM_FIELD_TYPES.TYPE_WEEKTIME.name:
                    return new weekTimeCustomField();
                default:
                    throw new Error('There is no field type with type ' + fieldType);
            }
        }
    }
})(); 
