﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.ClinicLocations;
using PatientCare.Web.Api.Models.Clinics;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.ReferralPhysicians;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PatientCases
{
    [DataContract]
    public class PatientCaseResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Patient { get; set; }

        [DataMember]
        public string PatientNumber { get; set; }

        [DataMember]
        public string CaseNo { get; set; }

        [DataMember]
        public long? ClinicId { get; set; }

        [DataMember]
        public ClinicAutoCompleteResponse Clinic { get; set; }

        [DataMember]
        public long? ClinicLocationId { get; set; }

        [DataMember]
        public ClinicLocationAutoCompleteResponse ClinicLocation { get; set; }

        [DataMember]
        public CaseStatusEnum? CaseStatus { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }
        
        [DataMember]
        public int? Age { get; set; }

        [DataMember]
        public long? InjuryRegionId { get; set; }

        [DataMember]
        public PicklistDefaultResponse InjuryRegion { get; set; }

        [DataMember]
        public long? AssignedPhysicianId { get; set; }

        [DataMember]
        public PicklistDefaultResponse AssignedPhysician { get; set; }

        [DataMember]
        public long? ReferingPhysicianId { get; set; }

        [DataMember]
        public ReferralPhysicianAutoCompleteResponse ReferingPhysician { get; set; }

        [DataMember]
        public DateTime? ReturnTo { get; set; }

        public AuthorizeationRequiredEnum? AuthorizeationRequired { get; set; }

        [DataMember]
        public string AuthorizeationNumber { get; set; }

        [DataMember]
        public RelatedCauseEnum? RelatedCause { get; set; }

        [DataMember]
        public long? PrimayDiagnosisId { get; set; }

        [DataMember]
        public PicklistDefaultResponse PrimayDiagnosis { get; set; }

        [DataMember]
        public long? SecondaryDiagnosisId { get; set; }

        [DataMember]
        public PicklistDefaultResponse SecondaryDiagnosis { get; set; }

        [DataMember]
        public string WebPTCaseId { get; set; }

        [DataMember]
        public bool? IsDeleted { get; set; }
    }
}