﻿using System;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle exercise operations And implementing all methods which is initialize in IExerciseService.
    /// </summary>
    public class ExerciseService : IExerciseService
    {
        /// <summary>
        ///Description : Exercise Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Exercise Service Field IExerciseRepository
        /// </summary>
        private readonly IExerciseRepository _exerciseRepository;

        /// <summary>
        ///Description : Exercise Service Field IMasterTemplateRepository
        /// </summary>
        private readonly IMasterTemplateRepository _masterTemplateRepository;

        /// <summary>
        ///Description : Exercise Service Field IMasterTemplateExerciseRepository
        /// </summary>
        private readonly IMasterTemplateExerciseRepository _masterTemplateExerciseRepository;

        /// <summary>
        ///Description :Exercise Service Field IExerciseMediaRepository
        /// </summary>
        private readonly IExerciseMediaRepository _exerciseMediaRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Exercise class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="exerciseRepository">Exercise Repository</param>
        /// <param name="masterTemplateRepository">Master Template Repository</param>
        /// <param name="masterTemplateExerciseRepository">Master Template Exercise Repository</param>
        /// <param name="exerciseMediaRepository">Exercise Media Repository</param>
        public ExerciseService
            (
                IDataContextScopeFactory dataContextScopeFactory,
                IExerciseRepository exerciseRepository,
                IMasterTemplateRepository masterTemplateRepository,
                IMasterTemplateExerciseRepository masterTemplateExerciseRepository,
                IExerciseMediaRepository exerciseMediaRepository
            )
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _exerciseRepository = exerciseRepository;
            _masterTemplateRepository = masterTemplateRepository;
            _masterTemplateExerciseRepository = masterTemplateExerciseRepository;
            _exerciseMediaRepository = exerciseMediaRepository;
        }

        #region Exercise Methods

        /// <summary>
        /// Method Description : Using for Update Exercise data.
        /// </summary>
        /// <param name="exercise">Exercise Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateExerciseAsync(Exercise exercise)
        {
            if (exercise == null) throw new ArgumentNullException("Exercises");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                ChidlrenProcessingExercise(ctx, exercise);
                var originExercise = _exerciseRepository.Read(exercise.Id, new ReadOptions<Exercise>().AsReadOnly());

                await _exerciseRepository.UpdateAsync(exercise, UpdateOptions.Create<Exercise>()
                .WithNotModified(p => p.Created));
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new Exercise.
        /// </summary>
        /// <param name="exercise">Exercise Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateExerciseAsync(Exercise exercise)
        {
            if (exercise == null) throw new ArgumentNullException("Exercises");

            await _exerciseRepository.CreateAsync(exercise, UpdateOptions.Create<Exercise>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Exercise data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Exercise Entity</returns>
        public async Task<Exercise> ReadExerciseAsync(long id)
        {
            var readOptions = new ReadOptions<Exercise>().AsReadOnly();

            var Exercise = await _exerciseRepository.ReadAsync(id, readOptions);

            return Exercise;
        }

        //public async Task<bool> DeleteExerciseAsync(long id)
        //{
        //    using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
        //    {
        //        var exercise = _exerciseRepository.Read(id);

        //        if (exercise != null)
        //        {
        //            _exerciseRepository.Delete(exercise);

        //            await dbContextScope.SaveChangesAsync();
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public async Task<string> DeleteExerciseAsync(Exercise exercise)
        //{
        //    var allowDeleting = true;
        //    var exerciseId = exercise.Id;
        //    var sb = new StringBuilder(string.Format("You can't delete {0} because Child Table Exists :", exercise.Name));

        //    #region Check In Master Template

        //    var exercisemastertemplateitem = (await _masterTemplateExerciseRepository.ListAsync(
        //               new FilterQuery<MasterTemplateExercise>().AddFilter(x => x.ExerciseId == exercise.Id),
        //               new ReadOptions<MasterTemplateExercise>().AsReadOnly()))
        //               .ToList();

        //    if (exercisemastertemplateitem.Any())
        //    {
        //        allowDeleting = false;
        //    }
        //    else
        //    {
        //        var exercisemediaitem = await _exerciseMediaRepository.ListAsync(new FilterQuery<ExerciseMedia>().AddFilter(x => x.ExerciseId == exercise.Id));
        //        DeletetestChildItemExercise(exercisemediaitem);
        //        allowDeleting = true;
        //    }

        //    #endregion

        //    //var exercisemastertemplateitem = await _masterTemplateExerciseRepository.ListAsync(new FilterQuery<MasterTemplateExercise>().AddFilter(x => x.ExerciseId == exercise.Id));
        //    //DeletetestChildItemMasterTemplate(exercisemastertemplateitem);

        //    if (allowDeleting)
        //    {
        //        await _exerciseRepository.DeleteAsync(exercise);
        //        return "";
        //    }

        //    sb.Replace(Environment.NewLine, "<br/>");

        //    return sb.ToString();

        //}

        /// <summary>
        /// Method Description : Using for Delete Exercise data.
        /// </summary>
        /// <param name="exercise">Exercise Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteExerciseAsync(Exercise exercise)
        {
            // here we are not delete Exercise but we are keep entry is deleted.
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                exercise.IsDeleted = true;

                var originPatient = _exerciseRepository.Read(exercise.Id, new ReadOptions<Exercise>().AsReadOnly());

                await _exerciseRepository.UpdateAsync(exercise, UpdateOptions.Create<Exercise>()
                    .WithNotModified(p => p.Created));

                return true;
            }
        }

        /// <summary>
        /// Method Description : Using for Delete Exercise Media.
        /// </summary>
        /// <param name="exerciseMediaItem">Exercise Media Item</param>
        /// <returns>Returns True/Flase</returns>
        private bool DeletetestChildItemExercise(IEnumerable<ExerciseMedia> exerciseMediaItem)
        {
            foreach (var exerciseMediasItem in exerciseMediaItem)
            {
                _exerciseMediaRepository.Delete(exerciseMediasItem);
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Delete Exercise Master Template.
        /// </summary>
        /// <param name="exerciseMasterTemplateItem">Exercise Master Template Item</param>
        /// <returns>Returns True/Flase</returns>
        private bool DeletetestChildItemMasterTemplate(IEnumerable<MasterTemplateExercise> exerciseMasterTemplateItem)
        {
            foreach (var exerciseMasterTemplatesItem in exerciseMasterTemplateItem)
            {
                _masterTemplateExerciseRepository.Delete(exerciseMasterTemplatesItem);
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Update Exercise Media.
        /// </summary>
        /// <param name="ctx">DbContext</param>
        /// <param name="exercise">Exercise Entity</param>
        private static void ChidlrenProcessingExercise(DbContext ctx, Exercise exercise)
        {
            var exerciseMedias = exercise.Medias;
            ctx.ChildrenProcessing(exerciseMedias, x => x.ExerciseId == exercise.Id);
        }

        #endregion

        #region Master Template Methods

        /// <summary>
        /// Method Description : Using for Update Master Template data.
        /// </summary>
        /// <param name="masterTemplate">Master Template Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateMasterTemplateAsync(MasterTemplate masterTemplate)
        {
            if (masterTemplate == null) throw new ArgumentNullException("masterTemplate");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                ChidlrenProcessingMasterTemplate(ctx, masterTemplate);
                var originmasterTemplate = _masterTemplateRepository.Read(masterTemplate.Id, new ReadOptions<MasterTemplate>().AsReadOnly());

                await _masterTemplateRepository.UpdateAsync(masterTemplate, UpdateOptions.Create<MasterTemplate>()
                .WithNotModified(p => p.Created));
            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new Master Template data.
        /// </summary>
        /// <param name="masterTemplate">Master Template Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateMasterTemplateAsync(MasterTemplate masterTemplate)
        {
            if (masterTemplate == null) throw new ArgumentNullException("masterTemplate");

            await _masterTemplateRepository.CreateAsync(masterTemplate, UpdateOptions.Create<MasterTemplate>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for get Master Template data by id.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Master Template Entity</returns>
        public async Task<MasterTemplate> ReadMasterTemplateAsync(long id)
        {
            var readOptions = new ReadOptions<MasterTemplate>().AsReadOnly();

            var masterTemplate = await _masterTemplateRepository.ReadAsync(id, readOptions);

            return masterTemplate;
        }

        //public async Task<string> DeleteMasterTemplateAsync(MasterTemplate masterTemplate)
        //{
        //    var allowDeleting = true;
        //    var resourceId = masterTemplate.Id;
        //    var sb = new StringBuilder(string.Format("You can't delete {0} because :", masterTemplate.Name));

        //    var masterTemplateExerciseItem = await _masterTemplateExerciseRepository.ListAsync(new FilterQuery<MasterTemplateExercise>().AddFilter(x => x.MasterTemplateId == masterTemplate.Id));
        //    DeletetestChildItem(masterTemplateExerciseItem);

        //    if (allowDeleting)
        //    {
        //        await _masterTemplateRepository.DeleteAsync(masterTemplate);
        //        return "";
        //    }

        //    sb.Replace(Environment.NewLine, "<br/>");

        //    return sb.ToString();

        //}

        /// <summary>
        /// Method Description : Using for Delete Master Template data.
        /// </summary>
        /// <param name="masterTemplate">Master Template Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteMasterTemplateAsync(MasterTemplate masterTemplate)
        {
            // here we are not delete Master Template but we are keep entry is deleted.

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                masterTemplate.IsDeleted = true;

                var originPatient = _masterTemplateRepository.Read(masterTemplate.Id, new ReadOptions<MasterTemplate>().AsReadOnly());

                await _masterTemplateRepository.UpdateAsync(masterTemplate, UpdateOptions.Create<MasterTemplate>()
                    .WithNotModified(p => p.Created));

                return true;
            }

        }

        /// <summary>
        /// Method Description : Using for Delete Master Template Exercise.
        /// </summary>
        /// <param name="masterTemplateExerciseItem">Master Template Exercise Item</param>
        /// <returns>Returns True/Flase</returns>
        private bool DeletetestChildItem(IEnumerable<MasterTemplateExercise> masterTemplateExerciseItem)
        {
            foreach (var masterTemplateExercisesItem in masterTemplateExerciseItem)
            {
                _masterTemplateExerciseRepository.Delete(masterTemplateExercisesItem);
            }
            return true;
        }

        //public async Task<bool> DeleteMasterTemplateAsync(long id)
        //{
        //    using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
        //    {
        //        var masterTemplate = _masterTemplateRepository.Read(id);

        //        if (masterTemplate != null)
        //        {
        //            _masterTemplateRepository.Delete(masterTemplate);

        //            await dbContextScope.SaveChangesAsync();
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        #endregion

        /// <summary>
        /// Method Description : Using for Update Master Template Exercise.
        /// </summary>
        /// <param name="ctx">DbContext</param>
        /// <param name="masterTemplate">Master Template Entity</param>
        private static void ChidlrenProcessingMasterTemplate(DbContext ctx, MasterTemplate masterTemplate)
        {
            var masterTemplateExercises = masterTemplate.TemplateExercises;
            ctx.ChildrenProcessing(masterTemplateExercises, x => x.MasterTemplateId == masterTemplate.Id);
        }

    }
}
