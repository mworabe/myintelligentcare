﻿using PatientCare.Web.Api.Models.HepDetails;
using PatientCare.Web.Api.Models.HepGroups;
using PatientCare.Web.ApiMobile.Models.HepDetails;
using PatientCare.Web.ApiMobile.Models.HepGroups;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.HepMasters
{
    [DataContract]
    public class HepMasterGroupMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        //[DataMember]
        //public PatientAutoCompleteResponse Patient { get; set; }

        [DataMember]
        public long? OwnerId { get; set; }

        //[DataMember]
        //public PicklistDefaultResponse Owner { get; set; }

        [DataMember]
        public long? PatientCaseId { get; set; }

        //[DataMember]
        //public PatientCaseAutoCompleteResponse PatientCase { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public DateTime? Modified { get; set; }

        [DataMember]
        public bool? IsSaveAsDraft { get; set; }

        [DataMember]
        public HepDetailsMobileResponse[] HepDetails { get; set; }

        [DataMember]
        public HepGroupsMobileResponse[] HepGroups { get; set; }
        
    }
}