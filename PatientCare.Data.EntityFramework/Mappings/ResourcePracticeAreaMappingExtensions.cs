﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourcePracticeAreaMappingExtensions
    {
        public static DbModelBuilder MapResourcePracticeArea(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourcePracticeArea>().ToTable("resource_practice_areas").HasKey(i => i.Id);

            modelBuilder.Entity<ResourcePracticeArea>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourcePracticeArea>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourcePracticeArea>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<ResourcePracticeArea>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourcePracticeArea>()
                .Property(p => p.PracticeAreaId)
                .HasColumnName("practice_area_id")
                .IsOptional();

            modelBuilder.Entity<ResourcePracticeArea>()
                .HasOptional<DropDownConfig>(p => p.PracticeArea)
                .WithMany()
                .HasForeignKey(p => p.PracticeAreaId);

            return modelBuilder;
        }
    }
}
