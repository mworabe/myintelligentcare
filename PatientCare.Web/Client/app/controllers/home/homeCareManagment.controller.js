'use strict';

angular.module('EDZoutstaffingPortalApp')
  .controller('HomeCareManagmentCtrl', ['AppointmentService',
    function (appointmentService) {
        


        var self = this;
        this.get = appointmentService.getTodaysAppointment;
        this.cols = appointmentService.getTableOption;
        this.delete = appointmentService.delete;
        this.cols.hideBulkEdit = true;

        this.processData = function (data) {
            if (data.data) {
                _.forEach(data.data, function (item) {
                    item.id = item.appointmentId;
                });
            }
        }

        this._onResponceTransform = function (params) {
            var today = moment().toDate();
            params.from = today;
            params.to = today;

        }

    }]);
