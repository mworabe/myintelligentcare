﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq.Expressions;

namespace PatientCare.Data.AbstractRepository
{
    public class PagedQuery<T>
    {
        protected readonly List<Expression<Func<T, object>>> OrderByExpressionsInternal = new List<Expression<Func<T, object>>>();
        protected readonly FilterQuery<T> Filter = FilterQuery.Create<T>();

        public int ItemsToGet { get; set; }

        public int ItemsToSkip { get; set; }

        public SortOrder SortOrder { get; set; }

        public Expression<Func<T, object>>[] OrderByExpressions { get { return OrderByExpressionsInternal.ToArray(); } }

        public Expression<Func<T, bool>>[] FilterExpressions { get { return Filter.FilterExpressions; } }

        public PagedQuery<T> AddOrderBy(Expression<Func<T, object>> expression)
        {
            if (expression == null) { throw new ArgumentNullException("expression"); }
            OrderByExpressionsInternal.Add(expression);
            return this;
        }

        public PagedQuery<T> AddFilter(Expression<Func<T, bool>> expression)
        {
            Filter.AddFilter(expression);
            return this;
        }

        public PagedQuery<T> AddOrdersBy(IEnumerable<Expression<Func<T, object>>> expressions)
        {
            if (expressions == null) { throw new ArgumentNullException("expressions"); }
            OrderByExpressionsInternal.AddRange(expressions);
            return this;
        }

        public PagedQuery<T> AddFilters(IEnumerable<Expression<Func<T, bool>>> expressions)
        {
            Filter.AddFilters(expressions);
            return this;
        }
    }
}
