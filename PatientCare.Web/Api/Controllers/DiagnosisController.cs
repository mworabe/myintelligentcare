﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Diagnosiss;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Diagnosis operations.
    /// </summary>
    [Authorize]
    public sealed class DiagnosisController : ApiBaseController
    {
        /// <summary>
        /// Description : Diagnosis Field IConfigurationDataService.
        /// </summary>
        private readonly IConfigurationDataService _configurationDataService;
        /// <summary>
        /// Description : Diagnosis Field IDiagnosisRepository.
        /// </summary>
        private readonly IDiagnosisRepository _diagnosisRepository;
        /// <summary>
        /// Description : Diagnosis Field IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<Diagnosis> _readOptions = new ReadOptions<Diagnosis>();

        /// <summary>
        /// Description : Initializes a new instance of the Diagnosis Class.
        /// </summary>
        /// <param name="configurationDataService">IConfiguration Data Service</param>
        /// <param name="diagnosisRepository">IDiagnosis Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public DiagnosisController
            (
                IConfigurationDataService configurationDataService,
                IDiagnosisRepository diagnosisRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _configurationDataService = configurationDataService;
            _diagnosisRepository = diagnosisRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Diagnosis/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Diagnosis.    
        /// </summary>
        /// <param name="request">Diagnosis List Request Model</param>
        /// <returns>Diagnosis Response Model</returns>
        [HttpGet]
        [Route("api/Diagnosis")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<DiagnosisResponse>))]
        public IHttpActionResult GetAllDiagnosis([FromUri]DiagnosisListRequest request)
        {
            var query = PagingExtensions<Diagnosis>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            var readOptions = new ReadOptions<Diagnosis>().AsReadOnly();
            var page = _diagnosisRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<Diagnosis[], DiagnosisResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<DiagnosisResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Diagnosis.    
        /// </summary>
        /// <param name="DiagnosisRequest">Diagnosis Request Model</param>
        /// <returns>Diagnosis Response Model</returns>
        [HttpPost]
        [Route("api/Diagnosis")]
        [SwaggerResponse(201, "Created", typeof(DiagnosisResponse))]
        public async Task<IHttpActionResult> CreateDiagnosis([FromBody]DiagnosisRequest DiagnosisRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_diagnosisRepository.Exists(FilterQuery.Create<Diagnosis>().AddFilter(p => p.Name == DiagnosisRequest.Name)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<Diagnosis>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var Diagnosis = Mapper.Map<DiagnosisRequest, Diagnosis>(DiagnosisRequest);
            await _configurationDataService.CreateDiagnosisAsync(Diagnosis);

            return Created(GetEntityLocation(Diagnosis.Id), Mapper.Map<Diagnosis, DiagnosisResponse>(Diagnosis));
        }

        /// <summary>
        /// Method Description : Using for Update Diagnosis.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="DiagnosisRequest">Diagnosis Request Model</param>
        /// <returns>Diagnosis Response Model</returns>
        [HttpPut]
        [Route("api/Diagnosis/{id}")]
        public async Task<IHttpActionResult> EditDiagnosis(long id, [FromBody] DiagnosisRequest DiagnosisRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_diagnosisRepository.Exists(FilterQuery.Create<Diagnosis>().AddFilter(p => p.Name == DiagnosisRequest.Name && p.Id != DiagnosisRequest.Id)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<Diagnosis>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var Diagnosis = Mapper.Map<DiagnosisRequest, Diagnosis>(DiagnosisRequest);
            Diagnosis.Id = id;

            await _configurationDataService.UpdateDiagnosisAsync(Diagnosis);

            return Ok(Mapper.Map<Diagnosis, DiagnosisResponse>(Diagnosis));
        }

        /// <summary>
        /// Method Description : Using for Delete Diagnosis by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Diagnosis Response Model</returns>
        [HttpDelete]
        [Route("api/Diagnosis/{id}")]
        public async Task<IHttpActionResult> DeleteDiagnosisItem(long id)
        {
            var success = await _configurationDataService.DeleteDiagnosisAsync(id);
            if (!success)
            {
                return NotFound();
            }

            return Ok(new EntryType { Id = id });
        }

        /// <summary>
        /// Method Description : Using for Get Diagnosis by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Diagnosis Response Model</returns>
        [HttpGet]
        [Route("api/Diagnosis/{id}")]
        public async Task<IHttpActionResult> GetDiagnosis(long id)
        {
            var Diagnosis = await _configurationDataService.ReadDiagnosisAsync(id);
            if (Diagnosis == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<Diagnosis, DiagnosisResponse>(Diagnosis);
            return Ok(viewModel);
        }

        private static Expression<Func<Diagnosis, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return clininc => clininc.Name.Contains(searchPhrase);
        }

        private static Expression<Func<Diagnosis, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return Diagnosis => Diagnosis.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Diagnosis.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Diagnosis Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/Diagnosis/autocomplete")]
        [ResponseType(typeof(DiagnosisAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<Diagnosis>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
            }

            var autocompleteResult = await _diagnosisRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<Diagnosis[], DiagnosisAutoCompleteResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }
    }
}
