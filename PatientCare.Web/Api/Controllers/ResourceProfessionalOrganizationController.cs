﻿using System;
using System.Web.Http.Results;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.ResourceProfessionalOrganizations;
using PatientCare.Core.Entities;
using System.Web.Http;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Resource Professional Organization operations.
    /// </summary>
    [Authorize]
    public sealed class ResourceProfessionalOrganizationController : EdzBaseApiController<ResourceProfessionalOrganization, ResourceProfessionalOrganizationRequest, ResourceProfessionalOrganizationResponse>
    {
        /// <summary>
        /// Description : In this class we pass  Resource Professional Organization IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">IResource Professional Organization Repository</param>
        public ResourceProfessionalOrganizationController(IResourceProfessionalOrganizationRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.ResourceId);
            GetStatusCodeErrorFunc = GetStatusCodeError;
        }

        private StatusCodeResult GetStatusCodeError(Crud crud)
        {
            return GetStatusCodeError(crud, Section.Resources);
        }


        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/ResourceProfessionalOrganizations/" + id, UriKind.Relative);
        }
    }
}