﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Patient Case.
    /// </summary>
    public class PatientCase : Entity
    {
        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public string PatientNumber { get; set; }
        public string CaseNo { get; set; }

        public long? ClinicId { get; set; }
        public virtual Clinic Clinic { get; set; }

        public long? ClinicLocationId { get; set; }
        public virtual ClinicLocation ClinicLocation { get; set; }

        public CaseStatusEnum? CaseStatus { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? BirthDate { get; set; }

        public int? Age { get; set; }

        public long? InjuryRegionId { get; set; }
        public virtual InjuryRegion InjuryRegion { get; set; }

        public long? AssignedPhysicianId { get; set; }
        public virtual Resource AssignedPhysician { get; set; }

        public long? ReferingPhysicianId { get; set; }
        public virtual ReferralPhysician ReferingPhysician { get; set; }

        public DateTime? ReturnTo { get; set; }

        public AuthorizeationRequiredEnum? AuthorizeationRequired { get; set; }
        public string AuthorizeationNumber { get; set; }

        public RelatedCauseEnum? RelatedCause { get; set; }

        public long? PrimayDiagnosisId { get; set; }
        public virtual Diagnosis PrimayDiagnosis { get; set; }

        public long? SecondaryDiagnosisId { get; set; }
        public virtual Diagnosis SecondaryDiagnosis { get; set; }

        public string WebPTCaseId { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
