﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientCare.Web.Api.Models.Reports
{
    public class ResourceReportRequest
    {
        public DateTime? HireDateFrom { get; set; }
        public DateTime? HireDateTo { get; set; }
        public string Department { get; set; }
        public string Division { get; set; }
        public string JobTitle { get; set; }

        public long? DepartmentId { get; set; }
        public long? DivisionId { get; set; }
        public long? JobTitleId { get; set; }

        public int? Year { get; set; }
        public int? SelectedQuarter { get; set; }
        public int? SelectedMonth { get; set; }
    }
}