namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedColumnsINStateCityCountryPostalCodeTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cities", "state_id", c => c.Long());
            AddColumn("dbo.Cities", "state_code", c => c.String());
            AddColumn("dbo.Cities", "country_id", c => c.Long());

            AddColumn("dbo.countries", "iso_code", c => c.String());
            AddColumn("dbo.countries", "isd_code", c => c.String());

            AddColumn("dbo.states", "state_code", c => c.String());
            AddColumn("dbo.states", "country_id", c => c.Long());

            AddColumn("dbo.postal_codes", "state_id", c => c.Long());
            AddColumn("dbo.postal_codes", "state_code", c => c.String());
            AddColumn("dbo.postal_codes", "country_id", c => c.Long());
            AddColumn("dbo.postal_codes", "latitude", c => c.String());
            AddColumn("dbo.postal_codes", "longitude", c => c.String());

            AlterColumn("dbo.Cities", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.Cities", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

            AlterColumn("dbo.states", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.states", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

        }

        public override void Down()
        {
            AlterColumn("dbo.Cities", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.Cities", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

            AlterColumn("dbo.states", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.states", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

            DropColumn("dbo.postal_codes", "longitude");
            DropColumn("dbo.postal_codes", "latitude");
            DropColumn("dbo.postal_codes", "country_id");
            DropColumn("dbo.postal_codes", "state_code");
            DropColumn("dbo.postal_codes", "state_id");

            DropColumn("dbo.states", "country_id");
            DropColumn("dbo.states", "state_code");

            DropColumn("dbo.countries", "isd_code");
            DropColumn("dbo.countries", "iso_code");

            DropColumn("dbo.Cities", "country_id");
            DropColumn("dbo.Cities", "state_code");
            DropColumn("dbo.Cities", "state_id");
        }
    }
}
