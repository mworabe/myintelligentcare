﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ClinicMappingExtensions
    {
        public static DbModelBuilder MapClinic(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<Clinic>().ToTable("clinics").HasKey(i => i.Id);

            modelBuilder.Entity<Clinic>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<Clinic>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<Clinic>()
              .Property(p => p.Name)
              .HasColumnName("name")
              .IsOptional();

            modelBuilder.Entity<Clinic>()
              .Property(p => p.ContactPerson)
              .HasColumnName("contact_person")
              .IsOptional();
            
            modelBuilder.Entity<Clinic>()
              .Property(p => p.Address)
              .HasColumnName("address")
              .IsOptional();

            modelBuilder.Entity<Clinic>()
              .Property(p => p.Email)
              .HasColumnName("email")
              .IsOptional();

            modelBuilder.Entity<Clinic>()
              .Property(p => p.Phone)
              .HasColumnName("phone")
              .IsOptional();

            modelBuilder.Entity<Clinic>()
              .Property(p => p.ClinicPrefix)
              .HasColumnName("clinic_prefix")
              .IsOptional();

            modelBuilder.Entity<Clinic>()
             .Property(p => p.IsDeleted)
             .HasColumnName("is_deleted")
             .IsOptional();

            return modelBuilder;
        }
    }
}
