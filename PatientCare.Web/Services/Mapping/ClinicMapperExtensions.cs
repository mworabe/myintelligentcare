﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.Clinics;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ClinicMapperExtensions
    {
        public static EmitMapperMapperProvider ClinicRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Clinic, ClinicResponse>
                    (new ExtendedMapConfig<Clinic, ClinicResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Clinic[], ClinicResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Clinic, ClinicAutoCompleteResponse>
                    (new ExtendedMapConfig<Clinic, ClinicAutoCompleteResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Clinic[], ClinicAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ClinicRequest, Clinic>
                    (new ExtendedMapConfig<ClinicRequest, Clinic>()));

            return mapperProvider;
        }
    }
}