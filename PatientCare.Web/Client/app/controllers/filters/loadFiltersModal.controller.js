﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("LoadFiltersCtrl", ["$uibModalInstance", "data", "ResourceScoreFilter", "toaster",
        function ($uibModalInstance, data, resourceScoreFilter, toaster) {
            this.data = data;

            this.ok = function() {
                $uibModalInstance.close(data);
            }

            this.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            }

            this.deleteFilter = function () {
                var filter = data.selectedFilter;
                if (filter != null && filter.originalObject) {
                    resourceScoreFilter.delete({ id: filter.originalObject.id }, function () {
                        $uibModalInstance.dismiss('deleted');
                        toaster.pop("success", "", "delete  successful");
                    });
                }
            }
        }
    ]);