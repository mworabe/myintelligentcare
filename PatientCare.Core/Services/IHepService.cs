﻿
using PatientCare.Core.Entities;
using System.Threading.Tasks;

namespace PatientCare.Core.Services
{
    public interface IHepService
    {
        Task<bool> UpdateAsync(HepMaster hepMaster);
        Task<bool> CreateAsync(HepMaster hepMaster);
        Task<HepMaster> ReadAsync(long id);
        Task<HepMaster> ReadByCaseAsync(long id);
        Task<string> DeleteAsync(HepMaster hepMaster);
        Task<string> DeleteGroupAsync(HepGroup hepGroup);
        Task<string> DeleteDetailAsync(HepDetail hepDetail);

        Task<bool> InsertUpdateHepAsync(HepMaster hepMaster);
        Task<bool> DeletehepGroupAsync(long id);

        #region Hep Patient Plan

        Task<bool> InsertUpdateHepPatientPlanAsync(HepPatientsPlan hepPatientsPlan);
        Task<HepPatientsPlan> ReadHepPatientAsync(long id);

        #endregion
    }
}
