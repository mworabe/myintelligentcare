﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("appCustomPagination", [function () {
        return {
            templateUrl: "Client/app/components/directives/AppCustomPagination/app-custom-pagination-view.html",
            restrict: "E",
            controller: "AppCustomPaginationCtrl",
            controllerAs: "AppCustomPaginationCtrl",
            replace: false,
            scope: {
                totalItems: '=ngTotalItems',
                pageIndex: "=ngPageIndex",
                pageSize: "=ngPageSize",
                onPageChange: '=ngOnPageChange',
            },
            link: function (scope) { }
        };
    }]);
angular.module("EDZoutstaffingPortalApp").controller("AppCustomPaginationCtrl", ["$scope", AppCustomPaginationFunc]);

function AppCustomPaginationFunc(scope) {
    var self = this;
    var AppCustomPaginationCtrl = self;
    self.pagination = [];
    self.tempPageIndex = scope.pageIndex;
    self.onPageChange = function onPageChange(page) {
        self.tempPageIndex = page.number;
        scope.pageIndex = self.tempPageIndex;
        scope.onPageChange(page);
    }

        scope.onPageChange;


    function generatePagination() {
        var maxPage, minPage, numPages, maxBlocks, minBlocks, maxPivotPages;
        maxBlocks = (maxBlocks && maxBlocks < 6) ? 6 : maxBlocks;
        //var totalItems = scope.totalItems;
        var currentPage = scope.pageIndex;
        minBlocks = 1, maxBlocks = 5;

        if (!scope.pageSize)
            scope.pageSize = 20;

        numPages = Math.ceil(scope.totalItems / scope.pageSize);

        if (numPages > 1) {
            self.pagination.push({
                type: "prev",
                number: Math.max(1, currentPage - 1),
                active: currentPage > 1,
            });
            self.pagination.push({
                type: 'first',
                number: 1,
                active: currentPage > 1,
                current: currentPage === 1
            });
            maxPivotPages = Math.round((maxBlocks - minBlocks) / 2);
            minPage = Math.max(2, currentPage - maxPivotPages);
            maxPage = Math.min(numPages - 1, currentPage + maxPivotPages * 2 - (currentPage - minPage));
            minPage = Math.max(2, minPage - (maxPivotPages * 2 - (maxPage - minPage)));

            var i = minPage;
            while (i <= maxPage) {
                if ((i === minPage && i !== 2) || (i === maxPage && i !== numPages - 1)) {
                    self.pagination.push({
                        type: 'more',
                        active: false
                    });
                } else {
                    self.pagination.push({
                        type: 'page',
                        number: i,
                        active: currentPage !== i,
                        current: currentPage === i
                    });
                }
                i++;
            }
            self.pagination.push({
                type: 'last',
                number: numPages,
                active: currentPage !== numPages,
                current: currentPage === numPages
            });
            self.pagination.push({
                type: 'next',
                number: Math.min(numPages, currentPage + 1),
                active: currentPage < numPages
            });

        }
        //return pages;
        console.info(self.pagination);
    }

    generatePagination();
}
