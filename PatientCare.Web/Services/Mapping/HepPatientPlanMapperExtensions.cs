﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.HepPatientPlan;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class HepPatientPlanMapperExtensions
    {
        public static EmitMapperMapperProvider hepPatientPlanRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepPatientsPlan, HepPatientPlanResponse>
                    (new ExtendedMapConfig<HepPatientsPlan, HepPatientPlanResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepPatientsPlan[], HepPatientPlanResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<HepPatientPlanRequest, HepPatientsPlan>
                    (new ExtendedMapConfig<HepPatientPlanRequest, HepPatientsPlan>()));

            return mapperProvider;
        }
    }
}