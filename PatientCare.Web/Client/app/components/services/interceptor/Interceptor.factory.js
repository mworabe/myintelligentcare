﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
angular.module("EDZoutstaffingPortalApp")
    .factory("interceptor", [
        "$rootScope", "$q", "$cookieStore", "$location", "toaster", "$injector",
        function ($rootScope, $q, $cookieStore, $location, toaster, $injector) {
            var $state;

            function getState() {
                if (!$state)
                    $state = $injector.get('$state');
                return $state;
            }

            return {
                // Add authorization token to headers
                request: function(config) {
                    config.headers = config.headers || {};
                    if ($cookieStore.get("token")) {
                        config.headers.uiauthorization = "Bearer " + $cookieStore.get("token");
                    }
                    return config;
                },
                // Intercept 401s and redirect you to login
                responseError: function(response) {
                    if (response.status === 401) {
                        $location.path("/login");
                        // remove any stale tokens
                        $cookieStore.remove("token");
                    }
                    if (response.status === 400) {
                        if (response.data && response.data.modelState) {
                            var keys = Object.keys(response.data.modelState);
                            var errorMessage = response.data.modelState[keys[0]];
                            if (errorMessage.length > 0)
                                errorMessage = errorMessage[0];
                            //for (var i = 0; i < keys.length; i++) {
                            //    errorMessage += response.data.modelState[keys[i]] + "<br />";
                            //}
                            toaster.pop("error", response.data.message, errorMessage, 5000, "trustedHtml");
                        }

                    }
                    if (response.status === 403) {
                        
                        toaster.pop("error", "", "Forbidden");
                        
                        getState().go("forbidden");

                    }
                    if (response.status === 500) {
                        console.error(response);
                    }
                    return $q.reject(response);
                }
            };
        }
    ]);