﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Appointment Status Enum.
    /// </summary>
    public enum AppointmentStatusEnum
    {
        Confirmed = 0,
        CheckedIn = 1,
        Inprogress = 2,
        Cancelled = 3,
        NoShow = 4,
        Complete = 5,
        Rescheduled = 6,
        Request = 7
    }
}
