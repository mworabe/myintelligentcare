﻿using PatientCare.Core.Enums;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Resources
{
    [DataContract]
    public class ResourceMainPicklistResponse
    {

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string EmployeeId { get; set; }

        [DataMember]
        public long? DivisionId { get; set; }

        [DataMember]
        public string DivisionName { get; set; }

        [DataMember]
        public long? DepartmentId { get; set; }

        [DataMember]
        public string DepartmentName { get; set; }

        [DataMember]
        public long? CountryId { get; set; }

        [DataMember]
        public string CountryName { get; set; }

        [DataMember]
        public long? JobTitleId { get; set; }

        [DataMember]
        public string JobTitleName { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public int? YearsEmployed { get; set; }

        [DataMember]
        public string PhotoFileName { get; set; }

        [DataMember]
        public string PhotoFileNameUrl { get; set; }

        [DataMember]
        public decimal HourlyRate { get; set; }

        [DataMember]
        public bool? Active { get; set; }

        [DataMember]
        public double? Rating { get; set; }

        [DataMember]
        public GenderEnum? Gender { get; set; }

        [DataMember]
        public long? StateId { get; set; }

        [DataMember]
        public long? CityId { get; set; }

        [DataMember]
        public long? LanguageId { get; set; }

        [DataMember]
        public long TotalDataCount { get; set; }

        [DataMember]
        public long? ClinicId { get; set; }
        [DataMember]
        public string ClinicName { get; set; }
        [DataMember]
        public string ClinicPrefix { get; set; }

        [DataMember]
        public long? ClinicLocationId { get; set; }
        [DataMember]
        public string ClinicLocationName { get; set; }
        [DataMember]
        public string LocationPrefix { get; set; }
    }

}