﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Patient Goal.
    /// </summary>
    public class PatientGoal : Entity
    {
        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public long? PatientCaseId { get; set; }
        public virtual PatientCase PatientCase { get; set; }

        public PatientGoalTypeEnum? GoalType { get; set; }
        public string GoalName { get; set; }

        public long? PatientMobileProfileId { get; set; }
        public virtual PatientMobileProfile PatientMobileProfile { get; set; }
    }
}
