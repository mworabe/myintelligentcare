namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedColumninHepDetailTableChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hep_detail", "hep_id", c => c.Long());
            CreateIndex("dbo.hep_detail", "hep_id");
            AddForeignKey("dbo.hep_detail", "hep_id", "dbo.hep_master", "Id");
            AddForeignKey("dbo.hep_exercise_frequency", "hep_detail_id", "dbo.hep_detail", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.hep_exercise_frequency", "hep_detail_id", "dbo.hep_detail");
            DropForeignKey("dbo.hep_detail", "hep_id", "dbo.hep_master");
            DropIndex("dbo.hep_detail", new[] { "hep_id" });
            DropColumn("dbo.hep_detail", "hep_id");
        }
    }
}
