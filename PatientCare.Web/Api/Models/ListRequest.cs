﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models
{
    [DataContract]
    public class ListRequest
    {
        [DataMember(Name = "pageIndex")]
        public int PageIndex { get; set; }
        [DataMember(Name = "pageSize")]
        public int PageSize { get; set; }
        [DataMember(Name = "sortOrder")]
        public SortOrders? SortOrder { get; set; }
        [DataMember(Name = "sortField")]
        public string SortField { get; set; }
        [DataMember(Name = "searchPhrase")]
        public string SearchPhrase { get; set; }
        [DataMember(Name = "searchField")]
        public string SearchField { get; set; }

        [DataMember(Name = "dateFrom")]
        public string DateFrom { get; set; }
        [DataMember(Name = "dateTo")]
        public string DateTo { get; set; }
        
        public bool showModelsForApproval { get; set; }
        public int approverId { get; set; }
       
        [DataMember(Name = "projectProtfolioIdList")]
        public string ProjectProtfolioIdList { get; set; }
    }

    public enum SortOrders
    {
        Ascending = 0,
        Descending = 1
    }
}