﻿using System;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Survey Result.
    /// </summary>
    public class ResourceSurveyResult : NamedEntity
    {
        public long ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        public int Score { get; set; }
        public DateTime SurveyDate { get; set; }
    }
}
