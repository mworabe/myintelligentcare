﻿
var app = angular.module('EDZoutstaffingPortalApp');
app.directive('appAutoComplete', function ($compile) {


    return {
        restrict: 'EA',
        scope: {
            apiUrl: '@apiUrl',
            controlId: '@controlId',
            initialValue: '=initialValue',
            dispFields: "@displayFields",
            dataContainer: "=data",
            configType: '=configType',
            appForm: '='

        },
        templateUrl: "Client/app/components/directives/finance-autocomplete/autocomplete.html",
        controller: ['$scope', '$http', function ($scope, $http) {

            
            //var defaultUrl = "api/FinancialPlanningProfitability/getConfigPickList?configType=1&search=something";
            var defaultUrl = "api/FinancialPlanningProfitability/getConfigPickList";
            if (!$scope.apiUrl) {
                $scope.apiUrl = defaultUrl;
            }


            $scope.searchAPI = function (userInputString, timeoutPromise) {
                
                var url = $scope.apiUrl + "?configType=" + $scope.configType + "&search=" + userInputString;
                return $http.get(url, {}, { timeout: timeoutPromise })
            }

            $scope.onSelection = function (object) {
                if (object && object.originalObject) {
                    $scope.dataContainer = object.originalObject.id;
                } else {
                    $scope.dataContainer = "";
                }
            }

        }]
        


    }

});

