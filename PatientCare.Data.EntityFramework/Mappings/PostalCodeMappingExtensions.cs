﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PostalCodeMappingExtensions
    {
        public static DbModelBuilder MapPostalCode(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<PostalCode>().ToTable("postal_codes").HasKey(i => i.Id);

            modelBuilder.Entity<PostalCode>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<PostalCode>()
                .Property(p => p.ZipCode)
                .HasColumnName("zip_code")
                .IsOptional();

            modelBuilder.Entity<PostalCode>()
               .Property(p => p.CityId)
               .HasColumnName("city_id")
               .IsOptional();

            modelBuilder.Entity<PostalCode>()
                .HasOptional<City>(p => p.City)
                .WithMany()
                .HasForeignKey(p => p.CityId);

            modelBuilder.Entity<PostalCode>()
                .Property(p => p.StateId)
                .HasColumnName("state_id")
                .IsOptional();

            modelBuilder.Entity<PostalCode>()
                .Property(p => p.StateCode)
                .HasColumnName("state_code")
                .IsOptional();

            modelBuilder.Entity<PostalCode>()
                .Property(p => p.CountryId)
                .HasColumnName("country_id")
                .IsOptional();

            modelBuilder.Entity<PostalCode>()
                .Property(p => p.Latitude)
                .HasColumnName("latitude")
                .IsOptional();

            modelBuilder.Entity<PostalCode>()
                .Property(p => p.Longitude)
                .HasColumnName("longitude")
                .IsOptional();

            return modelBuilder;
        }
    }
}