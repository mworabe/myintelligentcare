﻿
namespace PatientCare.Web.Api.Models.DropDownConfigs
{
    public class DropDownConfigRequest
    {
        public long Id { get; set; }
        public string Value { get; set; }
        public string Name { get; set; }
        public string DropDownType { get; set; }

        public string ConfigField1 { get; set; }
        public string ConfigField2 { get; set; }
        public string ConfigField3 { get; set; }
        public string ConfigField4 { get; set; }
        public string ConfigField5 { get; set; }

        public long? ActivityTypeId { get; set; }        
    }
}