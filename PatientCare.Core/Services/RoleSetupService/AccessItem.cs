﻿using System.Collections.Generic;

namespace PatientCare.Core.Services.RoleSetupService
{
    public class AccessItem<T> 
    {
        public AccessItem()
        {
            Actions = new List<Crud>();
        }
        public T Item { get; set; }
        public IList<Crud> Actions { get; set; }
    }
}
