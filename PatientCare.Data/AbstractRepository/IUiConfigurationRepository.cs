﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IUiConfigurationRepository : IRepository<UiConfiguration>
    {
    }
}
