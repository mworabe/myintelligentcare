﻿using System;
using System.Data.Entity;
using PatientCare.Core.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class SentEmailMappingExtensions
    {
        public static DbModelBuilder MapSentEmail(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

             modelBuilder.Entity<SentEmail>().ToTable("sentemail").HasKey(i => i.Id);

             modelBuilder.Entity<SentEmail>()
                 .Property(p => p.Created)
                 .HasColumnName("created")
                 .IsRequired()
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

             modelBuilder.Entity<SentEmail>()
                 .Property(p => p.SendedAt)
                 .IsOptional();

            return modelBuilder;
        }
    }
}
