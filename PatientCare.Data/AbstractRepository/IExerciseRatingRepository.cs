﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IExerciseRatingRepository : IRepository<ExerciseRating>
    {
    }
}
