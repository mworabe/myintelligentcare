﻿"use strict";

(function edzAlphanumericCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzAlphanumericCustomFieldEditOptions", [
            "edzCustomFieldEditHelper",
            "CUSTOM_FIELD",
            edzAlphanumericCustomFieldOptions
        ]);

    function edzAlphanumericCustomFieldOptions(edzCustomFieldEditHelper, CUSTOM_FIELD) {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/alphanumeric/edzAlphanumericCustomFieldEdit.html",
            restrict: "E",
            scope: true,
            controller: ["$scope", function ($scope) {
                var self = this;
                function loadData() {
                    self.model = {};
                    self.model.allowedAdditionalCharacters = edzCustomFieldEditHelper.
                        getOrCreatePropertyWithPush($scope.data, CUSTOM_FIELD.PROPERTY_NAME.ALPHANUMERIC.ALLOWED_ADDITIONAL_CHARACTERS, $scope.id);
                    self.model.length = edzCustomFieldEditHelper.getOrCreatePropertyWithPush($scope.data, CUSTOM_FIELD.PROPERTY_NAME.GENERAL.LENGTH, $scope.id);
                    self.model.length.value = parseInt(self.model.length.value);
                    self.model.resizeField = edzCustomFieldEditHelper.getOrCreatePropertyWithPush($scope.data, CUSTOM_FIELD.PROPERTY_NAME.ALPHANUMERIC.RESIZE_FIELD, $scope.id);
                    self.model.resizeField.value = Boolean(self.model.resizeField.value === "True");
                }

                function activate() {
                    loadData();
                }

                activate();
            }],
            controllerAs: "edzAlphanumericCustomFieldEditCtrl"
        };
    }
})();
