﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientCare.Web.Api.Models.EntryTypes
{
    public class DayItem
    {
        public bool? MON { get; set; }
        public bool? TUE { get; set; }
        public bool? WED { get; set; }
        public bool? THU { get; set; }
        public bool? FRI { get; set; }
        public bool? SAT { get; set; }
        public bool? SUN { get; set; }
    }
}