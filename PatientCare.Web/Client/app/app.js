﻿"use strict";
angular.module("EDZoutstaffingPortalApp", [
    "ngCookies",
    "ngResource",
    "ngSanitize",
    "ui.router",
    "ui.bootstrap",
    "ngAnimate",
    "ngTable",
    "adf",
    "adf.provider",
    "chart.js",
    "frapontillo.gage",
    "satellizer",
    "toaster",
    "ui.select",
    "ngSanitize",
    "angucomplete-alt",
    "datePicker",
    "ngMessages",
    "angularUUID2",
    "ngFileUpload",
    "cgBusy",
    "angular-loading-bar",
    "ngAnimate",
    "ngImgCrop",
    "ng-currency",
    "dndLists",
    "angular.panels",
    'nvd3ChartDirectives',
    "ui.grid",
    "ui.grid.grouping",
    "ui.grid.pinning",
    "ui.grid.pagination",
    "ui.grid.moveColumns",
    "ui.grid.edit",
    "ui.grid.cellNav",
    "ui.calendar",
    "ui.grid.treeView",
    "pascalprecht.translate",
    "angularjs-dropdown-multiselect",
    "minicolors",
    "ngIdle",
])
    .config(["$translateProvider", function translationConfigFn($translateProvider) {
        // Set Default Language/Tranlation Strings
        $translateProvider.preferredLanguage('en');

        $translateProvider.useStaticFilesLoader({
            prefix: 'i18n/ProjectToMatters/',
            suffix: '.txt'
        })
        $translateProvider.useLocalStorage();
    }])
    .config(['panelsProvider', function (panelsProvider) {
        panelsProvider.add({
            id: 'rightMenu',
            position: 'right',
            size: '321px',
            templateUrl: 'Client/app/components/rightmenu/rightmenu.html',
            controller: 'RightMenuCntrl'
        });
    }])
    .constant("config", {
        userRoles: {
            HUMAN_RESOURCE: "HumanResource",
            RESOURCE: "Resource",
            ADMIN: "Admin"
        }
    })
    .constant("APP_CONFIG", configData)
    .constant("CONS_BUILD_FOR", { client: "STINSON", isForStinson: false })
    .run(["$rootScope", "Auth", "$state", "$templateCache", "$q", "UserMonitorController",
        function ($rootScope, auth, $state, $templateCache, $q, userMonitorController) {
            var redirectTo = "/";
            var currentUser = auth.getCurrentUser();
            if (!currentUser && !currentUser.ResourceId) {
                if (localStorage.getItem("irms_access_token") != null) {
                    localStorage.removeItem("irms_access_token");
                    if (auth.isAuthenticated())
                        auth.logout();
                }
                $state.go("login");
                redirectTo = "login";
            }
            
            //if (auth.isAuthenticated() && !auth.getNDA()) {
            //    $state.go("terms");
            //    redirectTo = "terms";
            //}

            $rootScope.$on("$stateChangeStart", function (event, next) {

                var security = next.data.security || {};
                if (security.authenticated) {
                    if ((!auth.isAuthenticated() && next.requireLogin != false) || localStorage.getItem("irms_access_token") == null) {
                        event.preventDefault();
                        $state.go("login");
                    }
                }
                debugger;
                var isNDA = auth.getNDA();
                if (auth.isAuthenticated() && !isNDA && next.name != 'terms' && next.name != 'login') {
                    event.preventDefault();
                    $state.go("terms");
                }

            });
            //add loader
            $rootScope.stateIsLoading = false;
            var deferred;
            $rootScope.$on("$stateChangeStart", function () {
                deferred = $q.defer();
                $rootScope.loadState = deferred;
            });
            $rootScope.$on("$stateChangeSuccess", function (ev, to, toParams, from, fromParams) {
                $rootScope.appStateManager = from;
                $rootScope.appStateManager.fromParams = fromParams;
                $rootScope.currentState = to;

                if (deferred)
                    deferred.resolve();
            });
            $rootScope.$on("$locationChangeError", function () {
                if (deferred)
                    deferred.resolve();
            });
            $rootScope.$on("$cancelLeavePage", function () {
                if (deferred)
                    deferred.resolve();
            });
            //fix ui-select
            $templateCache.put("bootstrap/choices.tpl.html",
                "<ul class=\"ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu\" role=\"listbox\" ng-show=\"$select.items.length > 0\"><li class=\"ui-select-choices-group\" id=\"ui-select-choices-{{ $select.generatedId }}\"><div class=\"divider\" ng-show=\"$select.isGrouped && $index > 0\"></div><div ng-show=\"$select.isGrouped\" class=\"ui-select-choices-group-label dropdown-header\" ng-bind=\"$group.name\"></div><div id=\"ui-select-choices-row-{{ $select.generatedId }}-{{$index}}\" class=\"ui-select-choices-row\" ng-class=\"{active: $select.isActive(this), disabled: $select.isDisabled(this)}\" role=\"option\"><a href=\"javascript:void(0)\" onclick=\"return false;\" class=\"ui-select-choices-row-inner\"></a></div></li></ul>");

            if (auth.isAuthenticated())
                userMonitorController.initUserIdleTimeOutFunctionality();

            //$state.go(redirectTo, {}, { reload: true });

        }
    ])
    .config(["ChartJsProvider", function (ChartJsProvider) {
        ChartJsProvider.setOptions({
            //colours: ['#ff0000', '#93c47d', '#4a86e8'],
            responsive: true,
            animation: false
        });
    }
    ]);