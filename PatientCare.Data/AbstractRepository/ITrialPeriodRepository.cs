﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface ITrialPeriodRepository : IRepository<TrialPeriod>
    {
    }
}
