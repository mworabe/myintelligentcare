﻿using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.ChatSessions
{
    [DataContract]
    public class ChatResponse
    {
        [DataMember]
        public ChatSessionResponse[] ChatDetails { get; set; }

        [DataMember]
        public ChatUserDetailResponse[] UserDetails { get; set; }
    }
}