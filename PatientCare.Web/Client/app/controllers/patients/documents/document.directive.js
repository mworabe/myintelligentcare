﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('attachmentDocuments', function ($compile) {
    return {
        scope: {
            config: "=config",
            //loadingBarFlag: "=isLoading",
            patientId: "=",
            caseId: "="
        },
        templateUrl: "Client/app/controllers/patients/documents/document.html",
        controller: ['$scope',
        function ($scope) {



            $scope.tabs = [{
                id: "Attachment",
                title: "Attachment",
            }, {
                id: "DigitalForms",
                title: "Digital Forms"
            }];

            $scope.selectedTab = $scope.tabs[0].id;

            $scope.isTabSelected = function (id) {
                if (id == $scope.selectedTab) return true;
                return false;
            }
            $scope.setSelectedTab = function (id) {
                $scope.selectedTab = id;
            }


            $scope.attachmentList = [
                {
                    title: "Right Shoulder",
                    type: "header"
                },{
                    name: "MRI",
                    file: "file 2",
                    dateOfRecept: "01/02/2017",
                    timeField: "01/02/2017",
                    expirationDate: "01/02/2017",
                    noteType: "",
                    signee: "Sitter, Jack"
                }, {
                    name: "Prescription",
                    file: "file 1",
                    dateOfRecept: "01/02/2017",
                    timeField: "01/02/2017",
                    expirationDate: "01/02/2017",
                    noteType: "",
                    signee: "Klein, Sue"
                }, {
                    title: "785 – Tendonitis",
                    type: "header"
                }, {
                    name: "XRAY",
                    file: "file 3",
                    dateOfRecept: "01/02/2017",
                    timeField: "01/02/2017",
                    expirationDate: "01/02/2017",
                    noteType: "",
                    signee: "Sitter, Jack"
                }, {
                    name: "MRI",
                    file: "file 4",
                    dateOfRecept: "01/02/2017",
                    timeField: "01/02/2017",
                    expirationDate: "01/02/2017",
                    noteType: "",
                    signee: "Klein, Sue"
                }
            ];





        }]
    }
});



