﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("ChangePassword", ["$resource", function ($resource) {
        var resource = $resource("api/Accounts/:controller",
        null, //parameters default
          {
              'changePassword': { method: "POST", params: { controller: "change-password" }, isArray: false },
              'resetPassword': { method: "POST", params: { controller: "reset-password" }, isArray: false },
          });
      return resource;
  }]);