﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ConfigLayoutCtrl", [
        "ConfigLayout", "$scope", "$stateParams", "RoleSetup", "$q", "toaster", "CustomField", "loadedInitialData", "$state", "$translate", "$filter", "userTimeZoneFactory", "SystemFunctions",
        function (configLayoutFactory, $scope, $stateParams, roleSetupService, $q, toaster, customFieldService, loadedInitialData, $state, $translate, $filter, userTimeZoneService, systemFunctionsService) {
            var self = this;
            var roleId = $stateParams.id;
            self.titleText = (loadedInitialData ? loadedInitialData.name : "Default Layout") + " / " + "Layout Edit";

            function clearPage() {
                self.selectEditPage = null;
            }

            self.cancelClick = function () {
                $state.go("adminConfigData", { currentTab: "roles" });
            }

            function initialTabList(list) {
                _.forEach(list, function (item) {
                    item.__type = "tab";
                    item.name = $filter('translate')(item.name);
                });
            }

            function checkAndSetProperty(property, propertyName) {
                if (!property[propertyName]) {
                    property[propertyName] = [];
                }
                return property[propertyName];
            }


            function initialFieldList(list, cb) {
                cb = cb || angular.noop;

                _.forEach(list, function (item) {
                    item.__type = "field";
                    cb(item, list);
                });
                return list;
            }

            function removeFieldInList(item) {
                var deleteItem = _.remove(self.fieldList, { id: item.id });
                if (deleteItem.length === 0) {
                    item.needDelete = true;
                } else {
                    item.required = deleteItem[0].required;
                }
            }

            function clearContainerUnusedField(container) {
                _.remove(container, { needDelete: true });

            }

            function validate() {
                var item = _.find(self.fieldList, { required: true });
                if (item) {
                    toaster.pop("error", "", item.name + " is required");
                    return false;
                }
                return true;
            }

            function initialContainers(containers) {
                _.forEach(containers, function (item) {
                    clearContainerUnusedField(initialFieldList(checkAndSetProperty(item, "container1"), removeFieldInList));
                    clearContainerUnusedField(initialFieldList(checkAndSetProperty(item, "container2"), removeFieldInList));
                    clearContainerUnusedField(initialFieldList(checkAndSetProperty(item, "container3"), removeFieldInList));
                    clearContainerUnusedField(initialFieldList(checkAndSetProperty(item, "container4"), removeFieldInList));
                    clearContainerUnusedField(initialFieldList(checkAndSetProperty(item, "container5"), removeFieldInList));
                    clearContainerUnusedField(initialFieldList(checkAndSetProperty(item, "container6"), removeFieldInList));
                });
            }

            function addCustomFields(customFields, fieldList) {
                _.forEach(customFields, function (customField) {
                    fieldList.push(configLayoutFactory.createField(customField.id, customField.name, customField.require, customField.id));
                });
            }

            function afterLoadLayout() {
                self.__selectPage = null;
                self.selectedPageEdit = null;
            }


            function loadTask() {
                function loadConfigLayout() {
                    return $q(function (resolve) {
                        if ($stateParams.id) {
                            self.busyPage = roleSetupService.projectTaskLayoutConfigForRole({ id: $stateParams.id }, function (responce) {
                                afterLoadLayout();
                                resolve(responce);
                            });
                        } else {
                            self.busyPage = roleSetupService.projectTaskLayoutDefault(function (responce) {
                                afterLoadLayout();
                                resolve(responce);
                            });
                        }
                    });
                }

                function loadCustomFields() {
                    return $q(function (resolve) {
                        customFieldService.customFieldsForProjectTask(function (responce) {
                            resolve(responce);
                        });
                    });

                }

                self.busyPage = $q.all([loadConfigLayout(), loadCustomFields()])
                    .then(function (resolutions) {
                        var layout = resolutions[0];
                        var customFields = resolutions[1];
                        self.selectModel = {};

                        var editTabs = configLayoutFactory.getProjectTaskEditTabList();

                        self.fieldList = configLayoutFactory.getProjectTaskFields();

                        self.tabList = _.unionBy(layout.tabs || [], configLayoutFactory.getProjectTaskTab(), "id");
                        if (!layout.containers) {
                            self.containers = configLayoutFactory.getDefaultProjectTaskContainer();
                        } else {
                            self.containers = _.unionBy(layout.containers, editTabs, "id");
                        }

                        self.selectEditPageList = editTabs;
                        initialTabList(self.tabList);
                        addCustomFields(customFields, self.fieldList);
                        initialFieldList(self.fieldList);
                        initialContainers(self.containers);

                        self.visibleLayoutForm = true;
                    });
            }




            function loadIntake() {
                function loadConfigLayout() {
                    return $q(function (resolve) {
                        if ($stateParams.id) {
                            self.busyPage = roleSetupService.projectTaskLayoutConfigForRole({ id: $stateParams.id }, function (responce) {
                                afterLoadLayout();
                                resolve(responce);
                            });
                        } else {
                            self.busyPage = roleSetupService.intakeLayoutDefault(function (responce) {
                                afterLoadLayout();
                                resolve(responce);
                            });
                        }
                    });
                }

                function loadCustomFields() {
                    return $q(function (resolve) {
                        customFieldService.customFieldForIntake(function (responce) {
                            resolve(responce);
                        });
                    });

                }

                self.busyPage = $q.all([loadConfigLayout(), loadCustomFields()])
                    .then(function (resolutions) {
                        var layout = resolutions[0];
                        var customFields = resolutions[1];
                        self.selectModel = {};

                        var editTabs = configLayoutFactory.getIntakeEditTabs();

                        self.fieldList = configLayoutFactory.getItakeFields();

                        self.tabList = _.unionBy(layout.tabs || [], configLayoutFactory.getIntakeTabs(), "id");
                        if (!layout.containers) {
                            self.containers = configLayoutFactory.getDefaultIntakeContainer();
                        } else {
                            self.containers = _.unionBy(layout.containers, editTabs, "id");
                        }

                        self.selectEditPageList = editTabs;
                        initialTabList(self.tabList);
                        addCustomFields(customFields, self.fieldList);
                        initialFieldList(self.fieldList);
                        initialContainers(self.containers);

                        self.visibleLayoutForm = true;
                    });
            }

            function loadProject() {
                function loadConfigLayout() {
                    return $q(function (resolve) {
                        if ($stateParams.id) {
                            self.busyPage = roleSetupService.projectLayoutConfigForRole({ id: $stateParams.id }, function (responce) {
                                afterLoadLayout();
                                resolve(responce);
                            });
                        } else {
                            self.busyPage = roleSetupService.projectLayoutDefault(function (responce) {
                                afterLoadLayout();
                                resolve(responce);
                            });
                        }
                    });
                }

                function loadCustomFields() {
                    return $q(function (resolve, reject) {
                        customFieldService.customFieldsForProject(function (responce) {
                            resolve(responce);
                        });
                    });

                }

                self.busyPage = $q.all([loadConfigLayout(), loadCustomFields()])
                    .then(function (resolutions) {
                        var layout = resolutions[0];
                        var customFields = resolutions[1];
                        self.selectModel = {};

                        var editTabs = configLayoutFactory.getProjectEditTabList();

                        self.fieldList = configLayoutFactory.getProjectFields();

                        self.tabList = _.unionBy(layout.tabs || [], configLayoutFactory.getProjectTab(), "id");
                        if (!layout.containers) {
                            self.containers = configLayoutFactory.getDefaultProjectContainer();
                        } else {
                            self.containers = _.unionBy(layout.containers, editTabs, "id");

                        }
                        self.selectEditPageList = editTabs;
                        initialTabList(self.tabList);
                        addCustomFields(customFields, self.fieldList);
                        initialFieldList(self.fieldList);
                        initialContainers(self.containers);

                        self.visibleLayoutForm = true;
                    });
            }

            function loadResource() {

                function loadConfigLayout() {
                    return $q(function (resolve) {
                        if ($stateParams.id) {
                            self.busyPage = roleSetupService.resourceLayoutConfigForRole({ id: $stateParams.id }, function (responce) {
                                afterLoadLayout();
                                resolve(responce);
                            });
                        } else {
                            self.busyPage = roleSetupService.resourceLayoutDefault(function (responce) {
                                afterLoadLayout();
                                resolve(responce);
                            });
                        }
                    });
                }

                function loadCustomFields() {
                    return $q(function (resolve) {
                        customFieldService.customFieldsForResource(function (responce) {
                            self.__selectPage = null;
                            resolve(responce);
                        });
                    });
                }

                self.busyPage = $q.all([loadConfigLayout(), loadCustomFields()])
                    .then(function (resolutions) {
                        var layout = resolutions[0];
                        var customFields = resolutions[1];
                        self.selectModel = {};

                        var editTabs = configLayoutFactory.getResourceEditTabList();

                        self.fieldList = configLayoutFactory.getResourceFields();

                        self.tabList = _.unionBy(layout.tabs || [], configLayoutFactory.getResourceTab(), "id");
                        if (!layout.containers) {
                            self.containers = configLayoutFactory.getDefaultResourceContainer();
                        } else {
                            self.containers = _.unionBy(layout.containers, editTabs, "id");

                        }
                        self.selectEditPageList = editTabs;
                        initialTabList(self.tabList);
                        addCustomFields(customFields, self.fieldList);
                        initialFieldList(self.fieldList);
                        initialContainers(self.containers);

                        self.visibleLayoutForm = true;
                    });
            }

            function scrollTop() { systemFunctionsService.scrollToTopZero() }


            function onSaveResourceClick() {
                var actionSaveName = roleId ? "changeLayout" : "changeDefaultLayout";
                if (!validate()) {
                    return;
                }
                scrollTop();
                userTimeZoneService.clearResourceDefaultConfigs();
                self.busyPage = roleSetupService[actionSaveName]({ id: roleId }, {
                    type: "Resource",
                    configLayout: {
                        tabs: self.tabList,
                        containers: self.containers

                    }
                }, function () {
                    toaster.pop("success", "", "save successful");
                });

            }
            function onSaveProjectClick() {
                var actionSaveName = roleId ? "changeLayout" : "changeDefaultLayout";
                if (!validate()) {
                    return;
                }
                scrollTop();
                userTimeZoneService.clearResourceDefaultConfigs();
                self.busyPage = roleSetupService[actionSaveName]({ id: roleId }, {
                    type: "Project",
                    configLayout: {
                        tabs: self.tabList,
                        containers: self.containers

                    }
                }, function () {
                    toaster.pop("success", "", "save successful");
                });

            }
            function onSaveProjectTaskClick() {
                if (!validate()) {
                    return;
                }
                scrollTop();
                var actionSaveName = roleId ? "changeLayout" : "changeDefaultLayout";
                self.busyPage = roleSetupService[actionSaveName]({ id: roleId }, {
                    type: "Task",
                    configLayout: {
                        tabs: self.tabList,
                        containers: self.containers

                    }
                }, function () {
                    toaster.pop("success", "", "save successful");
                });

            }
            function onSaveIntakeClick() {
                if (!validate()) {
                    return;
                }
                scrollTop();
                var actionSaveName = roleId ? "changeLayout" : "changeDefaultLayout";
                self.busyPage = roleSetupService[actionSaveName]({ id: roleId }, {
                    type: "Intake",
                    configLayout: {
                        tabs: self.tabList,
                        containers: self.containers

                    }
                }, function () {
                    toaster.pop("success", "", "save successful");
                });

            }

            function initialList() {
                self.actions = [
                    { name: "Resource", action: loadResource, save: onSaveResourceClick }
                    //{ name: "Project", action: loadProject, save: onSaveProjectClick },
                    //{ name: "Task", action: loadTask, save: onSaveProjectTaskClick },
                    /*  Temporary commented, need to unblock after backend ready.*/
                    //{ name: "Intake", action: loadIntake, save: onSaveIntakeClick },
                ];
            }

            function activate() {
                initialList();
            }


            function action_Click($item) {
                clearPage();
                $item.action();
            }

            function type_Click($item) {
                self.selectedPageEdit = _.find(self.containers, { id: $item.id });
            }
            activate();
            this.action_Click = action_Click;
            this.type_Click = type_Click;
        }
    ]);