﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Services.Validation;
using FluentValidation;

namespace PatientCare.Web.Api.Models.ResourceSpokenLanguages
{
    public class ResourceSpokenLanguageRequestValidator : AbstractValidator<ResourceSpokenLanguageRequest>
    {
        private readonly IDataContextScopeFactory _dbContextScopeFactory;
        private readonly IResourceRepository _resourceRepository;
        private readonly IPicklistRepository<Language> _languageRepository;

        public ResourceSpokenLanguageRequestValidator(IDataContextScopeFactory dbContextScopeFactory, IResourceRepository resourceRepository, IPicklistRepository<Language> languageRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _resourceRepository = resourceRepository;
            _languageRepository = languageRepository;

            RuleFor(model => model.YearsOfExpirience)
                .InclusiveBetween(0, 100);

            RuleFor(model => model.SpeakingProficiency)
                .InclusiveBetween(1, 10);

            RuleFor(model => model.WritingProficiency)
                .InclusiveBetween(1, 10);

            //RuleFor(model => model.ResourceId)
            //    .NotEmpty()
            //    .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _resourceRepository, l))
            //    .WithMessage("The resource with provided Id doesn't exists");

            RuleFor(model => model.LanguageId)
                .NotEmpty()
                .Must(l => ValidationHelpers.ItemBeExist(_dbContextScopeFactory, _languageRepository, l))
                .WithMessage("The language with provided Id doesn't exists");

        }
    }
}