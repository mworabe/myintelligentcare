"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider, config) {
        $stateProvider
            .state("home", {
                url: "/",
                controller: ["PermissionWorker", "$state", function (permissionWorker, $state) {
                    //$state.go("home_human_resource");
                    //$state.go(permissionWorker.getHomePageState());
                    var homePage = permissionWorker.getHomePageState();
                    if (homePage.hasParams) {
                        $state.go(homePage.state, homePage.params);
                    } else {
                        $state.go(homePage.state);
                    }

                }],
                data: {
                    security: {
                        authenticated: true
                    }
                },
                requireLogin: true,
            })
            .state("home_human_resource", {
                url: "/",
                controller: "HomeHumanResourceCtrl as HomeHumanResourceCtrl",
                templateUrl: "Client/app/controllers/home/homeHumanResourceV2.html",
                data: {
                    security: {
                        authenticated: true,
                        availableRole: [config.userRoles.HUMAN_RESOURCE, config.userRoles.ADMIN]
                    }
                },
                requireLogin: true,
            })
            .state("home_resource", {
                url: "/",
                controller: "HomeHumanResourceCtrl as HomeHumanResourceCtrl",
                templateUrl: "Client/app/controllers/home/homeHumanResourceV2.html",
                data: {
                    security: {
                        authenticated: true,
                        availableRole: [config.userRoles.RESOURCE]
                    }
                },
                requireLogin: true,
            }).state("home_care_managment", {
                url: "/care-managment/",
                controller: "HomeCareManagmentCtrl as CareHomeCtrl",
                templateUrl: "Client/app/controllers/home/homeCareManagment.html",
                data: {
                    security: {
                        authenticated: true,
                        availableRole: [config.userRoles.RESOURCE]
                    }
                },
                requireLogin: true,
            });

        
    });