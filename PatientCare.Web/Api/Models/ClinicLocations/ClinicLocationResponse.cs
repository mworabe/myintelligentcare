﻿using PatientCare.Web.Api.Models.Clinics;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ClinicLocations
{
    [DataContract]
    public class ClinicLocationResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }        

        [DataMember]
        public string ContactPerson { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string LocationPrefix { get; set; }

        [DataMember]
        public long? ClinicId { get; set; }

        [DataMember]
        public ClinicAutoCompleteResponse Clinic { get; set; }

        [DataMember]
        public bool? IsDeleted { get; set; }
    }
}