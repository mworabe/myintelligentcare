"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("Modal", [
        "$rootScope", "$uibModal", function ($rootScope, $modal) {
            /**
             * Opens a modal
             * @param  {Object} scope      - an object to be merged with modal's scope
             * @param  {String} modalClass - (optional) class(es) to be applied to the modal
             * @return {Object}            - the instance $modal.open() returns
             */
            function openModal(scope, modalClass, backdrop) {
                var modalScope = $rootScope.$new();
                scope = scope || {};
                modalClass = modalClass || "modal-default";

                angular.extend(modalScope, scope);

                return $modal.open({
                    templateUrl: "Client/app/components/modal/modal.html",
                    windowClass: modalClass,
                    scope: modalScope,
                    backdrop: backdrop,
                    size: scope.modal.size,
                    keyboard: false
                });
            }

            // Public API here
            return {


                showCnfgDlg: function (config) {

                    return $modal.open({
                        templateUrl: "Client/app/components/modal/new-cnf-modal.html",
                        controller: "NewCnfModelCntrl",
                        size: "sm",
                        keyboard: false,
                        resolve: {
                            config: function () {
                                return config;
                            }
                        }
                    });
                    
                },


                /* Confirmation modals */
                confirm: {

                    /**
                     * Create a function to open a delete confirmation modal (ex. ng-click='myModalFn(name, arg1, arg2...)')
                     * @param  {Function} del - callback, ran when delete is confirmed
                     * @return {Function}     - the function to open the modal (ex. myModalFn)
                     */
                    delete: function (del) {
                        del = del || angular.noop;

                        /**
                           * Open a delete confirmation modal
                           * @param  {String} name   - name or info to show on modal
                           * @param  {All}           - any additional args are passed straight to del callback
                         */
                        return function (title, entityName) {
                            var args = Array.prototype.slice.call(arguments);

                            var deleteModal = openModal({
                                modal: {
                                    dismissable: true,
                                    title: title,
                                    entityName: entityName,
                                    templateUrl: "Client/app/components/modal/templates/delete.html",
                                    buttons: [
                                        {
                                            classes: "btn-danger btn-red",
                                            text: "Delete",
                                            click: function (e) {
                                                deleteModal.close(e);
                                            }
                                        }, {
                                            classes: "btn-default btn-light-gray",
                                            text: "Cancel",
                                            click: function (e) {
                                                deleteModal.dismiss(e);
                                            }
                                        }
                                    ]
                                }
                            }, "modal-danger");

                            deleteModal.result.then(function (event) {
                                del.apply(event, args);
                            });
                        };
                    },

                    angucompleteSelector: function (funcOk, funcCancel) {
                        funcOk = funcOk || angular.noop;
                        funcCancel = funcCancel || angular.noop;
                        function getValueInAnguAutocomplete(selected) {
                            return (selected && selected.originalObject) ? selected.originalObject.id : null;
                        }
                        return function (title, search) {
                            var id = 0;
                            var selectModal;
                            var modal = {
                                dismissable: true,
                                title: title,
                                search: search,
                                selected: function (selected) {
                                    modal.id = getValueInAnguAutocomplete(selected) || null;
                                    id = modal.id;
                                },
                                templateUrl: "Client/app/components/modal/templates/angucompleteSelect.html",
                                buttons: [
                                    {
                                        classes: "btn-danger",
                                        text: "Save",
                                        type: "submit",
                                        click: function (e, form) {
                                            if (!form.$valid)
                                                return;
                                            selectModal.close(e);
                                            funcOk(id);
                                        }
                                    }, {
                                        classes: "btn-default",
                                        text: "Cancel",
                                        click: function (e) {
                                            selectModal.dismiss(e);
                                            funcCancel();
                                        }
                                    }
                                ]
                            };
                            selectModal = openModal({
                                modal: modal
                            });
                        }
                    },

                    templateDialog: function (okCallback, templateUrl, noCallback) {
                        okCallback = okCallback || angular.noop;
                        noCallback = noCallback || angular.noop;
                        templateUrl = templateUrl || "";
                        return function (data) {
                            var modal;

                            var modalObject = {
                                dismissable: true,
                                backdrop: "static",
                                title: "",
                                data: data,
                                templateUrl: templateUrl,
                                buttons: [
                                    {
                                        classes: "btn-default btn-light-gray",
                                        text: "Cancel",
                                        click: function (e) {
                                            noCallback(modalObject.data);
                                            modal.close(e);
                                        }
                                    },
                                    {
                                        classes: "btn-success btn-blue",
                                        text: "Save",
                                        type: "submit",
                                        click: function (e, form) {
                                            if (!form.$valid)
                                                return;
                                            okCallback(modalObject.data);
                                            modal.close(e);
                                        }
                                    }

                                ]
                            };

                            modal = openModal({
                                modal: modalObject
                            }, "modal-warning", "static");
                        };
                    },

                    confirmCreateProject: function (del) {
                        del = del || angular.noop;

                        /**
                           * Open a delete confirmation modal
                           * @param  {String} name   - name or info to show on modal
                           * @param  {All}           - any additional args are passed straight to del callback
                         */
                        return function (title, entityName, successCallBack, failCallBack) {
                            var args = Array.prototype.slice.call(arguments);

                            var deleteModal = openModal({
                                modal: {
                                    dismissable: true,
                                    title: title,
                                    entityName: entityName,
                                    templateUrl: "Client/app/components/modal/templates/create-project-intake-template.html",
                                    buttons: [
                                        {
                                            classes: "btn-default btn-orange",
                                            text: "Create",
                                            click: function (e) {
                                                deleteModal.close(e, true);
                                                successCallBack();
                                            }
                                        }, {
                                            classes: "btn-default btn-light-gray",
                                            text: "Maybe Later",
                                            click: function (e) {
                                                deleteModal.close(e, false);
                                                failCallBack();
                                            }
                                        }
                                    ]
                                }
                            }, "modal-danger");

                            deleteModal.result.then(function (event) {
                                del.apply(event, args);
                            });
                        };
                    }

                }
            };
        }
    ]);