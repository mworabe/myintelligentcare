﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;
using System.Data.Entity.Infrastructure.Annotations;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceMappingExtensions
    {
        public static DbModelBuilder MapResource(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<Resource>().ToTable("resources").HasKey(i => i.Id);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<Resource>()
                .Property(p => p.FirstName)
                .HasColumnName("first_name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_first_name", 1)));

            modelBuilder.Entity<Resource>()
                .Property(p => p.MiddleName)
                .HasColumnName("middle_name")
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.LastName)
                .HasColumnName("last_name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_last_name", 2)));

            modelBuilder.Entity<Resource>()
                .Property(p => p.JobTitleId)
                .HasColumnName("job_title_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<JobTitle>(p => p.JobTitle)
                .WithMany()
                .HasForeignKey(p => p.JobTitleId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Address)
                .HasColumnName("address")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Address2)
                .HasColumnName("address2")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Address3)
                .HasColumnName("address3")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Address4)
                .HasColumnName("address4")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Address5)
                .HasColumnName("address5")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Address6)
                .HasColumnName("address6")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.ZipCode)
                .HasColumnName("zipcode")
                .IsOptional()
                .HasMaxLength(10);

            modelBuilder.Entity<Resource>()
               .Property(p => p.CityId)
               .HasColumnName("city_id")
               .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<City>(p => p.City)
                .WithMany()
                .HasForeignKey(p => p.CityId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.CountryId)
                .HasColumnName("country_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<Country>(p => p.Country)
                .WithMany()
                .HasForeignKey(p => p.CountryId);

            modelBuilder.Entity<Resource>()
               .Property(p => p.StateId)
               .HasColumnName("state_id")
               .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<State>(p => p.State)
                .WithMany()
                .HasForeignKey(p => p.StateId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.ResourceTeamId)
                .HasColumnName("resource_team_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<ResourceTeam>(p => p.ResourceTeam)
                .WithMany()
                .HasForeignKey(p => p.ResourceTeamId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.YearsEmployed)
                .HasColumnName("years_employed")
                .IsOptional()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_years_employed", 3)));

            modelBuilder.Entity<Resource>()
                .Property(p => p.GradeLevel)
                .HasColumnName("grade_level")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.YearsOfCollege)
                .HasColumnName("years_of_college")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.CollegeDegree)
                .HasColumnName("college_degree")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.TimeZoneInfo)
                .HasColumnName("time_zone_info")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.Location)
                .HasColumnName("location")
                .IsOptional()
                .IsUnicode()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_location", 4)));

            modelBuilder.Entity<Resource>()
                .Property(p => p.ValidPassport)
                .HasColumnName("valid_passport")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.Active)
                .HasColumnName("active")
                .IsOptional()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_Active", 5)));

            modelBuilder.Entity<Resource>()
                .Property(p => p.AvailableToTravel)
                .HasColumnName("available_to_travel")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.FirmRate)
                .HasColumnName("firm_rate")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.NationalRate)
                .HasColumnName("national_rate")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.HomeNumber)
                .HasColumnName("home_number")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.RatingDate)
                .HasColumnName("rating_date")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.TimekeeperNumber)
                .HasColumnName("timekeeper_number")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.WorksWellAlone)
                .HasColumnName("works_well_alone")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.WorksWellInTeam)
                .HasColumnName("works_well_in_team")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.EverBeenConvictedOfACrime)
                .HasColumnName("ever_been_convicted_of_crime")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.InformationCertifiedByEmployer)
                .HasColumnName("information_certified_by_employer")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.Ethnicity)
                .HasColumnName("ethnicity")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.ArmedForces)
                .HasColumnName("armed_forces")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.ArmedForcesCountryId)
                .HasColumnName("armed_forces_country_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<Country>(p => p.ArmedForcesCountry)
                .WithMany()
                .HasForeignKey(p => p.ArmedForcesCountryId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.ArmedForcesBranch)
                .HasColumnName("armed_forces_branch")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.ClinicId)
                .HasColumnName("clinic_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<Clinic>(p => p.Clinic)
                .WithMany()
                .HasForeignKey(p => p.ClinicId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.ClinicLocationId)
                .HasColumnName("clinic_location_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<ClinicLocation>(p => p.ClinicLocation)
                .WithMany()
                .HasForeignKey(p => p.ClinicLocationId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.DrugTest)
                .HasColumnName("drug_test")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.CreditReportOk)
                .HasColumnName("credit_report_ok")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.ProofOfCitizenship)
                .HasColumnName("proof_of_citizenship")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.HourlyRate)
                .HasColumnName("hourly_rate")
                .IsOptional()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_hourly_rate", 6)));

            modelBuilder.Entity<Resource>()
                .Property(p => p.Salary)
                .HasColumnName("salary")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.BonusOrOtherPay)
                .HasColumnName("bonus_or_other_pay")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.Ssn)
                .HasColumnName("ssn")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.EmployeeId)
                .HasColumnName("employee_id")
                .IsOptional()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Range)
                .HasColumnName("range")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.EmploymentType)
                .HasColumnName("employment_type")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.Gender)
                .HasColumnName("gender")
                .IsOptional()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_gender", 7)));

            modelBuilder.Entity<Resource>()
                .Property(p => p.Relationship)
                .HasColumnName("relationship")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.MartialStatus)
                .HasColumnName("martial_status")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.StartDate)
                .HasColumnName("start_date")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.BirthDate)
                .HasColumnName("birth_date")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.OfficeNumber)
                .HasColumnName("office_Number")
                .HasMaxLength(128)
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.MobilePhoneNumber)
                .HasColumnName("mobile_phone_number")
                .HasMaxLength(128)
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.Extension)
                .HasColumnName("extension")
                .HasMaxLength(128)
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.EmergencyContactName)
                .HasColumnName("emergency_contact_name")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.Email)
                .HasColumnName("email")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.PreferredFirstname)
                .HasColumnName("preferred_first_name")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.EmergencyContactPhoneNumber)
                .HasColumnName("emergency_contact_phone")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.MotorVehicleReport)
                .HasColumnName("motor_vehicle_report")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.MotorVehicleReportOther)
                .HasColumnName("motor_vehicle_report_other")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.ProfessionalReferenceChecks)
                .HasColumnName("professional_reference_checks")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.ProfessionalReferenceChecksOther)
                .HasColumnName("professional_reference_checks_other")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.EmploymentEligibilityVerification)
                .HasColumnName("employment_eligibility_verification")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.EmploymentEligibilityVerificationOther)
                .HasColumnName("employment_eligibility_verification_other")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.InternationalWorkHistory)
                .HasColumnName("international_work_history")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.InternationalWorkHistoryOther)
                .HasColumnName("international_work_history_other")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.CredentialVerifications)
                .HasColumnName("credential_verifications")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.CredentialVerificationsOther)
                .HasColumnName("credential_verifications_other")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.FormI9)
                .HasColumnName("form_i_9")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.FormI9Other)
                .HasColumnName("form_i_9_other")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.FormEVerify)
                .HasColumnName("form_e_verify")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.FormEVerifyOther)
                .HasColumnName("form_e_verify_other")
                .IsOptional()
                .HasMaxLength(1023);

            modelBuilder.Entity<Resource>()
                .Property(p => p.FingerPrinting)
                .HasColumnName("finger_printing")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.FingerPrintingOther)
                .HasColumnName("finger_printing_other")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.DrugScreening)
                .HasColumnName("drug_screening")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.DrugScreeningOther)
                .HasColumnName("drug_screening_other")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.CreditCheck)
                .HasColumnName("credit_check")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.CreditCheckOther)
                .HasColumnName("credit_check_other")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.RbParticipating)
                .HasColumnName("rb_participating")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.RbEligibleWages)
                .HasColumnName("rb_eligible_wages")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.RbEmployeeContribution)
                .HasColumnName("rb_employee_contribution")
                .IsOptional();


            modelBuilder.Entity<Resource>()
                .Property(p => p.RbEmployerMatching)
                .HasColumnName("rb_employer_matching")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.DivisionId)
                .HasColumnName("division_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<Division>(p => p.Division)
                .WithMany()
                .HasForeignKey(p => p.DivisionId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.SupervisorId)
                .HasColumnName("supervisor_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<Resource>(p => p.Supervisor)
                .WithMany()
                .HasForeignKey(p => p.SupervisorId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.DepartmentId)
                .HasColumnName("department_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<Department>(p => p.Department)
                .WithMany()
                .HasForeignKey(p => p.DepartmentId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceTravel>(p => p.Travels)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceSpokenLanguage>(p => p.SpokenLanguages)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceCriminalRecord>(p => p.CriminalRecords)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceEducation>(p => p.Educations)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceCustomFieldValue>(p => p.CustomFieldValues)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourcePreviousEmployment>(p => p.PreviousEmployments)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceSurveyResult>(p => p.SurveyResults)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceCertificate>(p => p.Certificates)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceSkill>(p => p.Skills)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceWorkExperience>(p => p.ResourceWorkExperiences)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceProfessionalOrganization>(p => p.ResourceProfessionalOrganizations)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .Property(p => p.PhotoFileName)
                .HasColumnName("photo_file_name")
                .IsOptional()
                .IsUnicode()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_photo_file_name", 8)));

            modelBuilder.Entity<Resource>()
                .Property(p => p.CvFileName)
                .HasColumnName("cv_file_name")
                .IsOptional()
                .IsUnicode()
                .HasMaxLength(255);

            modelBuilder.Entity<Resource>()
                .Property(p => p.Productivity)
                .HasColumnName("productivity")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.Rating)
                .HasColumnName("rating")
                .IsOptional()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_rating", 9)));

            modelBuilder.Entity<Resource>()
                .Property(p => p.LGBT)
                .HasColumnName("lgbt")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .Property(p => p.BusinessUnitId)
                .HasColumnName("business_unit_id")
                .IsOptional();

            modelBuilder.Entity<Resource>()
                .HasOptional<DropDownConfig>(p => p.BusinessUnit)
                .WithMany()
                .HasForeignKey(p => p.BusinessUnitId);

            modelBuilder.Entity<Resource>()
                 .Property(p => p.CubicalOrOfficeNumber)
                 .HasColumnName("cubical_or_office_number")
                 .IsOptional()
                 .HasMaxLength(100);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceBehaviouralDetail>(p => p.ResourceBehaviouralDetails)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceRecognition>(p => p.ResourceRecognitions)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourceIndustry>(p => p.Industry)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany<ResourcePracticeArea>(p => p.ResourcePracticeAreas)
                .WithOptional()
                .HasForeignKey(item => item.ResourceId);

            modelBuilder.Entity<Resource>()
                 .Property(p => p.Overview)
                 .HasColumnName("overview")
                 .IsOptional();

            modelBuilder.Entity<Resource>()
                 .HasMany<ResourceInsurance>(p => p.ResourceInsurances)
                 .WithOptional()
                 .HasForeignKey(item => item.InsuranceTypeId);

            modelBuilder.Entity<Resource>()
              .HasMany<ResourceTimeoffBenefit>(p => p.ResourceTimeoffBenefits)
              .WithOptional()
              .HasForeignKey(item => item.TimeoffTypeId);

            return modelBuilder;
        }
    }
}
