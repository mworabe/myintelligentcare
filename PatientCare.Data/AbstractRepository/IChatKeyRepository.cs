﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IChatKeyRepository : IRepository<ChatKey>
    {
    }
}
