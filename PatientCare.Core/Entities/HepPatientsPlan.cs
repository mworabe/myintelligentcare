﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Hep Patient Plan.
    /// </summary>
    public class HepPatientsPlan : Entity
    {
        public long? HepId { get; set; }
        public virtual HepMaster Hep { get; set; }

        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public string HepPlan { get; set; }
    }
}
