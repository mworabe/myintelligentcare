﻿using System;
using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.ResourceCertificates
{
    [DataContract]
    public class ResourceCertificateResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public PicklistResponse Certificate { get; set; }

        [DataMember]
        public DateTime Certified { get; set; }

        [DataMember]
        public long ResourceId { get; set; }

        [DataMember]
        public DateTime? ExpirationDate { get; set; }

        [DataMember]
        public long? CertificateStateId { get; set; }
        [DataMember]
        public PicklistResponse CertificateState { get; set; }

        [DataMember]
        public DateTime? IssueDate { get; set; }
    }
}