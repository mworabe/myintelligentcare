﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("entryTypeService", ["$resource", "toaster", function ($resource, toaster) {
      var resource = $resource("api/entry-type/:controller/:id",
        null, //parameters default
            {
                'update': { method: "PUT" },
                'query': { method: "GET", params: {}, isArray: false },
                'createTimesheet': { method: "PUT", params: { controller: "create-timesheet" } },
            });

      var entryTypeENUM = [
                            { title: "Daily", value: "Daily" },
                            { title: "Weekly", value: "Weekly" },
                            { title: "BiMonthly", value: "BiMonthly" },
                            { title: "Monthly", value: "Monthly" },
      ];


      var onEntryConfigButtonClick = function (row) {
          console.info(row.id);
          if (!row.isTimesheetCreated) {
              resource.createTimesheet({ id: row.id }, row, function (success) {
                  toaster.pop("success", "", "Timesheet for " + row.name + " is created successfully.");
                  console.info(success)
              }, function (error) {
                  toaster.pop("success", "", "Unablt to create Timesheet for " + row.name + ", please try again later.");
                  console.info(error)
              });
          } else {
              toaster.pop("warning", "", "Timesheet for " + row.name + " is already created.");
          }
      }

      resource.getTableOption = {
          pageName: "Entry Type",
          hideSearch: true,
          hideBulkEdit: true,
          cols: function () {
              return [
                  { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                  { field: "name", title: "Description", sortable: "name", show: true, dataType: "text", required: true },
                  { field: "entryPeriod", title: "Entry Type", sortable: "entryPeriod", show: true, viewProperty: "title", valueProperty: "value", dataTypeView: "ui-select", dataType: "ui-select", list: entryTypeENUM, required: true },
                  {
                      field: "days",
                      title: "Days",
                      sortable: "days",
                      show: true,
                      dataType: "custom-template",
                      dataTypeView: "custom-template",
                      comparisionField: "entryPeriod",
                      comparisionValue: entryTypeENUM[0].value,
                      templateUrl: "Client/app/controllers/DropDownConfigurations/template/entry-type-days-template.html",
                      required: false
                  },
                  { field: "startDate", title: "Start Date", sortable: "startDate", show: true, dataTypeView: "date-time", dataType: "date-time", required: false },
                  { field: "endDate", title: "End Date", sortable: "endDate", show: true, dataTypeView: "date-time", dataType: "date-time", required: false },
                  { field: "advancedCreatePeriod", title: "Advanced Create Periods", sortable: "advancedCreatePeriod", show: true, dataType: "number", required: false },
                  { field: "lastEntryDateCreated", title: "Last Entry Date Created", sortable: "lastEntryDateCreated", show: true, dataTypeView: "date-time", dataType: "date-time", required: false },
                  { field: "action", title: ".", class: 'blank-cell', dataType: "command" },
              ];
          },
          moreExtraButtonList: [
              {
                  label: "Button1",
                  click: onEntryConfigButtonClick,
                  iconClass: "icon-timesheet-create",
                  /*function (row) {
                                  $state.go("activityEdit", { id: row.id });
                              },
                              iconClass: "icon-cog",
                          }*/
              },
          ],
      };
      return resource;
  }]);