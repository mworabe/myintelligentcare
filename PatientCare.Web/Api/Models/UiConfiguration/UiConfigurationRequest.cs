﻿using PatientCare.Core.Enums;
using System;
using PatientCare.Core.Entities;

namespace PatientCare.Web.Api.Models.UiConfiguration
{
    public class UiConfigurationRequest
    {
        public long Id { get; set; }
        public long? ResourceId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string KeyType { get; set;}
    }
}