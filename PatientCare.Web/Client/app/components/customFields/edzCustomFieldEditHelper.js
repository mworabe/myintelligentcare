﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//

"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("edzCustomFieldEditHelper", [
        function () {


            function getPropertyByName(data, propertyName) {
                return _.find(data, { name: propertyName });
            }

            function generatePropertyByName(propertyName, customFieldId) {
                return {
                    id: 0,
                    value: undefined,
                    name: propertyName,
                    customFieldId: customFieldId
                }
            }

            return {
                getPropertyByName: getPropertyByName,
                generatePropertyByName : generatePropertyByName,
                getOrCreatePropertyWithPush: function (data, propertyName, customFieldId) {
                    var property = getPropertyByName(data, propertyName);
                    if (!property) {
                        property = generatePropertyByName(propertyName, customFieldId);
                        data.push(property);
                    }
                    return property;

                }

            };

        }
    ]);