﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("trackedTableCell", [function () {
            return {
                restrict: "A",
                priority: -1,
                require: ["^trackedTableRow", "ngForm"],
                controller: ['$scope', '$parse', '$attrs', '$element', function ($scope, $parse, $attrs, $element) {
                    var cellFormCtrl = $element.controller("form");
                    var cellName = cellFormCtrl.$name;
                    var trackedTableRowCtrl = $element.controller("trackedTableRow");

                    if (trackedTableRowCtrl.isCellDirty(cellName)) {
                        cellFormCtrl.$setDirty();
                    } else {
                        cellFormCtrl.$setPristine();
                    }
                    // note: we don't have to force setting validaty as angular will run validations
                    // when we page back to a row that contains invalid data

                    $scope.$watch(function () {
                        return cellFormCtrl.$dirty;
                    }, function (newValue, oldValue) {
                        if (newValue === oldValue) return;

                        trackedTableRowCtrl.setCellDirty(cellName, newValue);
                    });

                    $scope.$watch(function () {
                        return cellFormCtrl.$invalid;
                    }, function (newValue, oldValue) {
                        if (newValue === oldValue) return;

                        trackedTableRowCtrl.setCellInvalid(cellName, newValue);
                    });
                }]
            };
        }
    ]);