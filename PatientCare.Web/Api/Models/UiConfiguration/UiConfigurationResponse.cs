﻿using System.Runtime.Serialization;
using PatientCare.Core.Entities;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.UiConfiguration
{
    [DataContract]
    public class UiConfigurationResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public PicklistResponse Resource { get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string KeyType { get; set; }

    }
}