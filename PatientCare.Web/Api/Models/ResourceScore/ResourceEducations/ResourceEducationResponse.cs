﻿
using System;
using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.ResourceEducations
{
    [DataContract]
    public class ResourceEducationResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Subject { get; set; }
        
        [DataMember]
        public string NameOfSchool { get; set; }

        [DataMember]
        public DateTime? DateObtained { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }
        
        [DataMember]
        public long ResourceId { get; set; }

        [DataMember]
        public long? ResourceDesignationId { get; set; }
        [DataMember]
        public PicklistResponse ResourceDesignation { get; set; }
    }
}