namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedExerciseMediaTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.exercise_medias",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    exercise_id = c.Long(),
                    original_name = c.String(),
                    media_type = c.Int(),
                    name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.exercises", t => t.exercise_id)
                .Index(t => t.exercise_id);

            DropColumn("dbo.exercises", "sets");
        }

        public override void Down()
        {
            AddColumn("dbo.exercises", "sets", c => c.Int());
            DropForeignKey("dbo.exercise_medias", "exercise_id", "dbo.exercises");
            DropIndex("dbo.exercise_medias", new[] { "exercise_id" });
            DropTable("dbo.exercise_medias");
        }
    }
}
