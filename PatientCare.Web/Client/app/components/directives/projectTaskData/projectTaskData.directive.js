﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("ngProjectTaskData", ["uuid2", "CreatorSearchFunction", "Resource", "ProjectTask", "$q",
        function (uuid2, creatorSearchFunction, resource, projectTask, $q) {
            return {
                templateUrl: "Client/app/components/directives/projectTaskData/projectTaskData.html",
                restrict: "E",
                replace: true,
                scope: {
                    model: "=ngModel",
                    option: "=?ngOption",
                    addItem_Click: "=addItemClick",
                    deleteItem_Click: "=deleteItemClick"
                },
                link: function (scope) {

                    function generateList() {

                        scope.priorityEnum = [
                            "Low",
                            "Medium",
                            "High"
                        ];
                        scope.statusEnum = [
                            "Created",
                            "Started",
                            "Finished",
                            "Paused",
                            "Resumed",
                            "Canceled"
                        ];
                        scope.ganttTypeList = [
                            {
                                name: "Finish->Start",
                                value: "FS"
                            },
                            {
                                name: "Finish->Finish",
                                value: "FF"
                            },
                            {
                                name: "Start->Start",
                                value: "SS"
                            },
                            {
                                name: "Start->Finish",
                                value: "SF"
                            }

                        ];
                    }

                    scope.autocompleteMinLength = 0;
                    scope.option = scope.option || {};

                    function getValueInAnguAutocomplete(selected) {
                        return (selected && selected.originalObject) ? selected.originalObject.id : null;
                    }
                    scope.angucompleteReporterSelect = function (select) {
                        scope.model.reporterId = getValueInAnguAutocomplete(select);
                    }
                    scope.angucompleteResourceSelect = function (select) {
                        scope.model.resourceId = getValueInAnguAutocomplete(select);
                    }
                    scope.angucompletePredecessorSelected = function (select) {
                        scope.model.predecessorId = getValueInAnguAutocomplete(select);
                        if (!scope.model.predecessorId) {
                            scope.model.ganttType = null;
                            scope.model.lag = null;
                        }
                    }
                    scope.generateId = uuid2.newguid;

                    creatorSearchFunction(scope, "searchResource", resource, "firstName", { showInactive: true });
                    creatorSearchFunction(scope, "searchTasks", projectTask);


                    scope.decorateSearchTask = function (search) {
                        var defer = $q.defer();
                        scope.searchTasks(search).then(function (data) {

                            if(scope.model.id  > 0)
                            _.remove(data, function (item) {
                                return item.id === scope.model.id;
                            });
                            defer.resolve(data);
                        });
                        return defer.promise;
                    }
                    generateList();
                }
            };
        }
    ]);