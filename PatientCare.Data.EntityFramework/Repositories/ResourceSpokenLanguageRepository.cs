﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class ResourceSpokenLanguageRepository : RepositoryBase<ResourceSpokenLanguage>, IResourceSpokenLanguageRepository
    {
        public ResourceSpokenLanguageRepository(IAmbientDataContextLocator dataContextLocator)
            : base(dataContextLocator)
        {
        }
    }
}
