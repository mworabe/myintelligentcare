﻿using PatientCare.Core.Enums;
using System;

namespace PatientCare.Web.Api.Models.Calenders
{
    public class CalenderRequest
    {
        public string CalenderType { get; set; }
        public string EventType { get; set; }
        public long? Id { get; set; }
        public string ResourceName { get; set; }
        public string SupervisorName { get; set; }
        public string LeaveType { get; set; }
        public long? LeaveTypeId { get; set; }
        public string CommentFromSupervisor { get; set; }
        public string IsPaidType { get; set; }
        public long? ResourceId { get; set; }
        public long? SupervisorId { get; set; }
        public string Reason { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public int TakenLeaves { get; set; }
        public int RemainingLeave { get; set; }
        public bool? AllDay { get; set; }
    }
}