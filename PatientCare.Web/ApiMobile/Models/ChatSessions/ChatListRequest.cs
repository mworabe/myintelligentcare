﻿using PatientCare.Web.Api.Models;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.ChatSessions
{
    public class ChatListRequest : ListRequest
    {
        [DataMember(Name = "fromUserId")]
        public string FromUserId { get; set; }

        [DataMember(Name = "toUserId")]
        public string ToUserId { get; set; }
    }
}