﻿using System;
using System.Linq;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.ResourceCertificates;
using PatientCare.Web.Api.Models.ResourceCriminalRecords;
using PatientCare.Web.Api.Models.ResourceEducations;
using PatientCare.Web.Api.Models.ResourcePreviousEmployments;
using PatientCare.Web.Api.Models.Resources;
using PatientCare.Web.Api.Models.ResourceSkills;
using PatientCare.Web.Api.Models.ResourceSpokenLanguages;
using PatientCare.Web.Api.Models.ResourceTravels;
using PatientCare.Web.Services.Excel;
using PatientCare.Web.Services.FileStorage;
using EmitMapper;
using EmitMapper.MappingConfiguration;
using PatientCare.Web.Api.Models.ResourceWorkExperience;
using PatientCare.Web.Api.Models.ResourceProfessionalOrganizations;
using PatientCare.Web.Api.Models.ResourceBehaviouralDetails;
using PatientCare.Web.Api.Models.ResourceRecognitions;
using PatientCare.Web.Api.Models.ResourceIndustry;
using PatientCare.Web.Api.Models.ResourcePracticeAreas;
using PatientCare.Web.Api.Models.ResourceInsurances;
using PatientCare.Web.Api.Models.ResourceTimeoffBenefits;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Resource, ResourceResponse>(new ExtendedMapConfig<Resource, ResourceResponse>()
                    .ForMember(x => x.PhotoFileNameUrl, y => FileUploadHelpers.GetStorageUrlPhoto(y.PhotoFileName, FileUploadHelpers.ResourceFolderNameTemplate))
                    .ForMember(x => x.CvFileNameUrl, y => FileUploadHelpers.GetStorageUrl(y.CvFileName, ""))
                    .ForMember(x => x.Productivity, y => Math.Max(Math.Min(y.Productivity ?? 0, 10.0), 0))
                ));

            var newResSpokenLanguageMapper =
               ObjectMapperManager.DefaultInstance.GetMapper<ResourceSpokenLanguageResponse, ResourceSpokenLanguage>(
                   new DefaultMapConfig());

            var newResResourceCustomFieldValue =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCustomFieldValueResponse, ResourceCustomFieldValue>(
                    new DefaultMapConfig());

            var newResResourceEducationMapper =
               ObjectMapperManager.DefaultInstance.GetMapper<ResourceEducationResponse, ResourceEducation>(
                   new DefaultMapConfig());

            var newResResourceWorkExperienceMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkExperienceResponse, ResourceWorkExperience>(
                    new DefaultMapConfig());

            var newResResourceProfessionalOrganizationMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceProfessionalOrganizationResponse, ResourceProfessionalOrganization>(
                    new DefaultMapConfig());

            var newResResourceTravelMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTravelResponse, ResourceTravel>(
                    new DefaultMapConfig());

            var newResResourceBehaviouralDetailMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceBehaviouralDetailResponse, ResourceBehaviouralDetail>(
                    new DefaultMapConfig());

            var newResResourceRecognitionMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceRecognitionResponse, ResourceRecognition>(
                    new DefaultMapConfig());

            var newResResourceIndustryMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceIndustryResponse, ResourceIndustry>(
                    new DefaultMapConfig());

            var newResResourcePracticeAreaMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePracticeAreaResponse, ResourcePracticeArea>(
                    new DefaultMapConfig());

            var newResResourceSkillMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSkillResponse, ResourceSkill>(
                    new DefaultMapConfig());

            var newResResourceCertificateMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCertificateResponse, ResourceCertificate>(
                    new DefaultMapConfig());

            var newResResourcePreviousEmploymentMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePreviousEmploymentResponse, ResourcePreviousEmployment>(
                    new DefaultMapConfig());

            var newResResourceCriminalRecordMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCriminalRecordResponse, ResourceCriminalRecord>(
                    new DefaultMapConfig());

            var newResResourceInsuranceMapper =
            ObjectMapperManager.DefaultInstance.GetMapper<ResourceInsuranceResponse, ResourceInsurance>(
                new DefaultMapConfig());

            var newResResourceTimeoffBenefitMapper =
            ObjectMapperManager.DefaultInstance.GetMapper<ResourceTimeoffBenefitResponse, ResourceTimeoffBenefit>(
                new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
             ObjectMapperManager.DefaultInstance.GetMapper<ResourceResponse, Resource>
             (new ExtendedMapConfig<ResourceResponse, Resource>()
                .ForMember(x => x.SpokenLanguages, y => y.SpokenLanguages != null ? y.SpokenLanguages.Select(i => newResSpokenLanguageMapper.Map(i)).ToList() : null)
                .ForMember(x => x.CustomFieldValues, y => y.CustomFieldValues != null ? y.CustomFieldValues.Select(i => newResResourceCustomFieldValue.Map(i)).ToList() : null)
                .ForMember(x => x.Educations, y => y.Educations != null ? y.Educations.Select(i => newResResourceEducationMapper.Map(i)).ToList() : null)
                .ForMember(x => x.ResourceWorkExperiences, y => y.ResourceWorkExperiences != null ? y.ResourceWorkExperiences.Select(i => newResResourceWorkExperienceMapper.Map(i)).ToList() : null)
                .ForMember(x => x.ResourceProfessionalOrganizations, y => y.ResourceProfessionalOrganizations != null ? y.ResourceProfessionalOrganizations.Select(i => newResResourceProfessionalOrganizationMapper.Map(i)).ToList() : null)
                .ForMember(x => x.Travels, y => y.Travels != null ? y.Travels.Select(i => newResResourceTravelMapper.Map(i)).ToList() : null)
                .ForMember(x => x.ResourceBehaviouralDetails, y => y.ResourceBehaviouralDetails != null ? y.ResourceBehaviouralDetails.Select(i => newResResourceBehaviouralDetailMapper.Map(i)).ToList() : null)
                .ForMember(x => x.ResourceRecognitions, y => y.ResourceRecognitions != null ? y.ResourceRecognitions.Select(i => newResResourceRecognitionMapper.Map(i)).ToList() : null)
                .ForMember(x => x.Industry, y => y.Industry != null ? y.Industry.Select(i => newResResourceIndustryMapper.Map(i)).ToList() : null)
                .ForMember(x => x.ResourcePracticeAreas, y => y.ResourcePracticeAreas != null ? y.ResourcePracticeAreas.Select(i => newResResourcePracticeAreaMapper.Map(i)).ToList() : null)
                .ForMember(x => x.Skills, y => y.Skills != null ? y.Skills.Select(i => newResResourceSkillMapper.Map(i)).ToList() : null)
                .ForMember(x => x.Certificates, y => y.Certificates != null ? y.Certificates.Select(i => newResResourceCertificateMapper.Map(i)).ToList() : null)
                .ForMember(x => x.CriminalRecords, y => y.CriminalRecords != null ? y.CriminalRecords.Select(i => newResResourceCriminalRecordMapper.Map(i)).ToList() : null)
                .ForMember(x => x.PreviousEmployments, y => y.PreviousEmployments != null ? y.PreviousEmployments.Select(i => newResResourcePreviousEmploymentMapper.Map(i)).ToList() : null)
                .ForMember(x => x.ResourceInsurances, y => y.ResourceInsurances != null ? y.ResourceInsurances.Select(i => newResResourceInsuranceMapper.Map(i)).ToList() : null)
                .ForMember(x => x.ResourceTimeoffBenefits, y => y.ResourceTimeoffBenefits != null ? y.ResourceTimeoffBenefits.Select(i => newResResourceTimeoffBenefitMapper.Map(i)).ToList() : null)
             ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Resource, PicklistResponse>(new ExtendedMapConfig<Resource, PicklistResponse>()
                     .ForMember(x => x.Name, y => string.Format("{0} {1}", y.LastName ?? string.Empty, y.FirstName ?? string.Empty))
                ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Resource[], ResourceResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Resource, ResourcePicklistResponse>
                    (new ExtendedMapConfig<Resource, ResourceResponse>()
                        .ForMember(x => x.PhotoFileNameUrl, y => FileUploadHelpers.GetStorageUrl(y.PhotoFileName, ""))
                    ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceHierarchicalRequest, ResourceHierarchicalResponse>
                    (new ExtendedMapConfig<ResourceHierarchicalRequest, ResourceHierarchicalResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceHierarchicalRequest[], ResourceHierarchicalResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCustomFieldValue[], ResourceCustomFieldValueResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Resource[], ResourcePicklistResponse[]>());

            var spokenLanguageMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSpokenLanguageRequest, ResourceSpokenLanguage>(
                    new DefaultMapConfig());

            var resourceEducationMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceEducationRequest, ResourceEducation>(
                    new DefaultMapConfig());

            var resourceWorkExperienceMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceWorkExperienceRequest, ResourceWorkExperience>(
                    new DefaultMapConfig());

            var resourceProfessionalOrganizationMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceProfessionalOrganizationRequest, ResourceProfessionalOrganization>(
                    new DefaultMapConfig());

            var resourceTravelMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceTravelRequest, ResourceTravel>(
                    new DefaultMapConfig());

            var resourceBehaviouralDetailMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceBehaviouralDetailRequest, ResourceBehaviouralDetail>(
                    new DefaultMapConfig());

            var resourceRecognitionMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceRecognitionRequest, ResourceRecognition>(
                    new DefaultMapConfig());

            var resourceIndustryMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceIndustryRequest, ResourceIndustry>(
                    new DefaultMapConfig());

            var resourcePracticeAreaMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePracticeAreaRequest, ResourcePracticeArea>(
                    new DefaultMapConfig());

            var resourceSkillMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSkillRequest, ResourceSkill>(
                    new DefaultMapConfig());

            var resourceCertificateMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCertificateRequest, ResourceCertificate>(
                    new DefaultMapConfig());

            var resourcePreviousEmploymentMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourcePreviousEmploymentRequest, ResourcePreviousEmployment>(
                    new DefaultMapConfig());

            var resourceCriminalRecordMapper =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCriminalRecordRequest, ResourceCriminalRecord>(
                    new DefaultMapConfig());

            var resourceCustomFieldValue =
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCustomFieldValueRequest, ResourceCustomFieldValue>(
                    new DefaultMapConfig());

            var resourceInsuranceMapper =
              ObjectMapperManager.DefaultInstance.GetMapper<ResourceInsuranceRequest, ResourceInsurance>(
                  new DefaultMapConfig());

            var resourceTimeoffBenefitMapper =
               ObjectMapperManager.DefaultInstance.GetMapper<ResourceTimeoffBenefitRequest, ResourceTimeoffBenefit>(
                   new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceRequest, Resource>(
                    new ExtendedMapConfig<ResourceRequest, Resource>()
                    .ForMember(x => x.SpokenLanguages, y => y.SpokenLanguages != null ? y.SpokenLanguages.Select(i => spokenLanguageMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.Educations, y => y.Educations != null ? y.Educations.Select(i => resourceEducationMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.ResourceWorkExperiences, y => y.ResourceWorkExperiences != null ? y.ResourceWorkExperiences.Select(i => resourceWorkExperienceMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.ResourceProfessionalOrganizations, y => y.ResourceProfessionalOrganizations != null ? y.ResourceProfessionalOrganizations.Select(i => resourceProfessionalOrganizationMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.Travels, y => y.Travels != null ? y.Travels.Select(i => resourceTravelMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.ResourceBehaviouralDetails, y => y.ResourceBehaviouralDetails != null ? y.ResourceBehaviouralDetails.Select(i => resourceBehaviouralDetailMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.ResourceRecognitions, y => y.ResourceRecognitions != null ? y.ResourceRecognitions.Select(i => resourceRecognitionMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.Industry, y => y.Industry != null ? y.Industry.Select(i => resourceIndustryMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.ResourcePracticeAreas, y => y.ResourcePracticeAreas != null ? y.ResourcePracticeAreas.Select(i => resourcePracticeAreaMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.Skills, y => y.Skills != null ? y.Skills.Select(i => resourceSkillMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.Certificates, y => y.Certificates != null ? y.Certificates.Select(i => resourceCertificateMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.CriminalRecords, y => y.CriminalRecords != null ? y.CriminalRecords.Select(i => resourceCriminalRecordMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.PreviousEmployments, y => y.PreviousEmployments != null ? y.PreviousEmployments.Select(i => resourcePreviousEmploymentMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.CustomFieldValues, y => y.CustomFieldValues != null ? y.CustomFieldValues.Select(i => resourceCustomFieldValue.Map(i)).ToList() : null)
                    .ForMember(x => x.ResourceInsurances, y => y.ResourceInsurances != null ? y.ResourceInsurances.Select(i => resourceInsuranceMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.ResourceTimeoffBenefits, y => y.ResourceTimeoffBenefits != null ? y.ResourceTimeoffBenefits.Select(i => resourceTimeoffBenefitMapper.Map(i)).ToList() : null)
                    ));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceImportDto, Resource>(
                    new ExtendedMapConfig<ResourceRequest, Resource>()
                    .Ignore(x => x.Country)
                    .Ignore(x => x.Division)
                    .Ignore(x => x.Department)
                    .Ignore(x => x.ResourceTeam)
                    .Ignore(x => x.JobTitle)                 
                    .Ignore(x => x.City)
                    .Ignore(x => x.State)
                    .Ignore(x => x.ArmedForcesCountry)));

            return mapperProvider;
        }
    }
}