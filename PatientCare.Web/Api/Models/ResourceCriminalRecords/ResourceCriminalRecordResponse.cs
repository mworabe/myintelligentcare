﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceCriminalRecords
{
    [DataContract]
    public class ResourceCriminalRecordResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public DateTime DateOfOffense { get; set; }

        [DataMember]
        public string LocationOfOffense { get; set; }

        [DataMember]
        public string Offense { get; set; }

        [DataMember]
        public string PenaltyOrDisposition { get; set; }

        [DataMember]
        public long ResourceId { get; set; }
    }
}