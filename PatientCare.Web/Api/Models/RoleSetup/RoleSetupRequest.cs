﻿using PatientCare.Core.Services.RoleSetupService;

namespace PatientCare.Web.Api.Models.RoleSetup
{
    public class RoleSetupRequest
    {

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public long? DashboardSetupId { get; set; }

        public AccessRights AccessRights { get; set; }
    }
}