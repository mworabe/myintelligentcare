﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ForgotPasswordCtrl", [
        "toaster", "$http",
        function (toaster, $http) {
            var self = this;
            self.model = {};
            self.model.userName = null;
            var FORGOT_PASSWORD_URL = "api/forgotPassword";

            function initialData() {
                self.submitted = false;
            }

            function activate() {
                initialData();
            }
            self.submitQuery = function (form) {
                if (form.$invalid)
                    return;
                $http.post(FORGOT_PASSWORD_URL, { username: self.model.userName })
                    .success(function (res) {
                        toaster.pop(res.responseType, "", res.responseMessage);
                    })
                    .error(function (res) {
                        toaster.pop(res.responseType, "", res.responseMessage);
                    });
            }

            activate();
        }
    ]);