﻿using PatientCare.Web.Api.Models.HepDetails;
using PatientCare.Web.Api.Models.HepGroups;
using PatientCare.Web.Api.Models.PatientCases;
using PatientCare.Web.Api.Models.Patients;
using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.HepMasters
{
    [DataContract]
    public class HepMasterGroupResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public PatientAutoCompleteResponse Patient { get; set; }

        [DataMember]
        public long? OwnerId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Owner { get; set; }

        [DataMember]
        public long? PatientCaseId { get; set; }

        [DataMember]
        public PatientCaseAutoCompleteResponse PatientCase { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public DateTime? Modified { get; set; }

        [DataMember]
        public bool? IsSaveAsDraft { get; set; }

        [DataMember]
        public HepDetailResponse[] HepDetails { get; set; }

        [DataMember]
        public HepGroupResponse[] HepGroups { get; set; }
        
    }
}