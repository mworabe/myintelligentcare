﻿"use strict";
var EOPApp = angular.module("EDZoutstaffingPortalApp")
EOPApp.config(function ($stateProvider) {
    $stateProvider
        .state("assignment", {
            url: "/assignment",
            templateUrl: "Client/app/controllers/assignment/assignement-list-view.html",
            controller: "AssignmentController as AssignmentController",
            data: {
                requireLogin: true
            }
        }).state("assignmentEdit", {
            url: "/assignment/edit/:id",
            params: { resourceId: null, },
            templateUrl: "Client/app/controllers/assignment/assignement-edit-view.html",
            controller: "AssignmentEditController as AssignmentEditCtrl",
            data: {
                requireLogin: true
            },
            resolve: {
                assignmentResource: ["$q", "$stateParams", "AssignmentService", "userTimeZoneFactory",
                    function loadAssignmentIntitalData($q, $stateParams, assignmentService, userTimeZoneFactory) {
                        function loadAssignment() {
                            var defer = $q.defer();
                            if (!$stateParams.id) {
                                defer.resolve();
                            } else {
                                assignmentService.get({ id: $stateParams.id }).$promise.then(function (response) {
                                    defer.resolve(response);
                                }, function (reason) {
                                    defer.resolve({
                                        errorCode: reason.status
                                    });
                                });
                            }
                            return defer.promise;
                        }

                        function loadConfigLayout() {

                            var defer = $q.defer();
                            userTimeZoneFactory.loadDefaultAssignmentConfigs(defer);
                            return defer.promise;
                            /*
                            customFieldValuesByAssignment

                            /*
                            var defer = $q.defer();
                            roleSetupService.resourceConfigLayout().$promise.then(function (response) {
                                defer.resolve(response);
                            });
                            return defer.promise;
                            */
                        }

                        return $q.all([loadAssignment()]) // Removed - loadTimeZone()
                              .then(function (resolutions) {
                                  return resolutions;
                              });
                    }]
            }
        });
});