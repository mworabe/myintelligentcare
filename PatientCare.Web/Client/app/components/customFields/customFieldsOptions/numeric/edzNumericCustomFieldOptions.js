"use strict";

(function edzNumericCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzNumericCustomFieldOptions", [
            "edzCustomFieldEditHelper",
            "CUSTOM_FIELD",
            edzNumericCustomFieldOptions
        ]);

    function edzNumericCustomFieldOptions(edzCustomFieldEditHelper,  CUSTOM_FIELD) {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/numeric/edzNumericCustomField.html",
            restrict: "E",
            scope: false,
            controller: ["$scope", function ($scope) {
                var option = $scope.option;

                var from = edzCustomFieldEditHelper.getPropertyByName(option, CUSTOM_FIELD.PROPERTY_NAME.NUMERIC.FROM);
                var to = edzCustomFieldEditHelper.getPropertyByName(option, CUSTOM_FIELD.PROPERTY_NAME.NUMERIC.TO);
                var numberOfDecimals = edzCustomFieldEditHelper.getPropertyByName(option, CUSTOM_FIELD.PROPERTY_NAME.NUMERIC.NUMBER_OF_DECIMALS);

                $scope.validation = {};
                $scope.validation.from = from.value || undefined;
                $scope.validation.to = to.value || undefined;
                $scope.validation.numberOfDecimals = numberOfDecimals.value || undefined;
                $scope.validation.require = $scope.require;

                function loadData() {

                }

                function activate() {
                    loadData();
                }

                activate();
            }]
        };
    }
})();
