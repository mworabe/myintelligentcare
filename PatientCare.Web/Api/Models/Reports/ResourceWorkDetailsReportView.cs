﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Reports
{
    [DataContract]
    public class ResourceWorkDetailsReportView
    {
        [DataMember]
        public string EmployeeId { get; set; }
        [DataMember]
        public string ResourceName { get; set; }
        [DataMember]
        public string Division { get; set; }
        [DataMember]
        public string Department { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public string Education { get; set; }
        [DataMember]
        public string Skills { get; set; }
        [DataMember]
        public string Certifications { get; set; }
        [DataMember]
        public string WorkDetails { get; set; }
        [DataMember]
        public string Languages { get; set; }
    }
}