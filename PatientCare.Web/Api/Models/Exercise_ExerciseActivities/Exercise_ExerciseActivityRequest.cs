﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Models.Exercise_ExerciseActivities
{
    public class Exercise_ExerciseActivityRequest 
    {
        public long Id { get; set; }    
        public long? activity_Id { get; set; }
        public long? exercise_Id { get; set; }
    }   
}