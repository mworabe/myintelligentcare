﻿namespace PatientCare.Core.Services.Dashboards
{
    public class ExternalResourcesByCompanyModel
    {
        public string Key { get; set; }
        public string Color { get; set; }
        public ValueDataExternalResourcesByCompanyModel[] Values { get; set; }
    }
    public class ValueDataExternalResourcesByCompanyModel
    {
        public string Label { get; set; }
        public long? Value { get; set; }
        public long? CompanyId { get; set; }
        public long? ResourceTypeId { get; set; }
    }
}
