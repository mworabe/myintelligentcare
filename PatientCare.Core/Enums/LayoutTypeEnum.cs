﻿namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Layout Type Enum.
    /// </summary>
    public enum LayoutTypeEnum
    {
        Resource = 1
    }
}
