﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceCriminalRecords;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceCriminalRecordMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceCriminalRecordRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCriminalRecord, ResourceCriminalRecordResponse>(new ExtendedMapConfig<ResourceCriminalRecord, ResourceCriminalRecordResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCriminalRecord[], ResourceCriminalRecordResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceCriminalRecordRequest, ResourceCriminalRecord>(new ExtendedMapConfig<ResourceCriminalRecordRequest, ResourceCriminalRecord>()));

            return mapperProvider;
        }
    }
}