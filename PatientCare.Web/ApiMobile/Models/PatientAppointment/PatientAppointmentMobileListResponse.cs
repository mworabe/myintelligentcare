﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.ClinicLocations;
using PatientCare.Web.Api.Models.Clinics;
using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.PatientAppointment
{
    [DataContract]
    public class PatientAppointmentMobileListResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Patient { get; set; }

        [DataMember]
        public DateTime? AppointmentDate { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public long? ClinicLocationId { get; set; }

        [DataMember]
        public ClinicLocationAutoCompleteResponse ClinicLocation { get; set; }

        [DataMember]
        public long? AppointmentTypeId { get; set; }

        [DataMember]
        public PicklistDefaultResponse AppointmentType { get; set; }

        [DataMember]
        public long? ClinicianId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Clinician { get; set; }

        [DataMember]
        public long? FacilityId { get; set; }

        [DataMember]
        public ClinicAutoCompleteResponse Facility { get; set; }

        [DataMember]
        public string AdditionalDetail { get; set; }

        [DataMember]
        public bool? IsApproved { get; set; }

        [DataMember]
        public AppointmentStatusEnum? AppointmentStatus { get; set; }

        [DataMember]
        public bool? IsMobile { get; set; }

        [DataMember]
        public bool? IsReSchedule { get; set; }
    }
}