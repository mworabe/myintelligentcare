namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldInHepPatientPlan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hep_patient_plan", "patient_id", c => c.Long());
            CreateIndex("dbo.hep_patient_plan", "patient_id");
            AddForeignKey("dbo.hep_patient_plan", "patient_id", "dbo.patients", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.hep_patient_plan", "patient_id", "dbo.patients");
            DropIndex("dbo.hep_patient_plan", new[] { "patient_id" });
            DropColumn("dbo.hep_patient_plan", "patient_id");
        }
    }
}
