﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('financeResourceItem', function ($compile) {
    return {
        scope: {
            removeItem: "&",
            item: "=data",
            index: "=",
            periodTypeList: '=',
            appForm: '='
        },
        templateUrl: "Client/app/components/directives/finance-resource-item/finance-resource-item.html",
        controller: ['$scope', function ($scope) {

            if (!$scope.item.periodType) {
                $scope.item.periodType = $scope.periodTypeList[0].value;
            }

            $scope.onJobTitleSelected = function (object) {
                if (object && object.originalObject) {
                    $scope.item.jobTitleId = object.originalObject.id;
                    $scope.item.hourlyReate = object.originalObject.rate;
                } else {
                    $scope.item.jobTitleId = null;
                }
            }
            $scope.isJobTitleFieldRequired = true;
        }]
    }
});

