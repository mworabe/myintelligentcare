﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("patientCaseService", [
        "$resource", "$state", "$injector", "$uibModal", "$filter", "$translate", "uuid2", "$rootScope",
            function ($resource, $state, $injector, $uibModal, $filter, $translate, uuid2, $rootScope) {
                var permissionWorker = null;
                $rootScope.appointmentOpenFormCounter = 0;

                function getPermissionWorker() {
                    if (!permissionWorker) {
                        permissionWorker = $injector.get("PermissionWorker");
                    }
                    return permissionWorker;
                };

                var resource = $resource("api/patientcases/:patientId/:controller/:id",
                    null, //parameters default
                    {
                        'savePatientCase': { method: 'post', params: { controller: "cases" } },
                        'updatePatientCase': { method: 'put', params: { controller: "cases" } },
                        'update': { method: "PUT" },
                        'query': { method: "GET", params: {}, isArray: false },
                        'getPatientCases': { method: "GET", params: { controller: "cases" }, isArray: false },
                        'getCase': { method: "GET", params: { controller: "cases" }, isArray: false },
                        'onPatientCaseDelete': { method: "delete", params: { controller: "cases" }, isArray: false },
                        'all': { method: "GET", params: {}, isArray: true, url: "api/PatientsCases/autocomplete?search=" },
                        'alll': { method: "GET", params: {}, isArray: true, url: "api/PatientsCases/autocompletee" },

                    });

                resource.reloadCommand = {};

                function getNameToEntity(entity) {
                    if (!entity) return "";
                    return entity.name;
                }

                var resourceCheckboxInactive = false;

                resource.getTableOption = {
                    hideTitle: true,
                    formatDelete: function (row) {
                        return row.name;
                    },
                    disabledAddButton: function () {
                        return !getPermissionWorker().canPatientInsert();
                    },
                    disabledDeleteButton: function () {
                        return getPermissionWorker().canPatientDelete();
                    },
                    disabledEditButton: function () {
                        return getPermissionWorker().canPatientUpdate();
                    },
                    getResourceCheckboxInactive: function () {
                        return resourceCheckboxInactive;
                    },
                    reset: function () {
                        resourceCheckboxInactive = false;
                    },
                    cols: function getPatientCaseCols() {
                        return [
                            {
                                field: "caseNo",
                                title: "Case ID",
                                sortable: "caseId",
                                show: true,
                                dataType: "text",
                                dataTypeView: "href",
                                buildUrl: function (row) {
                                    return $state.href("patientCaseEdit", { patientId: row.patientId, caseId: row.id });
                                },
                                customViewText: function (row) {
                                    return (row.caseNo || "Temp");
                                }
                            }, {
                                field: "injuryRegion",
                                title: "Injury Region",
                                sortable: "injuryRegion.name",
                                show: true,
                                dataType: "text",
                                customViewText: function (row) {
                                    return (row.injuryRegion && row.injuryRegion.name) ? row.injuryRegion.name : "";
                                }
                            }, {
                                field: "assignedPhysician",
                                title: "Clinician",
                                sortable: "assignedPhysician.name",
                                show: true,
                                dataType: "text",
                                customViewText: function (row) {
                                    return (row.assignedPhysician && row.assignedPhysician.name) ? row.assignedPhysician.name : "";
                                }
                            }, {
                                field: "referingPhysician",
                                title: "Referring Physician",
                                sortable: "referingPhysician.name",
                                show: true,
                                dataType: "text",
                                customViewText: function (row) {
                                    return (row.referingPhysician && row.referingPhysician.name) ? row.referingPhysician.name : "";
                                }
                            }, {
                                field: "startDate",
                                title: "Created Date",
                                sortable: "startDate",
                                show: true,
                                dataType: "text",
                                customViewText: function (row) {
                                    return moment(row.startDate).format("MM/DD/YYYY");
                                }
                            }, {
                                field: "caseStatus",
                                title: "case Status",
                                sortable: "caseStatus",
                                show: true,
                                dataType: "text",
                                getStatusSecondaryClass: function (row) {
                                    return "status-" + row.caseStatus;
                                },

                                customViewText: function (row) {
                                    switch (row.caseStatus) {
                                        case "New":
                                            return "New";
                                        case "Inprogress":
                                            return "In Progress";
                                        case "Discharged":
                                            return "Discharged";
                                        case "Inactive":
                                            return "In Active";
                                        case "Pending":
                                            return "Pending";
                                        default:
                                            return "";
                                    }
                                }
                            }, {
                                field: "action",
                                title: "Action",
                                class: "",
                                dataType: "command"
                            }
                        ];
                    },
                    hideAddButton: true,
                    showDropDownAddButton: true,
                    dropDownAddButtonOptions: [
                        {
                            title: "New Patient",
                            state: "patientsEdit",
                            onOptionClick: function () {
                                $state.go("patientsEdit");
                            }
                        }, {
                            title: "New appointment",
                            state: "add-appointment",
                            onOptionClick: function () {
                                var modalId = "appointment_" + uuid2.newguid().replace("-", "_").substring(0, 5);
                                console.info(modalId);
                                $rootScope.appointmentOpenFormCounter++;
                                $state.go("add-appointment", { modalId: "" + modalId, id: $rootScope.appointmentOpenFormCounter }, { reload: true });
                            }
                        }, {
                            title: "New Refral Source",
                            state: "add-refral-source",
                            onOptionClick: function () {
                                $state.go("add-refral-source");
                            }
                        },
                    ],
                    onAdd_Click: function () {
                        $state.go("patientCaseEdit");
                    },
                    onEdit_Click: function (row) {
                        $state.go("patientCaseEdit", { caseId: row.id, patientId: row.patientId });
                    },
                    pageName: "Patients",
                    menuButtons: [
                        {
                            click: function (row) {
                                resource.setInactive({ id: row.id }, {}, function () {
                                    resource.reloadCommand.reload();
                                });
                            },

                            iconClass: "icon icon-user-times"
                        }
                    ],
                    headerButtons: [],
                    headerLeftItems: []
                };
                return resource;
            }
    ]);