﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using System.Linq;
using System.Data.Entity;
using PatientCare.Web.ApiMobile.Models.Ratings;
using PatientCare.Web.ApiMobile.Models;
using PatientCare.Data.EntityFramework.Identity;

namespace PatientCare.Web.ApiMobile.Controllers
{
    /// <summary>
    /// This Controller is used to handle Rating operations.
    /// </summary>
    [Authorize(Roles = ApplicationUser.PatientRole)]
    public sealed class RatingsController : ApiBaseController
    {
        /// <summary>
        /// Description : Rating IMobileService.
        /// </summary>
        private readonly IMobileService _mobileService;
        /// <summary>
        /// Description : Rating IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        /// <summary>
        /// Description : Rating IExerciseRatingRepository.
        /// </summary>
        private readonly IExerciseRatingRepository _exerciseRatingRepository;
        /// <summary>
        /// Description : Rating IExerciseCommentRepository.
        /// </summary>
        private readonly IExerciseCommentRepository _exerciseCommentRepository;
        /// <summary>
        /// Description : Rating IAppFunctionalRatingRepository.
        /// </summary>
        private readonly IAppFunctionalRatingRepository _appFunctionalRatingRepository;

        /// <summary>
        /// Description : Initializes a new instance of the Rating Class.  
        /// </summary>
        /// <param name="mobileService">IMobile Service</param>
        /// <param name="dataContextScopeFactory">IDataContext Scope Factory</param>
        /// <param name="exerciseRatingRepository">IExercise Rating Repository</param>
        /// <param name="exerciseCommentRepository">IExercise Comment Repository</param>
        /// <param name="appFunctionalRatingRepository">IAppFunctional Rating Repository</param>
        public RatingsController
            (
                IMobileService mobileService,
                IDataContextScopeFactory dataContextScopeFactory,
                IExerciseRatingRepository exerciseRatingRepository,
                IExerciseCommentRepository exerciseCommentRepository,
                IAppFunctionalRatingRepository appFunctionalRatingRepository
            )
        {
            _mobileService = mobileService;
            _dataContextScopeFactory = dataContextScopeFactory;
            _exerciseRatingRepository = exerciseRatingRepository;
            _exerciseCommentRepository = exerciseCommentRepository;
            _appFunctionalRatingRepository = appFunctionalRatingRepository;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/ratings/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Update Exercise Comment.    
        /// </summary>
        /// <param name="exerciseCommentMobileRequest">Exercise Comment Mobile Request Model</param>
        /// <returns>Mobile Error Response Model</returns>
        [HttpPut]
        [Route("api/mobile/v1/exercise-comment")]
        public async Task<IHttpActionResult> exerciseComment([FromBody]ExerciseCommentMobileRequest exerciseCommentMobileRequest)
        {
            if (exerciseCommentMobileRequest.PatientId == null
               && exerciseCommentMobileRequest.PatientCaseId == null
               && exerciseCommentMobileRequest.HepDetailId == null)
            {
                ModelState.AddModelError("exerciseComment", "Patient is not Found");
                return BadRequest(ModelState);
            }

            MobileErrorResponse res = new MobileErrorResponse();

            try
            {
                var exerciseComment = new ExerciseComment();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbSet1 = ctx.Set<ExerciseComment>();
                    exerciseComment = await dbSet1.AsNoTracking()
                            .Where
                                (x => x.PatientId == exerciseCommentMobileRequest.PatientId
                                    && x.PatientCaseId == exerciseCommentMobileRequest.PatientCaseId
                                    && x.HepDetailId == exerciseCommentMobileRequest.HepDetailId
                                ).FirstOrDefaultAsync();
                }

                if (exerciseComment != null)
                {
                    exerciseComment.PatientId = exerciseCommentMobileRequest.PatientId;
                    exerciseComment.PatientCaseId = exerciseCommentMobileRequest.PatientCaseId;
                    exerciseComment.HepDetailId = exerciseCommentMobileRequest.HepDetailId;
                    exerciseComment.ExerciseId = exerciseCommentMobileRequest.ExerciseId;
                    exerciseComment.Comment = exerciseCommentMobileRequest.Comment;
                    exerciseComment.CommentDate = DateTime.UtcNow;

                    await _mobileService.InsertUpdateExerciseCommentAsync(exerciseComment);
                }
                else
                {
                    var commentDetail = Mapper.Map<ExerciseCommentMobileRequest, ExerciseComment>(exerciseCommentMobileRequest);
                    commentDetail.CommentDate = DateTime.UtcNow;
                    await _mobileService.InsertUpdateExerciseCommentAsync(commentDetail);
                }
                res.Type = ErrorTypeEnum.Success;
                res.Message = "Data Inserted.";
            }
            catch (Exception ex)
            {
                res.Type = ErrorTypeEnum.Error;
                res.Message = ex.Message.ToString();
            }
            return Ok(res);
        }

        /// <summary>
        /// Method Description : Using for Update Exercise Rating.    
        /// </summary>
        /// <param name="exerciseRatingMobileRequest">Exercise Rating Mobile Request Model</param>
        /// <returns>Mobile Error Response Model</returns>
        [HttpPut]
        [Route("api/mobile/v1/exercise-rating")]
        public async Task<IHttpActionResult> exerciseComment([FromBody]ExerciseRatingMobileRequest exerciseRatingMobileRequest)
        {
            if (exerciseRatingMobileRequest.PatientId == null
               && exerciseRatingMobileRequest.PatientCaseId == null
               && exerciseRatingMobileRequest.HepDetailId == null)
            {
                ModelState.AddModelError("ExerciseRating", "HEP is not Found");
                return BadRequest(ModelState);
            }

            MobileErrorResponse res = new MobileErrorResponse();

            try
            {
                var exerciseRating = new ExerciseRating();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbSet1 = ctx.Set<ExerciseRating>();
                    exerciseRating = await dbSet1.AsNoTracking()
                            .Where(x => x.PatientId == exerciseRatingMobileRequest.PatientId
                                    && x.PatientCaseId == exerciseRatingMobileRequest.PatientCaseId
                                    && x.HepDetailId == exerciseRatingMobileRequest.HepDetailId
                                  ).FirstOrDefaultAsync();
                }

                if (exerciseRating != null)
                {
                    exerciseRating.PatientId = exerciseRatingMobileRequest.PatientId;
                    exerciseRating.PatientCaseId = exerciseRatingMobileRequest.PatientCaseId;
                    exerciseRating.HepDetailId = exerciseRatingMobileRequest.HepDetailId;
                    exerciseRating.ExerciseId = exerciseRatingMobileRequest.ExerciseId;
                    exerciseRating.Rating = exerciseRatingMobileRequest.Rating;
                    exerciseRating.RatingDate = DateTime.UtcNow;

                    await _mobileService.InsertUpdateExerciseRatingAsync(exerciseRating);
                }
                else
                {
                    var ratingDetail = Mapper.Map<ExerciseRatingMobileRequest, ExerciseRating>(exerciseRatingMobileRequest);
                    ratingDetail.RatingDate = DateTime.UtcNow;
                    await _mobileService.InsertUpdateExerciseRatingAsync(ratingDetail);
                }
                res.Type = ErrorTypeEnum.Success;
                res.Message = "Data Inserted.";
            }
            catch (Exception ex)
            {
                res.Type = ErrorTypeEnum.Error;
                res.Message = ex.Message.ToString();
            }
            return Ok(res);
        }

        /// <summary>
        /// Method Description : Using for Update App Functional Ratingg.    
        /// </summary>
        /// <param name="appFunctionalRatingMobileRequest">App Functional Rating Mobile Request Model</param>
        /// <returns>Mobile Error Response Model</returns>
        [HttpPut]
        [Route("api/mobile/v1/app-functional-rating")]
        public async Task<IHttpActionResult> exerciseComment([FromBody]AppFunctionalRatingMobileRequest appFunctionalRatingMobileRequest)
        {
            if (appFunctionalRatingMobileRequest.PatientId == null
               && appFunctionalRatingMobileRequest.PatientCaseId == null)
            {
                ModelState.AddModelError("ExerciseRating", "Patient is not Found");
                return BadRequest(ModelState);
            }

            MobileErrorResponse res = new MobileErrorResponse();

            try
            {
                var appFunctionalRating = new AppFunctionalRating();
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                    var dbSet1 = ctx.Set<AppFunctionalRating>();
                    appFunctionalRating = await dbSet1.AsNoTracking()
                            .Where(x => x.PatientId == appFunctionalRatingMobileRequest.PatientId
                                    && x.PatientCaseId == appFunctionalRatingMobileRequest.PatientCaseId
                                  ).FirstOrDefaultAsync();
                }

                if (appFunctionalRating != null)
                {
                    appFunctionalRating.PatientId = appFunctionalRatingMobileRequest.PatientId;
                    appFunctionalRating.PatientCaseId = appFunctionalRatingMobileRequest.PatientCaseId;
                    appFunctionalRating.FunctionalRating = appFunctionalRatingMobileRequest.FunctionalRating;
                    appFunctionalRating.Comment = appFunctionalRatingMobileRequest.Comment;

                    await _mobileService.InsertUpdateAppFunctionalRatingAsync(appFunctionalRating);
                }
                else
                {
                    var appFunctionalRatingDetail = Mapper.Map<AppFunctionalRatingMobileRequest, AppFunctionalRating>(appFunctionalRatingMobileRequest);
                    await _mobileService.InsertUpdateAppFunctionalRatingAsync(appFunctionalRatingDetail);
                }
                res.Type = ErrorTypeEnum.Success;
                res.Message = "Data Inserted.";
            }
            catch (Exception ex)
            {
                res.Type = ErrorTypeEnum.Error;
                res.Message = ex.Message.ToString();
            }

            return Ok(res);
        }
    }
}