﻿using System;
using System.Collections.Generic;
using PatientCare.Core.Entities;
using PatientCare.Core.Entities.Abstract;


namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Insurance.
    /// </summary>
    public class ResourceInsurance : Entity
    {
        public long? ResourceId { get; set; }
        public virtual Resource Resource { get; set; }

        public long? InsuranceTypeId { get; set; }
        public virtual DropDownConfig InsuranceType { get; set; }
        
        public string Participating { get; set; }
        public string EmployeeCost { get; set; }
        public string EmployerContribution { get; set; }
    }
}
