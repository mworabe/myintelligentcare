﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceScoreFilters;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceScoreFilterMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceScoreFilterRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceScoreFilter, ResourceScoreFilterResponse>(new ExtendedMapConfig<ResourceScoreFilter, ResourceScoreFilterResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceScoreFilter[], ResourceScoreFilterResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceScoreFilterRequest, ResourceScoreFilter>(new ExtendedMapConfig<ResourceScoreFilterRequest, ResourceScoreFilter>()));

            return mapperProvider;
        }
    }
}