﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("GroupCntrl", [
		"$scope",
		'toaster',
        'dataItem',
        '$uibModalInstance',
		function ($scope, toaster, dataItem, $uibModalInstance) {
		    $scope.title = dataItem.title;

		    $scope.groupItem = dataItem.groupItem;
		    $scope.hepDetails = [];

		    if (dataItem.normalItems) {
		        angular.forEach(dataItem.normalItems, function (item) {
		            $scope.hepDetails.push(item);
		        })
		    }

		    if ($scope.groupItem.hepDetails) {
		        angular.forEach($scope.groupItem.hepDetails, function (item) {
		            item.isSelected = true;
		            $scope.hepDetails.push(item);
		        })
		    }

		     

			$scope.close = function () {
				$uibModalInstance.dismiss('');
			}

			function validate() {
			    if (!$scope.groupItem.name) {
			        toaster.pop("error", "", "Please enter groupname.")
			        return false;
			    }

			    var isItemSelcted = false;
			    for (var i = 0; i < $scope.hepDetails.length; i++) {
			        if ($scope.hepDetails[i].isSelected) {
			            isItemSelcted = true;
			            break;
			        }
			    }
			    if (!isItemSelcted) {
			        toaster.pop("error", "", "Please select at least one item.")
			        return false;
			    }

			    return true;
			}

			function onSuccess(result) {
			    $uibModalInstance.close(result);
			}

			$scope.save = function () {
			    if (validate()) {
			        var items = [];
			        var notSelectedItems = [];
			        for (var i = 0; i < $scope.hepDetails.length; i++) {
			            if ($scope.hepDetails[i].isSelected)
			                items.push($scope.hepDetails[i]);
			            else
			                notSelectedItems.push($scope.hepDetails[i]);
			        }
			        $scope.groupItem.hepDetails = items;
			        var obj = {
			            groupItem: $scope.groupItem,
			            notSelectedItems:notSelectedItems
			        }
			        onSuccess(obj);
			    }
			}


		}
    ]);