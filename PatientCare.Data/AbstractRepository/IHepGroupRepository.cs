﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IHepGroupRepository : IRepository<HepGroup>
    {
    }
}
