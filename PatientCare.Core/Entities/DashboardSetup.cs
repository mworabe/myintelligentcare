﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Dashboard Setup.
    /// </summary>
    public class DashboardSetup : NamedEntity
    {
        public string SetupJson { get; set; }
    }
}
