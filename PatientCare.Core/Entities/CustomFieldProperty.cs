﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Custom Field Property.
    /// </summary>
    public class CustomFieldProperty : NamedEntity
    {
        public long CustomFieldId { get; set; }
        public CustomField CustomField { get; set; }
        public string Value { get; set; }
    }
}
