﻿using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientCare.Core.Entities
{
    public class Exercise_injury : Entity
    {
        
        public long? injury_Id { get; set; }
        public virtual DropDownConfig injury { get; set; }

        
        public long? exercise_Id { get; set; }
        public virtual Exercise exercise { get; set; }
    }
}
