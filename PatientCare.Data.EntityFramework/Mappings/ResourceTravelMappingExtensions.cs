﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceTravelMappingExtensions
    {
        public static DbModelBuilder MapResourceTravel(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceTravel>().ToTable("resource_travels").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceTravel>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceTravel>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceTravel>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsRequired();

            modelBuilder.Entity<ResourceTravel>()
                .HasRequired<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceTravel>()
                .Property(p => p.VisaOrPermit)
                .HasColumnName("visa_or_permit")
                .IsOptional();

            modelBuilder.Entity<ResourceTravel>()
                .Property(p => p.CountryId)
                .HasColumnName("country_id")
                .IsOptional();

            modelBuilder.Entity<ResourceTravel>()
                .HasOptional<Country>(p => p.Country)
                .WithMany()
                .HasForeignKey(p => p.CountryId);

            modelBuilder.Entity<ResourceTravel>()
                .Property(p => p.TimeRemainingInDays)
                .HasColumnName("time_remaining_in_days")
                .IsOptional();

            modelBuilder.Entity<ResourceTravel>()
                .Property(p => p.SpecialConsiderations)
                .HasColumnName("special_considerations")
                .IsOptional();

            return modelBuilder;
        }
    }
}
