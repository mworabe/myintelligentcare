﻿using System;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Skills Controller operations.
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Skills")]
    public sealed class SkillsController : EdzPicklistApiController<Skill>
    {
        /// <summary>
        /// Description : In this class we pass Skills IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="picklistRepository">IPicklist Repository Skill</param>
        public SkillsController(IPicklistRepository<Skill> picklistRepository)
            : base(picklistRepository)
        {
            GetPicklistStatusCodeErrorFunc = () => GetPicklistStatusCodeError(Picklist.Skills);
        }

        protected override Uri GetPicklistLocation(long id)
        {
            return new Uri("/Skills/" + id, UriKind.Relative);
        }
    }
}