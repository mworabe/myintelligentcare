﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("ReferralDialogFactory", ["$uibModal", function ($uibModal) {

        return {
            showPhysicianModal: function (args, modalListener) {
                $uibModal.open({
                    backdrop: "static",
                    templateUrl: "Client/app/controllers/referral/templates/addPhysician-Modal.html",
                    controller: "ReferralPhysicianEditController as ReferralPhysicianEditController",
                    resolve: {
                        intialData: args
                    },
                }).result.then(modalListener);
            },
            //showMarketingModal: function () { }
        }


    }]);