namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamHoldUnitToDayinFrequencyTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hep_exercise_frequency", "day", c => c.Int());
            DropColumn("dbo.hep_exercise_frequency", "holds_unit");
        }
        
        public override void Down()
        {
            AddColumn("dbo.hep_exercise_frequency", "holds_unit", c => c.Int());
            DropColumn("dbo.hep_exercise_frequency", "day");
        }
    }
}
