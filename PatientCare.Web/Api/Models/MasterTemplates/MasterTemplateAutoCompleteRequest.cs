﻿namespace PatientCare.Web.Api.Models.MasterTemplates
{
    public class MasterTemplateAutoCompleteRequest
    {
        public string Search { get; set; }
        
        public long? OwnerId { get; set; }

        public string Name { get; set; }
    }
}