﻿using PatientCare.Web.Api.Models.MasterTemplateExercises;
using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.MasterTemplates
{
    [DataContract]
    public class MasterTemplateListResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? OwnerId { get; set; }

        [DataMember]
        public PicklistDefaultResponse Owner { get; set; }

        [DataMember]
        public bool? IsMaster { get; set; }

        [DataMember]
        public bool? IsFavorite { get; set; }

        [DataMember]
        public bool? IsPublic { get; set; }

        [DataMember]
        public MasterTemplateExerciseResponse[] TemplateExercises { get; set; }
    }
}