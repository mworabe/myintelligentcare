﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This Class is for App Functional Rating .
    /// </summary>
    public class AppFunctionalRating : Entity
    {
        public long? PatientId { get; set; }
        public virtual Patient Patient { get; set; }

        public long? PatientCaseId { get; set; }
        public virtual PatientCase PatientCase { get; set; }

        public int? FunctionalRating { get; set; }
        public string Comment { get; set; }
    }
}
