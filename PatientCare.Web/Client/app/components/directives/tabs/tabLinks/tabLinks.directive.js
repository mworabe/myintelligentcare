﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .directive("edzTabLinks", [function () {
            return {
                templateUrl: "Client/app/components/directives/tabs/tabLinks/tabLinks.html",
                restrict: "E",
                replace: false,
                controllerAs: "tabLinksCtrl",
                transclude: true,
                scope : true,
                bindToController: {
                    currentTab: "=edzCurrentTab"
                },
                controller: [function () {

                }]
            };
        }
    ]);