﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceScoreFilters
{
    [DataContract]
    public class ResourceScoreFilterResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string JsonFilter { get; set; }

        [DataMember]
        public DateTime Modified { get; set; }

        [DataMember]
        public string FilterType { get; set; }

    }
}