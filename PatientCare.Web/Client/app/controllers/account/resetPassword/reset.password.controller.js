﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ResetPasswordController", [
        "$location", "toaster", "$scope", "$http", "$state",
       function ($location, toaster, $scope, $http, $state) {
           var self = this;
           self.isToShowFields = false;
           self.resetPasswordURL = "api/ForgotPassword/reset";

           self.userId = $location.search();
           if ($location && !self.userId) {
               toaster.pop("error", "", "we are unable to reset password, please try again later.");
               //return;
           }


           self.isToShowFields = true;

           $scope.resetPasswordFormSubmit = function (form) {
               if (form.$invalid) {
                   toaster.pop("error", "", "All Fields are required");
                   return;
               }

               var model = {};
               model.userId = self.userId.id;
               model.newPassword = self.newPassword;

               $http.put(self.resetPasswordURL, model)
                   .success(function (res) {
                       toaster.pop(res.responseType, "", res.responseMessage);
                       $state.go("login");
                   })
                   .error(function (res) {
                       toaster.pop(res.responseType, "", res.responseMessage);
                   });

               /*
               changePasswordService.resetPassword({},
                   {
                       oldPassword: self.newPassword,
                       newPassword: self.newPassword,
                       confirmPassword: self.retypePassword
                   },
                   function (success) {
                       toaster.pop('success', "", "Your password has been updated successfully, please re-login using new passowrd.");
                       auth.logout();
                   },
                   function (error) {
                       toaster.pop('error', "", "Unable to change Password");
                   }
               );*/
           }

       }
    ]);