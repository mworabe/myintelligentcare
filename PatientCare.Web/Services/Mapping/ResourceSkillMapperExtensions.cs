﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceScore;
using PatientCare.Web.Api.Models.ResourceSkills;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceSkillMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceSkillRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSkill, ResourceSkillResponse>(new ExtendedMapConfig<ResourceSkill, ResourceSkillResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSkill[], ResourceSkillResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSkill[], PicklistResponseWithSkills[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSkillRequest, ResourceSkill>(new ExtendedMapConfig<ResourceSkillRequest, ResourceSkill>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSkillFilterRequest, ResourceSkill>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSkillFilterRequest[], ResourceSkill[]>());

            return mapperProvider;
        }
    }
}