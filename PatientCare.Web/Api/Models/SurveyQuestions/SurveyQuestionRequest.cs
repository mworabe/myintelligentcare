﻿namespace PatientCare.Web.Api.Models.SurveyQuestions
{
    public class SurveyQuestionRequest
    {
        public string Text { get; set; }

        public long CategoryId { get; set; }
    }
}