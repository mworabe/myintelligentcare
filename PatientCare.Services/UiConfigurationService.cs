﻿using System;
using System.Data;
using System.Threading.Tasks;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;
using PatientCare.Data.EntityFramework;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;

namespace PatientCare.Services
{
    /// <summary>
    /// This Service is used to handle ui configuration operations And implementing all methods which is initialize in IUiConfigurationServices.
    /// </summary>
    public class UiConfigurationService : IUiConfigurationServices
    {
        /// <summary>
        ///Description : Ui Configuration Service Field IDataContextScopeFactory
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        ///Description : Ui Configuration Service Field IUiConfigurationRepository
        /// </summary>
        private readonly IUiConfigurationRepository _uiconfigurationRepository;

        /// <summary>
        ///Description : Initializes a new instance of the Ui Configuration class.
        /// </summary>
        /// <param name="dataContextScopeFactory">DataContext Scope Factory</param>
        /// <param name="uiconfigurationRepository">Ui configuration Repository</param>
        public UiConfigurationService(
               IDataContextScopeFactory dataContextScopeFactory,
             IUiConfigurationRepository uiconfigurationRepository)
        {
            _dataContextScopeFactory = dataContextScopeFactory;
            _uiconfigurationRepository = uiconfigurationRepository;
        }

        /// <summary>
        /// Method Description : Using for Update UiConfiguration. 
        /// </summary>
        /// <param name="uiConfiguration">UiConfiguration Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> UpdateUiConfigurationAsync(UiConfiguration uiConfiguration)
        {
            if (uiConfiguration == null) throw new ArgumentNullException("uiConfigurations");

            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                var originuiconfiguration = _uiconfigurationRepository.Read(uiConfiguration.Id, new ReadOptions<UiConfiguration>().AsReadOnly());

                await _uiconfigurationRepository.UpdateAsync(uiConfiguration, UpdateOptions.Create<UiConfiguration>()
                .WithNotModified(p => p.Created));

            }
            return true;
        }

        /// <summary>
        /// Method Description : Using for Create new UiConfiguration. 
        /// </summary>
        /// <param name="uiconfiguration">UiConfiguration Entity</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> CreateUiConfigurationAsync(UiConfiguration uiconfiguration)
        {
            if (uiconfiguration == null) throw new ArgumentNullException("uiconfigurations");

            await _uiconfigurationRepository.CreateAsync(uiconfiguration, UpdateOptions.Create<UiConfiguration>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            return true;
        }

        /// <summary>
        /// Method Description : Using for Delete UiConfiguration data.
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Returns True/False</returns>
        public async Task<bool> DeleteUiConfigurationAsync(long id)
        {
            using (var dbContextScope = _dataContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadCommitted))
            {
                var uiconfiguration = _uiconfigurationRepository.Read(id);

                if (uiconfiguration != null)
                {
                    _uiconfigurationRepository.Delete(uiconfiguration);

                    await dbContextScope.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

    }
}
