// <auto-generated />
namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CreateCHatMemberTableAndAlterCHatSessionTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateCHatMemberTableAndAlterCHatSessionTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201704071411112_CreateCHatMemberTableAndAlterCHatSessionTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
