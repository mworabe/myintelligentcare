﻿using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.AppointmentTypes
{
    [DataContract]
    public class AppointmentTypesPicklistResponse
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Color { get; set; }
    }
}