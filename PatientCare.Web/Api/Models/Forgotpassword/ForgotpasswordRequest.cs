﻿using PatientCare.Core.Enums;
using System;

namespace PatientCare.Web.Api.Models.Forgotpassword
{
    public class ForgotpasswordRequest
    {
        public string Username  { get; set; }
        public string ResponseType { get; set; }
        public string Responsemessage { get; set; }
        public string UserId { get; set; }
        public string NewPassword { get; set; }
    }
}