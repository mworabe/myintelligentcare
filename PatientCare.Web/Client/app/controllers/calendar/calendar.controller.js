"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("CalendarCtrl", [
        "$scope", "$uibModal", "$stateParams", "calendarService", 'uiCalendarConfig', '$q',
        function ($scope, $uibModal, $stateParams, calendarService, uiCalendarConfig, $q) {

            var self = this;
            self.pageTitle = "My Calendar";
            self.calendarTitle = "Testing Title";
            self.modal = {};
            var tempEvents = [];

            var MY_TIME_OFF_ITEM_INDEX = 0;
            var MY_DIRECT_REPORT_TIME_OFF_ITEM_INDEX = 1;
            var MY_TASKS_INDEX = 2;
            var MY_DIRECT_REPORT_TASKS_ITEM_INDEX = 3;


            var MY_TASKS_CALENDAR_TYPE = "MyTask";
            var MY_TIME_OFF_CALENDAR_TYPE = "MyTimeOff";
            var MY_RESOURCES_TASKS_CALENDAR_TYPE = "ResourceTask";
            var MY_RESOURCES_TIME_OFF_CALENDAR_TYPE = "ResourceTimeOff";
            

            self.calendarTypes = [{
                id: "myTimeOff",
                name: "My Time Off",
                visible: true,
                isChecked: true,
                cssClass: 'my_timeoff_calendar_event'
            }, {
                id: "resourceTimeOFf",
                name: "Direct Report Time Off",
                visible: true,
                isChecked: true,
                cssClass: 'my_direct_report_timeoff_calendar_event'
            }, {
                id: "myTask",
                name: "My Task",
                visible: true,
                isChecked: true,
                cssClass: 'my_tasks_calendar_event'
            }, {
                id: "resourceTask",
                name: "Direct Report Tasks",
                visible: true,
                isChecked: true,
                cssClass: 'my_direct_report_tasks_calendar_event'
            }
            ];

            self.currentCalendar = self.calendarTypes[0];

            self.selectCurrentCalendar = function (selectedCalendar) {
                self.currentCalendar = selectedCalendar;
            }

            self.isCalendarSelected = function (selectedCalendar) {
                return self.currentCalendar === selectedCalendar;
            }

            $scope.myEvents = [];
            $scope.myResourceEvents = [];
            $scope.myTaskEvents = [];
            $scope.resourceTaskEvents = [];

            var loadMyEvent = function () {
                var defer = $q.defer();
                calendarService.query().$promise
               .then(function (response) {
                   var processedResult = processResultAndGroupEvents(response, MY_TIME_OFF_ITEM_INDEX);
                   $scope.myEvents = processResult(processedResult);
                   defer.resolve($scope.myEvents);
               }, function () {
                   defer.resolve([]);// this line is comment for the next and previous button data not loaded
               });
                return defer.promise;
            }

            var loadMyResourcesEvents = function () {
                var defer = $q.defer();
                calendarService.getMyResourceTimeOff().$promise
               .then(function (response) {
                   var processedResult = processResultAndGroupEvents(response, MY_DIRECT_REPORT_TIME_OFF_ITEM_INDEX);
                   $scope.myResourceEvents = processResult(processedResult);
                   defer.resolve($scope.myResourceEvents);

               }, function () {
                   defer.resolve([]); //this line is comment for the next and previous button data not loaded
               });
                return defer.promise;
            }

            var loadMyTaskEvent = function () {
                var defer = $q.defer();
                calendarService.getMyTaskDetails().$promise
               .then(function (response) {
                   var processedResult = processResultAndGroupEvents(response,MY_TASKS_INDEX);
                   $scope.myTaskEvents = processResult(processedResult);
                   defer.resolve($scope.myTaskEvents);

               }, function () {
                   defer.resolve([]);// this line is comment for the next and previous button data not loaded
               });
                return defer.promise;
            }


            var loadResourceTaskEvent = function () {
                var defer = $q.defer();
                calendarService.getResourceTaskDetails().$promise
               .then(function (response) {
                   var processedResult = processResultAndGroupEvents(response, MY_DIRECT_REPORT_TASKS_ITEM_INDEX);
                   $scope.resourceTaskEvents = processResult(processedResult);
                   defer.resolve($scope.resourceTaskEvents);
               }, function () {
                   defer.resolve([]);// this line is comment for the next and previous button data not loaded
               });
                return defer.promise;
            }

            function processResultAndGroupEvents(result, eventType) {
                if (!result || result.length == 0) return result;

                var processedEvents = [];

                for (var index = 0; index < result.length; index++) {

                    var event = result[index];
                    var startDateOfEvent = new Date(event.start);
                    var endDateOfEvent = new Date(event.end);

                    var current_date = startDateOfEvent;

               
                    while (startDateOfEvent.getTime() <= endDateOfEvent.getTime()) {

                        //get date as key and create events for date between start and end date of event
                        var dateKey = getFormatDate(current_date);
                        var eventOnDate = null;
                        //Find if event for this date is already exist.
                        for (var existItemIndex = 0; existItemIndex < processedEvents.length; existItemIndex++) {
                            var existItem = processedEvents[existItemIndex];
                            if (existItem.start == dateKey) {
                                eventOnDate = existItem;                                
                                break;
                            }
                        }

                        // Create event if alrady not created for the date
                        if (!eventOnDate) {
                            //add event for this date
                            eventOnDate = createCommonEvent(current_date,eventType);                         
                            processedEvents.push(eventOnDate);
                        }

                        // add existing item as a child of created common/dummy event
                        var childrenEvents = eventOnDate.childrenEvents;
                        childrenEvents.push(event);
                        current_date.setDate(current_date.getDate() + 1);
                        eventOnDate.childrenEvents = childrenEvents;

                       
                    }
                   
                }
                return processedEvents;
            }

            function createCommonEvent(date, eventTypeIndex) {
                var startDateString = getFormatDate(date);
                var endDateString =   startDateString;

                var eventTitle = "";
                var className = "";
                var calenderType = "";

                if (eventTypeIndex == MY_TIME_OFF_ITEM_INDEX)
                {
                    eventTitle = "My Time Off";
                    className = "calendar_event my_timeoff_calendar_event";
                    calenderType = MY_TIME_OFF_CALENDAR_TYPE;
                } else if (eventTypeIndex == MY_DIRECT_REPORT_TIME_OFF_ITEM_INDEX)
                {
                    eventTitle = "Direct Report Time Off";
                    className = "calendar_event my_direct_report_timeoff_calendar_event";
                    calenderType = MY_RESOURCES_TIME_OFF_CALENDAR_TYPE;
                } else if (eventTypeIndex == MY_TASKS_INDEX)
                {
                    eventTitle = "My Tasks";
                    className = "calendar_event my_tasks_calendar_event";
                    calenderType = MY_TASKS_CALENDAR_TYPE;
                } else if (eventTypeIndex == MY_DIRECT_REPORT_TASKS_ITEM_INDEX)
                {
                    eventTitle = "Direct Report Tasks";
                    className = "calendar_event my_direct_report_tasks_calendar_event";
                    calenderType = MY_RESOURCES_TASKS_CALENDAR_TYPE;
                }

                 
                return {
                    "calenderType": calenderType,
                    "id": 0,
                    "title": eventTitle,
                    "_untouch_title":eventTitle,
                    "start": startDateString,
                    "end": endDateString,
                    "className": className,
                    "childrenEvents":[],
                    "allDay": true
                };
            }

            function getFormatDate(date) {
                return date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
            };

            $scope.alertOnEventClick = function (dataObject, jsEvent, view) {
                if (dataObject.calenderType == MY_TIME_OFF_CALENDAR_TYPE) {
                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'Client/app/controllers/calendar/detail-view/detail-my-time-off-view.html',
                        controller: 'CalendarDetailMyTimeOffViewCntrl',
                        size: 'lg',
                        resolve: {
                            data: function () {
                                return angular.copy(dataObject);
                            }
                        }
                    });
                    modalInstance.result.then(function (isToReload) {
                        if (isToReload) {
                            $scope.reloadAll();
                        }
                    }, function () {
                    });
                }
                if (dataObject.calenderType == MY_RESOURCES_TIME_OFF_CALENDAR_TYPE) {
                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'Client/app/controllers/calendar/detail-view/detail-resource-time-off-view.html',
                        controller: 'CalendarDetailResourceTimeOffViewCntrl',
                        size: 'lg',
                        resolve: {
                            data: function () {
                                return angular.copy(dataObject);
                            }
                        }
                    });
                    modalInstance.result.then(function (isToReload) {
                        if (isToReload) {
                            $scope.reloadAll();
                        }
                    }, function () {
                    });
                }
                if (dataObject.calenderType == MY_TASKS_CALENDAR_TYPE) {
                    var modalInstance1 = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'Client/app/controllers/calendar/detail-view/detail-my-task-view.html',
                        controller: 'CalendarDetailMyTaskViewCntrl',
                        size: 'lg',
                        resolve: {
                            data: function () {
                                return angular.copy(dataObject);
                            }
                        }
                    });
                    modalInstance1.result.then(function (isToReload) {
                        if (isToReload) {
                            $scope.reloadAll();
                        }                       
                    }, function () {
                    });
                }
                if (dataObject.calenderType == MY_RESOURCES_TASKS_CALENDAR_TYPE) {
                    var modalInstance1 = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'Client/app/controllers/calendar/detail-view/detail-resource-task-view.html',
                        controller: 'CalendarDetailResourceTaskViewCntrl',
                        size: 'lg',
                        resolve: {
                            data: function () {                                
                                return angular.copy(dataObject);
                                loadResourceTaskEvent();
                            }
                        }
                    });
                    modalInstance1.result.then(function (isToReload) {
                        if (isToReload) {
                            $scope.reloadAll();
                        }
                    }, function () {
                    });
                }
            };

            $scope.alertOnDrop = function () {

            }

            $scope.alertOnResize = function () {

            }

            $scope.changeCurrentDay = function (view, calendar) {
                uiCalendarConfig.calendars[calendar].fullCalendar(view);
            }

            $scope.changeView = function (view, calendar) {
                uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
            };

            $scope.uiConfig = {
                calendar: {
                    height: 550,
                    editable: false,
                    header: {
                        left: 'month, agendaWeek, agendaDay, agendayear',
                        //left: 'title',
                        center: 'title',
                        right: 'today prev,next'                        
                    },
                    eventClick: $scope.alertOnEventClick,
                    eventDrop: $scope.alertOnDrop,
                    eventResize: $scope.alertOnResize,
                    eventRender: function (event, element, view) {
                        if (event.childrenEvents.length > 0) {
                            element.find('.fc-content').after("<span class='badge'>" + (event.childrenEvents.length < 100 ? event.childrenEvents.length : "+99") + "</span>");
                        } 
                        return element;
                    },

                }
            };

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var processResult = function (response) {

                var result = [];
                if (response && response.length) {
                    for (var i = 0; i < response.length ; i++) {


                        var source = response[i]
                        source.stick = true;
                        source.displayEventTime = false;

                        if (source.start)
                            source.start = new Date(source.start);
                        if (source.end)
                            source.end = new Date(source.end);
                        result.push(source);

                    }
                }
                return result;
            }

            $scope.reloadAll = function () {
                self.loadCalendarProgress = true;                
                //$q.all([loadMyEvent(), loadMyResourcesEvents()])
                $q.all([loadMyEvent(), loadMyResourcesEvents(), loadMyTaskEvent(), loadResourceTaskEvent()])
                               .then(function (resolutions) {
                                   self.loadCalendarProgress = false;                                   
                                   $scope.refresh();
                               });

            }

            $scope.reloadAll();

            $scope.refresh = function (tab) {
                if (tab) {
                    tab.isChecked = !tab.isChecked;
                }

                $scope.eventSources[0] = [];
                if (self.calendarTypes[MY_TIME_OFF_ITEM_INDEX].isChecked) {
                    //Array.prototype.push.apply($scope.events, $scope.myEvents);
                    for (var i = 0; i < $scope.myEvents.length; i++) {
                        $scope.eventSources[0].push($scope.myEvents[i]);
                    }
                }
                if (self.calendarTypes[MY_DIRECT_REPORT_TIME_OFF_ITEM_INDEX].isChecked) {
                    //Array.prototype.push.apply($scope.events, $scope.myResourceEvents);

                    for (var i = 0; i < $scope.myResourceEvents.length; i++) {
                        $scope.eventSources[0].push($scope.myResourceEvents[i]);
                    }
                }
                if (self.calendarTypes[MY_TASKS_INDEX].isChecked) {
                    //Array.prototype.push.apply($scope.events, $scope.myResourceEvents);

                    for (var i = 0; i < $scope.myTaskEvents.length; i++) {
                        $scope.eventSources[0].push($scope.myTaskEvents[i]);
                    }
                }
                if (self.calendarTypes[MY_DIRECT_REPORT_TASKS_ITEM_INDEX].isChecked) {
                    //Array.prototype.push.apply($scope.events, $scope.myResourceEvents);

                    for (var i = 0; i < $scope.resourceTaskEvents.length; i++) {
                        $scope.eventSources[0].push($scope.resourceTaskEvents[i]);
                    }
                }
            }

            $scope.events = [];
            $scope.timeFormat = ' ';
            $scope.eventSources = [$scope.events];
        }
    ]);