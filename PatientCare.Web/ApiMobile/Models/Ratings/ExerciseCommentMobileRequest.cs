﻿namespace PatientCare.Web.ApiMobile.Models.Ratings
{
    public class ExerciseCommentMobileRequest
    {
        public long Id { get; set; }

        public long? ExerciseId { get; set; }

        public long? HepDetailId { get; set; }

        public long? PatientId { get; set; }

        public long? PatientCaseId { get; set; }

        public string Comment { get; set; }    
        
            
    }   
}