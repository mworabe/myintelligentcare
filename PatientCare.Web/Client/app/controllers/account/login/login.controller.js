"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("LoginCtrl", [
        "$rootScope", "Auth", "$state", "toaster", "PermissionWorker", "$scope", "Resource", "UserMonitorController",
        function ($rootScope, auth, $state, toaster, permissionWorker, $scope, Resource, userMonitorController) {
            var self = this;
            this.socialAuthenticate = function (provider) {
                auth.socialLogin(provider);
            };
            this.submitted = false;
            $scope.isLoading = false;
            $scope.currentYear = new Date().getFullYear();

            // If a User is already logged in than not showing Login Page again.
            
            if (auth.isAuthenticated()) {
                $state.go("home");
            }
            
            this.localAuthenticate = function (loginForm) {
                if (loginForm.$valid && !self.submitted) {
                    self.submitted = true;
                    $scope.isLoading = true;
                    auth.localLogin(this.login, this.password)
                        .then(function (response) {
                            self.submitted = false;
                            $scope.isLoading = false;
                            toaster.pop("success", "", "Login  successful");
                            $rootScope.idleTimeOut = parseInt(response.data.sessionExpirationTimeInMinit);
                            var obj = {
                                expirationTime: parseInt(response.data.sessionExpirationTimeInMinit),
                                refreshTokenExpTime: response.data.RefreshTokenExpirationTimeInMinit,
                                refreshToken: response.data.refresh_token,
                            };
                            localStorage.setItem("TIME_OUT_KEY", angular.toJson(obj));
                          //  userMonitorController.initUserIdleTimeOutFunctionality($rootScope.idleTimeOut);

                            $rootScope.$emit("update-menu");
                            //response.data.isNDA == "False" ||

                            if (!auth.getNDA()) {
                                $state.go("terms");
                            } else {
                                var homePage = permissionWorker.getHomePageState();
                                if (homePage.hasParams) {
                                    $state.go(homePage.state, homePage.params);
                                } else {
                                    $state.go(homePage.state);
                                }
                            }

                            //$state.go("home_human_resource"); // Temp Patch While Backend is not ready to store Home Page as String.
                        })
                        .catch(function (response) {
                            if (response.data)
                                toaster.pop("error", "", response.data.error_description);
                            self.submitted = false;
                            $scope.isLoading = false;
                        });
                }
            };
        }
    ]);