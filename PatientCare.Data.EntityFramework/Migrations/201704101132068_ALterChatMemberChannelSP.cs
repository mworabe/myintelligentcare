namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ALterChatMemberChannelSP : DbMigration
    {
        public override void Up()
        {
            AlterStoredProcedure(
        "dbo.sp_Chat_Member_channel",
        p => new
        {
            from_user_id = p.String(defaultValueSql: null),
            to_user_id = p.String(defaultValueSql: null)
        },
        body:
                      @"
                         with 
			                        temp1 as
			                        (
			                          SELECT *
			                          FROM chat_members 
			                          where user_id in (@from_user_id) --'47c8b737-f115-4b87-bb3e-99792f50926f'
			                        ),
			                        temp2
			                        as
			                        (
			                          SELECT *
			                          FROM chat_members 
			                          where user_id in (@to_user_id)  
			                        ) 
			                        select chats.Id,chats.channel_id as ChannelsId from temp1 t
			                         inner join temp2  t2 on t.chat_session_id = t2.chat_session_id
			                         inner join chat_Sessions chats on t.chat_session_id = chats.id and t2.chat_session_id = chats.id
                        ");
        }
        
        public override void Down()
        {
            AlterStoredProcedure(
        "dbo.sp_Chat_Member_channel",
        p => new
        {
            from_user_id = p.String(defaultValueSql: null),
            to_user_id = p.String(defaultValueSql: null)

        },
        body:
                      @"
                        select chatm.Id as memberId,chats.Id ,chats.channel_id as ChannelsId from chat_members chatm
    		                                    left join chat_Sessions chats on chatm.chat_session_id = chats.id
    	                                    where chatm.user_id in (@from_user_id,@to_user_id) 
                        ");
        }
    }
}
