namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPatientCaseNoGenerateStoreProcedure : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
                  "dbo.sp_patient_case_no_generate",
                  p => new
                  {
                      patientId = p.Long(),
                      caseId = p.Long()
                  },
                  body:
                      @"Declare @patientNo nvarchar(max);
	                    Declare @previousCaseCount int;
	                    Declare @newcount int;
	                    declare @newCaseNo as nvarchar(max);
	
	                    select @patientNo = patient_number from patients where id = @patientId;
	                    Select @previousCaseCount = COUNT(Id) from patient_cases where patient_id = @patientId;
	
	                    set @newcount = @previousCaseCount + 1;
	                    set @newCaseNo = @patientno + '-' + Convert(nvarchar(max),@newcount);
	
	                    update patient_cases set case_no = @newCaseNo where Id = @caseId"
                );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_patient_case_no_generate");
        }
    }
}
