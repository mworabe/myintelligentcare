﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceCertificateRepository : IRepository<ResourceCertificate>
    {
    }
}
