﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Models;

namespace PatientCare.Web.Api.Controllers.Abstract
{
    public abstract class EdzBaseApiController<T, TRq, TRs> : ApiBaseController where T : Entity
    {
        protected readonly IRepository<T> EntityRepository;

        protected ReadOptions<T> ReadOptions { get; set; }

        protected UpdateOptions<T> UpdateOptions { get; set; }

        protected Action<T> OnEntityCreating { get; set; }
        protected Action<T> OnEntityCreated { get; set; }
        protected Action<T> OnEntityUpdating { get; set; }
        protected Action<T> OnEntityUpdated { get; set; }

        protected Expression<Func<T, bool>> SearchExpression { get; set; }

        protected Func<Crud, StatusCodeResult> GetStatusCodeErrorFunc { get; set; }

        protected EdzBaseApiController(IRepository<T> entityRepository)
        {
            EntityRepository = entityRepository;
            ReadOptions = Data.AbstractRepository.ReadOptions.Create<T>().AsReadOnly();
            UpdateOptions = Data.AbstractRepository.UpdateOptions.Create<T>().WithNotModified(p => p.Created);
        }

        [HttpGet]
        public IHttpActionResult GetEntities([FromUri]ListRequest request)
        {
            var statusCodeResult = GetStatusCodeErrorFunc(Crud.Read);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (string.IsNullOrEmpty(request.SortField))
            {
                request.SortField = "name";
            }
            if (string.IsNullOrEmpty(request.SearchField))
            {
                request.SearchField = "name"; // default field for searching 
            }
            var query = PagingExtensions<T>.CreatePagedQuery(request, SearchExpression);
            var page = EntityRepository.PagedList(query);
            var dtos = Mapper.Map<T[], TRs[]>(page.Entities.ToArray());
            var response = new ListResponse<TRs>(dtos, page.TotalCount);
            return Ok(response);
        }

        [HttpGet]
        public IHttpActionResult GetEntity(long id)
        {
            var statusCodeResult = GetStatusCodeErrorFunc(Crud.Read);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            var item = EntityRepository.Read(id, ReadOptions);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<T, TRs>(item));
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult EditEntity(long id, [FromBody] TRq itemVm)
        {
            var statusCodeResult = GetStatusCodeErrorFunc(Crud.Update);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var enitity = Mapper.Map<TRq, T>(itemVm);
            enitity.Id = id;

            if (OnEntityUpdating != null)
            {
                OnEntityUpdating(enitity);
            }
            EntityRepository.Update(enitity,
                UpdateOptions);
            if (OnEntityUpdated != null)
            {
                OnEntityUpdated(enitity);
            }
            return this.Ok(Mapper.Map<T, TRs>(enitity));
        }

        [HttpPost]
        public IHttpActionResult CreateEntity([FromBody]TRq itemVm)
        {
            var statusCodeResult = GetStatusCodeErrorFunc(Crud.Create);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var enitity = Mapper.Map<TRq, T>(itemVm);
            if (OnEntityCreating != null)
            {
                OnEntityCreating(enitity);
            }
            EntityRepository.Create(enitity,
                new UpdateOptions<T>()
                .WithNotModified(p => p.Created)
                .WithNotModified(p => p.Modified));

            if (OnEntityCreated != null)
            {
                OnEntityCreated(enitity);
            }

            return Created(GetEntityLocation(enitity.Id), Mapper.Map<T, TRs>(enitity));
        }

        [HttpDelete]
        public IHttpActionResult DeleteItem(long id)
        {
            var statusCodeResult = GetStatusCodeErrorFunc(Crud.Delete);
            if (statusCodeResult != null)
            {
                return statusCodeResult;
            }
            var item = EntityRepository.Read(id);
            if (item == null)
            {
                return NotFound();
            }

            EntityRepository.Delete(item);

            return Ok(item);
        }

        protected abstract Uri GetEntityLocation(long id);
    }
}