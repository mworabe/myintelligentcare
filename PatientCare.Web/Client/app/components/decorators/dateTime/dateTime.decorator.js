﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function($provide) {
        $provide.decorator("dateTimeDirective", [
            "$delegate", "dateTimeConfig", function ($delegate, dateTimeConfig) {
                var directive = $delegate[0];

                var link = directive.link;
                directive.compile = function() {
                    return function(scope, element, attrs, ngModel) {
                        link.apply(this, arguments);

                        element.attr("autocomplete", "off");
                        var func = ngModel.$formatters.pop();
                        var length = ngModel.$formatters.length - 1;
                        ngModel.$formatters[length] = function(value) {
                            if (value)
                                return func.apply(this, arguments);
                            return null;

                        };

                        var format = attrs.format || dateTimeConfig.format;//"MM/DD/YYYY";// 
                        ngModel.$parsers[ngModel.$parsers.length -1] =  function(viewValue) {
                            if (!viewValue) {
                                return null;
                            }
                            if (viewValue.length === format.length) {
                                return viewValue;
                            }
                            return null;
                        }
                    };
                };

                directive.restrict = "AM";
                return $delegate;
            }
        ]);
    });