﻿using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.MasterTemplates
{
    [DataContract]
    public class MasterTemplateListDetailResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? OwnerId { get; set; }

        [DataMember]
        public bool? IsMaster { get; set; }

        [DataMember]
        public bool? IsFavorite { get; set; }

        [DataMember]
        public bool? IsPublic { get; set; }

        [DataMember]
        public Int32? TemplateExercisesCount { get; set; }
    }
}