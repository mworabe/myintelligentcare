﻿using PatientCare.Web.Api.Models.Picklist;
using System;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.Ratings
{
    [DataContract]
    public class ExerciseRatingMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ExerciseId { get; set; }
        [DataMember]
        public PicklistDefaultResponse Exercise { get; set; }

        [DataMember]
        public long? HepDetailId { get; set; }
        [DataMember]
        public PicklistDefaultResponse HepDetail { get; set; }

        [DataMember]
        public long? PatientId { get; set; }
        [DataMember]
        public PicklistDefaultResponse Patient { get; set; }

        [DataMember]
        public long? PatientCaseId { get; set; }
        [DataMember]
        public PicklistDefaultResponse PatientCase { get; set; }

        [DataMember]
        public int? Rating { get; set; }

        [DataMember]
        public DateTime? RatingDate { get; set; }
    }
}