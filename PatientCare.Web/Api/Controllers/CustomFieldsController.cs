﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.CustomFields;
using PatientCare.Web.Api.Models.Resources;
using PatientCare.Web.Controllers;
using Swashbuckle.Swagger.Annotations;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Custom Field operations.
    /// </summary>
    [Authorize]
    public class CustomFieldsController : AuthBaseApiController
    {
        /// <summary>
        /// Description : Custom Field ICustomFieldRepository.
        /// </summary>
        private readonly ICustomFieldRepository _customFieldRepository;

        /// <summary>
        /// Description : Custom Field ICustomFieldService.
        /// </summary>
        private readonly ICustomFieldService _customFieldService;

        /// <summary>
        /// Description : Initializes a new instance of the Custom Field Class.
        /// </summary>
        /// <param name="customFieldRepository">ICustom Field Repository</param>
        /// <param name="customFieldService">ICustom Field Service</param>
        public CustomFieldsController(ICustomFieldRepository customFieldRepository, ICustomFieldService customFieldService)
        {
            _customFieldRepository = customFieldRepository;
            _customFieldService = customFieldService;
        }
        private static Expression<Func<CustomField, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return role => role.Name.Contains(searchPhrase);
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/CustomFields/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Custom Field List.   
        /// </summary>
        /// <param name="request">List Request Model</param>
        /// <returns>Custom Field Response Model</returns>
        [HttpGet]
        [ResponseType(typeof(IList<CustomFieldResponse>))]
        public IHttpActionResult GetCustomFields([FromUri]ListRequest request)
        {
            if (string.IsNullOrEmpty(request.SortField))
                request.SortField = "name";
            var query = PagingExtensions<CustomField>.CreatePagedQuery(request, string.IsNullOrWhiteSpace(request.SearchPhrase) ? null : GetSearchColumnExpressions(request.SearchPhrase));

            var readOptions = new ReadOptions<CustomField>()
                .AsReadOnly();

            var page = _customFieldRepository.PagedList(query, readOptions);
            //need replace to picklist
            var dtos = Mapper.Map<CustomField[], CustomFieldResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<CustomFieldResponse>(dtos, page.TotalCount);
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Get Custom Field by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Custom Field Response Model</returns>
        [HttpGet]
        [ResponseType(typeof(CustomFieldResponse))]
        public async Task<IHttpActionResult> GetCustomField(long id)
        {
            var readOptions = new ReadOptions<CustomField>().AsReadOnly().WithIncludePredicate(x=>x.CustomFieldProperties);

            var customField = await _customFieldRepository.ReadAsync(id, readOptions);
            if (customField == null)
                return BadRequest();

            var viewModel = Mapper.Map<CustomField, CustomFieldResponse>(customField);
            return Ok(viewModel);

        }

        /// <summary>
        /// Method Description : Using for Get Custom Field for Resource.    
        /// </summary>
        /// <returns>Custom Field List Response</returns>
        [HttpGet]
        [ResponseType(typeof(IList<CustomFieldResponse>))]
        [Route("api/CustomFields/custom-fields-for-resource")]
        public async Task<IHttpActionResult> GetCustomFieldsForResource()
        {
            var customFields = await _customFieldRepository.ListAsync(new FilterQuery<CustomField>().AddFilter(x => x.ResourceAvailable));
            //need replace to picklist
            var response = Mapper.Map<CustomField[], CustomFieldResponse[]>(customFields.ToArray());
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Get Custom Field value for Resource.     
        /// </summary>
        /// <returns>Resource Custom Field Value Response Model</returns>
        [HttpGet]
        [ResponseType(typeof(CustomFieldResponse))]
        [Route("api/CustomFields/custom-field-values-for-resource")]
        public async Task<IHttpActionResult> GetCustomFieldValuesByResource()
        {
            var customFieldValuesForResource = await _customFieldService.CustomFieldValuesForResource();
            var viewModel = Mapper.Map<ResourceCustomFieldValue[], ResourceCustomFieldValueResponse[]>(customFieldValuesForResource.ToArray());
            return Ok(viewModel);
        }

        /// <summary>
        /// Method Description : Using for Create new Custom Field.     
        /// </summary>
        /// <param name="customFieldRequest">Custom Field Request Model</param>
        /// <returns>Custom Field Response Model</returns>
        [HttpPost]
        [SwaggerResponse(201, "Created", typeof(CustomFieldResponse))]
        public async Task<IHttpActionResult> CreateCustomField([FromBody]CustomFieldRequest customFieldRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customField = Mapper.Map<CustomFieldRequest, CustomField>(customFieldRequest);
            await _customFieldRepository.CreateAsync(customField);
            return Created(GetEntityLocation(customField.Id), Mapper.Map<CustomField, CustomFieldResponse>(customField));
        }

        /// <summary>
        /// Method Description : Using for Update Custom Field.     
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="customFieldRequest">Custom Field Request Model</param>
        /// <returns>Custom Field Response Model</returns>
        [HttpPut]
        public async Task<IHttpActionResult> EditCustomField(long id, [FromBody] CustomFieldRequest customFieldRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customField = Mapper.Map<CustomFieldRequest, CustomField>(customFieldRequest);
            customField.Id = id;

            await _customFieldService.UpdateAsync(customField);
            return Ok(Mapper.Map<CustomField, CustomFieldResponse>(customField));
        }

        /// <summary>
        /// Method Description : Using for Delete Custom Field by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Custom Field Response Model</returns>
        [HttpDelete]
        public IHttpActionResult DeleteCustomField(long id)
        {
            var item = _customFieldRepository.Read(id);
            if (item == null)
                return NotFound();

            _customFieldRepository.Delete(item);            
            return Ok(item);
        }
    }
}