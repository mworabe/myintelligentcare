﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Core.Services.RoleSetupService;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.Holidays;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using PatientCare.Web.Api.Models;
using System.Linq.Expressions;
using System.Linq;

namespace PatientCare.Web.Api.Controllers
{
    [Authorize]
    public sealed class HolidayController : ApiBaseController
    {
        private readonly ITimeManagementService _timeManagementService;
        private readonly IHolidayRepository _holidayRepository;
        ReadOptions<Holiday> _readOptions = new ReadOptions<Holiday>();

        public HolidayController(ITimeManagementService timeManagementService,
                                IHolidayRepository holidayRepository)
        {
            _timeManagementService = timeManagementService;
            _holidayRepository = holidayRepository;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Holidays/" + id, UriKind.Relative);
        }

        [HttpPost]
        [SwaggerResponse(201, "Created", typeof(HolidayResponse))]
        public async Task<IHttpActionResult> CreateHoliday([FromBody]HolidayRequest holidayRequest)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Create, Section.TimeOff);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            holidayRequest.HolidayDate = TimeZoneInfo.ConvertTimeToUtc(holidayRequest.HolidayDate.Value);

            var holiday = Mapper.Map<HolidayRequest, Holiday>(holidayRequest);
            await _timeManagementService.CreateHolidayAsync(holiday);

            return Created(GetEntityLocation(holiday.Id), Mapper.Map<Holiday, HolidayResponse>(holiday));
        }

        [HttpPut]
        public async Task<IHttpActionResult> EditHoliday(long id, [FromBody] HolidayRequest holidayRequest)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Update, Section.TimeOff);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            holidayRequest.HolidayDate = TimeZoneInfo.ConvertTimeToUtc(holidayRequest.HolidayDate.Value);

            var holiday = Mapper.Map<HolidayRequest, Holiday>(holidayRequest);
            holiday.Id = id;

            await _timeManagementService.UpdateHolidayAsync(holiday);

            return Ok(Mapper.Map<Holiday, HolidayResponse>(holiday));
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteHolidayItem(long id)
        {
            //var statusCodeResult = GetStatusCodeError(Crud.Delete, Section.Projects);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}
            var success = await _timeManagementService.DeleteHolidayAsync(id);
            if (!success)
            {
                return NotFound();
            }

            return Ok(new Holiday { Id = id });
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetHoliday(long id)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.Resources);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}
            var holiday = await _timeManagementService.ReadHolidayAsync(id);
            if (holiday == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<Holiday, HolidayResponse>(holiday);
            return Ok(viewModel);
        }


        [HttpGet]
        [Route("api/holidays/Monthly")]
        public IHttpActionResult GetHolidayMonthly([FromUri]HolidayListRequest request)//([FromUri]int month,int year)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.TimeOff);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}
            //var holidaylist = new List<Holiday>();
            //holidaylist = _timeManagementService.ReadHolidayMonthlyAsync(month,year);
            //if (holidaylist == null)
            //{
            //    return NotFound();
            //}          
            //return Ok(holidaylist);


            var query = PagingExtensions<Holiday>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            var readOptions = new ReadOptions<Holiday>().AsReadOnly();
            var page = _holidayRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<Holiday[], HolidayPicklistResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<HolidayPicklistResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        [HttpGet]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<HolidayResponse>))]
        public IHttpActionResult GetAllHoliday([FromUri]HolidayListRequest request)
        {
            //var roleInfo = GetRoleAccessRights();
            //var statusCodeResult = GetStatusCodeError(roleInfo, Crud.Read, Section.TimeOff);
            //if (statusCodeResult != null)
            //{
            //    return statusCodeResult;
            //}

            var query = PagingExtensions<Holiday>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            if (request.filter == "currentMonth")
            {
                int month = DateTime.Now.Date.Month;
                int year = DateTime.Now.Date.Year;
                query.AddFilter(x => x.HolidayDate.Value.Month == month
                                    &&
                                        x.HolidayDate.Value.Year == year);
            }
            var readOptions = new ReadOptions<Holiday>().AsReadOnly();
            var page = _holidayRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<Holiday[], HolidayPicklistResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<HolidayPicklistResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        private static Expression<Func<Holiday, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return holiday => holiday.Name.Contains(searchPhrase);
        }

        private static Expression<Func<Holiday, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return holiday => holiday.Name.Contains(searchPhrase);
        }
    }
}
