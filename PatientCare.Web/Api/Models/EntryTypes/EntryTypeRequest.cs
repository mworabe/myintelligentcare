﻿using PatientCare.Core.Enums;
using System;

namespace PatientCare.Web.Api.Models.EntryTypes
{
    public class EntryTypeRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public EntryPeriodEnum? EntryPeriod { get; set; }

        public DayItem Days { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long? AdvancedCreatePeriod { get; set; }

        public DateTime? LastEntryDateCreated { get; set; }

    }   
}