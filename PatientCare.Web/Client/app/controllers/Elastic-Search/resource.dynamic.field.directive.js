﻿"use strict";
var app = angular.module("EDZoutstaffingPortalApp");

app.directive("resourceDynamicField", ["ConfigLayout", "$filter",
    function (configLayoutService, $filter) {
        return {
            templateUrl: "Client/app/controllers/Elastic-Search/resourcec-dynamic-field-view.html",
            restrict: "E",
            replace: false,
            scope: {
                resourceFields: '=ngResourceFields',
                highLightSearchedText: "=ngHighlightFunc",
            },
            link: function (scope) {
                scope.limitData = 4;
                scope.isExpanded = false;
                /*
                scope.resourceFields = scope.resourceFields.replace(/,,/g, ",");

                scope.resourceFields = scope.resourceFields.replace(/\n/g, " ");

                scope.resourceFields = scope.resourceFields.replace(/\t/g, " ");

                scope.resourceFields = scope.resourceFields.replace(/\\/g, "\\\\");

                scope.resourceFields = scope.resourceFields.replace(/},]/g, "}]");

                scope.matchingData = angular.fromJson(scope.resourceFields);
                */

                scope.matchingData = scope.resourceFields;


                var resourceFieldList = configLayoutService.getResourceFields();
                var resourceTabList = configLayoutService.getResourceTab();

                scope.isKeyVisible = function (key, value) {
                    if (!value)
                        return false;
                    else if (key === 'photoFileNameUrl' || key === 'id' || key === 'name' || key === 'industry' || key === 'subject')
                        return false;
                    else
                        return true;
                }

                scope.expandButtonClick = function () {
                    scope.isExpanded = !scope.isExpanded;
                    (scope.isExpanded) ? scope.limitData = scope.matchingData.length : scope.limitData = 4;
                    event.stopPropagation();
                }

                function getResourceFieldsByTab(tabId) {
                    if (tabId === "personalInformation")
                        return resourceFieldList;
                    else if (tabId === "skills")
                        return [{
                            id: "skill",
                            name: "Skill",
                        }, {
                            id: "description",
                            name: "Description"
                        }];
                    else if (tabId === "education")
                        return [{
                            name: "Name Of School",
                            id: "nameOfSchool",
                        }, {
                            name: "Degree",
                            id: "subject",
                        }, {
                            name: "Designation",
                            id: "resourceDesignation",
                        }];
                    else if (tabId === "certificates")
                        return [{
                            name: "Certificate",
                            id: "certificate",
                        }];
                    else if (tabId === "languages")
                        return [{
                            name: "Language",
                            id: "language",
                        }];
                    else if (tabId === "workingDetails")
                        return [{
                            name: "Behavioral Detail",
                            id: "behaviour",
                        }];
                    else if (tabId === "previousEmployments")
                        return [{
                            name: "Title of Position",
                            id: "titleOfPosition",
                        }, {
                            name: "Duties of Position",
                            id: "dutiesOfPosition",
                        }, {
                            name: "Company Name",
                            id: "companyName",
                        }];
                    else if (tabId === "workExperience")
                        return [{
                            name: "Position",
                            id: "workExperienceJobTitle",
                        }, {
                            name: "Assignment",
                            id: "assignmentDescription",
                        }];
                }

                scope.getResourceKeyLabel = function (tabId, key) {
                    var resourcecFields = resourceFieldList;
                    if (tabId && (tabId !== "personalInformation")) {
                        resourcecFields = getResourceFieldsByTab(tabId)
                    } else if (tabId === "personalInformation") {
                        resourcecFields.push({ id: "organization", name: "Organization / Activity" });
                    }

                    var field = _.find(resourcecFields, function (_field) { return _field.id === key });
                    var tab = _.find(resourceTabList, function (_tab) { return _tab.id === tabId });
                    var returnStr = "";
                    if (tab && (tabId !== "personalInformation"))
                        returnStr += $filter('stringCamelCase')(tab.name) + " / ";

                    if (field && field.name) {
                        returnStr += field.name;
                    } else
                        returnStr += key;
                    return returnStr;
                }

                scope.elipssesText = function (value, fieldsCount) {
                    if (scope.isExpanded)
                        return value;
                    else return (value.length > 50 && fieldsCount > scope.limitData) ? value.substring(0, 50) + "..." : value;
                }

                scope.getContainerClass = function () {
                    var length = scope.matchingData.length - 1;
                    if (length >= 4) {
                        return 'col-md-3';
                    } else if (length >= 2) {
                        return 'col-md-6';
                    } else {
                        return 'col-md-12';
                    }
                }
            }
        }
    }
]);