﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models
{
    [DataContract]
    public class ListResponse<T>
    {

        public ListResponse()
        {
        }

        public ListResponse(T[] data, long itemsCount)
        {
            Data = data;
            ItemsCount = itemsCount;
        }

        [DataMember]
        public T[] Data { get; set; }

        [DataMember]
        public long ItemsCount { get; set; }

        [DataMember]
        public string AdditionalInfo { get; set; }
    }
}