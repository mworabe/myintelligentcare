﻿using Autofac;

namespace PatientCare.Web.Services.IoC
{
    public static class AutofacContainerBuilder
    {
        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder()
                .RegisterRepositories()
                .RegisterWebApi()
                .RegisterValidators()
                .RegisterJob();

            return builder.Build();
        }
    }
}