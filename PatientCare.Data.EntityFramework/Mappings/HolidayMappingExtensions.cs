﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class HolidayMappingExtensions
    {
        public static DbModelBuilder MapHoliday(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<Holiday>().ToTable("holidays").HasKey(i => i.Id);

            modelBuilder.Entity<Holiday>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<Holiday>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();
            
            modelBuilder.Entity<Holiday>()
                .Property(p => p.Location)
                .HasColumnName("location")
                .IsOptional();

            modelBuilder.Entity<Holiday>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            modelBuilder.Entity<Holiday>()
                .Property(p => p.HolidayDate)
                .HasColumnName("holiday_date")
                .IsOptional();

            modelBuilder.Entity<Holiday>()
                .Property(p => p.Description)
                .HasColumnName("description")
                .HasMaxLength(255)
                .IsOptional();

            return modelBuilder;
        }
    }
}
