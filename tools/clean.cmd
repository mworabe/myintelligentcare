@echo off

rd /S /Q ..\build_output

rd /S /Q ..\packages

call "%~dp0run-msbuild" ..\EDZoutstaffingportal.sln Clean Debug || goto :error

goto :done

:error
echo Server build failed with error %errorlevel%.

:done
exit /b %errorlevel%