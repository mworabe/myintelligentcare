﻿using System;

namespace PatientCare.Web.Api.Models.ResourceWorkAssignment
{
    public class ResourceWorkAssignmentRequest
    {
        public long Id { get; set; }

        public long ResourceWorkExperienceId { get; set; }

        public string AssignmentDescription { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long? AssignmentIndustryId { get; set; }
        
        public bool? OfNote { get; set; }
        public bool? JudicialClerkShip { get; set; }
    }
}