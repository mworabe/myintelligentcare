"use strict";
angular.module("EDZoutstaffingPortalApp")
    .config(function($stateProvider) {
        $stateProvider
            .state("exercise-add", {
                url: "/exercise-add",
                templateUrl: "Client/app/controllers/exercise/exercise-add-edit.html",
                controller: "ExerciseAddEditCtrl as ExerciseCtrl",
                resolve: {
                    dataObject: ['InjuryRegionService', 'ActivityService', '$q', function (InjuryRegionService, ActivityService, $q) {
                        function loadInjuryregion() {
                            var defer = $q.defer();
                            InjuryRegionService.getAllItems().$promise.then(function (response) {
                                defer.resolve(response);
                            }, function (reason) {
                                defer.resolve({
                                    errorCode: reason.status
                                });
                            });
                            return defer.promise;
                        }
                        function loadActivity() {
                            var defer = $q.defer();
                            ActivityService.all().$promise.then(function (response) {
                                defer.resolve(response);
                            }, function (reason) {
                                defer.resolve({
                                    errorCode: reason.status
                                });
                            });
                            return defer.promise;
                        }
                        return $q.all([loadInjuryregion(), loadActivity()])
                            .then(function (resolutions) {
                                return resolutions;
                            });
                    }]
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            }).state("exercise-edit", {
                url: "/exercise/edit/:id",
                templateUrl: "Client/app/controllers/exercise/exercise-add-edit.html",
                controller: "ExerciseAddEditCtrl as ExerciseCtrl",
                resolve: {
                    dataObject: ['ExerciseService', '$stateParams', 'InjuryRegionService', 'ActivityService', '$q', function (ExerciseService, $stateParams, injuryService, ActivityService, $q) {
                        function loadExercise() {
                            //console.info($stateParams);
                            var defer = $q.defer();
                            ExerciseService.get({ id: $stateParams.id }).$promise.then(function (response) {
                                defer.resolve(response);
                            }, function (reason) {
                                defer.resolve({
                                    errorCode: reason.status
                                });
                            });
                            return defer.promise;
                        }
                        function loadInjury() {
                            var defer = $q.defer();
                            injuryService.getAllItems().$promise.then(function (response) {
                                defer.resolve(response);
                            }, function (reason) {
                                defer.resolve({
                                    errorCode: reason.status
                                });
                            });
                            return defer.promise;
                        }
                        function loadActivity() {
                            var defer = $q.defer();
                            ActivityService.all().$promise.then(function (response) {
                                defer.resolve(response);
                            }, function (reason) {
                                defer.resolve({
                                    errorCode: reason.status
                                });
                            });
                            return defer.promise;
                        }

                        return $q.all([loadActivity(), loadInjury(), loadExercise()])
                            .then(function (resolutions) {
                                return resolutions;
                            });
                    }]
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });