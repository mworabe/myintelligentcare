﻿using PatientCare.Web.ApiMobile.Models.HepDetails;
using System.Runtime.Serialization;

namespace PatientCare.Web.ApiMobile.Models.HepGroups
{
    [DataContract]
    public class HepGroupsMobileResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? PatientId { get; set; }

        //[DataMember]
        //public PatientAutoCompleteResponse Patient { get; set; }

        [DataMember]
        public long? PatientCaseId { get; set; }

        //[DataMember]
        //public PatientCaseAutoCompleteResponse PatientCase { get; set; }

        [DataMember]
        public int? ExerciseOrder { get; set; }

        [DataMember]
        public long? HepId { get; set; }        

        [DataMember]
        public HepDetailsMobileResponse[] HepDetails { get; set; }
    }
}