﻿using Autofac;
using PatientCare.TaskScheduler.IoC;
using PatientCare.Data.AbstractDataContext;

namespace PatientCare.TaskScheduler
{
    class Program
    {
        private static IContainer IoCContainer;       
        static void Main(string[] args)
        {
            Configure();
        }
        
        private static void Configure()
        {
            IoCContainer = AutofacContainerBuilder.BuildContainer();

            using (var scope = IoCContainer.BeginLifetimeScope())
            {
                var data = scope.Resolve<IDataContextScopeFactory>();
                data.Create(DataContextCreationOptions.ForceCreateNew);
            }
        }
    }

    public static class AutofacContainerBuilder
    {
        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder()
                .RegisterRepositories();               

            return builder.Build();
        }
    }
}
