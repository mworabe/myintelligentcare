namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedUserWorkoutAnalyticsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.user_workout_analytics",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        patient_id = c.Long(),
                        case_id = c.Long(),
                        startdate = c.DateTime(),
                        enddate = c.DateTime(),
                        pre_exercises_painrating = c.Int(),
                        workout_feel_result = c.Int(),
                        workout_comment = c.String(),
                        created = c.DateTime(nullable: false),
                        modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.patient_cases", t => t.case_id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .Index(t => t.patient_id)
                .Index(t => t.case_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.user_workout_analytics", "patient_id", "dbo.patients");
            DropForeignKey("dbo.user_workout_analytics", "case_id", "dbo.patient_cases");
            DropIndex("dbo.user_workout_analytics", new[] { "case_id" });
            DropIndex("dbo.user_workout_analytics", new[] { "patient_id" });
            DropTable("dbo.user_workout_analytics");
        }
    }
}
