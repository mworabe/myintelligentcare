﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using PatientCare.Core.Services.Mailing;
using System.Configuration;
using System.Net;

namespace PatientCare.TaskScheduler.Mailing
{
    public class EmailSender : IEmailSender
    {
        public void Send(MailMessage message)
        {
            try
            {
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["smtp_host"];
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtp_port"]);

                var userName = ConfigurationManager.AppSettings["smtp_username"];
                var password = ConfigurationManager.AppSettings["smtp_password"];

                var enableSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["smtp_enableSSL"]);

                smtp.EnableSsl = enableSSL;

                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;

                smtp.Credentials = new NetworkCredential(userName, password);

                message.From = new MailAddress(userName);
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
            }
        }

        public void Send(IList<MailMessage> mesages)
        {
            foreach (var msg in mesages)
            {
                this.Send(msg);
            }
        }

        public void SendMailAsync(IList<MailMessage> mesages)
        {
            foreach (var msg in mesages)
            {
                this.SendMailAsync(msg);
            }
        }

        public void SendMailAsync(MailMessage message)
        {
            try
            {
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["smtp_host"];
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtp_port"]);

                var userName = ConfigurationManager.AppSettings["smtp_username"];
                var password = ConfigurationManager.AppSettings["smtp_password"];

                var enableSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["smtp_enableSSL"]);

                smtp.EnableSsl = enableSSL;


                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;

                smtp.Credentials = new NetworkCredential(userName, password);

                message.From = new MailAddress(userName);
                smtp.SendMailAsync(message);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
            }
        }
    }
}
