namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedColumnInHepDetailTableAndColumnChangesInExerciseRatingandCommentTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.exercise_comments", "hep_id", "dbo.hep_master");
            DropForeignKey("dbo.exercise_ratings", "hep_id", "dbo.hep_master");
            DropIndex("dbo.exercise_comments", new[] { "hep_id" });
            DropIndex("dbo.exercise_ratings", new[] { "hep_id" });
            AddColumn("dbo.exercise_comments", "hep_detail_id", c => c.Long());
            AddColumn("dbo.hep_detail", "comment", c => c.String());
            AddColumn("dbo.exercise_ratings", "hep_detail_id", c => c.Long());
            CreateIndex("dbo.exercise_comments", "hep_detail_id");
            CreateIndex("dbo.exercise_ratings", "hep_detail_id");
            AddForeignKey("dbo.exercise_comments", "hep_detail_id", "dbo.hep_detail", "Id");
            AddForeignKey("dbo.exercise_ratings", "hep_detail_id", "dbo.hep_detail", "Id");
            DropColumn("dbo.exercise_comments", "hep_id");
            DropColumn("dbo.exercise_ratings", "hep_id");

            AlterColumn("dbo.exercise_comments", "comment_date", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.exercise_ratings", "rating_date", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }

        public override void Down()
        {
            AddColumn("dbo.exercise_ratings", "hep_id", c => c.Long());
            AddColumn("dbo.exercise_comments", "hep_id", c => c.Long());
            DropForeignKey("dbo.exercise_ratings", "hep_detail_id", "dbo.hep_detail");
            DropForeignKey("dbo.exercise_comments", "hep_detail_id", "dbo.hep_detail");
            DropIndex("dbo.exercise_ratings", new[] { "hep_detail_id" });
            DropIndex("dbo.exercise_comments", new[] { "hep_detail_id" });
            DropColumn("dbo.exercise_ratings", "hep_detail_id");
            DropColumn("dbo.hep_detail", "comment");
            DropColumn("dbo.exercise_comments", "hep_detail_id");
            CreateIndex("dbo.exercise_ratings", "hep_id");
            CreateIndex("dbo.exercise_comments", "hep_id");
            AddForeignKey("dbo.exercise_ratings", "hep_id", "dbo.hep_master", "Id");
            AddForeignKey("dbo.exercise_comments", "hep_id", "dbo.hep_master", "Id");

            AlterColumn("dbo.exercise_comments", "comment_date", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.exercise_ratings", "rating_date", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
    }
}
