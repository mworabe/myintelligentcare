﻿using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Cities;
using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PatientParentGuardians
{
    [DataContract]
    public class PatientParentGuardianResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? PatientId { get; set; }
        
        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public RelationshipEnum? RelationshiptoPatient { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string ZipCodeTemp { get; set; }

        [DataMember]
        public PicklistResponse ZipCode { get; set; }

        [DataMember]
        public long? CityId { get; set; }

        [DataMember]
        public CityResponse City { get; set; }

        [DataMember]
        public long? CountryId { get; set; }

        [DataMember]
        public PicklistResponse Country { get; set; }

        [DataMember]
        public long? StateId { get; set; }

        [DataMember]
        public PicklistResponse State { get; set; }


        [DataMember]
        public string CellNumber { get; set; }

        [DataMember]
        public bool? CellNumberIsPrimary { get; set; }

        [DataMember]
        public string HomeNumber { get; set; }

        [DataMember]
        public bool? HomeNumberIsPrimary { get; set; }
    }
}