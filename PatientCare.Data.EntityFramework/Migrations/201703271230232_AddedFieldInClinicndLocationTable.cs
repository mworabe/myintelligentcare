namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldInClinicndLocationTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.clinics", "is_deleted", c => c.Boolean());
            AddColumn("dbo.clinic_locations", "is_deleted", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.clinic_locations", "is_deleted");
            DropColumn("dbo.clinics", "is_deleted");
        }
    }
}
