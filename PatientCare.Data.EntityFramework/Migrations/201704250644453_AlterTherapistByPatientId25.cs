namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterTherapistByPatientId25 : DbMigration
    {
        public override void Up()
        {
            AlterStoredProcedure(
       "dbo.sp_therapist_by_patientId",
       p => new
       {
           Type = p.String(defaultValueSql: null),
           DataCount = p.Int(defaultValueSql: null),
           Search = p.String(defaultValueSql: null),
           PatientId = p.Long(defaultValueSql: null)
       },
       body:
         @" declare @cliicId bigint,
    	    @CaseClinicId bigint,
		    @clinician bigint,
			@CaseClinician bigint;

    	            select @cliicId = clinic_id , @clinician = clinician_id from patients where Id = @PatientId
					select @CaseClinicId = clinic_id,@CaseClinician = assigned_physician_id from patient_cases as patcase where patcase.patient_id = @PatientId 
					               and patcase.is_deleted IS NULL and patcase.case_status = 1
    
    	            if (@Type = 'all')
    	            begin

						select distinct res.Id as Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,res.Email,
    							roles.name as Role,res.clinic_id , users.Id as UserId
    						from resources res 
    						 inner join AspNetUsers users on res.Id = users.ResourceId
    						 inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    						 inner join AspNetRoles roles on mapRole.RoleId = roles.Id 
    						where ((roles.Name in ('Front Office') and (res.clinic_id =@cliicId or res.clinic_id = @CaseClinicId))
							 or (res.Id = @CaseClinician or res.Id = @clinician))

    					     
    						--select distinct res.Id as Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,res.Email,
    						--	roles.name as Role,res.clinic_id , users.Id as UserId
    						--from resources res 
    						-- inner join AspNetUsers users on res.Id = users.ResourceId
    						-- inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    						-- inner join AspNetRoles roles on mapRole.RoleId = roles.Id 
    						--where roles.Name in ('Therapist','Front Office') and res.clinic_id = @cliicId	
    
    		                --            select Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email,
    						                --	roles.name as role
    						                --from resources 
    		                --            where Id in (
    		                --            select Distinct  users.ResourceId from AspNetUsers users
    		                --            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    		                --            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name in ('Therapist','FrontOffice')) 
    		                --            and clinic_id = @cliicId
    	                            end
    	            else if (@Type = 'withSearch')
    	            begin
    
					select TOP (@DataCount) res.Id as Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,
							res.Email,roles.name as Role,res.clinic_id,users.Id as UserId
    						from resources res 
    						 inner join AspNetUsers users on res.Id = users.ResourceId
    						 inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    						 inner join AspNetRoles roles on mapRole.RoleId = roles.Id 
    						where ((roles.Name in ('Front Office') and (res.clinic_id =@cliicId or res.clinic_id = @CaseClinicId))
							 or (res.Id = @CaseClinician or res.Id = @clinician))

    						--select TOP (@DataCount) res.Id as Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,res.Email,
    						--	roles.name as Role,res.clinic_id,users.Id as UserId
    						--from resources res 
    						-- inner join AspNetUsers users on res.Id = users.ResourceId
    						-- inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    						-- inner join AspNetRoles roles on mapRole.RoleId = roles.Id 
    						--where roles.Name in ('Therapist','Front Office') and res.clinic_id = @cliicId	
    
    		                --select TOP (@DataCount) Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email from resources 
    		                --where Id in (
    		                --select Distinct  users.ResourceId from AspNetUsers users
    		                --inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    		                --inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name in ('Therapist','FrontOffice'))
    		                --and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
    		                --and clinic_id = @cliicId
    
    	            end"
                 );
        }
        
        public override void Down()
        {
            AlterStoredProcedure(
       "dbo.sp_therapist_by_patientId",
       p => new
       {
           Type = p.String(defaultValueSql: null),
           DataCount = p.Int(defaultValueSql: null),
           Search = p.String(defaultValueSql: null),
           PatientId = p.Long(defaultValueSql: null)
       },
       body:
         @" declare @cliicId bigint,
    		               @clinicLocation bigint;
    	            select @cliicId = clinic_id , @clinicLocation = clinic_location_id  from patients where Id = @PatientId
    
    	            if (@Type = 'all')
    	            begin
					     
						select distinct res.Id as Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,res.Email,
							roles.name as Role,res.clinic_id , users.Id as UserId
						from resources res 
						 inner join AspNetUsers users on res.Id = users.ResourceId
						 inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
						 inner join AspNetRoles roles on mapRole.RoleId = roles.Id 
						where roles.Name in ('Therapist','Front Office') and res.clinic_id = @cliicId	

    		--            select Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email,
						--	roles.name as role
						--from resources 
    		--            where Id in (
    		--            select Distinct  users.ResourceId from AspNetUsers users
    		--            inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    		--            inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name in ('Therapist','FrontOffice')) 
    		--            and clinic_id = @cliicId
    	            end
    	            else if (@Type = 'withSearch')
    	            begin

						select TOP (@DataCount) res.Id as Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,res.Email,
							roles.name as Role,res.clinic_id,users.Id as UserId
						from resources res 
						 inner join AspNetUsers users on res.Id = users.ResourceId
						 inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
						 inner join AspNetRoles roles on mapRole.RoleId = roles.Id 
						where roles.Name in ('Therapist','Front Office') and res.clinic_id = @cliicId	

    		            --select TOP (@DataCount) Id,first_name as FirstName,last_name as LastName,middle_name as MiddleName,(last_name + ' ' + first_name) as Name,Email from resources 
    		            --where Id in (
    		            --select Distinct  users.ResourceId from AspNetUsers users
    		            --inner join AspNetUserRoles mapRole on users.Id = mapRole.UserId
    		            --inner join AspNetRoles roles on mapRole.RoleId = roles.Id and Name in ('Therapist','FrontOffice'))
    		            --and ((last_name +' '+ first_name) like ('%'+@Search+'%')) 
    		            --and clinic_id = @cliicId
    
    	            end"
                 );
        }
    }
}
