﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using System;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Ui Configuration.
    /// </summary>
    public class UiConfiguration : Entity
    {
        public long? ResourceId { get; set; }
        public virtual Resource Resource { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
