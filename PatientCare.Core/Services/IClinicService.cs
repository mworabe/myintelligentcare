﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PatientCare.Core.Entities;

namespace PatientCare.Core.Services
{
    public interface IClinicService
    {
        #region Clinic

        Task<bool> UpdateClinicAsync(Clinic clinic);
        Task<bool> CreateClinicAsync(Clinic clinic);
        Task<Clinic> ReadClinicAsync(long id);
        Task<bool> DeleteClinicAsync(Clinic clinic);

        #endregion

        #region Clinic Location

        Task<bool> UpdateClinicLocationAsync(ClinicLocation cliniclocation);
        Task<bool> CreateClinicLocationAsync(ClinicLocation cliniclocation);
        Task<ClinicLocation> ReadClinicLocationAsync(long id);
        Task<bool> DeleteClinicLocationAsync(ClinicLocation cliniclocation);

        #endregion
    }
}
