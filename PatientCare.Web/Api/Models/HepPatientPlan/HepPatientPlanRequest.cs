﻿namespace PatientCare.Web.ApiMobile.Models.HepPatientPlan
{
    public class HepPatientPlanRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public long? HepId { get; set; }

        public string HepPlan { get; set; }

    }
}