﻿using System;
using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.ResourceWorkAssignment;
using PatientCare.Web.Api.Models.Picklist;

namespace PatientCare.Web.Api.Models.ResourceWorkExperience
{
    [DataContract]
    public class ResourceWorkExperienceResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? JobTitleId { get; set; }

        [DataMember]
        public virtual PicklistResponse WorkExperienceJobTitle { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public long ResourceId { get; set; }

        [DataMember]
        public ResourceWorkAssignmentResponse[] ResourceWorkAssignments { get; set; }
    }
}