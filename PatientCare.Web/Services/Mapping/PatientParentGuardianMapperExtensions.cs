﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.PatientParentGuardians;

namespace PatientCare.Web.Services.Mapping
{
    public static class PatientParentGuardianMapperExtensions
    {
        public static EmitMapperMapperProvider PatientParentGuardianRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientParentGuardian, PatientParentGuardianResponse>
                    (new ExtendedMapConfig<PatientParentGuardian, PatientParentGuardianResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientParentGuardian[], PatientParentGuardianResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<PatientParentGuardianRequest, PatientParentGuardian>
                    (new ExtendedMapConfig<PatientParentGuardianRequest, PatientParentGuardian>()));

            return mapperProvider;
        }
    }
}