﻿using PatientCare.Web.Api.Models.Picklist;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceTimeoffBenefits
{
    [DataContract]
    public class ResourceTimeoffBenefitResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }

        [DataMember]
        public PicklistResponse Resource { get; set; }

        [DataMember]
        public long? TimeoffTypeId { get; set; }

        [DataMember]
        public PicklistResponse TimeoffType { get; set; }

        [DataMember]
        public decimal? TotalBalance { get; set; }

        [DataMember]
        public decimal? Balance { get; set; }

        [DataMember]
        public decimal? AccruedCurrent { get; set; }

        [DataMember]
        public string EmployerContribution { get; set; }
    }
}