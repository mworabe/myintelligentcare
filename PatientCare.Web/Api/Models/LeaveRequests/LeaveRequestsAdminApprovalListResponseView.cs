﻿using System.Runtime.Serialization;
using System;

namespace PatientCare.Web.Api.Models.LeaveRequests
{
    [DataContract]
    public class LeaveRequestsAdminApprovalListResponseView
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? LeaveTypeId { get; set; }

        [DataMember]
        public string LeaveTitle { get; set; }
                
        [DataMember]
        public int TakenLeaves { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public int RemainingLeave { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public string IsPaidType{get; set;}

        [DataMember]
        public long? SupervisorId { get; set; }

        [DataMember]
        public string SupervisorName { get; set; }

        [DataMember]
        public long? ResourceId { get; set; }
        [DataMember]
        public string ResourceName { get; set; }
                
        [DataMember]
        public DateTime? StatusModifiedDate { get; set; }

        [DataMember]
        public string Reason { get; set; }
            
        [DataMember]
        public string CommentFromSupervisor { get; set; }
        
        [DataMember]
        public string Canceled { get; set; }
    }
}