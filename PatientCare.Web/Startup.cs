﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac.Extras.CommonServiceLocator;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Web;
using PatientCare.Web.Services.IoC;
using PatientCare.Web.Services.Mapping;
using Microsoft.Owin;
using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json.Converters;
using Owin;
using Microsoft.AspNet.SignalR;

[assembly: OwinStartup(typeof(Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)] // For log 
namespace PatientCare.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Autofac
            var autofacContainer = AutofacContainerBuilder.BuildContainer();
            var autofacServiceLocator = new AutofacServiceLocator(autofacContainer);
            // ReSharper disable SuspiciousTypeConversion.Global
            ServiceLocator.SetLocatorProvider(() => (IServiceLocator)autofacServiceLocator);

            // EmitMapper
            MapperConfigurator.Configure();
            
            // MVC
            DependencyResolver.SetResolver(new AutofacDependencyResolver(autofacContainer));
            MvcHandler.DisableMvcResponseHeader = true;

            // WebApi
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(autofacContainer);
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.UseXmlSerializer = false;
            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());

            app.CreatePerOwinContext(() =>
            {
                var scopeFactory = ServiceLocator.Current.GetInstance<IDataContextScopeFactory>();
                return scopeFactory.Create(DataContextCreationOptions.ForceCreateNew);
            });

            ConfigureOAuthTokenGeneration(app);
            ConfigureOAuthTokenConsumption(app);
            
            var hubConfiguration = new HubConfiguration();
            hubConfiguration.EnableDetailedErrors = true;
            app.MapSignalR(hubConfiguration);

            //Swagger
            //HttpConfiguration config = new HttpConfiguration();
            //Swashbuckle.Bootstrapper.Init(config);
        }
    }
}
