"use strict";
(function edzBigtextCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzBigtextCustomField", ["edzBaseCustomField", bigtextCustomField]);

    ///////////////////////////////////////////////////////
    function bigtextCustomField(baseCustomField) {
        function BigtextCustomField() { }

        BigtextCustomField.prototype = Object.create(baseCustomField);
        BigtextCustomField.prototype.optionsTemplate = "<edz-bigtext-custom-field-options></edz-bigtext-custom-field-options>";
        BigtextCustomField.prototype.optionsTemplateEdit = "<edz-bigtext-custom-field-edit-options></edz-bigtext-custom-field-edit-options>";
        return BigtextCustomField;
    }
})();