﻿"use strict";
var EOPApp = angular.module("EDZoutstaffingPortalApp")
EOPApp.service("ExerciseActivitieService", ["$resource", "$state", "$injector", function ($resource, $state, $injector) {
    var permissionWorker = null;
    function getPermissionWorker() {
        if (!permissionWorker)
            permissionWorker = $injector.get("PermissionWorker");
        return permissionWorker;
    }
    var resource = $resource("api/ExerciseActivities/:id/:controller", null, {
        "query": { method: "GET", params: {}, isArray: false },
        "update": { method: "put", params: {}, isArray: false },
        "getAll": { method: "GET", params: {}, isArray: false },
        "getAllItems": { method: "GET", params: { controller: "autocomplete" }, isArray: true },
    });

    return resource;
}]);
