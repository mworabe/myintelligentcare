namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInPatientTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.patient_address", newName: "patient_addresss");
            RenameTable(name: "dbo.patient_parents_guardian", newName: "patient_parents_guardians");
            DropForeignKey("dbo.patients", "language_id", "dbo.languages");
            DropIndex("dbo.patients", "non_birth_date");
            DropIndex("dbo.patients", new[] { "language_id" });
            DropIndex("dbo.patients", "non_maritial_status");
            RenameColumn(table: "dbo.patients", name: "home_number_is_primary", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.patients", name: "HomeNumberIsPrimary", newName: "home_number_is_primary");
            RenameColumn(table: "dbo.patient_parents_guardians", name: "home_number_is_primary", newName: "__mig_tmp__1");
            RenameColumn(table: "dbo.patient_parents_guardians", name: "HomeNumberIsPrimary", newName: "home_number_is_primary");
            RenameColumn(table: "dbo.patients", name: "__mig_tmp__0", newName: "cell_number_is_primary");
            RenameColumn(table: "dbo.patient_parents_guardians", name: "__mig_tmp__1", newName: "cell_number_is_primary");
            AlterColumn("dbo.patients", "language_id", c => c.Long());
            CreateIndex("dbo.patients", "language_id");
            AddForeignKey("dbo.patients", "language_id", "dbo.languages", "Id");
            DropColumn("dbo.patients", "age");
        }
        
        public override void Down()
        {
            AddColumn("dbo.patients", "age", c => c.Int());
            DropForeignKey("dbo.patients", "language_id", "dbo.languages");
            DropIndex("dbo.patients", new[] { "language_id" });
            AlterColumn("dbo.patients", "language_id", c => c.Long(nullable: false));
            RenameColumn(table: "dbo.patient_parents_guardians", name: "cell_number_is_primary", newName: "__mig_tmp__1");
            RenameColumn(table: "dbo.patients", name: "cell_number_is_primary", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.patient_parents_guardians", name: "home_number_is_primary", newName: "HomeNumberIsPrimary");
            RenameColumn(table: "dbo.patient_parents_guardians", name: "__mig_tmp__1", newName: "home_number_is_primary");
            RenameColumn(table: "dbo.patients", name: "home_number_is_primary", newName: "HomeNumberIsPrimary");
            RenameColumn(table: "dbo.patients", name: "__mig_tmp__0", newName: "home_number_is_primary");
            CreateIndex("dbo.patients", "maritial_status", name: "non_maritial_status");
            CreateIndex("dbo.patients", "language_id");
            CreateIndex("dbo.patients", "birth_date", name: "non_birth_date");
            AddForeignKey("dbo.patients", "language_id", "dbo.languages", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.patient_parents_guardians", newName: "patient_parents_guardian");
            RenameTable(name: "dbo.patient_addresss", newName: "patient_address");
        }
    }
}
