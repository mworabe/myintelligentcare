"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("CalendarDetailResourceTimeOffViewCntrl", [
        "$scope", "$uibModalInstance", "calendarService",'data','LeaveRequestService','toaster','Auth',
    function ($scope, $uibModalInstance, calendarService, data, LeaveRequestService, toaster, Auth) {

            
        $scope.dataModal = data;

            if ($scope.dataModal && $scope.dataModal.start)
                $scope.dataModal.start = new Date(moment($scope.dataModal.start).format('LL'));
            if($scope.dataModal && $scope.dataModal.end)
                $scope.dataModal.end = new Date(moment($scope.dataModal.end).format('LL'));

            $scope.clear = function () {
                $uibModalInstance.close(false);
            };

         
            $scope.currentUserResourceId=Auth.getCurrentUser().ResourceId;

            $scope.approveLeave = function (timeOff) {
                var obj = {
                    id: timeOff.id,
                    status: "Approved",
                    comments: "Leave is approved."
                }

                LeaveRequestService.approveLeave(obj).$promise.then(
                    function (responce) {
                        toaster.pop("success", "", "Leave Request approved.");
                        timeOff.status = 'Approved';
                        //$uibModalInstance.close(true);
                        $scope.isLoading = true;
                    }, function (error) {
                        toaster.pop("error", "", error.data.message);
                        $scope.isLoading = false;
                    }
               );
            }

           
            

            $scope.rejectLeave = function (timeOff) {
                var obj = {
                    id: timeOff.id,
                    status: "Rejected",
                    comments: "",
                }

                LeaveRequestService.rejectLeave(obj).$promise.then(
                    function (responce) {
                        toaster.pop("success", "", "Leave request rejected.");
                        timeOff.status = 'Rejected';
                       // $uibModalInstance.close(true);
                        
                    }, function (error) {
                        toaster.pop("error", "", error.data.message);
                        $scope.isLoading = false;
                    }
               );
            }

        }
    ]);