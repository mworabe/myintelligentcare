﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.ResourceScore
{
    [DataContract]
    public class AdvanceSearchFilterRequest
    {
        [DataMember]
        public string Where { get; set; }

        [DataMember(Name = "pageIndex")]
        public int PageIndex { get; set; }
        [DataMember(Name = "pageSize")]
        public int PageSize { get; set; }
    }
}