﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class LeaveRequestMappingExtensions
    {
        public static DbModelBuilder MapLeaveRequest(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<LeaveRequest>().ToTable("leave_requests").HasKey(i => i.Id);

            modelBuilder.Entity<LeaveRequest>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<LeaveRequest>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<LeaveRequest>()
               .Property(p => p.LeaveTypeId)
               .HasColumnName("leave_type_id")
               .IsOptional();

            modelBuilder.Entity<LeaveRequest>()
                .HasOptional<LeaveType>(p => p.LeaveType)
                .WithMany()
                .HasForeignKey(p => p.LeaveTypeId);

            modelBuilder.Entity<LeaveRequest>()
                .Property(p => p.SupervisorId)
                .HasColumnName("supervisor_id")
                .IsOptional();

            modelBuilder.Entity<LeaveRequest>()
                .HasOptional<Resource>(p => p.Supervisor)
                .WithMany()
                .HasForeignKey(p => p.SupervisorId);

            modelBuilder.Entity<LeaveRequest>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<LeaveRequest>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<LeaveRequest>()
               .Property(p => p.StartDate)
               .HasColumnName("start_date")
               .IsOptional();

            modelBuilder.Entity<LeaveRequest>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();
            
            modelBuilder.Entity<LeaveRequest>()
                .Property(p => p.StatusModifiedDate)
                .HasColumnName("status_modified_date")
                .IsOptional();

            modelBuilder.Entity<LeaveRequest>()
                .Property(p => p.Reason)
                .HasColumnName("reason")
                .HasMaxLength(255)
                .IsOptional();

            modelBuilder.Entity<LeaveRequest>()
                .Property(p => p.Status)
                .HasColumnName("status")
                .HasMaxLength(255)
                .IsOptional();

            modelBuilder.Entity<LeaveRequest>()
                .Property(p => p.CommentFromSupervisor)
                .HasColumnName("comment_from_supervisor")
                .HasMaxLength(255)
                .IsOptional();

            modelBuilder.Entity<LeaveRequest>()
               .Property(p => p.TotalTimeOffDays)
               .HasColumnName("total_timeOff_days")
               .IsOptional();

            return modelBuilder;
        }
    }
}
