﻿using System.Configuration;
using System.Web.Mvc;
using Faker;
using Twilio.Jwt.AccessToken;
using System.Collections.Generic;
using Virgil.SDK;
using Virgil.SDK.Cryptography;

namespace TwilioIpMessaging.Controllers
{
    public class TokenController : Controller
    {
        // GET: /token
        public ActionResult Index(string Device)
        {
            // Load Twilio configuration from Web.config
            var accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"];
            var apiKey = ConfigurationManager.AppSettings["TwilioApiKey"];
            var apiSecret = ConfigurationManager.AppSettings["TwilioApiSecret"];
            var ipmServiceSid = ConfigurationManager.AppSettings["TwilioIpmServiceSid"];

            string virgilKey = ConfigurationManager.AppSettings["VirgilKey"];

            //var virgilToken = new VirgilApi(virgilKey);

            //var context = new VirgilApiContext
            //{
            //    AccessToken = virgilKey,
            //    // Credentials are required only in case of publish and revoke local Virgil Cards.
            //    Credentials = new AppCredentials
            //    {
            //        AppId = "4ff666b5e8b146036a8b1c80fba991f38af48fa903d697d839d7764a0bd280db",
            //        AppKey = VirgilBuffer.FromFile("D:\Patientcare-web\patientcare-web\PatientCare.Web\SampleApp.virgilkey"),
            //        AppKeyPassword = "[YOUR_APP_KEY_PASSWORD_HERE]"
            //    },
            //            CardVerifiers = new[] {
            //            new CardVerifierInfo {
            //                CardId = "[YOUR_CARD_ID_HERE]",
            //                PublicKeyData = VirgilBuffer.From("[YOUR_PUBLIC_KEY_HERE]", StringEncoding.Base64)
            //            }
            //        }
            //};

            //context.SetCrypto(new VirgilCrypto());
            //context.SetDeviceManager(new DefaultDeviceManager());
            //context.SetKeyStorage(new KeyStorage());

            //var virgil = new VirgilApi(context);


            // Create a random identity for the client
            var identity = Internet.UserName();

            // Create an IP messaging grant for this token
            var grant = new IpMessagingGrant();
            grant.EndpointId = $"TwilioChatDemo:{identity}:{Device}";
            grant.ServiceSid = ipmServiceSid;
            var grants = new HashSet<IGrant>
            {
                { grant }
            };

            // Create an Access Token generator
            var Token = new Token(accountSid, apiKey, apiSecret, identity: identity, grants: grants);

            var token = Json(new { identity = identity, token = Token.ToJwt() }, JsonRequestBehavior.AllowGet);

            return Json(new
            {
                identity = identity,
                token = Token.ToJwt()
            }, JsonRequestBehavior.AllowGet);


        }
    }
}