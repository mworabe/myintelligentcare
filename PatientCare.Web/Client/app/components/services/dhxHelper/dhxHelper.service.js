﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("DhxHelper", [
        function() {
            return {
                templateHelper: function ($element) {
                    var template = $element[0].innerHTML;
                    var x =  template.replace(/[\r\n]/g, "").replace(/"/g, "\\\"").replace(/\{\{task\.([^\}]+)\}\}/g, function(match, prop) {
                        if (prop.indexOf("|") !== -1) {
                            var parts = prop.split("|");
                            return "\"+gantt.aFilter('" + (parts[1]).trim() + "')(task." + (parts[0]).trim() + ")+\"";
                        }
                        return "\"+task." + prop + "+\"";
                    });
                    return x;
                }
            };
        }
    ]);