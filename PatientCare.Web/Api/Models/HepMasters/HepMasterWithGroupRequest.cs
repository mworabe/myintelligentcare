﻿using PatientCare.Web.Api.Models.HepDetails;
using PatientCare.Web.Api.Models.HepGroups;
using System;

namespace PatientCare.Web.Api.Models.HepMasters
{
    public class HepMasterWithGroupRequest
    {
        public long Id { get; set; }

        public long? PatientId { get; set; }

        public long? OwnerId { get; set; }

        public long? PatientCaseId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public HepDetailRequest[] HepDetails { get; set; }

        public HepGroupRequest[] HepGroups { get; set; }

        public bool? IsSaveAsDraft { get; set; }
    }   
}