﻿angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("resource", {
                url: "/resource",
                templateUrl: "Client/app/controllers/resource/resource-list.html",
                controller: "ResourceCtrl as ResourceCtrl",
                params: { tableName: null, fieldName: null, fieldValue: null, activityId: null, projectId: null },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            })
            .state("resourceEdit", {
                url: "/resource/edit/:id",
                templateUrl: "Client/app/controllers/resource/resource-add-edit.html",
                controller: "ResourceEditCtrl as ResourceEditCtrl",
                params: { currentTab: "personalInformation", availabilityData: [], availabilityHeaders: [] },
                resolve: {
                    customFieldsResource: [
                        "$q", "$stateParams", "CustomField", "RoleSetup", "Resource", 'userTimeZoneFactory', "SystemFunctions",
                        function loadResourceInitialData($q, $stateParams, customFieldService, roleSetupService, resourceService, userTimeZoneFactory, systemFunctions) {
                            function loadResource() {
                                var defer = $q.defer();
                                if (!$stateParams.id) {
                                    defer.resolve();
                                } else {
                                    resourceService.get({ id: $stateParams.id }).$promise.then(function (response) {
                                        defer.resolve(response);
                                    }, function (reason) {
                                        defer.resolve({
                                            errorCode: reason.status
                                        });
                                    });
                                }
                                return defer.promise;

                            }

                            function loadTimeZone() {
                                var defer = $q.defer();
                                userTimeZoneFactory.getTimeZone(defer);
                                return defer.promise;
                            }

                            function loadCustomFields() {
                                var defer = $q.defer();
                                if ($stateParams.id) {
                                    defer.resolve();
                                } else {
                                    customFieldService.customFieldValuesByResource().$promise.then(function (response) {
                                        defer.resolve(response);
                                    });
                                }
                                return defer.promise;
                            }

                            function loadConfigLayout() {
                                var defer = $q.defer();
                                userTimeZoneFactory.getResourceDefaultConfigs(defer);
                                return defer.promise;
                            }

                            return $q.all([loadCustomFields(), loadConfigLayout(), loadResource(), loadTimeZone()])
                                .then(function (resolutions) {
                                    systemFunctions.scrollToTopZero();
                                    return resolutions;
                                });
                        }
                    ]
                },
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });