﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("activityEdit", {
                url: "/activity-edit/:id",
                templateUrl: "Client/app/controllers/Activity/activity.html",
                controller: "ActivityCtrl as ActivityCtrl",
                resolve: {
                    activityPreLoadData: ["$q", "$stateParams", "SystemFunctions","ActivityService",
                        function ($q, $stateParams, systemFunctions, activityService) {
                            function loadActivity() {
                                var defer = $q.defer();
                                if (!$stateParams.id) {
                                    defer.resolve();
                                } else {
                                    activityService.get({ id: $stateParams.id }).$promise.then(function (response) {
                                        defer.resolve(response);
                                    }, function (reason) {
                                        defer.resolve({
                                            errorCode: reason.status
                                        });
                                    });
                                }
                                return defer.promise;
                            }
                            return $q.all([loadActivity()])
                            .then(function (resolutions) {
                                systemFunctions.scrollToTopZero();
                                return resolutions;
                            });
                        }]
                },
                data: {
                    requireLogin: true
                }
            });
    });