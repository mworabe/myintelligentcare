﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("MasterTemplateService", ["$resource", '$state', function ($resource, $state) {
    var resource = $resource("api/masterTemplates/:controller/:id",
        null, //parameters default
        {
            'query': { method: "GET", params: {}, isArray: false },
            'update': { method: "PUT", params: {}, isArray: false },
            'all': { method: "GET", params: {}, isArray: true, url: "api/masterTemplates/autocomplete?search=" },

            'getFullDetailList': { method: "GET", params: {}, isArray: false, url: "api/masterTemplates/list" },
            //



        });



    resource.getTableOption = {
        formatDelete: function (row) {
            return row.name;
        },
        cols: function () {
            return [
                {
                    field: "name",
                    title: "Name",
                    sortable: "name",
                    show: true,
                    dataType: "text",
                    dataTypeView: "href",
                    buildUrl: function (row) {
                        return $state.href("master-templates-edit", { id: row.id });
                    }
                }, {
                    field: "templateExercisesCount",
                    title: "No Of Exercise",
                    sortable: "templateExercisesCount",
                    show: true,
                    dataType: "text",
                    customViewText: function (row) {
                        return (row.templateExercisesCount) ? row.templateExercisesCount : "0";
                    },
                },{
                    field: "action",
                    title: "Action",
                    class: "",
                    dataType: "command"
                }
            ];
        },
        hideAddButton: false,
        showDropDownAddButton: false,

        onAdd_Click: function () {
            $state.go("master-templates-add");
        },
        onEdit_Click: function (row) {
            $state.go("master-templates-edit", { id: row.id });
        },
        
        pageName: "Master Templates",
        menuButtons: [
            {
                click: function (row) {
                    resource.setInactive({ id: row.id }, {}, function () {
                        resource.reloadCommand.reload();
                    });
                },
                iconClass: "icon icon-user-times"
            }
        ],
        headerButtons: [],
        headerLeftItems: []
    };

    return resource;
}
]);
