﻿using PatientCare.Core.Entities;
using System.Threading.Tasks;

namespace PatientCare.Core.Services
{
    public interface IMobileService
    {
        #region Exercise Rating Methods
        Task<bool> InsertUpdateExerciseRatingAsync(ExerciseRating exerciseRating);
        #endregion

        #region Exercise Comment Methods
        Task<bool> InsertUpdateExerciseCommentAsync(ExerciseComment exerciseComment);
        #endregion

        #region App Functional Rating Methods
        Task<bool> InsertUpdateAppFunctionalRatingAsync(AppFunctionalRating appFunctionalRating);
        #endregion

        #region Patient Mobile Profile Methods
        Task<bool> InsertUpdatePatientMobileProfileAsync(PatientMobileProfile patientMobileProfile);
        #endregion
    }
}
