namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedWebPtColuninPatientandCaseTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "webpt_patient_id", c => c.String(maxLength: 500));
            AddColumn("dbo.patient_cases", "webpt_case_id", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.patient_cases", "webpt_case_id");
            DropColumn("dbo.patients", "webpt_patient_id");
        }
    }
}
