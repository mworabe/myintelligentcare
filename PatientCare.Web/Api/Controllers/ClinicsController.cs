﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.Clinics;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Clinics operations.
    /// </summary>
    //[Authorize]
    public sealed class ClinicsController : ApiBaseController
    {
        /// <summary>
        /// Description : Clinic Field IClinicService.
        /// </summary>
        private readonly IClinicService _clinicService;

        /// <summary>
        /// Description : Clinic Field IClinicLocationRepository.
        /// </summary>
        private readonly IClinicRepository _clinicRepository;

        /// <summary>
        /// Description : Clinic Field IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<Clinic> _readOptions = new ReadOptions<Clinic>();

        /// <summary>
        /// Description : Initializes a new instance of the Clinic Class.    
        /// </summary>
        /// <param name="clinicService">Clinic Service</param>
        /// <param name="clinicRepository">Clinic Repository</param>
        /// <param name="dataContextScopeFactory">Data Context Scope Factory</param>
        public ClinicsController
            (
                IClinicService clinicService,
                IClinicRepository clinicRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _clinicService = clinicService;
            _clinicRepository = clinicRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/clinics/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Clinic.    
        /// </summary>
        /// <param name="request">Clinic List Request Model</param>
        /// <returns>Clinic Response Model</returns>
        [HttpGet]
        [Route("api/clinics")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<ClinicResponse>))]
        public IHttpActionResult GetAllClinic([FromUri]ClinicListRequest request)
        {
            try
            {
                var query = PagingExtensions<Clinic>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

                // here we not take deleted clinic in list screen
                query.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);

                var readOptions = new ReadOptions<Clinic>().AsReadOnly();
                var page = _clinicRepository.PagedList(query, readOptions);
                var dtos = Mapper.Map<Clinic[], ClinicResponse[]>(page.Entities.ToArray());

                var response = new ListResponse<ClinicResponse>(dtos, page.TotalCount);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Create New Clinic.    
        /// </summary>
        /// <param name="clinicRequest">Clinic Request Model</param>
        /// <returns>Clinic Response Model</returns>
        [HttpPost]
        [Route("api/clinics")]
        [SwaggerResponse(201, "Created", typeof(ClinicResponse))]
        public async Task<IHttpActionResult> CreateClinic([FromBody]ClinicRequest clinicRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (_clinicRepository.Exists(FilterQuery.Create<Clinic>().AddFilter(p => p.Name == clinicRequest.Name)))
                {
                    ModelState.AddModelError(ReflectionHelper.GetMemberName<Clinic>(p => p.Name), DisplayResources.Error_ItemNameExists);
                    return BadRequest(ModelState);
                }

                if (_clinicRepository.Exists(FilterQuery.Create<Clinic>().AddFilter(p => p.ClinicPrefix == clinicRequest.ClinicPrefix)))
                {
                    ModelState.AddModelError(ReflectionHelper.GetMemberName<Clinic>(p => p.ClinicPrefix), DisplayResources.Error_ClinicPrefixExists);
                    return BadRequest(ModelState);
                }

                var clinic = Mapper.Map<ClinicRequest, Clinic>(clinicRequest);
                await _clinicService.CreateClinicAsync(clinic);

                return Created(GetEntityLocation(clinic.Id), Mapper.Map<Clinic, ClinicResponse>(clinic));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Update Clinic.    
        /// </summary>
        /// <param name="clinicRequest">Clinic Request Model</param>
        /// <returns>Clinic Response Model</returns>
        [HttpPut]
        [Route("api/clinics/{id}")]
        public async Task<IHttpActionResult> EditClinic(long id, [FromBody] ClinicRequest clinicRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (_clinicRepository.Exists(FilterQuery.Create<Clinic>().AddFilter(p => p.Name == clinicRequest.Name && p.Id != clinicRequest.Id)))
                {
                    ModelState.AddModelError(ReflectionHelper.GetMemberName<Clinic>(p => p.Name), DisplayResources.Error_ItemNameExists);
                    return BadRequest(ModelState);
                }

                if (_clinicRepository.Exists(FilterQuery.Create<Clinic>().AddFilter(p => p.ClinicPrefix == clinicRequest.ClinicPrefix && p.Id != clinicRequest.Id)))
                {
                    ModelState.AddModelError(ReflectionHelper.GetMemberName<Clinic>(p => p.ClinicPrefix), DisplayResources.Error_ClinicPrefixExists);
                    return BadRequest(ModelState);
                }

                var clinic = Mapper.Map<ClinicRequest, Clinic>(clinicRequest);
                clinic.Id = id;

                await _clinicService.UpdateClinicAsync(clinic);

                return Ok(Mapper.Map<Clinic, ClinicResponse>(clinic));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Method Description : Using for Delete Clinic by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Clinic Response Model</returns>
        [HttpDelete]
        [Route("api/clinics/{id}")]
        public async Task<IHttpActionResult> DeleteClinicItem(long id)
        {
            try
            {
                var clinic = _clinicRepository.Read(id);
                if (clinic == null)
                    return NotFound();

                await _clinicService.DeleteClinicAsync(clinic);

                return Ok(new ClinicResponse { Id = clinic.Id });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// Method Description : Using for Get Clinic by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Clinic Response Model</returns>
        [HttpGet]
        [Route("api/clinics/{id}")]
        public async Task<IHttpActionResult> GetClinic(long id)
        {
            try
            {
                var clinic = await _clinicService.ReadClinicAsync(id);
                if (clinic == null)
                {
                    return NotFound();
                }

                var viewModel = Mapper.Map<Clinic, ClinicResponse>(clinic);
                return Ok(viewModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private static Expression<Func<Clinic, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return clininc => clininc.Name.Contains(searchPhrase);
        }

        private static Expression<Func<Clinic, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return clinic => clinic.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Clinic Type.      
        /// </summary>
        /// <param name="search">Search</param>
        /// <returns>Clinic Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/clinics/autocomplete")]
        [ResponseType(typeof(ClinicAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            try
            {
                var filter = new FilterQuery<Clinic>();
                if (!string.IsNullOrWhiteSpace(search))
                {
                    filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
                }

                filter.AddFilter(x => x.IsDeleted == false || x.IsDeleted == null);
                var autocompleteResult = await _clinicRepository.AutocompleteAsync(filter);

                var result = Mapper.Map<Clinic[], ClinicAutoCompleteResponse[]>(autocompleteResult.ToArray());

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
