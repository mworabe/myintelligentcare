﻿using PatientCare.Core.Entities;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Data.EntityFramework.Repositories
{
    public sealed class CustomFieldRepository : RepositoryBase<CustomField>, ICustomFieldRepository
    {
        public CustomFieldRepository(IAmbientDataContextLocator dataContextLocator) 
            : base(dataContextLocator)
        {
        }
    }
}
