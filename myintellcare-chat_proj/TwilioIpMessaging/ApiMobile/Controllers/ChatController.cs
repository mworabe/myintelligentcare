using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Twilio.Jwt.AccessToken;
using TwilioIpMessaging.Chat;
using Virgil.SDK;

namespace TwilioIpMessaging.ApiMobile.Controllers
{
    public class ChatController : ApiController
    {
        string appSetting1 = ConfigurationManager.AppSettings["TwilioAccountSid"];
        string appSetting2 = ConfigurationManager.AppSettings["TwilioApiKey"];
        string appSetting3 = ConfigurationManager.AppSettings["TwilioApiSecret"];
        string appSetting4 = ConfigurationManager.AppSettings["TwilioIpmServiceSid"];

        string VIRGIL_ACCESS_TOKEN = ConfigurationManager.AppSettings["VirgilKey"];
        string VIRGIL_APP_ID = ConfigurationManager.AppSettings["VirgilAppId"];
        string VIRGIL_KEY_FILE = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["VirgilKeyFile"]);
        string VIRGIL_KEY_PWD = ConfigurationManager.AppSettings["VirgilKeyPwd"];


        private ChatAuthHelper chatHelper;

        [HttpGet]
        [Route("api/mobile/v1/auth/twilio-token")]
        public async Task<object> ChatTokenGenerate([FromUri] string userId, [FromUri] string deviceType = null)
        {
            //string str1 = !(deviceType == "Android") ? "CR92626ca9ab7dca39088fd2cb4118d531" : "CRfddef647ce3d0db03cbebf36e4e0f830";
            string str1 = !(deviceType == "Android") ? ConfigurationManager.AppSettings["Iphone"] : ConfigurationManager.AppSettings["Android"];
            string str2 = userId;
            HashSet<IGrant> grantSet = new HashSet<IGrant>()
              {
                (IGrant) new IpMessagingGrant()
                {
                  EndpointId = string.Format("MyIntellCare:{0}", (object) str2),
                  ServiceSid = appSetting4,
                  PushCredentialSid = str1
                }
              };
            string signingKeySid = appSetting2;
            string secret = appSetting3;
            string identity = str2;
            DateTime? expiration = new DateTime?();
            DateTime? nbf = new DateTime?();
            HashSet<IGrant> grants = grantSet;
            Token token = new Token(appSetting1, signingKeySid, secret, identity, expiration, nbf, grants);
            return (object)new Dictionary<string, string>()
              {
                {
                  "identity",
                  userId
                },
                {
                  "token",
                  token.ToJwt()
                }
              };
        }

        [HttpGet]
        [Route("api/mobile/v1/auth/virgil-token")]
        public async Task<object> ChatVirgilTokenGenerate()
        {
            ChatController chatController = this;
            return (object)new Dictionary<string, VirgilApi>()
      {
        {
          "token",
          new VirgilApi(new VirgilApiContext()
          {
            AccessToken = chatController.VIRGIL_ACCESS_TOKEN,
            Credentials = (Credentials) new AppCredentials()
            {
              AppId = chatController.VIRGIL_APP_ID,
              AppKey = VirgilBuffer.FromFile(chatController.VIRGIL_KEY_FILE),
              AppKeyPassword = chatController.VIRGIL_KEY_PWD
            }
          })
        }
      };
        }

        [HttpGet]
        [Route("api/mobile/v1/auth/virgil-card")]
        public async Task<IHttpActionResult> CreateVirgilCard([FromUri] string userId)
        {
            ChatController chatController = this;
            string virgilKey = await chatController.GenerateVirgilKey(userId);
            if (virgilKey == null)
                virgilKey = await chatController.GenerateVirgilKey(userId);
            if (virgilKey == null)
                virgilKey = await chatController.GenerateVirgilKey(userId);
            return (IHttpActionResult)chatController.Ok<string>(virgilKey);
        }

        public async Task<string> GenerateVirgilKey(string userId)
        {
            ChatController chatController = this;
            try
            {
                VirgilApi virgil = new VirgilApi(new VirgilApiContext()
                {
                    AccessToken = chatController.VIRGIL_ACCESS_TOKEN,
                    Credentials = (Credentials)new AppCredentials()
                    {
                        AppId = chatController.VIRGIL_APP_ID,
                        AppKey = VirgilBuffer.FromFile(chatController.VIRGIL_KEY_FILE),
                        AppKeyPassword = chatController.VIRGIL_KEY_PWD
                    }
                });
                VirgilKey virgilKey = virgil.Keys.Generate();
                VirgilCard virgilCard = virgil.Cards.Create(userId, virgilKey, "unknown", (Dictionary<string, string>)null);
                virgilCard.Export();
                await virgil.Cards.PublishAsync(virgil.Cards.Import(virgilCard.Export()));
                IEnumerable<VirgilCard> async = await virgil.Cards.FindAsync(userId);
                return virgilKey.Export((string)null).ToString(StringEncoding.Base64);
            }
            catch (Exception ex)
            {
                return (string)null;
            }
        }

        [HttpGet]
        [Route("api/mobile/v1/auth/user-virgil-card")]
        public async Task<IHttpActionResult> userVirgilCardDetail([FromUri] string userId)
        {
            ChatController chatController = this;
            IEnumerable<VirgilCard> async = await new VirgilApi(new VirgilApiContext()
            {
                AccessToken = chatController.VIRGIL_ACCESS_TOKEN,
                Credentials = (Credentials)new AppCredentials()
                {
                    AppId = chatController.VIRGIL_APP_ID,
                    AppKey = VirgilBuffer.FromFile(chatController.VIRGIL_KEY_FILE),
                    AppKeyPassword = chatController.VIRGIL_KEY_PWD
                }
            }).Cards.FindAsync(userId);
            return (IHttpActionResult)chatController.Ok<IEnumerable<VirgilCard>>(async);
        }
    }
}
