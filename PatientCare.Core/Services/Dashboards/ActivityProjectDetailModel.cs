﻿namespace PatientCare.Core.Services.Dashboards
{
    public class ActivityProjectDetailModel
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public long Size { get; set; }
        public ActivityDetailModel[] Children { get; set; }
    }

    public class ActivityDetailModel
    {
        public long ActivityTypeId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public long Size { get; set; }
        public HealthDetailModel[] Children { get; set; }
    }

    public class HealthDetailModel
    {
        public long HealthValue { get; set; }
        public long ActivityTypeId { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string Color { get; set; }
    }
    public class ActivityTypeModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
        public string Color { get; set; }
    }
}
