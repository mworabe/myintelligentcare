"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("MediaPreview", ["$rootScope", "$uibModal", function ($rootScope, $modal) {

        function showVideoView(videoTitle,videoUrl) {
            $modal.open({
                templateUrl: "Client/app/components/media-preview/video-preview/video-view.html",
                controller: "VideoViewCntrl",
                size: "lg",
                keyboard: false,
                resolve: {
                    dataItem: function () {
                        return {
                            title: videoTitle,
                            url: videoUrl
                        }
                    }
                }
            });
        }

        return {
            showVideo: function (videoTitle,url) {
                showVideoView(videoTitle,url)
            }
        }


    }
    ]);