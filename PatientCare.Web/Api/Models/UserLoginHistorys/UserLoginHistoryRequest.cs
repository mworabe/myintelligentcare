﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
using System;

namespace PatientCare.Web.Api.Models.UserLoginHistorys
{
    public class UserLoginHistoryRequest
    {        
        public long Id { get; set; }

        public string UserId { get; set; }

        public string Action { get; set; }

        public DateTime? LogDateTime { get; set; }

        public string IPAddress { get; set; }

        public string SessionKey { get; set; }

        public string Username { get; set; }

    }
}