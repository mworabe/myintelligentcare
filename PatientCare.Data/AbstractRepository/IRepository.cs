﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Data.AbstractRepository
{
    public interface IRepository<T> where T : Entity
    {
        void Create(T entity, UpdateOptions<T> options = null);
        Task CreateAsync(T entity, UpdateOptions<T> options = null);
        T Read(long id, ReadOptions<T> readOptions = null);
        Task<T> ReadAsync(long id, ReadOptions<T> readOptions = null);
        void Update(T entity, UpdateOptions<T> options = null);
        Task UpdateAsync(T entity, UpdateOptions<T> options = null);
        void Delete(T entity);
        Task DeleteAsync(T entity);
        PagedResult<T> PagedList(PagedQuery<T> query, ReadOptions<T> readOptions = null);
        Task<PagedResult<T>> PagedListAsync(PagedQuery<T> query);
        IEnumerable<T> List(FilterQuery<T> filter = null, ReadOptions<T> readOptions = null);
        Task<IEnumerable<T>> ListAsync(FilterQuery<T> filter = null, ReadOptions<T> readOptions = null);
        void AsModified(T entity, Expression<Func<T, object>> propertyExpression = null);
        void AsNotModified(T entity, Expression<Func<T, object>> propertyExpression = null);
        Task<T> FirstOrDefaultAsync(FilterQuery<T> query, ReadOptions<T> readOptions = null);
        bool Exists(FilterQuery<T> query);
        Task<bool> ExistsAsync(FilterQuery<T> query);
        Task<T> LastOrDefaultAsync(FilterQuery<T> query, ReadOptions<T> readOptions = null);

        Task<IEnumerable<T>> AutocompleteAsync(FilterQuery<T> filter = null, ReadOptions<T> readOptions = null);

    }
}
