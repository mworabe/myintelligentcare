﻿namespace PatientCare.Core.Services.Dashboards
{
    public class ProjectsByStatusViewModel
    {
        public string status { get; set; }
        public int Count { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }
}
