/*
  This file in the main entry point for defining Gulp tasks and using Gulp plugins.
  Click here to learn more. https://go.microsoft.com/fwlink/?LinkId=518007
*/
"use strict";

var gulp = require("gulp");

var bowerFiles = require('main-bower-files'),

  inject = require('gulp-inject'),
  stylus = require('gulp-stylus'),
  es = require('event-stream'),
  gulpFilter = require('gulp-filter'),
  concat = require('gulp-concat'),
  order = require("gulp-order"),
  uglify = require('gulp-uglify'),
  debug = require('gulp-debug'),
  clean = require('gulp-clean'),
  cssmin = require('gulp-cssmin'),
  minifyHTML = require('gulp-minify-html'),
  templateCache = require('gulp-angular-templatecache'),
  rev = require('gulp-rev'),
  browserSync = require('browser-sync').create();




var paths = {
    webroot: "Client"
};

paths.appJS = paths.webroot+"/app";
paths.appStyle = paths.webroot + "/assets/css/";



gulp.task("build-html-template", function () {

    //var config = {
    //    htmltemplates: clientApp + '**/*.html',
    //    templateCache: {
    //        file: 'templates.js',
    //        options: {
    //            module: 'app.core',
    //            root: 'app/',
    //            standAlone: false
    //        }
    //    },
    //    temp: './.tmp/'
    //};

    return gulp.src([paths.webroot + "/app/**/*.html", ("!" + paths.webroot + "/**/index.html")])
      .pipe(minifyHTML({ quotes: true}))
      .pipe(templateCache({ module: "EDZoutstaffingPortalApp", root: "Client/" }))
      .pipe(rev())
      .pipe(uglify({ mangle: true }))
      .pipe(gulp.dest('./dist/js'));
});

gulp.task("build-vendor-style", function () {
    var cssFilter = gulpFilter('**/*.css');
    return gulp.src(bowerFiles()) // don't read
        .pipe(cssFilter)
        .pipe(concat('vendor.css'))
        .pipe(cssmin())
        .pipe(rev())
        .pipe(gulp.dest('./dist/css'));
});


gulp.task("build-app-style", function () {

    return gulp.src(paths.appStyle + "*.css")
        .pipe(concat('app.css'))
        .pipe(cssmin())
        .pipe(rev())
        .pipe(gulp.dest('./dist/css'));
});


gulp.task("build-vendor-js", function () {
    var jsFilter = gulpFilter('**/*.js');
    return gulp.src(bowerFiles()) // don't read
        .pipe(jsFilter)
        .pipe(concat('vendor.js'))
        .pipe(rev())
        .pipe(uglify({ mangle: true }))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task("build-app-js", function () {
    return gulp.src(paths.appJS + "/**/*.js")
            .pipe(concat('app.js'))
            .pipe(rev())
            .pipe(gulp.dest('./dist/js'))
            .pipe(uglify({ mangle: false }))
            .pipe(gulp.dest('./dist/js'))
});

gulp.task("clean", function () {
    return gulp.src('./dist', { read: false, force: true })
        .pipe(clean());
});
gulp.task("clean-inject", function () {
    var target = gulp.src(paths.webroot + '/index.html');
    var emptyCSS = gulp.src("*.css");
    var emptyJS = gulp.src(".js");
    
    //.pipe(inject(bowerSource, { name: 'bower', relative: false, addRootSlash: false }))

    return target
      .pipe(inject(emptyCSS, { name: 'bower', relative: false, addRootSlash: false, empty: true }))
      .pipe(inject(emptyCSS, { name: 'inject', relative: false, addRootSlash: false, empty: true }))
      .pipe(inject(emptyJS, { name: 'bower', relative: false, addRootSlash: false, empty: true }))
      .pipe(inject(emptyJS, { name: 'inject', relative: false, addRootSlash: false, empty: true }))

      .pipe(gulp.dest(paths.webroot + "/"));
});

gulp.task('fonts', function () {

    var array = ['bower_components/font-awesome/fonts/fontawesome-webfont.*',
                 'bower_components/bootstrap/fonts/glyphicons-halflings-regular.*'
                ];

    return gulp.src(array)
            .pipe(gulp.dest('./dist/fonts/'));

    //var fontfilter = gulpFilter('**/*.{eot,svg,ttf,woff,woff2}');

    //return gulp.src(bowerFiles())
    //    .pipe(fontfilter)
    //    .pipe(gulp.dest('./dist/fonts/'));
});

gulp.task("bundle-app", [ 'clean-inject', 'build-vendor-js', 'build-app-js', 'build-vendor-style', 'build-app-style', 'fonts', 'build-html-template'], function () {
    
    var target = gulp.src(paths.webroot + '/index.html');
    var vendorCssFiles = gulp.src("./dist/css/*.css")
                        .pipe(order(["vendor*.css", "app*.css"]));
    var sourcesJSFile = gulp.src("./dist/js/*.js", { read: false })
                        .pipe(order(["vendor*.js", "app*.js"]));

    return target
      .pipe(inject(vendorCssFiles, { relative: false, addRootSlash: false }))
      .pipe(inject(sourcesJSFile, { relative: false, addRootSlash: false }))
      .pipe(gulp.dest(paths.webroot + "/"))
      

});


gulp.task("inject-js", function () {

    var target = gulp.src(paths.webroot + '/index.html');
    var sourcesJSFile = gulp.src(paths.appJS + "/**/*.js", { read: false });
    var cssFiles = gulp.src(paths.appStyle + "/**/*.css");


    var bowerSource = gulp.src(bowerFiles(), { read: false })

    return target
      .pipe(inject(bowerSource, { name: 'bower', relative: false, addRootSlash: false }))
      .pipe(inject(es.merge(cssFiles, sourcesJSFile), { relative: false, addRootSlash: false }))
      .pipe(gulp.dest(paths.webroot));


});

gulp.task('js-watch', ['inject-js'], function () {
    browserSync.reload();
})

gulp.task("serve", ['inject-js'], function () {
    browserSync.init({
        server: paths.webroot
    });


    var watchList = [
      (paths.appJS + "/**/*"),
      (paths.webroot + "/index.html"),
      (paths.appStyle + "/**/*")
    ]

    gulp.watch(watchList, ['js-watch'])



});

gulp.task("build-app", ['clean'], function () {
    gulp.start("bundle-app");
});

gulp.task("default", function () {
    
});
