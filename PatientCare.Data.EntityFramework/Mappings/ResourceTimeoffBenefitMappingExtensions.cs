﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceTimeoffBenefitMappingExtensions
    {
        public static DbModelBuilder MapResourceTimeoffBenefit(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceTimeoffBenefit>().ToTable("resource_timeoff_benefits").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceTimeoffBenefit>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceTimeoffBenefit>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceTimeoffBenefit>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<ResourceTimeoffBenefit>()
                .HasOptional<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceTimeoffBenefit>()
              .Property(p => p.TimeoffTypeId)
              .HasColumnName("timeoff_type_id")
              .IsOptional();

            modelBuilder.Entity<ResourceTimeoffBenefit>()
                .HasOptional<LeaveType>(p => p.TimeoffType)
                .WithMany()
                .HasForeignKey(p => p.TimeoffTypeId);

            modelBuilder.Entity<ResourceTimeoffBenefit>()
                .Property(p => p.TotalBalance)
                .HasColumnName("total_balance")
                .IsOptional();

            modelBuilder.Entity<ResourceTimeoffBenefit>()
              .Property(p => p.Balance)
              .HasColumnName("balance")
              .IsOptional();

            modelBuilder.Entity<ResourceTimeoffBenefit>()
              .Property(p => p.AccruedCurrent)
              .HasColumnName("accrued_current")
              .IsOptional();


            return modelBuilder;
        }
    }
}
