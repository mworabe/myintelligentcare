namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedPatientAppointmentTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.patient_appointment",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    patient_id = c.Long(),
                    appointment_date = c.DateTime(),
                    start_date = c.DateTime(),
                    end_date = c.DateTime(),
                    clinic_location_id = c.Long(),
                    appointment_type_id = c.Long(),
                    clinician_id = c.Long(),
                    facility_id = c.Long(),
                    additional_detail = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),

                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.appointment_type", t => t.appointment_type_id)
                .ForeignKey("dbo.resources", t => t.clinician_id)
                .ForeignKey("dbo.clinic_locations", t => t.clinic_location_id)
                .ForeignKey("dbo.clinics", t => t.facility_id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .Index(t => t.patient_id)
                .Index(t => t.clinic_location_id)
                .Index(t => t.appointment_type_id)
                .Index(t => t.clinician_id)
                .Index(t => t.facility_id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.patient_appointment", "patient_id", "dbo.patients");
            DropForeignKey("dbo.patient_appointment", "facility_id", "dbo.clinics");
            DropForeignKey("dbo.patient_appointment", "clinic_location_id", "dbo.clinic_locations");
            DropForeignKey("dbo.patient_appointment", "clinician_id", "dbo.resources");
            DropForeignKey("dbo.patient_appointment", "appointment_type_id", "dbo.appointment_type");
            DropIndex("dbo.patient_appointment", new[] { "facility_id" });
            DropIndex("dbo.patient_appointment", new[] { "clinician_id" });
            DropIndex("dbo.patient_appointment", new[] { "appointment_type_id" });
            DropIndex("dbo.patient_appointment", new[] { "clinic_location_id" });
            DropIndex("dbo.patient_appointment", new[] { "patient_id" });
            DropTable("dbo.patient_appointment");
        }
    }
}
