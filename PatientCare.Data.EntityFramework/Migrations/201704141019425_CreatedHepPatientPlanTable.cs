namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class CreatedHepPatientPlanTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.hep_patient_plan",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    hep_id = c.Long(),
                    hep_plan = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.hep_master", t => t.hep_id)
                .Index(t => t.hep_id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.hep_patient_plan", "hep_id", "dbo.hep_master");
            DropIndex("dbo.hep_patient_plan", new[] { "hep_id" });
            DropTable("dbo.hep_patient_plan");
        }
    }
}
