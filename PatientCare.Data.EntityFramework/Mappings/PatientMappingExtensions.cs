﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;
using System.Data.Entity.Infrastructure.Annotations;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class PatientMappingExtensions
    {
        public static DbModelBuilder MapPatient(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<Patient>().ToTable("patients").HasKey(i => i.Id);

            modelBuilder.Entity<Patient>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<Patient>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<Patient>()
                .Property(p => p.FirstName)
                .HasColumnName("first_name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_first_name", 1)));

            modelBuilder.Entity<Patient>()
                .Property(p => p.MiddleName)
                .HasColumnName("middle_name")
                .HasMaxLength(255);

            modelBuilder.Entity<Patient>()
                .Property(p => p.LastName)
                .HasColumnName("last_name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_last_name", 2)));

            modelBuilder.Entity<Patient>()
                .Property(p => p.NickName)
                .HasColumnName("nick_name")
                .HasMaxLength(255);

            modelBuilder.Entity<Patient>()
                .Property(p => p.BirthDate)
                .HasColumnName("birth_date")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.IsMinor)
                .HasColumnName("is_minor")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.SSN)
                .HasColumnName("ssn")
                .IsOptional()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("non_ssn", 4)));

            modelBuilder.Entity<Patient>()
                .Property(p => p.Gender)
                .HasColumnName("gender")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.Email)
                .HasColumnName("email")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.PrefferedContactMethod)
                .HasColumnName("preffered_contact_method")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.CellNumber)
                .HasColumnName("cell_number")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.CellNumberIsPrimary)
                .HasColumnName("cell_number_is_primary")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.HomeNumber)
                .HasColumnName("home_number")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.HomeNumberIsPrimary)
                .HasColumnName("home_number_is_primary")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.SpecialAccomodations)
                .HasColumnName("special_accomodations")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.MaritialStatus)
                .HasColumnName("maritial_status")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.EmergencyContact)
                .HasColumnName("emergency_contact")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.RelationshipToContact)
                .HasColumnName("relationship_to_contact")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.EmergencyPhone)
                .HasColumnName("emergency_phone")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.EmployerName)
                .HasColumnName("employer_name")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.EmployerPhone)
                .HasColumnName("employer_phone")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.LanguageId)
                .HasColumnName("language_id")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .HasOptional<Language>(p => p.Language)
                .WithMany()
                .HasForeignKey(p => p.LanguageId);

            modelBuilder.Entity<Patient>()
                .HasMany<PatientAddress>(p => p.Addresses)
                .WithOptional()
                .HasForeignKey(item => item.PatientId);

            modelBuilder.Entity<Patient>()
                .HasMany<PatientParentGuardian>(p => p.ParentGaurdians)
                .WithOptional()
                .HasForeignKey(item => item.PatientId);

            modelBuilder.Entity<Patient>()
                .Property(p => p.ReferralPhysicianId)
                .HasColumnName("referral_physician_id")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .HasOptional<ReferralPhysician>(p => p.ReferralPhysician)
                .WithMany()
                .HasForeignKey(p => p.ReferralPhysicianId);

            modelBuilder.Entity<Patient>()
                .Property(p => p.ClinicId)
                .HasColumnName("clinic_id")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .HasOptional<Clinic>(p => p.Clinic)
                .WithMany()
                .HasForeignKey(p => p.ClinicId);

            modelBuilder.Entity<Patient>()
                .Property(p => p.ClinicLocationId)
                .HasColumnName("clinic_location_id")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .HasOptional<ClinicLocation>(p => p.ClinicLocation)
                .WithMany()
                .HasForeignKey(p => p.ClinicLocationId);

            modelBuilder.Entity<Patient>()
                .Property(p => p.PatientNumber)
                .HasColumnName("patient_number")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                 .Property(p => p.ClinicianId)
                 .HasColumnName("clinician_id")
                 .IsOptional();

            modelBuilder.Entity<Patient>()
                 .HasOptional<Resource>(p => p.Clinician)
                 .WithMany()
                 .HasForeignKey(p => p.ClinicianId);

            modelBuilder.Entity<Patient>()
                .Property(p => p.IsMedicarePatient)
                .HasColumnName("is_medicare_patient")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.HasMobileAccess)
                .HasColumnName("has_mobile_access")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.PhotoFileName)
                .HasColumnName("photo_file_name")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.Status)
                .HasColumnName("status")
                .IsOptional();

            modelBuilder.Entity<Patient>()
                .Property(p => p.WebPTPatientId)
                .HasColumnName("webpt_patient_id")
                .IsOptional()
                .HasMaxLength(500);

            modelBuilder.Entity<Patient>()
                .Property(p => p.IsDeleted)
                .HasColumnName("is_deleted")
                .IsOptional();

            return modelBuilder;
        }
    }
}
