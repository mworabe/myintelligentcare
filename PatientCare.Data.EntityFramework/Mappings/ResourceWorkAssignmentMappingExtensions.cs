﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceWorkAssignmentMappingExtensions
    {
        public static DbModelBuilder MapResourceWorkAssignment(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceWorkAssignment>().ToTable("resource_work_assignments").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceWorkAssignment>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceWorkAssignment>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();
            
            modelBuilder.Entity<ResourceWorkAssignment>()
                .Property(p => p.ResourceWorkExperienceId)
                .HasColumnName("resource_work_experience_id")
                .IsOptional();

            modelBuilder.Entity<ResourceWorkAssignment>()
                .HasOptional<ResourceWorkExperience>(p => p.ResourceWorkExperience)
                .WithMany()
                .HasForeignKey(p => p.ResourceWorkExperienceId);

            modelBuilder.Entity<ResourceWorkAssignment>()
                .Property(p => p.AssignmentDescription)
                .HasColumnName("assignment_description")
                .IsOptional();

            modelBuilder.Entity<ResourceWorkExperience>()
                .Property(p => p.StartDate)
                .HasColumnName("start_date")
                .IsOptional();

            modelBuilder.Entity<ResourceWorkAssignment>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

            modelBuilder.Entity<ResourceWorkAssignment>()
                .Property(p => p.AssignmentIndustryId)
                .HasColumnName("assignment_industry_id")
                .IsOptional();

            modelBuilder.Entity<ResourceWorkAssignment>()
                .HasOptional<DropDownConfig>(p => p.AssignmentIndustry)
                .WithMany()
                .HasForeignKey(p => p.AssignmentIndustryId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ResourceWorkAssignment>()
                .Property(p => p.OfNote)
                .HasColumnName("of_note")
                .IsOptional();

            modelBuilder.Entity<ResourceWorkAssignment>()
                .Property(p => p.JudicialClerkShip)
                .HasColumnName("judicial_clerk_ship")
                .IsOptional();

            return modelBuilder;
        }
    }
}
