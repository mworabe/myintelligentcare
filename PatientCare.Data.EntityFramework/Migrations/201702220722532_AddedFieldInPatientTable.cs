namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldInPatientTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "patient_number", c => c.String());
            AddColumn("dbo.patients", "clinician_id", c => c.Long());
            AddColumn("dbo.patients", "is_medicare_patient", c => c.Boolean());
            CreateIndex("dbo.patients", "clinician_id");
            AddForeignKey("dbo.patients", "clinician_id", "dbo.resources", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.patients", "clinician_id", "dbo.resources");
            DropIndex("dbo.patients", new[] { "clinician_id" });
            DropColumn("dbo.patients", "is_medicare_patient");
            DropColumn("dbo.patients", "clinician_id");
            DropColumn("dbo.patients", "patient_number");
        }
    }
}
