﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class CityMappingExtensions
    {
        public static DbModelBuilder MapCity(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<City>().ToTable("Cities")
                .HasKey(i => i.Id);

            modelBuilder.Entity<City>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            modelBuilder.Entity<City>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<City>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<City>()
                .Property(p => p.StateId)
                .HasColumnName("state_id")
                .IsOptional();

            modelBuilder.Entity<City>()
                .Property(p => p.StateCode)
                .HasColumnName("state_code")
                .IsOptional();

            modelBuilder.Entity<City>()
                .Property(p => p.CountryId)
                .HasColumnName("country_id")
                .IsOptional();

            return modelBuilder;
        }
    }
}