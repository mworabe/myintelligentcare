﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ExercisePositionMappingExtensions
    {
        public static DbModelBuilder MapExercisePosition(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ExercisePosition>().ToTable("exercise_positions").HasKey(i => i.Id);

            modelBuilder.Entity<ExercisePosition>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ExercisePosition>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();
            
           modelBuilder.Entity<ExercisePosition>()
                .Property(p => p.Name)
                .HasColumnName("name")
                .IsOptional();

            return modelBuilder;
        }
    }
}
