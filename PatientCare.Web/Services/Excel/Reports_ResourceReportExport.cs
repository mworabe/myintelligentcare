﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PatientCare.Core.Entities;
using PatientCare.Data.AbstractRepository;
using OfficeOpenXml;
using WebGrease;
using PatientCare.Web.Api.Models.Reports;

namespace PatientCare.Web.Services.Excel
{
    public static class ResourceReportExport
    {

        public static Stream ExportReport(IList<ResourcesReportView> reportData, string title)
        {
            using (var excelPackage = new ExcelPackage(new MemoryStream()))
            {
                excelPackage.Workbook.Properties.Author = "IRMS";
                excelPackage.Workbook.Properties.Title = title;

                var worksheet = excelPackage.Workbook.Worksheets.Add("Resources");

                ApplyHeader(worksheet);


                for (var i = 0; i < reportData.Count; i++)
                {
                    FillUpTheRow(worksheet, reportData[i], i + 2);
                }


                excelPackage.Save();
                return excelPackage.Stream;
            }
        }

        private static void FillUpTheRow(ExcelWorksheet worksheet, ResourcesReportView reporData, int i)
        {
            worksheet.Cells[i, 1].Value = reporData.EmployeeId;
            worksheet.Cells[i, 2].Value = reporData.Resourcename;
            worksheet.Cells[i, 3].Value = reporData.Division;
            worksheet.Cells[i, 4].Value = reporData.Department;
            worksheet.Cells[i, 5].Value = reporData.JobTitle;
            worksheet.Cells[i, 6].Value = reporData.Supervisor;
            worksheet.Cells[i, 7].Value = reporData.StartDate.HasValue ? reporData.StartDate.Value.Date.ToString("MM/dd/yyyy") : "";
        }

        private static void ApplyHeader(ExcelWorksheet worksheet)
        {
            worksheet.Cells[1, 1].Value = "Employee Id";
            worksheet.Cells[1, 1].Style.Font.Bold = true;

            worksheet.Cells[1, 2].Value = "Resource name";
            worksheet.Cells[1, 2].Style.Font.Bold = true;

            worksheet.Cells[1, 3].Value = "Division";
            worksheet.Cells[1, 3].Style.Font.Bold = true;

            worksheet.Cells[1, 4].Value = "Department";
            worksheet.Cells[1, 4].Style.Font.Bold = true;

            worksheet.Cells[1, 5].Value = "Job title";
            worksheet.Cells[1, 5].Style.Font.Bold = true;

            worksheet.Cells[1, 6].Value = "Supervisor";
            worksheet.Cells[1, 6].Style.Font.Bold = true;

            worksheet.Cells[1, 7].Value = "Hire date";
            worksheet.Cells[1, 7].Style.Font.Bold = true;

        }
    }
}