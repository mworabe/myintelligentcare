"use strict";
(function edzFileCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzFileCustomField", ["edzBaseCustomField", edzFileCustomField]);

    ///////////////////////////////////////////////////////
    function edzFileCustomField(baseCustomField) {
        function FileCustomField() { }

        FileCustomField.prototype = Object.create(baseCustomField);
        FileCustomField.prototype.optionsTemplate = "<edz-file-custom-field-options></edz-file-custom-field-options>";
        FileCustomField.prototype.optionsTemplateEdit = "<edz-file-custom-field-edit-options></edz-file-custom-field-edit-options>";
        return FileCustomField;
    }
})();