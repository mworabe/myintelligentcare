"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("CalendarDetailResourceTaskViewCntrl", [
        "$scope", "$uibModalInstance", "calendarService",'data','LeaveRequestService','toaster','Auth',
    function ($scope, $uibModalInstance, calendarService, data, LeaveRequestService, toaster, Auth) {

            $scope.dataModal = data;
            if ($scope.dataModal && $scope.dataModal.start)
                $scope.dataModal.start = new Date(moment($scope.dataModal.start).format('LL'));
            if($scope.dataModal && $scope.dataModal.end)
                $scope.dataModal.end = new Date(moment($scope.dataModal.end).format('LL'));

            if ($scope.dataModal && $scope.dataModal.projectStartDate)
                $scope.dataModal.projectStartDate = new Date($scope.dataModal.projectStartDate);
            if ($scope.dataModal && $scope.dataModal.projectEndDate)
                $scope.dataModal.projectEndDate = new Date($scope.dataModal.projectEndDate);

            $scope.clear = function () {
                $uibModalInstance.close(false);
            };
                 
            //$scope.currentUserResourceId=Auth.getCurrentUser().ResourceId;

            //$scope.approveLeave = function () {
            //    var obj = {
            //        id: $scope.dataModal.id,
            //        status: "Approved",
            //        comments: "Leave is approved."
            //    }
            //    LeaveRequestService.approveLeave(obj).$promise.then(
            //        function (responce) {
            //            toaster.pop("success", "", "Leave Request approved.");
            //            $uibModalInstance.close(true);
            //        }, function (error) {
            //            toaster.pop("error", "", error.data.message);
            //            $scope.isLoading = false;
            //        }
            //   );
            //}

            //$scope.rejectLeave = function () {
            //    var obj = {
            //        id: $scope.dataModal.id,
            //        status: "Rejected",
            //        comments: $scope.dataModal.commentFromSupervisor
            //    }
            //    LeaveRequestService.rejectLeave(obj).$promise.then(
            //        function (responce) {
            //            toaster.pop("success", "", "Leave request rejected.");
            //            $uibModalInstance.close(true);                        
            //        }, function (error) {
            //            toaster.pop("error", "", error.data.message);
            //            $scope.isLoading = false;
            //        }
            //   );
            //}

        }
    ]);