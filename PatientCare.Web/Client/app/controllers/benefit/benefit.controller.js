// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright � 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("BenefitCtrl", [      
        "$state",
        function ($state) {
            var self = this;

            self.analyticsClick = function () {
                $state.go("finances");
            }
            self.Settings = [];
            self.SettingsProject = [
                        "Project 01",
                        "Project 02",
                        "Project 03",
                        "Project 04",
                        "Project 05",
                        "Project Permit"
            ];
            self.benefitViewModel = {
                fields: [
                    {
                        label: "Skill",
                        fieldName: "Skill",
                        dataType: "ui-select",
                        list: self.SettingsProject,
                        classContainer: "col-md-4",
                        required: true
                    },
                    {
                        label: "Experience",
                        fieldName: "Experience",
                        classContainer: "col-md-4",
                        required: true
                    },
                    {
                        label: "Importance",
                        fieldName: "Importance",
                        classContainer: "col-md-3",
                        required: true
                    }
                ]
            };          
        }
    ]);