"use strict";
(function edzAlphaCustomFieldIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .factory("edzAlphaCustomField", ["edzBaseCustomField", edzAlphaCustomField]);

    ///////////////////////////////////////////////////////
    function edzAlphaCustomField(baseCustomField) {
        function AlphaCustomField() {}

        AlphaCustomField.prototype = Object.create(baseCustomField);
        AlphaCustomField.prototype.optionsTemplate = "<edz-alpha-custom-field-options></edz-alpha-custom-field-options>";
        AlphaCustomField.prototype.optionsTemplateEdit = "<edz-alpha-custom-field-edit-options></edz-alpha-custom-field-edit-options>";
        return AlphaCustomField;
    }
})();