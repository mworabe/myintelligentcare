﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("patientService", [
        "$resource", "$state", "$injector", "$uibModal", "$filter", "$translate", "uuid2", "$rootScope", 'AppointmentFactory', "ReferralDialogFactory", "APP_CONFIG", "MediaPreview",
            function ($resource, $state, $injector, $uibModal, $filter, $translate, uuid2, $rootScope, AppointmentFactory, referralDialogFactory, APP_CONFIG, MediaPreview) {
                var permissionWorker = null;
                $rootScope.appointmentOpenFormCounter = 0;

                function getPermissionWorker() {
                    if (!permissionWorker) {
                        permissionWorker = $injector.get("PermissionWorker");
                    }
                    return permissionWorker;
                };

                var resource = $resource("api/Patients/:id/:controller",
                    null, //parameters default
                    {
                        'update': { method: "PUT" },
                        'query': { method: "GET", params: {}, isArray: false },
                        'deletePatient': { method: "delete", params: {}, isArray: false },
                    });


                resource.avatarSrc = "api/Patients/UploadPhoto";
                resource.uploadAvatar = function (uploader, file, resourceId, cb) {
                    cb = cb || angular.noop;
                    var url = this.avatarSrc + "?id=" + resourceId;
                    uploader.upload({
                        url: url,
                        data: { file: file }
                    }).then(cb);
                };

                resource.cvSrc = "api/Patients/UploadCv";
                resource.uploadCV = function (uploader, file, resourceId, cb) {
                    cb = cb || angular.noop;
                    var url = this.cvSrc + "?id=" + resourceId;
                    uploader.upload({
                        url: url,
                        data: { file: file }
                    }).then(cb);
                };

                resource.fileSrc = "api/Patients/UploadFile";
                resource.uploadFile = function (uploader, file) {
                    var url = this.fileSrc;
                    return uploader.upload({
                        url: url,
                        data: { file: file }
                    });
                };

                resource.uploadResources = function (uploader, file, cb) {
                    cb = cb || angular.noop;
                    var url = "api/Patients/import";
                    uploader.upload({
                        url: url,
                        data: { file: file }
                    }).then(cb);
                };

                resource.reloadCommand = {};

                function getNameToEntity(entity) {
                    if (!entity) return "";
                    return entity.name;
                }

                var resourceCheckboxInactive = false;

               
                function openMediaDialog() {
                    MediaPreview.showVideo("Video", "https://amazon-storage-container-dev-use.s3.amazonaws.com/exercise/124/medias/85d5d05f-ef3c-429a-9978-7655a78013ac82%20active%20ankle%20warm-up%20(convert-video-online.com).mp4?AWSAccessKeyId=AKIAIL7V3Y2WORGLQHCQ&Expires=1496226589&Signature=b5oa%2F%2B2KvRfzB74sb10amMLzPys%3D");
                }

                resource.getTableOption = {
                    formatDelete: function (row) {
                        return row.name;
                    },
                    disabledAddButton: function () {
                        return !getPermissionWorker().canPatientInsert();
                    },
                    disabledDeleteButton: function () {
                        return getPermissionWorker().canPatientDelete();
                    },
                    disabledEditButton: function () {
                        return getPermissionWorker().canPatientUpdate();
                    },
                    disabledViewButton: function () {
                        return true;
                    },
                    getResourceCheckboxInactive: function () {
                        return resourceCheckboxInactive;
                    },
                    reset: function () {
                        resourceCheckboxInactive = false;
                    },
                    cols: function () {
                        return [
                            {
                                field: "name",
                                title: "Patient Name",
                                sortable: "Name",
                                show: true,
                                dataTypeView: "custom-html",
                                dataType: "text",
                                templateHTML: function (row) {
                                    var patientId = (row.patientId || uuid2.newguid().substring(0, 5).replace(/-/g, '_'));
                                    var photoUrl = "Client/assets/images/userIco.png";
                                    if (row.photoFileNameUrl)
                                        photoUrl = row.photoFileNameUrl;
                                    function getPatientURL() { return $state.href("patientsEdit", { id: row.id }); };
                                    function getHEPUrl() {
                                        if (row.caseId) {
                                            return "href=\"" + $state.href("patientCaseEdit", { patientId: row.id, caseId: row.caseId, currentTab: "hep" }) + "\" "
                                        }
                                        return "";
                                    }
                                    var templateStr = "";
                                    templateStr += "<div class=\"patient-detail-container\">" +
                                                "<div class=\"patient-img-container\">" +
                                                    "<img class=\"img img-circle\" src=\"" + photoUrl + "\" />" +
                                                "</div>" +
                                                "<div class=\"patient-name-container\">" +
                                                    "<div class=\"custom-html-title\" title=\"" + row.name + "\">" +
                                                        "<a title=\"" + row.name + "\" href=\"" + getPatientURL() + "\">" + row.name + "</a>" +
                                                    "</div>" +
                                                    "<div class=\"custom-html-sub-title\"><span>Patient ID:</span> <span>" + row.patientNumber + "</span></div>" +
                                                "</div>" +
                                                "<div class=\"patient-action-container\">";
                                    if (row.hasHep)
                                        templateStr += "<a class=\"icon icon-exercise\" " + getHEPUrl() + " ></a>";

                                    if (row.hasMobileAccess && !APP_CONFIG.isDemoOn)
                                        templateStr += "<a class=\"icon icon-mobile\" href=\"" + $state.href("patientsEdit", { id: row.id, currentTab: "mobileAccess" }) + "\"></a>";
                                                                        
                                    if (row.hasMobileAccess && APP_CONFIG.isDemoOn) {
                                        //$state.go("communication.chat.chat-box", { sessionObject: item }, { reload: "communication.chat.chat-box" });
                                        
                                        templateStr += "<div class=\"icon icon-mobile\" name=\"demovideo\" ></div>";
                                        $("demovideo").click(function () {
                                            openMediaDialog();
                                        });

                                        
                                    }

                                    templateStr += "</div></div>";

                                    return templateStr;
                                },
                                customViewText: function (row) {
                                    return row.name;
                                },
                                buildUrl: function (row) {
                                    return $state.href("patientsEdit", { id: row.id });
                                }
                            }, {
                                field: "caseNo",
                                title: $filter('translate')('Case ID'),
                                sortable: "caseNo",
                                show: true,
                                dataType: "text",
                                dataTypeView: "href",
                                buildUrl: function (row) {
                                    return $state.href("patientCaseEdit", { patientId: row.id, caseId: row.caseId });
                                }
                            }, {
                                field: "injuryRegion",
                                title: "Injury Region",
                                sortable: "injuryRegion",
                                show: true,
                                dataType: "text",
                                customViewText: function (row) {
                                    //return (row.injuryRegion) ? row.injuryRegion.name : "";
                                    return row.injuryRegion;
                                },
                            },/* {
                                field: "location",
                                title: "Clinician",
                                sortable: "location",
                                show: true,
                                dataType: "text"
                            }, {
                                field: "status",
                                title: "Related Cause",
                                sortable: "status",
                                show: true,
                                dataType: "text"
                            }, */{
                                field: "birthDate",
                                title: "Date Of Birth",
                                sortable: "birthDate",
                                show: true,
                                dataType: "text",
                                customViewText: function (row) {
                                    return (row.birthDate) ? moment(row.birthDate).format("MM/DD/YYYY") : "";
                                },
                            }, {
                                field: "lastVisitDate",
                                title: "Last Visit Date",
                                sortable: "lastVisitDate",
                                show: true,
                                dataType: "text",
                                customViewText: function (row) {
                                    return (row.lastVisitDate) ? moment(row.lastVisitDate).format("MM/DD/YYYY h:mm A") : "";
                                },
                            }, {
                                field: "preExercisesPainRating",
                                title: "Pain Rating",
                                sortable: "preExercisesPainRating",
                                show: true,
                                dataTypeView: "custom-html",
                                dataType: "text",
                                templateHTML: function (row) {
                                    function getPainRating() {
                                        //row.preExercisesPainRating = 3;
                                        if (row.preExercisesPainRating) {
                                            if (row.preExercisesPainRating == 0) {
                                                return "pain-rat-0";
                                            } else if (row.preExercisesPainRating >= 1 && row.preExercisesPainRating <= 2) {
                                                return "pain-rat-1-2";
                                            } else if (row.preExercisesPainRating >= 3 && row.preExercisesPainRating <= 4) {
                                                return "pain-rat-3-4";
                                            } else if (row.preExercisesPainRating >= 5 && row.preExercisesPainRating <= 6) {
                                                return "pain-rat-5-6";
                                            } else if (row.preExercisesPainRating >= 7 && row.preExercisesPainRating <= 8) {
                                                return "pain-rat-7-8";
                                            } else if (row.preExercisesPainRating >= 9) {
                                                return "pain-rat-9-10";
                                            } else {
                                                return "pain-rat-none";
                                            }
                                        }

                                        return "";
                                    }
                                    function getPainMessage() {
                                        //row.preExercisesPainRating = 3;
                                        if (row.preExercisesPainRating) {
                                            if (row.preExercisesPainRating == 0) {
                                                return "No Pain (" + row.preExercisesPainRating + ")";
                                            } else if (row.preExercisesPainRating >= 1 && row.preExercisesPainRating <= 2) {
                                                return "Mild Pain (" + row.preExercisesPainRating + ")";
                                            } else if (row.preExercisesPainRating >= 3 && row.preExercisesPainRating <= 4) {
                                                return "Moderate Pain (" + row.preExercisesPainRating + ")";
                                            } else if (row.preExercisesPainRating >= 5 && row.preExercisesPainRating <= 6) {
                                                return "Severe Pain (" + row.preExercisesPainRating + ")";
                                            } else if (row.preExercisesPainRating >= 7 && row.preExercisesPainRating <= 8) {
                                                return "Very Severe Pain (" + row.preExercisesPainRating + ")";
                                            } else if (row.preExercisesPainRating >= 9) {
                                                return "Worst Pain Possible (" + row.preExercisesPainRating + ")";
                                            } else {
                                                return "-";
                                            }
                                        }

                                        return "";
                                    }
                                    return "<div class=\"patient-pain-container\">" +
                                                    "<a class=\"icon rating-icon " + getPainRating() + "\" title=\"" + getPainMessage() + "\"></a>" +
                                           "</div>";

                                },
                                customViewText: function (row) {
                                    return row.name;
                                },
                                buildUrl: function (row) {
                                    return "";
                                }
                            },
                            {
                                field: "status",
                                title: "Status",
                                sortable: "status",
                                show: true,
                                dataType: "text",
                                dataTypeView: "status",
                                getStatusSecondaryClass: function (row) {
                                    return "status-" + row.status;
                                }
                            }, {
                                field: "action",
                                title: "Action",
                                class: "",
                                dataType: "command"
                            }
                        ];
                    },
                    hideAddButton: true,
                    showDropDownAddButton: true,
                    moreExtraButtonList: [{
                        title: "Chat",
                        iconClass: "fa fa-commenting-o",
                        click: function (row) { }
                    }, {
                        title: "Email",
                        iconClass: "fa fa-envelope-o",
                        click: function (row) { }
                    }, ],
                    dropDownAddButtonOptions: [
                        {
                            title: "New Patient",
                            state: "",
                            onOptionClick: function () {
                                $state.go("patients.patients-add");
                            }
                        }, {
                            title: "New Appointment",
                            state: "",
                            onOptionClick: function () {
                                AppointmentFactory.newAppointment({}, function (result) {

                                });
                            }
                        }, {
                            title: "New Referral Source",
                            state: "",
                            onOptionClick: function () {
                                referralDialogFactory.showPhysicianModal({}, function (data) { });
                            }
                        },
                    ],
                    onAdd_Click: function () {
                        $state.go("patients.patients-add");
                    },
                    onEdit_Click: function (row) {
                        $state.go("patientsEdit", { id: row.id });
                    },
                    pageName: "Patients",
                    menuButtons: [
                        {
                            click: function (row) {
                                resource.setInactive({ id: row.id }, {}, function () {
                                    resource.reloadCommand.reload();
                                });
                            },

                            iconClass: "icon icon-user-times"
                        }
                    ],
                    headerButtons: [],
                    headerLeftItems: []
                };
                return resource;
            }
    ]);
