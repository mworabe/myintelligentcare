namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddedHepGroupTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.hep_groups",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    patient_id = c.Long(),
                    patient_case_id = c.Long(),
                    name = c.String(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),

                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.patients", t => t.patient_id)
                .ForeignKey("dbo.patient_cases", t => t.patient_case_id)
                .Index(t => t.patient_id)
                .Index(t => t.patient_case_id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.hep_groups", "patient_case_id", "dbo.patient_cases");
            DropForeignKey("dbo.hep_groups", "patient_id", "dbo.patients");
            DropIndex("dbo.hep_groups", new[] { "patient_case_id" });
            DropIndex("dbo.hep_groups", new[] { "patient_id" });
            DropTable("dbo.hep_groups");
        }
    }
}
