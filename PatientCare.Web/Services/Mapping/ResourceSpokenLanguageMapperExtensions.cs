﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.ResourceScore;
using PatientCare.Web.Api.Models.ResourceSpokenLanguages;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class ResourceSpokenLanguageMapperExtensions
    {
        public static EmitMapperMapperProvider ResourceSpokenLanguageRegisterMappings(
            this EmitMapperMapperProvider mapperProvider) 
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSpokenLanguage, ResourceSpokenLanguageResponse>(new ExtendedMapConfig<ResourceSpokenLanguage, ResourceSpokenLanguageResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSpokenLanguage[], ResourceSpokenLanguageResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSpokenLanguageRequest, ResourceSpokenLanguage>(new ExtendedMapConfig<ResourceSpokenLanguageRequest, ResourceSpokenLanguage>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSpokenLanguageFilterRequest, ResourceSpokenLanguage>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ResourceSpokenLanguageFilterRequest[], ResourceSpokenLanguage[]>());

            return mapperProvider;
        }
    }
}