namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterIsActiveFrequencyIntoHepDetailTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.hep_detail", "is_active_frequency", c => c.Boolean(nullable: true, defaultValue: false));
        }
        
        public override void Down()
        {

        }
    }
}
