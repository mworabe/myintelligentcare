﻿namespace PatientCare.Web.Api.Models.ResourceIndustry
{
    public class ResourceIndustryRequest
    {
        public long Id { get; set; }
        
        public long? ResourceId { get; set; }
        
        public long? IndustryId { get; set; }
    }
}