﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("LoginHistory", ["$resource", function ($resource) {
    var resource = $resource("api/UserLoginHistorys/",
        null, //parameters default
        {
            'query': { method: "GET", params: {}, isArray: false }
        });

    resource.getTableOption = {
        pageName: "Login History",
        hideBulkEdit: true,
        hideAddButton: true,
        searchField: "username", // always Binded to Username, for Login History we are not Allowing search with any other fields.
        cols: function () {
            return [
                { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "userId", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "action", title: "Action", sortable: "action", show: true, dataType: "text", required: true },
                { field: "username", title: "Username", sortable: "username", show: true, dataType: "text", required: false, minimum: 0 },
                {
                    field: "logDateTime", title: "Date", sortable: "logDateTime", show: true, dataType: "date", required: false, minimum: 0,
                    customViewText: function (row) {
                        return moment(row.logDateTime).format("LLL");
                    }
                },
                /*{ field: "action", title: ".", dataType: "command" },*/
            ];
        }
    };
    return resource;
}
]);
