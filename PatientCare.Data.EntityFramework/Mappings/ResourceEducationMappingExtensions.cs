﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ResourceEducationMappingExtensions
    {
        public static DbModelBuilder MapResourceEducation(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ResourceEducation>().ToTable("resource_educations").HasKey(i => i.Id);

            modelBuilder.Entity<ResourceEducation>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ResourceEducation>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ResourceEducation>()
                .Property(p => p.ResourceId)
                .HasColumnName("resource_id")
                .IsOptional();

            modelBuilder.Entity<ResourceEducation>()
                .HasRequired<Resource>(p => p.Resource)
                .WithMany()
                .HasForeignKey(p => p.ResourceId);

            modelBuilder.Entity<ResourceEducation>()
                .Property(p => p.Subject)
                .HasColumnName("subject")
                .IsUnicode()
                .HasMaxLength(255)
                .IsOptional();

            //modelBuilder.Entity<ResourceEducation>()
            //    .Property(p => p.TypeOfDegreeOrCertObtained)
            //    .HasColumnName("type_of_degree_or_cert_obtained")
            //    .IsUnicode()
            //    .HasMaxLength(255)
            //    .IsRequired();

            modelBuilder.Entity<ResourceEducation>()
                .Property(p => p.NameOfSchool)
                .HasColumnName("name_of_school")
                .IsUnicode()
                .HasMaxLength(255)
                .IsOptional();

            modelBuilder.Entity<ResourceEducation>()
                .Property(p => p.DateObtained)
                .HasColumnName("date_obtained")
                .IsOptional();

            modelBuilder.Entity<ResourceEducation>()
                .Property(p => p.StartDate)
                .HasColumnName("start_date")
                .IsOptional();

            modelBuilder.Entity<ResourceEducation>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

            modelBuilder.Entity<ResourceEducation>()
                .Property(p => p.ResourceDesignationId)
                .HasColumnName("resource_designation_id")
                .IsOptional();

            modelBuilder.Entity<ResourceEducation>()
                .HasOptional<DropDownConfig>(p => p.ResourceDesignation)
                .WithMany()
                .HasForeignKey(p => p.ResourceDesignationId)
                .WillCascadeOnDelete(false);

            return modelBuilder;
        }
    }
}
