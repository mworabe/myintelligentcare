﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("TermsAndConditionsService", ["$resource", function ($resource) {
        var resource = $resource("api/nda/:id/:controller",
            null,
            {
                'update': { method: "PUT" },
                'query': { method: "GET", params: {}, isArray: false },
            });

        return resource;
    }
    ]);