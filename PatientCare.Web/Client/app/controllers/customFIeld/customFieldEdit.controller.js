﻿(function customFieldEditCtrlIIFE() {
    "use strict";
    angular.module("EDZoutstaffingPortalApp")
        .controller("CustomFieldEditCtrl", [
            "$stateParams",
            customFieldEditCtrl
        ]);

    function customFieldEditCtrl($stateParams) {
        var modelId = $stateParams.id;
        this.titleText = modelId ? "Edit Custom Field" : "New Custom Field";
    }

})();

