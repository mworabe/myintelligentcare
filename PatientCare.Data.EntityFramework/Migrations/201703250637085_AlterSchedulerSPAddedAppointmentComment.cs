namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AlterSchedulerSPAddedAppointmentComment : DbMigration
    {
        public override void Up()
        {
            AlterStoredProcedure(
                 "dbo.sp_get_appointment_for_schedular",
                 p => new
                 {
                     startDate = p.DateTime(defaultValueSql: null),
                     endDate = p.DateTime(defaultValueSql: null),
                     clinician = p.String(defaultValueSql: null),
                     Clinic = p.String(defaultValueSql: null),
                     Status = p.String(defaultValueSql: null)
                 },
                 body:
                     @"SELECT  CAST(pa.Id AS  BIGINT) AS AppointmentId,CAST(res.Id AS  BIGINT) AS ClinicianId,(res.last_name +' '+res.first_name) AS ClinicianName,
    	            CAST(p.Id AS  BIGINT) AS PatientId,(p.last_name +' '+ p.first_name) AS PatientName,P.patient_number AS PatientNumber,
    	            p.photo_file_name AS PhotoFileName,p.birth_DATE AS BirthDATE,p.cell_number AS CellNumber,p.email AS Email,P.patient_number, 
    	            CAST(pa.case_id AS  BIGINT) AS CaseId,pcase.case_no AS CaseNo,pcase.case_status AS CaseStatus,
    	            CAST(pcase.injury_region_id AS  BIGINT) AS InjuryRegionId,injury.name AS InjuryRegionName,
    	            CAST(pcase.primay_diagnosis_id  AS  BIGINT)  AS PrimayDiagnosisId,diag.name AS PrimayDiagnosisName,pcase.return_to AS ReturnTo,
    	            CAST(pcase.refering_physician_id AS  BIGINT)  AS ReferingPhysicianId,(refphy.last_name +' '+ refphy.first_name) AS ReferingPhysicianName,		
    	            CAST(pa.appointment_type_id AS  BIGINT)  AS AppointmentTypeId,aptype.Name AS AppointmentTypeName,aptype.color AS AppointmentTypeColor,	
    	            CAST(pa.facility_id AS  BIGINT)  AS FacilityId,pa.clinic_location_id AS ClinicLocationId,clocation.address as ClinicLocation,	
    	            pa.start_DATE AS StartDATE,pa.end_DATE AS EndDATE,appointment_status AS AppointmentStatus,
    pa.is_mobile as IsMobile,pa.is_reschedule as IsReschedule , pa.additional_detail As AppointmentComment
    FROM patient_appointments pa
    	            LEFT OUTER JOIN patients p ON pa.patient_id = p.Id
    	            LEFT OUTER JOIN patient_cases pcase ON pa.case_id = pcase.Id
    	            LEFT OUTER JOIN resources res ON pa.clinician_id = res.Id
    	            LEFT OUTER JOIN appointment_types aptype ON pa.appointment_type_id = aptype.Id
    	            LEFT OUTER JOIN injury_regions AS injury ON pcase.injury_region_id = injury.Id
    	            LEFT OUTER JOIN referral_physicians AS refphy ON pcase.refering_physician_id = refphy.Id
    	            LEFT OUTER JOIN diagnosis AS diag ON pcase.primay_diagnosis_id = diag.Id
    		            Left OUTER JOIN clinic_locations as clocation on pa.clinic_location_id = clocation.Id
    WHERE 
    	(@clinician = '' or Charindex(','+ cast(pa.clinician_id as varchar(max))+',', @clinician) > 0) 
    AND (@Clinic = '' or Charindex(','+ cast(pa.facility_id as varchar(max))+',', @Clinic) > 0) 
    AND (@Status = '' or Charindex(','+ cast(pa.appointment_status as varchar(max))+',', @Status) > 0) 
    AND CAST(pa.start_DATE AS DATE) >= CAST(@startDate AS DATE)
    AND CAST(pa.start_DATE AS DATE) <= CAST(@endDate as DATE)"
                );
        }

        public override void Down()
        {
            AlterStoredProcedure(
                 "dbo.sp_get_appointment_for_schedular",
                 p => new
                 {
                     startDate = p.DateTime(defaultValueSql: null),
                     endDate = p.DateTime(defaultValueSql: null),
                     clinician = p.String(defaultValueSql: null),
                     Clinic = p.String(defaultValueSql: null),
                     Status = p.String(defaultValueSql: null)
                 },
                 body:
                     @"SELECT  CAST(pa.Id AS  BIGINT) AS AppointmentId,CAST(res.Id AS  BIGINT) AS ClinicianId,(res.last_name +' '+res.first_name) AS ClinicianName,
    	            CAST(p.Id AS  BIGINT) AS PatientId,(p.last_name +' '+ p.first_name) AS PatientName,P.patient_number AS PatientNumber,
    	            p.photo_file_name AS PhotoFileName,p.birth_DATE AS BirthDATE,p.cell_number AS CellNumber,p.email AS Email,P.patient_number, 
    	            CAST(pa.case_id AS  BIGINT) AS CaseId,pcase.case_no AS CaseNo,pcase.case_status AS CaseStatus,
    	            CAST(pcase.injury_region_id AS  BIGINT) AS InjuryRegionId,injury.name AS InjuryRegionName,
    	            CAST(pcase.primay_diagnosis_id  AS  BIGINT)  AS PrimayDiagnosisId,diag.name AS PrimayDiagnosisName,pcase.return_to AS ReturnTo,
    	            CAST(pcase.refering_physician_id AS  BIGINT)  AS ReferingPhysicianId,(refphy.last_name +' '+ refphy.first_name) AS ReferingPhysicianName,		
    	            CAST(pa.appointment_type_id AS  BIGINT)  AS AppointmentTypeId,aptype.Name AS AppointmentTypeName,aptype.color AS AppointmentTypeColor,	
    	            CAST(pa.facility_id AS  BIGINT)  AS FacilityId,pa.clinic_location_id AS ClinicLocationId,clocation.address as ClinicLocation,	
    	            pa.start_DATE AS StartDATE,pa.end_DATE AS EndDATE,appointment_status AS AppointmentStatus,
    pa.is_mobile as IsMobile,pa.is_reschedule as IsReschedule 
    FROM patient_appointments pa
    	            LEFT OUTER JOIN patients p ON pa.patient_id = p.Id
    	            LEFT OUTER JOIN patient_cases pcase ON pa.case_id = pcase.Id
    	            LEFT OUTER JOIN resources res ON pa.clinician_id = res.Id
    	            LEFT OUTER JOIN appointment_types aptype ON pa.appointment_type_id = aptype.Id
    	            LEFT OUTER JOIN injury_regions AS injury ON pcase.injury_region_id = injury.Id
    	            LEFT OUTER JOIN referral_physicians AS refphy ON pcase.refering_physician_id = refphy.Id
    	            LEFT OUTER JOIN diagnosis AS diag ON pcase.primay_diagnosis_id = diag.Id
    		            Left OUTER JOIN clinic_locations as clocation on pa.clinic_location_id = clocation.Id
    WHERE 
    	(@clinician = '' or Charindex(','+ cast(pa.clinician_id as varchar(max))+',', @clinician) > 0) 
    AND (@Clinic = '' or Charindex(','+ cast(pa.facility_id as varchar(max))+',', @Clinic) > 0) 
    AND (@Status = '' or Charindex(','+ cast(pa.appointment_status as varchar(max))+',', @Status) > 0) 
    AND CAST(pa.start_DATE AS DATE) >= CAST(@startDate AS DATE)
    AND CAST(pa.start_DATE AS DATE) <= CAST(@endDate as DATE)"
                );
        }
    }
}
