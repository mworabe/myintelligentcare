﻿'use strict'

angular.module('EDZoutstaffingPortalApp')
    .factory('userTimeZoneFactory', ['Resource', 'RoleSetup', function (Resource, RoleSetup) {
        var timeZoneList = [];
        var resuorceConfigs;
        var projectConfigs;
        var taskConfigs;
        var intakeConfigs;
        var assignmentConfigs;


        function loadTimeZone(listener) {
            if (timeZoneList && timeZoneList.length > 0) {
                listener.resolve(timeZoneList);
            } else {
                Resource.getTimeZones().$promise.then(function (response) {
                    timeZoneList = response;
                    listener.resolve(response);
                });
            }
        }
        function loadDefaultResourceConfigs(listener) {
            if (resuorceConfigs) {
                listener.resolve(resuorceConfigs);
            } else {
                RoleSetup.resourceConfigLayout().$promise.then(function (response) {
                    resuorceConfigs = response;
                    listener.resolve(response);
                });
            }
        }
        function loadDefaultIntakeConfigs(listener) {
            if (resuorceConfigs) {
                listener.resolve(resuorceConfigs);
            } else {
                RoleSetup.intakeConfigLayout().$promise.then(function (response) {
                    intakeConfigs = response;
                    listener.resolve(response);
                });
            }
        }
        /*
        function loadDefaultAssignmentConfigs(listener) {
            if (resuorceConfigs) {
                listener.resolve(resuorceConfigs);
            } else {
                RoleSetup.assignmentConfigLayout().$promise.then(function (response) {
                    resuorceConfigs = response;
                    listener.resolve(response);
                });
            }
        }
        */
        function clearResourceDefaultConfigs() {
            resuorceConfigs = null;
        }
        function clearProjectDefaultConfigs() {
            projectConfigs = null;
        }
        function clearIntakeDefaultConfigs() {
            intakeConfigs = null;
        }
        function clearTaskDefaultConfigs() {
            taskConfigs = null;
        }
        function clearAssignmentDefaultConfigs() {
            assignmentConfigs = null;
        }
        return {
            getTimeZone: function (listener) {
                loadTimeZone(listener);
            },
            getResourceDefaultConfigs: function (listener) {
                loadDefaultResourceConfigs(listener);
            },
            getIntakeDefaultConfigs: function (listener) {
                loadDefaultIntakeConfigs(listener);
            },

            clearResourceDefaultConfigs: clearResourceDefaultConfigs,
            clearProjectDefaultConfigs: clearProjectDefaultConfigs,
            clearIntakeDefaultConfigs: clearIntakeDefaultConfigs,
            clearTaskDefaultConfigs: clearTaskDefaultConfigs,
            clearAssignmentDefaultConfigs: clearAssignmentDefaultConfigs,
        }
    }]);