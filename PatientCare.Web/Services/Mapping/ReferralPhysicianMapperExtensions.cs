﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.Api.Models.ReferralPhysicians;

namespace PatientCare.Web.Services.Mapping
{
    public static class ReferralPhysicianMapperExtensions
    {
        public static EmitMapperMapperProvider ReferralPhysicianRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ReferralPhysician, ReferralPhysicianResponse>
                    (new ExtendedMapConfig<ReferralPhysician, ReferralPhysicianResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ReferralPhysician[], ReferralPhysicianResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ReferralPhysician, ReferralPhysicianAutoCompleteResponse>
                    (new ExtendedMapConfig<ReferralPhysician, ReferralPhysicianAutoCompleteResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ReferralPhysician[], ReferralPhysicianAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ReferralPhysicianRequest, ReferralPhysician>
                    (new ExtendedMapConfig<ReferralPhysicianRequest, ReferralPhysician>()));

            return mapperProvider;
        }
    }
}