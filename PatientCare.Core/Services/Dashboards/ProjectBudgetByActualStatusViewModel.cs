﻿using System;

namespace PatientCare.Core.Services.Dashboards
{
    public class ProjectBudgetByActualStatusViewModel
    {
        public decimal? Budget { get; set; }
        public decimal? Actual { get; set; }
        public decimal? Variance { get; set; }
        public DateTime? ProjectStartDate { get; set; }
        public DateTime? ProjectEndDate { get; set; }
    }
}
