﻿namespace PatientCare.Web.Api.Models.ResourceSpokenLanguages
{
    public class ResourceSpokenLanguageRequest
    {
        public long Id { get; set; }

        public long ResourceId { get; set; }

        public long LanguageId { get; set; }

        public int YearsOfExpirience { get; set; }

        public int SpeakingProficiency { get; set; }

        public int WritingProficiency { get; set; }

        public FileRequest[] Files { get; set; }
    }
}