﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
"use strict";

angular.module("EDZoutstaffingPortalApp")
.service("JobTitle", ["$resource", function ($resource) {
    var resource = $resource("api/JobTitles/:id/:controller",
        null, //parameters default
        {
            'update': { method: "PUT" },
            'query': { method: "GET", params: {}, isArray: false }
        });

    resource.getTableOption = {
        pageName: "Job Titles",
        cols: function () {
            return [
                { field: "id", title: "Id", sortable: "id", show: false, dataType: "number" },
                { field: "name", title: "Name", sortable: "name", show: true, dataType: "text", required: true },
                { field: "rate", title: "Rate", sortable: "rate", show: true, dataType: "number", dataTypeView: 'currency', required: false, minimum: 0 },
                { field: "action", title: ".", dataType: "command" },
            ];
        }
    };
    return resource;
}
]);
