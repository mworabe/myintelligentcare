﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("DateTimeHelper", [
        function() {
            var dateTypeEnum = ["Day", "Month", "Year"];
            return {
                getColumnName:
                    function(date, type) {
                        if (type === "Day")
                            return date.format("MM/DD/YYYY");
                        if (type === "Month")
                            return date.format("MM/YYYY");
                        if (type === "mnth")
                            return date.format("MM-YYYY");
                        return date.format("YYYY");
                    },
                dateTypeEnum: function() {
                    return dateTypeEnum;
                }
            };
        }
    ]);