﻿using System;
using System.Linq.Expressions;
using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.AbstractRepository;

namespace PatientCare.Web.Services.Validation
{
    public static class ValidationHelpers
    {
        public static bool ItemBeExist<T>(IDataContextScopeFactory dbContextScopeFactory, IRepository<T> repository, long id) where T : Entity
        {
            if (dbContextScopeFactory == null) throw new ArgumentNullException("dbContextScopeFactory");
            if (repository == null) throw new ArgumentNullException("repository");
            using (dbContextScopeFactory.CreateReadOnly())
            {
                return repository.Exists(new FilterQuery<T>().AddFilter(item => item.Id == id));
            }
        }

        public static bool ItemBeUnique<T>(IDataContextScopeFactory dbContextScopeFactory, IRepository<T> repository, string name) where T : NamedEntity
        {
            if (dbContextScopeFactory == null) throw new ArgumentNullException("dbContextScopeFactory");
            if (repository == null) throw new ArgumentNullException("repository");
            using (dbContextScopeFactory.CreateReadOnly())
            {
                return !repository.Exists(new FilterQuery<T>().AddFilter(p => p.Name == name));
            }
        }

        public static bool ItemNameBeUnique<T>(IDataContextScopeFactory dbContextScopeFactory, IRepository<T> repository, string name, long id) where T : NamedEntity
        {
            if (dbContextScopeFactory == null) throw new ArgumentNullException("dbContextScopeFactory");
            if (repository == null) throw new ArgumentNullException("repository");
            using (dbContextScopeFactory.CreateReadOnly())
            {
                return !repository.Exists(new FilterQuery<T>().AddFilter(p => p.Name == name && p.Id != id));
            }
        }


        public static bool ItemBeUnique<T>(IDataContextScopeFactory dbContextScopeFactory, IRepository<T> repository, Expression<Func<T, bool>> func) where T : Entity
        {
            if (dbContextScopeFactory == null) throw new ArgumentNullException("dbContextScopeFactory");
            if (repository == null) throw new ArgumentNullException("repository");
            using (dbContextScopeFactory.CreateReadOnly())
            {
                return !repository.Exists(new FilterQuery<T>().AddFilter(func));
            }
        }
    }
}