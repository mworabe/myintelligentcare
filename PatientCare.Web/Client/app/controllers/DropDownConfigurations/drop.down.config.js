﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("projectConfigurationData", {
                url: "/project-config-data",
                templateUrl: "Client/app/controllers/DropDownConfigurations/DropDownConfigurations.html",
                controller: "DropDownConfigurationController as DropDownConfigurationController",
                data: {
                    security: {
                        authenticated: true
                    }
                }
            }).state("configurationData", {
                url: "/config-data",
                params: { currentView: null, currentTab:null },
                templateUrl: "Client/app/controllers/DropDownConfigurations/Drop-Down-Config-View.html",
                controller: "DropDownConfigurationViewController as DropDownConfigurationViewController",
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });