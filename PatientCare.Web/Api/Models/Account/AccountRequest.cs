﻿namespace PatientCare.Web.Api.Models.Account
{
    public class AccountRequest
    {
        public string Id { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }

        public long? ResourceId { get; set; }

        public long? PatientId { get; set; }

        public string Role { get; set; }

        public bool isUpdate { get; set; }

        public bool? IsNDA { get; set; }
    }
}