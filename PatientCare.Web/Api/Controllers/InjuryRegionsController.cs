﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.InjuryRegions;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Injury Regions operations.
    /// </summary>
    [Authorize]
    public sealed class InjuryRegionsController : ApiBaseController
    {
        /// <summary>
        /// Description : Injury Regions IConfigurationDataService.
        /// </summary>
        private readonly IConfigurationDataService _configurationDataService;
        /// <summary>
        /// Description : Injury Regions IInjuryRegionRepository.
        /// </summary>
        private readonly IInjuryRegionRepository _injuryRegionRepository;
        /// <summary>
        /// Description : Injury Regions IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<InjuryRegion> _readOptions = new ReadOptions<InjuryRegion>();

        /// <summary>
        /// Description : Initializes a new instance of the Injury Regions Class.  
        /// </summary>
        /// <param name="configurationDataService">IConfiguration Data Service</param>
        /// <param name="injuryRegionRepository">IInjuryRegion Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public InjuryRegionsController
            (
                IConfigurationDataService configurationDataService,
                IInjuryRegionRepository injuryRegionRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _configurationDataService = configurationDataService;
            _injuryRegionRepository = injuryRegionRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/InjuryRegions/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get All Injury Regions.
        /// </summary>
        /// <param name="request">Injury Regions List Request Model</param>
        /// <returns>Injury Regions Response Model</returns>
        [HttpGet]
        [Route("api/InjuryRegions")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<InjuryRegionResponse>))]
        public IHttpActionResult GetAllInjuryRegion([FromUri]InjuryRegionListRequest request)
        {
            var query = PagingExtensions<InjuryRegion>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            var readOptions = new ReadOptions<InjuryRegion>().AsReadOnly();
            var page = _injuryRegionRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<InjuryRegion[], InjuryRegionResponse[]>(page.Entities.ToArray());

            var response = new ListResponse<InjuryRegionResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Create New Injury Regions.   
        /// </summary>
        /// <param name="InjuryRegionRequest">Injury Regions Request Model</param>
        /// <returns>Injury Regions Response Model</returns>
        [HttpPost]
        [Route("api/InjuryRegions")]
        [SwaggerResponse(201, "Created", typeof(InjuryRegionResponse))]
        public async Task<IHttpActionResult> CreateInjuryRegion([FromBody]InjuryRegionRequest InjuryRegionRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_injuryRegionRepository.Exists(FilterQuery.Create<InjuryRegion>().AddFilter(p => p.Name == InjuryRegionRequest.Name)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<InjuryRegion>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var InjuryRegion = Mapper.Map<InjuryRegionRequest, InjuryRegion>(InjuryRegionRequest);
            var a =  await _configurationDataService.CreateInjuryRegionAsync(InjuryRegion);

            return Created(GetEntityLocation(InjuryRegion.Id), Mapper.Map<InjuryRegion, InjuryRegionResponse>(InjuryRegion));
        }

        /// <summary>
        /// Method Description : Using for Update Injury Regions.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="InjuryRegionRequest">Injury Regions Request Model</param>
        /// <returns>Injury Regions Response Model</returns>
        [HttpPut]
        [Route("api/InjuryRegions/{id}")]
        public async Task<IHttpActionResult> EditInjuryRegion(long id, [FromBody] InjuryRegionRequest InjuryRegionRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_injuryRegionRepository.Exists(FilterQuery.Create<InjuryRegion>().AddFilter(p => p.Name == InjuryRegionRequest.Name && p.Id != InjuryRegionRequest.Id)))
            {
                ModelState.AddModelError(ReflectionHelper.GetMemberName<InjuryRegion>(p => p.Name), DisplayResources.Error_ItemNameExists);
                return BadRequest(ModelState);
            }

            var InjuryRegion = Mapper.Map<InjuryRegionRequest, InjuryRegion>(InjuryRegionRequest);
            InjuryRegion.Id = id;

            await _configurationDataService.UpdateInjuryRegionAsync(InjuryRegion);

            return Ok(Mapper.Map<InjuryRegion, InjuryRegionResponse>(InjuryRegion));
        }

        /// <summary>
        /// Method Description : Using for Delete Injury Regions by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Injury Regions Response Model</returns>
        [HttpDelete]
        [Route("api/InjuryRegions/{id}")]
        public async Task<IHttpActionResult> DeleteInjuryRegionItem(long id)
        {
            var success = await _configurationDataService.DeleteInjuryRegionAsync(id);
            if (!success)
            {
                return NotFound();
            }

            return Ok(new EntryType { Id = id });
        }

        /// <summary>
        /// Method Description : Using for Get Injury Regions by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Injury Regions Response Model</returns>
        [HttpGet]
        [Route("api/InjuryRegions/{id}")]
        public async Task<IHttpActionResult> GetInjuryRegion(long id)
        {
            var InjuryRegion = await _configurationDataService.ReadInjuryRegionAsync(id);
            if (InjuryRegion == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<InjuryRegion, InjuryRegionResponse>(InjuryRegion);
            return Ok(viewModel);
        }

        private static Expression<Func<InjuryRegion, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return clininc => clininc.Name.Contains(searchPhrase);
        }

        private static Expression<Func<InjuryRegion, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return InjuryRegion => InjuryRegion.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Injury Regions.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Injury Regions Auto Complete Response Model</returns>
        [HttpGet]
        [Route("api/InjuryRegions/autocomplete")]
        [ResponseType(typeof(InjuryRegionAutoCompleteResponse))]
        public async Task<IHttpActionResult> Autocomplete([FromUri]string search)
        {
            var filter = new FilterQuery<InjuryRegion>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => x.Name.ToLower().StartsWith(search.ToLower()));
            }

            var autocompleteResult = await _injuryRegionRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<InjuryRegion[], InjuryRegionAutoCompleteResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }
    }
}
