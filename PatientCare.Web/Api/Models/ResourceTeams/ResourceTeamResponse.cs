﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.Resources;

namespace PatientCare.Web.Api.Models.ResourceTeams
{
    [DataContract]
    public class ResourceTeamResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long ResourceCompanyId { get; set; }

        [DataMember]
        public PicklistResponse ResourceCompany { get; set; }

        [DataMember]
        public PicklistResponse[] Resources { get; set; }
    }
}