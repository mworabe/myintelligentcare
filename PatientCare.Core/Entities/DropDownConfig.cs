﻿using PatientCare.Core.Entities.Abstract;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Drop Down Config.
    /// </summary>
    public class DropDownConfig : Entity
    {
        public string Value { get; set; }        
        public string Name { get; set; }
        public string DropDownType { get; set; }

        public string ConfigField1 { get; set; }

        public string ConfigField2 { get; set; }

        public string ConfigField3 { get; set; }

        public string ConfigField4 { get; set; }

        public string ConfigField5 { get; set; }

        public virtual ICollection<Exercise_injury> exerciseinjuries { get; set; }
    }
}
