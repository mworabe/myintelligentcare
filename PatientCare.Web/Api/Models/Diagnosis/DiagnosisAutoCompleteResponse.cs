﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Diagnosiss
{
    [DataContract]
    public class DiagnosisAutoCompleteResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}