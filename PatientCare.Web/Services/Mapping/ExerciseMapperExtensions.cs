﻿using System;
using EmitMapper;
using PatientCare.Services.Mapping;
using PatientCare.Core.Entities;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.Exercises;
using PatientCare.Web.Api.Models.ExerciseMedias;
using EmitMapper.MappingConfiguration;
using System.Linq;
using PatientCare.Web.Api.Models.Exercise_ExerciseActivities;
using PatientCare.Web.Api.Models.Exercise_injury;

namespace PatientCare.Web.Services.Mapping
{
    public static class ExerciseMapperExtensions
    {
        public static EmitMapperMapperProvider ExerciseRegisterMappings(
             this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise, ExerciseResponse>
                    (new ExtendedMapConfig<Exercise, ExerciseResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise[], ExerciseResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise, ExerciseListResponse>
                    (new ExtendedMapConfig<Exercise, ExerciseListResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise[], ExerciseListResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise, PicklistResponse>
                    (new ExtendedMapConfig<Exercise, PicklistResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<Exercise, PicklistDefaultResponse>
                    (new ExtendedMapConfig<Exercise, PicklistDefaultResponse>()));

            var mediasMapper =
                      ObjectMapperManager.DefaultInstance.GetMapper<ExerciseMediaRequest, ExerciseMedia>(
                      new DefaultMapConfig());
            var activityMapper =
                     ObjectMapperManager.DefaultInstance.GetMapper<Exercise_ExerciseActivityRequest, Exercise_ExerciseActivity>(
                     new DefaultMapConfig());

            var regionMapper =
                     ObjectMapperManager.DefaultInstance.GetMapper<Exercise_injuryRequest, Exercise_injury>(
                     new DefaultMapConfig());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseRequest, Exercise>(
                    new ExtendedMapConfig<ExerciseRequest, Exercise>()
                    .ForMember(x => x.Medias, y => y.Medias != null ? y.Medias.Select(i => mediasMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.exercises, y => y.exercises != null ? y.exercises.Select(i => activityMapper.Map(i)).ToList() : null)
                    .ForMember(x => x.injuries, y => y.injuries != null ? y.injuries.Select(i => regionMapper.Map(i)).ToList() : null)
                    ));
            return mapperProvider;
        }
    }
}