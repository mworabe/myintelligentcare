﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ExerciseRatingMappingExtensions
    {
        public static DbModelBuilder MapExerciseRating(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ExerciseRating>().ToTable("exercise_ratings").HasKey(i => i.Id);

            modelBuilder.Entity<ExerciseRating>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ExerciseRating>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ExerciseRating>()
                .Property(p => p.ExerciseId)
                .HasColumnName("exercise_id")
                .IsOptional();

            modelBuilder.Entity<ExerciseRating>()
                .HasOptional<Exercise>(p => p.Exercise)
                .WithMany()
                .HasForeignKey(p => p.ExerciseId);

            modelBuilder.Entity<ExerciseRating>()
                .Property(p => p.HepId)
                .HasColumnName("hep_id")
                .IsOptional();

            modelBuilder.Entity<ExerciseRating>()
                .Property(p => p.HepDetailId)
                .HasColumnName("hep_detail_id")
                .IsOptional();

            //modelBuilder.Entity<ExerciseRating>()
            //    .HasOptional<HepDetail>(p => p.HepDetail)
            //    .WithMany()
            //    .HasForeignKey(p => p.HepDetailId);

            modelBuilder.Entity<ExerciseRating>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<ExerciseRating>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<ExerciseRating>()
                .Property(p => p.PatientCaseId)
                .HasColumnName("patient_case_id")
                .IsOptional();

            modelBuilder.Entity<ExerciseRating>()
                .HasOptional<PatientCase>(p => p.PatientCase)
                .WithMany()
                .HasForeignKey(p => p.PatientCaseId);

            modelBuilder.Entity<ExerciseRating>()
                .Property(p => p.Rating)
                .HasColumnName("rating")
                .IsOptional();
            
            modelBuilder.Entity<ExerciseRating>()
                .Property(p => p.RatingDate)
                .HasColumnName("rating_date")
                .IsOptional();
            
            return modelBuilder;
        }
    }
}
