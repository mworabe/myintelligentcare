@echo off

rem call "%~dp0run-msbuild" "..\EDZoutstaffingportal.Web\EDZoutstaffingportal.Web.csproj" "Package" %~1 "/p:_PackageTempDir=..\package_output;AutoParameterizationWebConfigConnectionStrings=false" || goto :error
call "%~dp0run-msbuild" "..\EDZoutstaffingportal.Web\EDZoutstaffingportal.Web.csproj" Rebuild %~1 "/p:DeployOnBuild=true" "/p:PublishProfile=%~dp0LocalPublish.pubxml" || goto :error

goto :done

:error
echo Server build failed with error %errorlevel%.

:done
exit /b %errorlevel%
