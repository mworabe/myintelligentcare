"use strict";

(function edzFileCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzFileCustomFieldOptions", [
            "toaster",
            "Upload",
            "Resource",
            "$rootScope",
            edzFileCustomFieldOptions
        ]);

    function edzFileCustomFieldOptions(toaster, uploader, resource,  $rootScope) {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/file/edzFileCustomField.html",
            restrict: "E",
            scope: false,
            controller: ["$scope", function($scope) {

                    $scope.validation = {};
                    $scope.validation.require = $scope.require;

                    function reject() {
                    }

                    function updateFileName() {
                        $scope.fileName = $scope.value ? $scope.value.substring(36) : undefined;
                    }

                    function loadData() {
                        updateFileName();
                    }

                    function activate() {
                        loadData();
                    }

                    function onDeleteClick() {
                        $scope.value = null;
                        $scope.fileName = null;
                        $scope.info = null;
                        $scope.fileModel = undefined;
                    }

                $scope.upload = function (file, $invalidFile) {
                    if ($invalidFile && $invalidFile.length > 0) {
                        toaster.pop("error", "", "Max File Size 5MB");
                    }
                    if (!file)
                        return;
                    $rootScope.$emit("custom-file-upload-start");
                    file.upload = resource.uploadFile(uploader, file, function () {
                    }, reject);
                    file.upload.then(function (response) {
                        
                        $scope.value = response.data.fileName;
                        updateFileName();
                        $rootScope.$emit("custom-file-upload-end");
                    }, function (response) {
                        if (response.status > 0) {
                            toaster.pop("error", "", "file upload error");
                        }
                    });
                }

                activate();
                $scope.onDelete_Click = onDeleteClick;
            }]
        };
    }
})();
