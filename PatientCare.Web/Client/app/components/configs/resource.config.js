﻿// This software is the exclusive property of 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems and is protected under copyright law.  
// This software is furnished under a license agreement and may be used and/or copied only in accordance with the terms and conditions of the license.  
// 
// Copyright © 2015 -- 3 Fuerzas Technology Solutions, LLC d/b/a EDZ Systems.  All rights reserved.
//
'use strict';

angular.module("EDZoutstaffingPortalApp")
    .config([
        "$resourceProvider",
        function($resourceProvider) {
        
            // extend the default actions
            angular.extend($resourceProvider.defaults.actions, {
                'autocomplete': { method: "GET", isArray: true, params: { controller: "autocomplete" } }
                }
            );
        }
    ]);