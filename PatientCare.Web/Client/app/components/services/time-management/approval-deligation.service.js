﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
  .service("approvalDeligationService", ["$resource", "CreatorSearchFunction", "Resource", function ($resource, creatorSearchFunction, resourceService) {
      var self = this;
      var resource = $resource("api/approval-deligation/:id/:controller", null,
          {
              'update': { method: "PUT" },
              'query': { method: "GET", params: {}, isArray: false },
          });
      creatorSearchFunction(self, "searchResources", resourceService);
      resource.getTableOption = {
          /*
          formatDelete: function (row) {
              return row.lastName + " " + row.firstName;
          },
          disabledAddButton: function () {
              return !getPermissionWorker().canResourceInsert();
          },
          disabledDeleteButton: function () {
              return getPermissionWorker().canResourceDelete();
          },
          disabledEditButton: function () {
              return getPermissionWorker().canResourceUpdate();
          },
          getResourceCheckboxInactive: function () {
              return resourceCheckboxInactive;
          },
          reset: function () {
              resourceCheckboxInactive = false;
          },*/
          pageName: "Approval Deligation",
          hideSearch: true,
          hideBulkEdit: true,
          cols: function () {
              return [
                  {
                      field: "name",
                      title: "Deligation Approver",
                      sortable: "Name",
                      image: function (row) {
                          return row.photoFileNameUrl || "Client/assets/images/userIco.png";
                      },
                      show: true,
                      dataTypeView: "href",
                      dataType: "angucomplete-alt",
                      angucompleteSelect: function (selected) {
                          console.info(selected);
                          console.info(this);
                      },
                      search: self.searchResources,
                      customViewText: function (row) {
                          return row.lastName + ", " + row.firstName;
                      },
                      buildUrl: function (row) {
                          return $state.href("resourceEdit", { id: row.id });
                      }
                  }, {
                      field: "jobTitleName",
                      title: "From Date",
                      sortable: "jobTitleName",
                      show: true,
                      dataType: "text",
                  }, {
                      field: "departmentName",
                      title: "To Date",
                      sortable: "departmentName",
                      show: true,
                      dataType: "text",
                  }, {
                      field: "action",
                      title: ".",
                      class: "blank-cell",
                      dataType: "command"
                  }
              ];
          },/*
              onAdd_Click: function () {
                  $state.go("resourceEdit");
              },
              onEdit_Click: function (row) {
                  $state.go("resourceEdit", { id: row.id });
              },*/
          menuButtons: [],
          headerButtons: [],
          headerLeftItems: []
      };

      return resource;
  }]);