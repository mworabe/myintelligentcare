﻿var app = angular.module('EDZoutstaffingPortalApp');
app.directive('chooseExercise', function ($compile) {
    return {
        scope: {
            config: "=config",
            loadingBarFlag: "=isLoading"

        },
        templateUrl: "Client/app/controllers/exercise/choose-exercise/choose-exercise.html",
        controller: ['$scope',
                    'InjuryRegionService',
                    'ExercisePositionService',
                    'ExerciseActivitieService',
                    'ExerciseService',
                    'ConstantFacory',
                    'toaster',
                    '$modal',

                    function ($scope, InjuryRegionService, ExercisePositionService, ExerciseActivitieService, ExerciseService, toaster, ConstantFacory, $modal) {

            $scope.injuryRegionList = [];
            $scope.positionList = [];
            $scope.activityList = [];
            $scope.exerciseItems = [];
            $scope.selectedExerciseItems = [];

            $scope.totalItems = 20;
            $scope.pageSize = 1000;
            $scope.page = 1;

            $scope.filter = {};
            $scope.filter.byRegionFilterToggle = false;
            $scope.filter.byPositionFilterToggle = false;
            $scope.filter.byActivityFilterToggle = false;

            $scope.config.directiveScope = $scope;

            function isItemInList(item) {
                for (var i = 0; i < $scope.selectedExerciseItems.length; i++) {
                    var obj = $scope.selectedExerciseItems[i];
                    if (obj.id == item.id) {
                        return true;
                    }
                }
                return false;
            }

            var onExercisItemAddBtnClick = function (item, index) {
                if (!isItemInList(item)) {
                    var copy = angular.copy(item);
                    $scope.selectedExerciseItems.push(copy);
                } else {
                    toaster.pop("error", "", "Exercise " + item.name + " is already in list.");
                }
            }

            var onExerciseItemDeleteClick = function (item, index) {
                $scope.selectedExerciseItems.splice(index, 1);
            }

            $scope.exerciseConfig = {
                hasAddBtn: true,
                addBtnClick: onExercisItemAddBtnClick
            }

            $scope.selectedExerciseConfig = {
                hasDeleteBtn: true,
                deleteBtnClick: onExerciseItemDeleteClick
            }

            function loadConfig() {
                InjuryRegionService.all({  }, function (response) { $scope.injuryRegionList = response; });         
                ExerciseActivitieService.getAllItems({ search: "" }, function (response) { $scope.activityList = response; });
            }

            $scope.loadData = function () {
                var request = {

                    regionId: prepareComaSepratedList($scope.injuryRegionList),                 
                    activityId: prepareComaSepratedList($scope.activityList),
                    pageIndex: $scope.page,
                    pageSize: $scope.pageSize
                }
                if ($scope.searchText)
                    request.keywords = $scope.searchText;

                $scope.loadingBarFlag = ExerciseService.getFilterExercise(request).$promise.then(
                                    function (response) {
                                        $scope.exerciseItems = response.data;
                                        $scope.totalItems = response.itemsCount;
                                    },
                                    function (error) {

                                    });
            }

            function prepareComaSepratedList(array) {
                var str = "";
                _.forEach(array, function (item) {
                    if (item.isChecked) {
                        str += item.id + ",";
                    }
                });
                if (str.endsWith(",")) {
                    str = str.substring(0, (str.length - 1));
                }
                return str;
            }
            loadConfig();
            $scope.loadData();

            $scope.getImageURL = function (item) {
                if (item.mediaURL) {
                    return item.mediaURL;
                }
                return "";
            }

            $scope.onCancel = function () {
                if ($scope.config && $scope.config.onBackBtn) {
                    $scope.config.onBackBtn();
                }
            }

            $scope.onSave = function () {
                if ($scope.config && $scope.config.onSaveBtn) {
                    $scope.config.onSaveBtn($scope.selectedExerciseItems);
                }
            }


            $scope.showCopyFrom = function (from) {
                $modal.open({
                    templateUrl: "Client/app/controllers/exercise/choose-exercise/copy-from-model-view.html",
                    controller: "CopyFromCntrl",
                    keyboard: false,
                    resolve: {
                        dataItem: function () {
                            return {
                                title: "",
                                from: from
                            }
                        }
                    }
                }).result.then(function (result) {
                    if (result && result.templateExercises && result.templateExercises.length > 0) {
                        $scope.mm = 0;
                        _.forEach(result.templateExercises, function (item) {
                            if (item.exercise) {
                                item.exercise.template = result.name;
                                if ($scope.mm == 0) {
                                    item.exercise.todisplay = result.name;
                                }
                                $scope.mm = $scope.mm + 1 
                                onExercisItemAddBtnClick(item.exercise, null);
                            }
                        });
                    }
                });
            }


        }]
    }
});



