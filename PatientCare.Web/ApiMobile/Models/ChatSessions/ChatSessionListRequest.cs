﻿using PatientCare.Web.Api.Models;
using System;

namespace PatientCare.Web.ApiMobile.Models.ChatSessions
{
    public class ChatSessionListRequest : ListRequest
    {
        public long Id { get; set; }

        public string ChannelsId { get; set; }

        public string ChannelUniqueName { get; set; }

        public string ChannelName { get; set; }

        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public string CreatedByUserId { get; set; }

        public string LastMessage { get; set; }

        public int? Type { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

    }
}