﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.SurveyQuestions;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Survey Questions operations.
    /// </summary>
    [Authorize]
    public sealed class SurveyQuestionsController : EdzBaseApiController<SurveyQuestion, SurveyQuestionRequest, SurveyQuestionResponse>
    {
        /// <summary>
        /// Description : In this class we pass Survey Questions IPicklistRepository to EdzPicklistApiController where all methods is initialize.
        /// </summary>
        /// <param name="entityRepository">ISurvey Question Repository</param>
        public SurveyQuestionsController(ISurveyQuestionRepository entityRepository)
            : base(entityRepository)
        {
            ReadOptions = this.ReadOptions.WithIncludePredicate(item => item.Category);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get Survey Questions.      
        /// </summary>
        /// <param name="search">Search Value</param>
        /// <returns>Survey Questions Entity</returns>
        [HttpGet]
        [Route("autocomplete/{search}")]
        public async Task<IHttpActionResult> Autocomplete(string search)
        {
            var filter = new FilterQuery<SurveyQuestion>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                filter.AddFilter(x => x.Text.ToLower().StartsWith(search.ToLower()));
            }
            var autocompleteResult =
                await
                    EntityRepository.AutocompleteAsync(filter);

            var result = Mapper.Map<SurveyQuestion[], PicklistResponse[]>(autocompleteResult.ToArray());

            return Ok(result);
        }

        protected override Uri GetEntityLocation(long id)
        {
            return new Uri("/SurveyQuestions/" + id, UriKind.Relative);
        }
    }
}