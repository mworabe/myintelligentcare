namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyCreatedndMOdifyFieldInTables : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.hep_detail", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.hep_detail", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

            AlterColumn("dbo.hep_master", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.hep_master", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

            AlterColumn("dbo.hep_exercise_frequency", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.hep_exercise_frequency", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.hep_detail", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.hep_detail", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

            AlterColumn("dbo.hep_master", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.hep_master", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

            AlterColumn("dbo.hep_exercise_frequency", "created", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.hep_exercise_frequency", "modified", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
    }
}
