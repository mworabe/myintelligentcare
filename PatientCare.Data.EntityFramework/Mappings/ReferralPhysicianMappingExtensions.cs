﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class ReferralPhysicianMappingExtensions
    {
        public static DbModelBuilder MapReferralPhysician(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<ReferralPhysician>().ToTable("referral_physicians").HasKey(i => i.Id);

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.FirstName)
                .HasColumnName("first_name")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.LastName)
                .HasColumnName("last_name")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.Speciality)
                .HasColumnName("speciality")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.MedicalDesignation)
                .HasColumnName("medical_designation")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.MedicalDesignationOther)
                .HasColumnName("medical_designation_other")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.PhoneNumber)
                .HasColumnName("phone_number")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.ExtensionNumber)
                .HasColumnName("extension_number")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.Fax)
                .HasColumnName("fax")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.Email)
                .HasColumnName("email")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.FacilityName)
                .HasColumnName("facility_name")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.PostalCode)
                .HasColumnName("postal_code")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.CityId)
                .HasColumnName("city_id")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .HasOptional<City>(p => p.City)
                .WithMany()
                .HasForeignKey(p => p.CityId);

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.StateId)
                .HasColumnName("state_id")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .HasOptional<State>(p => p.State)
                .WithMany()
                .HasForeignKey(p => p.StateId);

            modelBuilder.Entity<ReferralPhysician>()
                .Property(p => p.CountryId)
                .HasColumnName("country_id")
                .IsOptional();

            modelBuilder.Entity<ReferralPhysician>()
                .HasOptional<Country>(p => p.Country)
                .WithMany()
                .HasForeignKey(p => p.CountryId);

            return modelBuilder;
        }
    }
}
