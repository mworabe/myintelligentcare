﻿using PatientCare.Core.Enums;
using PatientCare.Web.ApiMobile.Models.HepFrequencies;

namespace PatientCare.Web.ApiMobile.Models.HepDetails
{
    public class HepDetailsMobileRequest
    {
        public long Id { get; set; }

        public long? ExerciseId { get; set; }

        public long? GroupId { get; set; }

        public long? HepId { get; set; }

        public int? Sets { get; set; }

        public int? Reps { get; set; }

        public int? Time { get; set; }

        public TimeEnum? TimeUnit { get; set; }

        public int? Holds { get; set; }

        public TimeEnum? HoldsUnit { get; set; }

        public string Weight { get; set; }

        public int? ExerciseOrder { get; set; }

        public bool? IsActiveFrequency { get; set; }

        public HepFrequencyMobileRequest[] Frequencies { get; set; }
    }   
}