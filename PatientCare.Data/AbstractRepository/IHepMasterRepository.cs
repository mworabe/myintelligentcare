﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IHepMasterRepository : IRepository<HepMaster>
    {
    }
}
