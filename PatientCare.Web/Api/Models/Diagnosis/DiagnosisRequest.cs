﻿namespace PatientCare.Web.Api.Models.Diagnosiss
{
    public class DiagnosisRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }   
}