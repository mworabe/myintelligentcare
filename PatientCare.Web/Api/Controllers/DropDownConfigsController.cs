﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.DropDownConfigs;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using PatientCare.Web.Api.Models;
using System.Linq.Expressions;
using System.Linq;
using PatientCare.Data.AbstractDataContext;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// Initializes a new instance of the  Drop Down Configs operations.
    /// </summary>
    [Authorize]
    public sealed class DropDownConfigsController : ApiBaseController
    {
        /// <summary>
        /// Description : Drop Down Configs Field IDropDownConfigService.
        /// </summary>
        private readonly IDropDownConfigService _dropDownConfigService;

        /// <summary>
        /// Description : Drop Down Configs Field IDropDownConfigRepository.
        /// </summary>
        private readonly IDropDownConfigRepository _dropDownConfigRepository;

        /// <summary>
        /// Description : Drop Down Configs Field IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        ReadOptions<DropDownConfig> _readOptions = new ReadOptions<DropDownConfig>();

        /// <summary>
        /// Description : Initializes a new instance of the DropDownConfigs Class.
        /// </summary>
        /// <param name="dropDownConfigService">IDropDown Config Service</param>
        /// <param name="dropDownConfigRepository">IDropDown Config Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public DropDownConfigsController
            (
                IDropDownConfigService dropDownConfigService,
                IDropDownConfigRepository dropDownConfigRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _dropDownConfigService = dropDownConfigService;
            _dropDownConfigRepository = dropDownConfigRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/DropDownConfigs/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Create New Drop Down Config.    
        /// </summary>
        /// <param name="dropDownConfigRequest">Drop Down Config Request Model</param>
        /// <returns>Drop Down Config Response Model</returns>
        [HttpPost]
        [SwaggerResponse(201, "Created", typeof(DropDownConfigResponse))]
        public async Task<IHttpActionResult> CreateDropDownConfig([FromBody]DropDownConfigRequest dropDownConfigRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dropDownConfig = Mapper.Map<DropDownConfigRequest, DropDownConfig>(dropDownConfigRequest);
            await _dropDownConfigService.CreateDropDownConfigAsync(dropDownConfig);

            return Created(GetEntityLocation(dropDownConfig.Id), Mapper.Map<DropDownConfig, DropDownConfigResponse>(dropDownConfig));
        }

        /// <summary>
        /// Method Description : Using for Update Drop Down Config.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="dropDownConfigRequest">Drop Down Config Request Model</param>
        /// <returns>Drop Down Config Response Model</returns>
        [HttpPut]
        public async Task<IHttpActionResult> EditDropDownConfig(long id, [FromBody] DropDownConfigRequest dropDownConfigRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dropDownConfig = Mapper.Map<DropDownConfigRequest, DropDownConfig>(dropDownConfigRequest);
            dropDownConfig.Id = id;

            await _dropDownConfigService.UpdateDropDownConfigAsync(dropDownConfig);

            var viewModel = Mapper.Map<DropDownConfig, DropDownConfigResponse>(dropDownConfig);
            return Ok(viewModel);
        }

        /// <summary>
        /// Method Description : Using for Delete Drop Down Config by Id.   
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Drop Down Config Response Model</returns>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteDropDownConfigItem(long id)
        {
            var success = await _dropDownConfigService.DeleteDropDownConfigItemAsync(id);
            if (!success)
            {
                return NotFound();
            }

            return Ok(new DropDownConfig { Id = id });
        }

        /// <summary>
        /// Method Description : Using for Get Drop Down Config by Id.    
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Drop Down Config Response Model</returns>
        [HttpGet]
        public async Task<IHttpActionResult> GetDropDownConfig(long id)
        {
            var dropDownConfig = await _dropDownConfigService.ReadDropDownConfigAsync(id);
            if (dropDownConfig == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<DropDownConfig, DropDownConfigResponse>(dropDownConfig);
            return Ok(viewModel);
        }

        /// <summary>
        /// Method Description : Using for Get All Drop Down Config.    
        /// </summary>
        /// <param name="request">Drop Down Config List Request Model</param>
        /// <returns>Drop Down Config Picklist Responsee Model</returns>
        [HttpGet]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<DropDownConfigPicklistResponse>))]
        public IHttpActionResult GetAllDropDownConfig([FromUri]DropDownConfigListRequest request)
        {
            var query = PagingExtensions<DropDownConfig>.CreatePagedQuery
                           (request,
                               (string.IsNullOrWhiteSpace(request.SearchField)
                                   && !string.IsNullOrWhiteSpace(request.SearchPhrase)
                                       ?
                                  GetSearchColumnExpressions(request.SearchPhrase)
                                 : GetAutocompleteSearchExpressions(request.SearchPhrase)
                                ));

            if (request.DropDownType != null)
            {
                query.AddFilter(x => x.DropDownType == request.DropDownType);
            }

            var readOptions = new ReadOptions<DropDownConfig>().AsReadOnly();
            var page = _dropDownConfigRepository.PagedList(query, readOptions);
            var dtos = Mapper.Map<DropDownConfig[], DropDownConfigPicklistResponse[]>(page.Entities.ToArray());

            ListResponse<DropDownConfigPicklistResponse> response = new ListResponse<DropDownConfigPicklistResponse>();
            response = new ListResponse<DropDownConfigPicklistResponse>(dtos, page.TotalCount);

            return Ok(response);
        }

        private static Expression<Func<DropDownConfig, bool>> GetSearchColumnExpressions(string searchPhrase)
        {
            return dropDownConfig => dropDownConfig.Name.Contains(searchPhrase);
        }

        private static Expression<Func<DropDownConfig, bool>> GetAutocompleteSearchExpressions(string searchPhrase)
        {
            if (string.IsNullOrWhiteSpace(searchPhrase))
            {
                return null;
            }
            return dropDownConfig => dropDownConfig.Name.Contains(searchPhrase);
        }

        /// <summary>
        /// Method Description : Using for Autocomplete Get  Drop Down Config.    
        /// </summary>
        /// <param name="dropdowntype">Drop down type value</param>
        /// <param name="search">Search value</param>
        /// <param name="searchField">Search Field value</param>
        /// <param name="searchParameter">Search Parameter value</param>
        /// <param name="activityTypeId">Activity Type Id</param>
        /// <returns>Drop Down Config Response Model</returns>
        [HttpGet]
        [Route("api/DropDownConfigs/autocomplete")]
        public IHttpActionResult GetDropDownType([FromUri]string dropdowntype, string search, string searchField = null, string searchParameter = null, long? activityTypeId = null)
        {
            var page = _dropDownConfigService.ReadDropDownTypeAsync(dropdowntype, search, searchField, searchParameter, activityTypeId);

            var dtos = Mapper.Map<DropDownConfig[], DropDownConfigResponse[]>(page.ToArray());

            List<DropDownConfigResponse> response = new List<DropDownConfigResponse>();
            response = new List<DropDownConfigResponse>(dtos);

            return Ok(response);
        }
    }
}
