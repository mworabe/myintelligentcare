﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IPatientAppointmentRepository : IRepository<PatientAppointment>
    {
    }
}
