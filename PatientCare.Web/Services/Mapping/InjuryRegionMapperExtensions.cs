﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using PatientCare.Web.Api.Models.InjuryRegions;
using EmitMapper;

namespace PatientCare.Web.Services.Mapping
{
    public static class InjuryRegionMapperExtensions
    {
        public static EmitMapperMapperProvider InjuryRegionRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<InjuryRegion, InjuryRegionResponse>
                    (new ExtendedMapConfig<InjuryRegion, InjuryRegionResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<InjuryRegion[], InjuryRegionResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<InjuryRegion, InjuryRegionAutoCompleteResponse>
                    (new ExtendedMapConfig<InjuryRegion, InjuryRegionAutoCompleteResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<InjuryRegion[], InjuryRegionAutoCompleteResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<InjuryRegionRequest, InjuryRegion>
                    (new ExtendedMapConfig<InjuryRegionRequest, InjuryRegion>()));

            return mapperProvider;
        }
    }
}