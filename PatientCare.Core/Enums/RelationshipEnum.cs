namespace PatientCare.Core.Enums
{
    /// <summary>
    /// Created By : Swayam Tech Lab (Mohit Solanki).
    /// Created Date : Jun-24-2017.
    /// Title : Relationship Enum.
    /// </summary>
    public enum RelationshipEnum
    {
        Spouse = 0,
        Partner = 1,
        Parent = 2,
        Son = 3,
        Daughter = 4,
        Sibling = 5,
        Friend = 6,
        Other = 7
    }
}
