﻿using System.Web.Mvc;
using PatientCare.Web.Resources;

namespace PatientCare.Web.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }
    }
}