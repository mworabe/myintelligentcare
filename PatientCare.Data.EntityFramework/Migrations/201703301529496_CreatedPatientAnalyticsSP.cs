namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class CreatedPatientAnalyticsSP : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
                  "dbo.sp_Patient_Analytics",
                  p => new
                  {
                      startDate = p.DateTime(defaultValueSql: null),
                      endDate = p.DateTime(defaultValueSql: null),
                      caseId = p.Long(defaultValueSql: null),
                      patientId = p.Long(defaultValueSql: null),

                  },
                  body:
                      @" select  cast(SUM(DATEDIFF(MINUTE, start_date, end_date)+1) as decimal(18,2)) AS TotalTime,
	                     cast(start_date as date) as Dates,
	                     cast(avg(pre_exercises_pain_rating) as int) as painRating, 
			             cast(avg(workout_feel_result) as int) as avgRating
	                   from patient_workout_analytics
	                   where 
		               case_id = @caseId and patient_id = @patientId 
		                and CAST(start_DATE AS DATE) between CAST(@startDate AS DATE) and CAST(@endDate as DATE)

		               --and CAST(start_DATE AS DATE) >= CAST(@startDate AS DATE)
                       -- AND CAST(start_DATE AS DATE) <= CAST(@endDate as DATE)
		              group by cast(start_date as date),pre_exercises_pain_rating,workout_feel_result"
                 );
        }

        public override void Down()
        {

        }
    }
}
