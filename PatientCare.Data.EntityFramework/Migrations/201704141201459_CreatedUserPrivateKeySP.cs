namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class CreatedUserPrivateKeySP : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
            "dbo.sp_user_private_key",
            p => new
            {
                user_id = p.String(defaultValueSql: null)
            },
            body:
                 @" select chatkey.Id,chatkey.user_id as UserId,chatkey.private_key as PrivateKey,roles.name as Role from chat_user_key chatkey
			            left join  AspNetUsers users on chatkey.user_id = users.Id
			            left join AspNetUserRoles mapRole on users.Id = mapRole.UserId
			            left join AspNetRoles roles on mapRole.RoleId = roles.Id 
			            where user_id in (@user_id)"
                );
        }

        public override void Down()
        {
            DropStoredProcedure("dbo.sp_user_private_key");
        }
    }
}
