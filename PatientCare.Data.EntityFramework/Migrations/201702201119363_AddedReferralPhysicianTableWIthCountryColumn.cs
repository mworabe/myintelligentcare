namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedReferralPhysicianTableWIthCountryColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.referral_physicians", "country_id", c => c.Long());
            CreateIndex("dbo.referral_physicians", "country_id");
            AddForeignKey("dbo.referral_physicians", "country_id", "dbo.countries", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.referral_physicians", "country_id", "dbo.countries");
            DropIndex("dbo.referral_physicians", new[] { "country_id" });
            DropColumn("dbo.referral_physicians", "country_id");
        }
    }
}
