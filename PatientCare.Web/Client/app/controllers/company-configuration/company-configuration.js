"use strict";

angular.module("EDZoutstaffingPortalApp")
    .config(function ($stateProvider) {
        $stateProvider
            .state("company-configuration", {
                url: "/company/configuration",
                templateUrl: "Client/app/controllers/company-configuration/company-configuration.html",
                controller: "CompanyConfigurationCtrl as CompanyConfigurationCtrl",
                data: {
                    security: {
                        authenticated: true
                    }
                }
            });
    });