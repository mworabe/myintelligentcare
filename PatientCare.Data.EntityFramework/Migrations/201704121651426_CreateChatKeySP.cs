namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateChatKeySP : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
              "dbo.sp_chat_keys",
              p => new
              {
                  from_user_id = p.String(defaultValueSql: null),
                  to_user_id = p.String(defaultValueSql: null)
              },
              body:
                   @"select Id,user_id as UserId,private_key as PrivateKey from chat_user_key where user_id in (@from_user_id,@to_user_id)"
                  );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_chat_keys");
        }
    }
}
