﻿using PatientCare.Core.Entities.Abstract;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Resource Recognition.
    /// </summary>
    public class ResourceRecognition : Entity
    {
        public long? ResourceId { get; set; }
        public virtual Resource Resource { get; set; }

        public string Recognition { get; set; }       
    }
}
