"use strict";
angular.module("EDZoutstaffingPortalApp")
    .config(["$provide",
        function ($provide) {
            $provide.decorator("angucompleteAltDirective", [
                "$delegate", "$templateCache", '$timeout', function ($delegate, $templateCache, $timeout) {
                    var directive = $delegate[0];
                    var scopeExtension = {/*isRequired: "="*/ };
                    angular.forEach(scopeExtension, function (value, key) {
                        $delegate[0].$$isolateBindings[key] = {
                            attrName: key,
                            mode: value,
                            optional: true
                        };
                    });

                    var compile = directive.compile;

                    directive.compile = function (tElement, tAttrs) {
                        var link = compile.apply(this, arguments);
                        return function (scope, elem, attrs, ctrl) {
                            link.apply(this, arguments);
                            var inputField = elem.find('input');
                            inputField.on("keyup", function () {
                                if (scope.minlength === "0" && !scope.searchStr) {
                                    scope.selectedObject(undefined);
                                    if (scope.fieldRequired && ctrl && scope.inputName) {
                                        ctrl[scope.inputName].$setValidity("autocomplete-required", false);
                                    }
                                    return;
                                }
                            });

                            

                            if (elem) {
                                
                                var buttonDropdown = elem[0].querySelector('.search-btn-unq');
                                
                                //var buttonDropdown = elem.getElementsByClassName('.search-btn-unq');
                                
                                if (!scope.disableInput) {
                                    console.info("setting btn click");
                                    console.info(buttonDropdown);

                                    buttonDropdown.onclick= function () {
                                        console.info("on auto btn click");
                                        $timeout(function () {
                                            scope.inputChangeHandler('');
                                            inputField.focus();
                                        }, 200);
                                    }
                                }
                            }
                            
                            // We can extend the link function here
                        };
                    };

                    directive.templateUrl = "Client/app/components/decorators/angucomplete-alt/template.html";
                    return $delegate;
                }
            ]);
        }
    ]);

/*

                    $templateCache.put("components/decorators/angucomplete-alt/template.html",
                        "<div class=\"angucomplete-holder\" ng-class=\"{'angucomplete-dropdown-visible': showDropdown}\">" +
                         "<div class=\"input-group\">" +
                              " <input id=\"{{id}}_value\" " +
                              "class=\"form-control {{inputClass}}\"" +
                              "name=\"{{inputName}}\" ng-class=\"{'angucomplete-input-not-empty': notEmpty}\" " +
                              "ng-model=\"searchStr\" ng-disabled=\"disableInput\" type=\"{{inputType}}\" " +
                              "placeholder=\"{{placeholder}}\" maxlength=\"{{maxlength}}\" ng-focus=\"onFocusHandler()\" " +
                              "class=\"{{inputClass}}\" ng-focus=\"resetHideResults()\" ng-blur=\"hideResults($event)\"" +
                              "autocapitalize=\"off\" autocorrect=\"off\" autocomplete=\"off\" ng-change=\"inputChangeHandler(searchStr || '')\" />" +
                              /* ng-required=\"isRequired\" <-- Removed due to raising error. *
"<span class=\"input-group-btn\">" +
"<div class='btn btn-default search-btn-unq' type='button' ng-disabled=\"disableInput\"><span class='fa fa-sort-desc'><span></div>" +
" </span> " +
" </div> " +
"  <div id=\"{{id}}_dropdown\" class=\"angucomplete-dropdown\" ng-show=\"showDropdown\">" +
"    <div class=\"angucomplete-searching\" ng-show=\"searching\" ng-bind=\"textSearching\"></div>" +
"    <div class=\"angucomplete-searching\" ng-show=\"!searching && (!results || results.length == 0)\" ng-bind=\"textNoResults\"></div>" +
"    <div class=\"angucomplete-row\" ng-repeat=\"result in results\" ng-click=\"selectResult(result)\" ng-mouseenter=\"hoverRow($index)\" ng-class=\"{'angucomplete-selected-row': $index == currentIndex}\">" +
"      <div ng-if=\"imageField\" class=\"angucomplete-image-holder\">" +
"        <img ng-if=\"result.image && result.image != ''\" ng-src=\"{{result.image}}\" class=\"angucomplete-image\"/>" +
"        <div ng-if=\"!result.image && result.image != ''\" class=\"angucomplete-image-default\"></div>" +
"      </div>" +
"      <div class=\"angucomplete-title\" ng-if=\"matchClass\" ng-bind-html=\"result.title\"></div>" +
"      <div class=\"angucomplete-title\" ng-if=\"!matchClass\">{{ result.title }}</div>" +
"      <div ng-if=\"matchClass && result.description && result.description != ''\" class=\"angucomplete-description\" ng-bind-html=\"result.description\"></div>" +
"      <div ng-if=\"!matchClass && result.description && result.description != ''\" class=\"angucomplete-description\">{{result.description}}</div>" +
"    </div>" +
"  </div>" +
"</div>"
                    );

*/