﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("ElasticSearchController",
    ["$scope", "$state", "$stateParams", "$sce", "ConfigLayout", "ElasticSearch", "toaster", "SystemFunctions",
        "$anchorScroll", "$location", "$timeout",
        ElasticSearchFunc]);

function ElasticSearchFunc($scope, $state, $stateParams, $sce, configLayoutService, elasticSearchService, toaster, systemFunctionsService,
    $anchorScroll, $location, $timeout) {
    var self = this;
    self.model = {};
    self.model.search = $stateParams.filter;
    self.resourceDynamicFieldList = [];
    self.resourceFieldList = configLayoutService.getResourceFields();
    self.queryParams = {};
    self.queryParams.searchType = "All";
    self.queryParams.pageIndex = 1;
    self.queryParams.pageSize = 20;
    self.model.resourceList = [];
    self.model.projectList = [];


    function scrollToAnchor(elementId) {
        if ($location.hash() !== elementId)
            $location.hash(elementId);
        else
            $anchorScroll();
    }
    self.getSystemFormatedDate = systemFunctionsService.getSystemFormatedDate;
    self.getEllipsesProjectDesc = function getEllipsesProjectDesc(needToEllipsesText, value, length) {
        if (needToEllipsesText)
            return (value && value.length > length) ? value.substring(0, length) + "..." : value;
        else
            return value;
    }

    self.onResourcePageChange = function (page) {
        self.model.resourceList = [];
        self.model.resourceItemCount = undefined;
        self.queryParams.pageIndex = parseInt(page.number);
        self.queryParams.searchType = "Resource";
        self.getDataFromServer(self.queryParams);
    }

    self.onProjectPageChange = function (page) {
        self.model.projectList = [];
        self.model.projectItemCount = undefined;
        self.queryParams.pageIndex = parseInt(page.number);
        self.queryParams.searchType = "Project";
        self.getDataFromServer(self.queryParams);
    }

    self.elasticSearchButtonClick = function (queryParams) {
        self.model.resourceList = [];
        self.model.resourceItemCount = undefined;
        self.model.projectList = [];
        self.model.projectItemCount = undefined;
        queryParams.pageIndex = 1;
        queryParams.searchType = "All";
        if ($stateParams.filter === self.model.search)
            self.getDataFromServer(queryParams);
        else
            $state.go("elastic-search", { filter: self.model.search }, { reload: true });
    }

    self.getDataFromServer = function (queryParams) {
        if (!self.model.search || self.model.search.length <= 0) {
            toaster.pop('error', '', 'Search Term cannot be empty');
            return;
        }

        queryParams.filter = self.model.search;
        self.loadCorrect = elasticSearchService.getFilterData(queryParams, function (response) {
            if (response.searchType === "All" || response.searchType === "Resource") {
                self.model.resourceList = (response.resource) ? response.resource.data : [];
                self.model.resourceItemCount = (response.resource) ? response.resource.itemsCount : 0;
                /*  Pagination for Resource */
                if (response.resource && response.resource.data) {
                    if (!self.resourcePagination)
                        self.resourcePagination = {};
                    self.resourcePagination = systemFunctionsService.generatePaginationPageArray(response.resource, response.pageIndex, self.queryParams.pageSize);
                }
            }
            if (response.searchType === "All" || response.searchType === "Project") {
                self.model.projectList = (response.project) ? response.project.data : [];
                self.model.projectItemCount = (response.project) ? response.project.itemsCount : 0;
                /*  Pagination for Project  */
                if (response.project && response.project.data) {
                    if (!self.projectPagination)
                        self.projectPagination = {};
                    self.projectPagination = systemFunctionsService.generatePaginationPageArray(response.project, response.pageIndex, self.queryParams.pageSize);
                }
            }
            if (response.searchType === "Project")
                $timeout(function () { scrollToAnchor("project_panel_container"); }, 200);
            else if (response.searchType === "Resource")
                $timeout(function () { scrollToAnchor("resource_panel_container"); }, 200);
            else
                $timeout(function () { scrollToAnchor("top_of_page"); }, 200);

        }, function (error) {
            console.info(error);
            if (error) { }
            toaster.pop("error", "", "unable to process you request, please try again later.");
        });
    }

    self.getResourceDynamicFields = function (jsonString) { return angular.fromJson(jsonString); }

    self.highLightSearchedText = function (value) {
        if (!self.model.search)
            return $sce.trustAsHtml(value);

        if (!value)
            return value;

        if (typeof value === "object" && value.value)
            return value.value;

        return $sce.trustAsHtml(("" + value).replace(new RegExp(self.model.search, 'gi'), '<span class="highlightedText">$&</span>'));
    }

    if (self.model.search !== "")
        self.elasticSearchButtonClick(self.queryParams);


}