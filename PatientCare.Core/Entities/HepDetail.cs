﻿using PatientCare.Core.Entities.Abstract;
using PatientCare.Core.Enums;
using System.Collections.Generic;

namespace PatientCare.Core.Entities
{
    /// <summary>
    /// This calss is for Hep Detail.
    /// </summary>
    public class HepDetail : Entity
    {
        public string name { get; set; }

        public long? ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        public long? GroupId { get; set; }
        public virtual HepGroup Group { get; set; }

        public long? HepId { get; set; }
        public virtual HepMaster Hep { get; set; }

        public int? Sets { get; set; }

        public int? Reps { get; set; }

        public int? Time { get; set; }

        public TimeEnum? TimeUnit { get; set; }

        public decimal? Weight { get; set; }

        public string WeightUnit { get; set; }

        public int? Holds { get; set; }

        public TimeEnum? HoldsUnit { get; set; }

        public string frequency { get; set; }

        public string resistance { get; set; }

        public int? ExerciseOrder { get; set; }

        public virtual ICollection<HepFrequency> Frequencies { get; set; }

        public string Comments { get; set; }
        

        public bool? IsActiveFrequency { get; set; }

        public string others { get; set; }
    }
}
