"use strict";

(function edzAlphanumericCustomFieldOptionsIIFE() {
    angular.module("EDZoutstaffingPortalApp")
        .directive("edzAlphanumericCustomFieldOptions", [
            "CUSTOM_FIELD",
            "edzCustomFieldEditHelper",
            edzAlphanumericCustomFieldOptions
        ]);

    function edzAlphanumericCustomFieldOptions(CUSTOM_FIELD, edzCustomFieldEditHelper) {
        return {
            templateUrl: "Client/app/components/customFields/customFieldsOptions/alphanumeric/edzAlphanumericCustomField.html",
            restrict: "E",
            scope: false,
            controller: ["$scope", function ($scope) {
                var option = $scope.option;
                var allowedAdditionalCharacters = edzCustomFieldEditHelper.getPropertyByName(option, CUSTOM_FIELD.PROPERTY_NAME.ALPHA.ALLOWED_ADDITIONAL_CHARACTERS) || {};
                var length = edzCustomFieldEditHelper.getPropertyByName(option, CUSTOM_FIELD.PROPERTY_NAME.GENERAL.LENGTH) || {};

                var resizeField = edzCustomFieldEditHelper.getPropertyByName(option, CUSTOM_FIELD.PROPERTY_NAME.ALPHANUMERIC.RESIZE_FIELD) || {};
                $scope.fieldStyle = resizeField.value === "True" ? {
                    width: length.value * 10 + 12 + 'px'
                } : {};

                $scope.validation = {};
                $scope.validation.length = length.value;
                $scope.validation.allowedAdditionalCharacters = allowedAdditionalCharacters.value;
                $scope.validation.require = $scope.require;

                var additionalCharacters = $scope.validation.allowedAdditionalCharacters ? $scope.validation.allowedAdditionalCharacters : "";
                //additionalCharacters = additionalCharacters.replace(/(.)(?=.*\1)/g, "");
                additionalCharacters = additionalCharacters.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");;
                $scope.validation.regex = new RegExp("^[a-zA-Z0-9]*[A-Za-z0-9 " + additionalCharacters + "]*$", "i");

                function loadData() {

                }

                function activate() {
                    loadData();
                }

                activate();
            }]
        };
    }
})();
