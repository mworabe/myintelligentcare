﻿using System;
using System.Runtime.Serialization;
using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.ResourceCertificates;
using PatientCare.Web.Api.Models.ResourceCriminalRecords;
using PatientCare.Web.Api.Models.ResourceEducations;
using PatientCare.Web.Api.Models.ResourcePreviousEmployments;
using PatientCare.Web.Api.Models.ResourceSkills;
using PatientCare.Web.Api.Models.ResourceSpokenLanguages;
using PatientCare.Web.Api.Models.ResourceTravels;
using PatientCare.Web.Api.Models.ResourceWorkExperience;
using PatientCare.Web.Api.Models.ResourceProfessionalOrganizations;
using PatientCare.Web.Api.Models.ResourceBehaviouralDetails;
using PatientCare.Web.Api.Models.ResourceRecognitions;
using PatientCare.Web.Api.Models.ResourceIndustry;
using PatientCare.Web.Api.Models.ResourcePracticeAreas;
using PatientCare.Web.Api.Models.ResourceInsurances;
using PatientCare.Web.Api.Models.ResourceTimeoffBenefits;
using PatientCare.Web.Api.Models.Clinics;
using PatientCare.Web.Api.Models.ClinicLocations;
using PatientCare.Web.Api.Models.Cities;

namespace PatientCare.Web.Api.Models.Resources
{
    [DataContract]
    public class ResourceResponse
    {
        #region Personal Info Division

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Overview { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public GenderEnum? Gender { get; set; }

        [DataMember]
        public LGBTEnum? LGBT { get; set; }

        [DataMember]
        public MartialStatusEnum? MartialStatus { get; set; }

        [DataMember]
        public string Ethnicity { get; set; }

        [DataMember]
        public string Prefix { get; set; }

        [DataMember]
        public string Suffix { get; set; }

        [DataMember]
        public bool? Active { get; set; }

        [DataMember]
        public string PreferredFirstname { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string Address3 { get; set; }

        [DataMember]
        public string Address4 { get; set; }

        [DataMember]
        public string Address5 { get; set; }

        [DataMember]
        public string Address6 { get; set; }

        [DataMember]
        public string ZipCodeTemp { get; set; }

        [DataMember]
        public PicklistResponse ZipCode { get; set; }

        [DataMember]
        public long? CityId { get; set; }

        [DataMember]
        public CityResponse City { get; set; }

        [DataMember]
        public long? CountryId { get; set; }

        [DataMember]
        public PicklistResponse Country { get; set; }

        [DataMember]
        public long? StateId { get; set; }

        [DataMember]
        public PicklistResponse State { get; set; }

        [DataMember]
        public string TimekeeperNumber { get; set; }

        [DataMember]
        public long? ClinicId { get; set; }
        [DataMember]
        public ClinicAutoCompleteResponse Clinic { get; set; }

        [DataMember]
        public long? ClinicLocationId { get; set; }
        [DataMember]
        public ClinicLocationAutoCompleteResponse ClinicLocation { get; set; }

        #endregion

        #region Employement Detail Division

        [DataMember]
        public string EmployeeId { get; set; }

        [DataMember]
        public PicklistResponse JobTitle { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public string Ssn { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public string OfficeNumber { get; set; }

        [DataMember]
        public string Extension { get; set; }

        [DataMember]
        public PicklistResponse Supervisor { get; set; }

        [DataMember]
        public string TimeZoneInfo { get; set; }

        [DataMember]
        public string HomeNumber { get; set; }

        [DataMember]
        public PicklistResponse Division { get; set; }

        [DataMember]
        public RelationshipEnum? Relationship { get; set; }

        [DataMember]
        public string MobilePhoneNumber { get; set; }

        [DataMember]
        public PicklistResponse Department { get; set; }

        [DataMember]
        public string EmergencyContactName { get; set; }

        [DataMember]
        public string EmergencyContactPhoneNumber { get; set; }

        #endregion

        #region Compensation and Benefits

        [DataMember]
        public decimal? FirmRate { get; set; }

        [DataMember]
        public decimal? NationalRate { get; set; }

        [DataMember]
        public EmploymentTypeEnum? EmploymentType { get; set; }

        [DataMember]
        public decimal? Salary { get; set; }

        [DataMember]
        public decimal? BonusOrOtherPay { get; set; }

        [DataMember]
        public ResourceTypeEnum? ResourceType { get; set; }
        #endregion

        #region Education

        [DataMember]
        public int? GradeLevel { get; set; }

        [DataMember]
        public int? YearsOfCollege { get; set; }
        #endregion

        #region Working Details

        [DataMember]
        public int? YearsEmployed { get; set; }

        [DataMember]
        public bool? AvailableToTravel { get; set; }

        [DataMember]
        public bool? WorksWellInTeam { get; set; }

        [DataMember]
        public bool? ValidPassport { get; set; }

        [DataMember]
        public bool? WorksWellAlone { get; set; }

        #endregion

        #region Background Check

        [DataMember]
        public PicklistResponse ArmedForcesCountry { get; set; }

        [DataMember]
        public string ArmedForcesBranch { get; set; }

        [DataMember]
        public bool? ArmedForces { get; set; }

        [DataMember]
        public string DrugTest { get; set; }

        [DataMember]
        public string CreditReportOk { get; set; }

        [DataMember]
        public YesNoOtherEnum? MotorVehicleReport { get; set; }

        [DataMember]
        public YesNoOtherEnum? EmploymentEligibilityVerification { get; set; }

        [DataMember]
        public YesNoOtherEnum? InternationalWorkHistory { get; set; }

        [DataMember]
        public YesNoOtherEnum? ProfessionalReferenceChecks { get; set; }

        [DataMember]
        public YesNoOtherEnum? FormI9 { get; set; }

        [DataMember]
        public YesNoOtherEnum? FormEVerify { get; set; }

        [DataMember]
        public PassFailEnum? FingerPrinting { get; set; }
        [DataMember]
        public string FingerPrintingOther { get; set; }

        [DataMember]
        public PassFailEnum? DrugScreening { get; set; }
        [DataMember]
        public string DrugScreeningOther { get; set; }

        [DataMember]
        public PassFailEnum? CreditCheck { get; set; }
        [DataMember]
        public string CreditCheckOther { get; set; }

        #endregion

        #region Retirement Benefits

        [DataMember]
        public bool? RbParticipating { get; set; }

        [DataMember]
        public string RbEligibleWages { get; set; }

        [DataMember]
        public string RbEmployeeContribution { get; set; }

        [DataMember]
        public string RbEmployerMatching { get; set; }

        #endregion

        [DataMember]
        public long? BusinessUnitId { get; set; }

        [DataMember]
        public string CubicalOrOfficeNumber { get; set; }

        [DataMember]
        public PicklistResponse BusinessUnit { get; set; }

        [DataMember]
        public PicklistResponse ResourceTeam { get; set; }

        [DataMember]
        public ResourceSpokenLanguageResponse[] SpokenLanguages { get; set; }

        [DataMember]
        public ResourceEducationResponse[] Educations { get; set; }

        [DataMember]
        public ResourceTravelResponse[] Travels { get; set; }

        [DataMember]
        public ResourceBehaviouralDetailResponse[] ResourceBehaviouralDetails { get; set; }

        [DataMember]
        public ResourceIndustryResponse[] Industry { get; set; }

        [DataMember]
        public ResourcePracticeAreaResponse[] ResourcePracticeAreas { get; set; }

        [DataMember]
        public ResourceRecognitionResponse[] ResourceRecognitions { get; set; }

        [DataMember]
        public ResourceCertificateResponse[] Certificates { get; set; }

        [DataMember]
        public ResourceSkillResponse[] Skills { get; set; }

        [DataMember]
        public ResourceCriminalRecordResponse[] CriminalRecords { get; set; }

        [DataMember]
        public ResourcePreviousEmploymentResponse[] PreviousEmployments { get; set; }

        [DataMember]
        public virtual ResourceCustomFieldValueResponse[] CustomFieldValues { get; set; }

        [DataMember]
        public virtual ResourceWorkExperienceResponse[] ResourceWorkExperiences { get; set; }

        [DataMember]
        public virtual ResourceProfessionalOrganizationResponse[] ResourceProfessionalOrganizations { get; set; }

        [DataMember]
        public string ProfessionalReferenceChecksOther { get; set; }

        [DataMember]
        public string EmploymentEligibilityVerificationOther { get; set; }

        [DataMember]
        public string MotorVehicleReportOther { get; set; }

        [DataMember]
        public string InternationalWorkHistoryOther { get; set; }

        [DataMember]
        public YesNoOtherEnum? CredentialVerifications { get; set; }

        [DataMember]
        public string CredentialVerificationsOther { get; set; }

        [DataMember]
        public string FormEVerifyOther { get; set; }

        [DataMember]
        public string FormI9Other { get; set; }

        [DataMember]
        public bool? CollegeDegree { get; set; }

        [DataMember]
        public bool? EverBeenConvictedOfACrime { get; set; }

        [DataMember]
        public bool? InformationCertifiedByEmployer { get; set; }

        [DataMember]
        public string ProofOfCitizenship { get; set; }

        [DataMember]
        public decimal? HourlyRate { get; set; }

        [DataMember]
        public RangeTypeEnum? Range { get; set; }

        [DataMember]
        public string PhotoFileNameUrl { get; set; }

        [DataMember]
        public string PhotoFileName { get; set; }

        [DataMember]
        public string CvFileNameUrl { get; set; }

        [DataMember]
        public string CvFileName { get; set; }

        [DataMember]
        public double? Productivity { get; set; }

        [DataMember]
        public double? Rating { get; set; }

        [DataMember]
        public DateTime? RatingDate { get; set; }

        [DataMember]
        public long? DepartmentId { get; set; }

        [DataMember]
        public long? DivisionId { get; set; }

        [DataMember]
        public long? SupervisorId { get; set; }

        // This is use for debug time only
        [DataMember]
        public string AdditionalInfo { get; set; }

        [DataMember]
        public ResourceInsuranceResponse[] ResourceInsurances { get; set; }

        [DataMember]
        public ResourceTimeoffBenefitResponse[] ResourceTimeoffBenefits { get; set; }
    }
}