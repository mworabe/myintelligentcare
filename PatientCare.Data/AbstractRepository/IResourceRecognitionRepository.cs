﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceRecognitionRepository : IRepository<ResourceRecognition>
    {
    }
}
