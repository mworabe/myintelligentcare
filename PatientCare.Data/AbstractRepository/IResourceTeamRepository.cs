﻿using PatientCare.Core.Entities;

namespace PatientCare.Data.AbstractRepository
{
    public interface IResourceTeamRepository : IRepository<ResourceTeam>
    {
    }
}
