namespace PatientCare.Data.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedReferralPhyscianTableWithPatientRelation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.referral_physicians",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    first_name = c.String(),
                    last_name = c.String(),
                    speciality = c.String(),
                    medical_designation = c.Int(),
                    medical_designation_other = c.String(),
                    phone_number = c.String(),
                    fax = c.String(),
                    email = c.String(),
                    facility_name = c.String(),
                    city_id = c.Long(),
                    state_id = c.Long(),
                    created = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    modified = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.city_id)
                .ForeignKey("dbo.States", t => t.state_id)
                .Index(t => t.city_id)
                .Index(t => t.state_id);

            AddColumn("dbo.patients", "referral_physician_id", c => c.Long());
            CreateIndex("dbo.patients", "referral_physician_id");
            AddForeignKey("dbo.patients", "referral_physician_id", "dbo.referral_physicians", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.patients", "referral_physician_id", "dbo.referral_physicians");
            DropForeignKey("dbo.referral_physicians", "state_id", "dbo.States");
            DropForeignKey("dbo.referral_physicians", "city_id", "dbo.Cities");
            DropIndex("dbo.referral_physicians", new[] { "state_id" });
            DropIndex("dbo.referral_physicians", new[] { "city_id" });
            DropIndex("dbo.patients", new[] { "referral_physician_id" });
            DropColumn("dbo.patients", "referral_physician_id");
            DropTable("dbo.referral_physicians");
        }
    }
}
