﻿
"use strict";

angular.module("EDZoutstaffingPortalApp")
    .service("calendarService", ["$resource",
    function ($resource) {
        var res = $resource("api/Calender/MyTimeOff", null,
            {
                'query': { method: "GET", params: {}, isArray: true },
                'getMyResourceTimeOff': {
                    url: 'api/Calender/ResourceTimeOff',
                    method: "GET",
                    params: {},
                    isArray: true
                },
                'getMyTaskDetails': {
                    url: 'api/Calender/MyTask',
                    method: "GET",
                    params: {},
                    isArray: true
                },
                'getResourceTaskDetails': {
                    url: 'api/Calender/ResourceTask',
                    method: "GET",
                    params: {},
                    isArray: true
                },
            }
            );
        return res;
    }
    ]);