﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .controller("ClientCompanyEditCtrl", [
        "$state", "initialValues", "ClientCompany", "CreatorSearchFunction", "StateService", "CityService", "toaster",
        "$timeout", "$filter", "$translate", "SystemFunctions", "ConfigLayout", "Country",
        function ($state, initialValues, clientCompany, creatorSearchFunction, StateService, CityService, toaster,
            $timeout, $filter, $translate, systemFunctionsService, configLayoutFactory, countryService) {
            var self = this;
            self.autocompleteMinLength = 0;
            self.clientCompanyFieldList = clientCompany.getTableOption.cols();
            self.projectConfigs = initialValues[0] || {};
            self.model = initialValues[1] || {};
            // Temp Patch for 2nd Element of Array -- Need to get it from server.
            //initialValues.push({});

            self.activityModelList = angular.fromJson(localStorage.getItem("PROJECT_MENU_ITEMS"));// initialValues[3];
            self.pageTitle = self.model.name || "Client Company"

            self.loadCorrect;
            self.paymentTerms_Enum = [
                { name: "Net 30 Days", value: "Net 30 Days" }
            ];

            creatorSearchFunction(self, "state_Search", StateService);
            creatorSearchFunction(self, "city_Search", CityService);
            creatorSearchFunction(self, "country_Search", countryService);

            function parceLayout() {
                self.tabs = configLayoutFactory.getClientCompanyTabs();   //_.unionBy(initialValues[2].tabs, configLayoutFactory.getResourceTab(), "id");
                initialValues[2].containers = configLayoutFactory.getDefaultClientCompanyContainers();  //initialValues[1].containers || configLayoutFactory.getDefaultResourceContainer();
                self.clientDetailsContainer = _.find(initialValues[2].containers, { id: "clientDetails" });
                self.ClientCommunicationDetails = _.find(initialValues[2].containers, { id: "clientCommunicationDetails" });
                self.generalPoropertyList = configLayoutFactory.getClientCompanyFields();
                self.__ResourceEditCtrl = "ClientCompanyEditCtrl";
            }
            parceLayout();

            self.onActivityButtonClick = function (activity) {
                $state.go("project", { id: activity.id, clientId: self.model.id });
            }

            self.isProjectButtonVisible = function isProjectButtonVisible() {
                return self.model && self.model.id > 0 && self.activityModelList && self.activityModelList.length > 0;
            }

            function getStakeholdersSectionTitle() {
                if (self.projectConfigs && self.projectConfigs.tabs) {
                    var obj = _.find(self.projectConfigs.tabs, { id: "stakeholders" });
                    if (obj && obj.name) {
                        return $filter('translate')(obj.name);
                    } else {
                        return $filter('translate')('Stakeholders');
                    }
                } else
                    return $filter('translate')('Stakeholders');
            }
            self.getStakeholdersSectionTitle = getStakeholdersSectionTitle;

            self.city_Selected = function (object) {
                if (object && object.originalObject) {
                    self.model.city = object.originalObject;
                    self.model.cityId = object.originalObject.id;
                } else {
                    self.model.cityId = 0;
                    self.model.city = {};
                }
            }

            self.state_Selected = function (object) {
                if (object && object.originalObject) {
                    self.model.state = object.originalObject;
                    self.model.stateId = object.originalObject.id;
                } else {
                    self.model.stateId = 0;
                    self.model.state = {};
                }
            }

            self.country_Selected = function (object) {
                if (object && object.originalObject) {
                    self.model.country = object.originalObject;
                    self.model.countryId = object.originalObject.id;
                } else {
                    self.model.countryId = 0;
                    self.model.country = {};
                }
            }

            self.cancelButtonClick = function () {
                //$state.go("adminConfigData", { currentTab: "clientCompany" });
                $state.go("clientCompany");
            }

            function prepareErrorListForForm(form) {
                self.formErrorList = [];
                var errors = form.$error;
                var allFields = self.generalPoropertyList;
                if (errors) {
                    _.forEach(errors, function (invalidFieldList) {
                        _.forEach(invalidFieldList, function (invalidField) {
                            var fieldId = invalidField.$name;
                            var actualField = _.find(allFields, { id: fieldId });
                            var fieldError = (actualField.name) + " is Required."
                            self.formErrorList.push(fieldError);
                        });
                    });
                }
            }

            self.hideErrorTab = function () { self.formErrorList = undefined }

            self.saveButtonClick = function (clientCompanyForm) {
                systemFunctionsService.scrollToTopZero();

                if (!clientCompanyForm) {
                    systemFunctionsService.scrollToTopZero();
                    return;
                }
                if (clientCompanyForm.$invalid) {
                    prepareErrorListForForm(clientCompanyForm);
                    return;
                }
                /*
                if (clientCompanyForm.$dirty) {
                    if (!self.formErrorList)
                        self.formErrorList = [];
                    self.formErrorList.push("Payment Terms is Required.");
                    return;
                }*/

                if (!self.model.id) {
                    self.loadCorrect = clientCompany.save(self.model,
                        function (success) {
                            toaster.pop("success", "", "Client Company Saved Successfull");
                            $state.go("clientCompanyEdit", { id: success.id }, { reload: true });
                        },
                        function (error) {
                            toaster.pop("error", error.data.message);
                        }
                    );
                } else {
                    self.loadCorrect = clientCompany.update({ id: self.model.id }, self.model,
                        function (success) {
                            toaster.pop("success", "", "Client Company Updated Successfull");
                            $state.go("clientCompanyEdit", { id: success.id }, { reload: true });
                        },
                        function (error) {
                            toaster.pop("error", error.data.message);
                        }
                    );
                }
            }

            self.initForm = function () {
                var clientCompanyId = $state.params.id;
                self.loadCorrect = clientCompany.get({ id: clientCompanyId },
                    function (success) {
                        self.model = success;
                        if (!self.model.cityId && success.city) {
                            self.model.cityId = success.city.id;
                        }
                        if (!self.model.stateId && success.state) {
                            self.model.stateId = success.state.id;
                        }
                        if (!self.model.countryId && success.country) {
                            self.model.countryId = success.country.id;
                        }
                    },
                    function (error) {
                        toaster.pop("error", "", error.message);
                    }
                );
            }
            //self.initForm();
        }
    ]);