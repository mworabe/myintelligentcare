﻿using System.Collections.Generic;

namespace PatientCare.Core.Services.Dashboards
{
    public interface IDashboardService
    {
        List<ProjectTypeViewModel> ResourceCountry();        
        
        List<ProjectTypeViewModel> ResourceByDivision();

        List<ProjectTypeViewModel> ResourceByDepartment();

        List<ProjectTypeViewModel> ResourceByExperienceLevel();

        List<ProjectTypeViewModel> ResourceByJobTitle();

        List<ProjectTypeViewModel> ResourceByLanguage();

        List<ProjectTypeViewModel> ResourceByGender();

        List<ProjectTypeViewModel> ResourceByState();

        List<ProjectTypeViewModel> ResourceByCity();

        List<ProjectTypeViewModel> ResourceByLocation();

        List<ProjectTypeViewModel> ResourceByEducationNameOfSchool();

        List<ProjectTypeViewModel> ResourceByRating();
    }
}
