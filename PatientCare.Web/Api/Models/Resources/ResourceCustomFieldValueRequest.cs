﻿namespace PatientCare.Web.Api.Models.Resources
{
    public class ResourceCustomFieldValueRequest
    {
        public long Id { get; set; }
        public long CustomFieldId { get; set; }
        public long ResourceId { get; set; }
        public string Value { get; set; }
    }
}