﻿using PatientCare.Core.Entities;
using PatientCare.Core.Enums;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Services.FileStorage;
using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.Exewrcise_ExerciseActivities
{
    [DataContract]
    public class Exercise_ExerciseActivityResponse
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long? activity_Id { get; set; }

        [DataMember]
        public long? exercise_Id { get; set; }
    }
}