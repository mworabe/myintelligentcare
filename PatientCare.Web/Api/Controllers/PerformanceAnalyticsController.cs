﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Mapping;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Web.Api.Models.InjuryRegions;
using Swashbuckle.Swagger.Annotations;
using System.Linq.Expressions;
using PatientCare.Web.Resources.area.Resources;
using PatientCare.Utilities;
using System.Web.Http.Description;
using PatientCare.Data.AbstractDataContext;
using System.Linq;
using PatientCare.Web.Api.Models;
using PatientCare.Web.Api.Models.Picklist;
using PatientCare.Web.Api.Models.AutoCompletes;
using System.Collections.Generic;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.ApiMobile.Models.PatientWorkoutAnalytics;
using PatientCare.Web.Api.Models.Schedulers;

namespace PatientCare.Web.Api.Controllers
{
    /// <summary>
    /// This Controller is used to handle Performance Analytics operations.
    /// </summary>
    [Authorize]
    public sealed class PerformanceAnalyticsController : ApiBaseController
    {
        /// <summary>
        /// Description :  Performance Analytics IPatientWorkoutAnalyticRepository.
        /// </summary>
        private readonly IPatientWorkoutAnalyticRepository _patientWorkoutAnalyticRepository;
        /// <summary>
        /// Description :  Performance Analytics IDataContextScopeFactory.
        /// </summary>
        private readonly IDataContextScopeFactory _dataContextScopeFactory;

        /// <summary>
        /// Description : Initializes a new instance of the Performance Analytics Class.     
        /// </summary>
        /// <param name="patientWorkoutAnalyticRepository">IPatient Workout Analytic Repository</param>
        /// <param name="dataContextScopeFactory">IData Context Scope Factory</param>
        public PerformanceAnalyticsController
            (
               IPatientWorkoutAnalyticRepository patientWorkoutAnalyticRepository,
               IDataContextScopeFactory dataContextScopeFactory

            )
        {
            _patientWorkoutAnalyticRepository = patientWorkoutAnalyticRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/PerformanceAnalytics/" + id, UriKind.Relative);
        }

        /// <summary>
        /// Method Description : Using for Get Patient Workout by caseId. 
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Patient Workout Analytic Response Model</returns>
        [HttpGet]
        [Route("api/PerformanceAnalytics/{id}")]// caseId
        public async Task<IHttpActionResult> GetPatientsWorkout(long id)
        {
            var response = new PatientWorkoutAnalyticResponse[0];
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                var analytics = ctx.PatientWorkoutAnalytics.AsNoTracking().Where(x => x.CaseId == id).OrderByDescending(x => x.StartDate).ToList();
                //analytics.OrderByDescending(x => x.StartDate).ToList();
                if (analytics != null && analytics.Count > 0)
                {
                    response = Mapper.Map<PatientWorkoutAnalytic[], PatientWorkoutAnalyticResponse[]>(analytics.ToArray());
                }
            }
            return Ok(response);
        }

        /// <summary>
        /// Method Description : Using for Get Patient Workout. 
        /// </summary>
        /// <param name="request">Patient Workout Analytic List Request Model</param>
        /// <returns>Patient Workout Analytic Response Model</returns>
        [HttpGet]
        [Route("api/PerformanceAnalytics")]
        [SwaggerResponse(200, "Ok", typeof(ListResponse<PatientWorkoutAnalyticListResponse>))]
        public async Task<IHttpActionResult> GetAllPatientAppointment([FromUri]PatientWorkoutAnalyticListRequest request)
        {
            List<PatientWorkoutAnalyticListResponse> analytics = new List<PatientWorkoutAnalyticListResponse>();
            using (var dbContextScope = _dataContextScopeFactory.Create())
            {
                var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();
                string query = "EXEC sp_Patient_Analytics @caseId = '" + request.CaseId + "',@patientId = '" + request.PatientId + "',@startDate = '" + request.From.Value.ToString("MM/dd/yyyy") + "',@endDate = '" + request.To.Value.ToString("MM/dd/yyyy") + "'";
                analytics = await ctx.Database.SqlQuery<PatientWorkoutAnalyticListResponse>(query).ToListAsync();
                return Ok(analytics.ToArray());
            }
        }
    }
}
