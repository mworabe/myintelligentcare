﻿"use strict";

angular.module("EDZoutstaffingPortalApp")
    .factory("ConfigLayout", [
        "DYNAMIC_FORM_CONSTROLLER",
        function (DYNAMIC_FORM_CONSTROLLER) {
            var model = {};


            /**
                General Fields Configurations for Forms..
            */
            model.createField = createField;
            model.addRequiredCustomField = addRequiredCustomField;
            model.restrictFields = restrictFields;

            /**
                Resource Fields Configurations.
            */
            model.getResourceTab = getResourceTab;
            model.getResourceFields = getResourceFields;
            model.getResourceEditTabList = getResourceEditTabList;
            model.getDefaultResourceContainer = getDefaultResourceContainer;

            /**
                Patient Fields Configurations.
            */
            model.getPatientTab = getPatientTab;
            model.getPatientEditTabs = getPatientEditTabs;
            model.getPatientFields = getPatientFields;
            model.getPatientsHiddenFieldsForNewModel = getPatientsHiddenFieldsForNewModel;
            model.getPatientEditTabList = getPatientEditTabList;
            model.getDefaultPatientContainer = getDefaultPatientContainer;

            /**
                Patient Case Fields Configurations.
            */
            model.getPatientCaseTab = getPatientCaseTab;
            model.getPatientCaseEditTab = getPatientCaseEditTab;
            model.getPatientCaseFields = getPatientCaseFields;
            model.getPatientCaseEditTabList = getPatientCaseEditTabList;
            model.getDefaultPatientCaseContainer = getDefaultPatientCaseContainer;


            function addRequiredCustomField(loadFields, customFieldValues, container) {

                var containerName = "container";
                var items = [];
                _.forEach(loadFields, function (container) {

                    for (var i = 1; ; i++) {
                        var itemContainer = container[containerName + i];
                        if (!itemContainer)
                            break;
                        _.forEach(itemContainer, function (field) {
                            items.push(field);
                        });
                    }
                });
                var customFieldIds = _.map(_.filter(customFieldValues, function (item) { return item.customField.require }), "customFieldId");
                var diff = _.differenceWith(customFieldIds, items, function (item1, item2) {
                    return item1.toString() === item2.id;
                });
                var i = 3;
                _.forEach(diff, function (item) {
                    var containerIndex = (i % 3) + 4;
                    i++;
                    container[containerName + containerIndex].push(createField(item.toString(), "customField", true, item));

                });
            }

            function restrictFields(fields, fieldList) {
                _.forEach(fields, function (field) {
                    _.forEach(fieldList, function (_field) {
                        if (_field.id === field.id)
                            _field.isReadOnly = true;
                    })
                });

                return fieldList;
            }

            function createField(id, name, require, customFieldId) {
                return {
                    id: id.toString(),
                    name: name,
                    require: require,
                    customFieldId: customFieldId
                }
            }

            /*  Patients    */
            function getPatientTab() {
                return [{
                    id: "patientDetails",
                    container: 'patientDetails',
                    name: "Patient Information",
                    required: true,
                    visible: true,
                }, {
                    id: "patientCases",
                    container: 'patientCases',
                    name: "Case History",
                    required: true,
                    visible: true,
                }, {
                    id: "patientHep",
                    container: 'patientCases',
                    name: "HEP",
                    required: true,
                    visible: true,
                },/* {
                    id: "schedualHistory",
                    container: 'schedualHistory',
                    name: "Schedual History",
                    required: true,
                    visible: true,
                },*/ {
                    id: "documents",
                    container: 'documents',
                    name: "Documents",
                    required: true,
                    visible: true,
                }, {
                    id: "communication",
                    container: 'communication',
                    name: "Communications",
                    required: true,
                    visible: true,
                },/* {
                    id: "billing",
                    container: 'billing',
                    name: "Billing",
                    required: true,
                    visible: true,
                }, {
                    id: "chartNotes",
                    container: 'chartNotes',
                    name: "Chart Notes",
                    required: true,
                    visible: true,
                }, {
                    id: "outcomes",
                    container: 'outcomes',
                    name: "Outcomes",
                    required: true,
                    visible: true,
                }, {
                    id: "summary",
                    container: 'summary',
                    name: "Summary",
                    required: true,
                    visible: true,
                },*/
                {
                    id: "mobileAccess",
                    container: 'mobileAccess',
                    name: "Mobile Access",
                    required: true,
                    visible: true,
                },/*
                {
                    id: "logHistory",
                    container: 'logHistory',
                    name: "Log History",
                    required: true,
                    visible: true,
                }, */];
            }
            function getPatientEditTabs() {
                return [{
                    id: "patientDetails",
                    container: 'patientDetails',
                    name: "Patient Information",
                    required: true,
                    visible: true,
                }, {
                    id: "patientCases",
                    container: 'patientCases',
                    name: "Patient Cases",
                    required: true,
                    visible: true,
                },/* {
                    id: "schedualHistory",
                    container: 'schedualHistory',
                    name: "Schedual History",
                    required: true,
                    visible: true,
                },/* {
                    id: "documents",
                    container: 'documents',
                    name: "Documents",
                    required: true,
                    visible: true,
                }, {
                    id: "communication",
                    container: 'communication',
                    name: "Communications",
                    required: true,
                    visible: true,
                }, {
                    id: "billing",
                    container: 'billing',
                    name: "Billing",
                    required: true,
                    visible: true,
                }, {
                    id: "chartNotes",
                    container: 'chartNotes',
                    name: "Chart Notes",
                    required: true,
                    visible: true,
                }, {
                    id: "outcomes",
                    container: 'outcomes',
                    name: "Outcomes",
                    required: true,
                    visible: true,
                }, {
                    id: "summary",
                    container: 'summary',
                    name: "Summary",
                    required: true,
                    visible: true,
                }, {
                    id: "logHistory",
                    container: 'logHistory',
                    name: "Log History",
                    required: true,
                    visible: true,
                }, */];
            }
            function getPatientEditTabList() {
                return [{
                    id: "patientDetails",
                    container: 'patientDetails',
                    name: "Patient Information",
                    required: true,
                    visible: true,
                }, {
                    id: "patientCases",
                    container: 'patientCases',
                    name: "Patient Cases",
                    required: true,
                    visible: true,
                },/* {
                    id: "schedualHistory",
                    container: 'schedualHistory',
                    name: "Schedual History",
                    required: true,
                    visible: true,
                },*/
                {
                    id: "documents",
                    container: 'documents',
                    name: "Documents",
                    required: true,
                    visible: true,
                },
                /*{
                    id: "communication",
                    container: 'communication',
                    name: "Communications",
                    required: true,
                    visible: true,
                }, {
                    id: "billing",
                    container: 'billing',
                    name: "Billing",
                    required: true,
                    visible: true,
                }, {
                    id: "chartNotes",
                    container: 'chartNotes',
                    name: "Chart Notes",
                    required: true,
                    visible: true,
                }, {
                    id: "outcomes",
                    container: 'outcomes',
                    name: "Outcomes",
                    required: true,
                    visible: true,
                }, {
                    id: "summary",
                    container: 'summary',
                    name: "Summary",
                    required: true,
                    visible: true,
                }, {
                    id: "logHistory",
                    container: 'logHistory',
                    name: "Log History",
                    required: true,
                    visible: true,
                },*/ ];
            }
            function getPatientsHiddenFieldsForNewModel() {
                return [{
                    id: "status"
                }, {
                    id: "patientNumber"
                }, {
                    id: "overview"
                }, ];
            }
            function getPatientFields() {
                return [
                    /*   container 1   */
                     {
                         id: "status",
                         name: "Status",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: true,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                     }, {
                         id: "patientNumber",
                         name: "Patient ID",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         isReadOnly: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "overview",
                         name: "Overview",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-6",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "prefix",
                         name: "Title",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-1",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                     }, {
                         id: "firstName",
                         name: "First Name",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: true,
                         visible: true,
                         containerCssClass: "col-md-2",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "middleName",
                         name: "Middle Name",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "lastName",
                         name: "Last Name",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: true,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "nickName",
                         name: "Nick Name",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "ssn",
                         name: "SSN",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: true,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.MASKEDTEXT,
                     }, {
                         id: "webPTPatientId",
                         name: "Incoming Patient ID",
                         container: "patientDetails",
                         position: "1",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "gender",
                         name: "Gender",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                     }, {
                         id: "maritialStatus",
                         name: "Marital Status",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: true,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                     }, {
                         id: "birthDate",
                         name: "Date of Birth",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: true,
                         visible: true,
                         containerCssClass: "col-md-2",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                     }, {
                         id: "age",
                         name: "Age",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         isReadOnly: true,
                         visible: true,
                         containerCssClass: "col-md-1",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "referralPhysician",
                         name: "Referring Physician",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                     }, {
                         id: "returnToPhysician",
                         name: "Return To Physician",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                     }, {
                         id: "clinician",
                         name: "Assigned Clinician",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                     }, {
                         id: "clinic",
                         name: "Clinic",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: true,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                     }, {
                         id: "clinicLocation",
                         name: "Clinic Location",
                         container: "patientDetails",
                         position: "1",
                         pattern: ".{0,255}",
                         required: true,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                     },
                     /* Container 1  ---------- */
                     /* Contact Info    */
                      {
                          id: "email",
                          name: "Email",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: true,
                          visible: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                      }, {
                          id: "cellNumber",
                          name: "Cell Phone",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          isPrimaryButtonVisible: true,
                          primaryFieldId: "cellNumberIsPrimary",
                          required: true,
                          visible: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                      }, {
                          id: "homeNumber",
                          name: "Home Phone",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          isPrimaryButtonVisible: true,
                          primaryFieldId: "homeNumberIsPrimary",
                          required: true,
                          visible: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                      }, {
                          id: "prefferedContactMethod",
                          name: "Preffered Contact Method",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: false,
                          visible: true,
                          specialName: "title",
                          specialValue: "value",
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                      }, {
                          id: "add1",
                          name: "Address 1",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          isPrimaryButtonVisible: true,
                          primaryFieldId: "address1IsPrimary",
                          required: true,
                          visible: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                      }, {
                          id: "city1",
                          name: "City",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: true,
                          visible: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                          descriptionField: "stateName",
                      }, {
                          id: "state1",
                          name: "State",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: true,
                          visible: true,
                          containerCssClass: "col-md-2",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                      }, {
                          id: "country1",
                          name: "Country",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: true,
                          visible: true,
                          containerCssClass: "col-md-2",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                      }, {
                          id: "zip1",
                          name: "Postal Code",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: true,
                          visible: true,
                          containerCssClass: "col-md-2",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                      }, {
                          id: "add2",
                          name: "Address 2",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          isPrimaryButtonVisible: true,
                          primaryFieldId: "address2IsPrimary",
                          required: false,
                          visible: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                      }, {
                          id: "city2",
                          name: "City",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: false,
                          visible: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                      }, {
                          id: "state2",
                          name: "State",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: false,
                          visible: true,
                          containerCssClass: "col-md-2",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                      }, {
                          id: "country2",
                          name: "Country",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: false,
                          visible: true,
                          containerCssClass: "col-md-2",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                      }, {
                          id: "zip2",
                          name: "Postal Code",
                          container: "patientDetails",
                          position: "2",
                          pattern: ".{0,255}",
                          required: false,
                          visible: true,
                          containerCssClass: "col-md-2",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                      },
                      {
                          id: "medicareCapOT",
                          name: "Medicare Cap - OT",
                          container: "patientDetails",
                          position: "1",
                          required: false,
                          visible: true,
                          isReadOnly: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CURRENCY,
                      },
                      {
                          id: "remainingYTD",
                          name: "Remaining YTD",
                          container: "patientDetails",
                          position: "1",
                          required: false,
                          visible: true,
                          isReadOnly: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CURRENCY,
                      }, {
                          id: "medicareCapPT_SLP",
                          name: "Medicare Cap - PT/SLP",
                          container: "patientDetails",
                          position: "1",
                          required: false,
                          visible: true,
                          isReadOnly: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CURRENCY,
                      },
                      {
                          id: "remainingYTD_PT_SLP",
                          name: "Remaining YTD",
                          container: "patientDetails",
                          position: "1",
                          required: false,
                          visible: true,
                          isReadOnly: true,
                          containerCssClass: "col-md-3",
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CURRENCY,
                      },





                      /* Contact Info   ------------     */
                      /*    Emergency Contacts  */
                       {
                           id: "emergencyContact",
                           name: "Emergency Contact Name",
                           container: "patientDetails",
                           position: "3",
                           pattern: ".{0,255}",
                           required: true,
                           visible: true,
                           containerCssClass: "col-md-3",
                           type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                       }, {
                           id: "relationshipToContact",
                           name: "Relationship to Contact",
                           container: "patientDetails",
                           position: "3",
                           pattern: ".{0,255}",
                           required: true,
                           visible: true,
                           specialName: "title",
                           specialValue: "value",
                           containerCssClass: "col-md-3",
                           type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                       }, {
                           id: "emergencyPhone",
                           name: "Emergency Contact Number",
                           container: "patientDetails",
                           position: "3",
                           pattern: ".{0,255}",
                           required: false,
                           visible: true,
                           containerCssClass: "col-md-3",
                           type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                       },
                       /*    Emergency Contacts  -----------------  */
                       /*    Other  */
                     {
                         id: "employerName",
                         name: "Employer Name",
                         container: "patientDetails",
                         position: "4",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "employerPhone",
                         name: "Employer Phone #",
                         container: "patientDetails",
                         position: "4",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                     }, {
                         id: "language",
                         name: "Preffered Language",
                         container: "patientDetails",
                         position: "4",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                     }, {
                         id: "specialAccomodations",
                         name: "Special Accomodations",
                         container: "patientDetails",
                         position: "4",
                         pattern: ".{0,255}",
                         required: false,
                         visible: true,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                     }, {
                         id: "firstVisitDate",
                         name: "First Visit Date",
                         container: "patientDetails",
                         position: "4",
                         pattern: ".{0,255}",
                         required: false,
                         visible: false,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                     }, {
                         id: "lastVisitDate",
                         name: "Last Visit Date",
                         container: "patientDetails",
                         position: "4",
                         pattern: ".{0,255}",
                         required: false,
                         visible: false,
                         containerCssClass: "col-md-3",
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                     },
                   /*   Legal Gardian Details    */
                   {
                       id: "gardianFirstName",
                       name: "First Name",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       required: false,
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                   }, {
                       id: "gardianLastName",
                       name: "Last Name",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       required: false,
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                   }, {
                       id: "relationshipToPatient",
                       name: "Relationship  to Patient",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       required: false,
                       specialName: "title",
                       specialValue: "value",
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                   }, {
                       id: "address",
                       name: "Address",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       required: false,
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                   }, {
                       id: "city",
                       name: "City",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       required: false,
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                   }, {
                       id: "state",
                       name: "State",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       required: false,
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                   }, {
                       id: "country",
                       name: "Country",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       required: false,
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                   }, {
                       id: "zip",
                       name: "Zip",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       required: false,
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                   }, {
                       id: "gardianHomePhone",
                       name: "Home Phone",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       isPrimaryButtonVisible: true,
                       required: false,
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                   }, {
                       id: "gardianCellPhone",
                       name: "Cell Phone",
                       container: "patientDetails",
                       position: "4",
                       pattern: ".{0,255}",
                       isPrimaryButtonVisible: true,
                       required: false,
                       visible: false,
                       containerCssClass: "col-md-3",
                       type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                   },
                ]
            }
            function getDefaultPatientContainer() {
                var patientFields = getPatientFields();
                function initialDefaultValue(container, position) {
                    var items = _.filter(patientFields, { container: container, position: position });
                    return items;
                }

                function initialContainers(containers) {
                    _.forEach(containers, function (item) {
                        item.container1 = [];
                        item.container2 = [];
                        item.container3 = [];
                        item.container4 = [];
                        item.container5 = [];
                        item.container6 = [];
                        item.container7 = [];

                        Array.prototype.push.apply(item.container1, initialDefaultValue(item.id, "1"));
                        Array.prototype.push.apply(item.container2, initialDefaultValue(item.id, "2"));
                        Array.prototype.push.apply(item.container3, initialDefaultValue(item.id, "3"));
                        Array.prototype.push.apply(item.container4, initialDefaultValue(item.id, "4"));
                        Array.prototype.push.apply(item.container5, initialDefaultValue(item.id, "5"));
                        Array.prototype.push.apply(item.container6, initialDefaultValue(item.id, "6"));
                    });
                }
                var containers = getPatientEditTabList();
                initialContainers(containers);
                return containers;
            }

            /*  Patient Case    */
            function getPatientCaseTab() {
                return [{
                    id: "patientCaseDetails",
                    container: 'patientCaseDetails',
                    name: "Details",
                    required: true,
                    visible: true,
                }, {
                    id: "records",
                    container: 'records',
                    name: "Records",
                    required: false,
                    visible: true,
                }, {
                    id: "hep",
                    container: 'hep',
                    name: "HEP",
                    required: true,
                    visible: true,
                }, {
                    id: "currentCondition",
                    container: 'currentCondition',
                    name: "Current Condition",
                    required: false,
                    visible: true,
                },{
                      id: "medicalCondition",
                      container: 'MedicalCondition',
                      name: "Medical Condition",
                      required: false,
                      visible: true,
                },{
                      id: "outcomes",
                      container: 'outcomes',
                      name: "Outcomes",
                      required: false,
                      visible: true,
                  }
                ]
            }
            function getPatientCaseEditTab() {
                return [{
                    id: "patientCaseDetails",
                    container: 'patientCaseDetails',
                    name: "Details",
                    required: true,
                    visible: true,

                },
                {
                    id: "hep",
                    container: 'hep',
                    name: "HEP",
                    required: true,
                    visible: true,
                },
                ]
            }
            function getPatientCaseEditTabList() {
                return [{
                    id: "patientCaseDetails",
                    container: 'patientCaseDetails',
                    name: "Details",
                    required: true,
                    visible: true,
                }, {
                    id: "hep",
                    container: 'hep',
                    name: "HEP",
                    required: true,
                    visible: false,
                }, ]
            }
            /*
            {
                    id: "caseStatus",
                    name: "Status",
                    container: "caseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                }, {
                    id: "startDate",
                    name: "Start Date",
                    container: "caseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                }, {
                    id: "birthDate",
                    name: "Birth Date",
                    container: "caseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                }, {
                    id: "age",
                    name: "Age",
                    container: "caseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                }, {
                    id: "startDate",
                    name: "Start Date",
                    container: "caseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                },
            */
            function getPatientCaseFields() {
                return [{
                    id: "caseStatus",
                    name: "Status",
                    container: "patientCaseDetails",
                    position: "1",
                    required: true,
                    visible: true,
                    containerCssClass: "col-md-3",
                    specialName: "title",
                    specialValue: "value",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                }, {
                    id: "startDate",
                    name: "Start Date",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                }, {
                    id: "injuryRegion",
                    name: "Injury Region",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                }, {
                    id: "primayDiagnosis",
                    name: "Primay Diagnosis",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                }, {
                    id: "secondaryDiagnosis",
                    name: "Secondary Diagnosis",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: false,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                }, {
                    id: "relatedCause",
                    name: "Related Cause",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: false,
                    visible: true,
                    specialName: "title",
                    specialValue: "value",
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                }, {
                    id: "assignedPhysician",
                    name: "Assigned Physician",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTO_COMPLETE_WITH_BUTTON,
                }, {
                    id: "referingPhysician",
                    name: "Refering Physician",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: false,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                }, {
                    id: "returnTo",
                    name: "Return To Referral",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: false,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                }, {
                    id: "clinic",
                    name: "Clinic",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                }, {
                    id: "clinicLocation",
                    name: "Clinic Location",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: true,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                }, {
                    id: "webPTCaseId",
                    name: "Incoming Case #",
                    container: "patientCaseDetails",
                    position: "1",
                    required: false,
                    visible: true,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                }, {
                    id: "prescription",
                    name: "Prescription",
                    container: "patientCaseDetails",
                    position: "1",
                    pattern: ".{0,255}",
                    required: false,
                    visible: false,
                    containerCssClass: "col-md-3",
                    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXTAREA,
                }, ];
            }
            function getDefaultPatientCaseContainer() {
                var patientFields = getPatientCaseFields();
                function initialDefaultValue(container, position) {
                    var items = _.filter(patientFields, { container: container, position: position });
                    return items;
                }

                function initialContainers(containers) {
                    _.forEach(containers, function (item) {
                        item.container1 = [];
                        item.container2 = [];
                        item.container3 = [];
                        item.container4 = [];
                        item.container5 = [];
                        item.container6 = [];
                        item.container7 = [];

                        Array.prototype.push.apply(item.container1, initialDefaultValue(item.id, "1"));
                        Array.prototype.push.apply(item.container2, initialDefaultValue(item.id, "2"));
                        Array.prototype.push.apply(item.container3, initialDefaultValue(item.id, "3"));
                        Array.prototype.push.apply(item.container4, initialDefaultValue(item.id, "4"));
                        Array.prototype.push.apply(item.container5, initialDefaultValue(item.id, "5"));
                        Array.prototype.push.apply(item.container6, initialDefaultValue(item.id, "6"));
                    });
                }
                var containers = getPatientCaseEditTabList();
                initialContainers(containers);
                return containers;
            }

            /*  Resources   */
            function getResourceEditTabList() {
                return [
                    {
                        name: "Personal Info",
                        id: "personalInfo"
                    },
                    /*{
                        name: "Working Details",
                        id: "workingDetails"
                    },*/
                    {
                        name: "Background Check",
                        id: "backgroundCheck"
                    },
                    {
                        name: "Other",
                        id: "other"
                    },
                ];
            }
            function getResourceFields() {
                return [
                    {
                        id: "avatar",
                        name: "Avatar",
                        container: "personalInfo",
                        position: "1",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/avatarV2.html",
                        isVisibleOnSummary: false,
                    },
                    {
                        id: "rating",
                        name: "Rating",
                        container: "personalInfo",
                        position: "1",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/ratingV2.html",
                        isVisibleOnSummary: false,
                    },
                    {
                        id: "productivity",
                        name: "Productivity",
                        container: "personalInfo",
                        position: "1",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/productivityV2.html",
                        isVisibleOnSummary: false,
                    },
                    {
                        id: "range",
                        name: "Range",
                        container: "personalInfo",
                        position: "1",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/rangeV2.html",
                        isVisibleOnSummary: false,
                    },
                    {
                        id: "overview",
                        name: "Additional Information",
                        container: "personalInfo",
                        position: "1",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXTAREA,
                        isVisibleOnSummary: false,
                        rows: 6,
                    },
                    {
                        id: "firstName",
                        name: "First Name",
                        required: true,
                        pattern: ".{0,255}",
                        container: "personalInfo",
                        position: "2",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: false,
                    },

                    {
                        id: "middleName",
                        name: "Middle Name",
                        required: false,
                        pattern: ".{0,255}",
                        container: "personalInfo",
                        position: "2",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: false,
                    },
                    {
                        id: "lastName",
                        name: "Last Name",
                        required: true,
                        pattern: ".{0,255}",
                        container: "personalInfo",
                        position: "2",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: false,
                    },
                    {
                        id: "birthDate",
                        name: "Birth Date",
                        required: false,
                        container: "personalInfo",
                        position: "2",
                        //customErrorMessage: "The age should be more than 15 years, and less than 100",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "gender",
                        name: "Gender",
                        required: false,
                        container: "personalInfo",
                        position: "2",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "martialStatus",
                        name: "Martial Status",
                        container: "personalInfo",
                        position: "2",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "ethnicity",
                        name: "Ethnicity",
                        container: "personalInfo",
                        position: "2",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "prefix",
                        name: "Prefix",
                        container: "personalInfo",
                        position: "2",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "suffix",
                        name: "Suffix",
                        container: "personalInfo",
                        position: "2",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "preferredFirstname",
                        name: "Preferred First Name",
                        container: "personalInfo",
                        position: "2",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "email",
                        name: "Email",
                        container: "personalInfo",
                        position: "2",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "timekeeperNumber",
                        name: "Timekeeper Number",
                        container: "personalInfo",
                        position: "2",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "address",
                        name: "Address 1",
                        container: "personalInfo",
                        position: "3",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "address2",
                        name: "Address 2",
                        container: "personalInfo",
                        position: "3",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "address3",
                        name: "Address 3",
                        container: "personalInfo",
                        position: "3",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "city",
                        name: "City",
                        container: "personalInfo",
                        position: "3",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                        isVisibleOnSummary: true,
                        descriptionField: "stateName",
                    },
                    {
                        id: "state",
                        name: "State Province",
                        container: "personalInfo",
                        position: "3",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "country",
                        name: "Country",
                        container: "personalInfo",
                        position: "3",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "zipCode",
                        name: "Postal Code",
                        container: "personalInfo",
                        position: "3",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                        titleField: "zipCode",
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "lgbt",
                        name: "LGBT",
                        container: "personalInfo",
                        position: "2",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                        specialName: "title",
                        specialValue: "value",
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "active",
                        name: "Active",
                        container: "personalInfo",
                        position: "3",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CHECKBOX,
                        isVisibleOnSummary: false,
                    },
                    {
                        id: "employeeId",
                        name: "Employee ID",
                        container: "personalInfo",
                        position: "4",
                        required: true,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: false,
                    },
                    {
                        id: "jobTitle",
                        name: "Job Title",
                        container: "personalInfo",
                        position: "4",
                        required: true,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                        isVisibleOnSummary: true,
                    }, {
                        id: "startDate",
                        name: "Start Date",
                        container: "personalInfo",
                        position: "4",
                        required: true,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                        maxDateField: "endDate",
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "endDate",
                        name: "Last Day Employed",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "ssn",
                        name: "SSN",
                        required: false,
                        pattern: ".{0,255}",
                        container: "personalInfo",
                        position: "4",
                        max_length: 9,
                        min_length: 9,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.MASKEDTEXT,
                        isVisibleOnSummary: false,
                    },

                    //{
                    //    id: "location",
                    //    name: "Location",
                    //    container: "personalInfo",
                    //    position: "4",
                    //    required: true,
                    //    pattern: ".{0,255}",
                    //    type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                    //    isVisibleOnSummary: true,
                    //},

                    {
                        id: "clinic",
                        name: "Clinic",
                        container: "personalInfo",
                        position: "4",
                        pattern: ".{0,255}",
                        required: true,
                        visible: true,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                    }, {
                        id: "clinicLocation",
                        name: "Location",
                        container: "personalInfo",
                        position: "4",
                        pattern: ".{0,255}",
                        required: true,
                        visible: true,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                    },
                    {
                        id: "officeNumber",
                        name: "Office Phone Number",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "extension",
                        name: "Extension",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                        isVisibleOnSummary: true,
                    }, {
                        id: "businessUnit",
                        name: "Business Unit",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                        isVisibleOnSummary: true,
                    }, {
                        id: "supervisor",
                        name: "Supervisor",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                        specialName: "name",
                        specialValue: "id",
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "timeZoneInfo",
                        name: "Time Zone (UTC)",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                        specialName: "name",
                        specialValue: "id",
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "homeNumber",
                        name: "Home Phone Number",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "industry",
                        name: "Industry",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.MULTI_SELECT,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "division",
                        name: "Division",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "resourcePracticeAreas",
                        name: "Practices",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.MULTI_SELECT,
                        isVisibleOnSummary: true,
                    },
                  {
                      id: "department",
                      name: "Department",
                      container: "personalInfo",
                      position: "4",
                      required: false,
                      type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE,
                      isVisibleOnSummary: true,
                  },
                     {
                         id: "mobilePhoneNumber",
                         name: "Mobile Phone Number",
                         container: "personalInfo",
                         position: "4",
                         required: false,
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                         isVisibleOnSummary: true,
                     },
                      {
                          id: "cubicalOrOfficeNumber",
                          name: "Cubical/Office Number",
                          container: "personalInfo",
                          position: "4",
                          required: false,
                          type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                          isVisibleOnSummary: true,
                      },
                     {
                         id: "emergencyContactName",
                         name: "Emergency Contact Name",
                         container: "personalInfo",
                         position: "4",
                         required: false,
                         type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT,
                         isVisibleOnSummary: true,
                     },
                    {
                        id: "relationship",
                        name: "Relationship",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                        specialName: "title",
                        specialValue: "value",
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "emergencyContactPhoneNumber",
                        name: "Emergency Contact Phone Number",
                        container: "personalInfo",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.PHONE,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "firmRate",
                        name: "Firm Rate",
                        container: "personalInfo",
                        position: "5",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CURRENCY,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "nationalRate",
                        name: "National Rate",
                        container: "personalInfo",
                        position: "5",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CURRENCY,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "employmentType",
                        name: "Employment Type",
                        container: "personalInfo",
                        position: "5",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.UI_SELECT,
                        specialName: "title",
                        specialValue: "value",
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "salary",
                        name: "Salary",
                        container: "personalInfo",
                        position: "5",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CURRENCY,
                        isVisibleOnSummary: true,
                    },
                    {
                        id: "bonusOrOtherPay",
                        name: "Bonus or Other Pay",
                        container: "personalInfo",
                        position: "5",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CURRENCY,
                        isVisibleOnSummary: true,
                    },

                    {
                        id: "rating",
                        name: "Value",
                        container: "performanceReview",
                        position: "7",
                        required: false,
                        isReadOnly: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.NUMBER,
                        isVisibleOnSummary: false,
                    }, {
                        id: "ratingDate",
                        name: "Date",
                        container: "performanceReview",
                        position: "7",
                        required: false,
                        isReadOnly: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.DATE_TIME,
                        isVisibleOnSummary: false,
                    },
                    {
                        id: "yearsEmployed",
                        name: "Years Employed",
                        container: "workingDetails",
                        position: "1",
                        required: false,
                        isReadOnly: true,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT
                    },
                    {
                        id: "monthsEmployed",
                        name: "Months Employed",
                        container: "workingDetails",
                        position: "1",
                        required: false,
                        isReadOnly: true,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT
                    },
                    {
                        id: "availableToTravel",
                        name: "Available to Travel",
                        container: "travelDetails",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CHECKBOX
                    },
                    {
                        id: "validPassport",
                        name: "Valid Passport",
                        container: "travelDetails",
                        position: "4",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CHECKBOX
                    },
                    {
                        id: "armedForcesCountry",
                        name: "Armed Forces Country",
                        required: false,
                        container: "backgroundCheck",
                        position: "1",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.AUTOCOMPLETE
                    },
                    {
                        id: "armedForcesBranch",
                        name: "Armed Forces Branch",
                        required: false,
                        container: "backgroundCheck",
                        position: "1",
                        pattern: ".{0,255}",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXT
                    },
                    {
                        id: "armedForces",
                        name: "Active Duty",
                        container: "backgroundCheck",
                        position: "1",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CHECKBOX
                    },
                    {
                        id: "drugTest",
                        name: "Drug Screening",
                        container: "backgroundCheck",
                        position: "1",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXTAREA
                    },
                    {
                        id: "creditReportOk",
                        name: "Credit Check",
                        container: "backgroundCheck",
                        position: "1",
                        required: false,
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.TEXTAREA
                    },
                    {
                        id: "fingerPrinting",
                        name: "Finger Printing",
                        firstLabel: "Finger Printing",
                        firstId: "fingerPrinting",
                        secondLabel: "Finger Printing Remarks",
                        secondId: "fingerPrintingOther",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/background-check-pass-fail-UISelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    },

                    {
                        id: "drugScreening",
                        name: "Drug Screening",
                        firstLabel: "Drug Screening",
                        firstId: "drugScreening",
                        secondLabel: "Drug Screening Remarks",
                        secondId: "drugScreeningOther",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/background-check-pass-fail-UISelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    },

                    {
                        id: "creditCheck",
                        name: "Credit Check",
                        firstLabel: "Credit Check",
                        firstId: "creditCheck",
                        secondLabel: "Credit Check Remarks",
                        secondId: "creditCheckOther",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/background-check-pass-fail-UISelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    },

                    {
                        id: "motorVehicleReport",
                        name: "Motor Vehicle Report",
                        firstLabel: "Motor Vehicle Report",
                        firstId: "motorVehicleReport",
                        secondLabel: "Motor Vehicle Report Remarks",
                        secondId: "motorVehicleReportOther",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/backgroundCheckUiSelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    },
                    {
                        id: "employmentEligibilityVerification",
                        name: "Eligibility Verification",
                        firstLabel: "Eligibility Verification",
                        firstId: "employmentEligibilityVerification",
                        secondLabel: "Eligibility Verification Remarks",
                        secondId: "employmentEligibilityVerificationOther",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/backgroundCheckUiSelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    },
                    {
                        id: "internationalWorkHistory",
                        name: "International Work History",
                        firstLabel: "International Work History",
                        firstId: "internationalWorkHistory",
                        secondLabel: "International Work History Remarks",
                        secondId: "internationalWorkHistoryOther",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/backgroundCheckUiSelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    },
                    {
                        id: "professionalReferenceChecks",
                        name: "Professional Reference Checks",
                        firstLabel: "Professional Reference Checks",
                        firstId: "professionalReferenceChecks",
                        secondLabel: "Reference Checks Remarks",
                        secondId: "professionalReferenceChecksOther",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/backgroundCheckUiSelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    },
                    {
                        id: "formI9",
                        name: "Form I-9",
                        firstLabel: "Form I-9",
                        firstId: "formI9",
                        secondLabel: "Form I-9 Remarks",
                        secondId: "formI9Other",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/backgroundCheckUiSelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    },
                    {
                        id: "formEVerify",
                        name: "Form E-Verify",
                        firstLabel: "Form E-Verify",
                        firstId: "formEVerify",
                        secondLabel: "Form E-Verify Remarks",
                        secondId: "formEVerifyOther",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/backgroundCheckUiSelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    },
                    {
                        id: "credentialVerifications",
                        name: "Credential Verifications",
                        firstLabel: "Credential Verifications",
                        firstId: "credentialVerifications",
                        secondLabel: "Credential Verifications Remarks",
                        secondId: "credentialVerificationsOther",
                        type: DYNAMIC_FORM_CONSTROLLER.FIELD_TYPE.CUSTOM_TEMPLATE,
                        templateUrl: "Client/app/controllers/resource/templates/backgroundCheckUiSelect.html",
                        container: "backgroundCheck",
                        position: "4"
                    }
                ];
            }
            function getDefaultResourceContainer() {
                var resourceTabFields = getResourceFields();
                function initialDefaultValue(container, position) {
                    var items = _.filter(resourceTabFields, { container: container, position: position });
                    return items;
                }

                function initialContainers(containers) {
                    _.forEach(containers, function (item) {
                        item.container1 = [];
                        item.container2 = [];
                        item.container3 = [];
                        item.container4 = [];
                        item.container5 = [];
                        item.container6 = [];
                        item.container7 = [];

                        Array.prototype.push.apply(item.container1, initialDefaultValue(item.id, "1"));
                        Array.prototype.push.apply(item.container2, initialDefaultValue(item.id, "2"));
                        Array.prototype.push.apply(item.container3, initialDefaultValue(item.id, "3"));
                        Array.prototype.push.apply(item.container4, initialDefaultValue(item.id, "4"));
                        Array.prototype.push.apply(item.container5, initialDefaultValue(item.id, "5"));
                        Array.prototype.push.apply(item.container6, initialDefaultValue(item.id, "6"));
                        Array.prototype.push.apply(item.container6, initialDefaultValue(item.id, "7"));
                    });
                }
                var containers = getResourceEditTabList();
                initialContainers(containers);
                return containers;
            }
            function getResourceTab() {
                return [
                    {
                        id: "personalInformation",
                        container: 'personalInfo',
                        name: "Personal Info",
                        required: true,
                        visible: true,
                    }, {
                        id: "availability",
                        container: 'availability',
                        name: "Availability",
                        visible: true
                    }, {
                        id: "skills",
                        container: 'skills',
                        name: "Skills",
                        visible: true
                    }, {
                        id: "education",
                        container: 'educations',
                        name: "Education",
                        visible: true
                    }, {
                        id: "certificates",
                        container: 'certificates',
                        name: "Certificates",
                        visible: true
                    }, {
                        id: "continualEducationCredits",
                        container: 'continualEducationCredits',
                        name: "Continual Education Credits",
                        visible: true
                    }, {
                        id: "workExperience",
                        container: 'workExperience',
                        name: "Work Experience",
                        visible: true
                    }, {
                        id: "addResume",
                        container: '',
                        name: "Resume",
                        visible: true
                    }, {
                        id: "summary",
                        container: '',
                        name: "Summary",
                        visible: true
                    }, {
                        id: "resourcePerformance",
                        container: '',
                        name: "Performance Reviews",
                        visible: true
                    }, {
                        id: "goalsAndPerformance",
                        container: '',
                        name: "Goals & Performance",
                        visible: true
                    }, {
                        id: "other",
                        container: '',
                        name: "Other",
                        visible: true
                    }, {
                        id: "organizationalHierarchy",
                        container: '',
                        name: "Organizational Hierarchy",
                        visible: true
                    },
                ];
            }

            return model;
        }
    ]);