﻿using System.Runtime.Serialization;

namespace PatientCare.Web.Api.Models.PostalCodes
{
    [DataContract]
    public class PostalCodeResponse
    {
        [DataMember]
        public long Id { get; set; }
        
        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public string Name
        {
            get { return (ZipCode != null ? ZipCode : ""); }
            set { Name = value; }
        }

        [DataMember]
        public CodeResponse City { get; set; }

        [DataMember]
        public CodeResponse State { get; set; }

        [DataMember]
        public CodeResponse Country { get; set; }
    }    
}