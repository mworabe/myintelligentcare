﻿using System;

namespace PatientCare.Web.Api.Models.ResourceCertificates
{
    public class ResourceCertificateRequest
    {
        public long Id { get; set; }

        public long ResourceId { get; set; }

        public long CertificateId { get; set; }

        public DateTime Certified { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public long? CertificateStateId { get; set; }
        
        public DateTime? IssueDate { get; set; }
    }
}