﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class DashboardSetupMappingExtensions
    {
        public static DbModelBuilder MapDashboardSetup(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<DashboardSetup>().ToTable("dashboard_setups").HasKey(i => i.Id);

            modelBuilder.Entity<DashboardSetup>().Property(p => p.Name)
                .HasColumnName("name")
                .IsRequired()
                .IsUnicode()
                .HasMaxLength(255);

            modelBuilder.Entity<DashboardSetup>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<DashboardSetup>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<DashboardSetup>()
                .Property(p => p.SetupJson)
                .HasColumnName("setup-json")
                .IsOptional();

            return modelBuilder;
        }
    }
}
