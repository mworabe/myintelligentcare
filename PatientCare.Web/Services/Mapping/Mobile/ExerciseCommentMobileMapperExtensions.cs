﻿using System;
using PatientCare.Core.Entities;
using PatientCare.Services.Mapping;
using EmitMapper;
using PatientCare.Web.ApiMobile.Models.Ratings;

namespace PatientCare.Web.Services.Mapping.Mobile
{
    public static class ExerciseCommentMobileMapperExtensions
    {
        public static EmitMapperMapperProvider ExerciseCommentMobileRegisterMappings(
            this EmitMapperMapperProvider mapperProvider)
        {
            if (mapperProvider == null) throw new ArgumentNullException("mapperProvider");

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseComment, ExerciseCommentMobileResponse>
                    (new ExtendedMapConfig<ExerciseComment, ExerciseCommentMobileResponse>()));

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseComment[], ExerciseCommentMobileResponse[]>());

            mapperProvider.RegisterMapper(() =>
                ObjectMapperManager.DefaultInstance.GetMapper<ExerciseCommentMobileRequest, ExerciseComment>(
                        new ExtendedMapConfig<ExerciseCommentMobileRequest, ExerciseComment>()
                    ));

            return mapperProvider;
        }
    }
}