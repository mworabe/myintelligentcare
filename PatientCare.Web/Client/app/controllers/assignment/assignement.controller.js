﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("AssignmentController", ["AssignmentService",
        function (assignmentService) {
            this.get = assignmentService.query;
            this.save = assignmentService.save;
            this.update = assignmentService.update;
            this.delete = assignmentService.delete;
            this.cols = assignmentService.getTableOption;
        }
    ]);