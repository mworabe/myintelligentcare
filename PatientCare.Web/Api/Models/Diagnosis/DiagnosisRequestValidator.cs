﻿using FluentValidation;
using PatientCare.Web.Api.Models.Diagnosiss;

namespace PatientCare.Web.Api.Models.DropDownConfigs
{
    public class DiagnosisRequestValidator : AbstractValidator<DiagnosisRequest>
    {
        public DiagnosisRequestValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .Length(0, 255);            
        }
    }
}