﻿"use strict";
var EOPApp = angular.module("EDZoutstaffingPortalApp");
EOPApp.constant("AdvancedFilterConstants", {
    //advancedFilterDataTypes
    advancedFilterDataTypes: [
        {
            dataType: "TEXT",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                }, {
                    value: "LIKE",
                    name: "Like"
                },
            ],
        },
        {
            dataType: "DATE_TIME",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                }, {
                    value: ">",
                    name: "Greater than",
                }, {
                    value: "<",
                    name: "Less than",
                }, {
                    value: ">=",
                    name: "Greater than or Equal To",
                }, {
                    value: "<=",
                    name: "Less than or Equal To",
                }, {
                    value: "BETWEEN",
                    name: "Between",
                },
            ],
        },
        {
            dataType: "UI_SELECT",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                }, {
                    value: "IN",
                    name: "In"
                },
            ],
        },
        {
            dataType: "AUTOCOMPLETE",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                }, {
                    value: "IN",
                    name: "In"
                },
            ],
        },
        {
            dataType: "PHONE",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                }, {
                    value: "LIKE",
                    name: "Like"
                },
            ],
        },
        {
            dataType: "NUMBER",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                }, {
                    value: ">",
                    name: "Greater than",
                }, {
                    value: "<",
                    name: "Less than",
                }, {
                    value: ">=",
                    name: "Greater than or Equal To",
                }, {
                    value: "<=",
                    name: "Less than or Equal To",
                },
            ],
        },
        {
            dataType: "CHECKBOX",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                },
            ],
        },
        {
            dataType: "TEXTAREA",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                }, {
                    value: "LIKE",
                    name: "Like"
                },
            ],
        },
        {
            dataType: "CURRENCY",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                }, {
                    value: ">",
                    name: "Greater than",
                }, {
                    value: "<",
                    name: "Less than",
                }, {
                    value: ">=",
                    name: "Greater than or Equal To",
                }, {
                    value: "<=",
                    name: "Less than or Equal To",
                },
            ],
        },
        {
            dataType: "CUSTOM_TEMPLATE",
            supportedComparisionOperators: [
                {
                    value: "=",
                    name: "Equal To"
                },
            ],
        },
    ],
    // allComparisionOperators
    allComparisionOperators: [
        {
            value: "=",
            name: "Equal To"
        }, {
            value: ">",
            name: "Greater than",
            supportedDataTypes: [],
        }, {
            value: "<",
            name: "Less than",
            supportedDataTypes: [],
        }, {
            value: ">=",
            name: "Greater than or Equal To",
            supportedDataTypes: [],
        }, {
            value: "<=",
            name: "Less than or Equal To",
            supportedDataTypes: [],
        }, {
            value: "<>",
            name: "Not Equal To",
            supportedDataTypes: [],
        },
    ],
    //allLogicalOperators
    allLogicalOperators: [
        {
            value: "AND",
            name: "And"
        }, {
            value: "OR",
            name: "Or"
        }
    ],
});
EOPApp.filter('trimWhiteSpaces', [function () {
    return function (string) {
        if (!angular.isString(string)) {
            return string;
        }
        return string.replace(/[\s]/g, '');
    };
}])