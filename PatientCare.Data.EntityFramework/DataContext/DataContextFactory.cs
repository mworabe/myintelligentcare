﻿using PatientCare.Data.AbstractDataContext;

namespace PatientCare.Data.EntityFramework.DataContext
{
    public sealed class DataContextFactory : IDataContextFactory
    {
        public object CreateDataContext()
        {
            return new PatientCareDbContext();
        }
    }
}
