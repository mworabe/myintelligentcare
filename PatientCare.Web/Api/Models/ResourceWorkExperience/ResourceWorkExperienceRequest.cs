﻿using PatientCare.Web.Api.Models.ResourceWorkAssignment;
using System;

namespace PatientCare.Web.Api.Models.ResourceWorkExperience
{
    public class ResourceWorkExperienceRequest
    {
        public long Id { get; set; }

        public long ResourceId { get; set; }

        public long? JobTitleId { get; set; }
        
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        //public ResourceWorkAssignmentRequest[] ResourceWorkAssignments { get; set; }

        public ResourceWorkAssignmentRequest[] ResourceWorkAssignments { get; set; }
    }
}