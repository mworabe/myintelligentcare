﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using PatientCare.Core.Entities;

namespace PatientCare.Data.EntityFramework.Mappings
{
    internal static class HepMasterMappingExtensions
    {
        public static DbModelBuilder MapHepMaster(this DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException("modelBuilder");

            modelBuilder.Entity<HepMaster>().ToTable("hep_master")
                .HasKey(i => i.Id);

            modelBuilder.Entity<HepMaster>()
                .Property(p => p.Created)
                .HasColumnName("created")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<HepMaster>()
                .Property(p => p.Modified)
                .HasColumnName("modified")
                .IsRequired();

            modelBuilder.Entity<HepMaster>()
                .Property(p => p.PatientId)
                .HasColumnName("patient_id")
                .IsOptional();

            modelBuilder.Entity<HepMaster>()
                .HasOptional<Patient>(p => p.Patient)
                .WithMany()
                .HasForeignKey(p => p.PatientId);

            modelBuilder.Entity<HepMaster>()
                .Property(p => p.OwnerId)
                .HasColumnName("owner_id")
                .IsOptional();

            modelBuilder.Entity<HepMaster>()
                .HasOptional<Resource>(p => p.Owner)
                .WithMany()
                .HasForeignKey(p => p.OwnerId);

            modelBuilder.Entity<HepMaster>()
                .Property(p => p.PatientCaseId)
                .HasColumnName("patient_case_id")
                .IsOptional();

            modelBuilder.Entity<HepMaster>()
                .HasOptional<PatientCase>(p => p.PatientCase)
                .WithMany()
                .HasForeignKey(p => p.PatientCaseId);

            modelBuilder.Entity<HepMaster>()
                .Property(p => p.StartDate)
                .HasColumnName("start_date")
                .IsOptional();

            modelBuilder.Entity<HepMaster>()
                .Property(p => p.EndDate)
                .HasColumnName("end_date")
                .IsOptional();

            modelBuilder.Entity<HepMaster>()
                .HasMany<HepDetail>(p => p.HepDetails)
                .WithOptional()
                .HasForeignKey(item => item.HepId);

            modelBuilder.Entity<HepMaster>()
                .Property(p => p.IsSaveAsDraft)
                .HasColumnName("is_save_as_draft")
                .IsOptional();

            modelBuilder.Entity<HepMaster>()
                .HasMany<HepGroup>(p => p.HepGroups)
                .WithOptional()
                .HasForeignKey(item => item.HepId);

            return modelBuilder;
        }
    }
}
