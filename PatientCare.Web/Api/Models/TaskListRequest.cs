﻿namespace PatientCare.Web.Api.Models
{
    public class TasksListRequest : ListRequest
    {
        public bool OnlyMyTasks { get; set; }
    }
}