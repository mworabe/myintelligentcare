﻿namespace PatientCare.Web.Api.Models.ClinicLocations
{
    public class ClinicLocationRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long? ClinicId { get; set; }        

        public string ContactPerson { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string LocationPrefix { get; set; }

        public bool? IsDeleted { get; set; }

    }   
}