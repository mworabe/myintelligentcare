namespace PatientCare.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedChatUserListByChannelId : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
              "dbo.sp_Chat_User_List_ChannelId",
              p => new
              {
                  user_id = p.String(defaultValueSql: null),
                  channel_id = p.String(defaultValueSql: null)
              },
              body:
                   @"select 
		                chatmem.user_id as UserId
		                from chat_Sessions chats
		                left join chat_members chatmem on chats.Id = chatmem.chat_session_id
                    where chats.channel_id = @channel_id --and chatmem.user_id != @user_id"
                  );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.sp_Chat_User_List_ChannelId");
        }
    }
}
