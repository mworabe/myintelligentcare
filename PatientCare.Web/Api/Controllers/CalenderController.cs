﻿using System;
using System.Linq;
using System.Web.Http;
using PatientCare.Core.Entities;
using PatientCare.Core.Services;
using PatientCare.Data.AbstractRepository;
using PatientCare.Web.Api.Controllers.Abstract;
using PatientCare.Data.AbstractDataContext;
using PatientCare.Data.EntityFramework;
using PatientCare.Web.Api.Models.Calenders;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Collections.Generic;
using PatientCare.Web.Api.Models.LeaveRequests;
using PatientCare.Web.Services.Excel;
using PatientCare.Core.Enums;

namespace PatientCare.Web.Api.Controllers
{
    [Authorize]
    public sealed class CalenderController : ApiBaseController
    {
        private readonly ITimeManagementService _timeManagementService;
        private readonly ILeaveRequestRepository _leaveRequestRepository;
        private readonly IDataContextScopeFactory _dataContextScopeFactory;
        
        ReadOptions<LeaveRequest> _readOptions = new ReadOptions<LeaveRequest>();
        private PatientCareDbContext db = new PatientCareDbContext();

        
        public CalenderController
            (
                ITimeManagementService timeManagementService,
                ILeaveRequestRepository leaveRequestRepository,
                IDataContextScopeFactory dataContextScopeFactory
            )
        {
            _timeManagementService = timeManagementService;
            _leaveRequestRepository = leaveRequestRepository;
            _dataContextScopeFactory = dataContextScopeFactory;
        }

        private static Uri GetEntityLocation(long id)
        {
            return new Uri("/Calender/" + id, UriKind.Relative);
        }
        [HttpGet]
        [Route("api/Calender/MyTimeOff")]
        public List<CalenderResponse> GetAllMyTimeOff()
        {
            var userId = User.Identity.GetUserId();
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            List<CalenderResponse> lst = new List<CalenderResponse>();
            if (user.ResourceId != null)
            {
                //    ModelState.AddModelError("MyTimeOff:", "Current user have not assign any resources.please ask administrator to assign one.");
                //    return BadRequest(ModelState.ToString());

                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                    string s = _timeManagementService.GetStringForLeaveRequestList(Convert.ToInt32(user.ResourceId), false);
                    List<LeaveRequestsAdminApprovalListResponseView> leaveList = new List<LeaveRequestsAdminApprovalListResponseView>();
                    var query = ctx.Database.SqlQuery<LeaveRequestsAdminApprovalListResponseView>(s);
                    leaveList = query.ToList();

                    foreach (var item in leaveList)
                    {
                        CalenderResponse Cr = new CalenderResponse();
                        Cr.CalenderType = "MyTimeOff";
                        Cr.EventType = "TimeOff";
                        Cr.Id = item.Id;
                        Cr.ResourceName = item.ResourceName;
                        Cr.ResourceId = item.ResourceId;
                        Cr.SupervisorName = item.SupervisorName;
                        Cr.SupervisorId = item.SupervisorId;
                        Cr.Reason = item.Reason;
                        Cr.LeaveType = item.LeaveTitle;
                        Cr.LeaveTypeId = item.LeaveTypeId;
                        Cr.CommentFromSupervisor = (item.CommentFromSupervisor != null) ? item.CommentFromSupervisor : null;
                        Cr.IsPaidType = item.IsPaidType;
                        Cr.TakenLeaves = item.TakenLeaves;
                        Cr.RemainingLeave = Cr.RemainingLeave;
                        if (item.Status == "Approved")
                        {
                            Cr.ClassName = "calendar-timeoff-approved";
                            Cr.Title = "My Time Off Request - " + item.Status + ".";
                            Cr.StatusClassName = "calendar-timeoffstatus-approved";
                        }
                        if (item.Status == "Pending")
                        {
                            Cr.ClassName = "calendar-timeoff-pending";
                            Cr.Title = "My Time Off Request - " + item.Status + ".";
                            Cr.StatusClassName = "calendar-timeoffstatus-pending";
                        }
                        if (item.Status == "Rejected")
                        {
                            Cr.ClassName = "calendar-timeoff-rejected";
                            Cr.Title = "My Time Off Request - " + item.Status + ".";
                            Cr.StatusClassName = "calendar-timeoffstatus-rejected";
                        }
                        // string dateDiff = ((item.EndDate - item.StartDate).TotalDays).ToString();                        
                        Cr.Status = item.Status;
                        //Cr.Start = TimeZone.CurrentTimeZone.ToLocalTime(item.StartDate); 
                        //Cr.End = TimeZone.CurrentTimeZone.ToLocalTime(item.EndDate);
                        Cr.Start = item.StartDate;
                        Cr.End = item.EndDate;
                        Cr.AllDay = false;
                        // TimeZone.CurrentTimeZone.ToLocalTime(item.StartDate);
                        lst.Add(Cr);
                    }
                }
            }
            return lst;
        }

        [HttpGet]
        [Route("api/Calender/ResourceTimeOff")]
        public List<CalenderResponse> GetAllResourceTimeOff()
        {
            var userId = User.Identity.GetUserId();
            var user = AppUserManager.Users.Include(x => x.Resource).FirstOrDefault(x => x.Id == userId);
            List<CalenderResponse> lst = new List<CalenderResponse>();
            if (user.ResourceId != null)
            {
                using (var dbContextScope = _dataContextScopeFactory.Create())
                {
                    var ctx = dbContextScope.DataContexts.Get<PatientCareDbContext>();

                    string s = _timeManagementService.GetStringForLeaveRequestList(Convert.ToInt32(user.ResourceId), true);
                    List<LeaveRequestsAdminApprovalListResponseView> leaveList = new List<LeaveRequestsAdminApprovalListResponseView>();
                    var query = ctx.Database.SqlQuery<LeaveRequestsAdminApprovalListResponseView>(s);
                    leaveList = query.ToList();

                    foreach (var item in leaveList)
                    {
                        CalenderResponse Cr = new CalenderResponse();
                        Cr.CalenderType = "ResourceTimeOff";
                        Cr.EventType = "TimeOff";
                        Cr.Id = item.Id;
                        Cr.ResourceName = item.ResourceName;
                        Cr.ResourceId = item.ResourceId;
                        Cr.SupervisorName = item.SupervisorName;
                        Cr.SupervisorId = item.SupervisorId;
                        Cr.Reason = item.Reason;
                        Cr.LeaveType = item.LeaveTitle;
                        Cr.LeaveTypeId = item.LeaveTypeId;
                        Cr.CommentFromSupervisor = (item.CommentFromSupervisor != null) ? item.CommentFromSupervisor : null;
                        Cr.IsPaidType = item.IsPaidType;
                        Cr.TakenLeaves = item.TakenLeaves;
                        Cr.RemainingLeave = Cr.RemainingLeave;
                        if (item.Status == "Approved")
                        {
                            Cr.ClassName = "calendar-timeoff-approved";
                            Cr.Title = item.ResourceName + " Time Off Request - " + item.Status + ".";
                            Cr.StatusClassName = "calendar-timeoffstatus-approved";
                        }
                        if (item.Status == "Pending")
                        {
                            Cr.ClassName = "calendar-timeoff-pending";
                            Cr.Title = item.ResourceName + " Time Off Request - " + item.Status + ".";
                            Cr.StatusClassName = "calendar-timeoffstatus-pending";
                        }
                        if (item.Status == "Rejected")
                        {
                            Cr.ClassName = "calendar-timeoff-rejected";
                            Cr.Title = item.ResourceName + " Time Off Request - " + item.Status + ".";
                            Cr.StatusClassName = "calendar-timeoffstatus-rejected";
                        }
                        //string dateDiff = ((item.EndDate - item.StartDate).TotalDays).ToString();                        
                        Cr.Status = item.Status;
                        Cr.Start = item.StartDate;
                        Cr.End = item.EndDate;
                        Cr.AllDay = false;
                        lst.Add(Cr);
                    }
                }
            }
            return lst;
        }
    }
}
