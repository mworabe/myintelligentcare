﻿"use strict";
angular.module("EDZoutstaffingPortalApp")
    .controller("ChatContactCntrl", [
		"$scope",
		'toaster',
        'chatData',
        '$uibModalInstance',
        'ChatService',
        'TwilioChatFactory',
        'Auth',
        '$stateParams',
		function ($scope, toaster, chatData, $uibModalInstance, ChatService, TwilioChatFactory, Auth, $stateParams) {

		    $scope.dataItems = chatData[0];
		    var client=TwilioChatFactory.getClient();
		    var currentUser = Auth.getCurrentUser();

		    $scope.isLoading = false;

		    $scope.dataList = [];
		    if ($stateParams.view == "patients") {
		        $scope.title = "Patients";
		        $scope.dataList = $scope.dataItems.chatPatientLists;
		        $scope.isPatientView = true;
		    } else {
		        $scope.title = "Staff";
		        $scope.dataList = $scope.dataItems.chatTherapistLists;
		    }
		    
		    
		    $scope.tabs = [{
		        id: "Staff",
		        title: "Staff",
		    }, {
		        id: "Patients",
		        title: "Patients",
		    }];

		    $scope.selectedTab = $scope.tabs[0].id;
		    $scope.isTabSelected = function (id) {
		        if (id == $scope.selectedTab) return true;
		        return false;
		    }
		    $scope.setSelectedTab = function (id) {
		        $scope.selectedTab = id;
		    }


		    $scope.onItemClick = function (item) {
		        if (item) {
		            checkSessionExist(item);
		        }
		    }
            
		    function checkSessionExist(item) {
		        var req = {
		            fromUserId: currentUser.nameid,
		            toUserId: item.userId
		        }
		        $scope.isLoading = ChatService.checkSessionExist(req, function (result) {
		            if (result && result.chatDetails && result.chatDetails.length > 0) {
		                redirect(result.chatDetails[0]);
		            } else {
		                createChannel(item);
		            }
		        }, function (error) {
		            toaster.pop("error", "", "Unable to get session");
		        });
		    }

		    function createChannel(item) {
		        if (client) {
		            var attrs = {
		                fromUserId: currentUser.nameid,
		                toUserId: item.userId
		            }
		            var uniqueName = attrs.fromUserId + "-" + attrs.toUserId;
		            
		            $scope.isLoading=client.createChannel({
		                attributes: attrs,
		                friendlyName: "test1",
		                isPrivate: false,
		                uniqueName: uniqueName
		            }).then(function joinChannel(channel) {
		                channel.join().then(function (channel) {
		                    submitNewSession(attrs, channel);
		                });
		            }, function (error) {
		                toaster.pop("error", "", error.message);
		            });
		        }
		    }

		    function sendInvite(attrs, channel, result) {
		        var inviatedUser = attrs.toUserId;
		        $scope.isLoading=channel.invite(inviatedUser).then(function () {
		            console.log('Your friend has been invited!');
		            redirect(result);
		        }, function (error) {
		            toaster.pop("error", "", error.message);
		        });
		    }

		    function submitNewSession(attrs, channel) {
		        var request = {
		            channelsId: channel.sid,
		            channelUniqueName: channel.ChannelUniqueName,
		            channelName: channel.friendlyName,
		            createdByUserId: attrs.fromUserId,
		            type:1,
		            chatMembers: [
                        { userId: attrs.fromUserId },
		                { userId: attrs.toUserId }
		            ]
		        }

		        $scope.isLoading =ChatService.createNewSession(request, function (result) {
		            //redirect(result);
		            sendInvite(attrs, channel, result);
		        }, function (error) {
		            $scope.isLoading = false;
		        });
		    }


		    $scope.close = function () {
		        $uibModalInstance.dismiss('');
		    }

		    function redirect(result) {
		        $uibModalInstance.close(result);
		    }

		}
    ]);